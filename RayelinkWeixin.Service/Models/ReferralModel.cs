﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayelinkWeixin.Service.Models
{
    public class ReferralModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Desc { get; set; }
        public int OrganizationId { get; set; }
        public int DocId { get; set; }
        public string Status { get; set; }
        public int CreaterId { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreaterName { get; set; }
        public int Sex { get; set; }
        public int Age { get; set; }
        public string Imgs { get; set; }
    }
}