﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Data.Const;
using RayelinkWeixin.Data.Repository;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;

namespace RayelinkWeixin.WxApi.Tests
{
    [TestClass()]
    public class CustomTests
    {
        [TestMethod()]
        public void SendTextAsyncTest()
        {
            string openid = Base64Helper.DecodeBase64("b0VTMzl0aEgyYnREQ001aTVSSkR2emUybDBybw==");
            //new Custom(WeixinConfig.appID, WeixinConfig.appSecret).SendText(openid, "您已成功付款，远程门诊预约卡号及密码已短信发送至您的手机，请注意查收！如有任何疑问，请联系021-62457275，谢谢配合");

            //new Custom(WeixinConfig.appID, WeixinConfig.appSecret).SendTextAsync(openid, "您已成功付款，远程门诊预约卡号及密码已短信发送至您的手机，请注意查收！如有任何疑问，请联系021-62457275，谢谢配合");


            WxTemplateData temp = new WxTemplateData();

            //刚创建转诊单待建档，要给护士推送消息
            temp.first = new TemplateDataItem("您好，您有一条转诊单待处理");
            temp.keyword1 = new TemplateDataItem("消息测试");
            temp.keyword2 = new TemplateDataItem("测试使用");
            temp.remark = new TemplateDataItem("请及时查看！");

            //new RayelinkWeixin.WxApi.TemplateMessage(WeixinConfig.appID, WeixinConfig.appSecret).SendTemplateMessage(openid, "Uz-eEInZOf2uixHqLx8-bqlEDG04eY2GTM5mT7fwIMg", temp, "https://www.baidu.com");
            //new RayelinkWeixin.WxApi.TemplateMessage(WeixinConfig.appID, WeixinConfig.appSecret).SendTemplateMessageAsync(openid, "Uz-eEInZOf2uixHqLx8-bqlEDG04eY2GTM5mT7fwIMg", temp, "https://www.baidu.com");


            Assert.Fail();
        }
    }
}