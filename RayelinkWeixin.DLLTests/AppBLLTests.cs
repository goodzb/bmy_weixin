﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Model;
using System;
using System.Collections;
using System.Collections.Generic;

namespace RayelinkWeixin.DLL.Tests
{
    [TestClass()]
    public class AppBLLTests
    {
        [TestMethod()]
        public void InsertTest()
        {
            IList<UserActionEventLogInfo> list = new List<UserActionEventLogInfo>();
            for (int i = 0; i < 50000; i++)
            {
                UserActionEventLogInfo info = new UserActionEventLogInfo()
                {
                    ActionName = "Test_" + i,
                    ActionType = "click",
                    RequestUrl = "https://www.bing.com/translator",
                    Parameter = "测试",
                    ActionDesc = "批量增加性能测试",
                    ActionSid = Guid.NewGuid(),
                    ActionStatus = 1,
                    AppId = 1,
                    ModID = 1,
                    ActionDate = 20170531,
                    ActionYear = (short)DateTime.Now.Year,
                    ActionMonth = (short)DateTime.Now.Month,
                    ActionDay = (short)DateTime.Now.Day,
                    ActionHour = (short)DateTime.Now.Hour,
                    ReferUrl = "http://www.cnblogs.com/haight/p/5208682.html",
                    ActionIp = "127.0.0.1",
                    OpenId = RadomHelper.GetRandomString(30, false)

                };
                var newModel = BusinessContent.UserActionEventLog.CheckData(info);
                list.Add(newModel);
            }

            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
            int result = BusinessContent.UserActionEventLog.BulkInsert(list, true);
            TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            //0天0小时1分钟46秒
            string dateDiff = ts.Days.ToString() + "天" + ts.Hours.ToString() + "小时" + ts.Minutes.ToString() + "分钟" + ts.Seconds.ToString() + "秒";

            ts1 = new TimeSpan(DateTime.Now.Ticks);
            bool res = BusinessContent.UserActionEventLog.BulkInsert(list);
            ts2 = new TimeSpan(DateTime.Now.Ticks);
            ts = ts1.Subtract(ts2).Duration();

            //0天0小时0分钟4秒
            dateDiff = ts.Days.ToString() + "天" + ts.Hours.ToString() + "小时" + ts.Minutes.ToString() + "分钟" + ts.Seconds.ToString() + "秒";

            return;
            //var appInfo = new Model.AppInfo()
            //{
            //    AppId = 5,
            //    AppName = "测试",
            //    AppSid = Guid.NewGuid(),
            //    AppCreateTime = DateTime.Now,
            //    AppLastUpdateTime = DateTime.Now,
            //    AppEncodingAESKey = "AppEncodingAESKey",
            //    AppKey = "AppKey",
            //    AppPwd = "123",
            //    AppWebSite = "AppWebSite",
            //    AppAuthrizeType = 1,
            //    AppType = 1,
            //    AppWelcomingType = 1,
            //    AppWelcomingContent = "欢迎你",
            //    AppStatus = 1,
            //    AppTicket = "AAAAAAAAA",
            //    ExpiresTime = DateTime.Now,
            //    LastTokenDateTime = DateTime.Now,
            //    TokenExpires = 10,
            //    AppToken = "AppToken",
            //    AppCreator = 11,
            //    AppAgentID = 1,
            //    AppSecret = "AppSecret",
            //    AppHeading = "AppHeading",
            //    AppShortName = "AppShortName",
            //    AppUpdator = 123,
            //    AppUserName = "AppUserName"
            //}; var m = 0;
            //// m = BusinessContent.App.Insert(appInfo);

            //var appInfo2 = appInfo;
            //appInfo2.AppName = "修改后名称";
            //appInfo2.AppType = 2;
            //m = BusinessContent.App.Update(appInfo2);

            //var res = BusinessContent.App.FindOne(string.Format("AppId={0}", appInfo.AppId));

            Assert.Fail();
        }


        [TestMethod()]
        public void AccountTest()
        {
            BusinessContent.Account.Insert(new AccountInfo
            {
                Email = "123456789@msn.cn",
                Pwd = TextHelper.MD5("666666"),
                DisplayName = "测试用户",
                Type = 1,
                Status = 1
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod()]
        public void FileTest()
        {
            FileLogHelper.Instance.Log("读取log的测试");
            var files = FilesHelper.EnumerateFiles(System.AppDomain.CurrentDomain.BaseDirectory + "\\logs");

            foreach (string file in files)
            {
                string name = FilesHelper.Read2Buffer(file, 1024);
            }

        }

    }
}