﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using System;
using System.Collections.Generic;

namespace Pay.Tests
{
    [TestClass()]
    public class PayTests
    {
        [TestMethod()]
        public void PayTest()
        {
            try
            {
                var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(Guid.Parse("77386407-1748-4DBA-ADC1-9499CE43877D"));  // 从缓存里面获取一个订单的信息
                string out_refund_no = RadomHelper.GetRandomString(30, false);
                new RayelinkWeixin.WxApi.Pay(WeixinConfig.appID, WeixinConfig.appSecret, WeixinConfig.TenPayV3_MchId, WeixinConfig.TenPayV3_MchId).Refund(WeixinConfig.TenPayV3_Key, @"E:\Code\WorkCode\bmy_weixin\RayelinkWeixin.Web\cert\1226312102.p12", new Dictionary<string, string>() {
                { "out_trade_no",orderInfo.Out_trade_no},  //商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
               { "out_refund_no",out_refund_no},//商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。
                { "total_fee","1"},//订单总金额，单位为分，只能为整数，详见支付金额
                { "refund_fee","1"},//退款总金额，订单总金额，单位为分，只能为整数，详见支付金额
                { "op_user_id",WeixinConfig.TenPayV3_MchId}, //操作员Id，默认就是商户号             
            });
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
            Assert.Fail();
        }
    }
}