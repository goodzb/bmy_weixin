﻿using RayelinkWeixin.Data.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;
using RayelinkWeixin.Data.PO.Extend;
using RayelinkWeixin.Common.Utilites;
using System.Configuration;

namespace RayelinkWeixin.WinService
{
    partial class ReferralService : ServiceBase
    {
        public string[] status = new string[] { "", "110", "121", "", "122" };//CRS预约单状态与转诊单状态对应

        protected CRS_Service.CRS_ServiceSoapClient client = new CRS_Service.CRS_ServiceSoapClient();
        protected Timer timer=null;

        public ReferralService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: 在此处添加代码以启动服务。
            //Task.Factory.StartNew(TimerDo);
            LogHelper.Debug("启动");
            TimerCallback timerCallback = new TimerCallback(ReferAsync);
            AutoResetEvent autoEvent = new AutoResetEvent(false);

            timer= new System.Threading.Timer(timerCallback, autoEvent, 1000 * 3, 1000 * 60*60);//服务启动 后3秒执行 每隔60分钟执行一次
            //timer = new System.Threading.Timer(timerCallback, autoEvent, 1000 * 3, 1000 * 60 );//服务启动 后3秒执行 每隔1分钟执行一次
        }

        protected override void OnStop()
        {
            // TODO: 在此处添加代码以执行停止服务所需的关闭操作。
            if (timer!=null)
            {
                timer.Dispose();
            }
            LogHelper.Debug("服务停止");
        }
        
        protected void ReferAsync(object arg)
        {
            try
            {
                LogHelper.Debug("执行一次new");
                List<ReferralExtend> refers = ReferralRepository.Instance.FindReferralByPage(int.MaxValue, 1, "[Status]='103'", "Id desc");
                string appointNos = string.Join(",", refers.Select(r => r.AppointmentNo).ToArray());
                LogHelper.Debug("appointNos=" + appointNos);
                if (string.IsNullOrWhiteSpace(appointNos)) return;
                var res = client.GetAppointmentList(appointNos);

                if (!res.Success) return;
                if (string.IsNullOrWhiteSpace(res.Data.ToString())) return;

                List<ReturnModel> list = JsonConvert.DeserializeObject<List<ReturnModel>>(res.Data.ToString());
                foreach (var item in list)
                {
                    LogHelper.Debug("item：" + item.id + "--" + item.status + "--" + item.data.ToString("yyyy-MM-dd HH:mm:ss"));
                    //Status为0再判断预约日期是否已过
                    if (item.status == 0)
                    {
                        if (item.data.Date < DateTime.Now.Date)
                        {
                            //如果过期就是未就诊
                            item.status = 4;
                        }
                    }

                    //还没有到预约的时间而且为取消的，不处理
                    if (item.status != 2 && item.data.Date >= DateTime.Now.Date) continue;
                    //更新操作
                    ReferralExtend refer = refers.Find(r => r.AppointmentNo == item.id);
                    if (refer == null) continue;
                    //LogHelper.Debug("转诊单信息：" + refer.Id + "-" + refer.Status);
                    refer.Status = status[item.status];//把CRS那边返回的状态转换为我们转诊单的状态
                    ReferralRepository.Instance.UpdateReferral(refer, 0, item.updatetime);
                    //LogHelper.Debug("更新结果：" + rr);

                    //已就诊的要推送
                    if (item.status == 1)
                    {
                        string domain = ConfigurationManager.AppSettings["webDomain"];
                        string jsonStr = JsonHelper.ObjectToJSON(refer);

                        LogHelper.Debug("推送序列化：" + jsonStr);
                        string push = HttpHelper.PostReq(domain + "/service/PushWxMsg", jsonStr);
                        LogHelper.Debug("推送结果：" + push);
                    }

                }
            }
            catch (Exception e)
            {
                LogHelper.Error(e);
            }
        }
    }

    public class ReturnModel
    {
        public string id { get; set; }
        public int status { get; set; }
        public string docid { get; set; }
        public DateTime data { get; set; }
        public string DocName { get; set; }
        public int orgid { get; set; }
        public string orgname { get; set; }
        public string updatetime { get; set; }
    }
}
