﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.WinService
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main()
        {
            //加入log日志
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\log4net.config");
            log4net.Config.XmlConfigurator.Configure(fileInfo);

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ReferralService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
