﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：UserController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-19
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using RayelinkWeixin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    //[AllowAnonymous]
    public class UserController : BaseController
    {
        public static DateTime ConvertTime(long milliTime)
        {
            long timeTricks = new DateTime(1970, 1, 1).Ticks + milliTime * 10000 + TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours * 3600 * (long)10000000;
            return new DateTime(timeTricks);
        }
        /// <summary>
        /// 绑定账号页面
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [WeixinSpeficAuthorize]
        public ActionResult Index()
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            //用户退出微信帐号，重新登录时，用户绑定状态会丢失，需要这里判断一下
            bool isBinded = false;
            var obj = Request.Params["openid"];
            if (!string.IsNullOrWhiteSpace(obj))
            {
                obj = Base64Helper.DecodeBase64(obj);
                string userName = obj.ToString();
                //用户退出微信帐号，重新登录时，用户绑定状态会丢失，需要这里判断一下
                //还要判断是否登陆了
                CRMWebService.ServiceForWebSoapClient client = new CRMWebService.ServiceForWebSoapClient();
                var jsonStr = client.CRM_WEB_LoginByPartner(obj, WeixinConfig.crmPartner);
                var model = JsonConvert.DeserializeObject<CRMLoginModel>(jsonStr);
                if (model != null && model.result == "0")
                {
                    var account = AuthorizeUser.GetAuthorizeUser(obj);
                    if (account != null)
                    {
                        isBinded = true;
                    }
                }
            }
            if (isBinded)
            {
                //用户退出微信帐号，重新登录时，用户绑定状态会丢失，需要这里判断一下isBinded：true
                return RedirectToAction("UserCenter", "User");
            }
            else
            {
                return View();
            }
        }
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult AddUser(int type, string hidUrl)
        {
            Session[KeyConst.SessionOrderUrl] = hidUrl;
            ViewBag.type = type;

            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            return View();
        }
        /// <summary>
        /// 就诊人页面
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public ActionResult ChooseUser(int type)
        {
            var url1 = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url1);
            var url = Session[KeyConst.SessionOrderUrl];
            Session[KeyConst.SessionOrderUrl] = null;
            UserModel user = new UserModel();
            if (url != null)
            {
                url = Base64Helper.DecodeBase64(url.ToString());
                if (url.ToString().IndexOf("&userId") >= 0)
                {
                    int index = url.ToString().LastIndexOf('&');
                    user.url = url.ToString().Substring(0, index);
                }
                else
                {
                    user.url = url.ToString();
                }
            }
            // type =1 来自预约的更换就诊人
            user.type = type;
            return View(user);
        }

        /// <summary>
        /// 获得就诊人列表
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult GetChooseUserList()
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var list = new List<UserModel>();
            var pid = 0;
            var name = "";
            var phone = "";
            var cardId = "";
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    pid = user.pid;
                }
            }
            CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
            var returnUserinfo = crmClient.CRM_WEB_GetMemberModelByPid(pid);
            if (returnUserinfo != null)
            {
                name = returnUserinfo.Name;
                phone = returnUserinfo.Mobile;
                if (returnUserinfo.MemberCertList != null)
                {
                    var cardInfo = returnUserinfo.MemberCertList.Where(p => p.CertTypeCode == "CID").OrderByDescending(p => p.UpdateTime).FirstOrDefault(); // 身份证号 
                    if (cardInfo != null)
                    {
                        cardId = cardInfo.CertNo;
                    }
                }
            }

            var returnModel = client.FamilyMember_GetList(pid);
            var familyMemberArry = returnModel.Data;
            var familyMemberList = familyMemberArry.ToList().Select(p => new UserModel()
            {
                id = p.Id,
                FamilyName = p.FamilyName,
                FamilyMobile = p.FamilyMobile.Substring(0, 3) + "*****" + p.FamilyMobile.Substring(p.FamilyMobile.Length - 3, 3),
                Gender = p.Gender,
                IsDefault = p.IsDefault,
                IDCardNo = p.IDCardNo != "" ? p.IDCardNo.Substring(0, 3) + "****************" + p.IDCardNo.Substring(p.IDCardNo.Length - 4, 4) : "",
                Birthday = p.Birthday,

            });
            bool isDefault = false;
            if (familyMemberList != null && familyMemberList.Count() > 0)
            {
                if (familyMemberList.Where(p => p.IsDefault == true).Count() == 0)
                {
                    isDefault = true;
                }
            }
            else
            {
                isDefault = true;
            }

            string sex = "";
            if (!String.IsNullOrEmpty(cardId))
            {
                int str = int.Parse(cardId.Substring(cardId.Length - 2, 1));
                if (str % 2 == 0)
                {
                    sex = "女";
                }
                else
                {
                    sex = "男";
                }
            }
            var nowUser = new UserModel()
            {
                id = pid,
                FamilyName = name,
                FamilyMobile = phone.Substring(0, 3) + "*****" + phone.Substring(phone.Length - 3, 3),
                Gender = sex,
                IsDefault = isDefault,
                IDCardNo = cardId != "" ? cardId.Substring(0, 3) + "****************" + cardId.Substring(cardId.Length - 4, 4) : "",
                Birthday = null
            };
            list.Add(nowUser);
            list.AddRange(familyMemberList.ToArray());
            return Json(list);
        }

        /// <summary>
        /// 跳转至编辑就诊人界面
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public ActionResult EditUser(int id)
        {
            var url1 = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url1);
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            UserModel model = new UserModel();
            var pid = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    pid = user.pid;
                }
            }
            ViewBag.pid = pid;
            // 判断选择的就诊人是否是crm当前绑定账户
            if (pid == id)
            {
                CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
                var returnUserinfo = crmClient.CRM_WEB_GetMemberModelByPid(pid);
                var name = "";
                var phone = "";
                var cardId = "";
                if (returnUserinfo != null)
                {
                    name = returnUserinfo.Name;
                    phone = returnUserinfo.Mobile;
                    if (returnUserinfo.MemberCertList != null)
                    {
                        var cardInfo = returnUserinfo.MemberCertList.Where(p => p.CertTypeCode == "CID").OrderByDescending(p => p.UpdateTime).FirstOrDefault(); // 身份证号 
                        if (cardInfo != null)
                        {
                            cardId = cardInfo.CertNo;
                        }
                    }
                }
                // 判断当前绑定账户是否是默认就诊人
                int isdefault = 0;
                var returnModelList = client.FamilyMember_GetList(pid);
                var familyMemberArry = returnModelList.Data;
                if (familyMemberArry != null)
                {
                    var familyMemberList = familyMemberArry.ToList().Where(p => p.IsDefault == true).FirstOrDefault();
                    if (familyMemberList != null)
                    {
                        isdefault = 0;
                    }
                    else
                    {
                        isdefault = 1;
                    }
                    model = new UserModel
                    {
                        FamilyName = name,
                        FamilyMobile = phone,
                        IDCardNo = cardId,
                        Gender = "",
                        id = pid,
                        isSetDefault = isdefault
                    };
                }
            }
            else
            {
                var url = Request.Url.ToString().Split('#')[0];
                ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

                var returnModel = client.FamilyMember_Get(id);
                var famliyMember = returnModel.Data;
                int isdefault = 0;
                if (famliyMember.IsDefault)
                {
                    isdefault = 1;
                }
                model = new UserModel
            {
                FamilyName = famliyMember.FamilyName,
                FamilyMobile = famliyMember.FamilyMobile,
                IDCardNo = famliyMember.IDCardNo,
                Gender = famliyMember.Gender,
                id = famliyMember.Id,
                isSetDefault = isdefault
            };
            }

            return View(model);
        }

        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        /// <summary>
        /// 添加就诊人
        /// </summary>
        /// <returns></returns>
        public ActionResult User_Add(string name, string phone, string id, int isDefault)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var pid = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    pid = user.pid;
                }
            }
            bool isdefault = false;
            if (isDefault == 1)
            {
                isdefault = true;
            }
            int str = int.Parse(id.Substring(id.Length - 2, 1));
            string sex = "";
            if (str % 2 == 0)
            {
                sex = "女";
            }
            else
            {
                sex = "男";
            }
            var returnModel = client.FamilyMember_Add(pid, name, phone, id, isdefault, "", null, sex, "", null);
            return Json(new { success = returnModel.Success, data = returnModel.Data }); ;
        }

        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        /// <summary>
        /// 编辑就诊人
        /// </summary>
        /// <returns></returns>
        public ActionResult User_Edit(int id, string name, string phone, string cardid, int isDefault)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
            var pid = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    pid = user.pid;
                }
            }
            // 判断选择的就诊人是否是crm当前绑定账户
            if (pid != id)
            {
                bool isdefault = false;
                if (isDefault == 1)
                {
                    isdefault = true;
                }
                int str = int.Parse(cardid.Substring(cardid.Length - 2, 1));
                string sex = "";
                if (str % 2 == 0)
                {
                    sex = "女";
                }
                else
                {
                    sex = "男";
                }
                var returnModel = client.FamilyMember_Update(id, "", name, phone, "", null, sex, cardid, isdefault);
                return Json(new { success = returnModel.Success });
            }
            else
            {
                CRMWebService.Member member = crmClient.CRM_WEB_GetMemberModelByPid(pid);
                member.Name = name;
                var returnMember = crmClient.CRM_WEB_UpdatePersonBaseWithCert(member, "CID", cardid);
                bool suc = false;
                if (returnMember == 1)
                {
                    suc = true;
                }
                if (isDefault == 1)
                {
                    var returnModelList = client.FamilyMember_GetList(pid);
                    var familyMemberArry = returnModelList.Data;
                    if (familyMemberArry != null)
                    {
                        var familyMemberList = familyMemberArry.ToList().Where(p => p.IsDefault == true).FirstOrDefault();
                        if (familyMemberList != null)
                        {
                            client.FamilyMember_Update(familyMemberList.Id, familyMemberList.Relationship, familyMemberList.FamilyName, familyMemberList.FamilyMobile, familyMemberList.FamilyAddress, familyMemberList.Birthday, familyMemberList.Gender, familyMemberList.IDCardNo, false);
                        }
                    }
                }
                return Json(new { success = suc });
            }
        }

        public ActionResult User_Delete(int id, int isDefault)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var returnModel = client.FamilyMember_Delete(id);
            return Json(new { success = returnModel.Success });
        }

        [WeixinSpeficAuthorize]
        /// <summary>
        /// 我的病例
        /// </summary>
        /// <returns></returns>
        public ActionResult MyCaseList(int cardNo = 0)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            ViewBag.cardNo = cardNo;
            return View();
        }

        /// <summary>
        /// 我的病例列表
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult MyCase()
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            List<UserCaseModel> newList = new List<UserCaseModel>();
            var url = RayelinkWeixin.Const.Config.WeixinConfig.crsUrl;

            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            if (user != null)
            {
                var returnModel = client.EMR_UserCase_Get(user.pid);
                CRSService.ReturnModelOfString ned = new CRSService.ReturnModelOfString();

                var caseJson = returnModel.Data != null ? returnModel.Data.ToString() : "";
                var list = JsonConvert.DeserializeObject<List<UserCaseModel>>(caseJson);
                string docIdList = "";
                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        docIdList += "'" + item.接诊医生工号 + "'" + ",";
                    }
                }
                // 根据医生工号获取医生详情
                docIdList = docIdList != "" ? docIdList.TrimEnd(',') : docIdList;
                var docList = client.getDoctorListByNumber(docIdList).Data;
                string doctorListJson = "";
                if (docList != null)
                {
                    doctorListJson = client.getDoctorListByNumber(docIdList).Data.ToString();
                }
                var doctorList = JsonConvert.DeserializeObject<List<DoctorModel>>(doctorListJson);

                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        UserCaseModel userCase = new UserCaseModel();

                        if (doctorList != null)
                        {
                            var doctorModel = doctorList.FirstOrDefault(d => d.doctorNumber == item.接诊医生工号);
                            var commonNum = 0;
                            // 获得留言
                            var latestUserMessage = client.getLatestUserMessage(doctorModel.id, user.pid, int.Parse(item.就诊ID));
                            if (latestUserMessage != null && latestUserMessage.Data != null)
                            {
                                var returnMessage = latestUserMessage.Data.ToString();
                                var messageModel = JsonConvert.DeserializeObject<Message>(returnMessage);
                                if (!String.IsNullOrEmpty(returnMessage))
                                {
                                    commonNum = 1;
                                    // 获得留言回复信息
                                    var replyMessage1 = client.getMessageReplyList(messageModel.id);
                                    if (replyMessage1 != null)
                                    {
                                        var replyMessage = replyMessage1.Data;
                                        if (replyMessage != null)
                                        {
                                            var relpyList = JsonConvert.DeserializeObject<List<Message>>(replyMessage.ToString());
                                            if (relpyList != null)
                                            {
                                                commonNum = relpyList.Count() + 1;
                                            }
                                        }
                                    }
                                }
                            }

                            if (doctorModel != null)
                            {
                                userCase.评论数量 = commonNum.ToString();
                                userCase.图章 = doctorModel.stampUrl == null ? "" : url + doctorModel.stampUrl;
                                userCase.医生头像 = doctorModel.image == null ? "" : url + doctorModel.image;
                                userCase.医生职称 = doctorModel.titleName;
                                userCase.接诊医生姓名 = doctorModel.name;
                                userCase.接诊医生科室 = doctorModel.depName2;
                            }
                        }

                        userCase.PID = item.PID;
                        userCase.就诊ID = item.就诊ID;
                        userCase.就诊人 = item.就诊人;
                        userCase.就诊日期 = item.就诊日期;
                        userCase.就诊时间 = item.就诊时间;
                        userCase.诊断结论 = item.诊断结论;
                        userCase.诊断建议 = item.诊断建议;
                        userCase.诊所ID = item.诊所ID;
                        userCase.病历ID = item.病历ID;
                        userCase.主诉 = item.主诉;
                        userCase.是否查看 = item.是否查看;
                        userCase.既往病史 = item.既往病史;
                        userCase.接诊医生工号 = item.接诊医生工号;
                        newList.Add(userCase);
                    }
                }

            }

            var listJson = newList.Select(p => new
                {
                    PID = p.PID,
                    VisitID = p.就诊ID,
                    VisitName = p.就诊人,
                    VisitDate = p.就诊日期.Substring(0, 4),
                    VisitTime = p.就诊日期.Substring(4, 2) + "/" + p.就诊日期.Substring(6, 2),
                    DocName = p.接诊医生姓名,
                    DocDepartment = p.接诊医生科室,
                    conclusion = p.诊断结论,
                    suggest = p.诊断建议,
                    OrgId = p.诊所ID,
                    Title = p.医生职称,
                    CaseId = p.病历ID,
                    isLook = p.是否查看,
                    userCase = p.主诉,
                    docImage = p.医生头像,
                    oldCase = p.既往病史,
                    DoctorId = p.接诊医生工号,
                    commonNum = p.评论数量
                }).OrderByDescending(p => p.VisitTime);
            var casesJson = JsonConvert.SerializeObject(listJson);
            return Json(casesJson);
        }

        [WeixinSpeficAuthorize]
        /// <summary>
        /// 我的病例详情
        /// </summary>
        /// <returns></returns>
        public ActionResult MyCaseDetail(int orgId, int caseId, string VisitID)
        {
            var url1 = Request.Url.ToString().Split('#')[0];
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url1);
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();

            var returnModel = client.EMR_UserCaseDetails_Get(caseId, orgId, user.pid);
            var model = new UserCaseModel();

            if (returnModel != null && returnModel.Data != null)
            {
                var caseJson = returnModel.Data.ToString();
                var list = JsonConvert.DeserializeObject<List<UserCaseModel>>(caseJson);


                if (user != null)
                {
                    if (list != null && list.Count > 0)
                    {
                        model = list[0];
                        var doctorList = new List<DoctorModel>();
                        // 根据工号查询医生详情
                        var doctors = client.getDoctorListByNumber("'" + model.接诊医生工号 + "'");
                        int doctorID = 0;
                        if (doctors != null && doctors.Data != null)
                        {
                            var commonNum = 0;


                            string doctorListJson = doctors.Data.ToString();
                            doctorList = JsonConvert.DeserializeObject<List<DoctorModel>>(doctorListJson);
                            var url = RayelinkWeixin.Const.Config.WeixinConfig.crsUrl;
                            if (doctorList != null)
                            {
                                var doctorModel = doctorList.FirstOrDefault(d => d.doctorNumber == model.接诊医生工号);
                                if (doctorModel != null)
                                {

                                    model.图章 = doctorModel.stampUrl == null ? "" : url + doctorModel.stampUrl;
                                    model.医生头像 = doctorModel.image == null ? "" : url + doctorModel.image;
                                    model.医生职称 = doctorModel.titleName;
                                    model.接诊医生姓名 = doctorModel.name;
                                    model.接诊医生科室 = doctorModel.depName2;
                                    ViewBag.docId = doctorModel.id;
                                    doctorID = doctorModel.id;
                                }
                            }
                            model.就诊日期 = DateTime.ParseExact(model.就诊日期, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture).ToString("yyyy年MM月dd日");
                            model.就诊时间 = DateTime.ParseExact(model.就诊时间, "HHmmss", System.Globalization.CultureInfo.CurrentCulture).ToString("HH:mm:ss");

                            //查看是否有检查报告
                            var returnCheckReport = client.EMR_Report_Get(int.Parse(VisitID)).Data;
                            if (returnCheckReport != null && returnCheckReport != "")
                            {
                                var reportList = JsonConvert.DeserializeObject<List<ReportModel>>(returnCheckReport);
                                model.listReportModel = reportList;
                            }

                            //查看是否有影像图片
                            //var returnPicture = client.EMR_PacsPictures_GetByTrId(int.Parse(VisitID), orgId).Data;
                            //if (returnPicture != null && returnPicture != "")
                            //{
                            //    var picList = JsonConvert.DeserializeObject<List<PacsPicturesModel>>(returnPicture);
                            //    model.listPacsPicturesModel = picList;
                            //    //LogHelper.Info("病历详情:查看是否有影像图片：" + returnPicture);
                            //}
                            // 留言
                            ViewBag.Uname = user.username;
                            var latestUserMessage = client.getLatestUserMessage(doctorID, user.pid, int.Parse(model.就诊ID));
                            if (latestUserMessage != null && latestUserMessage.Data != null)
                            {
                                var returnMessage = latestUserMessage.Data.ToString();
                                var messageModel = JsonConvert.DeserializeObject<Message>(returnMessage);

                                StringBuilder html = new StringBuilder();
                                if (!String.IsNullOrEmpty(returnMessage))
                                {
                                    commonNum = 1;
                                    ViewBag.messageId = messageModel.id;
                                    string addtime = messageModel.date;
                                    var adddate = long.Parse(addtime.Split('(')[1].Split(')')[0]);
                                    messageModel.date = Convert.ToDateTime(ConvertTime(adddate)).ToString("yyyy.MM.dd");

                                    html.Append("<div class='user_info_wrap'>");
                                    html.Append("<figure class='user_icon' style='width:50px,height:50px,border-radius:50%'>");
                                    html.AppendFormat("<img src='{0}' alt='' style='border-radius:50%;height:50px;width:50px'>", AuthorizeUser.headimgurl());
                                    html.Append("</figure>");
                                    html.Append(" <span class='user_info_name' data-id=" + messageModel.id + ">" + user.username + "<span class='comment_date'>" + messageModel.date + "</span></span>");
                                    html.Append("</div>");
                                    html.Append("</div>");
                                    html.Append("<div class='comments_content_wrap'>");
                                    html.Append("<p class='user_comment'>" + messageModel.info + "</p>");
                                    // 回复留言
                                    var replyMessage1 = client.getMessageReplyList(messageModel.id);

                                    if (replyMessage1 != null)
                                    {
                                        var replyMessage = replyMessage1.Data;
                                        if (replyMessage != null)
                                        {
                                            var relpyList = JsonConvert.DeserializeObject<List<Message>>(replyMessage.ToString());
                                            if (relpyList != null)
                                            {
                                                commonNum = relpyList.Count() + 1;
                                                int preType = 1;
                                                foreach (var mes in relpyList)
                                                {
                                                    string time = mes.CreateTime.ToString();
                                                    var date = long.Parse(time.Split('(')[1].Split(')')[0]);
                                                    mes.CreateTime = Convert.ToDateTime(ConvertTime(date)).ToString("yyyy.MM.dd");
                                                    // 医生回复
                                                    if (mes.Type == 0)
                                                    {
                                                        html.Append("<p class='doc_comments'>");
                                                        html.Append("<span class='docs_name'>" + model.接诊医生姓名 + "医生</span>回复" + "：");
                                                        html.Append(mes.Content);
                                                        html.Append("<span class='comments_date'>" + mes.CreateTime + "</span>");
                                                        html.Append("</p>");
                                                    }
                                                    // 用户回复
                                                    else
                                                    {
                                                        if (preType == 1)
                                                        {
                                                            html.Append("<p class='doc_comments'>");
                                                            html.Append("<span class='users_name'>" + messageModel.userName + "</span>" + "：");
                                                            html.Append(mes.Content);
                                                            html.Append("<span class='comments_date'>" + mes.CreateTime + "</span>");
                                                            html.Append("</p>");
                                                        }
                                                        else
                                                        {
                                                            html.Append("<p class='doc_comments'>");
                                                            html.Append("<span class='users_name'>" + messageModel.userName + "</span>回复医生" + "：");
                                                            html.Append(mes.Content);
                                                            html.Append("<span class='comments_date'>" + mes.CreateTime + "</span>");
                                                            html.Append("</p>");
                                                        }

                                                    }
                                                    preType = mes.Type;
                                                }
                                            }
                                        }
                                    }
                                    html.Append("</div>");
                                }
                                model.html = html.ToString();
                            }
                            model.评论数量 = commonNum.ToString();

                        }

                    }
                }
            }
            else
            {
                model.html = "";
                model.PID = 0;
                model.messageId = 0;
                model.病历ID = 0;
                model.费用 = 0;
                model.既往病史 = "";
                model.接诊医生工号 = "";
                model.接诊医生科室 = "";
                model.接诊医生姓名 = "";
                model.就诊ID = "";
                model.就诊人 = "";
                model.就诊日期 = "";
                model.就诊时间 = "";
                model.类型 = "";
                model.评论数量 = "0";
                model.是否查看 = 0;
                model.图章 = "";
                model.医生头像 = "";
                model.医生职称 = "";
                model.诊断建议 = "";
                model.诊断结论 = "";
                model.诊所ID = 0;
                model.诊所名 = "";
                model.主诉 = "";
            }
            return View(model);
        }

        [WeixinSpeficAuthorize]
        /// <summary>
        ///添加回复信息
        /// </summary>
        /// <returns></returns>
        public ActionResult AddReplyMessage(int messageId, string content)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var returnMessage = client.MessageReply_Add(messageId, content, "微信");
            return Json(new { success = returnMessage.Success, data = returnMessage.Data });
        }

        /// <summary>
        /// 添加留言
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="docId"></param>
        /// <param name="trID"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult AddMessage(int orgId, int docId, int trID, string content)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            StringBuilder html = new StringBuilder();
            if (user != null)
            {
                var returnMessage = client.addUserMessage(user.pid, docId, user.username, orgId, trID, content, "微信");
                if (returnMessage.Success)
                {
                    html.Append("<div class='user_info_wrap'>");
                    html.Append("<figure class='user_icon' style='border-radius:50%;height:50px;width:50px;'>");
                    html.AppendFormat("<img src='{0}' alt='' style='border-radius:50%;height:50px;width:50px'>", AuthorizeUser.headimgurl());
                    html.Append("</figure>");
                    html.Append(" <span class='user_info_name' data-id=" + returnMessage.Data + ">" + user.username + "<span class='comment_date'>" + DateTime.Now.ToString("yyyy.MM.dd") + "</span></span>");
                    html.Append("</div>");
                    html.Append("<div class='comments_content_wrap'>");
                    html.Append("<p class='user_comment'>" + content + "</p>");
                    html.Append("</div>");
                }
            }
            return Content(html.ToString());
        }
        /// <summary>
        /// 我的评价页
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult MyComments()
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View();
        }
        /// <summary>
        /// 我的预约是否就诊
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult MyOrders()
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View();
        }

        /// <summary>
        /// 我的未评价预约记录列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public ActionResult MyUnCommentList(int pageIndex, int pageSize)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            if (user != null)
            {
                //从预约记录接口抓已到诊且未评价的数据
                var returnModel = client.ResvIntention_GetList(user.pid); 

                var listJson = returnModel.Data
                    .Where(m => m.Status.Value == 7 && m.HasComment == false)  //状态: 0-未确认, 1-已确认点诊, 2-已确认, 4-点诊失败, 5-异常, 6-未到诊, 7-已到诊, 8-已取消
                    .OrderByDescending(p => p.ResvDate).ThenByDescending(p => p.Duration)
                    .Select(p => new ResvIntention
                    {
                        DoctorId = p.DoctorId,
                        ConfirmTime = p.ConfirmTime,
                        HospName = p.HospName,
                        VisitAddress = p.VisitAddress,
                        Name = p.Name,
                        Price = p.Price,
                        DoctorName = p.DoctorName,
                        HospId = p.HospId,
                        DepName = p.DepName,
                        ResvDateString = p.ResvDate.ToString("yyyy年MM月dd日"),
                        Duration = p.Duration,
                        TitleName = "",
                        Image = "",
                        TypeString = p.Type.Value == 0 ? "门诊" : (p.Type.Value == 1 ? "加号" : "点诊"),
                        OfflineOrRemote = p.IsRemote == true ? "远程就诊" : "线下就诊",
                        VisitId = p.TrId,
                        CaseId = 0,
                        HasComment = p.HasComment,
                        RemoteHospId = p.RemoteHospId
                    }).Skip((pageIndex - 1) * pageSize).Take(pageSize);

                List<ResvIntention> newList = new List<ResvIntention>();
                foreach (var item in listJson)
                {
                    int caseID = 0;
                    if (item.VisitId > 0)
                    {
                        //根据就诊ID抓病历ID
                        var userCaseObj = client.EMR_UserCase_GetByTrID(item.VisitId, item.RemoteHospId, user.pid).Data;
                        if (userCaseObj != null)
                        {
                            string userCaseJson = userCaseObj.ToString();
                            if (userCaseJson.Length > 0)
                            {
                                var caseList = JsonConvert.DeserializeObject<List<UserCaseModel>>(userCaseJson);
                                if (caseList != null && caseList.Count > 0)
                                {
                                    caseID = caseList[0].病历ID;
                                }
                            }
                        }
                    }
                    //获取医生详情
                    var doctorModel = client.getDoctor(item.DoctorId);
                    var doctor = JsonConvert.DeserializeObject<DoctorModel>(doctorModel.Data.ToString());

                    //输出对象
                    ResvIntention newResvIntention = new ResvIntention();
                    newResvIntention.DoctorId = item.DoctorId;
                    newResvIntention.ConfirmTime = item.ConfirmTime;
                    newResvIntention.HospName = item.HospName;
                    newResvIntention.VisitAddress = item.VisitAddress;
                    newResvIntention.Name = item.Name;
                    newResvIntention.Price = item.Price;
                    newResvIntention.DoctorName = item.DoctorName;
                    newResvIntention.HospId = item.HospId;
                    newResvIntention.RemoteHospId = item.RemoteHospId;
                    //newResvIntention.DepName = item.DepName;
                    newResvIntention.ResvDateString = item.ResvDateString;
                    newResvIntention.Duration = item.Duration;
                    if (doctor != null)
                    {
                        newResvIntention.TitleName = doctor.titleName;
                        newResvIntention.Image = doctor.image;
                        newResvIntention.DepName = doctor.depName2;
                    }
                    newResvIntention.TypeString = item.TypeString;
                    newResvIntention.OfflineOrRemote = item.OfflineOrRemote;
                    newResvIntention.VisitId = item.VisitId;
                    newResvIntention.CaseId = caseID;
                    newResvIntention.HasComment = item.HasComment;
                    newList.Add(newResvIntention);
                }

                string caseJson = JsonConvert.SerializeObject(newList);
                return Json(caseJson);
            }
            else
            {
                return Json("");
            }
        }

        /// <summary>
        /// 获取评价信息
        /// </summary>
        /// <param name="visitId"></param>
        /// <param name="doctorId"></param>
        /// <param name="hospId"></param>
        /// <returns></returns>
        public ActionResult GetWriteCommentBasicInfo(int visitId, int doctorId, int hospId)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();

            var doctorJson = client.getDoctor(doctorId).Data.ToString();
            var doctorEntity = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
            string hospJson = client.getOrganization(hospId).Data.ToString();
            var hospEntity = JsonConvert.DeserializeObject<HospitalModel>(hospJson);
            return Json("[{\"DoctorName\":\"" + doctorEntity.name
                + "\", \"DoctorImage\":\"" + (doctorEntity.image == null ? "" : doctorEntity.image)
                + "\", \"DoctorTitle\":\"" + (doctorEntity.titleName == null ? "" : doctorEntity.titleName)
                + "\", \"DeptName\":\"" + (doctorEntity.depName2 == null ? "" : doctorEntity.depName2)
                + "\", \"HospName\":\"" + (hospEntity.hospitalName == null ? "" : hospEntity.hospitalName) + "\"}]");
        }

        /// <summary>
        /// 添加评价
        /// </summary>
        /// <param name="visitId"></param>
        /// <param name="doctorId"></param>
        /// <param name="hospId"></param>
        /// <param name="docComment"></param>
        /// <param name="hospComment"></param>
        /// <param name="docStar"></param>
        /// <param name="hospStar"></param>
        /// <returns></returns>
        public ActionResult AddComment(int visitId, int doctorId, int hospId, string docComment, string hospComment, int docStar, int hospStar)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            if (user != null)
            {
                CRSService.ReturnModel returnModel = client.addComment(doctorId, docStar, docComment, hospId, hospStar,
                    hospComment, user.pid, user.username, visitId, "微信");
                return Json(returnModel);
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 我的评价列表
        /// </summary>
        /// <returns></returns>
        public ActionResult MyCommentList(int pageIndex, int pageSize, int viewVisitID)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            var returnModel = new CRSService.ReturnModel();
            if (user != null)
            {
                if (viewVisitID > 0) //>0意味着是从其他页面点“查看评价”进来的
                {
                    //如果是点“查看评价”要定位某个特定的评价信息，则要把评价全部加载出来
                    returnModel = client.getMyCommentList(user.pid, 1, int.MaxValue); //user.pid
                }
                else
                {
                    //如果是直接进的我的评价列表，则分页抓。因为每一个就诊ID对应医生和诊所两笔评价记录，所以这里要*2
                    returnModel = client.getMyCommentList(user.pid, pageIndex, pageSize * 2); //user.pid
                }

                List<UserCommentModel> commentList = JsonConvert.DeserializeObject<List<UserCommentModel>>(returnModel.Data.ToString());

                if (commentList != null && commentList.Count > 0)
                {
                    var distinctVisitID = commentList.Select(p => p.visitId).Distinct();
                    //因bug 1124 微信：我的预约-已就诊页面，点击查看评价，跳转到我的评价-已评价页面，此时应该定位到该评价，而不是将该评价筛选出来
                    //注释掉下面的逻辑
                    //if (viewVisitID > 0)
                    //{
                    //    distinctVisitID = commentList.Where(v => v.visitId == viewVisitID).Select(p => p.visitId).Distinct();
                    //}

                    //因为要显示医生相关信息，所以这里把医生信息先都抓出来，以减少下面频繁调用接口。
                    string doctorIDString = "";
                    foreach (var item in commentList)
                    {
                        doctorIDString += item.subjectId + ",";
                    }
                    string doctorListJson = client.getDoctorListBy(doctorIDString.TrimEnd(',')).Data.ToString();
                    var doctorList = JsonConvert.DeserializeObject<List<DoctorModel>>(doctorListJson);


                    List<MyComment> myCommentList = new List<MyComment>();
                    foreach (var visitID in distinctVisitID)
                    {
                        //循环拼装评价列表对象
                        MyComment myCommentEntity = new MyComment();
                        int doctorID = 0;
                        var commentToDoctor = commentList.FirstOrDefault(p => p.visitId == visitID && p.type == 1);
                        if (commentToDoctor != null)
                        {
                            doctorID = commentToDoctor.subjectId;
                        }
                        int hospID = 0;
                        var commentToHosp = commentList.FirstOrDefault(p => p.visitId == visitID && p.type == 2);
                        if (commentToHosp != null)
                        {
                            hospID = commentToHosp.subjectId;
                        }
                        if (doctorID != 0 )
                        {
                            string timeString = commentToDoctor.add_time.Split('(')[1].Split(')')[0];
                            DateTime dateStart = new DateTime(1970, 1, 1);
                            DateTime addTime = dateStart.AddMilliseconds(double.Parse(timeString));

                            string hospJson = client.getOrganization(hospID).Data.ToString();
                            var hospEntity = JsonConvert.DeserializeObject<HospitalModel>(hospJson);


                            myCommentEntity.DoctorId = doctorID;
                            if (doctorList != null)
                            {
                                var doctorEntity = doctorList.FirstOrDefault(p => p.id == doctorID);
                                if (doctorEntity != null)
                                {
                                    myCommentEntity.DoctorName = doctorEntity.name;
                                    myCommentEntity.TitleName = doctorEntity.titleName;
                                    myCommentEntity.DoctorImage = doctorEntity.image;
                                    myCommentEntity.DepName = doctorEntity.depName2;
                                }
                            }
                            myCommentEntity.DoctorComment = string.IsNullOrEmpty(commentToDoctor.content) ? "该用户未发表文字评价" : commentToDoctor.content;
                            myCommentEntity.DoctorReplyContent = string.IsNullOrEmpty(commentToDoctor.replyContent) ? "" : commentToDoctor.replyContent;
                            myCommentEntity.HasReplyComment = string.IsNullOrEmpty(commentToDoctor.replyContent) ? false : true;
                            myCommentEntity.DoctorStar = commentToDoctor.stars;
                            myCommentEntity.VisitId = visitID;
                            myCommentEntity.AddTimeString = addTime.ToString("yyyy.MM.dd");
                            myCommentEntity.HospId = hospID;
                            myCommentEntity.HospName = commentToHosp.subjectName;
                            myCommentEntity.HospImage = hospEntity != null && hospEntity.hospitalImage != null && hospEntity.hospitalImage != "" ? hospEntity.hospitalImage : "";
                            myCommentEntity.HospComment = string.IsNullOrEmpty(commentToHosp.content) ? "该用户未发表文字评价" : commentToHosp.content;
                            myCommentEntity.HospStar = commentToHosp.stars;
                            myCommentList.Add(myCommentEntity);
                        }
                    }

                    string caseJson = JsonConvert.SerializeObject(myCommentList);
                    return Json(caseJson);
                }
                else
                {
                    return Json("");
                }
            }
            else
            {
                return Json("");
            }
        }
        /// <summary>
        /// 我的预约列表
        /// </summary>
        /// <returns></returns>
        public ActionResult MyOrderList(int pageIndex, int pageSize, string listType)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);

            if (user != null)
            {
                var returnModel = client.ResvIntention_GetList(user.pid); //user.pid

                DateTime nowDate = DateTime.Today;
                string nowHour = DateTime.Now.ToString("HHmm");
                int[] weiJiuZhenStatus = { 1, 2 }; //我的预约：未就诊列表上只显示状态为1-已确认点诊, 2-已确认，并且预约日期大于等于今天的数据。已就诊列表上只显示状态为7-已到诊的数据
                var listJson = returnModel.Data
                    .Where(m => (listType == "未就诊" && m.ResvDate >= nowDate && weiJiuZhenStatus.Contains(m.Status.Value) && m.Status.Value != 1) // || (m.ResvDate == nowDate && string.Compare((m.Duration.Split('-').Length > 1?m.Duration.Split('-')[1]:m.Duration), nowHour) > 0)
                    || (listType == "已就诊" && m.Status.Value == 7))  // && m.Status.Value == 7//状态: 0-未确认, 1-已确认点诊, 2-已确认, 4-点诊失败, 5-异常, 6-未到诊, 7-已到诊, 8-已取消
                    .Select(p => new ResvIntention
                    {
                        DoctorId = p.DoctorId,
                        ConfirmTime = p.ConfirmTime,
                        HospName = p.HospName,
                        VisitAddress = p.VisitAddress,
                        Name = p.Name,
                        Price = p.Price,
                        DoctorName = p.DoctorName,
                        HospId = p.HospId,
                        DepName = p.DepName,
                        ResvDateString = p.ResvDate.ToString("yyyy年MM月dd日"),
                        Duration = p.Duration,
                        TitleName = "",
                        Image = "",
                        TypeString = p.Type.Value == 0 ? "门诊" : (p.Type.Value == 1 ? "加号" : "点诊"),
                        OfflineOrRemote = p.IsRemote == true ? "远程就诊" : "线下就诊",
                        VisitId = p.TrId,
                        CaseId = 0,
                        ResvDate = p.ResvDate,
                        HasComment = p.HasComment,
                        StartTime = DurationTime(true, p.Duration),
                        EndTime = DurationTime(false, p.Duration),
                        RemoteHospId = p.RemoteHospId
                    });

                //排序
                if (listJson != null)
                {
                    if (listType == "已就诊")
                    {
                        listJson = listJson.OrderByDescending(p => p.ResvDate).ThenByDescending(p => p.EndTime);
                    }
                    else
                    {
                        listJson = listJson.Where(l => l.ResvDate >= nowDate).OrderBy(p => p.ResvDate).ThenBy(p => p.StartTime); // || (l.ResvDate == nowDate && l.EndTime > int.Parse(nowHour))
                    }
                }

                //因为要显示医生相关信息，所以这里把医生信息先都抓出来，以减少下面频繁调用接口。
                string doctorIDString = "";
                foreach (var item in listJson)
                {
                    doctorIDString += item.DoctorId + ",";
                }
                var doctorListObj = client.getDoctorListBy(doctorIDString.TrimEnd(',')).Data;
                List<DoctorModel> doctorList = new List<DoctorModel>();
                if (doctorListObj != null)
                {
                    string doctorListJson = doctorListObj.ToString();
                    doctorList = JsonConvert.DeserializeObject<List<DoctorModel>>(doctorListJson);
                }

                List<ResvIntention> newList = new List<ResvIntention>();
                //循环拼装评价列表对象
                foreach (var item in listJson)
                {
                    //根据就诊ID抓病历ID
                    int caseID = 0;
                    if (item.VisitId > 0)
                    {
                        var userCaseObj = client.EMR_UserCase_GetByTrID(item.VisitId, item.RemoteHospId, user.pid).Data;
                        if (userCaseObj != null)
                        {
                            string userCaseJson = userCaseObj.ToString();
                            if (userCaseJson.Length > 0)
                            {
                                var caseList = JsonConvert.DeserializeObject<List<UserCaseModel>>(userCaseJson);
                                if (caseList != null && caseList.Count > 0)
                                {
                                    caseID = caseList[0].病历ID;
                                }
                            }
                        }
                    }
                    ResvIntention newResvIntention = new ResvIntention();
                    newResvIntention.DoctorId = item.DoctorId;
                    newResvIntention.ConfirmTime = item.ConfirmTime;
                    newResvIntention.HospName = item.HospName;
                    newResvIntention.VisitAddress = item.VisitAddress;
                    newResvIntention.Name = item.Name;
                    newResvIntention.Price = item.Price;
                    newResvIntention.DoctorName = item.DoctorName;
                    newResvIntention.HospId = item.HospId;
                    newResvIntention.RemoteHospId = item.RemoteHospId;
                    //newResvIntention.DepName = item.DepName;
                    newResvIntention.ResvDateString = item.ResvDateString;
                    newResvIntention.Duration = item.Duration;
                    if (doctorList != null && doctorList.Count > 0)
                    {
                        var doctorModel = doctorList.FirstOrDefault(d => d.id == item.DoctorId);
                        if (doctorModel != null)
                        {
                            newResvIntention.TitleName = doctorModel.titleName;
                            newResvIntention.Image = doctorModel.image;
                            newResvIntention.DepName = doctorModel.depName2;
                        }
                    }
                    newResvIntention.TypeString = item.TypeString;
                    newResvIntention.OfflineOrRemote = item.OfflineOrRemote;
                    newResvIntention.VisitId = item.VisitId;
                    newResvIntention.CaseId = caseID;
                    newResvIntention.HasComment = item.HasComment;
                    newList.Add(newResvIntention);
                }

                string caseJson = JsonConvert.SerializeObject(newList);
                return Json(caseJson);
            }
            else
            {
                return Json("");
            }
        }

        private int DurationTime(bool isStart, string duration)
        {
            int startTime = 0;
            int endTime = 0;

            if (duration.IndexOf("-") > 0)
            {
                //10:00-12:00 --> 1200
                bool validStartTime = int.TryParse(duration.Split('-')[0].Replace(":", ""), out startTime);
                bool validEndTime = int.TryParse(duration.Split('-')[1].Replace(":", ""), out endTime);
            }
            else
            {
                if (duration == "上午")
                {
                    startTime = 0;
                    endTime = 1200;
                }
                if (duration == "下午")
                {
                    startTime = 1201;
                    endTime = 1800;
                }
                if (duration == "晚上")
                {
                    startTime = 1801;
                    endTime = 2400;
                }
            }
            if (isStart)
            {
                return startTime;
            }
            else
            {
                return endTime;
            }
        }
        /// <summary>
        /// 用户中心首页
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult UserCenter()
        {
            var model = new UserModel();
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            ServiceForWeChat.ServiceForWeChatSoapClient weClient = new ServiceForWeChat.ServiceForWeChatSoapClient();
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    if (client.GetUserInfoManifest(user.pid) != null && client.GetUserInfoManifest(user.pid).Data != null)
                    {
                        var returnUserInfo = client.GetUserInfoManifest(user.pid).Data.ToString();
                        var userInfo = JsonConvert.DeserializeObject<UserModel>(returnUserInfo);
                        model.UserName = user.username;
                        model.image = AuthorizeUser.headimgurl();
                        model.orderNum = userInfo.orderNum;
                        model.commNum = userInfo.commNum;
                        model.caseNum = userInfo.caseNum;
                    }
                    // 获得可用代金券数量
                    var returnModel = weClient.CRM_WeChat_GetAllCoupon(user.openid, 0);
                    List<ServiceForWeChat.CouponALL> couPonAllList = new List<ServiceForWeChat.CouponALL>();
                    if (returnModel != null && returnModel.Length > 0)
                    {
                        couPonAllList = returnModel.ToList();
                        var now = DateTime.Now;
                        couPonAllList = couPonAllList.Where(p => p.CouponAccountp.State == 0 && (p.CouponAccountp.OverdueDate == null || DateTime.Compare(p.CouponAccountp.OverdueDate.Value, now) > 0)).ToList();
                    }
                    model.MyCashNum = couPonAllList.Count;
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        /// <summary>
        /// 送心意
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public ActionResult UserSendPrice(int doctorId)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            var model = new DoctorModel();
            var docEnt = client.getDoctor(doctorId);
            if (docEnt != null && docEnt.Data != null)
            {
                var jsonString = docEnt.Data.ToString();
                model = JsonConvert.DeserializeObject<DoctorModel>(jsonString);
                if (string.IsNullOrEmpty(model.image))
                {
                    model.image = "../images/default.png";
                }
                else
                {
                    model.image = WeixinConfig.crsUrl + model.image;
                }
            }
            return View(model);

        }

        /// <summary>
        /// 添加病例页
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult AddCase()
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var pid = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    pid = user.pid;
                }
            }
            var returnModel = client.FamilyMember_GetList(pid);
            if (returnModel.Data != null)
            {
                var familyMemberArry = returnModel.Data.ToArray();
                var familyMemberModel = familyMemberArry.FirstOrDefault(p => p.IsDefault == true);
                // 判断就诊人列表是否有默认就诊人
                if (familyMemberModel != null)
                {
                    var userModel = new UserModel()
                    {
                        FamilyName = familyMemberModel.FamilyName,
                        id = familyMemberModel.Id
                    };
                    return View(userModel);
                }
                else
                {
                    CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
                    var returnUserinfo = crmClient.CRM_WEB_GetMemberModelByPid(pid);
                    if (returnUserinfo != null)
                    {
                        var userModel = new UserModel()
                        {
                            FamilyName = returnUserinfo.Name,
                            id = 0
                        };
                        return View(userModel);
                    }
                }

            }
            var userModelNull = new UserModel()
            {
                FamilyName = "",
                id = 0
            };
            return View(userModelNull);
        }
        /// <summary>
        /// 对医生的评价
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult WriteComments(int visitID)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View();
        }
        [WeixinSpeficAuthorize]
        /// <summary>
        /// 我的卡券
        /// </summary>
        /// <returns></returns>
        public ActionResult MyCards()
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View();
        }

        [WeixinSpeficAuthorize]
        /// <summary>
        /// 我的卡券列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMyCardsList(string type)
        {
            ServiceForWeChat.ServiceForWeChatSoapClient client = new ServiceForWeChat.ServiceForWeChatSoapClient();
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            var openid = "";
            if (user != null)
            {
                openid = user.openid;
            }
            // 0: 所有和已过期 1未使用 2已使用
            int status = 0;
            if (type == "all" || type == "yiguoqi")
            {
                status = 0;
            }
            else if (type == "weishiyong")
            {
                status = 1;
            }
            else if (type == "yishiyong")
            {
                status = 2;
            }
            var returnModel = client.CRM_WeChat_GetAllCoupon(openid, status);
            List<ServiceForWeChat.CouponALL> couPonAllList = new List<ServiceForWeChat.CouponALL>();
            StringBuilder html = new StringBuilder();
            if (returnModel != null && returnModel.Length > 0)
            {
                couPonAllList = returnModel.OrderByDescending(p => p.CouponAccountp.AddDate).ToList();
                var now = DateTime.Now;
                // 获取未使用数据
                var weishiyongList = couPonAllList.Where(p => p.CouponAccountp.State == 0 && (p.CouponAccountp.OverdueDate == null || DateTime.Compare(p.CouponAccountp.OverdueDate.Value, now) > 0));
                // 获取已使用数据
                var yishiyongList = couPonAllList.Where(p => p.CouponAccountp.State == 1 && (p.CouponAccountp.OverdueDate == null || DateTime.Compare(p.CouponAccountp.OverdueDate.Value, now) > 0));
                // 获取已过期数据
                var yiguoqiList = couPonAllList.Where(p => p.CouponAccountp.OverdueDate != null && DateTime.Compare(p.CouponAccountp.OverdueDate.Value, now) < 0);
                // 全部or 未使用
                if (type == "all" || type == "weishiyong")
                {
                    if (weishiyongList.Count() > 0)
                    {
                        html.Append("<section class='new_cards_wrap' data-status='weishiyong'>");
                        foreach (ServiceForWeChat.CouponALL coupon in weishiyongList)
                        {
                            // 判断有效期类型
                            if (coupon.Couponp.OverdueType == 1)
                            {
                                html.Append("<figure class='cards_bg' data-prize='" + coupon.Couponp.CouponValue.Value.ToString("0") + "'>");
                                html.Append("<span class='use_date_wrap'>有效期：<span class='use_date'>" + (coupon.Couponp.EndDate != null ? coupon.Couponp.BeginDate.Value.ToString("yyyy.MM.dd") : "") + "-" + (coupon.Couponp.EndDate != null ? coupon.Couponp.EndDate.Value.ToString("yyyy.MM.dd") : "") + "</span></span>");
                                html.Append(" <span class='card_id_wrap'>券号：<span class='ard_id'>" + coupon.CouponAccountp.CouponCode + "</span></span>");
                                html.Append(" </figure>");
                            }
                            else
                            {
                                html.Append("<figure class='cards_bg' data-prize='" + coupon.Couponp.CouponValue.Value.ToString("0") + "'>");
                                html.Append("<span class='use_date_wrap'>有效期：<span class='use_date'>" + (coupon.CouponAccountp.AddDate != null ? coupon.CouponAccountp.AddDate.Value.ToString("yyyy.MM.dd") : "") + "-" + (coupon.CouponAccountp.OverdueDate != null ? coupon.CouponAccountp.OverdueDate.Value.ToString("yyyy.MM.dd") : "") + "</span></span>");
                                html.Append(" <span class='card_id_wrap'>券号：<span class='ard_id'>" + coupon.CouponAccountp.CouponCode + "</span></span>");
                                html.Append(" </figure>");
                            }
                        }
                        html.Append("</section>");
                    }
                }
                // 全部or 已使用
                if (type == "all" || type == "yishiyong")
                {
                    if (yishiyongList.Count() > 0)
                    {
                        html.Append("<div class='fenge'>");
                        html.Append("<p>已失效的券</p>");
                        html.Append("</div>");
                        html.Append("<section class='old_cards_wrap' data-status='yishiyong'>");
                        foreach (ServiceForWeChat.CouponALL coupon in yishiyongList)
                        {
                            if (coupon.Couponp.OverdueType == 1)
                            {
                                html.Append("<figure class='cards_bg' data-prize='" + coupon.Couponp.CouponValue.Value.ToString("0") + "'>");
                                html.Append("<span class='use_date_wrap'>有效期：<span class='use_date'>" + (coupon.Couponp.EndDate != null ? coupon.Couponp.BeginDate.Value.ToString("yyyy.MM.dd") : "") + "-" + (coupon.Couponp.EndDate != null ? coupon.Couponp.EndDate.Value.ToString("yyyy.MM.dd") : "") + "</span></span>");
                                html.Append(" <span class='card_id_wrap'>券号：<span class='ard_id'>" + coupon.CouponAccountp.CouponCode + "</span></span>");
                                html.Append(" </figure>");
                            }
                            else
                            {
                                html.Append("<figure class='cards_bg' data-prize='" + coupon.Couponp.CouponValue.Value.ToString("0") + "'>");
                                html.Append("<span class='use_date_wrap'>有效期：<span class='use_date'>" + (coupon.CouponAccountp.AddDate != null ? coupon.CouponAccountp.AddDate.Value.ToString("yyyy.MM.dd") : "") + "-" + (coupon.CouponAccountp.OverdueDate != null ? coupon.CouponAccountp.OverdueDate.Value.ToString("yyyy.MM.dd") : "") + "</span></span>");
                                html.Append(" <span class='card_id_wrap'>券号：<span class='ard_id'>" + coupon.CouponAccountp.CouponCode + "</span></span>");
                                html.Append(" </figure>");
                            }
                        }
                        html.Append("</section>");
                    }
                }
                // 全部or 已过期
                if (type == "all" || type == "yiguoqi")
                {
                    if (yiguoqiList.Count() > 0)
                    {
                        html.Append("<div class='fenge'>");
                        html.Append("<p>已失效的券</p>");
                        html.Append("</div>");
                        html.Append("<section class='old_cards_wrap' data-status='yiguoqi'>");
                        foreach (ServiceForWeChat.CouponALL coupon in yiguoqiList)
                        {
                            if (coupon.Couponp.OverdueType == 1)
                            {
                                html.Append("<figure class='cards_bg' data-prize='" + coupon.Couponp.CouponValue.Value.ToString("0") + "'>");
                                html.Append("<span class='use_date_wrap'>有效期：<span class='use_date'>" + (coupon.Couponp.EndDate != null ? coupon.Couponp.BeginDate.Value.ToString("yyyy.MM.dd") : "") + "-" + (coupon.Couponp.EndDate != null ? coupon.Couponp.EndDate.Value.ToString("yyyy.MM.dd") : "") + "</span></span>");
                                html.Append(" <span class='card_id_wrap'>券号：<span class='ard_id'>" + coupon.CouponAccountp.CouponCode + "</span></span>");
                                html.Append(" </figure>");
                            }
                            else
                            {
                                html.Append("<figure class='cards_bg' data-prize='" + coupon.Couponp.CouponValue.Value.ToString("0") + "'>");
                                html.Append("<span class='use_date_wrap'>有效期：<span class='use_date'>" + (coupon.CouponAccountp.AddDate != null ? coupon.CouponAccountp.AddDate.Value.ToString("yyyy.MM.dd") : "") + "-" + (coupon.CouponAccountp.OverdueDate != null ? coupon.CouponAccountp.OverdueDate.Value.ToString("yyyy.MM.dd") : "") + "</span></span>");
                                html.Append(" <span class='card_id_wrap'>券号：<span class='ard_id'>" + coupon.CouponAccountp.CouponCode + "</span></span>");
                                html.Append(" </figure>");
                            }
                        }
                        html.Append("</section>");
                    }
                }


            }
            return Content(html.ToString());
        }

        [WeixinSpeficAuthorize]
        /// <summary>
        /// 我的关注页面
        /// </summary>
        /// <returns></returns>
        public ActionResult MyAttentions()
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            var objDept = CacheHelper.GetCache(KeyConst.CacheAllDept);//缓存的数据
            var objTitle = CacheHelper.GetCache(KeyConst.CacheAllTitle);//缓存的数据
            var listDepartment = new List<SelectItemModel>();
            var listTitle = new List<SelectItemModel>();
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            if (objDept != null)
            {
                listDepartment = objDept as List<SelectItemModel>;
            }
            else
            {
                listDepartment.Add(new SelectItemModel()
                {
                    id = "",
                    name = "全部"
                });

                var returnModel = client.getDeptList();//获取科室列表
                var deptJson = returnModel.Data.ToString();
                var deptList = JsonConvert.DeserializeObject<List<SelectItemModel>>(deptJson);
                listDepartment.AddRange(deptList.ToArray());
                if (listDepartment.Count > 0)
                {
                    CacheHelper.SetCache(KeyConst.CacheAllDept, listDepartment);//添加进缓存
                }
            }
            if (objTitle != null)
            {
                listTitle = objTitle as List<SelectItemModel>;
            }
            else
            {
                listTitle.Add(new SelectItemModel()
                {
                    id = "",
                    name = "全部"
                });
                var returnModel1 = client.getTitleList();//获取职称列表
                var titleJson = returnModel1.Data.ToString();
                var titleList = JsonConvert.DeserializeObject<List<SelectItemModel>>(titleJson);
                listTitle.AddRange(titleList.ToArray());
                if (listTitle.Count > 0)
                {
                    CacheHelper.SetCache(KeyConst.CacheAllTitle, listTitle);//添加进缓存
                }
            }
            ViewBag.Department = listDepartment;
            ViewBag.Title = listTitle;
            return View();
        }
        /// <summary>
        /// 我的关注列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDoctorList(int? deptid, int? titleid, string keyword)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var pid = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    pid = user.pid;
                }
            }
            var returnModel = client.getMyAttentionDoctorList(deptid, titleid, keyword, pid);
            var doctorListJson = "";
            if (returnModel.Data != null)
            {
                doctorListJson = returnModel.Data.ToString();
            }
            if (doctorListJson == "")
            {
                doctorListJson = "[]"; //防止前台jquery ajax报parseerror 错误
            }
            return Content(doctorListJson);
        }

        [WeixinSpeficAuthorize]
        public ActionResult DoctorSearch()
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View();
        }
        /// <summary>
        /// 添加病例：验证是否已经被添加或是否可添加
        /// </summary>
        /// <returns></returns>
        public ActionResult CheckPid(string name, int cardNo)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();

            var pid = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    pid = user.pid;
                }
            }
            var result = client.EMR_UserCase_Add(pid, cardNo, name);
            if (result != null && result.Success)
            {
                return Json(new { success = true, data = result.Data.ToString() });
            }
            else
            {
                return Json(new { success = false, message = result.Message });
            }


        }
        [AllowAnonymous]
        /// <summary>
        /// 检查姓名是否与原姓名一致
        /// </summary>
        /// <returns></returns>
        public JsonResult CheckName(string phoneNumber, string Name)
        {
            string message = "";
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
            var returnModel = crmClient.CRM_WEB_GetMemberByMobile(phoneNumber);
            if (returnModel != null)
            {
                var name = returnModel.Name;
                if (!Name.Equals(name))
                {
                    message = "用户名不一致，是否覆盖？";
                    return Json(new { success = false, message = message });
                }
                else
                {
                    return Json(new { success = true, message = message });
                }
            }
            else
            {
                return Json(new { success = true, message = message });
            }
        }
        [AllowAnonymous]
        /// <summary>
        /// 检查手机号是否被绑定
        /// </summary>
        /// <returns></returns>
        public JsonResult CheckIfPhoneBinded(string phoneNumber)
        {
            CRMWebService.ServiceForWebSoapClient client = new CRMWebService.ServiceForWebSoapClient();
            var existedOpenID = client.CRM_WEB_GetOpenIdByMobile(phoneNumber, WeixinConfig.crmPartner);

            if (existedOpenID != "-1" && existedOpenID != "0")
            {
                return Json(new { success = true, message = "此电话已经绑定" });
                //-1：参数非法
                //0：无数据
            }
            else if (existedOpenID == "-1")
            {
                return Json(new { success = true, message = "参数非法" });
            }
            else if (existedOpenID == "0")
            {
                return Json(new { success = false, message = "电话未绑定" });
            }
            return Json(new { success = false, message = "" });
        }
    }
}
