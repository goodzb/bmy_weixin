﻿using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Models;
using Senparc.Weixin;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using System;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 微信授权
    /// </summary>
    [AllowAnonymous]
    public class OAuthController : BaseController
    {
        #region 变量
        /// <summary>
        /// 来源url
        /// </summary>
        public string sourceUrl = string.Empty;

        /// <summary>
        /// 完成后跳转url
        /// </summary>
        public string callbackUrl = string.Empty;

        /// <summary>
        /// 授权信息
        /// </summary>
        public OAuthAccessTokenResult ati = null;

        #endregion  

        #region 用户每次验证是否绑定
        /// <summary>
        /// 用户每次验证是否绑定
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult ValidWeixinLogin(string id, int type = 0)
        {
            //GetAuthorizeUrl的state参数有长度限制，不能超过128个字节，所以用缓存进行数据传输。
            string cacheKey = Guid.NewGuid().ToString();
            CacheHelper.SetCache(cacheKey, id);
            //var _appId = WeixinConfig.appID;
            string url = string.Empty;
            string _redirect_uri = string.Empty;
            if (type == 0)
            {
                //url = OAuthApi.GetAuthorizeUrl(_appId, WeixinConfig.webDomain + "/OAuth/ValidWeixinOpenid", cacheKey, OAuthScope.snsapi_userinfo);              
                _redirect_uri = WeixinConfig.webDomain + "/OAuth/ValidWeixinOpenid?cacheKey=" + cacheKey;
            }
            else
            {
                // url = OAuthApi.GetAuthorizeUrl(_appId, WeixinConfig.webDomain + "/OAuth/ValidWxOpenId", cacheKey, OAuthScope.snsapi_userinfo);
                _redirect_uri = WeixinConfig.webDomain + "/OAuth/ValidWxOpenId?cacheKey=" + cacheKey;
            }
            string qu = string.Format("?type={0}&source={1}&redirect_uri={2}", "super", "ValidWeixinLogin", _redirect_uri);
            url = WeixinConfig.WxOAuthAuthorize + KeyConst.PayAppId + qu;
            return Redirect(url);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="openid"></param>
        /// <param name="userinfo"></param>
        /// <returns></returns>
        public ActionResult ValidWeixinOpenid(string cacheKey, string openid, string userinfo)
        {
            string encodeUrl = "";  //回调地址
            if (CacheHelper.GetCache(cacheKey) != null)
            {
                encodeUrl = CacheHelper.GetCache(cacheKey).ToString();
            }
            //LogHelper.Info("ValidWeixinOpenid-encodeUrl: " + encodeUrl);
            string returnUrl = "";
            if (!string.IsNullOrEmpty(cacheKey))
            {
                if (encodeUrl != "")
                {
                    returnUrl = Base64Helper.DecodeBase64(encodeUrl);
                }
                if (string.IsNullOrEmpty(returnUrl))
                {
                    returnUrl = WeixinConfig.webDomain;
                }

            }
            //获取用户信息
            OAuthUserInfo userInfo = JsonConvert.DeserializeObject<OAuthUserInfo>(userinfo);
            //添加到cookie中
            Session[KeyConst.SessionUserHeadImageUrl] = userInfo.headimgurl;
            Session[KeyConst.SessionUserNickName] = userInfo.nickname;
            //注册绑定用户openid 不需要密码
            CRMWebService.ServiceForWebSoapClient client = new CRMWebService.ServiceForWebSoapClient();
            var jsonStr = client.CRM_WEB_LoginByPartner(openid, WeixinConfig.crmPartner);
            var model = JsonConvert.DeserializeObject<CRMLoginModel>(jsonStr);
            var url = returnUrl;
            var port = WeixinConfig.port;
            if (!string.IsNullOrWhiteSpace(port))
            {
                url = url.Replace(":" + port, "");
            }
            if (model != null && model.result == "0")
            {
                //可以登陆成功
                AuthorizeUser.CreateLoginUserTicket(openid, model.userName);
            }

            if (url.IndexOf('?') >= 0 && url.IndexOf("&openid=") < 0)
            {
                url += "&openid=" + Base64Helper.EncodeBase64(openid);
            }
            else
            {
                url += "?openid=" + Base64Helper.EncodeBase64(openid);
            }

            if (url != "")
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("UserCenter", "User");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ActionResult ValidWeixinOpenid11(string code, string state)
        {
            //LogHelper.Info("ValidWeixinOpenid-parameter-state: " + state);
            string encodeUrl = "";  //回调地址
            if (CacheHelper.GetCache(state) != null)
            {
                encodeUrl = CacheHelper.GetCache(state).ToString();
                //CacheHelper.RemoveCache(state);
            }
            //LogHelper.Info("ValidWeixinOpenid-encodeUrl: " + encodeUrl);
            if (!string.IsNullOrEmpty(code))
            {
                string returnUrl = "";
                if (encodeUrl != "")
                {
                    returnUrl = Base64Helper.DecodeBase64(encodeUrl);
                }
                //LogHelper.Info("ValidWeixinOpenid-returnUrl:" + returnUrl);
                if (string.IsNullOrEmpty(returnUrl))
                {
                    returnUrl = WeixinConfig.webDomain;
                }
                var url = returnUrl;
                var port = WeixinConfig.port;
                if (!string.IsNullOrWhiteSpace(port))
                {
                    url = url.Replace(":" + port, "");
                }
                try
                {
                    var _appId = WeixinConfig.appID;
                    var _appSecret = WeixinConfig.appSecret;

                    var result = OAuthApi.GetAccessToken(_appId, _appSecret, code);
                    if (result.errcode != ReturnCode.请求成功)
                    {
                        return Content("错误：" + result.errmsg);
                    }
                    var openid = result.openid;
                    //获取用户信息
                    OAuthUserInfo userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                    //添加到cookie中
                    Session[KeyConst.SessionUserHeadImageUrl] = userInfo.headimgurl;
                    Session[KeyConst.SessionUserNickName] = userInfo.nickname;
                    //注册绑定用户openid 不需要密码
                    CRMWebService.ServiceForWebSoapClient client = new CRMWebService.ServiceForWebSoapClient();
                    var jsonStr = client.CRM_WEB_LoginByPartner(openid, WeixinConfig.crmPartner);
                    var model = JsonConvert.DeserializeObject<CRMLoginModel>(jsonStr);
                    if (model != null && model.result == "0")
                    {
                        //可以登陆成功
                        AuthorizeUser.CreateLoginUserTicket(openid, model.userName);
                    }

                    if (url.IndexOf('?') >= 0 && url.IndexOf("&openid=") < 0)
                    {
                        url += "&openid=" + Base64Helper.EncodeBase64(openid);
                    }
                    else
                    {
                        url += "?openid=" + Base64Helper.EncodeBase64(openid);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Info("ValidWeixinOpenid-Exception：" + ex.Message);
                    return RedirectToAction("UserCenter", "User");
                }
                if (url != "")
                {
                    return Redirect(url);
                }
                else
                {
                    return RedirectToAction("UserCenter", "User");
                }
            }
            else
            {
                LogHelper.Info("ValidWeixinOpenid-code为空");
                return RedirectToAction("UserCenter", "User");
            }
        }


        public ActionResult ValidWxOpenId(string cacheKey, string openid, string userinfo)
        {
            string encodeUrl = "";
            if (CacheHelper.GetCache(cacheKey) != null)
            {
                encodeUrl = CacheHelper.GetCache(cacheKey).ToString();
                //CacheHelper.RemoveCache(state);
            }
            //LogHelper.Info("ValidWeixinOpenid-encodeUrl: " + encodeUrl);
            if (!string.IsNullOrEmpty(cacheKey))
            {
                string returnUrl = "";
                if (encodeUrl != "")
                {
                    returnUrl = Base64Helper.DecodeBase64(encodeUrl);
                }
                if (string.IsNullOrEmpty(returnUrl))
                {
                    returnUrl = WeixinConfig.webDomain;
                }
                var url = returnUrl;
                var port = WeixinConfig.port;
                if (!string.IsNullOrWhiteSpace(port))
                {
                    url = url.Replace(":" + port, "");
                }
                try
                {
                    //获取用户信息
                    OAuthUserInfo userInfo = JsonConvert.DeserializeObject<OAuthUserInfo>(userinfo);
                    //添加到cookie中
                    Session[KeyConst.SessionUserHeadImageUrl] = userInfo.headimgurl;
                    Session[KeyConst.SessionUserNickName] = userInfo.nickname;

                    if (url.IndexOf('?') >= 0 && url.IndexOf("&openid=") < 0)
                    {
                        url += "&openid=" + Base64Helper.EncodeBase64(openid);
                    }
                    else
                    {
                        url += "?openid=" + Base64Helper.EncodeBase64(openid);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Info("ValidWeixinOpenid-Exception：" + ex.Message);
                    return RedirectToAction("MyReferral", "Referral");
                }
                if (url != "")
                {
                    return Redirect(url);
                }
                else
                {
                    return RedirectToAction("MyReferral", "Referral");
                }
            }
            else
            {
                LogHelper.Info("ValidWeixinOpenid-code为空");
                return RedirectToAction("MyReferral", "Referral");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ActionResult ValidWxOpenId111(string code, string state)
        {
            //LogHelper.Info("ValidWeixinOpenid-parameter-state: " + state);
            string encodeUrl = "";
            if (CacheHelper.GetCache(state) != null)
            {
                encodeUrl = CacheHelper.GetCache(state).ToString();
                //CacheHelper.RemoveCache(state);
            }
            //LogHelper.Info("ValidWeixinOpenid-encodeUrl: " + encodeUrl);
            if (!string.IsNullOrEmpty(code))
            {
                string returnUrl = "";
                if (encodeUrl != "")
                {
                    returnUrl = Base64Helper.DecodeBase64(encodeUrl);
                }
                //LogHelper.Info("ValidWeixinOpenid-returnUrl:" + returnUrl);
                if (string.IsNullOrEmpty(returnUrl))
                {
                    returnUrl = WeixinConfig.webDomain;
                }
                var url = returnUrl;
                var port = WeixinConfig.port;
                if (!string.IsNullOrWhiteSpace(port))
                {
                    url = url.Replace(":" + port, "");
                }
                try
                {
                    var _appId = WeixinConfig.appID;
                    var _appSecret = WeixinConfig.appSecret;

                    var result = OAuthApi.GetAccessToken(_appId, _appSecret, code);
                    if (result.errcode != ReturnCode.请求成功)
                    {
                        return Content("错误：" + result.errmsg);
                    }
                    var openid = result.openid;
                    //获取用户信息
                    OAuthUserInfo userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                    //添加到cookie中
                    Session[KeyConst.SessionUserHeadImageUrl] = userInfo.headimgurl;
                    Session[KeyConst.SessionUserNickName] = userInfo.nickname;

                    if (url.IndexOf('?') >= 0 && url.IndexOf("&openid=") < 0)
                    {
                        url += "&openid=" + Base64Helper.EncodeBase64(openid);
                    }
                    else
                    {
                        url += "?openid=" + Base64Helper.EncodeBase64(openid);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Info("ValidWeixinOpenid-Exception：" + ex.Message);
                    return RedirectToAction("MyReferral", "Referral");
                }
                if (url != "")
                {
                    return Redirect(url);
                }
                else
                {
                    return RedirectToAction("MyReferral", "Referral");
                }
            }
            else
            {
                LogHelper.Info("ValidWeixinOpenid-code为空");
                return RedirectToAction("MyReferral", "Referral");
            }
        }
        #endregion

        #region 用户第一次绑定使用的代码

        public ActionResult ValidBindUser(FormModel<BindUserModel> model)
        {
            var userName = model.TModel.UserName;
            var telPhone = model.TModel.Telphone;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return Content("非法请求");
            }
            if (string.IsNullOrWhiteSpace(telPhone))
            {
                return Content("非法请求！");
            }
            //此页面引导用户点击授权
            var _appId = WeixinConfig.appID;
            Session["UserNameBind"] = userName;
            Session["TelphoneBind"] = telPhone;
            //var url = OAuthApi.GetAuthorizeUrl(_appId, WeixinConfig.webDomain + "/OAuth/UseBaseCallback", "ruci", OAuthScope.snsapi_userinfo);
            string qu = string.Format("?type={0}&source={1}&redirect_uri={2}", "super", "ruci", WeixinConfig.webDomain + "/OAuth/UseBaseCallback");
            string url = WeixinConfig.WxOAuthAuthorize + KeyConst.PayAppId + qu;
            return Redirect(url);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ActionResult UseBaseCallback(string openid, string userinfo)
        {
            LogHelper.Info("UseBaseCallback-result.openid：" + openid);
            if (Session["UserNameBind"] == null || Session["TelphoneBind"] == null)
            {
                return Content("非法请求");
            }
            var userName = Session["UserNameBind"].ToString();
            var telPhone = Session["TelphoneBind"].ToString();
            Session["UserNameBind"] = null;
            Session["TelphoneBind"] = null;
            //if (string.IsNullOrEmpty(code))
            //{
            //    return Content("您拒绝了授权！");
            //}
            //if (state != "ruci")
            //{
            //    return Content("验证失败！请从正规途径进入！");
            //}
            //OAuthAccessTokenResult result = null;

            //通过，用code换取access_token
            try
            {
                //var _appId = WeixinConfig.appID;
                //var _appSecret = WeixinConfig.appSecret;
                //result = OAuthApi.GetAccessToken(_appId, _appSecret, code);
                //if (result.errcode != ReturnCode.请求成功)
                //{
                //    return Content("错误：" + result.errmsg);
                //}
                //var openid = result.openid;               

                CRMWebService.ServiceForWebSoapClient client = new CRMWebService.ServiceForWebSoapClient();

                var existedOpenID = client.CRM_WEB_GetOpenIdByMobile(telPhone, WeixinConfig.crmPartner);

                LogHelper.Info("existedOpenID：" + existedOpenID);
                if (existedOpenID != "-1" && existedOpenID != "0")
                {
                    //-1：参数非法
                    //0：无数据
                    int resultInt = client.CRM_WEB_RemovePartnerAccount(existedOpenID, WeixinConfig.crmPartner);
                    LogHelper.Info("resultInt：" + resultInt);
                    if (resultInt == 1)
                    {
                        LogHelper.Info("existedOpenID：" + existedOpenID + " 解绑成功");
                    }
                }

                //R998	参数不合法
                //R997	OpenID已被绑定
                //R996	注册失败，添加网站账号异常
                //R995	绑定接口异常
                //R000	注册成功，返回值的object[2]属性为PID等会员信息
                //注册绑定用户openid 不需要密码
                var jsonStr = client.CRM_WEB_RegisterBindOpenIdWithoutPWD(telPhone, userName, openid, WeixinConfig.crmPartner);
                LogHelper.Info("新用户绑定(" + userName + ")(" + telPhone + ")(" + openid + ")：CRM_WEB_RegisterBindOpenIdWithoutPWD 返回值：" + jsonStr);
                if (jsonStr.IndexOf("R000") >= 0)
                {
                    //获取用户信息
                    OAuthUserInfo userInfo = JsonConvert.DeserializeObject<OAuthUserInfo>(userinfo);
                    //OAuthUserInfo userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                    //添加到cookie中
                    Session[KeyConst.SessionUserHeadImageUrl] = userInfo.headimgurl;
                    Session[KeyConst.SessionUserNickName] = userInfo.nickname;

                    //在网站上注册登陆
                    AuthorizeUser.CreateLoginUserTicket(openid, userName);
                    var user = AuthorizeUser.GetAuthorizeUser(openid);
                    var docid = 0;

                    if (user != null)
                    {
                        var backUrl = WeixinConfig.webDomain;
                        if (Session[KeyConst.SessionBindUserBackUrl] != null)
                        {
                            backUrl = Session[KeyConst.SessionBindUserBackUrl].ToString();
                            // type =1 表示从我的关注跳转
                            if (backUrl.IndexOf("type=1") != -1)
                            {
                                docid = int.Parse(backUrl.Split('?')[1].Split('&')[0].Substring(3, backUrl.Split('?')[1].Split('&')[0].Length - 3));
                                CRSService.CRS_ServiceSoapClient clientCRS = new CRSService.CRS_ServiceSoapClient();
                                if (user != null)
                                {
                                    var returnModel = clientCRS.attentionDoctor(user.pid, docid, 1);
                                }
                            }
                            Session[KeyConst.SessionBindUserBackUrl] = null;
                        }
                        else
                        {
                            backUrl += "/User/UserCenter";
                        }

                        LogHelper.Info("UseBaseCallback-backURL: " + backUrl);
                        if (!string.IsNullOrWhiteSpace(backUrl))
                        {
                            if (backUrl.IndexOf('?') >= 0)
                            {
                                backUrl += "&openid=" + Base64Helper.EncodeBase64(openid);
                            }
                            else
                            {
                                backUrl += "?openid=" + Base64Helper.EncodeBase64(openid);
                            }
                            return Redirect(backUrl);
                        }
                        return RedirectToAction("UserCenter", "User");
                    }
                    else
                    {
                        return RedirectToAction("UserCenter", "User");
                    }
                }
                else
                {
                    return Content(jsonStr);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info("Error：" + ex.Message);
                return Content(ex.Message);
            }
        }
        #endregion

        #region IsLogin
        /// <summary>
        /// IsLogin
        /// </summary>
        /// <returns></returns>
        public JsonResult IsLogin()
        {
            if (User != null && User.Identity.IsAuthenticated)
            {
                return Json(new { islogin = true });
            }
            return Json(new { islogin = false });
        }
        #endregion
    }
}