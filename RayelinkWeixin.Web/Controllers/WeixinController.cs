﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：WeixinController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-12
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


using PaylinkWeixin.MP.CommonService.MessageHandlers;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.MP.MvcExtension;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
namespace RayelinkWeixin.Web.Controllers
{
    public class WeixinController : BaseController
    {
        #region --属性--

        public string Token = "weixin";

        public readonly string AppId = "";

        public readonly string EncodingAESKey = "";

        #endregion

        #region --公有方法--

        #region --查询--

        #endregion

        #region --增删改--

        #endregion

        #region --导出--

        #endregion

        #endregion

        #region --私有方法--

        #endregion

        /// <summary>
        /// 签名校验
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="timestamp"></param>
        /// <param name="nonce"></param>
        /// <param name="echostr"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Index")]
        [AllowAnonymous]
        public ActionResult Index(string signature, string timestamp, string nonce, string echostr)
        {
            List<string> contents = new List<string>() { };
            contents.Add("URL:" + Request.Url.AbsoluteUri);
            string _token = Token;
            short appid = -1;
            if (RouteData.Values.ContainsKey("id"))
            {
                appid = Convert.ToInt16(RouteData.Values["id"]);
            }
            if (appid > 0)
            {
                var appInfo = new ShareFunction(appid).App;
                if (null != appInfo)
                {
                    _token = appInfo.AppToken;
                }
            }
            FileLogHelper.Instance.Log(contents, "WxIndex");
            if (CheckSignature.Check(signature, timestamp, nonce, _token))
            {
                return Content(echostr); //返回随机字符串则表示验证通过
            }
            else
            {
                return Content("failed:" + signature + "," + CheckSignature.GetSignature(timestamp, nonce, Token));
            }
        }

        /// <summary>
        /// 接受mp参数
        /// </summary>
        /// <param name="postModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("Index")]
        [AllowAnonymous]
        public ActionResult Post(PostModel postModel)
        {
            short appid = -1;
            List<string> contents = new List<string>() { };        
            try
            {
                contents.Add("URL:" + Request.Url.AbsoluteUri);
                string _token = Token;
                #region 前置参数处理

                if (RouteData.Values.ContainsKey("id"))
                {
                    appid = Convert.ToInt16(RouteData.Values["id"]);
                    if (appid > 0)
                    {
                        var appInfo = new ShareFunction(appid).App;
                        if (null != appInfo)
                        {
                            _token = appInfo.AppToken;
                            postModel.Token = appInfo.AppToken;
                            postModel.EncodingAESKey = appInfo.AppEncodingAESKey;
                            postModel.AppId = appInfo.AppKey;
                        }
                    }
                }
              
                if (!CheckSignature.Check(postModel.Signature, postModel.Timestamp, postModel.Nonce, _token))
                {
                    return Content("参数错误！");
                }
                if (string.IsNullOrWhiteSpace(postModel.Token))
                {
                    postModel.Token = Token;
                    postModel.EncodingAESKey = WeixinConfig.EncodingAESKey;//根据自己后台的设置保持一致
                    postModel.AppId = WeixinConfig.appID;//根据自己后台的设置保持一致
                }
                #endregion

                var messageHandler = new DoctorMessageHandler(Request.InputStream, postModel);//接收消息

                contents.Add(messageHandler.RequestDocument.ToString());
                FileLogHelper.Instance.Log(contents, appid + "_WxRequest");

                messageHandler.Execute();//执行微信处理过程
                return new WeixinResult(messageHandler);//返回结果
            }
            catch (Exception ex)
            {
                FileLogHelper.Instance.Log(contents, appid + "_WxRequest");
                new ExceptionHelper().LogException(ex);
            }
            return null;
        }

    }
}
