﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：UserProfileController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-03-10
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Web.Filters;
using Rotativa;
using System.IO;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    //[WeixinSpeficAuthorize]
    public class UserProfileController : Controller
    {

        public ActionResult CreatePDF()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseID">病历ID</param>
        /// <param name="orgID">机构ID</param>
        /// <param name="type">类型 1 病历 2 检查报告 3 检验报告</param>
        /// <returns></returns>
        public JsonResult GeneratePDF(int caseID, int orgID, int type, int reportId, string reportType, int pictureId, string pictureType, int phonetype)
        {
            var path = WeixinDownloadHelper.GetSavePDFPath();
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            //TODO:要根据type调用不同的接口 现在都是一个
            var html = "";
            if (type == 1)
            {
                var retunModel = client.EMR_UserCase_GetHTML(caseID, orgID);
                html = retunModel.Data;
            }
            else if (type == 2)
            {
                var retunModel = client.EMR_ReportDetail_GetHTML(reportId, orgID, reportType);
                html = retunModel.Data;
            }
            else if (type == 3)
            {
                var retunModel = client.EMR_ReportDetail_GetHTML(reportId, orgID, reportType);
                html = retunModel.Data;
            }
            if (type != 4)
            {
                if (!string.IsNullOrWhiteSpace(html))
                {
                    //使用黑体会出现中文乱码
                    //phonetype 1 ios   2 android
                    if (phonetype == 1)
                    {
                        html = html.Replace("黑体", "宋体");
                        if(type !=1)
                        {
                            html = html.Replace("style=\"", "style=\"font-family: '楷体';");
                        }
                        if(type ==1)
                        {
                            html = html.Replace("gb2312", "utf-8");
                        }
                    }else
                    {
                        html = html.Replace("黑体", "宋体");
                        if (type != 1)
                        {
                            html = html.Replace("style=\"", "style=\"font-family: '楷体';");
                        }
                        if (type == 1)
                        {
                            html = html.Replace("gb2312", "utf-8");
                        }
                    }
                    
                }
                else
                {
                    return Json(new { success = false });
                }
                ViewBag.Html = html;
            }
            
            byte[] binary = null;
            var fileName = "";
            if (type == 4)
            {
                fileName = caseID.ToString() + orgID.ToString() + type.ToString() + pictureType.ToString() + ".png";
                var retunModel = client.EMR_PacsPictures_DownloadJpg(pictureId, orgID);
                binary = retunModel.Data;
            }
            else
            {
                fileName = caseID.ToString() + orgID.ToString() + type.ToString() +reportId.ToString() + ".pdf";
                binary = new ViewAsPdf("CreatePDF", html)
                {
                    FileName = fileName
                }.BuildPdf(this.ControllerContext);
            }
            using (FileStream fs = new FileStream(path + "/" + fileName, FileMode.Create))
            {
                fs.Write(binary, 0, binary.Length);
                fs.Flush();
                fs.Close();
            }
            var kbSize = binary.Length / 1024m;
            var fileSize = kbSize.ToString("0.##") + "KB";
            if (kbSize > 103)
            {
                var mSize = kbSize / 1024m;
                fileSize = mSize.ToString("0.##") + "M";
            }
            return Json(new { success = true, fileSize = fileSize });
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="caseID"></param>
        /// <param name="orgID"></param>
        /// <param name="type"></param>
        /// <param name="reportId"></param>
        /// <param name="reportType"></param>
        /// <param name="pictureId"></param>
        /// <param name="pictureType"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult DownLoadPDF(int caseID, int orgID, int type, int reportId, string reportType, int pictureId, string pictureType)
        {
            var fileName = "";
            // 影像图片
            if (type == 4)
            {
                fileName = caseID.ToString() + orgID.ToString() + type.ToString() + pictureType.ToString() + ".png";
            }
            else
            {
                // 病例报告
                fileName = caseID.ToString() + orgID.ToString() + type.ToString() + reportId.ToString() + ".pdf";
            }

            var filePath = Path.Combine(WeixinDownloadHelper.GetSavePDFPath(), fileName);
            var fileResult = new FilePathResult(filePath, "application/octet-stream");
            fileResult.FileDownloadName = Server.UrlEncode(fileName);
            return fileResult;
        }
    }
}
