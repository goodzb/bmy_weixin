﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：MenuController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-13
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP.Containers;
using Senparc.Weixin.MP.Entities.Menu;
using System;
using System.Reflection;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    //[AllowAnonymous]
    public class MenuController : BaseController
    {
        #region --属性--

        #endregion

        #region --公有方法--

        #region --查询--

        #endregion

        #region --增删改--

        #endregion

        #region --导出--

        #endregion

        #endregion

        #region --私有方法--

        #endregion

        //
        // GET: /Menu/
        [AllowAnonymous]
        [ActionName("CreateMenu")]
        public ActionResult CreateMenu()
        {
            ButtonGroup bg = new ButtonGroup();
            //帮忙医门诊
            var subButton = new SubButton()
            {
                name = "远程医疗"
            };
            subButton.sub_button.Add(new SingleViewButton()
            {
                //url = WeixinConfig.webDomain + "/html/fuwu.html",
                url="http://ntweixin.bmyi.cn/html/expert-app.html",
                name = "远程介绍"
            });
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = WeixinConfig.webDomain + "/html/jigoulist.html",
                name = "服务站点"
            });
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = "https://h5.youzan.com/v2/feature/tf0kdsyq",
                name = "专家预约"
            });
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = "https://h5.youzan.com/v2/feature/18amgrw4j",
                name = "复旦儿科"
            });
            subButton.sub_button.Add(new SingleClickButton()
            {
                name = "预约电话",
                key = "OneClick",
                type = ButtonType.click.ToString()
            });
            bg.button.Add(subButton);
            subButton = new SubButton()
            {
                name = "专家讲堂"
            };
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = "http://weixin.bmyi.cn/html/doctor-forum.html",
                name = "名医直播"
            });
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = "http://mp.weixin.qq.com/mp/homepage?__biz=MzAwMDU5NTkxMw==&hid=2&sn=b48079009501e9a56e25e1930d4a1172#wechat_redirect",
                name = "缪晓辉论健"
            });
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = "http://mp.weixin.qq.com/mp/homepage?__biz=MzAwMDU5NTkxMw==&hid=3&sn=a6a9f0ea08d6846b77420cbbc87bf27c#wechat_redirect",
                name = "冬雷说脑"
            });
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = "http://mp.weixin.qq.com/mp/homepage?__biz=MzAwMDU5NTkxMw==&hid=1&sn=613d7582c7a0fed995662c8025bd86a4#wechat_redirect",
                name = "更多推荐"
            });
            bg.button.Add(subButton);
            subButton = new SubButton()
            {
                name = "自助服务"
            };
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = WeixinConfig.webDomain + @"/User/UserCenter",
                name = "个人中心"
            });
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = WeixinConfig.webDomain + "/referral/myreferral",
                name = "推荐患者"
            });
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = "http://a.app.qq.com/o/simple.jsp?pkgname=com.ytdinfo.keephealth",
                name = "下载APP"
            });
            //subButton.sub_button.Add(new SingleViewButton()
            //{
            //    url = WeixinConfig.webDomain + @"/NewFreeDiagnosis/Index",
            //    name = "51义诊"
            //});
            subButton.sub_button.Add(new SingleViewButton()
            {
                url = WeixinConfig.webDomain + @"/ReVisit/ReVisit/Interrogate",
                name = "图文复诊"
            });


            bg.button.Add(subButton);
            //subButton.sub_button.Add(new SingleViewButton()
            //{
            //    url = WeixinConfig.webDomain,
            //    name = "微官网"
            //});
            //subButton.sub_button.Add(new SingleViewButton()
            //{
            //    url = WeixinConfig.webDomain + "/html/fuwu.html",
            //    name = "项目介绍"
            //});
            //subButton.sub_button.Add(new SingleViewButton()
            //{
            //    url = WeixinConfig.webDomain + "/html/shebei.html",
            //    name = "专业设备"
            //});
            //subButton.sub_button.Add(new SingleViewButton()
            //{
            //    url = WeixinConfig.webDomain + "/Home/NewsList",
            //    name = "最新动态"
            //});
            //subButton.sub_button.Add(new SingleViewButton()
            //{
            //    url = "http://form.mikecrm.com/qB9DMf",
            //    name = "名医诊疗卡"
            //});
            //bg.button.Add(subButton);
            //subButton = new SubButton()
            //{
            //    name = "我要预约"
            //};
            //subButton.sub_button.Add(new SingleViewButton()
            //{
            //    url = WeixinConfig.webDomain + @"/Doctor/DoctorList",
            //    name = "线上预约"
            //});
            //subButton.sub_button.Add(new SingleClickButton()
            //{
            //    name = "电话预约",
            //    key = "OneClick",
            //    type = ButtonType.click.ToString(),//默认已经设为此类型，这里只作为演示
            //});
            //subButton.sub_button.Add(new SingleViewButton()
            //{
            //    url = WeixinConfig.webDomain + "/Order/OrderList",
            //    name = "专家排班表"
            //});
            //subButton.sub_button.Add(new SingleViewButton()
            //{
            //    url = WeixinConfig.webDomain + "/html/jigouList.html",
            //    name = "远程视频点"
            //});
            //bg.button.Add(subButton);
            //bg.button.Add(new SingleViewButton()
            //{
            //    url = WeixinConfig.webDomain + @"/User/UserCenter",
            //    name = "我的帮忙医"
            //});
            var _appId = WeixinConfig.appID;
            var _appSecret = WeixinConfig.appSecret;
            var accessToken = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret);
            var result = CommonApi.CreateMenu(accessToken, bg);
            return Json(result);

        }



        /// <summary>
        /// 发布微信菜单
        /// </summary>
        /// <param name="appid"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult PushMeun(short appid)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                var appInfo = new ShareFunction(appid).App;
                if (null != appInfo)
                {
                    ButtonGroup bg = new ButtonGroup();
                    var subButton = new SubButton()
                    {
                        name = "自助服务"
                    };
                    subButton.sub_button.Add(new SingleViewButton()
                    {
                        url = WeixinConfig.webDomain + @"/ReVisit/ReVisit/Interrogate",
                        name = "图文复诊"
                    });
                    bg.button.Add(subButton);
                    var _appId = appInfo.AppKey;
                    var _appSecret = appInfo.AppSecret;
                    var accessToken = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret);
                    var result = CommonApi.CreateMenu(accessToken, bg);
                    rm.ResultData["Result"] = result;
                    if (result.errmsg== "ok")
                    {
                        rm.IsSuccess = true;
                        rm.Text = "菜单发布成功~";
                    }
                }
                else
                {
                    rm.Text = "未找到相应的app信息，请核实~";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteMenu()
        {
            var _appId = WeixinConfig.appID;
            var _appSecret = WeixinConfig.appSecret;
            var accessToken = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret);
            var result = CommonApi.DeleteMenu(accessToken);
            return Json(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene_id"></param>
        /// <returns></returns>
        public ActionResult GetQCode(string scene_id)
        {
            //string scene_id = "1";//场景Id，扫描生产的二维码 会带这个id，可以做推广、统计用

            //二维码图片存储路径
            string path = Server.MapPath("~/Document/QrCode/" + scene_id + ".jpg");
            //直接保存二维码到本地
            SaveQrCodeImage(scene_id, path);

            ViewBag.Path = Url.Content("~/Document/QrCode/" + scene_id + ".jpg");
            return View();
        }
        /// <summary>
        /// 根据SceneId 获取 二维码图片流
        /// </summary>
        /// <param name="scene_id"></param>
        /// <returns></returns>
        public static System.IO.Stream GetQrCodeImageStream(string scene_id)
        {
            var _appId = WeixinConfig.appID;
            var _appSecret = WeixinConfig.appSecret;
            var accessToken = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret);
            RayelinkWeixin.Common.Utilites.LogHelper.Info("accessToken=" + accessToken);
            string Ticket = RayelinkWeixin.Common.Utilites.WeixinQcodeHelper.CreateTicket(accessToken, scene_id);

            if (Ticket == null)
                throw new System.ArgumentNullException("Ticket");

            //ticket需 urlEncode
            RayelinkWeixin.Common.Utilites.LogHelper.Info("Ticket=" + Ticket);
            string strUrl = string.Format("https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}", System.Web.HttpUtility.UrlEncode(Ticket));
            RayelinkWeixin.Common.Utilites.LogHelper.Info("url=" + strUrl);
            try
            {
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                System.Byte[] bytes = client.GetByteArrayAsync(strUrl).Result;
                return new System.IO.MemoryStream(bytes);
            }
            catch
            {
                throw new System.Exception("获取二维码图片失败！");
            }
        }
        /// <summary>
        /// 根据Ticket获取二维码图片保存在本地
        /// </summary>
        /// <param name="scene_id">二维码场景id</param>
        /// <param name="imgPath">图片存储路径</param>
        public static void SaveQrCodeImage(string scene_id, string imgPath)
        {
            var _appId = WeixinConfig.appID;
            var _appSecret = WeixinConfig.appSecret;
            var accessToken = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret);
            string Ticket = RayelinkWeixin.Common.Utilites.WeixinQcodeHelper.CreateTicket(accessToken, scene_id);

            if (Ticket == null)
                throw new System.ArgumentNullException("Ticket");

            //ticket需 urlEncode
            string stUrl = string.Format("https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}", System.Web.HttpUtility.UrlEncode(Ticket));

            System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(stUrl);

            req.Method = "GET";

            using (System.Net.WebResponse wr = req.GetResponse())
            {
                System.Net.HttpWebResponse myResponse = (System.Net.HttpWebResponse)req.GetResponse();
                string strpath = myResponse.ResponseUri.ToString();

                System.Net.WebClient mywebclient = new System.Net.WebClient();

                try
                {
                    mywebclient.DownloadFile(strpath, imgPath);
                }
                catch (System.Exception)
                {
                    throw new System.Exception("获取二维码图片失败！");
                }
            }
        }
    }
}
