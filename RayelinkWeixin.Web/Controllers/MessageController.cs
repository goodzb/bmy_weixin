﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：MessageController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-19
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Model;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    [AllowAnonymous]
    public class MessageController : BaseController
    {
        #region 发送手机获取验证码
        /// <summary>
        /// 发送手机获取验证码
        /// </summary>
        /// <param name="mobile">接收手机号</param>
        /// <param name="type">区分这次验证码的活动标识</param>
        /// <returns></returns>
        public JsonResult SendMessageValidCode(string mobile, string type = "mobile")
        {
            if (!validateUserFotCapcha(mobile, HttpContext.Request.UserHostAddress))
            {
                LogHelper.Info("/***********拒绝短信**********/ \r\n 手机号:" + mobile + "\r\n SessionID:" + Session.SessionID + "\r\n  IP:" + HttpContext.Request.UserHostAddress + "\r\n/***********End**********/");
                return Json(new { success = false, message = "您的请求过于频繁，请改日再来。" });
            }

            string cacheKey = string.Format(ValidCodeCacheKey, mobile, type);
            SMS_Service.WebServiceClient client = new SMS_Service.WebServiceClient();
            var code = CookieHelper.GetValueByCookieskey(Request, cacheKey);  //先从缓存里面取出没有过期的验证码
            if (string.IsNullOrWhiteSpace(code))
            {
                code = RadomHelper.GetRandomString(6);
            }
            string content = "验证码：" + code + "，请在10分钟内完成验证。如非本人操作，请忽略。";
            var jsonStr = client.sendMsg(mobile, content, "BMY", "CMCC_BMY");
            var obj = JsonConvert.DeserializeObject<CRMMessageResult>(jsonStr);
            var message = "验证码已发送，请注意查收";
            if (obj.success)
            {
                CookieHelper.AddCookies(Response, cacheKey, code, 10);
            }
            else
            {
                message = obj.info;
            }
            Session[cacheKey] = jsonStr;
            return Json(new { success = obj.success, message = message });
        }

        /// <summary>
        /// 验证验证码是否正确
        /// </summary>
        /// <param name="mobile">接收手机号</param>
        /// <param name="code">需要校验的验证码</param>
        /// <param name="type">区分这次验证码的活动标识</param>
        /// <param name="expendCode">是否消耗掉这次的验证码(1:是，从缓存里面清理掉)</param>
        /// <returns></returns>
        public JsonResult ValidMessageCode(string mobile, string code, string type = "mobile", byte expendCode = 0)
        {
            Common.Share.ReturnMessage rm = ValidCode(mobile, code, type, expendCode.Equals(1));
            return Json(new { success = rm.IsSuccess, message = rm.Text });
        } 
        #endregion

    }
}
