﻿/*----------------------------------------------------------------
// Copyright (C) 2017瑞一互联
// 版权所有。
// 文   件   名：FreeDiagnosisController
// 文件功能描述：义诊意向单
//
// 
// 创 建 人：liuwq
// 创建日期：2017-02-13
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using RayelinkWeixin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers.FreeDiagnosis
{
    public class SenEmailJsonResult
    {
        public bool success { get; set; }
        public string info { get; set; }
    } 
    [AllowAnonymous]
    public class FreeDiagnosisController : Controller
    {
        //
        // GET: /FreeDiagnosis/
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 预约页
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [Authorize]
        public ActionResult Order(OrderModel model)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            //var objDoctor = CacheHelper.GetCache(KeyConst.CacheDoctor + model.docID);
            //var objOrganization = CacheHelper.GetCache(KeyConst.CacheOrganization + model.hospitalID);
            //// 获得医生诊所信息
            //DoctorModel doctorModel = new DoctorModel();
            //HospitalModel hospitalModel = new HospitalModel();
            //if (objDoctor != null)
            //{
            //    doctorModel = JsonConvert.DeserializeObject<DoctorModel>(objDoctor.ToString()); ;
            //}
            //else
            //{
            //    var returnModel = client.getDoctor(model.docID);
            //    var doctorJson = returnModel.Data.ToString();
            //    if (!String.IsNullOrEmpty(doctorJson))
            //    {
            //        CacheHelper.SetCache(KeyConst.CacheDoctor + model.docID, doctorJson);
            //    }
            //    doctorModel = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
            //}
            //if (objOrganization != null)
            //{
            //    hospitalModel = JsonConvert.DeserializeObject<HospitalModel>(objOrganization.ToString());
            //}
            //else
            //{
            //    var returnModel1 = client.getOrganization(model.hospitalID);
            //    var organizationJson = returnModel1.Data.ToString();
            //    hospitalModel = JsonConvert.DeserializeObject<HospitalModel>(organizationJson);
            //    if (!String.IsNullOrEmpty(organizationJson))
            //    {
            //        CacheHelper.SetCache(KeyConst.CacheOrganization + model.hospitalID, organizationJson);
            //    }
            //}

            var pid = 0;
            var name = "";
            var phone = "";
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);

                if (user != null)
                {
                    pid = user.pid;
                    name = user.username;
                    phone = user.telphone;
                }
            }
            // 判断是否选择就诊人
            if (model.userId > 0)
            {
                // 判断是否选择当前绑定账户
                if (model.userId == pid)
                {
                    CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
                    var returnUserinfo = crmClient.CRM_WEB_GetMemberModelByPid(pid);
                    if (returnUserinfo != null)
                    {

                        model.name = returnUserinfo.Name;
                        model.phone = returnUserinfo.Mobile;
                        if (returnUserinfo.MemberCertList != null)
                        {
                            var cardInfo = returnUserinfo.MemberCertList.Where(p => p.CertTypeCode == "CID").OrderByDescending(p => p.UpdateTime).FirstOrDefault(); // 身份证号 
                            if (cardInfo != null)
                            {
                                model.id = cardInfo.CertNo;
                            }
                        }
                    }
                    // 判断当前绑定账号是否是默认就诊人
                    model.famliyId = pid;
                    //model.nowUserId = model.userId;
                    var returnModelList = client.FamilyMember_GetList(pid);
                    var familyMemberA = returnModelList.Data;
                    if (familyMemberA != null)
                    {
                        var familyMemberList = familyMemberA.ToList().Where(p => p.IsDefault == true).FirstOrDefault();
                        if (familyMemberList != null)
                        {
                            model.IsDefault = false;
                        }
                        else
                        {
                            model.IsDefault = true;
                        }
                    }
                }
                else
                {
                    var returnModel4 = client.FamilyMember_Get(model.userId);
                    var familyMember = returnModel4.Data;
                    if (familyMember != null)
                    {
                        UserModel familyMemberModel = new UserModel()
                        {
                            FamilyName = familyMember.FamilyName,
                            FamilyMobile = familyMember.FamilyMobile,
                            IDCardNo = familyMember.IDCardNo,
                            Gender = familyMember.Gender,
                            Birthday = familyMember.Birthday,
                            IsDefault = familyMember.IsDefault
                        };
                        model.name = familyMemberModel.FamilyName;
                        model.phone = familyMemberModel.FamilyMobile;
                        model.id = familyMemberModel.IDCardNo;
                        model.sex = familyMemberModel.Gender;
                        model.Birthday = familyMemberModel.Birthday;
                        model.famliyId = model.famliyId;
                        model.nowUserId = model.userId;
                        model.IsDefault = familyMemberModel.IsDefault;
                    }
                }


            }
            else
            {
                var returnModel = client.FamilyMember_GetList(pid);
                var familyMemberArry = returnModel.Data;
                var familyMemberModelList = familyMemberArry.FirstOrDefault(p => p.IsDefault == true);
                // 获取就诊人列表中默认就诊人 若无 则取当前绑定用户
                if (familyMemberModelList != null)
                {
                    model.name = familyMemberModelList.FamilyName;
                    model.phone = familyMemberModelList.FamilyMobile;
                    model.id = familyMemberModelList.IDCardNo;
                    model.sex = familyMemberModelList.Gender;
                    model.Birthday = familyMemberModelList.Birthday;
                    model.famliyId = familyMemberModelList.FamilyPID == null ? 0 : familyMemberModelList.FamilyPID.Value;
                    model.nowUserId = familyMemberModelList.Id;
                    model.IsDefault = familyMemberModelList.IsDefault;
                }
                else
                {
                    CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
                    var returnUserinfo = crmClient.CRM_WEB_GetMemberModelByPid(pid);
                    if (returnUserinfo != null)
                    {
                        model.name = returnUserinfo.Name;
                        model.phone = returnUserinfo.Mobile;
                        if (returnUserinfo.MemberCertList != null)
                        {
                            var cardInfo = returnUserinfo.MemberCertList.Where(p => p.CertTypeCode == "CID").OrderByDescending(p => p.UpdateTime).FirstOrDefault(); // 身份证号 
                            if (cardInfo != null)
                            {
                                model.id = cardInfo.CertNo;
                            }
                        }
                    }
                    model.famliyId = pid;
                    //model.nowUserId = pid;
                    var returnModelList = client.FamilyMember_GetList(pid);
                    var familyMemberA = returnModelList.Data;
                    if (familyMemberA != null)
                    {
                        var familyMemberList = familyMemberA.ToList().Where(p => p.IsDefault == true).FirstOrDefault();
                        if (familyMemberList != null)
                        {
                            model.IsDefault = false;
                        }
                        else
                        {
                            model.IsDefault = true;
                        }
                    }

                }
            }

            // 获得诊费
            var returnModel3 = client.GetClinicCategory(model.clinicinfoID);
            var clinicCategoryJson = returnModel3.Data.ToString();
            var clinicCategoryModel = JsonConvert.DeserializeObject<ClinicCategoryModel>(clinicCategoryJson);


            model.pid = pid;
            //model.docName = doctorModel.name;
            //model.docImage = doctorModel.image;
            //model.deptName = doctorModel.depName2;
            //model.titleName = doctorModel.titleName;
            model.formatDate = Convert.ToDateTime(model.date).ToString("yyyy年MM月dd日");
            if (String.IsNullOrEmpty(model.time) || (model.time != null && String.IsNullOrEmpty(model.time.Trim())))
            {
                if ("am".Equals(model.target))
                {
                    model.formatTime = "上午";
                }
                else if ("pm".Equals(model.target))
                {
                    model.formatTime = "下午";
                }
                else if ("night".Equals(model.target))
                {
                    model.formatTime = "晚上";
                }
            }
            else
            {
                model.formatTime = model.time;
            }
            //model.hospitalName = hospitalModel.hospitalName;
            //model.address = hospitalModel.hospitalAddress;
            if (clinicCategoryModel != null)
            {
                if ("1".Equals(model.onOffLine))
                {
                    model.fee = clinicCategoryModel.remoteregistrationFee;
                }
                else
                {
                    model.fee = clinicCategoryModel.registrationFee;
                }

            }
            var url = Request.Url.ToString().Split('#').Length > 0 ? Request.Url.ToString().Split('#')[0].Replace(":" + WeixinConfig.port, "") : Request.Url.ToString().Replace(":" + WeixinConfig.port, "");
            Session[KeyConst.SessionOrderUrl] = Base64Helper.EncodeBase64(url);
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View(model);
        }

        [WeixinSpeficAuthorize]
        ///<summary>
        /// 添加预约
        ///</summary>
        public ActionResult ResvServerAdd(string resvDate, string hospitalName, string sectionName, string userName, string phone, string id, string description, string pictures, string pid, string openId)
        {
            string strPic = "";
            try
            {
                int str = int.Parse(id.Substring(id.Length - 2, 1));
                string sex = "";
                if (str % 2 == 0)
                {
                    sex = "女";
                }
                else
                {
                    sex = "男";
                }

                if (pictures != "")
                {
                    string[] arrlist = pictures.Split(',');
                    for (int i = 0; i < arrlist.Length; i++)
                    {
                        if (arrlist[i] != "")
                        {
                            strPic += "图片" + (i + 1).ToString() + ": " + arrlist[i] + " \r\n";
                        }
                    }

                }


                string contentHtml = string.Format("联系人姓名:{0}\r\n性别:{1}\r\n联系人电话:{2}\r\n身份证号:{3}\r\n就诊日期:{5}\r\n就诊医院:{4}\r\n症状:{6}\r\n{7}\r\n", userName, sex, phone, id, hospitalName, resvDate, description, strPic);
                string toAccounts = System.Web.Configuration.WebConfigurationManager.AppSettings["freeDiagnosisEmailAccounts"];
                SMS_Service.WebServiceClient client = new SMS_Service.WebServiceClient();
                string[] accountlist = toAccounts.Split(',');
                string result = string.Empty;
                bool sendResult = false;

                foreach (string item in accountlist)
                {
                    result = client.sendMail(item, "就医服务-义诊意向单", contentHtml);
                    SenEmailJsonResult sendResultModel = JsonConvert.DeserializeObject<SenEmailJsonResult>(result);
                    if (sendResultModel.success)
                    {
                        sendResult = true;
                    }
                }
                if (sendResult)
                {
                    return Json(new { success = true, data = "" });
                }
                else
                {
                    LogHelper.Error("下单信息及错误内容：\r\n预约项目：就医服务-义诊意向单\r\n openid:" + openId + "\r\n pid:" + pid + "\r\n 联系人姓名：" + userName + "\r\n联系人电话：" + phone + "\r\n身份证号:" + id + "\r\n就诊日期:" + resvDate + "\r\n就诊医院:" + hospitalName + "\r\n症状:" + description + "\r\n" + strPic + "\r\n代码错误信息：发送邮件失败");
                    return Json(new { success = false, data = "发送邮件失败" });
                }


            }
            catch (Exception ex)
            {

                LogHelper.Error("下单信息及错误内容：\r\n预约项目：就医服务-义诊意向单\r\n openid:" + openId + "\r\n  pid:" + pid + "\r\n 联系人姓名：" + userName + "\r\n联系人电话：" + phone + "\r\n身份证号:" + id + "\r\n就诊日期:" + resvDate + "\r\n就诊医院:" + hospitalName + "\r\n症状:" + description + "\r\n" + strPic + "\r\n代码错误信息：" + ex.ToString());
                return Json(new { success = false, data = "接口错误" });
            }

        }
    }
}
