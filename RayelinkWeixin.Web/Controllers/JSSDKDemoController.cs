﻿using RayelinkWeixin.Common.Share;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Web.Filters;
using RayelinkWeixin.Web.Models;
using Senparc.Weixin.MP.CommonAPIs;
using System;
using System.Diagnostics;
using System.IO;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class JSSDKDemoController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult Index()
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View();
        }

        /// <summary>
        /// 将语音文件存放在本地
        /// </summary>
        /// <param name="media_id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownloadVoice(string media_id)
        {
            return base.DownloadMedia(media_id, 1);
        }

        /// <summary>
        /// 执行cmd
        /// </summary>
        /// <param name="dosCommand"></param>
        /// <param name="milliseconds"></param>
        /// <returns></returns>
        public static string ExecuteCommand(string dosCommand, int milliseconds)
        {
            string output = "";     //输出字符串
            if (dosCommand != null && dosCommand != "")
            {
                Process process = new Process();     //创建进程对象
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "cmd.exe";      //设定需要执行的命令
                startInfo.UseShellExecute = false;   //不使用系统外壳程序启动
                startInfo.RedirectStandardInput = true;   //不重定向输入
                startInfo.RedirectStandardOutput = true;   //重定向输出
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;     //不创建窗口
                process.StartInfo = startInfo;

                try
                {
                    if (process.Start())       //开始进程
                    {
                        process.StandardInput.WriteLine(dosCommand);

                        if (milliseconds == 0)
                        {
                            process.WaitForExit();     //这里无限等待进程结束
                        }
                        else
                        {
                            process.WaitForExit(milliseconds);
                            if (process != null && !process.HasExited)
                            {
                                process.Close();
                            }

                        }
                    }
                }
                catch (Exception)
                {
                    if (process != null && !process.HasExited)
                    {
                        process.Kill();
                        process.Close();
                    }
                }
                finally
                {

                }
            }
            return output;
        }
    }
}
