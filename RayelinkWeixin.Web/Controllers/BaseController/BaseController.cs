﻿using Newtonsoft.Json;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    ///作者：xiao99
    ///日期：2017/04/28 10:27:16
    ///说明：Controller基类
    ///版本号：  V1.0.0.0
    /// </summary>
	public partial class BaseController : Controller
    {
        #region 变量
        /// <summary>
        /// 一般默认的查询条数 
        /// </summary>
        protected const int BasePageSize = 99999;

        /// <summary>
        /// json序列化时的参数
        /// </summary>
        protected readonly JsonSerializerSettings JsonDataSerializerSettings = new JsonSerializerSettings()
        {          
            NullValueHandling = NullValueHandling.Ignore,  //Json序列化时去掉空的
            DateFormatHandling = DateFormatHandling.IsoDateFormat
        };

        /// <summary>
        /// 不需要记录LogUserAction的配置
        /// </summary>
        string[] ExcludeAction = new string[] { "Weixin/Index" };
        #endregion

        #region 公共方法 短信频繁发送目标手机号、IP拦截

        public bool validateUserFotCapcha(string mobile, string ip)
        {
            if (new[] { "121.57.59.34", "1.25.59.92" , "121.57.60.48", "113.122.212.9", "1.25.59.171" }.Contains(ip))
            {
                //待加入黑名单功能
                return false;
            }
            var cacheKeyPrefix = "capcha_";
            var numMobile = CacheHelper.GetCache<int>(cacheKeyPrefix + mobile);
            if (numMobile >= 5)
            {
                return false;
            }
            CacheHelper.SetCache(cacheKeyPrefix + mobile, numMobile + 1, new TimeSpan(24, 0, 0));

            /*******每次SessionID都会发生变化，故作废*********
            var numSession = Session[cacheKeyPrefix + mobile].ToInt32() ?? 0;
            if (numSession >= 5)
            {
                return false;
            }
            CacheHelper.SetCache(cacheKeyPrefix + mobile, numSession + 1, new TimeSpan(1, 0, 0));
            */

            var numIP = CacheHelper.GetCache<int>(cacheKeyPrefix + ip);
            if (numIP >= 20)
            {
                return false;
            }
            CacheHelper.SetCache(cacheKeyPrefix + ip, numIP + 1, new TimeSpan(6, 0, 0));
            return true;
        }
        #endregion

        #region MVCAction处理
        /// <summary>
        /// 在行为方法执行前执行
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string openid = string.Empty;
            int appid = 1;
            string actionName = filterContext.RouteData.Values["action"].ToString();
            string ControllerName = filterContext.RouteData.Values["controller"].ToString();
            if (filterContext.HttpContext.Request.QueryString.AllKeys.Contains("openid"))
            {
                openid = filterContext.HttpContext.Request["openid"];
            }
            if (filterContext.HttpContext.Request.QueryString.AllKeys.Contains("appid"))
            {
                appid = Convert.ToInt32(filterContext.HttpContext.Request["appid"]);
            }
            string newActionName = ControllerName + "/" + actionName;
            if (!ExcludeAction.Contains(newActionName))
            {
                LogUserAction(appid, openid, ControllerName + "/" + actionName);
            }         
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// 在行为方法返回后执行
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }
        #endregion

        #region 返回JsonResult

        /// <summary>
        /// 返回JsonResult
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="contentType">内容类型</param>
        /// <param name="contentEncoding">内容编码</param>
        /// <param name="behavior">行为</param>
        /// <returns>JsonReuslt</returns>
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new MyJsonResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        /// <summary>
        /// 返回JsonResult        
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="behavior">行为</param>
        /// <param name="format">json中dateTime类型的格式</param>
        /// <returns>Json</returns>
        protected JsonResult MyJson(object data, JsonRequestBehavior behavior, string format = "yyyy-MM-dd HH:mm:ss")
        {
            return new MyJsonResult
            {
                Data = data,
                JsonRequestBehavior = behavior,
                FormateStr = format
            };
        }

        /// <summary>
        /// 返回JsonResult     
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="format">数据格式</param>
        /// <returns>Json</returns>
        protected JsonResult MyJson(object data, string format = "yyyy-MM-dd HH:mm:ss")
        {
            try
            {
                return new MyJsonResult { Data = data, FormateStr = format };
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
            return null;
        }

        /// <summary>
        /// JSON2
        /// </summary>
        protected JsonResult MyJsonNet(object obj)
        {
            return new MyJsonResult2() { Data = obj };
        }
        #endregion

        #region 用行为记录

        /// <summary>
        /// 用行为记录
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UserActionLogs(UserActionEventLogInfo mode)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                if (UserActionEventLog(mode))
                {
                    rm.IsSuccess = true;
                    rm.Text = "成功";
                }
                else
                {
                    rm.IsSuccess = false;
                    rm.Text = "失败";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Message = ex.Message;
                rm.Text = "操作出错";
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, "yyyy-MM-dd");
        }

        /// <summary>
        /// 记录用户行为事件
        /// </summary>
        /// <param name="appId">AppId</param>
        /// <param name="openId">OpenId</param>
        /// <param name="actionName">行为名称（如页面名称）</param>
        /// <param name="actionType">行为类型（View/Click/Event/Share)</param>
        /// <returns>返回结果</returns>
        protected bool LogUserAction(int appId, string openId, string actionName, string actionType = "View")
        {
            return UserActionEventLogBusiness.Instance.Push(new UserActionEventLogInfo
            {
                AppId = appId,
                OpenId = openId,
                ActionName = actionName,
                ActionType = actionType
            });
        }

        /// <summary>
        /// 记录用户行为事件
        /// </summary>
        /// <param name="model">(需要填写：AppId：当前微信标识，OpenId：操作者，ActionType：事件类型，从类UserActionEventActionType中的属性值选择，
        /// ActionName：事件名称，parameter：可选，自定义扩展参数，ActionDesc：可选，备注信息)</param>
        /// <returns></returns>
        protected bool UserActionEventLog(UserActionEventLogInfo model)
        {
            try
            {
                bool result = UserActionEventLogBusiness.Instance.Push(model);
                return result;
            }
            catch (Exception ex)
            {
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return false;
        }
        #endregion

        #region 是否本地运行

        /// <summary>
        /// 是否本地运行
        /// </summary>
        /// <returns></returns>
        protected bool IsLocal()
        {
            bool result = false;
            string host = this.Request.Url.Host;
            if (host.Equals("localhost", StringComparison.CurrentCultureIgnoreCase))
            {
                result = true;
            }
            return result;
        }
        #endregion

        #region 清空App配置信息
        /// <summary>
        /// 清空App配置信息
        /// </summary>
        /// <param name="appid"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult RefreshAppConfig(int appid = 1)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作完成" };
            try
            {
                LangAdapter.ClearLangDict(appid);
                rm.IsSuccess = true;
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        } 
        #endregion
    }

    /// <summary>
    /// 自定义Json视图
    /// </summary>
    public class MyJsonResult : JsonResult
    {
        /// <summary>
        /// 格式化字符串
        /// </summary>
        public string FormateStr
        {
            get;
            set;
        }

        /// <summary>
        /// 重写执行视图
        /// </summary>
        /// <param name="context">上下文</param>
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;

            if (!string.IsNullOrEmpty(this.ContentType))
            {
                response.ContentType = this.ContentType;
            }
            else
            {
                response.ContentType = "application/json";
            }

            if (this.ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (this.Data != null)
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                string jsonString = jss.Serialize(Data);
                string p = @"\\/Date\(([-]*\d+)\)\\/";
                MatchEvaluator matchEvaluator = new MatchEvaluator(this.ConvertJsonDateToDateString);
                Regex reg = new Regex(p);
                jsonString = reg.Replace(jsonString, matchEvaluator);

                response.Write(jsonString);
            }
        }

        /// <summary>  
        /// 将Json序列化的时间由/Date(1294499956278)转为字符串 .
        /// </summary>  
        /// <param name="m">正则匹配</param>
        /// <returns>格式化后的字符串</returns>
        private string ConvertJsonDateToDateString(Match m)
        {
            string result = string.Empty;
            DateTime dt = new DateTime(1970, 1, 1);
            dt = dt.AddMilliseconds(long.Parse(m.Groups[1].Value));
            dt = dt.ToLocalTime();
            result = dt.ToString(FormateStr);
            return result;
        }
    }

    /// <summary>
    /// 自定义Json视图
    /// </summary>
    public class MyJsonResult2 : JsonResult
    {
        /// <summary>
        /// 格式化字符串
        /// </summary>
        public string FormateStr
        {
            get;
            set;
        }

        /// <summary>
        /// 重写执行视图
        /// </summary>
        /// <param name="context">上下文</param>
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;

            if (!string.IsNullOrEmpty(this.ContentType))
            {
                response.ContentType = this.ContentType;
            }
            else
            {
                response.ContentType = "application/json";
            }

            if (this.ContentEncoding != null)
            {
                response.ContentEncoding = this.ContentEncoding;
            }

            if (this.Data != null)
            {
                response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(this.Data));
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class XmlResult : ActionResult
    {
        private object objectToSerialize;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlResult"/> class.
        /// </summary>
        /// <param name="objectToSerialize">The object to serialize to XML.</param>
        public XmlResult(object objectToSerialize)
        {
            this.objectToSerialize = objectToSerialize;
        }

        /// <summary>
        /// Gets the object to be serialized to XML.
        /// </summary>
        public object ObjectToSerialize
        {
            get { return this.objectToSerialize; }
        }

        /// <summary>
        /// Serialises the object that was passed into the constructor to XML and writes the corresponding XML to the result stream.
        /// </summary>
        /// <param name="context">The controller context for the current request.</param>
        public override void ExecuteResult(ControllerContext context)
        {
            if (this.objectToSerialize != null)
            {
                context.HttpContext.Response.Clear();
                var xs = new System.Xml.Serialization.XmlSerializer(this.objectToSerialize.GetType());
                context.HttpContext.Response.ContentType = "text/xml";
                xs.Serialize(context.HttpContext.Response.Output, this.objectToSerialize);
            }
        }
    }
}