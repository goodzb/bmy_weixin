﻿using Newtonsoft.Json;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Share;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using Senparc.Weixin.MP.Containers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// Controller公用函数
    /// </summary>
    public partial class BaseController
    {
        #region 变量
        protected const string ValidCodeCacheKey = "{0}_ValidCode_{1}";
        protected const string CityListCacheKey = "HZ_CityList";
        /// <summary>
        /// 根据当前的请求获取网站的根路径
        /// </summary>
        protected readonly string BaseUrl = TextHelper.BaseUrl();

        protected const string WechatViewPath = "~/Views/WeChat/{0}.cshtml";
        #endregion

        #region 验证验证码是否正确

        /// <summary>
        /// 验证验证码是否正确
        /// </summary>
        /// <param name="mobile">接收手机号</param>
        /// <param name="code">需要校验的验证码</param>
        /// <param name="type">区分这次验证码的活动标识</param>
        /// <param name="expendCode">是否消耗掉这次的验证码(true:是，从缓存里面清理掉)</param>
        /// <returns></returns>
        protected Common.Share.ReturnMessage ValidCode(string mobile, string code, string type = "mobile", bool expendCode = false)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage();
            try
            {
                string cacheKey = string.Format(ValidCodeCacheKey, mobile, type);
                var cookieCode = CookieHelper.GetValueByCookieskey(Request, cacheKey);
                if (cookieCode != code)
                {
                    rm.Text = "请输入正确的验证码";
                    rm.IsSuccess = false;
                }
                else
                {
                    rm.IsSuccess = true;
                    if (expendCode)
                    {
                        CookieHelper.RemoveCookiesByCookieskey(Request, Response, cacheKey);  //将这个缓存消耗掉
                    }
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return rm;
        }
        #endregion

        #region 获取可以就诊的城市列表
        /// <summary>
        /// 获取可以就诊的城市列表
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        private Common.Share.ReturnMessage CityList(string openid)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                List<SelectItemModel> cityList = null;
                var objCiry = CacheHelper.GetCache(CityListCacheKey);
                if (cityList != null)
                {
                    cityList = objCiry as List<SelectItemModel>;
                }
                else
                {
                    CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
                    var returnModel = client.getCityList();
                    var cityJson = returnModel.Data.ToString();
                    cityList = JsonConvert.DeserializeObject<List<SelectItemModel>>(cityJson);
                    if (null != cityList && cityList.Any())
                    {
                        CacheHelper.SetCache(CityListCacheKey, cityList, 24);
                    }
                }

                if (null != cityList && cityList.Any())
                {
                    rm.IsSuccess = true;
                    rm.ResultData["Result"] = from c in cityList select new { id = c.id, value = c.name, parentId = 0 };
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return rm;
        }

        /// <summary>
        /// 获取可以就诊的城市列表
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>

        [System.Web.Mvc.HttpGet]
        public JsonResult GetCityList(string openid)
        {
            Common.Share.ReturnMessage rm = CityList(openid);
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 在页面显示出用户的openid
        /// <summary>
        /// 在页面显示出用户的openid
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult OpenidAction(string openid)
        {
            openid = Base64Helper.DecodeBase64(openid);
            return Content(string.Format("<h1>{0}</h1>", openid));
        }
        #endregion

        #region 发送手机短信
        /// <summary>
        /// 发送手机短信
        /// </summary>
        /// <param name="openid">当前微信标识</param>
        /// <param name="mobile">接收消息手机号</param>
        /// <param name="content">消息内容</param>
        protected CRMMessageResult SendSmsMessage(string openid, string mobile, string content)
        {
            SMS_Service.WebServiceClient client = new SMS_Service.WebServiceClient();
            var jsonStr = client.sendMsg(mobile, content, "BMY", "CMCC_BMY");
            var obj = JsonConvert.DeserializeObject<CRMMessageResult>(jsonStr);
            UserActionEventLog(new UserActionEventLogInfo
            {
                AppId = 1,
                OpenId = openid,
                ActionType = KeyConst.Uceat_Event,
                ActionName = "SendSmsMessage",
                Parameter = jsonStr
            });
            return obj;
        }
        #endregion

        #region 发邮件

        /// <summary>
        /// 发邮件
        /// </summary>
        /// <param name="openid">当前微信标识</param>
        /// <param name="toAccount">接收邮件</param>
        /// <param name="title"></param>
        /// <param name="content">邮件内容信息</param>
        /// <returns></returns>
        protected string SendMailMessage(string openid, string toAccount, string title, string content)
        {
            SMS_Service.WebServiceClient client = new SMS_Service.WebServiceClient();
            var obj = client.sendMail(toAccount, title, content);
            UserActionEventLog(new UserActionEventLogInfo
            {
                AppId = 1,
                OpenId = openid,
                ActionType = KeyConst.Uceat_Event,
                ActionName = "SendMailMessage",
                Parameter = obj
            });
            return obj;
        }
        #endregion

        #region 将媒体文件下载至本地服务器

        /// <summary>
        /// 将媒体文件下载至本地服务器
        /// </summary>
        /// <param name="media_id">media_id</param>
        /// <param name="suffix">文件后缀名</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public ActionResult DownloadMedia(string media_id, short appid = KeyConst.OrderAppId, string suffix = "amr")
        {
            ReturnMessage rm = DownloadMediaFunction(appid, media_id, suffix);
            return MyJson(rm);
        }

        /// <summary>
        /// 将媒体文件下载至本地服务器
        /// </summary>
        /// <param name="media_id">media_id</param>
        /// <param name="suffix">文件后缀名</param>
        /// <returns></returns>
        protected ReturnMessage DownloadMediaFunction(short appid, string media_id, string suffix = "amr")
        {
            ReturnMessage rm = new ReturnMessage { Text = "操作未完成" };
            try
            {
                LogHelper.Info("DownloadVoice：media_id:" + media_id);
                string src = string.Format("Medias/{0}/", DateTime.Now.ToString("yyyyMMdd"));    //虚拟路径            

                string path = Server.MapPath(string.Format("~/{0}", src));     //文件夹IIS绝对路径文件夹
                string fileName = string.Format("{0}.{1}", media_id, suffix);   //媒体文件名
                string filePath = string.Format("{0}{1}", path, fileName);      //文件IIS绝对路径文件路径
                string httpUrl = BaseUrl + src + fileName;

                if (!System.IO.File.Exists(filePath))
                {
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    var _appId = WeixinConfig.appID;
                    var _appSecret = WeixinConfig.appSecret;
                    var appInfo = new ShareFunction(appid).App;
                    if (null != appInfo)
                    {
                        _appId = appInfo.AppKey;
                        _appSecret = appInfo.AppSecret;
                    }
                    string accessToken = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret);
                    using (FileStream fileStream = new System.IO.FileStream(filePath, FileMode.Create))
                    {
                        Senparc.Weixin.MP.AdvancedAPIs.MediaApi.Get(accessToken, media_id, fileStream);
                        byte[] bytes = new byte[fileStream.Length];
                        BinaryWriter bw = new BinaryWriter(fileStream);
                        bw.Write(bytes);
                        rm.IsSuccess = true;
                        rm.Text = httpUrl;

                        #region amr转mp3

                        if ("amr".Equals(suffix, StringComparison.CurrentCultureIgnoreCase))
                        {
                            try
                            {
                                string mp3path = filePath.Replace(".amr", ".mp3");
                                if (System.IO.File.Exists(filePath) && !System.IO.File.Exists(mp3path))
                                {
                                    string c = Server.MapPath("~/ffmpeg/") + string.Format("ffmpeg.exe -y -i {0} {1}", filePath, mp3path);
                                    CommandHelper.Execute(c, 1500);
                                    if (System.IO.File.Exists(mp3path))
                                    {
                                        rm.IsSuccess = true;
                                        rm.Text = httpUrl.Replace(".amr", ".mp3");
                                    }
                                    else
                                    {
                                        rm.IsSuccess = false;
                                        rm.Text = "文件下载转换失败";
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return rm;
        }
        #endregion

        #region 发送客服消息
        /// <summary>
        /// 发送客服消息
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public JsonResult SendCustomText(string openid, string message)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                rm.IsSuccess = ReVisitBusiness.Instance.SendCustomText(KeyConst.OrderAppId, openid, message);
                if (rm.IsSuccess)
                {
                    rm.Text = "OK";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        /// <summary>
        /// 清空本地的语言变量
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="isMain"></param>
        /// <returns></returns>
        public ActionResult ClearLangDict(int appid, int isMain = 1)
        {
            ReturnMessage rm = new ReturnMessage(true);
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }

    }
}
