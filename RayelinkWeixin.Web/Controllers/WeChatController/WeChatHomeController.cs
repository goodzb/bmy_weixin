﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    public class WeChatHomeController : BaseController
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Home()
        {
            return View(string.Format(WechatViewPath, RouteData.Values["action"]));
        }
    }
}