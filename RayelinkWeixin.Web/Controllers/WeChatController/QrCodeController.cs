﻿using RayelinkWeixin.Auth;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Share;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using RayelinkWeixin.WxApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 二维码管理
    /// </summary>
    public class QrCodeController : BaseController
    {
        #region 变量
        private const string SERVICEONLINEKEY = "3e175ea2-cec1-4b71-b845-c04fa367c6f8";
        private const string REPLACE_REGEX = "(?<!\\\\)\"";
        private const string QrCoPath = "Downloads/{0}/200/{1}.jpg";
        #endregion

        #region 页面

        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>

        public ActionResult Index()
        {
            ViewBag.DefaultQrCode = new QRCodeInfo { AppId = Auth.Authorize.CurrentAppID, QRCodeStatus = 1 };
            return View(string.Format(WechatViewPath, "QrCode"));
        }

        /// <summary>
        /// 编辑页面
        /// </summary>
        /// <returns></returns>
        public ActionResult QrCode_Edit()
        {
            return View(string.Format(WechatViewPath, RouteData.Values["action"]));
        }

        #endregion

        #region 编辑/创建二维码信息

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private ReturnMessage CreateQrCode(QRCodeInfo info)
        {
            ReturnMessage rm = new ReturnMessage(false);
            try
            {
                info.AppId = Auth.Authorize.CurrentAppID;
                var appInfo = new ShareFunction(info.AppId).App;

                var exist = BusinessContent.QRCode.FindOne(where: string.Format("AppID={0} AND QRMedia='{1}' AND QRDesc='{2}' AND QRSceneId<>{3}", info.AppId, info.QRMedia, info.QRDesc, info.QRSceneId));
                if (exist != null)
                {
                    rm.Text = "相同的二维码渠道和描述已经存在，请重试！";
                    return rm;
                }
                info.QRCodeStatus = 1;

                info.QRCataLog = "推广二维码";
                info.QRContent = info.QRSceneId.ToString();

                info.LastUpdateTime = DateTime.Now;

                info.QRDesc = System.Text.RegularExpressions.Regex.Replace(info.QRDesc ?? "", REPLACE_REGEX, "\\\"");
                info.QRcodeValue = System.Text.RegularExpressions.Regex.Replace(info.QRcodeValue ?? "", REPLACE_REGEX, "\\\"");

                exist = BusinessContent.QRCode.FindOne(string.Format("AppID={0} AND QRSceneId={1}", info.AppId, info.QRSceneId), isContainDelete: true);
                if (exist == null)
                {
                    info.QRSID = Guid.NewGuid();
                }
                info.QRcodeValue = TextHelper.ReplaceHexadecimalSymbols(info.QRcodeValue);
                string imgsrc = new QrCode(appInfo.AppKey, appInfo.AppSecret).ShowQrcodeUrl(info.QRSceneId.ToString(), info.QRSID.ToString());
                string logosrc = new ShareFunction(info.AppId).App.AppHeading;

                info.QRSrc = ImageHelper.LoadUrl("Downloads", info.AppId, imgsrc, logosrc);
                if (exist == null)
                {
                    info.QRcodeType = 1;
                    info.QRSID = Guid.NewGuid();
                    info.CreateTime = DateTime.Now;
                    info.QRBeginTime = DateTime.Now;
                    info.Creator = Authorize.CurrentUser.AccountId;
                    info.QRId = BusinessContent.QRCode.Insert(info);
                    if (info.QRId > 0)
                    {
                        rm.IsSuccess = true;
                    }
                }
                else
                {
                    if (info.QRId == 0)
                    {
                        if (exist.QRCodeStatus == 0)
                        {
                            info.QRId = exist.QRId;
                            info.QRSID = exist.QRSID;
                            info.LastUpdateTime = DateTime.Now;
                            info.Updator = Authorize.CurrentUser.AccountId;
                            rm.IsSuccess = BusinessContent.QRCode.Update(info) > 0;
                            rm.ResultData["Result"] = info;
                        }
                        else
                        {
                            //如果场景存在，则加1继续执行
                            info.QRSceneId = (info.QRSceneId + 1);
                            return CreateQrCode(info);  //继续添加
                        }
                    }
                    else
                    {
                        info.LastUpdateTime = DateTime.Now;
                        info.Updator = Authorize.CurrentUser.AccountId;
                        rm.IsSuccess = BusinessContent.QRCode.Update(info) > 0;
                        rm.ResultData["Result"] = info;
                    }
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Message = ex.Message;
                rm.Text = "操作出错";
                new ExceptionHelper().LogException(ex);
            }
            return rm;
        }

        /// <summary>
        /// 编辑一个二维码信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult QRCodeEdit(QRCodeInfo info)
        {
            if (info.QRSceneId == 0)
            {
                int sceneId = 1;
                var qr = BusinessContent.QRCode.FindOne(string.Format("AppID={0}", info.AppId), "QRSceneId DESC", true);  //找到最大的场景ID二维码
                if (null != qr)
                {
                    sceneId = qr.QRSceneId + 1;
                }
                info.QRSceneId = sceneId;
            }
            return MyJson(CreateQrCode(info), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 获取二维码列表
        /// <summary>
        /// 获取二维码列表
        /// </summary>
        /// <param name="appid"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetQrCodeList(int appid)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                IEnumerable<QRCodeInfo> data = BusinessContent.QRCode.FindAll(string.Format("AppID={0}", appid), "QRSceneId"); ;
                if (null != data && data.Any())
                {
                    rm.IsSuccess = true;
                    rm.ResultData["Result"] = data;
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 删除
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                if (BusinessContent.QRCode.Update(new { QRCodeStatus = 0, Updator = Authorize.CurrentUser.AccountId, LastUpdateTime = DateTime.Now }, id) > 0)
                {
                    rm.IsSuccess = true;
                    rm.Text = "删除完成";
                }
                else
                {
                    rm.Text = "删除失败";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm);
        }
        #endregion
    }
}