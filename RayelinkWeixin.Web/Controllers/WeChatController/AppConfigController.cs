﻿using RayelinkWeixin.Common.Share;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Auth;
using System.Reflection;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class AppConfigController : BaseController
    {
        #region 微信帐号配置

        /// <summary>
        /// 微信帐号配置
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="querystring"></param>
        /// <param name="strsel"></param>
        /// <returns></returns>
        //[Authorize]
        public ActionResult AppSettingConfig(string querystring = "", string strsel = "")
        {
            int appid = Authorize.CurrentAppID;
            //ViewBag.ConfigNameList = GetConfigNameList(appid, strsel);

            //string section = string.Empty;
            //ViewBag.section = section;
            //DataSet ds = RayelinkWeixin.DLL.BusinessContent.AppConfig.Fill("SELECT DISTINCT [Name] FROM AppConfig WHERE AppID=@0", appid);
            //if (ds != null)
            //{
            //    List<SelectListItem> item = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--请选择--" } };
            //    ds.Tables[0].AsEnumerable().All(
            //    s =>
            //    {
            //        item.Add(new SelectListItem { Value = s["Name"].ToString(), Text = s["Name"].ToString() }); return true;
            //    });
            //    ViewBag.SectionList = item;
            //}
            ViewBag.DefaultAppConfigInfo = new AppConfigInfo
            {
                AppId = appid,
                Creator = Authorize.CurrentUser.AccountId,
                Status = 1
            };
            return View(string.Format(WechatViewPath, RouteData.Values["action"]));
        }


        /// <summary>
        /// 获取配置列表
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="querystring"></param>
        /// <param name="strsel"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetAppSetting(int appid = 0, string querystring = "", string strsel = "")
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                IEnumerable<AppConfigInfo> aciList = new List<AppConfigInfo>();

                if (string.IsNullOrWhiteSpace(querystring))
                {
                    aciList = BusinessContent.AppConfig.Find(string.Format("AppID={0}", appid), 100, "Name");
                }
                else
                {
                    aciList = BusinessContent.AppConfig.Find(string.Format("AppID={0} and {1}", appid, querystring), 100, "Name");
                }
                if (null != aciList && aciList.Any())
                {
                    rm.IsSuccess = true;
                    rm.ResultData["Result"] = aciList;
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AppConfig_Edit(int id = 0)
        {
            AppConfigInfo appConfigInfo = null;
            if (id > 0)
            {
                appConfigInfo = BusinessContent.AppConfig.FindOne("ConfigId = @0", id);
            }
            else
            {
                appConfigInfo = new AppConfigInfo
                {
                    AppId = Authorize.CurrentAppID,
                    Creator = Authorize.CurrentUser.AccountId,
                    Status = 1
                };
            }
            return View(string.Format(WechatViewPath, RouteData.Values["action"]), appConfigInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="strsel"></param>
        /// <returns></returns>
        private IList<SelectListItem> GetConfigNameList(int appid, string strsel)
        {
            var aciList = BusinessContent.AppConfig.Find(string.Format("AppID={0}", appid), 100);
            IList<SelectListItem> sliList = new List<SelectListItem>
            {
                new SelectListItem { Value = "", Text = "--请选择--" }
            };
            var disAciList = aciList.Select(s => s.Name).Distinct().ToList();
            foreach (var item in disAciList)
            {
                if (item == strsel)
                {
                    sliList.Add(new SelectListItem { Value = item, Text = item, Selected = true });
                }
                else
                {
                    sliList.Add(new SelectListItem { Value = item, Text = item });
                }
            }
            return sliList;
        }

        /// <summary>
        /// 添加配置项
        /// </summary>
        /// <param name="info"></param>
        /// <param name="appid"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddAppConfig(AppConfigInfo info)
        {
            int appid = info.AppId;
            ReturnMessage rm = new ReturnMessage(false);
            try
            {
                if (info.ConfigId > 0)
                {
                    if (BusinessContent.AppConfig.Update(new { Updator = Authorize.CurrentUser.AccountId, LastUpdateTime = DateTime.Now, info.Name, info.Value, info.Key, info.Memo }, info.ConfigId) > 0)
                    {
                        rm.IsSuccess = true;
                        rm.Text = "修改成功!";
                    }
                    else
                    {
                        rm.Text = "修改失败!";
                    }
                }
                else
                {
                    if (appid != Authorize.CurrentAppID) appid = 0;
                    info.AppId = appid;
                    info.Type = 1;
                    info.Status = 1;
                    info.Creator = Authorize.CurrentUser.AccountId;
                    info.ConfigId = BusinessContent.AppConfig.Insert(info);
                    if (info.ConfigId > 0)
                    {
                        rm.Text = "增加成功!";
                    }
                    else
                    {
                        rm.Text = "增加失败!";
                    }
                }
                RefreshAppConfig(info.AppId);
                ClearLangDict(appid);
                Auth.Authorize.WeChatAdmin(true);
            }
            catch (Exception e)
            {
                rm.IsSuccess = false;
                rm.Message = e.Message;
                rm.Text = "添加出错!";
                new ExceptionHelper().LogException(e);
            }
            return MyJson(rm);
        }

        /// <summary>
        /// 删除一个配置项
        /// </summary>
        /// <param name="id"></param>
        /// <param name="appId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteAppConfig(int id, int appId)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                if (BusinessContent.AppConfig.Update(new { Status = 0, Updator = Authorize.CurrentUser.AccountId, LastUpdateTime = DateTime.Now }, id) > 0)
                {
                    rm.IsSuccess = true;
                    rm.Text = "删除完成";
                    RefreshAppConfig(appId);
                }
                else
                {
                    rm.Text = "删除失败";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm);
        }
        #endregion

        #region App

        /// <summary>
        /// AppConfig初始页面
        /// </summary>
        /// <param name="appid"></param>
        /// <returns></returns>
        public ActionResult AppConfig(int appid = 0)
        {
            return View();
        }

        /// <summary>
        /// 获取appList
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult AppPageList(int pageIndex = 1, int pageSize = 500)
        {
            ReturnMessage rm = new ReturnMessage(true);
            try
            {
                int count = 0;
                rm.ResultData["List"] = BusinessContent.App.GetPerPageFormDataList("", pageIndex, pageSize, out count).Select(p => new
                {
                    AppName = p.AppName,
                    AppUserName = p.AppUserName,
                    AppShortName = p.AppShortName,
                    AppSID = p.AppSid,
                    AppWebSite = p.AppWebSite,
                    AppToken = p.AppToken,
                    AppID = p.AppId,
                    AppHeading = p.AppHeading
                }).OrderBy(t => t.AppID);
                rm.ResultData["TotalCount"] = count;
                rm.ResultData["PageCount"] = (int)Math.Ceiling(count * 1.0 / pageSize);
                rm.IsSuccess = true;
            }
            catch (Exception e)
            {
                rm.IsSuccess = false;
                new ExceptionHelper().LogException(e);
            }
            return MyJson(rm);
        }

        private const string AppListKey = "AppList_";
        /// <summary>
        /// 获取APP列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult AppList()
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                IEnumerable<AppInfo> data = new List<AppInfo>();
                var cache = CacheHelper.GetCache(AppListKey);
                if (null != cache)
                {
                    data = cache as List<AppInfo>;
                }
                else
                {
                    data = BusinessContent.App.Find("", 100, "appid");
                }
                if (null != data && data.Any())
                {
                    rm.IsSuccess = true;
                    rm.ResultData["Result"] = data;
                    rm.Text = "操作完成";
                    CacheHelper.SetCache(AppListKey, data, 60 * 10);
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 更新app
        /// </summary>
        /// <param name="ai"></param>
        /// <returns></returns>
        public JsonResult AppUpdate(AppInfo ai)
        {
            ReturnMessage rm = new ReturnMessage(true);
            AppInfo old = DLL.BusinessContent.App.GetFormDataInfoByIdentityKey(ai.AppId);
            if (old != null)
            {
                old.AppShortName = ai.AppShortName;   //简称
                old.AppName = ai.AppName;        //名称
                old.AppHeading = ai.AppHeading;
                old.AppLastUpdateTime = DateTime.Now;
                if (DLL.BusinessContent.App.Update(old) > 0)
                {
                    rm.IsSuccess = true;
                }
                else
                {
                    rm.IsSuccess = false;
                }
            }
            else
            {
                rm.IsSuccess = false;
            }
            return MyJson(rm);
        }

        /// <summary>
        /// 切换管理App
        /// </summary>
        /// <param name="appId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SwitchApp(int appId)
        {
            ReturnMessage rm = new ReturnMessage(true);
            //检查是否要切换App
            if (appId != 0 && appId != Authorize.CurrentAppID)
            {
                Authorize.CurrentAppID = appId;
                rm.IsSuccess = true;
            }
            return Json(rm);
        }

        #endregion

    }
}