﻿using Newtonsoft.Json;
using RayelinkWeixin.Auth;
using RayelinkWeixin.Common.Share;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;

namespace RayelinkWeixin.Web.Controllers
{
    public class AccountController : BaseController
    {

        #region 打开登录页面
        /// <summary>
        /// 打开登录页面
        /// </summary>
        /// <param name="isAjax"></param>
        /// <returns></returns>
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult Logon(bool isAjax = false)
        {
            string email = CookieHelper.ReadEncryptCookie("userlogoninfo", "email");
            string pwd = CookieHelper.ReadEncryptCookie("userlogoninfo", "pwd");
            AccountInfo ai = BusinessContent.Account.FindOne(sqls: "Email=@0", arg: email);

            IList<string> logs = new List<string>
            {
                "email:" + email,
                "pwd:" + pwd
            };
            if (ai != null && string.Equals(pwd, ai.Pwd, StringComparison.CurrentCultureIgnoreCase))
            {
                logs.Add("AccountInfo:" + JsonConvert.SerializeObject(ai));
                Auth.Authorize.SignIn(email);
                ai.LastUpdateTime = DateTime.Now;
                BusinessContent.Account.Update(ai);

                Authorize.CurrentUser = ai;

                logs.Add("自动登录成功");
                FileLogHelper.Instance.Log(logs, "Logon_");
                return Redirect(GetRedirectUrl(email, false));
            }
            else
            {
                if (ai == null)
                {
                    logs.Add("AccountInfo:");
                }
                else
                {
                    logs.Add("AccountInfo:" + JsonConvert.SerializeObject(ai));
                }
                logs.Add("自动登录失败");
                FileLogHelper.Instance.Log(logs, "Logon_");
                return View(string.Format(WechatViewPath, RouteData.Values["action"]));
            }
        }
        #endregion

        #region 用户登录验证操作
        /// <summary>
        /// 用户登录验证操作
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="verifycode"></param>
        /// <param name="rember"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public JsonResult LogonAPI(string email, string password, string verifycode, bool rember = true)
        {
            ReturnMessage rm = new ReturnMessage(false);
            try
            {
                {
                    AccountInfo ai = BusinessContent.Account.FindOne(sqls: "Email=@0", arg: email);

                    if (ai != null && string.Equals(password, ai.Pwd, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (ai.Status != 1)
                        {
                            //不是正常状态
                            rm.Text = "帐号未激活或已被禁用";
                            return MyJson(rm, JsonRequestBehavior.AllowGet);
                        }

                        rm.IsSuccess = true;
                        rm.Text = "登录成功!";

                        if (rember)
                        {
                            CookieHelper.WriteEncryptCookie("userlogoninfo", "email", email);
                            CookieHelper.WriteEncryptCookie("userlogoninfo", "pwd", ai.Pwd);
                        }

                        //这里需要注意了,会跳转到首页去
                        Auth.Authorize.SignIn(email);

                        ai.LastUpdateTime = DateTime.Now;
                        BusinessContent.Account.Update(ai);
                        Auth.Authorize.CurrentUser = ai;

                        rm.ReturnUrl = GetRedirectUrl(email, false);
                    }
                    else
                    {
                        rm.IsSuccess = false;
                        rm.Text = "登录失败!用户不存在或者用户名密码不匹配!";
                    }
                }
            }
            catch (Exception ee)
            {
                rm.IsSuccess = false;
                rm.Message = ee.Message;
                rm.Text = "操作出错";
                new ExceptionHelper().LogException(ee);
            }
            return MyJson(rm);
        }
        #endregion

        #region 退出登录
        /// <summary>
        /// 退出登录
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Logout()
        {
            Auth.Authorize.SignOut();
            Response.Redirect("~/Account/Logon");
            return View();
        }
        #endregion

        #region 返回导致重定向到登录页的原始请求的重定向 URL。
        /// <summary>
        /// 返回导致重定向到登录页的原始请求的重定向 URL。
        /// </summary>
        /// <param name="userName">已验证身份的用户的名称</param>
        /// <param name="createPersistentCookie">忽略此参数</param>
        /// <returns>一个字符串，其中包含重定向 URL。</returns>
        private string GetRedirectUrl(string userName, bool createPersistentCookie)
        {
            string result = FormsAuthentication.GetRedirectUrl(userName, createPersistentCookie);
            if (result.EndsWith("/User", StringComparison.CurrentCultureIgnoreCase))
            {
                result = BaseUrl + "WeChatHome/Home"; 
            }
            return result;
        }
        #endregion
    }
}