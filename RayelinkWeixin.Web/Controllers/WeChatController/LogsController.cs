﻿using RayelinkWeixin.Common.Utilites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// Log
    /// </summary>
    public class LogsController : BaseController
    {
        private const string LogsCacheKey = "LogsCacheKey";
        private const string LogCacheKey = "LogKey_{0}";

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Logs()
        {
            return View(string.Format(WechatViewPath, RouteData.Values["action"]));
        }


        /// <summary>
        /// 是否读取缓存里面的数据
        /// </summary>
        /// <param name="isCache"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetLogs(bool isCache = true)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                IEnumerable<string> files = null;
                var cache = CacheHelper.GetCache(LogsCacheKey);
                if (null != cache && isCache)
                {
                    files = cache as IEnumerable<string>;
                }
                else
                {
                    files = FilesHelper.EnumerateFiles(System.AppDomain.CurrentDomain.BaseDirectory + "\\logs");
                }
                if (null != files && files.Any())
                {
                    CacheHelper.SetCache(LogsCacheKey, files, 10);
                    rm.IsSuccess = true;
                    rm.ResultData["Result"] = from f in files select new { FileName = f.Remove(0, f.LastIndexOf("\\") + 1), FilePath = f };
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="isCache"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetFileContents(string filePath, bool isCache = true)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                string data = string.Empty;
                var cache = CacheHelper.GetCache(string.Format(LogCacheKey, TextHelper.MD5(filePath)));
                if (null != cache && isCache)
                {
                    data = cache.ToString();
                }
                else
                {
                    data = FilesHelper.Read2Buffer(filePath); ;
                }
                if (!string.IsNullOrEmpty(data))
                {
                    CacheHelper.SetCache(string.Format(string.Format(LogCacheKey, TextHelper.MD5(filePath))), data, 10);
                    rm.IsSuccess = true;
                    rm.ResultData["Result"] = data;
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }

    }
}