﻿using RayelinkWeixin.Common.Utilites;
using System;
using System.Net;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class StolenImgController : BaseController
    {
        #region 加载网络图片路径

        /// <summary>
        /// 加载网络图片路径
        /// </summary>
        /// <param name="imgsrc"></param>
        /// <returns></returns>
        public ActionResult Load(string imgsrc)
        {
            int appid = SqlFilterHelper.RequestString<int>("appid", "0");
            try
            {
                if (!string.IsNullOrWhiteSpace(imgsrc))
                {
                    string[] imgpaths = imgsrc.Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    string name = string.Empty;
                    string size = string.Empty;
                    string piclocalpath = string.Empty;
                    if (imgsrc.ToLower().Contains("wx.qlogo.cn"))
                    {
                        name = imgpaths[3];
                        size = imgpaths[4];
                        int zi = 0;
                        if (!int.TryParse(size, out zi))
                        {
                            size = appid.ToString();
                            name = TextHelper.MD5(imgsrc);
                        }
                    }
                    else
                    {
                        name = Guid.NewGuid().ToString().ToLower();
                        size = "200";
                    }
                    string defaultpiclocalpath = Server.MapPath("~/Images/defaultavatar.png");
                    if (!string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(size))
                    {

                        piclocalpath = Server.MapPath(string.Format("~/Images/{0}/{1}/{2}.jpg", appid, size, name));
                        string path = Server.MapPath(string.Format("~/Images/{0}/{1}", appid, size));
                        if (!System.IO.File.Exists(piclocalpath))
                        {
                            WebClient wc = new WebClient();

                            if (!System.IO.Directory.Exists(path))
                            {
                                System.IO.Directory.CreateDirectory(path);
                            }

                            try
                            {
                                wc.DownloadFile(imgsrc, piclocalpath);
                            }
                            catch (Exception ex)
                            {
                                return Content(ex.Message);
                            }
                        }
                        return File(piclocalpath, "application/x-jpg", string.Format("{0}.jpg", name));
                    }
                    else
                    {
                        return Content("");
                    }
                }
                else
                {
                    return Content("");
                }
            }
            catch (Exception e)
            {
                e.Data["ErrorMessage"] = e.Message;
                e.Data["Method"] = "StolenImgController=>Load";
                new ExceptionHelper().LogException(e);
                return Content(e.Message);
            }
        }
        #endregion

        #region 加载网络图片路径,上传到图片服务器
        /// <summary>
        /// 加载网络图片路径,上传到图片服务器
        /// </summary>
        /// <param name="imgsrc">原图URL</param>
        /// <param name="logosrc">logoURL</param>
        /// <returns></returns>
        public ActionResult LoadUrl(string imgsrc, string logosrc = "")
        {
            int appid = SqlFilterHelper.RequestString<int>("appid", "0");
            string res = ImageHelper.LoadUrl("Downloads", appid, imgsrc, logosrc);
            return Content(res);
        } 
        #endregion
    }
}