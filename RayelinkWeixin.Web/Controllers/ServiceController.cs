﻿using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Data.PO.Extend;
using RayelinkWeixin.Data.Repository;
using RayelinkWeixin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    [AllowAnonymous]
    public class ServiceController : Controller
    {
        //
        // GET: /Service/

        public CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();

        /// <summary>
        /// 获取地方诊所机构
        /// </summary>
        public ActionResult GetOrganizationList(int? cityId, int pagesize, int pagenumber)
        {
            //string key = "organizationlist" + cityId;
            //var organization= CacheHelper.GetCache(key);
            //if (organization!=null)
            //{
            //    return Json(organization.ToString());
            //}
            var res = client.getOrganizationList(cityId, "", pagenumber, pagesize);
            var organizationListJson = res.Data.ToString();
            var organizationList = JsonConvert.DeserializeObject<List<HospitalModel>>(organizationListJson);
            if (organizationList != null)
            {
                organizationList = organizationList.OrderBy(p => p.hospitalName).ToList();
            }
            return Json(JsonConvert.SerializeObject(organizationList));
            //CacheHelper.SetCache(key, res.Data.ToString(),3);
            //return Json(res.Data);
        }

        /// <summary>
        /// 获取诊所城市列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCityList()
        {
            var res = client.getCityList();
            return Json(res.Data);
        }

        /// <summary>
        /// 获取所有医生列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDoctorList(int deptId, string key, int pagenumber, int pagesize, string hospitalName, string deptName2)
        {
            try
            {

                LogHelper.Debug(string.Format("查询医生列表参数：{0}--{1}--{2}--{3}--{4}--{5}", pagesize, pagenumber, key, deptId, hospitalName,deptName2));
                var res = client.getDoctorList(deptId, -1, key, deptName2, "1,2,3", hospitalName == "全部" ? "" : hospitalName, pagenumber, pagesize);
                return Json(res.Data);
            }
            catch (Exception e)
            {
                LogHelper.Error(e);
                return Json(string.Empty);
            }
            //CacheHelper.SetCache("hospitals664", res.Data.ToString());
        }

        #region 微信推送
        public ActionResult PushWxMsg(ReferralExtend refer)
        {
            int r = PushMessageRepository.Instance.ReferralSendWxMsg(refer);
            return Json(r);
        }
        #endregion
    }
}
