﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：OrderController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-28
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using RayelinkWeixin.Web.Models;
using Senparc.Weixin.MP.Containers;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
namespace RayelinkWeixin.Web.Controllers
{
    [AllowAnonymous]
    public class OrderController : BaseController
    {
        /// <summary>
        /// 预约页
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [Authorize]
        public ActionResult Subscribe(OrderModel model)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var objDoctor = CacheHelper.GetCache(KeyConst.CacheDoctor + model.docID);
            var objOrganization = CacheHelper.GetCache(KeyConst.CacheOrganization + model.hospitalID);
            // 获得医生诊所信息
            DoctorModel doctorModel = new DoctorModel();
            HospitalModel hospitalModel = new HospitalModel();
            if (objDoctor != null)
            {
                doctorModel = JsonConvert.DeserializeObject<DoctorModel>(objDoctor.ToString()); ;
            }
            else
            {
                var returnModel = client.getDoctor(model.docID);
                var doctorJson = returnModel.Data.ToString();
                if (!String.IsNullOrEmpty(doctorJson))
                {
                    CacheHelper.SetCache(KeyConst.CacheDoctor + model.docID, doctorJson);
                }
                doctorModel = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
            }
            if (objOrganization != null)
            {
                hospitalModel = JsonConvert.DeserializeObject<HospitalModel>(objOrganization.ToString());
            }
            else
            {
                var returnModel1 = client.getOrganization(model.hospitalID);
                var organizationJson = returnModel1.Data.ToString();
                hospitalModel = JsonConvert.DeserializeObject<HospitalModel>(organizationJson);
                if (!String.IsNullOrEmpty(organizationJson))
                {
                    CacheHelper.SetCache(KeyConst.CacheOrganization + model.hospitalID, organizationJson);
                }
            }

            var pid = 0;
            var name = "";
            var phone = "";
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);

                if (user != null)
                {
                    pid = user.pid;
                    name = user.username;
                    phone = user.telphone;
                }
            }
            // 判断是否选择就诊人
            if (model.userId > 0)
            {
                // 判断是否选择当前绑定账户
                if(model.userId == pid)
                {
                    CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
                    var returnUserinfo = crmClient.CRM_WEB_GetMemberModelByPid(pid);
                    if (returnUserinfo != null)
                    {

                        model.name = returnUserinfo.Name;
                        model.phone = returnUserinfo.Mobile;
                        if (returnUserinfo.MemberCertList != null)
                        {
                            var cardInfo = returnUserinfo.MemberCertList.Where(p => p.CertTypeCode == "CID").OrderByDescending(p => p.UpdateTime).FirstOrDefault(); // 身份证号 
                            if (cardInfo != null)
                            {
                                model.id = cardInfo.CertNo;
                            }
                        }
                    }
                    // 判断当前绑定账号是否是默认就诊人
                    model.famliyId = pid;
                    //model.nowUserId = model.userId;
                    var returnModelList = client.FamilyMember_GetList(pid);
                    var familyMemberA = returnModelList.Data;
                    if (familyMemberA != null)
                    {
                        var familyMemberList = familyMemberA.ToList().Where(p => p.IsDefault == true).FirstOrDefault();
                        if (familyMemberList != null)
                        {
                            model.IsDefault = false;
                        }
                        else
                        {
                            model.IsDefault = true;
                        }
                    }
                }else
                {
                    var returnModel4 = client.FamilyMember_Get(model.userId);
                    var familyMember = returnModel4.Data;
                    if (familyMember != null)
                    {
                        UserModel familyMemberModel = new UserModel()
                        {
                            FamilyName = familyMember.FamilyName,
                            FamilyMobile = familyMember.FamilyMobile,
                            IDCardNo = familyMember.IDCardNo,
                            Gender = familyMember.Gender,
                            Birthday = familyMember.Birthday,
                            IsDefault = familyMember.IsDefault
                        };
                        model.name = familyMemberModel.FamilyName;
                        model.phone = familyMemberModel.FamilyMobile;
                        model.id = familyMemberModel.IDCardNo;
                        model.sex = familyMemberModel.Gender;
                        model.Birthday = familyMemberModel.Birthday;
                        model.famliyId = model.famliyId;
                        model.nowUserId = model.userId;
                        model.IsDefault = familyMemberModel.IsDefault;
                    }
                }
                
               
            }
            else
            {
                var returnModel = client.FamilyMember_GetList(pid);
                var familyMemberArry = returnModel.Data;
                var familyMemberModelList = familyMemberArry.FirstOrDefault(p => p.IsDefault == true);
                // 获取就诊人列表中默认就诊人 若无 则取当前绑定用户
                if (familyMemberModelList != null)
                {
                    model.name = familyMemberModelList.FamilyName;
                    model.phone = familyMemberModelList.FamilyMobile;
                    model.id = familyMemberModelList.IDCardNo;
                    model.sex = familyMemberModelList.Gender;
                    model.Birthday = familyMemberModelList.Birthday;
                    model.famliyId = familyMemberModelList.FamilyPID == null ? 0 : familyMemberModelList.FamilyPID.Value;
                    model.nowUserId = familyMemberModelList.Id;
                    model.IsDefault = familyMemberModelList.IsDefault;
                }else
                {
                    CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
                    var returnUserinfo = crmClient.CRM_WEB_GetMemberModelByPid(pid);
                    if (returnUserinfo != null)
                    {
                        model.name = returnUserinfo.Name;
                        model.phone = returnUserinfo.Mobile;
                        if (returnUserinfo.MemberCertList != null)
                        {
                            var cardInfo = returnUserinfo.MemberCertList.Where(p => p.CertTypeCode == "CID").OrderByDescending(p => p.UpdateTime).FirstOrDefault(); // 身份证号 
                            if (cardInfo != null)
                            {
                                model.id = cardInfo.CertNo;
                            }
                        }
                    }
                    model.famliyId = pid;
                    //model.nowUserId = pid;
                    var returnModelList = client.FamilyMember_GetList(pid);
                    var familyMemberA = returnModelList.Data;
                    if (familyMemberA != null)
                    {
                        var familyMemberList = familyMemberA.ToList().Where(p => p.IsDefault == true).FirstOrDefault();
                        if (familyMemberList != null)
                        {
                            model.IsDefault = false;
                        }
                        else
                        {
                            model.IsDefault = true;
                        }
                    }

                }
            }

            // 获得诊费
            var returnModel3 = client.GetClinicCategory(model.clinicinfoID);
            var clinicCategoryJson = returnModel3.Data.ToString();
            var clinicCategoryModel = JsonConvert.DeserializeObject<ClinicCategoryModel>(clinicCategoryJson);


            model.pid = pid;
            model.docName = doctorModel.name;
            model.docImage = doctorModel.image;
            model.deptName = doctorModel.depName2;
            model.titleName = doctorModel.titleName;
            model.formatDate = Convert.ToDateTime(model.date).ToString("yyyy年MM月dd日");
            if (String.IsNullOrEmpty(model.time) || (model.time != null && String.IsNullOrEmpty(model.time.Trim())))
            {
                if ("am".Equals(model.target))
                {
                    model.formatTime = "上午";
                }
                else if ("pm".Equals(model.target))
                {
                    model.formatTime = "下午";
                }
                else if ("night".Equals(model.target))
                {
                    model.formatTime = "晚上";
                }
            }
            else
            {
                model.formatTime = model.time;
            }
            model.hospitalName = hospitalModel.hospitalName;
            model.address = hospitalModel.hospitalAddress;
            if (clinicCategoryModel !=null)
            {
                if("1".Equals(model.onOffLine))
                {
                    model.fee = clinicCategoryModel.remoteregistrationFee;
                }else
                {
                    model.fee = clinicCategoryModel.registrationFee;
                }
                
            }
            var url = Request.Url.ToString().Split('#').Length > 0 ? Request.Url.ToString().Split('#')[0].Replace(":" + WeixinConfig.port, "") : Request.Url.ToString().Replace(":" + WeixinConfig.port, "");
            Session[KeyConst.SessionOrderUrl] = Base64Helper.EncodeBase64(url);
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View(model);
        }

        ///<summary>
        /// 下载图片
        /// </summary>
        public JsonResult downloadPics(string serverId)
        {
            var _appId = WeixinConfig.appID;
            var _appSecret = WeixinConfig.appSecret;
            WeixinDownloadHelper.WeixinAsyncDownload(AccessTokenContainer.TryGetAccessToken(_appId, _appSecret), serverId);
            return Json(serverId + ".png");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="target"></param>
        /// <param name="hospId"></param>
        /// <param name="remoteHospId"></param>
        /// <param name="depId"></param>
        /// <param name="depTypeId"></param>
        /// <param name="doctorId"></param>
        /// <param name="timePeriod"></param>
        /// <param name="resvDate"></param>
        /// <param name="description"></param>
        /// <param name="pictures"></param>
        /// <param name="phone"></param>
        /// <param name="id"></param>
        /// <param name="famliyId"></param>
        /// <param name="nowUserId"></param>
        /// <param name="isdefault"></param>
        /// <param name="name"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        ///<summary>
        /// 添加预约
        ///</summary>
        public ActionResult ResvIntention_Add(string type, string target, int hospId, int remoteHospId, int depId, int depTypeId, int doctorId, string timePeriod, DateTime resvDate, string description, string pictures, string phone, string id, string famliyId, string nowUserId,string isdefault, string name,string openid)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var pid = 0;
            var userIP = Request.UserHostAddress;
            int str = int.Parse(id.Substring(id.Length - 2, 1));
            string sex = "";
            if (str % 2 == 0)
            {
                sex = "女";
            }
            else
            {
                sex = "男";
            }
            JObject userInfoJson = JObject.FromObject(new
            {
                Name = name,
                Mobile = phone,
                Sex =sex,
                CertNo=id
              
            });
            bool fault = false;
            if ("1".Equals(isdefault))
            {
                fault = true;
            }
            int ty = 0;
            if ("yuyue".Equals(type))
            {
                ty = 0;
            }
            else if ("jiahao".Equals(type))
            {
                ty = 1;
            }
            else
            {
                ty = 2;
            }
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);

                if (user != null)
                {
                    pid = user.pid;
                }
            }
            if("am".Equals(target))
            {
                target = "上午";
            }else  if("pm".Equals(target))
            {
                target = "下午";
            }
            else if ("night".Equals(target))
            {
                target = "晚上";
            }

            if(String.IsNullOrEmpty(timePeriod))
            {
                timePeriod = target;
            }

            if("0".Equals(famliyId))
            {
                famliyId = "0";
            }

            string urlOpenID = Request.Params["openid"] == null ? "" : Request.Params["openid"].ToString();
            openid = Base64Helper.DecodeBase64(openid);
            if (string.IsNullOrEmpty(openid))
            {
                openid = Base64Helper.DecodeBase64(urlOpenID);
            }
            var model = client.ResvIntention_Add(int.Parse(famliyId), ty, hospId, remoteHospId, depId, depTypeId, doctorId, timePeriod, resvDate, description, pictures.TrimEnd(','), userIP, "微信", openid, userInfoJson.ToString(), pid);
            if (model != null)
            {
                if (model.Success)
                {
                    // 保存预约图片
                    // type 1 咨询 2预约
                    var returnMoble = client.addSeekDetail(pid, pictures.TrimEnd(','), model.Data.ToString(), 2, "微信");
                    if (int.Parse(nowUserId) != 0 && int.Parse(nowUserId) !=pid)
                    {
                        // 更新就诊人信息
                        var returnModel = client.FamilyMember_Update(int.Parse(nowUserId), "", name, phone, "", null, sex, id, fault);
                    }
                    if(int.Parse(nowUserId) == pid)
                    {
                        CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
                        CRMWebService.Member member = crmClient.CRM_WEB_GetMemberModelByPid(pid);
                        if (member != null)
                        {
                            // 更新绑定用户信息
                            member.Name = name;
                            var returnMember = crmClient.CRM_WEB_UpdatePersonBaseWithCert(member, "CID", id);
                        }
                    }
                   
                    return Json(new { success = true, data = model.Data });
                }
                else
                {
                    return Json(new { success = false, message = model.Message });
                }
            }
            return Json(new { success = false, data = model.Message });
        }

        /// <summary>
        /// 专家排班
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public ActionResult OrderList(string time)
        {

            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            var model = new OrderModel();
            DateTime dt = new DateTime();
            if (time != null && !"".Equals(time))
            {
                dt = DateTime.Parse(time);
            }
            else
            {
                dt = DateTime.Now;
            }

            model.formatTime = getFormatTime(dt);
            model.formatDate = dt.ToString("yyyy-MM-dd");
            model.isLastSelectableDate = IsLastSelectableDate(dt);
            model.isFirstSelectableDate = IsFirstSelectableDate(dt);
            return View(model);
        }

        /// <summary>
        /// 获取医生列表
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="page"></param>
        /// <param name="currentIndex"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public ActionResult GetDoctorList(string keyword, int page, int currentIndex)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var dateTime = Convert.ToDateTime(keyword);
            var returnModel = client.GetDoctorScheduleByDate(-1, dateTime, dateTime);
            var scheduleArray = returnModel.Data;
            string doctorIdList = "";
            foreach (var docSchedule in scheduleArray)
            {
               doctorIdList += docSchedule.docID.ToString() + ",";
            }
            var docList = client.getDoctorListBy(doctorIdList.TrimEnd(','));
            var jsonString = docList.Data == null ? "" : docList.Data;
            return Json(jsonString);

        }

        /// <summary>
        /// 往前翻页
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public ActionResult GetPreDay(string keyword)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            DateTime dt = DateTime.Parse(keyword);
            var model = new OrderModel();
            DateTime date = dt.AddDays(-1);

            model.formatTime = getFormatTime(date);
            model.formatDate = date.ToString("yyyy-MM-dd");
            model.isLastSelectableDate = IsLastSelectableDate(date);
            model.isFirstSelectableDate = IsFirstSelectableDate(date);
            return View("OrderList", model);
        }

        /// <summary>
        /// 往后翻页
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public ActionResult GetNextDay(string keyword)
        {

            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            DateTime dt = DateTime.Parse(keyword);

            var model = new OrderModel();
            DateTime date = dt.AddDays(1);


            model.formatTime = getFormatTime(date);
            model.formatDate = date.ToString("yyyy-MM-dd");
            model.isLastSelectableDate = IsLastSelectableDate(date);
            model.isFirstSelectableDate = IsFirstSelectableDate(date);
            return View("OrderList", model);
        }

        public String getFormatTime(DateTime time)
        {
            string date = "";
            string dt = time.DayOfWeek.ToString();
            date += time.ToString("MM-dd");
            switch (dt)
            {
                case "Monday":
                    date += "周一";
                    break;
                case "Tuesday":
                    date += "周二";
                    break;
                case "Wednesday":
                    date += "周三";
                    break;
                case "Thursday":
                    date += "周四";
                    break;
                case "Friday":
                    date += "周五";
                    break;
                case "Saturday":
                    date += "周六";
                    break;
                case "Sunday":
                    date += "周日";
                    break;
            }
            return date;
        }

        /// <summary>
        /// 选择日期
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public ActionResult ChooseDate(string keyword)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            var model = new OrderModel();
            model.formatTime = keyword;
            return View(model);
        }

        /// <summary>
        /// 是否是第一个选择日期
        /// </summary>
        /// <param name="selectedDate"></param>
        /// <returns></returns>
        private bool IsFirstSelectableDate(DateTime selectedDate)
        {
            bool isFirst = false;
            DateTime dtFirstSelectable = new DateTime();
            var firstSelectabelDate = CacheHelper.GetCache(KeyConst.CacheFirstSelectableDate);
            if (firstSelectabelDate != null && !"".Equals(firstSelectabelDate))
            {
                dtFirstSelectable = DateTime.Parse(firstSelectabelDate.ToString());
                if (selectedDate <= dtFirstSelectable)
                {
                    isFirst = true;
                }
            }
            else
            {
                CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
                var dtNow = DateTime.Today;
                var docList = client.GetDoctorScheduleByDate(-1, DateTime.Today, DateTime.Today.AddYears(1));
                if (docList != null && docList.Data != null && docList.Data.Count() > 0)
                {
                    var maxSelectableDate = docList.Data.Select(p => p.Date).Where(p => p >= dtNow).Max();
                    var minSelectableDate = docList.Data.Select(p => p.Date).Where(p => p >= dtNow).Min();
                    if (minSelectableDate != null)
                    {
                        CacheHelper.SetCache(KeyConst.CacheFirstSelectableDate, minSelectableDate.ToString("yyyy-MM-dd"));
                        if (selectedDate <= DateTime.Parse(minSelectableDate.ToString("yyyy-MM-dd")))
                        {
                            isFirst = true;
                        }
                    }
                    if (maxSelectableDate != null)
                    {
                        CacheHelper.SetCache(KeyConst.CacheLastSelectableDate, maxSelectableDate.ToString("yyyy-MM-dd"));
                    }
                }
                else
                {
                    isFirst = true;
                }
            }
            return isFirst;
        }

        /// <summary>
        /// 是否是最后一个选择日期
        /// </summary>
        /// <param name="selectedDate"></param>
        /// <returns></returns>
        private bool IsLastSelectableDate(DateTime selectedDate)
        {
            bool isLast = false;
            DateTime dtSelectable = new DateTime();
            var lastSelectabelDate = CacheHelper.GetCache(KeyConst.CacheLastSelectableDate);
            if (lastSelectabelDate != null && !"".Equals(lastSelectabelDate))
            {
                dtSelectable = DateTime.Parse(lastSelectabelDate.ToString());
                if (selectedDate >= dtSelectable)
                {
                    isLast = true;
                }
            }
            else
            {
                CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
                var dtNow = DateTime.Today;
                var docList = client.GetDoctorScheduleByDate(-1, DateTime.Today, DateTime.Today.AddYears(1));
                if (docList != null && docList.Data != null && docList.Data.Count() > 0)
                {
                    var maxSelectableDate = docList.Data.Select(p => p.Date).Where(p => p >= dtNow).Max();
                    var minSelectableDate = docList.Data.Select(p => p.Date).Where(p => p >= dtNow).Min();
                    if (maxSelectableDate != null)
                    {
                        CacheHelper.SetCache(KeyConst.CacheLastSelectableDate, maxSelectableDate.ToString("yyyy-MM-dd"));
                        if (selectedDate >= DateTime.Parse(maxSelectableDate.ToString("yyyy-MM-dd")))
                        {
                            isLast = true;
                        }
                    }
                    if (minSelectableDate != null)
                    {
                        CacheHelper.SetCache(KeyConst.CacheFirstSelectableDate, minSelectableDate.ToString("yyyy-MM-dd"));
                    }
                }
                else
                {
                    isLast = true;
                }
            }
            return isLast;
        }

        /// <summary>
        /// 获取排班表
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public JsonResult GetSchedulingTime(string keyword)
        {
            var cacheSchedule = CacheHelper.GetCache(KeyConst.CacheDoctorScheduleTime);
            if (cacheSchedule != null)
            {
                return Json(cacheSchedule);
            }
            else
            {

                //获取当前选择的时间
                var dtCurrentSelect = DateTime.Parse(keyword);
                //获取当前时间
                var dtNow = DateTime.Today;
                var html = new StringBuilder();
                CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
                var dataScheduleList = new List<int>();
                var currentSelect = Convert.ToInt32(dtCurrentSelect.ToString("yyyyMMdd"));

                //var docList = client.GetDoctorScheduleByDate(-1, DateTime.Today, DateTime.Today.AddMonths(6));
                var docList = client.GetDoctorScheduleDates(-1, DateTime.Today, DateTime.Today.AddMonths(6));
                if (docList != null && docList.Data != null && docList.Data.Count() > 0)
                {
                    //List<CRSService.DoctorSchedule> newDocList = new List<CRSService.DoctorSchedule>();
                    List<DateTime> newDocList = new List<DateTime>();
                    foreach (var docSchedule in docList.Data)
                    {
                        //加上这个太慢，所以要保证数据质量，不要有垃圾数据出现在排班表上
                        //var doctorCheck = client.getDoctor(docSchedule.docID);
                        //if (doctorCheck.Data != null)
                        //{
                            
                        //}
                        DateTime dt_Schedule = DateTime.ParseExact(docSchedule, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                        newDocList.Add(dt_Schedule);
                        //newDocList.Add(docSchedule);
                    }
                    //var maxSelectableDate = newDocList.Select(p=>p.Date).Where(p=>p >= dtNow).Max();
                    //var minSelectableDate = newDocList.Select(p => p.Date).Where(p => p >= dtNow).Min();
                    var maxSelectableDate = newDocList.Select(p => p).Where(p => p >= dtNow).Max();
                    var minSelectableDate = newDocList.Select(p => p).Where(p => p >= dtNow).Min();
                    CacheHelper.SetCache(KeyConst.CacheFirstSelectableDate, minSelectableDate.ToString("yyyy-MM-dd"));
                    CacheHelper.SetCache(KeyConst.CacheLastSelectableDate, maxSelectableDate.ToString("yyyy-MM-dd"));
                    //dataScheduleList = newDocList.Select(p => Convert.ToInt32(p.Date.ToString("yyyyMMdd"))).Where(p => p >= Convert.ToInt32(dtNow.ToString("yyyyMMdd"))).Distinct().ToList();
                    dataScheduleList = newDocList.Select(p => Convert.ToInt32(p.ToString("yyyyMMdd"))).Where(p => p >= Convert.ToInt32(dtNow.ToString("yyyyMMdd"))).Distinct().ToList();
                }
               
                for (int i = 0; i < 6; i++)
                {
                    var dtSelect = DateTime.Today.AddMonths(i);
                    html.Append(this.appendDateHtml(dtSelect, dtNow, currentSelect, dataScheduleList));
                }
                CacheHelper.SetCache(KeyConst.CacheDoctorScheduleTime, html.ToString());
                return Json(html.ToString());
            }
        }

        private string appendDateHtml(DateTime dtSelect, DateTime now, int currentSelect, List<int> dataScheduleList)
        {
            //获取当月的第一天
            var dtMonthFirst = new DateTime(dtSelect.Year, dtSelect.Month, 1);
            //获取当月的最后一天
            var dtMonthLast = dtMonthFirst.AddMonths(1).AddDays(-1);
            //当前月的天数
            var monthDays = DateTime.DaysInMonth(dtMonthFirst.Year, dtMonthFirst.Month);

            #region 当前星期的tag
            var weekFirstTarget = (int)dtMonthFirst.DayOfWeek;
            var weekLastTarget = (int)dtMonthLast.DayOfWeek;
            #endregion

            var html = new StringBuilder();
            html.AppendFormat("<section class='current_month' data-date='{0}'>", dtMonthFirst.ToString("yyyy-MM"));
            html.AppendFormat("   <p>{0}</p>", dtMonthFirst.ToString("yyyy年MM月"));
            html.Append("<div class='date_wrap'>");
            html.Append("   <table>");
            html.Append("       <tbody>");

            #region 处理

            monthDays += weekFirstTarget;
            monthDays += 7 - weekLastTarget - 1;

            int len = monthDays / 7;
            int dateIndex = 1;
            DateTime dateTemp = dtMonthFirst;

            for (int i = 0; i < len; i++)
            {
                html.Append("           <tr>");


                for (int j = 0; j < 7; j++)
                {

                    if ((i == 0 && (j < weekFirstTarget)) || ((i == len - 1) && (j > weekLastTarget)))
                    {
                        html.Append("<td style='width:14.2%'></td>");
                    }
                    else
                    {
                        var dateInt = Convert.ToInt32(dateTemp.ToString("yyyyMMdd"));
                        if (dateInt == currentSelect)
                        {
                            html.Append("      <td style='width:14.2%' data-day=" + dateIndex + " class='a'>" + dateIndex + "</td>");
                        }
                        else
                        {
                            if (dataScheduleList.Contains(dateInt))
                            {
                                if (j == 0 || j == 6)
                                {
                                    html.Append("      <td style='width:14.2%;color:red;' data-day=" + dateIndex + " class='a'>" + dateIndex + "</td>");
                                }
                                else
                                {
                                    html.Append("      <td style='width:14.2%' data-day=" + dateIndex + " class='a'>" + dateIndex + "</td>");
                                }
                            }
                            else
                            {
                                html.Append("      <td style='width:14.2%;color:#d7cfcf;' data-day=" + dateIndex + " class='over_day'>" + dateIndex + "</td>");
                            }
                        }
                        dateIndex++;
                        dateTemp = dateTemp.AddDays(1);
                    }
                }
                html.Append("           </tr>");
            }
            #endregion

            html.Append("       </tbody>");
            html.Append("   </table>");
            html.Append("   </div>");
            html.Append("</section>");
            return html.ToString();
        }

        public DateTime ChangeTime(DateTime dt, int day)
        {
            DateTime d = new DateTime();
            string ym = dt.ToString("yyyy-MM") + "-" + day;
            d = DateTime.Parse(ym);
            return d;
        }

        /// <summary>
        /// 发送模板
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="phone"></param>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <param name="docName"></param>
        /// <param name="date"></param>
        /// <param name="time"></param>
        /// <param name="target"></param>
        /// <param name="address"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult SendTmplMessage(string openid, string phone, string name, string id, string docName, string date, string time, string target, string address, int type)
        {
            openid = Base64Helper.DecodeBase64(openid);
            var templateId = "";
            //type  1 预约成功  2  加号/点诊申请成功  3 失败
            if (type == 1)
            {
                templateId = WeixinConfig.BookingSuccess;
            }
            else if (type == 2)
            {
                templateId = WeixinConfig.PlusClinicalSuccess;
            }
            else if (type == 3)
            {
                templateId = WeixinConfig.BookingFail;
            }
            if(String.IsNullOrEmpty(templateId))
            {
                return Json("");
            }
            //var accessToken = AccessTokenContainer.GetAccessToken(WeixinConfig.appID);
            var accessToken = AccessTokenContainer.TryGetAccessToken(WeixinConfig.appID, WeixinConfig.appSecret);
            var url = WeixinConfig.webDomain + "/User/MyOrders";
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            string titleinfo = "";
            if (time == null || time == "")
            {
                if ("am".Equals(target))
                {
                    time = "上午";
                }
                else if ("pm".Equals(target))
                {
                    time = "下午";
                }
                else if ("night".Equals(target))
                {
                    time = "晚上";
                }
                titleinfo = "";
            }
            var tmplData = new TestTemplateData();

            if(type ==3)
            {
                tmplData = new TestTemplateData()
                {
                    first = new TemplateDataItem("尊敬的" + (user != null ? user.username : "") + "，非常抱歉，由于专家预约已满，您的预约没有成功。\n"),
                    keyword1 = new TemplateDataItem(name),
                    keyword2 = new TemplateDataItem(docName),
                    remark = new TemplateDataItem(titleinfo + "就诊时间：" + date + " " + time + "\n\n 您可以重新选择时间或更换专家再次预约，也可以咨询我们的客服电话进行电话预约，预约电话4001-108-102。")
                };
            }
            else
            {
                tmplData = new TestTemplateData()
                {
                    first = new TemplateDataItem("尊敬的" + (user != null ? user.username : "") + "，您已成功预约帮忙医门诊，预约信息如下：\n"),
                    keyword1 = new TemplateDataItem(name),
                    keyword2 = new TemplateDataItem(phone),
                    keyword3 = new TemplateDataItem(id),
                    keyword4 = new TemplateDataItem(docName),
                    keyword5 = new TemplateDataItem(date + " " + time),
                    remark = new TemplateDataItem("就诊地点：" + address + "\n\n" + "请您核对以上信息，我们的专家助理会尽快与您联系。如您想修改预约信息，请咨询4001-108-102。感谢您对帮忙医门诊的支持，祝您早日康复！")
                };
            }
            var result = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(accessToken, openid, templateId, url, tmplData);
            LogHelper.Info("发微信模板消息接口调用结果：code:" + result.errcode + ", msg:" + result.errmsg);
            return Json(result);
        }

        [AllowAnonymous]
        public ActionResult SendWXMessage(string userOpenID, string userName,
            string patientName, string patientPhone, string patientCardNo, string docName, string reserveDate, string reserveTime, 
            string reserveTimeTarget, string hospitalAddress, int reservationType)
        {
            var templateId = "";
            //type  1 预约成功  2  加号/点诊申请成功  3 失败 4 取消
            if (reservationType == 1)
            {
                templateId = WeixinConfig.BookingSuccess;
            }
            else if (reservationType == 2)
            {
                templateId = WeixinConfig.PlusClinicalSuccess;
            }
            else if (reservationType == 3)
            {
                templateId = WeixinConfig.BookingFail;
            }
            else if (reservationType == 4)
            {
                templateId = WeixinConfig.BookingCancel;
            }
            if (String.IsNullOrEmpty(templateId))
            {
                return Json("预约类型reservationType不能为空");
            }
            //var accessToken = AccessTokenContainer.GetAccessToken(WeixinConfig.appID);
            var _appId = WeixinConfig.appID;
            var _appSecret = WeixinConfig.appSecret;
            var accessToken = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret, false);
            if (string.IsNullOrEmpty(userName))
            {
                if (!string.IsNullOrEmpty(accessToken))
                {
                    var userinfo = Senparc.Weixin.MP.AdvancedAPIs.UserApi.Info(accessToken, userOpenID);
                    userName = userinfo.nickname;
                }
            }
            var url = WeixinConfig.webDomain + "User/MyOrders";
            string titleinfo = "";
            if (reserveTime == null || reserveTime == "")
            {
                reserveTime = reserveTimeTarget;
                titleinfo = "";
            }
            var tmplData = new TestTemplateData();
            if (reservationType == 4)
            {
                tmplData = new TestTemplateData()
                {
                    first = new TemplateDataItem("尊敬的" + userName + "，您已成功取消就诊时间为" + reserveDate + " " + reserveTime + "的预约，对应的信息如下：\n"),
                    keyword1 = new TemplateDataItem(patientName),
                    keyword2 = new TemplateDataItem(docName),
                    remark = new TemplateDataItem(titleinfo + "就诊时间：" + reserveDate + " " + reserveTime + "\n\n如您需要，可以再次预约，或者咨询客服电话4001-108-102，反馈意见和建议。有您的支持，我们会做的更好！")
                };
            }
            else if (reservationType == 3)
            {
                tmplData = new TestTemplateData()
                {
                    first = new TemplateDataItem("尊敬的" + userName + "，非常抱歉，由于专家预约已满，您的预约没有成功。\n"),
                    keyword1 = new TemplateDataItem(patientName),
                    keyword2 = new TemplateDataItem(docName),
                    remark = new TemplateDataItem(titleinfo + "就诊时间：" + reserveDate + " " + reserveTime + "\n\n您可以重新选择时间或更换专家再次预约，也可以咨询我们的客服电话进行电话预约，预约电话4001-108-102。")
                };
            }
            else if (reservationType == 2)
            {
                tmplData = new TestTemplateData()
                {
                    first = new TemplateDataItem("尊敬的" + userName + "，您已成功申请帮忙医门诊，申请信息如下：\n"),
                    keyword1 = new TemplateDataItem(patientName),
                    keyword2 = new TemplateDataItem(patientPhone),
                    keyword3 = new TemplateDataItem(patientCardNo),
                    keyword4 = new TemplateDataItem(docName),
                    keyword5 = new TemplateDataItem(reserveDate + " " + reserveTime),
                    remark = new TemplateDataItem("就诊地点：" + hospitalAddress + "\n\n" + "请您核对以上信息，我们的专家助理会尽快与您联系。如您想修改预约信息，请咨询4001-108-102。感谢您对帮忙医门诊的支持，祝您早日康复！")
                };
            }
            else
            {
                tmplData = new TestTemplateData()
                {
                    first = new TemplateDataItem("尊敬的" + userName + "，您已成功预约帮忙医门诊，预约信息如下：\n"),
                    keyword1 = new TemplateDataItem(patientName),
                    keyword2 = new TemplateDataItem(patientPhone),
                    keyword3 = new TemplateDataItem(patientCardNo),
                    keyword4 = new TemplateDataItem(docName),
                    keyword5 = new TemplateDataItem(reserveDate + " " + reserveTime),
                    remark = new TemplateDataItem("就诊地点：" + hospitalAddress + "\n\n" + "请您核对以上信息，我们的专家助理会尽快与您联系。如您想修改预约信息，请咨询4001-108-102。感谢您对帮忙医门诊的支持，祝您早日康复！")
                };
            }
            var result = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(accessToken, userOpenID, templateId,  "", tmplData);
            return Json(result);
        }

        public class TestTemplateData
        {
            public TemplateDataItem first { get; set; }
            public TemplateDataItem keyword1 { get; set; }
            public TemplateDataItem keyword2 { get; set; }
            public TemplateDataItem keyword3 { get; set; }
            public TemplateDataItem keyword4 { get; set; }
            public TemplateDataItem keyword5 { get; set; }
            public TemplateDataItem remark { get; set; }
        }
    }
}
