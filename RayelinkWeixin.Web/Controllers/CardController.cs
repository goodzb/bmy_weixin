﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：CardController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-04-01
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using Senparc.Weixin;
using Senparc.Weixin.Helpers;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.Card;
using Senparc.Weixin.MP.AdvancedAPIs.GroupMessage;
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP.Containers;
using System.IO;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    [AllowAnonymous]
    public class CardController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 上传卡券Logo
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadLogo()
        {
            var postFileBase = Request.Files["file"];
            var path = Path.Combine(Request.PhysicalApplicationPath, "Temp");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var fullName = Path.Combine(path, postFileBase.FileName);
            postFileBase.SaveAs(fullName);
            var result = MediaApi.UploadImg(WeixinConfig.appID, fullName);
            if (!string.IsNullOrWhiteSpace(result.url))
            {
                return Content("卡券Logo的url地址是，请注意保存：" + result.url);
            }
            else
            {
                return Content("错误代码:" + result.errcode + " 错误信息" + result.errmsg);
            }
        }

        /// <summary>
        /// 创建卡券
        /// </summary>
        /// <param name="logo_url"></param>
        /// <param name="brand_name"></param>
        /// <param name="title"></param>
        /// <param name="sub_title"></param>
        /// <param name="color"></param>
        /// <param name="notice"></param>
        /// <param name="service_phone"></param>
        /// <param name="description"></param>
        /// <param name="fixed_begin_term"></param>
        /// <param name="fixed_term"></param>
        /// <param name="least_cost"></param>
        /// <param name="reduce_cost"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateCard(string logo_url, string brand_name, string title, string sub_title,
            string color, string notice, string service_phone, string description,
            int fixed_begin_term, int fixed_term,
            int least_cost, int reduce_cost)
        {
            var accessToken = AccessTokenContainer.TryGetAccessToken(WeixinConfig.appID, WeixinConfig.appSecret);

            #region 基础信息
            var _BaseInfo = new Card_BaseInfoBase()
                {
                    logo_url = logo_url,
                    brand_name = brand_name,
                    code_type = Card_CodeType.CODE_TYPE_TEXT,
                    title = title,
                    sub_title = sub_title,
                    color = color,
                    notice = notice,
                    service_phone = service_phone,
                    description = description,
                    date_info = new Card_BaseInfo_DateInfo()
                    {
                        type = Card_DateInfo_Type.DATE_TYPE_FIX_TERM.ToString(),
                        fixed_begin_term = fixed_begin_term,
                        fixed_term = fixed_term
                    },
                    sku = new Card_BaseInfo_Sku()
                    {
                        quantity = 0
                    },
                    use_limit = 1,
                    get_limit = 1,
                    use_custom_code = true,
                    bind_openid = false,
                    can_share = false,
                    can_give_friend = false,

                    //先注释掉 后面如果需要在进行修改
                    //url_name_type = Card_UrlNameType.URL_NAME_TYPE_RESERVATION,
                    //custom_url = "http://www.weiweihi.com",
                    //source = "大众点评",
                    //custom_url_name = "立即使用",
                    //custom_url_sub_title = "6个汉字tips",
                    //promotion_url_name = "更多优惠",
                    //promotion_url = "http://www.qq.com",
                };
            #endregion

            //代金卷的卡卷
            var data = new Card_CashData()
            {
                base_info = _BaseInfo,
                least_cost = least_cost,
                reduce_cost = reduce_cost,
            };
            var result = CardApi.CreateCard(accessToken, data);
            if (!string.IsNullOrWhiteSpace(result.card_id))
            {
                return Content("卡卷的Card ID 请注意保存:" + result.card_id);
            }
            else
            {
                return Content("错误代码:" + result.errcode + " 错误信息" + result.errmsg);
            }
        }

        /// <summary>
        /// 导入自定义卡券
        /// </summary>
        /// <param name="cardID"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CodeDeposit(string cardID, string code)
        {
            try
            {
                var result = CardApi.CodeDeposit(WeixinConfig.appID, cardID, code.Split(','));
                return Content("errcode:" + result.errcode + " errmsg:" + result.errmsg);
            }
            catch (System.Exception ex)
            {

                return Content(ex.Message);
            }

        }

        public ActionResult CheckCode(string cardID, string code)
        {
            var result = CardApi.CheckCode(WeixinConfig.appID, cardID, code.Split(','));
            var str = "";
            foreach (var item in result.exist_code)
            {
                str += item;
            }
            return Content("errcode:" + str);
        }

        /// <summary>
        /// 查看卡券数量
        /// </summary>
        /// <param name="cardID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetDepositCount(string cardID)
        {
            var result = CardApi.GetDepositCount(WeixinConfig.appID, cardID);
            return Content("数量:" + result.count);
        }

        /// <summary>
        /// 修改库存
        /// </summary>
        /// <param name="cardID"></param>
        /// <param name="increaseStockValue"></param>
        /// <param name="reduceStockValue"></param>
        /// <returns></returns>
        public ActionResult ModifyStock(string cardID, int increaseStockValue = 0, int reduceStockValue = 0)
        {
            var result = CardApi.ModifyStock(WeixinConfig.appID, cardID, increaseStockValue, reduceStockValue);
            return Content("errcode:" + result.errcode + " errmsg:" + result.errmsg);
        }

        /// <summary>
        /// 核销卡券
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public JsonResult CardConsume(string code)
        {
            var result = CardApi.CardConsume(WeixinConfig.appID, code);
            //result.errcode == Senparc.Weixin.ReturnCode.请求成功;
            return Json(result);
        }

        public ActionResult CardDelete(string cardID)
        {
            var result = CardApi.CardDelete(WeixinConfig.appID, cardID);
            return Content("errcode:" + result.errcode + " errmsg:" + result.errmsg);
        }

        public ActionResult GetHtml(string cardID)
        {
            var result = CardApi.GetHtml(WeixinConfig.appID, cardID);
            var news = new NewsModel()
            {
                author = "瑞慈",
                content = result.content,
                content_source_url = "",
                digest = "卡券",
                show_cover_pic = "0",
                thumb_media_id = "",
                title = ""
            };
            var media = MediaApi.UploadTemporaryNews(WeixinConfig.appID, 10000, news);
            var mediaId = media.media_id;
            return Content(mediaId);
            //var groupMessage = GroupMessageApi.SendGroupMessageByGroupId(WeixinConfig.appID, "", mediaId, GroupMessageType.mpnews, true);
            //return Content("errcode:" + groupMessage.errcode + "errmsg:" + groupMessage.errmsg);
        }

        public ActionResult CardBatchGet()
        {
            // var accessToken = AccessTokenContainer.GetAccessToken(WeixinConfig.appID);
            var accessToken = AccessTokenContainer.TryGetAccessToken(WeixinConfig.appID, WeixinConfig.appSecret);
            var result = CardApi.CardBatchGet(accessToken, 0, 5,KeyConst.WxCardStatus);
            var str = string.Empty;
            foreach (var item in result.card_id_list)
            {
                str += item + ",";
            }
            return Content("CardID：" + str);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardID"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public ActionResult CreateQR(string cardID, string code)
        {
            var accessTokenOrAppId = AccessTokenContainer.GetAccessToken(WeixinConfig.appID);
            var result = ApiHandlerWapper.TryCommonApi(accessToken =>
            {
                var urlFormat = string.Format("https://api.weixin.qq.com/card/qrcode/create?access_token={0}", accessToken);

                var data = new
                {
                    action_name = "QR_CARD",
                    action_info = new
                    {
                        card = new
                        {
                            card_id = cardID,
                            is_unique_code = false
                        }
                    }
                };

                var jsonSettingne = new JsonSetting(true);
                return CommonJsonSend.Send<CreateQRResultJson>(null, urlFormat, data, timeOut: Config.TIME_OUT, jsonSetting: jsonSettingne);

            }, accessTokenOrAppId);
            var qr = QrCodeApi.GetShowQrCodeUrl(result.ticket);
            return Content(qr);
        }

    }
}
