﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：WeixinPayController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-03-08
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using Senparc.Weixin;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.TenPayLibV3;
using System;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;
using System.Text;
using System.IO;

namespace RayelinkWeixin.Web.Controllers
{
    [AllowAnonymous]
    public class WeixinPayController : Controller
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            if (errors == SslPolicyErrors.None)
                return true;
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="amount"></param>
        /// <param name="docID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult Index(string orderNo, string amount, string docID, int type)
        {
            var returnUrl = WeixinConfig.webDomain + "/WeixinPay/JsApi";
            var state = Base64Helper.EncodeBase64(orderNo + "@" + amount + '@' + docID + '@' + type);
            Session[KeyConst.SessionPayState] = state;
            //var url = OAuthApi.GetAuthorizeUrl(WeixinConfig.TenPayV3_AppId, returnUrl, state, OAuthScope.snsapi_userinfo);
            returnUrl = returnUrl + "?state=" + state;
            string qu = string.Format("?type={0}&source={1}&redirect_uri={2}", "normal", "WeixinPayJsApi", returnUrl);
            string url = WeixinConfig.WxOAuthAuthorize + KeyConst.PayAppId + qu;
            return Redirect(url);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ActionResult JsApi(string state, string code, string openid)
        {
            //if (string.IsNullOrEmpty(code))
            //{
            //    return Content("您拒绝了授权！");
            //}
            //if (state != Session[KeyConst.SessionPayState].ToString())
            //{
            //    return Content("验证失败！请从正规途径进入！1001");
            //}
            //var openIdResult = OAuthApi.GetAccessToken(WeixinConfig.TenPayV3_AppId, WeixinConfig.TenPayV3_AppSecret, code);
            //if (openIdResult.errcode != ReturnCode.请求成功)
            //{
            //    return Content("错误：" + openIdResult.errmsg);
            //}
            Session[KeyConst.SessionPayState] = null;
            state = Base64Helper.DecodeBase64(state);
            var array = state.Split('@');
            string orderNo = array[0];
            var fee = Convert.ToInt32(array[1]) * 100;
            //fee = 1;
            var docID = array[2];
            var type = int.Parse(array[3]);
            //string orderNo = state;
            //var fee = 1;
            string timeStamp = "";
            string nonceStr = "";
            string paySign = "";
            WeixinPayModel payModel;
            WeChatPay(openid, orderNo, fee, docID, type, out timeStamp, out nonceStr, out paySign, out payModel);
            return View(payModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="openIdResult"></param>
        /// <param name="orderNo"></param>
        /// <param name="fee"></param>
        /// <param name="docID"></param>
        /// <param name="type"></param>
        /// <param name="timeStamp"></param>
        /// <param name="nonceStr"></param>
        /// <param name="paySign"></param>
        /// <param name="payModel"></param>
        private void WeChatPay(string openid, string orderNo, int fee, string docID, int type, out string timeStamp, out string nonceStr, out string paySign, out WeixinPayModel payModel)
        {

            //创建支付应答对象
            RequestHandler packageReqHandler = new RequestHandler(null);
            //初始化
            packageReqHandler.Init();

            timeStamp = TenPayV3Util.GetTimestamp();
            nonceStr = TenPayV3Util.GetNoncestr();

            //设置package订单参数
            packageReqHandler.SetParameter("appid", WeixinConfig.TenPayV3_AppId);		  //公众账号ID
            packageReqHandler.SetParameter("mch_id", WeixinConfig.TenPayV3_MchId);		  //商户号
            packageReqHandler.SetParameter("nonce_str", nonceStr);                    //随机字符串
            packageReqHandler.SetParameter("body", "送心意");                  //商品信息
            packageReqHandler.SetParameter("out_trade_no", orderNo);		    //商家订单号
            packageReqHandler.SetParameter("total_fee", fee.ToString());			        //商品金额,以分为单位(money * 100).ToString()
            packageReqHandler.SetParameter("spbill_create_ip", Request.UserHostAddress);   //用户的公网ip，不是商户服务器IP
            packageReqHandler.SetParameter("notify_url", WeixinConfig.TenPayV3_TenpayNotify);		    //接收财付通通知的URL
            packageReqHandler.SetParameter("trade_type", TenPayV3Type.JSAPI.ToString());	                    //交易类型
            packageReqHandler.SetParameter("openid", openid);	                    //用户的openId

            string sign = packageReqHandler.CreateMd5Sign("key", WeixinConfig.TenPayV3_Key);
            packageReqHandler.SetParameter("sign", sign);	                    //签名

            string data = packageReqHandler.ParseXML();
            var result = TenPayV3.Unifiedorder(data);
            var res = XDocument.Parse(result);
            string prepayId = res.Element("xml").Element("prepay_id").Value;

            //设置支付参数
            RequestHandler paySignReqHandler = new RequestHandler(null);
            paySignReqHandler.SetParameter("appId", WeixinConfig.TenPayV3_AppId);
            paySignReqHandler.SetParameter("timeStamp", timeStamp);
            paySignReqHandler.SetParameter("nonceStr", nonceStr);
            paySignReqHandler.SetParameter("package", string.Format("prepay_id={0}", prepayId));
            paySignReqHandler.SetParameter("signType", "MD5");
            paySign = paySignReqHandler.CreateMd5Sign("key", WeixinConfig.TenPayV3_Key);

            payModel = new WeixinPayModel()
            {
                appID = WeixinConfig.TenPayV3_AppId,
                timeStamp = timeStamp,
                nonceStr = nonceStr,
                package = string.Format("prepay_id={0}", prepayId),
                paySign = paySign,
                docID = docID,
                type = type
            };
        }

        /// <summary>
        /// 退款申请，实际退款是否完成，需要通过退款查询接口查询
        /// 具体的业务参数传值以及返回结果，按照实际业务需求，可在此基础上coding
        /// </summary>
        /// <returns></returns>
        public JsonResult Refund()
        {
            string nonceStr = TenPayV3Util.GetNoncestr();
            RequestHandler packageReqHandler = new RequestHandler(null);

            //设置package订单参数
            packageReqHandler.SetParameter("appid", WeixinConfig.TenPayV3_AppId);		  //公众账号ID
            packageReqHandler.SetParameter("mch_id", WeixinConfig.TenPayV3_MchId);		  //商户号
            packageReqHandler.SetParameter("out_trade_no", "");                 //商家订单号
            packageReqHandler.SetParameter("out_refund_no", "");                //退款订单号
            packageReqHandler.SetParameter("total_fee", "");               //总金额
            packageReqHandler.SetParameter("refund_fee", "");               //退款金额
            packageReqHandler.SetParameter("op_user_id", WeixinConfig.TenPayV3_MchId);   //操作员Id，默认就是商户号
            packageReqHandler.SetParameter("nonce_str", nonceStr);              //随机字符串
            string sign = packageReqHandler.CreateMd5Sign("key", WeixinConfig.TenPayV3_Key);
            packageReqHandler.SetParameter("sign", sign);	                    //签名
            //退款需要post的数据
            string data = packageReqHandler.ParseXML();

            //退款接口地址
            string url = "https://api.mch.weixin.qq.com/secapi/pay/refund";
            //本地或者服务器的证书位置（证书在微信支付申请成功发来的通知邮件中）
            string cert = WeixinConfig.TenPayV3_CertPath;
            //私钥（在安装证书时设置）
            string password = WeixinConfig.TenPayV3_MchId;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            //调用证书
            X509Certificate2 cer = new X509Certificate2(cert, password, X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.MachineKeySet);

            #region 发起post请求
            HttpWebRequest webrequest = (HttpWebRequest)HttpWebRequest.Create(url);
            webrequest.ClientCertificates.Add(cer);
            webrequest.Method = "post";

            byte[] postdatabyte = Encoding.UTF8.GetBytes(data);
            webrequest.ContentLength = postdatabyte.Length;
            Stream stream;
            stream = webrequest.GetRequestStream();
            stream.Write(postdatabyte, 0, postdatabyte.Length);
            stream.Close();

            HttpWebResponse httpWebResponse = (HttpWebResponse)webrequest.GetResponse();
            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
            string responseContent = streamReader.ReadToEnd();
            #endregion

            var res = XDocument.Parse(responseContent);
            string return_code = res.Element("xml").Element("return_code").Value;//调用退款申请接口的状态码 SUCCESS/FAIL
            string out_refund_no = "";
            string refund_id = "";
            string result_code = "";
            if ("SUCCESS".Equals(return_code))
            {
                //return_code为SUCCESS的时候有以下返回
                result_code = res.Element("xml").Element("result_code").Value;//提交结果，SUCCESS退款申请接收成功，FAIL 提交业务失败
                out_refund_no = res.Element("xml").Element("out_refund_no").Value;//商户退款单号
                refund_id = res.Element("xml").Element("refund_id").Value;//微信退款单号

                //可根据实际业务，做业务数据处理
                LogHelper.Info("微信退款申请---result_code:" + result_code);

            }
            return Json(new { return_code = return_code, result_code = result_code, out_refund_no = out_refund_no, refund_id = refund_id });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult notifyUrl()
        {
            ResponseHandler resHandler = new ResponseHandler(null);
            string res = null;
            resHandler.SetKey(WeixinConfig.TenPayV3_Key);
            //验证请求是否从微信发过来（安全）
            if (resHandler.IsTenpaySign())
            {
                //商户在收到后台通知后根据通知ID向财付通发起验证确认，采用后台系统调用交互模式
                string return_code = resHandler.GetParameter("return_code");

                //即时到账
                if ("SUCCESS".Equals(return_code))
                {
                    //取结果参数做业务处理
                    string out_trade_no = resHandler.GetParameter("out_trade_no");
                    LogHelper.Info("微信回调订单号----" + out_trade_no);
                    CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
                    client.paySuccessPrize(out_trade_no, DateTime.Now);
                    res = "success";
                }
            }
            else
            {
                res = "fail";
            }
            return Content(res);
        }
    }
}
