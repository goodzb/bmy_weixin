﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：DoctorController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-21
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using RayelinkWeixin.Web.Models;
using Senparc.Weixin.MP.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using RayelinkWeixin.Web.Controllers;

namespace RayelinkWeixin.Web.Areas.ReVisit.Controllers
{
    [AllowAnonymous]
    public class DoctorController : BaseController
    {
        /// <summary>
        /// 进入专家列表页面
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult DoctorList()
        {

            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            // 科室  职称列表（带缓存）
            var objDept = CacheHelper.GetCache(KeyConst.CacheAllDept);
            var objTitle = CacheHelper.GetCache(KeyConst.CacheAllTitle);
            var listDepartment = new List<SelectItemModel>();
            var listTitle = new List<SelectItemModel>();
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            // 判断缓存是否有
            if (objDept != null)
            {
                listDepartment = objDept as List<SelectItemModel>;
            }
            else
            {
                listDepartment.Add(new SelectItemModel()
                {
                    id = "",
                    name = "全部"
                });

                var returnModel = client.getDeptList();
                var deptJson = returnModel.Data.ToString();
                var deptList = JsonConvert.DeserializeObject<List<SelectItemModel>>(deptJson).OrderBy(p => p.name).ToList();
                foreach (SelectItemModel obj in deptList)
                {
                    obj.name = obj.name.Length > 8 ? obj.name.Substring(0, 8) + "..." : obj.name;
                }
                listDepartment.AddRange(deptList.ToArray());
                if (listDepartment.Count > 0)
                {
                    CacheHelper.SetCache(KeyConst.CacheAllDept, listDepartment);
                }
            }
            // 判断缓存是否有
            if (objTitle != null)
            {
                listTitle = objTitle as List<SelectItemModel>;
            }
            else
            {
                listTitle.Add(new SelectItemModel()
                {
                    id = "",
                    name = "全部"
                });
                var returnModel1 = client.getTitleList();
                var titleJson = returnModel1.Data.ToString();
                var titleList = JsonConvert.DeserializeObject<List<SelectItemModel>>(titleJson);
                listTitle.AddRange(titleList.ToArray());
                if (listTitle.Count > 0)
                {
                    CacheHelper.SetCache(KeyConst.CacheAllTitle, listTitle);
                }
            }
            ViewBag.Department = listDepartment;
            ViewBag.Title = listTitle;
            return View();
        }

        /// <summary>
        /// 获取专家列表数据
        /// </summary>
        /// <param name="deptid">部门</param>
        /// <param name="titleid">职称</param>
        /// <param name="keyword">关键字</param>
        /// <param name="currentIndex">当前页</param>
        /// <param name="page">页数</param>
        /// <returns></returns>
        public ActionResult GetDoctorList(int? deptid, int? titleid, string keyword, int currentIndex, int page = 10)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var returnModel = client.getDoctorList(deptid, titleid, keyword,"","1,2,3","", currentIndex, page);
            var doctorListJson = "";
            if (returnModel.Data != null)
            {
                doctorListJson = returnModel.Data.ToString();
            }
            if (doctorListJson == "")
            {
                doctorListJson = "[]"; //防止前台jquery ajax报parseerror 错误
            }
            return Content(doctorListJson);


        }

        /// <summary>
        /// 搜索页
        /// </summary>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult DoctorSearch()
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View();
        }

        /// <summary>
        /// 搜索空白页
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult DoctorSearchNull(string searchText)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            ViewBag.searchText = searchText;
            return View();
        }

        /// <summary>
        /// 医生详情页
        /// </summary>
        /// <param name="id">医生id</param>
        /// <param name="date">日期</param>
        /// <param name="isPrize">定位送心意</param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult DoctorSub(int id, string date = "", int isPrize =0)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            ViewBag.isPrize = isPrize;
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            DoctorModel doctorModel = new DoctorModel();
            var returnModel5 = client.getDoctor(id);
            // 获得医生详细数据
            var doctorJson = "";
            if (returnModel5.Data != null)
            {
                doctorJson = returnModel5.Data.ToString();
                if (doctorJson != "")
                {
                    doctorModel = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
                }
            }


            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    // 是否关注医生
                    var returnModel = client.isAttention(user.pid, id);
                    var attentionJson = "0";
                    if (returnModel.Data != null)
                    {
                        attentionJson = returnModel.Data.ToString();
                    }
                    if ("0".Equals(attentionJson))
                    {
                        doctorModel.isFocus = false;
                    }
                    else
                    {
                        doctorModel.isFocus = true;
                    }
                    doctorModel.isLogin = true;
                    doctorModel.userName = user.username;
                }
            }
            else
            {
                doctorModel.isFocus = false;
                doctorModel.isLogin = false;
                doctorModel.userName = AuthorizeUser.nickname();
            }
            #region 医生信息 应该从接口获取

            var pid = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    pid = user.pid;
                }
            }

            #region 我的咨询
            var returnModel6 = client.GetLatestUserConsultations(id, pid);
            var latestUserMessageJson = "";
            if (returnModel6.Data != null)
            {
                latestUserMessageJson = returnModel6.Data.ToString();
            }
            doctorModel.currentLatestConsult = new OnlineConsultModel();
            if (latestUserMessageJson != "")
            {
                var latestUserMessageModel = JsonConvert.DeserializeObject<OnlineConsultModel>(latestUserMessageJson);

                if (latestUserMessageModel != null)
                {
                    doctorModel.currentLatestConsult = latestUserMessageModel;
                    doctorModel.currentLatestConsult.CreateTime = Convert.ToDateTime(doctorModel.currentLatestConsult.CreateTime).ToString("yyyy.MM.dd");
                    doctorModel.currentLatestConsult.Content = doctorModel.currentLatestConsult.Content;
                }
            }
            
            // 其他咨询
            var returnModel7 = client.GetTopUserConsultations(id, pid, 4);
            var userMessageListJson = returnModel7.Data.ToString();
            var userMessageListModel = JsonConvert.DeserializeObject<List<OnlineConsultModel>>(userMessageListJson);
            if (userMessageListModel != null)
            {
                foreach (var OnlineConsultModel in userMessageListModel)
                {
                    OnlineConsultModel.CreateTime = Convert.ToDateTime(OnlineConsultModel.CreateTime).ToString("yyyy.MM.dd");
                    OnlineConsultModel.UserName = OnlineConsultModel.UserName.Substring(0, 1) + "**";
                    OnlineConsultModel.Content = OnlineConsultModel.Content;
                }
            }

            doctorModel.listConsult = new List<OnlineConsultModel>();
            if (userMessageListModel != null)
            {
                doctorModel.listConsult.AddRange(userMessageListModel.ToArray());
            }
            #endregion

            #region 评论
            doctorModel.listUserComments = new List<UserCommentModel>();
            var returnModel2 = client.getCommentList(id, null, pid, 1, 5);
            if (returnModel2.Data != null)
            {
                var commentJson = returnModel2.Data.ToString();
                var commentList = JsonConvert.DeserializeObject<List<UserCommentModel>>(commentJson);
                if (commentList != null)
                {
                    foreach (var UserCommentModel in commentList)
                    {
                        string addtime = UserCommentModel.add_time.ToString();
                        var adddate = long.Parse(addtime.Split('(')[1].Split(')')[0]);
                        UserCommentModel.add_time = Convert.ToDateTime(ConvertTime(adddate)).ToString("yyyy.MM.dd");
                        UserCommentModel.content = UserCommentModel.content;
                        UserCommentModel.visitResult = UserCommentModel.visitResult != null ? UserCommentModel.visitResult.TrimEnd('/') : UserCommentModel.visitResult;
                        string replytime = UserCommentModel.reply_time;
                        if (!String.IsNullOrEmpty(replytime))
                        {
                            var replydate = long.Parse(replytime.Split('(')[1].Split(')')[0]);

                            UserCommentModel.reply_time = Convert.ToDateTime(ConvertTime(replydate)).ToString("yyyy.MM.dd");
                            UserCommentModel.replyContent = UserCommentModel.replyContent;
                        }
                    }
                    doctorModel.listUserComments.AddRange(commentList.ToArray());
                }

            }
            #endregion

            #region 用户心意 4 代表妙手回春 5 救死扶伤 6 白衣天使
            doctorModel.listPrizeModel = new List<PrizeModel>();
            var returnModel3 = client.getSendPrizeList(id, pid, 1, 5);
            if (returnModel3.Data != null)
            {
                var prizeJson = returnModel3.Data.ToString();
                if (!String.IsNullOrEmpty(prizeJson))
                {
                    var prizeList = JsonConvert.DeserializeObject<List<PrizeModel>>(prizeJson);
                    if (prizeList != null && prizeList.Count > 0)
                    {
                        foreach (var PrizeModel in prizeList)
                        {
                            if (PrizeModel.PayTime != null)
                            {
                                string payTime = PrizeModel.PayTime.ToString();
                                var paydate = long.Parse(payTime.Split('(')[1].Split(')')[0]);
                                PrizeModel.PayTime = Convert.ToDateTime(ConvertTime(paydate)).ToString("yyyy.MM.dd");
                            }

                            if (!String.IsNullOrEmpty(PrizeModel.Content))
                            {
                                PrizeModel.Content = PrizeModel.Content;
                            }
                            if (PrizeModel.pid == 0 || PrizeModel.pid != pid)
                            {
                                if (!String.IsNullOrEmpty(PrizeModel.userName))
                                {
                                    if (PrizeModel.userName.Length > 1)
                                    {
                                        var name = PrizeModel.userName.Substring(0, 1);
                                        if (Regex.IsMatch(name, @"^[0-9]*$") || Regex.IsMatch(name, @"^[A-Za-z]+$") || Regex.IsMatch(name, @"^[\u4e00-\u9fa5]{0,}$"))
                                        {
                                            PrizeModel.userName = PrizeModel.userName.Length > 2 ? PrizeModel.userName.Substring(0, 2) + "*" : PrizeModel.userName;
                                        }
                                        else
                                        {
                                            PrizeModel.userName = "游客";
                                        }

                                    }
                                    else
                                    {
                                        PrizeModel.userName = PrizeModel.userName + "*";
                                    }
                                    
                                }
                                else
                                {
                                    PrizeModel.userName = "游客";
                                }
                            }

                        }
                        doctorModel.listPrizeModel.AddRange(prizeList.ToArray());
                    }

                }
            }

            #endregion

            #endregion

            #region 排班信息
            var now = DateTime.Now;
            var startDate = now.WeekStartDay();
            var endDate = now.AddYears(1).WeekEndDay();

            if (date != "")
            {
                startDate = DateTime.Parse(date).WeekStartDay();
                endDate = DateTime.Parse(date).AddYears(1).WeekEndDay();
            }
            doctorModel.WeekStartDate = startDate;
            var returnModel8 = client.GetDoctorScheduleByDate(id, startDate, endDate);
            if (returnModel8.Data != null && returnModel8.Data.Length > 0)
            {
                doctorModel.clinicID = returnModel8.Data[0].clinicID;
                doctorModel.clinicinfoID = returnModel8.Data[0].clinicinfoID;
                doctorModel.hospitalID = returnModel8.Data[0].hospitalID;
            }
            #endregion

            //查看医生线上远程
            //var returnModel9 = client.isRemote(id);
            //if (returnModel9.Data != null)
            //{
            //    var isRemote = returnModel9.Data.ToString();
            //    doctorModel.isRemote = isRemote;
            //}
            return View(doctorModel);
        }

        /// <summary>
        /// 转换2进制时间
        /// </summary>
        /// <param name="milliTime"></param>
        /// <returns></returns>
        public static DateTime ConvertTime(long milliTime)
        {
            long timeTricks = new DateTime(1970, 1, 1).Ticks + milliTime * 10000 + TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours * 3600 * (long)10000000;
            return new DateTime(timeTricks);
        }

        /// <summary>
        /// 获取医生的排班信息
        /// </summary>
        /// <param name="docID"></param>
        /// <param name="weekAdd"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetSchedulingList(int docID, int weekAdd, int pointDiagnosis)
        {
            var now = DateTime.Now;
            var nowTemp = DateTime.Now.AddDays(weekAdd * 7);
            var startDate = nowTemp.WeekStartDay();
            var endDate = nowTemp.WeekEndDay();
            var searchStartDate = startDate;
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            // 医生排班
            var returnModel = client.GetDoctorScheduleByDate(docID, startDate, endDate);
            var array = returnModel.Data;
            var listScheduleDate = new List<ScheduleDateModel>();
            // 早
            var listAMTimeDate = new List<TimeModel>();
            // 中
            var listPMTimeDate = new List<TimeModel>();
            // 晚
            var listNMTimeDate = new List<TimeModel>();
            // 控制左箭头是否隐藏
            var previousShow = false;
            //控制右箭头是否隐藏
            var nextShow = true;
            if (weekAdd > 0)
            {
                previousShow = true;
                nextShow = true;
            }

            var obj = new Object();
            // 早中晚 时间段
            List<String> listAMTime = new List<string>();
            List<String> listPMTime = new List<string>();
            List<String> listNMTime = new List<string>();
            if (array.Length > 0)
            {
                var listIntrfaceModel = array.ToList();
                var target = Convert.ToInt32(now.DayOfWeek) - 1;
                if (weekAdd > 0)
                {
                    target = -1;
                }
                for (int i = 0; i < 7; i++)
                {
                    var dateTemp = startDate.AddDays(i);
                    listScheduleDate.Add(new ScheduleDateModel()
                    {
                        title = (target == i ? "今天" : "周" + ChangeWeek(i + 1)),
                        date = dateTemp.ToString("MM-dd")
                    });
                    var scheduleModel = listIntrfaceModel.Where(p => p.Date.ToString("yyyyMMdd") == dateTemp.ToString("yyyyMMdd")).ToList();
                    if (scheduleModel.Count() <= 1)
                    {
                        listAMTime = new List<string>();
                        listPMTime = new List<string>();
                        listNMTime = new List<string>();
                    }
                    // 判断是否是当天
                    if (scheduleModel != null && scheduleModel.Count() > 0 && Convert.ToInt32(dateTemp.ToString("yyyyMMdd")) >= Convert.ToInt32(now.ToString("yyyyMMdd")))
                    {
                        var checkTime = 0;
                        if(Convert.ToInt32(dateTemp.ToString("yyyyMMdd")) == Convert.ToInt32(now.ToString("yyyyMMdd")))
                        {
                            // checkTime:  1 表示今天上午已过期  2 表示今天下午已过期
                            if (Convert.ToInt32(now.ToString("HHmm")) >= 1200 && Convert.ToInt32(now.ToString("HHmm")) <= 1800)
                            {
                                checkTime = 1;
                            }
                            else if (Convert.ToInt32(now.ToString("HHmm")) >= 1800)
                            {
                                checkTime = 2;
                            }
                            else
                            {
                                checkTime = 0;
                            }
                        }
                        // 预约数
                        var appointment = scheduleModel.Sum(p => p.appointment);
                        // 容量
                        var capacity = scheduleModel.Sum(p => p.capacity);
                        var classname = "";
                        var classtype = " ";
                        var text = "";

                        for (int j = 0; j < scheduleModel.Count(); j++)
                        {
                            var subDateModel = scheduleModel[j].subdate;
                        
                            var hospitalID = scheduleModel[j].hospitalID;
                            var clinicID = scheduleModel[j].clinicID;
                            var clinicinfoID = scheduleModel[j].clinicinfoID;
                            if (subDateModel != null)
                            {
                                // 当前时间已过上午
                                if (checkTime == 1 || checkTime == 2)
                                {
                                    listAMTimeDate.Add(new TimeModel()
                                    {
                                        className = "",
                                        text = "",
                                        date = "",
                                        time = new List<string>()
                                    });
                                }
                                else
                                {
                                    #region 上午
                                    var amList = subDateModel.Where(p => p.TimeTarget == "AM");
                                    if (amList != null && amList.Count() > 0)
                                    {
                                        var sumAppointmentAM = amList.Sum(p => p.appointment);
                                        var sumCapacityAM = amList.Sum(p => p.capacity);
                                        if (appointment == capacity)
                                        {
                                            classname = "jiahao";
                                            text = "点击加号";
                                        }
                                        var am = amList.Where(p => p.appointment < p.capacity);
                                        if (am != null && am.Count() > 0)
                                        {
                                            classname = "yuyue";
                                            text = "点击预约";
                                        }else
                                        {
                                            classname = "jiahao";
                                            text = "点击加号";
                                        }
                                        //}else if (sumAppointmentAM < sumCapacityAM)
                                        //{
                                        //    classname = "yuyue";
                                        //    text = "点击预约";
                                        //}
                                        //if (sumAppointmentAM == sumCapacityAM)
                                        //{
                                        //    classname = "jiahao";
                                        //    text = "点击加号";
                                        //}
                                        #region 时间段的判断
                                        foreach (var amtemp in amList)
                                        {
                                            var timeAM = amtemp.Time;
                                            // 预约数
                                            var appointmentAM = amtemp.appointment;
                                            // 容量
                                            var capacityAM = amtemp.capacity;

                                            if (target == i)
                                            {
                                                //判断今天的时间有没有过期
                                                if (!IsTimeExpried(now.ToString("HHmm"), amtemp.Time))
                                                {
                                                    classtype = "opacity_time";
                                                }
                                                else
                                                {
                                                    if (appointmentAM >= capacityAM)
                                                    {
                                                        classtype = "opacity_time";
                                                    }
                                                    else
                                                    {
                                                        classtype = "";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (appointmentAM >= capacityAM)
                                                {
                                                    classtype = "opacity_time";
                                                }
                                                else
                                                {
                                                    classtype = "";
                                                }
                                            }
                                            if (timeAM.Length == 9)
                                            {
                                                timeAM = "0" + timeAM.Replace("-", "-0");
                                            }
                                            if (timeAM.Length == 10)
                                            {
                                                timeAM = "0" + timeAM;
                                            }
                                            if (classtype == "")
                                            {

                                            }
                                            listAMTime.Add(timeAM + "*" + hospitalID + "*" + clinicID + "*" + clinicinfoID + "*" + classtype);
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        if (pointDiagnosis == 0)
                                        {
                                            classname = "";
                                            text = "";
                                        }
                                        else
                                        {
                                            classname = "dianzhen";
                                            text = "发起点诊";
                                        }
                                    }
                                    var dateStrAm = dateTemp.ToString("yyyy-MM-dd");
                                    var tempAm = listAMTimeDate.FirstOrDefault(p => p.date == dateStrAm);
                                    if (tempAm == null)
                                    {
                                        listAMTimeDate.Add(new TimeModel()
                                        {
                                            className = classname,
                                            text = text,
                                            date = dateStrAm,
                                            time = listAMTime
                                        });
                                    }
                                    else
                                    {
                                        listAMTime = listAMTime.OrderBy(p => p).ToList();
                                        tempAm.time = listAMTime;
                                    }
                                    #endregion
                                }

                                if (checkTime == 2)
                                {
                                    listPMTimeDate.Add(new TimeModel()
                                    {
                                        className = "",
                                        text = "",
                                        date = "",
                                        time = new List<string>()
                                    });
                                }
                                else
                                {
                                    #region 下午
                                    var pmList = subDateModel.Where(p => p.TimeTarget == "PM");
                                    if (pmList != null && pmList.Count() > 0)
                                    {
                                        #region 时间段的判断
                                        var sumAppointmentPM = pmList.Sum(p => p.appointment);
                                        var sumCapacityPM = pmList.Sum(p => p.capacity);
                                        if (appointment == capacity)
                                        {
                                            classname = "jiahao";
                                            text = "点击加号";
                                        }
                                        var pm = pmList.Where(p => p.appointment < p.capacity);
                                        if (pm != null && pm.Count() > 0)
                                        {
                                            classname = "yuyue";
                                            text = "点击预约";
                                        }
                                        else
                                        {
                                            classname = "jiahao";
                                            text = "点击加号";
                                        }
                                        //else if (sumAppointmentPM < sumCapacityPM)
                                        //{
                                        //    classname = "yuyue";
                                        //    text = "点击预约";
                                        //}
                                        //if (sumAppointmentPM == sumCapacityPM)
                                        //{
                                        //    classname = "jiahao";
                                        //    text = "点击加号";
                                        //}
                                        foreach (var pmtemp in pmList)
                                        {
                                            var timePM = pmtemp.Time;
                                            // 预约数
                                            var appointmentPM = pmtemp.appointment;
                                            // 容量
                                            var capacityPM = pmtemp.capacity;

                                            if (target == i)
                                            {
                                                //判断今天的时间有没有过期
                                                if (!IsTimeExpried(now.ToString("HHmm"), pmtemp.Time))
                                                {
                                                    classtype = "opacity_time";
                                                }
                                                else
                                                {
                                                    if (appointmentPM >= capacityPM)
                                                    {
                                                        classtype = "opacity_time";
                                                    }
                                                    else
                                                    {
                                                        classtype = "";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (appointmentPM >= capacityPM)
                                                {
                                                    classtype = "opacity_time";
                                                }
                                                else
                                                {
                                                    classtype = "";
                                                }
                                            }
                                            if (timePM.Length == 9)
                                            {
                                                timePM = "0" + timePM.Replace("-", "-0");
                                            }
                                            if (timePM.Length == 10)
                                            {
                                                timePM = "0" + timePM;
                                            }
                                            listPMTime.Add(timePM + "*" + hospitalID + "*" + clinicID + "*" + clinicinfoID + "*" + classtype);
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        if (pointDiagnosis == 0)
                                        {
                                            classname = "";
                                            text = "";
                                        }
                                        else
                                        {
                                            classname = "dianzhen";
                                            text = "发起点诊";
                                        }
                                    }
                                    var dateStrPm = dateTemp.ToString("yyyy-MM-dd");
                                    var tempPm = listPMTimeDate.FirstOrDefault(p => p.date == dateStrPm);
                                    if (tempPm == null)
                                    {
                                        listPMTimeDate.Add(new TimeModel()
                                        {
                                            className = classname,
                                            text = text,
                                            date = dateStrPm,
                                            time = listPMTime
                                        });
                                    }
                                    else
                                    {
                                        listPMTime = listPMTime.OrderBy(p => p).ToList();
                                        tempPm.time = listPMTime;
                                    }
                                    #endregion

                                }

                               
                                #region 晚上
                                var nmList = subDateModel.Where(p => p.TimeTarget == "NM");
                                if (nmList != null && nmList.Count() > 0)
                                {
                                    #region 时间段的判断
                                    var sumAppointmentNM = nmList.Sum(p => p.appointment);
                                    var sumCapacityNM = nmList.Sum(p => p.capacity);
                                    if (appointment == capacity)
                                    {
                                        classname = "jiahao";
                                        text = "点击加号";
                                    }
                                    var nm = nmList.Where(p => p.appointment < p.capacity);
                                    if (nm != null && nm.Count() > 0)
                                    {
                                        classname = "yuyue";
                                        text = "点击预约";
                                    }
                                    else
                                    {
                                        classname = "jiahao";
                                        text = "点击加号";
                                    }
                                    //else if (sumAppointmentNM < sumCapacityNM)
                                    //{
                                    //    classname = "yuyue";
                                    //    text = "点击预约";
                                    //}
                                    //if (sumAppointmentNM == sumCapacityNM)
                                    //{
                                    //    classname = "jiahao";
                                    //    text = "点击加号";
                                    //}
                                    foreach (var nmtemp in nmList)
                                    {
                                        var timeNM = nmtemp.Time;
                                        // 预约数
                                        var appointmentNM = nmtemp.appointment;
                                        // 容量
                                        var capacityNM = nmtemp.capacity;

                                        if (target == i)
                                        {
                                            //判断今天的时间有没有过期
                                            if (!IsTimeExpried(now.ToString("HHmm"), nmtemp.Time))
                                            {
                                                classtype = "opacity_time";
                                            }
                                            else
                                            {
                                                if (appointmentNM >= capacityNM)
                                                {
                                                    classtype = "opacity_time";
                                                }
                                                else
                                                {
                                                    classtype = "";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (appointmentNM >= capacityNM)
                                            {
                                                classtype = "opacity_time";
                                            }
                                            else
                                            {
                                                classtype = "";
                                            }
                                        }
                                        if (timeNM.Length == 9)
                                        {
                                            timeNM = "0" + timeNM.Replace("-", "-0");
                                        }
                                        if (timeNM.Length == 10)
                                        {
                                            timeNM = "0" + timeNM;
                                        }
                                        listNMTime.Add(timeNM + "*" + hospitalID + "*" + clinicID + "*" + clinicinfoID + "*" + classtype);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    if (pointDiagnosis == 0)
                                    {
                                        classname = "";
                                        text = "";
                                    }
                                    else
                                    {
                                        classname = "dianzhen";
                                        text = "发起点诊";
                                    }

                                }
                                var dateStrNm = dateTemp.ToString("yyyy-MM-dd");
                                var tempNm = listNMTimeDate.FirstOrDefault(p => p.date == dateStrNm);
                                if (tempNm == null)
                                {
                                    listNMTimeDate.Add(new TimeModel()
                                    {
                                        className = classname,
                                        text = text,
                                        date = dateStrNm,
                                        time = listNMTime
                                    });
                                }
                                else
                                {
                                    listNMTime = listNMTime.OrderBy(p => p).ToList();
                                    tempNm.time = listNMTime;
                                }
                                #endregion
                            }
                        }

                    }
                    else
                    {
                        // 当天之前的日期
                        int type = isTimeExpried(target, i, dateTemp.ToString("MMdd"), now);
                        // 判断是否支持点诊 0 不支持 1支持
                        if (pointDiagnosis == 0)
                        {
                            listAMTimeDate.Add(new TimeModel()
                            {
                                className = "",
                                text = "",
                                date = dateTemp.ToString("yyyy-MM-dd"),
                            });
                            listNMTimeDate.Add(new TimeModel()
                            {
                                className = "",
                                text = "",
                                date = dateTemp.ToString("yyyy-MM-dd"),
                            });
                            listPMTimeDate.Add(new TimeModel()
                            {
                                className = "",
                                text = "",
                                date = dateTemp.ToString("yyyy-MM-dd"),
                            });
                        }
                        else
                        {
                            if (type == 1)
                            {
                                //当天不存在排班
                                listAMTimeDate.Add(new TimeModel()
                                {
                                    className = "",
                                    text = "",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listNMTimeDate.Add(new TimeModel()
                                {
                                    className = "",
                                    text = "",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listPMTimeDate.Add(new TimeModel()
                                {
                                    className = "",
                                    text = "",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                            }
                            else if (type == 2)
                            {
                                //当天不存在排班,但在今天
                                listAMTimeDate.Add(new TimeModel()
                                {
                                    className = "dianzhen",
                                    text = "发起点诊",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listNMTimeDate.Add(new TimeModel()
                                {
                                    className = "dianzhen",
                                    text = "发起点诊",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listPMTimeDate.Add(new TimeModel()
                                {
                                    className = "dianzhen",
                                    text = "发起点诊",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                            }
                            else if (type == 3)
                            {
                                //将来
                                listAMTimeDate.Add(new TimeModel()
                                {
                                    className = "dianzhen",
                                    text = "发起点诊",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listNMTimeDate.Add(new TimeModel()
                                {
                                    className = "dianzhen",
                                    text = "发起点诊",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listPMTimeDate.Add(new TimeModel()
                                {
                                    className = "dianzhen",
                                    text = "发起点诊",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                            }
                            else if (type == 4)
                            {
                                //当天上午不存在排班
                                listAMTimeDate.Add(new TimeModel()
                                {
                                    className = "",
                                    text = "",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listNMTimeDate.Add(new TimeModel()
                                {
                                    className = "dianzhen",
                                    text = "发起点诊",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listPMTimeDate.Add(new TimeModel()
                                {
                                    className = "dianzhen",
                                    text = "发起点诊",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                            }
                            else
                            {
                                // 当天上中午不存在排班
                                listAMTimeDate.Add(new TimeModel()
                                {
                                    className = "",
                                    text = "",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listPMTimeDate.Add(new TimeModel()
                                {
                                    className = "",
                                    text = "",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                                listNMTimeDate.Add(new TimeModel()
                                {
                                    className = "dianzhen",
                                    text = "发起点诊",
                                    date = dateTemp.ToString("yyyy-MM-dd"),
                                });
                            }
                        }
                    }
                }
                obj = new
                {
                    previousShow = previousShow,
                    nextShow = nextShow,
                    startDate = startDate.ToString("MM月dd日"),
                    endDate = endDate.ToString("MM月dd日"),
                    scheduleDate = listScheduleDate,
                    am = listAMTimeDate,
                    pm = listPMTimeDate,
                    night = listNMTimeDate
                };
            }
            else
            {
                // 当前周无排班
                var listIntrfaceModel = array.ToList();
                var target = Convert.ToInt32(now.DayOfWeek) - 1;
                if (weekAdd > 0)
                {
                    target = -1;
                }
                var classname = "";
                var text = "";
                if (pointDiagnosis == 0)
                {
                    classname = "";
                    text = "";
                }
                else
                {
                    classname = "dianzhen";
                    text = "发起点诊";
                }
                for (int i = 0; i < 7; i++)
                {
                    listAMTime = new List<string>();
                    listPMTime = new List<string>();
                    listNMTime = new List<string>();
                    var dateTemp = startDate.AddDays(i);
                    listScheduleDate.Add(new ScheduleDateModel()
                    {
                        title = (target == i ? "今天" : "周" + ChangeWeek(i + 1)),
                        date = dateTemp.ToString("MM-dd")
                    });

                    int type = isTimeExpried(target, i, dateTemp.ToString("MMdd"), now);
                    if (type == 1)
                    {
                        //当天不存在排班
                        listAMTimeDate.Add(new TimeModel()
                        {
                            className = "",
                            text = "",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listNMTimeDate.Add(new TimeModel()
                        {
                            className = "",
                            text = "",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listPMTimeDate.Add(new TimeModel()
                        {
                            className = "",
                            text = "",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                    }
                    else if (type == 2)
                    {
                        //当天不存在排班,但在今天
                        listAMTimeDate.Add(new TimeModel()
                        {
                            className = "dianzhen",
                            text = "发起点诊",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listNMTimeDate.Add(new TimeModel()
                        {
                            className = "dianzhen",
                            text = "发起点诊",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listPMTimeDate.Add(new TimeModel()
                        {
                            className = "dianzhen",
                            text = "发起点诊",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                    }
                    else if (type == 3)
                    {
                        //将来
                        listAMTimeDate.Add(new TimeModel()
                        {
                            className = "dianzhen",
                            text = "发起点诊",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listNMTimeDate.Add(new TimeModel()
                        {
                            className = "dianzhen",
                            text = "发起点诊",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listPMTimeDate.Add(new TimeModel()
                        {
                            className = "dianzhen",
                            text = "发起点诊",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                    }
                    else if (type == 4)
                    {
                        listAMTimeDate.Add(new TimeModel()
                        {
                            className = "",
                            text = "",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listNMTimeDate.Add(new TimeModel()
                        {
                            className = "dianzhen",
                            text = "发起点诊",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listPMTimeDate.Add(new TimeModel()
                        {
                            className = "dianzhen",
                            text = "发起点诊",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                    }
                    else
                    {
                        listAMTimeDate.Add(new TimeModel()
                        {
                            className = "",
                            text = "",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listPMTimeDate.Add(new TimeModel()
                        {
                            className = "",
                            text = "",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                        listNMTimeDate.Add(new TimeModel()
                        {
                            className = "dianzhen",
                            text = "发起点诊",
                            date = dateTemp.ToString("yyyy-MM-dd"),
                        });
                    }


                }
                obj = new
                {
                    previousShow = previousShow,
                    nextShow = nextShow,
                    startDate = startDate.ToString("MM月dd日"),
                    endDate = endDate.ToString("MM月dd日"),
                    scheduleDate = listScheduleDate,
                    am = listAMTimeDate,
                    pm = listPMTimeDate,
                    night = listNMTimeDate
                };
            }
            return Json(obj);
        }

        public String ChangeWeek(int i)
        {
            string week = "";
            switch (i)
            {
                case 1: week = "一"; break;
                case 2: week = "二"; break;
                case 3: week = "三"; break;
                case 4: week = "四"; break;
                case 5: week = "五"; break;
                case 6: week = "六"; break;
                case 7: week = "日"; break;
            }
            return week;
        }


        private bool IsTimeExpried(string nowtime, string compareTimeRank)
        {
            string[] strTime = compareTimeRank.Split('-');
            int startTime = Int32.Parse(strTime[0].Replace(":", ""));
            if (Int32.Parse(nowtime) < startTime)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="i"></param>
        /// <param name="time">上午判断传1200 下面判断传1800</param>
        /// <param name="now"></param>
        /// <returns></returns>
        private int isTimeExpried(int target, int i, string time, DateTime now)
        {
            int w = 0;
            if (target <= i)
            {
                if (target == i)
                {
                    //表示今天
                    if (Convert.ToInt32(now.ToString("HHmm")) <= 1200)
                    {
                        // 当前时间在上午
                        return 2;
                    }
                    else if (Convert.ToInt32(now.ToString("HHmm")) <= 1800)
                    {
                        // 当前时间在下午
                        return 4;
                    }
                    else
                    {
                        // 当前时间在晚上
                        return 5;
                    }
                }
                else
                {
                    //表示将来
                    return 3;
                }

            }
            if (target > i)
            {
                //表示已经过期了
                return w = 1;
            }
            return w;
        }

        /// <summary>
        /// 获取排班时间表
        /// </summary>
        /// <param name="docID"></param>
        /// <param name="dateTarget">am shangwu pm 下午 night 晚上</param>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetScheduleTime(int docID, string dateTarget, string date, string type, string time)
        {
            //10:00-11:00*
            string[] timeTypeList = time.Split(',');
            var timeList = timeTypeList.ToList();
            StringBuilder html = new StringBuilder();
            // 每3个时间段换行
            if (timeList.Count % 3 != 0)
            {
                var addIndex = 3 - timeList.Count % 3;
                for (int i = 0; i < addIndex; i++)
                {
                    timeList.Add("");
                }
            }
            for (int i = 0; i < timeList.Count; i++)
            {
                if (i % 3 == 0)
                {
                    html.Append("<tr>");
                }
                if (string.IsNullOrEmpty(timeList[i]))
                {
                    html.Append("<td style='background:#ffffff'></td>");
                }
                else
                {
                    html.Append("<td data-type='" + type + "' data-time='" + timeList[i].Split('*')[0] + "' data-remoteHospId='" + timeList[i].Split('*')[1] + "' data-clinicID='" + timeList[i].Split('*')[2] + "' data-clinicinfoID='" + timeList[i].Split('*')[3] + "' class='" + timeList[i].Split('*')[4] + "'>" + timeList[i].Split('*')[0] + "</td>");
                }
                if ((i + 1) % 3 == 0)
                {
                    html.Append("</tr>");
                }
            }
            var jsonStr = new StringBuilder();
            jsonStr.Append(html);
            return Json(jsonStr.ToString());
        }

        /// <summary>
        /// 验证并记录url
        /// </summary>
        /// <returns></returns>
        public JsonResult ValidAuthorizeRememberUrl(int id)
        {
            if (User == null || User.Identity == null || !User.Identity.IsAuthenticated)
            {
                Session[KeyConst.SessionBindUserBackUrl] = Url.Action("DoctorSub", "Doctor") + "?id=" + id;
                return Json(new { success = false, message = "" });
            }
            else
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user == null)
                {
                    Session[KeyConst.SessionBindUserBackUrl] = Url.Action("DoctorSub", "Doctor") + "?id=" + id;
                    return Json(new { success = false, message = "" });
                }
            }


            return Json(new { success = true, message = "" });
        }

        /// <summary>
        /// 关注医生
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult Attention(int id)
        {
            if (User == null || User.Identity == null || !User.Identity.IsAuthenticated)
            {
                Session[KeyConst.SessionBindUserBackUrl] = Url.Action("DoctorSub", "Doctor") + "?id=" + id + "&type=1"; // type =1 表示从我的关注跳转
                return Json(new { success = false, message = "" });
            }
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            if (user != null)
            {
                var returnModel = client.attentionDoctor(user.pid, id, 1);
                return Json(new { success = returnModel.Success, message = "关注成功" });
            }
            else
            {
                Session[KeyConst.SessionBindUserBackUrl] = Url.Action("DoctorSub", "Doctor") + "?id=" + id + "&type=1"; // type =1 表示从我的关注跳转
                return Json(new { success = false, message = "未绑定的用户" });
            }
        }

        /// <summary>
        /// 取消关注医生
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult CancelAttention(int id)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            if (user != null)
            {
                var returnModel = client.attentionDoctor(user.pid, id, 0);
                return Json(new { success = returnModel.Success, message = "取消关注成功" });
            }
            else
            {
                return Json(new { success = false, message = "未绑定的用户" });
            }
        }

        /// <summary>
        /// 进入送心意页
        /// </summary>
        /// <param name="docID"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public ActionResult SendPrize(int docID)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var model = new DoctorModel();
            var docEnt = client.getDoctor(docID);
            if (docEnt != null && docEnt.Data != null)
            {
                var jsonString = docEnt.Data.ToString();
                model = JsonConvert.DeserializeObject<DoctorModel>(jsonString);
                if (model.image != "" && model.image != null)
                {
                    model.image = WeixinConfig.crsUrl + model.image;
                }
                else
                {
                    model.image = "";
                }

            }
            return View(model);
        }

        /// <summary>
        /// 发起支付
        /// </summary>
        /// <param name="docID"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [AllowAnonymous]
        public JsonResult goToPay(int docID, int level, string content)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var pid = 0;
            var username = "";
            var amount = 0;
            switch (level)
            {
                case 1:
                    amount = 10;
                    break;
                case 2:
                    amount = 5;
                    break;
                case 3:
                    amount = 1;
                    break;
                default:
                    break;
            }
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
                if (user != null)
                {
                    pid = user.pid;
                    username = user.username;
                }
                else
                {
                    var nickname = Session[KeyConst.SessionUserNickName];
                    if (nickname != null)
                    {
                        username = nickname.ToString();
                    }
                }
            }
            else
            {
                var nickname = Session[KeyConst.SessionUserNickName];
                if (nickname != null)
                {
                    username = nickname.ToString();
                }
            }
            if (pid == 0)
            {
                username = username.Length >= 2 ? username.Substring(0, 2) + "*" : username;
            }
            var returnModel = client.sendPrize(pid, docID, amount, level, "微信", username, content);
            if (returnModel.Success)
            {
                return Json(new { success = true, data = returnModel.Data.ToString(), amount = amount });
            }
            return Json(new { success = false });
        }


        /// <summary>
        /// 其他人在线咨询
        /// </summary>
        [WeixinSpeficAuthorize]
        public ActionResult OnlineMessages(int id, int docid, int myId)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            var model = new DoctorModel();
            model.UserModel = new UserModel();
            model.image = "../images/user_icon.png";
            //model.UserModel.UserName = name;
            model.id = docid;
            //model.name = docName;
            model.UserModel.id = id;
            model.OnlineConsultModel = new OnlineConsultModel();
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var returnModel = client.Consultations_Get(id).Data;
            if (returnModel != null)
            {
                model.OnlineConsultModel.Content = returnModel.Content;
                ViewBag.myId = myId;
                model.OnlineConsultModel.CreateTime = Convert.ToDateTime(returnModel.CreateTime).ToString("yyyy.MM.dd");
                model.UserModel.UserName = returnModel.UserName.Substring(0, 1) + "**";
            }
            return View(model);
        }
        /// 其他人咨询详情
        /// </summary>
        [WeixinSpeficAuthorize]
        public ActionResult getMessage(int docID, string openid, int id)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var consultationReplyListJson = client.GetConsultationReplyList(id);
            var consultationReplyListModel = JsonConvert.DeserializeObject<List<OnlineConsultModel>>(consultationReplyListJson.Data);
            var returnModel = client.Consultations_Get(id).Data;
            string docName = "";
            DoctorModel doctorModel1 = new DoctorModel();
            var returnDoctor = client.getDoctor(docID);

            var doctorJson = "";
            if (returnDoctor.Data != null)
            {
                doctorJson = returnDoctor.Data.ToString();
                if (doctorJson != "")
                {
                    doctorModel1 = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);

                    docName = doctorModel1.name;
                }
            }


            List<DoctorModel> doc = new List<DoctorModel>();
            StringBuilder html = new StringBuilder();
            if (consultationReplyListModel != null)
            {

                for (int i = 0; i < consultationReplyListModel.Count; i++)
                {
                    if (consultationReplyListModel[i].type == 0)
                    {
                        html.Append("<p class='d_message'><span class='d_name'>" + docName + "医生</span>回复：<span class='m_info'>" + consultationReplyListModel[i].Content + "<span class='m_d'>" + Convert.ToDateTime(consultationReplyListModel[i].CreateTime).ToString("yyyy.MM.dd") + "</span></span></p>");
                    }
                    else
                    {
                        html.Append("<p class='u_message'><span class='u_name'>" + returnModel.UserName.Substring(0, 1) + "**" + "</span>回复医生：<span class='m_info'>" + consultationReplyListModel[i].Content + "<span class='m_d'>" + Convert.ToDateTime(consultationReplyListModel[i].CreateTime).ToString("yyyy.MM.dd") + "</span></span></p>");
                    }
                }
            }

            return Content(html.ToString());
        }

        /// <summary>
        /// 在线问题
        /// </summary>
        [WeixinSpeficAuthorize]
        public ActionResult OnlineQuestion(int docID)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            var user = AuthorizeUser.GetAuthorizeUser(this.User.Identity.Name);
            var model = new DoctorModel();
            model.id = docID;
            OrderModel orm = new OrderModel();
            if (user != null)
            {
                CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
                var returnModel = client.FamilyMember_GetList(user.pid);
                var familyMemberArry = returnModel.Data;

                var familyMemberModelList = familyMemberArry.FirstOrDefault(p => p.IsDefault == true);
                //获取默认就诊人 姓名性别
                if (familyMemberModelList != null)
                {
                    orm.name = familyMemberModelList.FamilyName;
                    if (!String.IsNullOrEmpty(familyMemberModelList.IDCardNo))
                    {
                        orm.id = familyMemberModelList.IDCardNo;
                        int sex = int.Parse(orm.id.Substring(16, 1));
                        if (sex % 2 == 0)
                        {
                            orm.sex = "女";
                        }
                        else
                        {
                            orm.sex = "男";
                        }
                        int year = int.Parse(orm.id.Substring(6, 4));
                        int nowyear = int.Parse(DateTime.Now.Year.ToString());
                        orm.age = (nowyear - year).ToString();

                    }
                    else
                    {
                        orm.sex = "男";
                        orm.age = "";
                    }

                }
                else
                {
                    // 若就诊人无默认就诊人 则取crm 当前绑定用户信息，
                    CRMWebService.ServiceForWebSoapClient crmClient = new CRMWebService.ServiceForWebSoapClient();
                    var name = "";
                    var phone = "";
                    var cardId = "";
                    var returnUserinfo = crmClient.CRM_WEB_GetMemberModelByPid(user.pid);
                    if (returnUserinfo != null)
                    {
                        name = returnUserinfo.Name;
                        phone = returnUserinfo.Mobile;
                        if (returnUserinfo.MemberCertList != null)
                        {
                            var cardInfo = returnUserinfo.MemberCertList.Where(p => p.CertTypeCode == "CID").OrderByDescending(p => p.UpdateTime).FirstOrDefault(); // 身份证号 
                            if (cardInfo != null)
                            {
                                cardId = cardInfo.CertNo;
                            }
                        }
                    }
                    orm.name = name;
                    if (!String.IsNullOrEmpty(cardId))
                    {
                        orm.id = cardId;
                        int sex = int.Parse(orm.id.Substring(16, 1));
                        if (sex % 2 == 0)
                        {
                            orm.sex = "女";
                        }
                        else
                        {
                            orm.sex = "男";
                        }
                        int year = int.Parse(orm.id.Substring(6, 4));
                        int nowyear = int.Parse(DateTime.Now.Year.ToString());
                        orm.age = (nowyear - year).ToString();

                    }
                    else
                    {
                        orm.sex = "男";
                        orm.age = "";
                    }
                }
            }
            else
            {
                orm.sex = "男";
                orm.age = "";
            }
            model.orderModel = orm;
            return View(model);
        }

        /// <summary>
        /// 添加咨询
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userSex"></param>
        /// <param name="age"></param>
        /// <param name="content"></param>
        /// <param name="docid"></param>
        /// <param name="picList"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult AddConsult(string userName, string userSex, string age, string content, int docid, string picList)
        {
            var user = AuthorizeUser.GetAuthorizeUser(this.User.Identity.Name);
            int pid = 0;
            if (user != null)
            {
                pid = user.pid;
            }
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var returnConsult = client.Consultations_Add(pid, userName, userSex, int.Parse(age), docid, content, "微信");
            if (returnConsult.Success)
            {
                // type 1 咨询 2预约
                var returnMoble = client.addSeekDetail(pid, picList.TrimEnd(','), returnConsult.Data.ToString(), 1, "微信");
                return Json(new { success = true, data = returnConsult.Data });
            }
            else
            {
                return Json(new { success = false, message = returnConsult.Message });
            }
        }

        /// <summary>
        /// 我的咨询
        /// </summary>
        /// <param name="id"></param>
        /// <param name="docid"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult MyOnlineMessages(int id, int docid)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            var user = AuthorizeUser.GetAuthorizeUser(this.User.Identity.Name);
            var model = new DoctorModel();

            model.UserModel = new UserModel();
            model.image = AuthorizeUser.headimgurl();
            model.id = docid;
            //model.name = docName;
            model.UserModel.id = id;
            model.OnlineConsultModel = new OnlineConsultModel();
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var returnModel = client.Consultations_Get(id).Data;
            if (returnModel != null)
            {
                model.OnlineConsultModel.Content = returnModel.Content;
                model.OnlineConsultModel.CreateTime = Convert.ToDateTime(returnModel.CreateTime).ToString("yyyy.MM.dd");

                model.UserModel.UserName = returnModel.UserName;
            }
            return View(model);
        }
        /// 获得我的在线咨询
        /// </summary>
        [WeixinSpeficAuthorize]
        public ActionResult getMyonlineMessage(int docID, string openid, int id)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var consultationReplyListJson = client.GetConsultationReplyList(id);
            var consultationReplyListModel = JsonConvert.DeserializeObject<List<OnlineConsultModel>>(consultationReplyListJson.Data);

            string docName = "";
            DoctorModel doctorModel1 = new DoctorModel();
            var returnDoctor = client.getDoctor(docID);

            var doctorJson = "";
            if (returnDoctor.Data != null)
            {
                doctorJson = returnDoctor.Data.ToString();
                if (doctorJson != "")
                {
                    doctorModel1 = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
                    docName = doctorModel1.name;
                }
            }

            List<DoctorModel> doc = new List<DoctorModel>();
            var returnModel = client.Consultations_Get(id).Data;
            StringBuilder html = new StringBuilder();
            if (consultationReplyListModel != null)
            {
                for (int i = 0; i < consultationReplyListModel.Count; i++)
                {
                    if (consultationReplyListModel[i].type == 0)
                    {
                        html.Append("<p class='d_message'><span class='d_name'>" + docName + "医生</span>回复：<span class='m_info'>" + consultationReplyListModel[i].Content + "<span class='m_d'>" + Convert.ToDateTime(consultationReplyListModel[i].CreateTime).ToString("yyyy.MM.dd") + "</span></span></p>");
                    }
                    else
                    {
                        html.Append("<p class='u_message'><span class='u_name'>" + returnModel.UserName + "</span>回复医生：<span class='m_info'>" + consultationReplyListModel[i].Content + "<span class='m_d'>" + Convert.ToDateTime(consultationReplyListModel[i].CreateTime).ToString("yyyy.MM.dd") + "</span></span></p>");
                    }
                }
            }

            return Content(html.ToString());
        }


        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult sendMessage(int id, string param)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var returnModel = client.ConsultationReply_Add(id, param, "微信");
            return Json(new { success = returnModel.Success, data = returnModel.Data });
        }

        ///<summary>
        /// 下载图片
        /// </summary>
        public JsonResult downloadPics(string serverId, string openid)
        {
            var _appId = WeixinConfig.appID;
            var _appSecret = WeixinConfig.appSecret;
            // Senparc.Weixin.MP.Containers.AccessTokenContainer.TryGetAccessToken(SiteConfig.appID, SiteConfig.appSecret);
            WeixinDownloadHelper.WeixinAsyncDownload(AccessTokenContainer.TryGetAccessToken(_appId, _appSecret), serverId);
            return Json("");
        }
        /// <summary>
        /// 所有咨询
        /// </summary>
        /// <param name="num"></param>
        /// <param name="docId"></param>
        /// <param name="docName"></param>
        /// <param name="myId"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult AllMessages(int num, int docId, string docName, int myId)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            DoctorModel doctorModel = new DoctorModel();
            var returnModel = client.getDoctor(docId);

            var doctorJson = "";
            if (returnModel.Data != null)
            {
                doctorJson = returnModel.Data.ToString();
                if (doctorJson != "")
                {
                    doctorModel = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
                }
            }

            ViewBag.num = num;
            ViewBag.docId = docId;
            ViewBag.docName = doctorModel.name.Length > 4 ? doctorModel.name.Substring(0, 4) + "..." : doctorModel.name;
            ViewBag.myId = myId;
            return View();
        }
        /// <summary>
        /// 获得所有咨询
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="page"></param>
        /// <param name="docId"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult GetAllMessages(int pageIndex, int page, int docId)
        {

            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            DoctorModel doctorModel = new DoctorModel();
            var user = AuthorizeUser.GetAuthorizeUser(this.User.Identity.Name);
            int pid = 0;
            if (user != null)
            {
                pid = user.pid;
            }
            StringBuilder html = new StringBuilder();
            if (user != null && pageIndex == 1)
            {
                #region 我的咨询
                var returnModel = client.GetLatestUserConsultations(docId, user.pid);
                var latestUserMessageJson = returnModel.Data.ToString();
                if (!String.IsNullOrEmpty(latestUserMessageJson))
                {
                    var latestUserMessageModel = JsonConvert.DeserializeObject<OnlineConsultModel>(latestUserMessageJson);
                    doctorModel.currentLatestConsult = new OnlineConsultModel();
                    if (latestUserMessageModel != null)
                    {
                        doctorModel.currentLatestConsult = latestUserMessageModel;
                        doctorModel.currentLatestConsult.CreateTime = Convert.ToDateTime(doctorModel.currentLatestConsult.CreateTime).ToString("yyyy.MM.dd");
                    }


                    if (doctorModel.currentLatestConsult != null)
                    {
                        html.Append("<section class='summary_section top_online_menssage ' id='online_menssage'>");
                        html.Append("<header class='summary_section_header'>");
                        html.Append("<p class='summary_section_title first_message' id='message'>我的咨询<a href='javascript:;' class='read_more'></a></p>");
                        html.Append("</header>");
                        html.Append(" <input id='hidConsultId' type='hidden' value='" + doctorModel.currentLatestConsult.id + "' />");
                        html.Append(" <input id='hidContent' type='hidden' value='" + doctorModel.currentLatestConsult.Content + "' />");
                        html.Append(" <input id='hidTime' type='hidden' value='" + doctorModel.currentLatestConsult.CreateTime + "' />");
                        html.Append("<article class='summary_content first_message firstmessage '>");
                        html.Append("<p>" + doctorModel.currentLatestConsult.Content + "</p>");
                        html.Append("</article>");
                        html.Append(" <p class='my_info'><span class='my_name'>" + doctorModel.currentLatestConsult.UserName + "</span><span class='info_date'>（" + doctorModel.currentLatestConsult.CreateTime + "）</span></p>");
                        html.Append("</section>");
                    }
                }
                #endregion
            }
            // 其他咨询
            var returnModel1 = client.GetUserConsultationsList(docId, "", pageIndex, page);
            var userMessageListJson = "";
            if(returnModel1 !=null && returnModel1.Data !=null)
            {
                userMessageListJson = returnModel1.Data.ToString();
            }
            var userMessageListModel = JsonConvert.DeserializeObject<List<OnlineConsultModel>>(userMessageListJson);
            if (userMessageListModel != null)
            {
                userMessageListModel = userMessageListModel.Where(p => p.pid != pid).ToList();
                foreach (var OnlineConsultModel in userMessageListModel)
                {
                    OnlineConsultModel.CreateTime = Convert.ToDateTime(OnlineConsultModel.CreateTime).ToString("yyyy.MM.dd");
                    OnlineConsultModel.UserName = OnlineConsultModel.UserName.Substring(0, 1) + "**";
                }
                doctorModel.listConsult = new List<OnlineConsultModel>();
                if (userMessageListModel != null)
                {
                    doctorModel.listConsult.AddRange(userMessageListModel.ToArray());
                }
                if (doctorModel.listConsult != null && doctorModel.listConsult.Count > 0)
                {
                    foreach (var item in doctorModel.listConsult)
                    {
                        html.Append("<section class='summary_section online_content' id='other_message' data-consultid=" + item.id + " data-content=" + item.Content + " data-time=" + item.CreateTime + " data-name=" + item.UserName + ">");
                        html.Append("<header class='summary_section_header'>");
                        html.Append("<p class='summary_section_title'></p>");
                        html.Append("</header>");
                        html.Append("<article class='summary_content '>");
                        html.Append("<p class='summary'>" + item.Content + "</p>");
                        html.Append(" </article>");
                        html.Append("<p class='my_info'><span class='my_name'>" + item.UserName + "</span><span class='info_date'>（" + item.CreateTime + "）</span></p>");
                        html.Append("</section>");

                    }
                }

            }
            return Content(html.ToString());
        }

        /// <summary>
        /// 所有评价
        /// </summary>
        /// <param name="num"></param>
        /// <param name="docId"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult AllCommons(int num, int docId)
        {

            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            DoctorModel doctorModel = new DoctorModel();
            var returnModel = client.getDoctor(docId);
            var doctorJson = "";
            if (returnModel.Data != null)
            {
                doctorJson = returnModel.Data.ToString();
                if (doctorJson != "")
                {
                    doctorModel = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
                }
            }
            ViewBag.num = num;
            ViewBag.docId = docId;
            ViewBag.docName = doctorModel.name.Length > 4 ? doctorModel.name.Substring(0, 4) + "..." : doctorModel.name;
            return View();
        }

        /// <summary>
        /// 获得所有评价
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="page"></param>
        /// <param name="docId"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult GetAllCommons(int pageIndex, int page, int docId)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            string docName = "";
            string image = "";
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            DoctorModel doctorModel1 = new DoctorModel();
            var returnDoctor = client.getDoctor(docId);

            var doctorJson = "";
            if (returnDoctor.Data != null)
            {
                doctorJson = returnDoctor.Data.ToString();
                if (doctorJson != "")
                {
                    doctorModel1 = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);

                    docName = doctorModel1.name;
                    image = doctorModel1.image;
                }
            }


            DoctorModel doctorModel = new DoctorModel();
            StringBuilder html = new StringBuilder();
            var user = AuthorizeUser.GetAuthorizeUser(this.User.Identity.Name);
            int pid = 0;
            if (user != null)
            {
                pid = user.pid;
            }
            #region 评论
            doctorModel.listUserComments = new List<UserCommentModel>();
            var returnModel = client.getCommentList(docId, null, pid, pageIndex, page);
            if (returnModel.Data != null)
            {
                var commentJson = returnModel.Data.ToString();
                var commentList = JsonConvert.DeserializeObject<List<UserCommentModel>>(commentJson);
                if (commentList != null)
                {
                    foreach (var UserCommentModel in commentList)
                    {
                        string addtime = UserCommentModel.add_time.ToString();
                        var adddate = long.Parse(addtime.Split('(')[1].Split(')')[0]);
                        UserCommentModel.add_time = Convert.ToDateTime(ConvertTime(adddate)).ToString("yyyy.MM.dd");
                        UserCommentModel.visitResult = UserCommentModel.visitResult != null ? UserCommentModel.visitResult.TrimEnd('/') : UserCommentModel.visitResult;
                        string replytime = UserCommentModel.reply_time;
                        if (!String.IsNullOrEmpty(replytime))
                        {
                            var replydate = long.Parse(replytime.Split('(')[1].Split(')')[0]);

                            UserCommentModel.reply_time = Convert.ToDateTime(ConvertTime(replydate)).ToString("yyyy.MM.dd");
                        }
                    }
                    doctorModel.listUserComments.AddRange(commentList.ToArray());
                }

            }
            var imgurl = RayelinkWeixin.Const.Config.WeixinConfig.crsUrl;
            #endregion
            if (doctorModel.listUserComments != null && doctorModel.listUserComments.Count > 0)
            {
                foreach (var item in doctorModel.listUserComments)
                {
                    html.Append("<section class='summary_section top_online_menssage user_messages'>");
                    html.Append("<header class='summary_section_header' style='margin-top:5px; margin-bottom:2px;'>");
                    html.Append("<p class='summary_section_title first_message comment_title online_sub' style='top:-5px'>"+item.visitResult+"&nbsp;</p>");
                    html.Append("<span class='user_rate'>");
                    if (item.stars == 0)
                    {
                        html.Append("<img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 1)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 2)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 3)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 4)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 5)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''>");
                    }
                    html.Append("</span>");
                    html.Append("</header>");
                    html.Append("<article class='summary_content first_message user_comment'>");
                    if (item.content == "" || item.content == null)
                    {
                        html.Append(" <p style='font-size: 14px;'>该用户未发表文字评价</p>");
                    }
                    else
                    {
                        html.Append(" <p style='font-size: 14px;'>" + item.content + "</p>");
                    }
                    html.Append("</article>");
                    html.Append("<p class='my_info'><span class='my_name'>" + item.userName + "</span><span class='info_date'>（" + item.add_time + "）</span></p>");
                    if (item.replyContent != null)
                    {
                        html.Append("<div class='d_comment'>");
                        html.Append("<img src='" +imgurl+image + "' class='d_icon' alt='' style='width:60px;height:60px' />");
                        html.Append("<div class='d_comment_wrap'>");
                        html.Append("<span class='d_name'>" + docName + "医生回复：</span>");
                        html.Append("<span class='d_com_content'>" + item.replyContent + "</span>");
                        html.Append("<p class='d_com_date'>" + item.reply_time + "</p>");
                        html.Append("</div>");
                        html.Append("</div>");

                    }
                    html.Append("</section>");

                }
            }
            return Content(html.ToString());
        }

        /// <summary>
        /// 所有送心意
        /// </summary>
        /// <param name="num"></param>
        /// <param name="docId"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult AllPrizes(int num, int docId)
        {

            var url = Request.Url.ToString().Split('#')[0];
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            DoctorModel doctorModel = new DoctorModel();
            var returnModel = client.getDoctor(docId);
            var doctorJson = "";
            if (returnModel.Data != null)
            {
                doctorJson = returnModel.Data.ToString();
                if (doctorJson != "")
                {
                    doctorModel = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
                }
            }
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            ViewBag.docName = doctorModel.name.Length > 4 ? doctorModel.name.Substring(0, 4) + "..." : doctorModel.name;
            ViewBag.num = num;
            ViewBag.docId = docId;
            return View();
        }

        /// <summary>
        /// 获得所有送心意
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="page"></param>
        /// <param name="docId"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult GetAllPrizes(int pageIndex, int page, int docId)
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);

            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            DoctorModel doctorModel = new DoctorModel();
            var user = AuthorizeUser.GetAuthorizeUser(this.User.Identity.Name);

            StringBuilder html = new StringBuilder();

            int pid = 0;
            if (user != null)
            {
                pid = user.pid;
            }
            #region 送心意
            doctorModel.listPrizeModel = new List<PrizeModel>();
            var returnModel = client.getSendPrizeList(docId, pid, pageIndex, page);

            if (returnModel.Data != null)
            {
                var commentJson = returnModel.Data.ToString();
                var prizeList = JsonConvert.DeserializeObject<List<PrizeModel>>(commentJson);
                if (prizeList != null)
                {
                    foreach (var prizeModel in prizeList)
                    {
                        if (prizeModel.PayTime != null)
                        {
                            string addtime = prizeModel.PayTime.ToString();
                            var adddate = long.Parse(addtime.Split('(')[1].Split(')')[0]);
                            prizeModel.PayTime = Convert.ToDateTime(ConvertTime(adddate)).ToString("yyyy.MM.dd");
                        }
                        if (prizeModel.pid == 0 || prizeModel.pid != pid)
                        {
                            if (!String.IsNullOrEmpty(prizeModel.userName))
                            {
                                if (prizeModel.userName.Length > 1)
                                {
                                    var name = prizeModel.userName.Substring(0, 1);
                                    if (Regex.IsMatch(name, @"^[0-9]*$") || Regex.IsMatch(name, @"^[A-Za-z]+$") || Regex.IsMatch(name, @"^[\u4e00-\u9fa5]{0,}$"))
                                    {
                                        prizeModel.userName = prizeModel.userName.Length > 2 ? prizeModel.userName.Substring(0, 2) + "*" : prizeModel.userName;
                                    }
                                    else
                                    {
                                        prizeModel.userName = "游客";
                                    }

                                }
                                else
                                {
                                    prizeModel.userName = prizeModel.userName + "*";
                                }
                            }
                            else
                            {
                                prizeModel.userName = "游客";
                            }
                        }
                    }
                    doctorModel.listPrizeModel.AddRange(prizeList.ToArray());
                }

            }
            #endregion
            if (doctorModel.listPrizeModel != null && doctorModel.listPrizeModel.Count > 0)
            {

                foreach (var item in doctorModel.listPrizeModel)
                {
                    html.Append("<section class='summary_section allPrizes'>");
                    html.Append("<header class='summary_section_header'>");
                    html.Append("<p class='summary_section_title'></p>");
                    html.Append("</header>");
                    html.Append(" <article class='summary_content'>");
                    html.Append("<figure class='flag_icon'><img src='../images/doctor_sub/flag" + (item.Level) + ".png' alt='' /></figure>");
                    html.Append("<div class='xinyi_content'>");
                    if (item.Content == null)
                    {
                        if (item.Level == 1)
                        {
                            html.Append("<p class='ganxie'>妙手回春</p>");
                        }
                        else if (item.Level == 2)
                        {
                            html.Append("<p class='ganxie'>救死扶伤</p>");
                        }
                        else
                        {
                            html.Append("<p class='ganxie'>白衣天使</p>");
                        }
                    }
                    else
                    {
                        html.Append("<p class='ganxie'>" + item.Content + "</p>");
                    }
                    html.Append("<div class='xinyi_user_info'>");
                    html.Append("<span class='xinyi_user_name'>" + item.userName + "</span>");
                    html.Append("<span class='xinyi_date'>（" + item.PayTime + "）</span>");
                    html.Append("</div>");
                    html.Append("</div>");
                    html.Append("</article>");
                    html.Append("</section>");
                    html.Append("<br/>");
                }
            }

            return Content(html.ToString());
        }

    }
}
