﻿/*----------------------------------------------------------------
// Copyright (C) 2017瑞一互联
// 版权所有。
// 文   件   名：FreeDiagnosisController
// 文件功能描述：义诊意向单
//
// 
// 创 建 人：liuwq
// 创建日期：2017-02-13
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using RayelinkWeixin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace RayelinkWeixin.Web.Controllers.NewFreeDiagnosis
{
    public class SenEmailJsonResult
    {
        public bool success { get; set; }
        public string info { get; set; }
    } 
    [AllowAnonymous]
    public class NewFreeDiagnosisController : Controller
    {
        //
        // GET: /FreeDiagnosis/
        [WeixinSpeficAuthorize]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 预约页
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        [Authorize]
        public ActionResult Order(int id)
        {

            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            DoctorModel doctorModel = new DoctorModel();
            var returnModel5 = client.getDoctor(id);
            // 获得医生详细数据
            var doctorJson = "";
            if (returnModel5.Data != null)
            {
                doctorJson = returnModel5.Data.ToString();
                if (doctorJson != "")
                {
                    doctorModel = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
                }
            }


            //if (User.Identity.IsAuthenticated)
            //{
            //    var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            //    if (user != null)
            //    {
            //        // 是否关注医生
            //        var returnModel = client.isAttention(user.pid, id);
            //        var attentionJson = "0";
            //        if (returnModel.Data != null)
            //        {
            //            attentionJson = returnModel.Data.ToString();
            //        }
            //        if ("0".Equals(attentionJson))
            //        {
            //            doctorModel.isFocus = false;
            //        }
            //        else
            //        {
            //            doctorModel.isFocus = true;
            //        }
            //        doctorModel.isLogin = true;
            //        doctorModel.userName = user.username;
            //    }
            //}
            //else
            //{
            //    doctorModel.isFocus = false;
            //    doctorModel.isLogin = false;
            //    doctorModel.userName = AuthorizeUser.nickname();
            //}

            var url = Request.Url.ToString().Split('#').Length > 0 ? Request.Url.ToString().Split('#')[0].Replace(":" + WeixinConfig.port, "") : Request.Url.ToString().Replace(":" + WeixinConfig.port, "");
            Session[KeyConst.SessionOrderUrl] = Base64Helper.EncodeBase64(url);
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            return View(doctorModel);
        }
        [WeixinSpeficAuthorize]
        ///<summary>
        /// 添加预约
        ///</summary>
        public ActionResult ResvServerAdd(string hospitalName, string doctorName, string userName, string phone, string id, string description, string pictures)
        {
            string strPic = "";
            try
            {
                int str = int.Parse(id.Substring(id.Length - 2, 1));
                string sex = "";
                if (str % 2 == 0)
                {
                    sex = "女";
                }
                else
                {
                    sex = "男";
                }

                if (pictures != "")
                {
                    string[] arrlist = pictures.Split(',');
                    for (int i = 0; i < arrlist.Length; i++)
                    {
                        if (arrlist[i] != "")
                        {
                            strPic += "图片" + (i + 1).ToString() + ": " + arrlist[i] + " \r\n";
                        }
                    }

                }


                string contentHtml = string.Format("联系人姓名:{0}\r\n性别:{1}\r\n联系人电话:{2}\r\n身份证号:{3}\r\n就诊医院:{4}\r\n预约医生:{7}\r\n症状:{5}\r\n{6}\r\n", userName, sex, phone, id, hospitalName, description, strPic, doctorName);
                string toAccounts = System.Web.Configuration.WebConfigurationManager.AppSettings["newfreeDiagnosisEmailAccounts"];
                SMS_Service.WebServiceClient client = new SMS_Service.WebServiceClient();
                string[] accountlist = toAccounts.Split(',');
                string result = string.Empty;
                bool sendResult = false;

                foreach (string item in accountlist)
                {
                    result = client.sendMail(item, "就医服务-新义诊意向单", contentHtml);
                    SenEmailJsonResult sendResultModel = JsonConvert.DeserializeObject<SenEmailJsonResult>(result);
                    if (sendResultModel.success)
                    {
                        sendResult = true;
                    }
                }
                if (sendResult)
                {
                    return Json(new { success = true, data = GetOrderCount() });
                }
                else
                {
                    LogHelper.Error("下单信息及错误内容：\r\n预约项目：就医服务-新义诊意向单\r\n 联系人姓名：" + userName + "\r\n联系人电话：" + phone + "\r\n身份证号:" + id + "\r\n就诊医院:" + hospitalName + "\r\n症状:" + description + "\r\n" + strPic + "\r\n预约医生:" + doctorName + "\r\n代码错误信息：发送邮件失败");
                    return Json(new { success = false, data = "发送邮件失败" });
                }


            }
            catch (Exception ex)
            {

                LogHelper.Error("下单信息及错误内容：\r\n预约项目：就医服务-新义诊意向单\r\n联系人姓名：" + userName + "\r\n联系人电话：" + phone + "\r\n身份证号:" + id + "\r\n就诊医院:" + hospitalName + "\r\n症状:" + description + "\r\n" + strPic + "\r\n预约医生:" + doctorName + "\r\n代码错误信息：" + ex.ToString());
                return Json(new { success = false, data = "接口错误" });
            }

        }

        public string GetOrderCount()
        {

            var filename = System.Web.Hosting.HostingEnvironment.MapPath("/XMLForActivities/NewFreeDiagnosis.xml");
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(filename);
                //List<Banner> listNull = new List<Banner>();
                XmlNodeList xnList = xmlDoc.SelectNodes("//listitem");
                if (xnList.Count > 0)
                {
                    var count = xnList[0].SelectSingleNode("OrderCount").InnerText;
                    if (!string.IsNullOrEmpty(count))
                    {
                        xnList[0].SelectSingleNode("OrderCount").InnerText = (Convert.ToInt32(count) + 1).ToString();
                    }
                    xmlDoc.Save(filename);
                    LogHelper.Error("\r\n义诊统计数量：" + xnList[0].SelectSingleNode("OrderCount").InnerText + "\r\n");
                    return xnList[0].SelectSingleNode("OrderCount").InnerText;
                }
                else
                {
                    return "9";
                }

            }
            catch (Exception e)
            {
                //显示错误信息
                LogHelper.Error("\r\n义诊统计错误：" + e.ToString() + "\r\n");
                return "8";
            }
        }

    }
}
