﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：ExampleController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-03-09
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Web.Filters;
using Rotativa;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using Senparc.Weixin.MP.CommonAPIs;
using System.IO;
using System.Web.Mvc;
using RayelinkWeixin.Web.Models;
using Senparc.Weixin.MP.Containers;

namespace RayelinkWeixin.Web.Controllers
{
    [AllowAnonymous]
    public class ExampleController : Controller
    {
        //
        // GET: /Example/

        //把PDF展示在网页上面
        public ActionResult Index()
        {
            return new ViewAsPdf();
        }

        public ActionResult CreatePDF()
        {
            ViewBag.Html = "<h2>接口获取的html</h2>";
            return View("Index");
        }

        public ActionResult SavePdf()
        {
            var path = WeixinDownloadHelper.GetSavePDFPath();
            var binary = new ActionAsPdf("CreatePDF", new { name = "Giorgio" })
            {
                FileName = "Test.pdf"
            }.BuildPdf(this.ControllerContext);
            var kbSize = binary.Length / 1024m;
            var mSize = kbSize / 1024m;
            using (FileStream fs = new FileStream(path + "/Test.pdf", FileMode.Create))
            {
                fs.Write(binary, 0, binary.Length);
                fs.Flush();
                fs.Close();
            }
            return View();
        }

        [WeixinSpeficAuthorize]
        [WebAuthorize]
        public ActionResult TestTmplMessage()
        {
            return View();
        }

        /// <summary>
        /// 发送模板
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="phone"></param>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <param name="docName"></param>
        /// <param name="date"></param>
        /// <param name="time"></param>
        /// <param name="target"></param>
        /// <param name="address"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult SendTmplMessage(string openid, string phone, string name, string id, string docName, string date, string time, string target, string address,int type)
        {
            openid = Base64Helper.DecodeBase64(openid);
            var templateId = "";
            //type  1 预约成功  2  加号/点诊申请成功  3 失败
            if(type ==1)
            {
                templateId = WeixinConfig.BookingSuccess;
            }else if(type ==2)
            {
                templateId = WeixinConfig.PlusClinicalSuccess;
            }else if(type ==3)
            {
                templateId = WeixinConfig.BookingFail;
            }

            //var accessToken = AccessTokenContainer.GetAccessToken(WeixinConfig.appID);
            var accessToken = AccessTokenContainer.TryGetAccessToken(WeixinConfig.appID, WeixinConfig.appSecret);
            var url = WeixinConfig.webDomain;
            var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);
            if (time == null || time == "")
            {
                if ("am".Equals(target))
                {
                    time = "上午";
                }
                else if ("pm".Equals(target))
                {
                    time = "下午";
                }
                else if ("night".Equals(target))
                {
                    time = "晚上";
                }
            }
            var tmplData = new TestTemplateData()
            {
                username = new TemplateDataItem((user != null ?user.username:"")),
                name = new TemplateDataItem(name),
                phone = new TemplateDataItem(phone),
                id = new TemplateDataItem(id),
                docName = new TemplateDataItem(docName),
                date = new TemplateDataItem(date),
                time = new TemplateDataItem(time),
                address = new TemplateDataItem(address)
            };
            var result = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(accessToken, openid, templateId,  url, tmplData);
            return Json(result.errcode);
        }

        /// <summary>
        /// 
        /// </summary>
        public class TestTemplateData
        {
            public TemplateDataItem username { get; set; }
            public TemplateDataItem docName { get; set; }
            public TemplateDataItem name { get; set; }
            public TemplateDataItem phone { get; set; }
            public TemplateDataItem id { get; set; }
            public TemplateDataItem address { get; set; }
            public TemplateDataItem date { get; set; }
            public TemplateDataItem time { get; set; }
            public TemplateDataItem target { get; set; }


        }

    }
}
