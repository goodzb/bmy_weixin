﻿using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Data.PO.Extend;
using RayelinkWeixin.Data.Repository;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using RayelinkWeixin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    [ReferralAttribute]
    public class ReferralController : Controller
    {

        // GET: /Referral/
        //public RefUserModel user = new RefUserModel();

        //public RefUserModel user = new RefUserModel()
        //{
        //    Id = 22,
        //    ClientId = 11,
        //    Name = "蓝精灵",
        //    OpenId = "jlsdfklaskdjflasdjflasdkf",
        //    Phone = "18649215876",
        //    Type = 0
        //};


        #region 绑定
        /// <summary>
        /// 绑定页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Bind()
        {
            //LogHelper.Debug(Request["openid"]);
            //LogHelper.Debug(Session[KeyConst.SessionUserHeadImageUrl]);
            //LogHelper.Debug(Session[KeyConst.SessionUserNickName]);
            //LogHelper.Debug(JsonConvert.SerializeObject( Session["jsModel"]));
            //Session[KeyConst.SessionUserHeadImageUrl] = "";
            //Session[KeyConst.SessionUserNickName] = "";//测试数据

            var openid = Request["openid"];
            if (openid.Length >= 32 && (openid.Length % 4) == 0)
            {
                openid = Base64Helper.DecodeBase64(openid);
            }
            ViewBag.openid = openid;
            return View();
        }

        /// <summary>
        /// 保存绑定信息
        /// </summary>
        public ActionResult SaveBind(string name, string phone, string openid, bool force = false)
        {
            string res = HttpHelper.PostReq(WeixinConfig.userApi + "/APIAccount/WechatBindUser", "UserAccount=" + phone + "&ThirdPartyID=" + openid + "&DoctorName=" + name + "&ChangeBind=" + force, "application/x-www-form-urlencoded");
            BindRes bres = JsonConvert.DeserializeObject<BindRes>(res);

            if (bres.Result == 0)
            {
                RefUserModel user = new RefUserModel();
                //绑定成功
                user.Id = bres.UserId;
                user.Name = name;
                user.Phone = phone;
                user.OpenId = openid;
                user.Type = bres.UType;
                user.ClientId = bres.HospitalId;
                Session["user"] = user;
                if (bres.UType == 2)
                {
                    //如果绑定的是护士，还要把该护士所在诊所对应的转诊单上的护士openid都更新为当前护士
                    ReferralRepository.Instance.UpdateNurseOpenId(bres.HospitalId, openid);
                }
                else
                {
                    //更新医生姓名
                    ReferralRepository.Instance.Update("CreaterName='"+name+ "',CreaterOpenId='"+openid+"'", "CreaterPhone='"+phone+"'");
                }
                return Json(new {code=1,content="" });
            }
            else if (bres.Result == 1)
            {
                //已绑定
                return Json(new { code = -1, content =bres.DoctorName });
            }
            else
            {
                //绑定失败
                return Json(new { code = 0, content = bres.DoctorName });
            }
        }

        /// <summary>
        /// 进行绑定操作
        /// </summary>
        ///[WeixinSpeficAuthorize]
        public ActionResult SendCode(string phone)
        {
            return Json(0);
        }
        
        #endregion

        #region 我的转诊
        public ActionResult MyReferral()
        {
            RefUserModel user = Session["user"] as RefUserModel;
            LogHelper.Info("openid:" + user.OpenId);

            bool isNurse = true;//是否是护士
            ViewBag.Title = "转诊单";

            if (user.Type == 0)
            {
                isNurse = false;
                ViewBag.Title = "我的转诊";
            }
            ViewBag.isNurse = isNurse;
            ViewBag.openid = user.OpenId;

            return View();
        }

        /// <summary>
        /// 获取未读数据条数信息
        /// </summary>
        /// <returns></returns>
        public ActionResult Unread()
        {
            RefUserModel user = Session["user"] as RefUserModel;
            List<int> unread = new List<int>();
            try
            {
                LogHelper.Debug("UserType："+user.Type+"UserId："+user.Id);
                if (user.Type == 0)
                {
                    unread = ReferralRepository.Instance.GetReferralUnreadCount(0, user.Id);
                }
                else
                {
                    unread = ReferralRepository.Instance.GetReferralUnreadCount(2, user.ClientId);
                }

            }
            catch (Exception e)
            {
                LogHelper.Error(e);
            }

            return Json(unread);
        }

        /// <summary>
        /// 分页获取转诊信息
        /// </summary>
        public ActionResult GetReferralPage(int pagesize, int pageindex, string status)
        {
            /*
             * 根据用户角色加载不同数据
             * 医生只加载他发起的转诊
             * 如果是护士，只加载发往当前医院的转诊信息
             * 
             * */
            RefUserModel user = Session["user"] as RefUserModel;
            List<ReferralExtend> list = new List<ReferralExtend>();
            try
            {
                if (user.Type == 0)
                {
                    list = ReferralRepository.Instance.FindReferralByPage(pagesize, pageindex, "Status='" + status + "' and CreaterId=" + user.Id, "[CreateTime] ASC");
                }
                else
                {
                    list = ReferralRepository.Instance.FindReferralByPage(pagesize, pageindex, "Status='" + status + "' and ClientId=" + user.ClientId, "[CreateTime] ASC");
                    list.ForEach(r=> {
                        r.IsRead = 1;
                    });
                }
            }
            catch (Exception e)
            {
                LogHelper.Error(e);
            }
            return Json(list);
        }
        #endregion

        #region 新建转诊
        public ActionResult NewReferral()
        {
            //Session["user"] = new RefUserModel {OpenId="123456"};//测试数据
            RefUserModel user = Session["user"] as RefUserModel;
            ViewBag.openid = user.OpenId;
            return View();
        }

        /// <summary>
        /// 添加转诊单
        /// </summary>
        public ActionResult AddReferral(string name, string phone, string desc, int hospitalId, int docId, string imgPaths, string docBrif, string hospitalName)
        {
            LogHelper.Debug(string.Format("传入参数：{0}-{1}-{2}-{3}-{4}-{5}-{6}", name, phone, desc, hospitalId, docId, docBrif, hospitalName));
            LogHelper.Debug(imgPaths);
            string[] paths = null;
            if (!string.IsNullOrEmpty(imgPaths))
            {
                paths = imgPaths.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            }
            try
            {
                //获取对应诊所护士的openid，推送微信消息
                string res = HttpHelper.PostReq(WeixinConfig.userApi + "/APIAccount/GetNurse", "{'hospitalId':" + hospitalId + "}");
                NurseId nurse = JsonConvert.DeserializeObject<NurseId>(res);

                RefUserModel user = Session["user"] as RefUserModel;
                ReferralExtend refer = new ReferralExtend
                {
                    CreaterId = user.Id,
                    CreaterName = user.Name,
                    CreaterPhone = user.Phone,
                    CreateTime = DateTime.Now,
                    Desc = desc,
                    DocId = docId,
                    ClientId = hospitalId,
                    IsDel = false,
                    Name = name,
                    Phone = phone,
                    Status = "101",
                    DocBrif = docBrif,
                    ClientName = hospitalName,
                    CreaterOpenId = user.OpenId,
                    NurseOpenId = ""
                };
                if (!string.IsNullOrEmpty(nurse.OpenID))
                {
                    refer.NurseOpenId = nurse.OpenID;
                }

                int newid = ReferralRepository.Instance.AddReferral(refer);

                if (newid <= 0) return Json(0);

                if (paths != null)
                {
                    foreach (string item in paths)
                    {
                        ReferralImgExtend img = new ReferralImgExtend
                        {
                            Path = item,
                            RefId = newid
                        };
                        ReferralRepository.Instance.AddImg(img);
                    }
                }

                return Json(newid);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
                return Json(0);
            }

        }
        #endregion

        #region 选择医生
        public ActionResult ChooseDoc()
        {
            RefUserModel user = Session["user"] as RefUserModel;
            ViewBag.openid = user.OpenId;
            ViewBag.crsUrl = WeixinConfig.crsUrl;
            return View();
        }

        public ActionResult FindDocList(int pagesize, int pagenumber, string key, int depId, int hospitalId)
        {
            List<ReferralDocExtend> docs = new List<ReferralDocExtend>();
            try
            {
                docs = HospitalRepository.Instance.FindDocByPage(pagesize, pagenumber, key, depId, hospitalId);
            }
            catch (Exception e)
            {
                LogHelper.Error(e);
            }

            return Json(docs);
        }
        #endregion

        #region 选择医院
        public ActionResult ChooseHospital()
        {
            return View();
        }

        public ActionResult GetHospital(int cityId)
        {
            List<ReferralHospitalExtend> list = HospitalRepository.Instance.FindHospitalByCityId(cityId);
            return Json(list);
        }
        #endregion

        #region 搜索医生
        public ActionResult SearchDoc()
        {
            return View();
        }

        /// <summary>
        /// 加载所有医院列表
        /// </summary>
        /// <returns></returns>
        public ActionResult FindAllHospital()
        {
            List<ReferralHospitalExtend>  hospitals = HospitalRepository.Instance.FindAllHospital();
                hospitals.Insert(0, new ReferralHospitalExtend { Id = 0, Name = "全部" });
            return Json(hospitals);
        }

        /// <summary>
        /// 加载科室列表，二级
        /// </summary>
        /// <returns></returns>
        public ActionResult FindAllDepts()
        {
            List<ReferralDeptExtend> depts = HospitalRepository.Instance.FindDeptOrg();
            return Json(depts);
        }
        #endregion

        #region 创建档案
        public ActionResult NewDocument(int id)
        {
            ReferralExtend refer = ReferralRepository.Instance.FindReferralDeailById(id);
            refer.CreateTimeStr = refer.CreateTime.ToString("yyyy-MM-dd HH:mm");
            string refStr = JsonConvert.SerializeObject(refer);
            ViewBag.json = refStr;
            RefUserModel user = Session["user"] as RefUserModel;
            ViewBag.openid = user.OpenId;
            return View();
        }

        public ActionResult SaveDocument(ReferralExtend refer)
        {
            try
            {
                //先判断是否已经创建档案
                var dbRefer = ReferralRepository.Instance.FindReferralById(refer.Id);
                if (dbRefer == null)
                {
                    return Json("转诊单不存在！");
                }
                if (dbRefer.Status != "101")
                {
                    return Json("转诊单已经建档！");
                }

                RefUserModel user = Session["user"] as RefUserModel;
                refer.Status = "102";
                //先把之前的图片都删除
                ReferralRepository.Instance.DelImg(refer.Id);

                //再重新保存新的所有图片路径
                if (!string.IsNullOrEmpty(refer.ImgsStr))
                {
                    LogHelper.Debug("建档上传的照片：" + refer.ImgsStr);
                    string[] imgs = refer.ImgsStr.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string item in imgs)
                    {
                        ReferralImgExtend img = new ReferralImgExtend
                        {
                            Path = item,
                            RefId = refer.Id
                        };
                        ReferralRepository.Instance.AddImg(img);
                    }
                }
                //最后更新对象
                int r = ReferralRepository.Instance.UpdateReferral(refer, user.Id);
                return Json(r);
            }
            catch (Exception e)
            {
                LogHelper.Error(e);
                return Json(0);
            }

        }
        public ActionResult DelImg(string path)
        {
            try
            {
                int r = ReferralRepository.Instance.DelImg(path);
                return Json(r);
            }
            catch (Exception e)
            {
                LogHelper.Error(e);
                return Json(0);
            }
        }
        #endregion

        #region 详情
        public ActionResult ReferDetail(int id)
        {
            RefUserModel user = Session["user"] as RefUserModel;
            ReferralExtend refer = ReferralRepository.Instance.FindReferralDeailById(id);
            if (user.Type == 0)
            {
                ReferralRepository.Instance.AddRead(new ReferralReadExtend
                {
                    UserId = user.Id,
                    RefId = refer.Id
                });
            }

            string isShowDocu = "0";//是否显示页面建档
            if (user.Type == 2 && refer.Status == "101")
            {
                isShowDocu = "1";
            }
            ViewBag.isShowDocu = isShowDocu;

            string isShowCancel = "0";//是否显示取消按钮
            if (user.Type == 2 && refer.Status == "101")
            {
                isShowCancel = "1";
            }
            ViewBag.isShowCancel = isShowCancel;

            ViewBag.isNurse = user.Type == 2;

            refer.Desc = string.IsNullOrEmpty(refer.Desc) ? "无" : refer.Desc;
            refer.DocBrif = string.IsNullOrEmpty(refer.DocBrif) ? "无" : refer.DocBrif;

            return View(refer);
        }

        /// <summary>
        /// 取消转诊单
        /// </summary>
        public ActionResult Cancel(int id)
        {
            ReferralExtend refer = ReferralRepository.Instance.FindReferralById(id);
            refer.Status = "121";

            RefUserModel user = Session["user"] as RefUserModel;
            int r = ReferralRepository.Instance.UpdateReferral(refer, user.Id,DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            return Json(r);
        }
        #endregion

    }

    #region 构造数据结构
    public class ResModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public UserInfoModel User { get; set; }
    }
    public class UserInfoModel
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string ThirdPartyID { get; set; }
        /// <summary>
        /// 0是地方医生，1是中央医生，2是护士
        /// </summary>
        public int UserType { get; set; }
        public UserInfo UserInfo { get; set; }
    }
    public class UserInfo
    {
        public string DoctorName { get; set; }
        public int HospitalId { get; set; }
    }

    public class BindRes
    {
        public int Result { get; set; }
        public string Message { get; set; }
        public int UserId { get; set; }
        public string Phone { get; set; }
        public string UserAccount { get; set; }
        public string ThirdPartyID { get; set; }
        public int UType { get; set; }
        public int HospitalId { get; set; }
        public string DoctorName { get; set; }
    }

    public class NurseId
    {
        public int UserID { get; set; }
        public string OpenID { get; set; }
    }
    #endregion
}
