﻿using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    [System.Runtime.InteropServices.GuidAttribute("E9CB3F28-5B6C-409A-89C3-A95CBB753BA0")]
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        /// <summary>
        /// 微官网
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            var objDoctorListJson = CacheHelper.GetCache(KeyConst.CacheDoctorList);
            var doctorModelList = new List<DoctorModel>();
            if (objDoctorListJson != null)
            {
                doctorModelList = JsonConvert.DeserializeObject<List<DoctorModel>>(objDoctorListJson.ToString());
            }
            else
            {
                // 医生列表
                var returnModel = client.getDoctorList(null, null, "","","1,2,3","", 1, int.MaxValue);
                if (returnModel != null && returnModel.Data != null)
                {
                    var doctorListJson = returnModel.Data.ToString();
                    doctorModelList = JsonConvert.DeserializeObject<List<DoctorModel>>(doctorListJson);
                    CacheHelper.SetCache(KeyConst.CacheDoctorList, doctorListJson);
                }
                else
                {
                    if (returnModel == null)
                    {
                        LogHelper.Info("returnModel没有数据");
                    }
                    else
                    {
                        if (returnModel.Data == null)
                        {
                            LogHelper.Info("returnModel.Data没有数据");
                        }
                    }
                }
            }

            DoctorModel doctorModel = new DoctorModel();
            if (doctorModelList != null)
            {
                string[] name = new string[doctorModelList.Count];
                string[] comment = new string[doctorModelList.Count];
                string[] zhiwei = new string[doctorModelList.Count];
                string[] hop = new string[doctorModelList.Count];
                int[] id = new int[doctorModelList.Count];
                for (int i = 0; i < doctorModelList.Count; i++)
                {
                    doctorModel = doctorModelList[i];
                    name[i] = doctorModel.name;
                    comment[i] = doctorModel.comments + "条评价";
                    zhiwei[i] = doctorModel.depName2 + "-" + doctorModel.titleName;
                    hop[i] = doctorModel.affiliatedHospital;
                    id[i] = doctorModel.id;
                }
                var docotrGroup = new
                {
                    name = name,
                    comment = comment,
                    zhiwei = zhiwei,
                    hop = hop,
                    id = id
                };
                ViewBag.docotrGroup = JsonConvert.SerializeObject(docotrGroup);
                ViewBag.doctorModelList = doctorModelList;
                if (doctorModelList.Count > 0)
                {
                    doctorModel = new DoctorModel();
                    doctorModel.name = doctorModelList[0].name;
                    doctorModel.comments = doctorModelList[0].comments;
                    doctorModel.deptName = doctorModelList[0].depName2;
                    doctorModel.titleName = doctorModelList[0].titleName;
                    doctorModel.affiliatedHospital = doctorModelList[0].affiliatedHospital;
                    doctorModel.id = doctorModelList[0].id;
                }
                
            }
            // 新闻
            var newsJson = client.GetNewsList(5, 1, "status=0").Data;
            if (newsJson != null)
            {
                var newsString = client.GetNewsList(5, 1, "status=0").Data.ToString();
                var newsList = JsonConvert.DeserializeObject<NewListModel>(newsString);
                if (newsList != null)
                {
                    doctorModel.newsList = newsList.Table;
                }
            }
            return View(doctorModel);
        }


        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// 新闻详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult NewsPage(int id)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var returnModel = client.GetNewsModel(id);
            var newsJson = returnModel.Data.ToString();
            var newsModel = JsonConvert.DeserializeObject<NewListModel>(newsJson);
            newsModel.format_time = newsModel.add_time.ToString("yyyy-MM-dd");
            var urlimg =  RayelinkWeixin.Const.Config.WeixinConfig.crsUrl;
            newsModel.content = newsModel.content.Replace("src=\"/upload", "src=\"" + urlimg + "upload");
            return View(newsModel);
        }

        public ActionResult NewsList()
        {
            return View();
        }

        /// <summary>
        /// 所有新闻
        /// </summary>
        /// <param name="page"></param>
        /// <param name="currentIndex"></param>
        /// <returns></returns>
        public ActionResult GetNewsList(int page, int currentIndex)
        {
            var objNewsList = CacheHelper.GetCache(KeyConst.CacheNewsList + currentIndex);
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            List<NewsModel> newsList2 = new List<NewsModel>();
            if (objNewsList != null)
            {
                newsList2 = objNewsList as List<NewsModel>;
            }
            else
            {
                var newsString = client.GetNewsList(page, currentIndex, "status=0").Data.ToString();
                var newsList = JsonConvert.DeserializeObject<NewListModel>(newsString);
                if (newsList.Table.Rows.Count > 0)
                {
                    foreach (DataRow news in newsList.Table.Rows)
                    {
                        NewsModel news2 = new NewsModel();
                        news2.add_time = DateTime.Parse(news["add_time"].ToString()).ToString("yyyy-MM-dd");
                        news2.update_time = news["update_time"] != null && news["update_time"].ToString() != "" ? DateTime.Parse(news["update_time"].ToString()).ToString("yyyy-MM-dd") : null;
                        news2.zhaiyao = news["zhaiyao"].ToString();
                        news2.id = int.Parse(news["id"].ToString());
                        news2.title = news["title"].ToString(); 
                        news2.img_url = news["img_url"] != null && news["img_url"].ToString() != "" ? news["img_url"].ToString() : null;
                        newsList2.Add(news2);
                    }
                    CacheHelper.SetCache(KeyConst.CacheNewsList, newsList2);
                }
            }
            var json = JsonConvert.SerializeObject(newsList2);
            return Json(json);
        }


    }
}
