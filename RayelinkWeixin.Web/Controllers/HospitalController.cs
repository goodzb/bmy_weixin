﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：HospitalController
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-26
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using RayelinkWeixin.Web.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Linq;

namespace RayelinkWeixin.Web.Controllers
{
    [AllowAnonymous]
    public class HospitalController : Controller
    {
        /// <summary>
        /// 诊所列表页
        /// </summary>
        /// <param name="docID"></param>
        /// <param name="date"></param>
        /// <param name="time"></param>
        /// <param name="type"></param>
        /// <param name="target"></param>
        /// <param name="remoteHospId"></param>
        /// <param name="clinicID"></param>
        /// <param name="clinicinfoID"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult HospitalList(int docID, string date, string time, string type, string target, int remoteHospId = 0, int clinicID = 0, int clinicinfoID = 0)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            var model = new HospitalListModel();
            model.listCity = new List<SelectItemModel>();

            model.clinicID = clinicID;
            model.clinicinfoID = clinicinfoID;
            model.remoteHospId = remoteHospId;
            model.docID = docID;
            model.date = date;
            model.time = time;
            model.type = type;
            model.target = target;
            #region 选择城市

            var objCiry = CacheHelper.GetCache(KeyConst.CacheAllCity);
            if (objCiry != null)
            {
                model.listCity = objCiry as List<SelectItemModel>;
            }
            else
            {
                model.listCity.Add(new SelectItemModel() { id = "-1", name = "全部" });
                var returnModel = client.getCityList();
                var cityJson = returnModel.Data.ToString();
                var cityList = JsonConvert.DeserializeObject<List<SelectItemModel>>(cityJson);
                model.listCity.AddRange(cityList.ToArray());
                if (model.listCity.Count > 0)
                {
                    CacheHelper.SetCache(KeyConst.CacheAllCity, model.listCity);
                }
            }
            #endregion
            return View(model);
        }

        /// <summary>
        /// 获得诊所列表
        /// </summary>
        /// <param name="cityID"></param>
        /// <param name="type"></param>
        /// <param name="searchText"></param>
        /// <param name="coordinate"></param>
        /// <param name="remoteHospId"></param>
        /// <param name="currentIndex"></param>
        /// <param name="page"></param>
        /// <param name="nowCity"></param>
        /// <param name="cityText"></param>
        /// <returns></returns>
        public JsonResult GetHospitalList(int docID, int cityID, string type, string searchText, string coordinate, int remoteHospId, int currentIndex, int page = 10, string nowCity = "", string cityText = "")
        {
            var list = new List<HospitalModel>();
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();

            var returnModel = client.getOrganizationList(cityID, searchText, currentIndex, page);
            
            DoctorModel doctorModel = new DoctorModel();
            var returnModel5 = client.getDoctor(docID);
            // 获得医生详细数据
            var doctorJson = "";
            if (returnModel5.Data != null)
            {
                doctorJson = returnModel5.Data.ToString();
                if (doctorJson != "")
                {
                    doctorModel = JsonConvert.DeserializeObject<DoctorModel>(doctorJson);
                }
            }

            if (returnModel.Data != null)
            {
                var organizationListJson = returnModel.Data.ToString();
                if (!String.IsNullOrEmpty(organizationListJson))
                {
                    var organizationList = JsonConvert.DeserializeObject<List<HospitalModel>>(organizationListJson);
                    // 判断定位是否成功
                    if (!String.IsNullOrEmpty(coordinate) && organizationList != null && !coordinate.Split(',')[0].Equals("undefined"))
                    {

                        for (int i = 0; i < organizationList.Count; i++)
                        {
                            //if (remoteHospId != 0)
                            //{
                                // remote 0 表情线下 1表示远程 2 不显示
                                if (type == "dianzhen")
                                {
                                    organizationList[i].remote = 2;
                                }
                                else
                                {
                                    if (remoteHospId == organizationList[i].hospitalID)
                                    {
                                        organizationList[i].remote = 0;
                                    }
                                    else
                                    {
                                        organizationList[i].remote = 1;
                                    }
                                }
                            //}
                            //else
                            //{
                            //    organizationList[i].remote = 2;
                            //}


                            var listDistinct = organizationList[i].coordinate;
                            if (!String.IsNullOrEmpty(listDistinct) && listDistinct.IndexOf(',') != -1)
                            {
                                // 根据定位经纬度和诊所坐标计算距离
                                var dictince = QQMapHelper.GetDistance(Double.Parse(coordinate.Split(',')[0]), Double.Parse(coordinate.Split(',')[1]), Double.Parse(listDistinct.Split(',')[0]), Double.Parse(listDistinct.Split(',')[1]));
                                if ("全部".Equals(cityText))
                                {
                                    //organizationList[i].distance = (int)dictince + "km";
                                    //organizationList[i].dist = (int)dictince;
                                    //organizationList[i].distance = "未知";
                                    //organizationList[i].dist = int.MaxValue;
                                }
                                else
                                {
                                    // 判断诊所是否在当前定位城市
                                    if (cityText.Equals(nowCity))
                                    {
                                        organizationList[i].distance = dictince.ToString("n1") + "km";
                                        organizationList[i].dist = (int)dictince;

                                        if (dictince > 0 && dictince < 1)
                                        {
                                            var dis = dictince * 1000 + "";
                                            if (dis.Length > 3)
                                            {
                                                organizationList[i].distance = dis.Substring(0, 3) + "m";
                                            }
                                            else
                                            {
                                                organizationList[i].distance = (dictince * 1000) + "m";
                                            }


                                        }
                                    }
                                    else
                                    {
                                        //organizationList[i].distance = "未知";
                                        //organizationList[i].dist = int.MaxValue;
                                    }
                                }
                            }
                            else
                            {
                                //organizationList[i].distance = "未知";
                                //organizationList[i].dist = int.MaxValue;
                            }
                            organizationList[i].hospitalImage = WeixinConfig.crsUrl + organizationList[i].hospitalImage;
                        }
                    }
                    else
                    {
                        if (organizationList != null)
                        {
                            for (int i = 0; i < organizationList.Count; i++)
                            {
                                //if (remoteHospId != 0)
                                //{
                                    if (type == "dianzhen")
                                    {
                                        organizationList[i].remote = 2;
                                    }
                                    else
                                    {
                                        if (remoteHospId == organizationList[i].hospitalID)
                                        {
                                            organizationList[i].remote = 0;
                                        }
                                        else
                                        {
                                            organizationList[i].remote = 1;
                                        }
                                    }
                                //}
                                //else
                                //{
                                //    organizationList[i].remote = 2;
                                //}
                                organizationList[i].distance = "";
                                organizationList[i].hospitalImage = WeixinConfig.crsUrl + organizationList[i].hospitalImage;
                            }
                        }
                    }
                    if (type != "dianzhen")
                    {
                        if (doctorModel.isRemote == "1")
                        {
                            list.AddRange(organizationList.Where(o => o.remote == 0).ToArray());
                        }
                        else if (doctorModel.isRemote == "2")
                        {
                            list.AddRange(organizationList.Where(o => o.remote == 1).ToArray());
                        }
                        else
                        {
                            list.AddRange(organizationList.ToArray());
                        }
                    }
                    else
                    {
                        list.AddRange(organizationList.ToArray());
                    }
                }
            }
            if (list != null)
            {
                if (type == "dianzhen")
                {
                    list = list.OrderBy(p => p.dist).ThenBy(p => p.hospitalName).ToList();
                }
                else
                {
                    list = list.OrderBy(p => p.remote).ThenBy(p => p.dist).ThenBy(p => p.hospitalName).ToList();
                }
            }
            return Json(list);
        }

        /// <summary>
        /// 获取地址
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        public JsonResult GetAddress(string latitude, string longitude)
        {
            var str = QQMapHelper.GetAddress(latitude, longitude);
            return Json(str);
        }

        /// <summary>
        /// 诊所搜索
        /// </summary>
        /// <param name="docID"></param>
        /// <param name="date"></param>
        /// <param name="time"></param>
        /// <param name="type"></param>
        /// <param name="target"></param>
        /// <param name="clinicID"></param>
        /// <param name="clinicinfoID"></param>
        /// <param name="hospitalID"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult HospitalSearch(int docID, string date, string time, string type, string target, int clinicID = 0, int clinicinfoID = 0, int hospitalID = 0)
        {

            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            var model = new HospitalListModel();
            model.docID = docID;
            model.date = date;
            model.time = time;
            model.type = type;
            model.clinicID = clinicID;
            model.clinicinfoID = clinicinfoID;
            model.hospitalID = hospitalID;
            model.target = target;
            return View(model);
        }

        /// <summary>
        /// 诊所详情
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="docID"></param>
        /// <param name="date"></param>
        /// <param name="time"></param>
        /// <param name="target"></param>
        /// <param name="type"></param>
        /// <param name="onOffLine"></param>
        /// <param name="range"></param>
        /// <param name="clinicID"></param>
        /// <param name="clinicinfoID"></param>
        /// <param name="remoteHospId"></param>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult HospitalSub(int hospitalID, string docID = "", string date = "", string time = "", string target = "", string type = "", string onOffLine = "", string range = "", int clinicID = 0, int clinicinfoID = 0, int remoteHospId = 0, string coordinate = "")
        {
            var url = Request.Url.ToString().Split('#')[0];
            ViewBag.JsSDKModel = JsSDKModel.GetJsSDKModel(url);
            var model = new HospitalModel();
            var objOrganization = CacheHelper.GetCache(KeyConst.CacheOrganization + hospitalID);
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            if (objOrganization != null)
            {
                model = JsonConvert.DeserializeObject<HospitalModel>(objOrganization.ToString());
            }
            else
            {
                var returnModel = client.getOrganization(hospitalID);
                var organizationJson = returnModel.Data.ToString();
                if (!String.IsNullOrEmpty(organizationJson))
                {
                    CacheHelper.SetCache(KeyConst.CacheOrganization + hospitalID, organizationJson);
                }
                model = JsonConvert.DeserializeObject<HospitalModel>(organizationJson);

            }
            // 计算距离
            if (!String.IsNullOrEmpty(coordinate) && !coordinate.Split(',')[0].Equals("undefined"))
            {

                if (!String.IsNullOrEmpty(range) && range.IndexOf(',') != -1)
                {
                    var dictince = QQMapHelper.GetDistance(Double.Parse(coordinate.Split(',')[0]), Double.Parse(coordinate.Split(',')[1]), Double.Parse(range.Split(',')[0]), Double.Parse(range.Split(',')[1]));
                    if (dictince > 1)
                    {
                        model.distance = dictince.ToString("n1") + "km";
                        model.dist = (int)dictince;

                    }
                    if (dictince > 0 && dictince < 1)
                    {
                        var dis = dictince * 1000 + "";
                        if (dis.Length > 3)
                        {
                            model.distance = dis.Substring(0, 3) + "m";
                        }
                        else
                        {
                            model.distance = (dictince * 1000) + "m";
                        }


                    }
                }
                else
                {
                    //model.distance = "未知";
                }

            }
            else
            {
                //model.distance = "未知";
            }
            var urlimg = RayelinkWeixin.Const.Config.WeixinConfig.crsUrl;
            model.hospitalIntroduce = model.hospitalIntroduce.Replace("src=\"/upload", "src=\"" + urlimg + "upload");
            model.hospitalID = hospitalID;
            model.docID = docID;
            model.selectDate = date;
            model.selectTime = time;
            model.onOffLine = onOffLine;
            model.type = type;
            model.range = range;
            model.target = target;
            model.clinicID = clinicID;
            model.clinicinfoID = clinicinfoID;
            model.remoteHospId = remoteHospId;
            var list = new List<UserCommentModel>();
            #region 用户的评论

            var pid = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = AuthorizeUser.GetAuthorizeUser(User.Identity.Name);

                if (user != null)
                {
                    pid = user.pid;
                }
            }
            var returnModel1 = client.getCommentList(null, hospitalID, pid, 1, 5);
            if (returnModel1.Data != null)
            {
                var commentListJson = returnModel1.Data.ToString();
                var commentListModel = JsonConvert.DeserializeObject<List<UserCommentModel>>(commentListJson);
                if (commentListModel != null)
                {
                    foreach (var UserCommentModel in commentListModel)
                    {
                        string addtime = UserCommentModel.add_time.ToString();
                        var adddate = long.Parse(addtime.Split('(')[1].Split(')')[0]);
                        UserCommentModel.add_time = Convert.ToDateTime(ConvertTime(adddate)).ToString("yyyy.MM.dd");
                        string replytime = UserCommentModel.reply_time;
                        if (!String.IsNullOrEmpty(replytime))
                        {
                            var replydate = long.Parse(replytime.Split('(')[1].Split(')')[0]);

                            UserCommentModel.reply_time = Convert.ToDateTime(ConvertTime(replydate)).ToString("yyyy.MM.dd");
                        }
                    }
                    list.AddRange(commentListModel.ToArray());
                }
            }
            #endregion
            model.listUserComment = list;
            return View(model);
        }

        public static DateTime ConvertTime(long milliTime)
        {
            long timeTricks = new DateTime(1970, 1, 1).Ticks + milliTime * 10000 + TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours * 3600 * (long)10000000;
            return new DateTime(timeTricks);
        }

        /// <summary>
        /// 所有评价
        /// </summary>
        /// <param name="num"></param>
        /// <param name="orgId"></param>
        /// <param name="orgName"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult AllCommons(int num, int orgId, string orgName, string image)
        {
            ViewBag.num = num;
            ViewBag.orgId = orgId;
            ViewBag.orgName = orgName;
            ViewBag.image = image;
            return View();
        }


        /// <summary>
        /// 获得所有评价
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="page"></param>
        /// <param name="orgId"></param>
        /// <param name="orgName"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        [WeixinSpeficAuthorize]
        public ActionResult GetAllCommons(int pageIndex, int page, int orgId, string orgName, string image)
        {
            CRSService.CRS_ServiceSoapClient client = new CRSService.CRS_ServiceSoapClient();
            DoctorModel doctorModel = new DoctorModel();
            var user = AuthorizeUser.GetAuthorizeUser(this.User.Identity.Name);
            StringBuilder html = new StringBuilder();
            int pid = 0;
            if (user != null)
            {
                pid = user.pid;
            }
            #region 评论
            doctorModel.listUserComments = new List<UserCommentModel>();
            var returnModel = client.getCommentList(null, orgId, pid, pageIndex, page);
            if (returnModel.Data != null)
            {
                var commentJson = returnModel.Data.ToString();
                var commentList = JsonConvert.DeserializeObject<List<UserCommentModel>>(commentJson);
                if (commentList != null)
                {
                    foreach (var UserCommentModel in commentList)
                    {
                        string addtime = UserCommentModel.add_time.ToString();
                        var adddate = long.Parse(addtime.Split('(')[1].Split(')')[0]);
                        UserCommentModel.add_time = Convert.ToDateTime(ConvertTime(adddate)).ToString("yyyy.MM.dd");
                        string replytime = UserCommentModel.reply_time;
                        if (!String.IsNullOrEmpty(replytime))
                        {
                            var replydate = long.Parse(replytime.Split('(')[1].Split(')')[0]);

                            UserCommentModel.reply_time = Convert.ToDateTime(ConvertTime(replydate)).ToString("yyyy.MM.dd");
                        }
                    }
                    doctorModel.listUserComments.AddRange(commentList.ToArray());
                }

            }
            #endregion
            if (doctorModel.listUserComments != null && doctorModel.listUserComments.Count > 0)
            {
                html.Append("<ul>");
                foreach (var item in doctorModel.listUserComments)
                {
                    html.Append("<li>");
                    html.Append("<div class='comment_wrap'>");
                    html.Append("<div class='rate'>");
                    if (item.stars == 0)
                    {
                        html.Append("<img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 1)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 2)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 3)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 4)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s5.png' alt=''>");
                    }
                    else if (item.stars == 5)
                    {
                        html.Append("<img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''><img src='../images/doctor_sub/s1.png' alt=''>");
                    }
                    html.Append("</div>");
                    if ("" == item.content || item.content == null)
                    {
                        html.Append("<p>" + "该用户未发表文字评价" + "</p>");
                    }
                    else
                    {
                        html.Append("<p>" + item.content + "</p>");
                    }

                    html.Append("<p class='name_date'><span class='name'>" + item.userName + "</span>（<span class='date'>" + item.add_time + "</span>）</p>");
                    html.Append("</div>");

                    if (item.replyContent != null)
                    {
                        html.Append("<div class='d_comment'>");
                        html.Append("<span class='d_name' style=' color:#00abb0;'>客服回复：</span>");
                        html.Append("<span class='d_com_content'>" + item.replyContent + "</span>");
                        html.Append("<p class='d_com_date'>" + item.reply_time + "</p>");
                        html.Append("</div>");
                        html.Append("</div>");

                    }

                }
                html.Append("</ul>");
            }


            return Content(html.ToString());
        }
    }
}