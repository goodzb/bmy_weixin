﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayelinkWeixin.Web.Models
{
    [Serializable]
    public class RefUserModel
    {
        /// <summary>
        /// 用户类型，0是地方医生，1是中央医生，2是护士
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 在用户中心那边的编号
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 用户手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 微信openid
        /// </summary>
        public string OpenId { get; set; }

        /// <summary>
        /// 所属诊所Id
        /// </summary>
        public int ClientId { get; set; }
        
    }
}