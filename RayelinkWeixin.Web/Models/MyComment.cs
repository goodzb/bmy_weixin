﻿using RayelinkWeixin.Const.Config;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace RayelinkWeixin.Web.Models
{ /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class MyComment
    {
        public int DoctorId { get; set; }
        public string DoctorName { get; set; }
        public string DepName { get; set; }
        public string TitleName { get; set; }
        public string DoctorImage { get; set; }
        public string DoctorComment { get; set; }
        public int DoctorStar { get; set; }
        public string DoctorReplyContent { get; set; }
        public int HospId { get; set; }
        public string HospName { get; set; }
        public string HospImage { get; set; }
        public string HospComment { get; set; }
        public int HospStar { get; set; }
        public string AddTimeString { get; set; }
        public int VisitId { get; set; }
        public bool HasReplyComment { get; set; }
    }
}