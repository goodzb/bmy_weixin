﻿using RayelinkWeixin.Business;
using RayelinkWeixin.Const.Config;
using Senparc.Weixin.MP.Containers;
using Senparc.Weixin.MP.Helpers;
using System;
using System.Web;

namespace RayelinkWeixin.Web.Models
{
    [Serializable]
    public class JsSDKModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string appId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string timestamp { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string nonceStr { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string signature { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Ticket { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static JsSDKModel GetJsSDKModel(string url)
        {
            var appid = WeixinConfig.appID;
            var appSecret = WeixinConfig.appSecret;
            return GetJsSDK(url, appid, appSecret);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appid"></param>
        /// <returns></returns>
        public static JsSDKModel GetJsSDK(short appid)
        {
            var appInfo = new ShareFunction(appid).App;
            if (null != appInfo)
            {
                if (HttpContext.Current != null)
                {
                    HttpRequest request = System.Web.HttpContext.Current.Request;
                    return GetJsSDK(request.Url.ToString().Split('#')[0], appInfo.AppKey, appInfo.AppSecret);
                }
            }
            return null;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="appKey"></param>
        /// <param name="appSecret"></param>
        /// <returns></returns>
        private static JsSDKModel GetJsSDK(string url, string appKey, string appSecret)
        {
            if (url.StartsWith("http://localhost")) return null;
            url = url.Split('#')[0];
            string ticket = JsApiTicketContainer.TryGetJsApiTicket(appKey, appSecret);
            var timestamp = JSSDKHelper.GetTimestamp();
            var nonceStr = JSSDKHelper.GetNoncestr();
            var signature = JSSDKHelper.GetSignature(ticket, nonceStr, timestamp, url);
            var model = new JsSDKModel()
            {
                appId = appKey,
                nonceStr = nonceStr,
                timestamp = timestamp,
                signature = signature,
                Ticket = ticket,
                Url = url
            };
            return model;
        }
    }
}