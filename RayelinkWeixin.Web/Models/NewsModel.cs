﻿using RayelinkWeixin.Const.Config;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace RayelinkWeixin.Web.Models
{ /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class NewsModel
    {
        public int id { get; set; }
        public string img_url { get; set; }
        public string title { get; set; }
        public string zhaiyao { get; set; }
        public string update_time { get; set; }
        public string add_time { get; set; }
    }
}