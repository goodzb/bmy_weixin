﻿using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace RayelinkWeixin.Web.Models
{ /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class AuthorizeUser
    {
        /// <summary>
        /// 微信头像
        /// </summary>
        public static string headimgurl()
        {
            var image = HttpContext.Current.Session[KeyConst.SessionUserHeadImageUrl];
            if(image != null){
                var imgStr = image.ToString();
                //image = null;
                return imgStr;
            }else{
                return "";
            }
        }

        /// <summary>
        /// 微信头像
        /// </summary>
        public static string nickname()
        {
            var nickname = HttpContext.Current.Session[KeyConst.SessionUserNickName];
            if (nickname != null)
            {
                return nickname.ToString();
            }
            else
            {
                return "";
            }
        }

        public string openid { get; set; }

        public int pid { get; set; }

        public string username { get; set; }

        public string telphone { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        public static AuthorizeUser GetAuthorizeUser(string openid)
        {
            CRMWebService.ServiceForWebSoapClient client = new CRMWebService.ServiceForWebSoapClient();
            var webAccount = client.CRM_WEB_LoginWebUserByOpenID(openid, WeixinConfig.crmPartner);
            var user = new AuthorizeUser();
            user.openid = openid;
            if (webAccount != null)
            {
                user.pid = webAccount.PID;
                user.telphone = webAccount.Mobile;
                user.username = webAccount.Name;
                return user;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Create user ticket
        /// </summary>
        /// <param name="strUserName"></param>
        public static string CreateLoginUserTicket(string openid, string username)
        {
            //构建 ticket
            FormsAuthenticationTicket ticket =new FormsAuthenticationTicket(1, username, DateTime.Now, DateTime.Now.AddDays(1),true, string.Format("{0}:{1}", openid, username), FormsAuthentication.FormsCookiePath);

            string ticString = FormsAuthentication.Encrypt(ticket);

            //put ticket into Cookie and Session
            //SetAuthCookie make Identity true
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, ticString);
            //设置HttpOnly
            cookie.HttpOnly = true;
            if (ticket.IsPersistent)
            {
                cookie.Expires = ticket.Expiration;
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
            FormsAuthentication.SetAuthCookie(openid, true);

            //rewrite user info in HttpContext
            //string[] roles = ticket.UserData.Split(',');
            var identity = new FormsIdentity(ticket);
            IPrincipal principal = new GenericPrincipal(identity, null);
            HttpContext.Current.User = principal;

            return ticString;
        }
    }
}