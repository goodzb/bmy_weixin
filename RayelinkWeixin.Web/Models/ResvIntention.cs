﻿using RayelinkWeixin.Const.Config;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace RayelinkWeixin.Web.Models
{ /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ResvIntention
    {


        public int DoctorId { get; set; }

        public DateTime? ConfirmTime { get; set; }
        public string HospName { get; set; }
        public string VisitAddress { get; set; }
        public string Name { get; set; }
        public string DoctorName { get; set; }
        public int HospId { get; set; }
        public int RemoteHospId { get; set; }
        public string DepName { get; set; }
        public DateTime ResvDate { get; set; }

        public string ResvDateString { get; set; }
        public string TitleName { get; set; }
        public string Image { get; set; }
        public decimal? Price { get; set; }
        public string Duration { get; set; }

        public int? Type  { get; set; }

        public string TypeString { get; set; }
        public string OfflineOrRemote { get; set; }
        public int VisitId { get; set; }
        public int CaseId { get; set; }
        public bool HasComment { get; set; }

        public int StartTime { get; set; }
        public int EndTime { get; set; }


    }
}