﻿//http://www.cnblogs.com/axl234/p/5899137.html
var VueHttpGet = function (url, params, sucessCallBack, catchCallBack) {
	Vue.http.get(url, { params }, { emulateJSON: true }).then((res) => {
		if (sucessCallBack && 200 === res.status) {
			var result = res.body;
			if (result) {
				sucessCallBack(result);
			}
		}
	}).catch((res) => {
		if (catchCallBack) {
			catchCallBack(res);
		}
	});
}

var VueHttpPost = function (url, params, sucessCallBack, catchCallBack) {
	Vue.http.post(url, params, { emulateJSON: true }).then((res) => {
		if (sucessCallBack && 200 === res.status) {
			var result = res.body;
			if (result) {
				sucessCallBack(result);
			}
		}
	}).catch((res) => {
		if (catchCallBack) {
			catchCallBack(res);
		}
	});
}