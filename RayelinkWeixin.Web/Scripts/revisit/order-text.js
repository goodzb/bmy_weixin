/**
 * Created by chesw on 2017/4/27.
 */
(function() {
    new Vue({
        el: '#order-text',
        data: {
            IsSubmit: true
        },
        mounted: function() {
            showHtml();
        },
        methods: {
            pay: function() {
                var payModel = PageModel.PayModel;
                var _this = this;
                if (payModel) {
                    if (typeof(WeixinJSBridge) !== "undefined") {
                        //公众号支付
                        WeixinJSBridge.invoke('getBrandWCPayRequest', {
                            "appId": payModel.appID, //公众号名称，由商户传入
                            "timeStamp": payModel.timeStamp, //时间戳
                            "nonceStr": payModel.nonceStr, //随机串
                            "package": payModel.package, //扩展包
                            "signType": "MD5", //微信签名方式:MD5
                            "paySign": payModel.paySign //微信签名
                        }, function(res) {
                            //alert(JSON.stringify(res));
                            _this.SubmitPayResult(res);
                        });
                    } else {
                        DialogNoTip('请在微信端完成支付');
                    }
                } else {
                    DialogNoTip('当前订单状态有误，请核实');
                }
            },
            SubmitPayResult: function(res) {
                //alert(JSON.stringify(res));
                var _this = this;
                if (_this.IsSubmit) {
                    var payResult = 2;
                    if (res.err_msg == "get_brand_wcpay_request:cancel") {
                        payResult = 5;
                    } else if (res.err_msg == "get_brand_wcpay_request:fail") {
                        payResult = 4;
                    } else if (res.err_msg != "get_brand_wcpay_request:ok") {
                        DialogNoTip('支付遇到问题，请重试');
                        return false;
                    }
                    _this.IsSubmit = false;
                    VueHttpPost(PageModel.PayResult, { payResult: payResult }, function(result) {
                        if (result.IsSuccess) {
                            location.replace(result.ReturnUrl);
                        } else {
                            DialogNoTip(result.Text);
                            _this.IsSubmit = true;
                        }
                    });
                }
            }
        }
    });
})()