(function() {
    var vm = new Vue({
        el: '#wrapper',
        data: {
            tab: '', //true：选中第一个tab
            options: {
                effective: [],
                invalid: [],
                all: [],
                NE: []
            },
            DataList: [],
            param: {
                Content: '取消后，医生将无法为您提供在线图文复诊咨询服务。您确定要取消吗？',
                LeftText: '取消订单',
                LeftFunc: null,
                RightText: "我在想想",
                RightFunc: null,
                Show: false
            },
            ShowNoDataBox: false,
            ShowFailed: false
        },
        components: {
            'dialog-confirm': DialogConfirm
        },
        watch: {
            tab(curVal, oldVal) {
                this.DataList = this.options[this.tab];
                this.ShowNoDataBox = this.DataList.length == 0;
            }
        },
        created: function() {
            this.GetOrderList();
        },
        mounted: function() {
            showHtml();
        },
        methods: {
            changeTab: function(p_index) {
                //切换初始化
                this.tab = p_index;
                if (!this.ShowFailed) this.ShowNoDataBox = this.options[this.tab].length == 0;
            },
            InitData: function() {
                this.options = {
                    effective: [],
                    invalid: [],
                    all: []
                };
                this.DataList = [];
            },
            CancelOrder: function(event, _item) {
                var _this = this;
                console.log(_item);
                _this.param.Show = true;
                _this.param.LeftFunc = function() {
                    VueHttpPost(PageUrlModel.CancelOrder, { orderSid: _item.OrderSid }, function(result) {
                        if (result.IsSuccess) {
                            DialogOkTip(result.Text);
                        } else {
                            DialogNoTip(result.Text);
                        }
                        _this.tab = 'NE';
                        _this.GetOrderList();
                        //location.replace(location.href);
                    });
                }
            },
            GetOrderList: function() {
                var _this = this;
                VueHttpGet(PageUrlModel.OrderList, null, function(result) {
                    _this.InitData();
                    //alert(JSON.stringify(result));
                    if (result.IsSuccess) {
                        var msgList = result.ResultData.Result;
                        if (msgList) {
                            if (typeof(msgList) == "string") msgList = eval(msgList);
                            msgList.forEach(function(order) {
                                _this.PushData(order);
                            }, this);

                            var list = Enumerable.From(_this.options.effective).OrderByDescending(function(x) { return x.OrderTime }).OrderBy(function(x) { return x.SortPosition }).ToArray();

                            var newArray = [];
                            var oGrop = Enumerable.From(list).GroupBy("$.SortPosition").ForEach(function(val) {
                                var ee = Enumerable.From(val).OrderByDescending(function(x) { return x.OrderTime }).ToArray();
                                ee.forEach(function(oooooo) {
                                    newArray.push(oooooo);
                                }, this);
                            });
                            _this.options.effective = newArray;

                            _this.options.all.sort(function(a, b) {
                                return b.OrderTime - a.OrderTime;
                            });

                            _this.options.invalid.sort(function(a, b) {
                                return b.OrderTime - a.OrderTime;
                            });

                            if (_this.options.effective.length == 0) _this.ShowNoDataBox = true;

                        } else {
                            _this.ShowNoDataBox = true;
                        }
                    } else {
                        _this.ShowFailed = true;
                    }
                    _this.tab = 'effective';
                });
            },
            PushData: function(order) {
                var _this = this;
                order.CancelStatus = 0;
                order.ifCancel = "取消";
                order.InvalidStatus = false;
                order.SortPosition = 10; //排序位置
                order.OrderTime = FDate(order.OrderTime.replace('T', ' '));
                if (order.MessageTime) {
                    order.Date = FDate(order.MessageTime.replace('T', ' ')).formatDate('YYYY.MM.DD');
                } else {
                    order.Date = order.OrderTime.formatDate('YYYY.MM.DD');
                }
                //状态: 1：未支付，默认提交状态；2：H5支付成功；3:回调支付成功(这个是支付后微信回调post回来的支付数据，这才是真正意义上的支付成功):4：支付失败；
                //5：支付过程中用户取消支付; 6：用户取消；7：超时job取消；
                //8：医生结束咨询(此订单完成咨询服务)；9:JOB关闭(超过24小时医生没有回复，job关闭了)；10:退款中(已经提交了退款申请)；11:已退款(去接口扫描出已经退款成功的);
                //20：已经确认单还未支付(远程门诊会存在)


                //会话状态：0-未开始、1-医生未回复、2-用户未回复、3-用户取消、4-医生关闭、5-系统超时关闭、6-其他原因关闭、7：正在会话中

                // 排序规则
                // 1、 会话是否已结束
                // 未结束的会话列表排在已结束列表上方
                // 2、 时间（成功付款建立交流页的时间）
                // 倒序排列
                //     未结束的会话列表（按时间倒序）+已结束的会话列表（按时间倒序）


                if (order.Status == 2 || order.Status == 3 || order.Status == 8 || order.Status == 9) { //有效问题
                    if (order.Status == 3 && order.ConversationStatus <= 1) { //可以进行取消操作,还没有开始对话
                        order.CancelStatus = 1;
                        order.SortPosition = 1;
                    } else {
                        order.CancelStatus = 2; //不可以进行取消，已经开始对话或者完成了对话
                        if (order.ConversationStatus == 7) { order.SortPosition = 2; }

                        if (order.Status == 8 || order.Status == 9) { order.SortPosition = 3; };
                    }
                    _this.options.effective.push(order);
                    console.log(order.UserName + order.SortPosition);
                } else if (order.Status >= 6 && order.Status <= 7) { //有效，但是已经是取消状态了
                    // order.CancelStatus = 3;
                    // order.SortPosition = 4;
                    // _this.options.effective.push(order);

                    return;
                } else { //无效问题  4  5 9 10 11
                    order.InvalidStatus = true;
                    _this.options.invalid.push(order);
                }

                _this.options.all.push(order);
            },
            GoChatPage: function(event, _item) {
                window.event ? window.event.cancelBubble = true : e.stopPropagation();
                location.href = PageUrlModel.Chat + "&orderSId=" + _item.OrderSid;
            }
        }
    })
})(Vue)