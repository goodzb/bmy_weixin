(function() {
    new Vue({
        el: '#expert-list',
        data: {

        },
        mounted: function() {
            showHtml();
        },
        methods: {
            jumpToOrder: function(did) {
                location.href = LayoutModel.BaseUrl + "ReVisit/ReVisit/OrderIndex?doctorId=" + did + "&openid=" + LayoutModel.OpenId;
            }
        }
    })
})()