/**
 * Created by chesw on 2017/4/27.
 */
(function() {
    new Vue({
        el: '#order-remotely',
        data: {
            cityCls: 'invalid-color',
            CityList: [],
            OrderInfo: PageModel.OrderInfo,
            IsSubmit: true
        },
        mounted: function() {
            showHtml();
            this.GetCityList();
        },
        methods: {
            GetCityList: function() {
                var _this = this;
                VueHttpGet(PageUrlModel.GetCityList, { openid: LayoutModel.OpenId }, function(result) {
                    if (result.IsSuccess) {
                        _this.CityList = result.ResultData.Result;
                        console.log(_this.CityList);
                    }
                });
            },
            //选择城市
            choiceCity: function() {
                var _this = this;
                var iosSelect = new IosSelect(1, [_this.CityList], {
                    oneLevelId: _this.OrderInfo.PatientCityId,
                    title: '选择地址',
                    itemHeight: 35,
                    relation: [1, 1, 0, 0],
                    callback: function(objOne) {
                        _this.OrderInfo.PatientCityId = objOne.id;
                        _this.OrderInfo.PatientCityName = objOne.value;
                        //_this.city = objOne.value + ' ' + objTwo.value;
                        _this.cityCls = 'valid-color';
                    }
                });
            },
            //点击删除按钮
            del: function(p_index) {
                switch (p_index) {
                    case 1:
                        {
                            this.OrderInfo.PatientName = "";
                            break;
                        }
                    case 2:
                        {
                            this.OrderInfo.PatientMobile = "";
                            break;
                        }
                    case 3:
                        {
                            this.OrderInfo.CloseReason = "";
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            },
            GetVerificationCode: function(params) { //获取验证码
                var _this = this;
                var codeBtn = document.getElementById("get_code");
                if (codeBtn.innerText == '获取验证码') {
                    if (Rayelink.isEmpty(_this.OrderInfo.PatientMobile) || !Rayelink.isPhone(_this.OrderInfo.PatientMobile)) {
                        _this.ShowMsg("请输入正确的手机号");
                        return false;
                    }
                    VueHttpPost(PageUrlModel.SendMessageValidCode, { mobile: _this.OrderInfo.PatientMobile, type: LayoutModel.CampaignSid }, function(result) {
                        if (result.success) {
                            DialogOkTip(result.message);
                            countDown("get_code");
                        } else {
                            _this.ShowMsg(result.message);
                        }
                    });
                }
            },
            SubmitValidate: function() { //提交验证
                var _this = this;
                if (_this.OrderInfo.PatientCityId == -1) {
                    _this.ShowMsg("请选择就诊城市 ");
                    return false;
                } else if (Rayelink.isEmpty(_this.OrderInfo.PatientName) || _this.OrderInfo.PatientName.length <= 1) {
                    _this.ShowMsg("请输入正确的患者姓名");
                    return false;
                } else if (Rayelink.isEmpty(_this.OrderInfo.PatientMobile) || !Rayelink.isPhone(_this.OrderInfo.PatientMobile)) {
                    _this.ShowMsg("请输入正确的手机号 ");
                    return false;
                } else if (_this.OrderInfo.PatientMobile !== _this.OrderInfo.UserMobile && Rayelink.isEmpty(_this.OrderInfo.CloseReason)) {
                    _this.ShowMsg("请输入正确的验证码");
                    return false;
                }
                return true;
            },
            ShowMsg: function(msg) {
                DialogNoTip(msg);
            },
            Submit: function(params) {
                var _this = this;
                if (_this.IsSubmit) {
                    if (_this.SubmitValidate()) {
                        _this.IsSubmit = false;
                        VueHttpPost(PageUrlModel.SubmitRemotely, _this.OrderInfo, function(result) {
                            if (result.IsSuccess) {
                                _this.WeixinJSBridge(result.ResultData.Result);
                            } else {
                                _this.ShowMsg(result.Text);
                                _this.IsSubmit = true;

                                if (result.ReturnUrl) location.replace(result.ReturnUrl);

                            }
                        });
                    }
                }
            },
            WeixinJSBridge: function() {
                var _this = this;
                payModel = PageModel.PayJsJdk;
                if (payModel) {
                    if (typeof(WeixinJSBridge) !== "undefined") {
                        //公众号支付
                        WeixinJSBridge.invoke('getBrandWCPayRequest', {
                            "appId": payModel.appID, //公众号名称，由商户传入
                            "timeStamp": payModel.timeStamp, //时间戳
                            "nonceStr": payModel.nonceStr, //随机串
                            "package": payModel.package, //扩展包
                            "signType": "MD5", //微信签名方式:MD5
                            "paySign": payModel.paySign //微信签名
                        }, function(res) {
                            //alert(JSON.stringify(res));
                            _this.SubmitPay(res);
                        });
                    } else {
                        _this.ShowMsg('请在微信端完成支付');
                    }
                } else {
                    _this.ShowMsg('当前订单状态有误，请核实');
                }
            },
            SubmitPay: function(res) {
                var _this = this;
                var payResult = 2;
                if (res.err_msg == "get_brand_wcpay_request:cancel") {
                    payResult = 5;
                } else if (res.err_msg == "get_brand_wcpay_request:fail") {
                    payResult = 4;
                } else if (res.err_msg != "get_brand_wcpay_request:ok") {
                    _this.ShowMsg('支付遇到问题，请重试');
                    return false;
                }
                _this.IsSubmit = false;
                VueHttpPost(PageUrlModel.PayResult, { payResult: payResult, orderType: 1 }, function(result) {
                    if (result.IsSuccess) {
                        location.replace(result.ReturnUrl);
                    } else {
                        _this.ShowMsg(result.Text);
                        _this.IsSubmit = true;
                    }
                });
            }
        }
    })
})()