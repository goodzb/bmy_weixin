/**
 * Created by chesw on 2017/3/31.
 */
//页面加载完成，显示整个页面
function showHtml(callBack) {
    Vue.nextTick(function () {
        var elem = document.getElementsByTagName('body');
        elem[0].style.display = 'block';
        if (callBack) {
            callBack();
        }
    })
}
//解析url参数
function paramUrl(p_url) {
    if(!p_url)  return;

    var tempUrl = null, param = null, paramArr = {};
    tempUrl = p_url.split('?')[1];
    if(tempUrl) {
        param = tempUrl.split('&');
        for(var i = 0; i < param.length; i++) {
            var temp = param[i].split('=');
            paramArr[temp[0]] = temp[1];
        }
    }
    return paramArr;
}

function FDate(time) {
    if (typeof (time) =='object') {
        return time;
    }
    time = time.replace(/-/g, ':').replace(' ', ':').replace('T', ' ');
    time = time.split(':');
    var time1 = new Date(time[0], (time[1] - 1), time[2], time[3], time[4], time[5]);
    return time1;
}


//vue-resource请求
// function getWxConfVue(p_this, p_url) {
//     var _this = p_this;
//     var url = "../Share/GetWXJsSDKInfo?pageurl=" + p_url;
//     p_this.$http.get(url).then(function (data) {
//         var data = data.data;
//         console.log(data);
//
//         wx.config({
//             debug: false,
//             appId: data.appId,
//             timestamp: data.timestamp,
//             nonceStr: data.nonceStr,
//             signature: data.signature,
//             jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone']
//         });
//     })
// }
/* 弹窗
 * p_obj: {url: 'logo', text:'提示文字'}
 * */

window.HtmlAlertShowStatus = false;
function htmlAlert(p_obj, time) {
    console.log(window.HtmlAlertShowStatus);
    if (window.HtmlAlertShowStatus) return;
    var _html = null;
    var _doc = document.getElementsByClassName('html-alert');
    if(_doc.length <= 0) {
        _html = document.createElement('div');
        _html.className = 'html-alert';
        _html.innerHTML = '<figure class="tip_box">' +
            '<img src="' + p_obj.url + '">' +
            '<p class="title">' + p_obj.text + '</p>' +
            '</figure>';
        document.body.appendChild(_html);
    }else{
        _html = document.createElement('figure');
        _html.className = 'tip_box';
        _html.innerHTML = '<img src="' + p_obj.url + '">' +
            '<p class="title">' + p_obj.text + '</p>';
        _doc[0].appendChild(_html);
    }
 
    var _newDoc = document.getElementsByClassName('html-alert');
    if (p_obj.text.length <= 20) { //提示文字长度小于等于20则1秒后消失
        setTimeout(function () {
            _newDoc[0].parentNode.removeChild(_newDoc[0]);
            window.HtmlAlertShowStatus = false;
        }, time || 1000);
    }
    else if (p_obj.text.length > 20) {//提示文字长度大于20则2秒后消失
        setTimeout(function () {
            _newDoc[0].parentNode.removeChild(_newDoc[0]);
            window.HtmlAlertShowStatus = false;
        }, time || 2000);
    }
    window.HtmlAlertShowStatus = true;
}


//验证码倒计时
function countDown(p_id) {
    var code = document.getElementById(p_id);
    var time = 60;
    code.style.background = 'gray';
    code.innerText = time + 's'; //倒计时时间
    var timeSt = setInterval(function () {
        time = time - 1;
        code.innerText = time + 's';
        if (time == 0) {
            clearInterval(timeSt);
            code.style.background = '#00abb0';
            code.innerText = '获取验证码';
        }
    }, 1000)
}

//  电话号码，包括手机号+固话
function isPhone(str, all) {
    var reg;
    if (all) {
        reg = /(^0{0,1}(13[0-9]|14[0-9]|16[0-9]|15[0-9]|17[0-9]|18[0-9])[0-9]{8})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$/;
    } else {
        reg = /^0{0,1}(13[0-9]|14[0-9]|16[0-9]|15[0-9]|17[0-9]|18[0-9])[0-9]{8}$/;
    }
    return reg.test(str);
}


/***************************************************************************************************************************/




/**
 * 文本框根据输入内容自适应高度
 * @param                {HTMLElement}        输入框元素
 * @param                {Number}                设置光标与输入框保持的距离(默认0)
 * @param                {Number}                设置最大高度(可选)
 */
var autoTextarea = function (elem, extra, maxHeight) {
    extra = extra || 0;
    var isFirefox = !!document.getBoxObjectFor || 'mozInnerScreenX' in window,
        isOpera = !!window.opera && !!window.opera.toString().indexOf('Opera'),
        addEvent = function (type, callback) {
            elem.addEventListener ?
                elem.addEventListener(type, callback, false) :
                elem.attachEvent('on' + type, callback);
        },
        getStyle = elem.currentStyle ? function (name) {
            var val = elem.currentStyle[name];

            if (name === 'height' && val.search(/px/i) !== 1) {
                var rect = elem.getBoundingClientRect();
                return rect.bottom - rect.top -
                    parseFloat(getStyle('paddingTop')) -
                    parseFloat(getStyle('paddingBottom')) + 'px';
            };

            return val;
        } : function (name) {
            return getComputedStyle(elem, null)[name];
        },
        minHeight = parseFloat(getStyle('height'));

    elem.style.resize = 'none';

    var change = function () {
        var scrollTop, height,
            padding = 0,
            style = elem.style;

        if (elem._length === elem.value.length) return;
        elem._length = elem.value.length;

        if (!isFirefox && !isOpera) {
            padding = parseInt(getStyle('paddingTop')) + parseInt(getStyle('paddingBottom'));
        };
        scrollTop = document.body.scrollTop || document.documentElement.scrollTop;

        elem.style.height = minHeight + 'px';
        if (elem.scrollHeight > minHeight) {
            if (maxHeight && elem.scrollHeight > maxHeight) {
                height = maxHeight - padding;
                style.overflowY = 'scroll';
            } else {
                height = elem.scrollHeight - padding;
                style.overflowY = 'scroll';
            };
            style.height = height + extra + 'px';
            if (elem.currHeight == undefined) {
                scrollTop += parseInt(style.height);
            } else {
                scrollTop += parseInt(style.height) - elem.currHeight;
            }
            document.body.scrollTop = scrollTop;
            document.documentElement.scrollTop = scrollTop;
            elem.currHeight = parseInt(style.height);
        };
    };
    addEvent('propertychange', change);
    addEvent('input', change);
    addEvent('focus', change);
    change();
};

/*----列表置顶-----*/
var backTop = function (objId) {
    var obj = document.getElementById(objId);
    if (obj) {
        var objIdTop = obj.offsetTop;
        document.body.scrollTop = objIdTop - 45;
    }    
}
