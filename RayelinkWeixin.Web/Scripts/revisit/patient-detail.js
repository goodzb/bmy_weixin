/**
 * Created by chesw on 2017/4/24. */
(function() {
    new Vue({
        el: '#patient-detail',
        data: {
            showBlack: false, //黑布显示
            code: '', //验证码
            age: '请选择出生日期', //生日日期,
            ageCls: this.InvalidClr,
            addressCls: this.InvalidClr,
            sex: '男/女',
            sexCls: this.InvalidClr,
            NextBtnClass_01: "next",
            NextBtnClass_02: "next invalid-next",
            NextBtnClass: "next invalid-next",
            sexStyle: {
                height: '0px',
                bottom: '0px'
            },
            doctorCls: this.InvalidClr,
            validClr: 'valid-color',
            InvalidClr: 'invalid-color',
            OrderInfo: PageModel.OrderInfo, //提交的订单信息
            DoctorList: null, //医生下拉框列表
            SelectDoctorInfo: null, //当前选择的医生对象
            IsSubmit: true
        },
        mounted: function() {
            showHtml();
            var _this = this;
            _this.OrderInfo.Age = '请选择出生日期';
            var calendar = new datePicker();
            calendar.init({
                'trigger': '#birthday',
                /*按钮选择器，用于触发弹出插件*/
                'type': 'date',
                /*模式：date日期；datetime日期时间；time时间；ym年月；*/
                'minDate': '1930-1-1',
                /*最小日期*/
                'maxDate': PageModel.MaxDate,
                /*最大日期*/
                'onSubmit': function() { /*确认时触发事件*/
                    _this.age = calendar.value;
                    _this.ageCls = _this.validClr;
                },
                'onClose': function() { /*取消时触发事件*/ }
            });

            document.getElementById('name').addEventListener('input', function(event) {
                if (!Rayelink.isEmpty(_this.OrderInfo.UserName)) {
                    _this.OrderInfo.UserName = _this.OrderInfo.UserName.ReplaceEmoji();
                }
                if (!Rayelink.isEmpty(_this.OrderInfo.UserName)) {
                    _this.NextBtnClass = _this.NextBtnClass_01; //不置灰
                } else {
                    _this.NextBtnClass = _this.NextBtnClass_02; //置灰
                }
            }, false);

            var desc = document.getElementById('disease');
            autoTextarea(desc);
        },
        watch: {
            age(curVal, oldVal) {
                var _this = this;
                if ('请选择出生日期' == curVal) return false;
                console.log(curVal + "与" + PageModel.MaxDate + "相差");　
                var timespan = new Date(PageModel.MaxDate).Sub(new Date(curVal));
                console.log(timespan);　
                if (timespan.Years == 0 && timespan.Months <= 1) {
                    _this.ShowMsg("请输入正确的年龄");
                    _this.OrderInfo.Age = '请选择出生日期';
                    _this.OrderInfo.Birthday = '';
                    _this.ageCls = _this.InvalidClr;
                    _this.age = '';
                    return false;
                }
                if (timespan.Years == 0) {
                    _this.OrderInfo.Age = (timespan.Months - 1) + "个月";
                } else if (timespan.Years > 0) {
                    _this.OrderInfo.Age = timespan.Years + "岁";
                }
                _this.OrderInfo.Birthday = curVal;
                console.log(_this.OrderInfo.Age);　
            }
        },
        created: function() {
            this.GetDoctors(); //获取所有的医生信息       
        },
        methods: {
            GetDoctors: function() { //获取所有的医生信息
                var _this = this;
                VueHttpGet(PageUrlModel.GetDoctors, { openid: LayoutModel.OpenId, dataSourceType: 2 }, function(result) {
                    if (result.IsSuccess) {
                        _this.DoctorList = result.ResultData.Result;
                        _this.DoctorList.push({ id: 0, value: "其他", parentId: 0 });
                    }
                });
            },
            choiceDoctor: function() {
                var _this = this;
                console.log(this.DoctorList);
                var obj = _this.DoctorList;
                var iosSelect = new IosSelect(1, [obj], {
                    oneLevelId: _this.OrderInfo.TherapyDoctorId,
                    title: '选择医生',
                    itemHeight: 35,
                    relation: [1, 1, 0, 0],
                    callback: function(objOne) {
                        _this.OrderInfo.TherapyDoctorId = objOne.id;
                        _this.OrderInfo.TherapyDoctorName = objOne.value;
                        _this.SelectDoctorInfo = objOne;
                        // _this.doctor = objOne.value;
                        _this.doctorCls = _this.validClr;
                    }
                });
            },
            showSex: function() { //选择性别
                this.showBlack = true;
                this.sexStyle = {
                    height: '125px',
                    bottom: '20px'
                };
            },
            choiceSex: function(p_sex) { //选择性别关闭事件
                this.sex = p_sex;
                if ('女' == p_sex) {
                    this.OrderInfo.Gender = 2;
                } else {
                    this.OrderInfo.Gender = 1;
                }
                this.sexCls = 'valid-color';
                this.clickBlack();
            },
            clickBlack: function() {
                this.showBlack = false;
                this.sexStyle = {
                    height: '0px',
                    bottom: '0px'
                };
            },
            SubmitValidate: function() { //提交验证
                var _this = this;
                if (Rayelink.isEmpty(_this.OrderInfo.UserName) || _this.OrderInfo.UserName.byteLength() <= 2 || _this.OrderInfo.UserName.isChineseText() == false) {
                    _this.ShowMsg("请输入正确的姓名");
                    _this.NextBtnClass = _this.NextBtnClass_02; //置灰
                    return false;
                } else if (_this.OrderInfo.Gender != 1 && _this.OrderInfo.Gender != 2) {
                    _this.ShowMsg("请选择性别");
                    return false;
                } else if (Rayelink.isEmpty(_this.age) || '请选择出生日期' == _this.age) {
                    _this.ShowMsg("请选择年龄");
                    return false;
                } else
                if (_this.OrderInfo.ProvinceId == 0 || _this.OrderInfo.CityId == 0 || _this.OrderInfo.DistrictId == 0) {
                    _this.ShowMsg("请选择所在地区 ");
                    return false;
                } else if (_this.OrderInfo.TherapyDoctorId === -1) {
                    _this.ShowMsg("请选择主刀专家");
                    return false;
                } else if (Rayelink.isEmpty(_this.OrderInfo.UserMobile) || !Rayelink.isPhone(_this.OrderInfo.UserMobile)) {
                    _this.ShowMsg("请输入正确的手机号 ");
                    return false;
                } else if (Rayelink.isEmpty(_this.code)) {
                    _this.ShowMsg("请输入正确的验证码");
                    return false;
                }
                _this.OrderInfo.WechatOrderId = _this.code; //这里将验证码先放在这里面
                _this.NextBtnClass = _this.NextBtnClass_01; //不置灰
                return true;
            },
            ShowMsg: function(msg) {
                alert(msg);
            },
            GetVerificationCode: function(params) { //获取验证码
                var _this = this;
                var codeBtn = document.getElementById("get_code");
                if (codeBtn.innerText == '获取验证码') {
                    if (Rayelink.isEmpty(_this.OrderInfo.UserMobile) || !Rayelink.isPhone(_this.OrderInfo.UserMobile)) {
                        _this.ShowMsg("请输入正确的手机号");
                        return false;
                    }
                    VueHttpPost(PageUrlModel.SendMessageValidCode, { mobile: _this.OrderInfo.UserMobile, type: LayoutModel.CampaignSid }, function(result) {
                        if (result.success) {
                            DialogOkTip(result.message);
                            countDown("get_code");
                        } else {
                            _this.ShowMsg(result.message);
                        }
                    });
                }
            },
            choiceAddress: function() {
                var _this = this;
                var iosSelect = new IosSelect(3, [iosProvinces, iosCitys, iosCountys], {
                    oneLevelId: _this.OrderInfo.ProvinceId,
                    twoLevelId: _this.OrderInfo.CityId,
                    threeLevelId: _this.OrderInfo.DistrictId,
                    title: '地址选择',
                    itemHeight: 35,
                    relation: [1, 1, 0, 0],
                    callback: function(objOne, objTwo, objThree) {
                        _this.OrderInfo.ProvinceId = objOne.id;
                        _this.OrderInfo.ProvinceName = objOne.value;

                        _this.OrderInfo.CityId = objTwo.id;
                        _this.OrderInfo.CityName = objTwo.value;

                        _this.OrderInfo.DistrictId = objThree.id;
                        _this.OrderInfo.DistrictName = objThree.value;

                        _this.addressCls = _this.validClr;
                    }
                });
            },
            next: function() {
                var _this = this;
                if (_this.IsSubmit && _this.NextBtnClass === _this.NextBtnClass_01) {
                    if (_this.SubmitValidate()) {
                        _this.IsSubmit = false;
                        VueHttpPost(PageUrlModel.SubmitOrder, _this.OrderInfo, function(result) {
                            if (result.IsSuccess) {
                                location.replace(result.ReturnUrl);
                            } else {
                                _this.ShowMsg(result.Text);
                                _this.IsSubmit = true;
                            }
                        });
                    }
                }
            },
            ShowMsg: function(mag) {
                DialogNoTip(mag);
            },
            CloseCode: function() {
                this.code = '';
            },
            CloseMobile: function() {
                this.OrderInfo.UserMobile = '';
            },
            CloseDesc: function() {
                this.OrderInfo.Description = '';
            },
            CloseUserName: function() {
                this.OrderInfo.UserName = '';
                this.NextBtnClass = this.NextBtnClass_02; //置灰
            }
        }
    })
})()