(function() {
    var vm = new Vue({
        el: '#referral-list',
        data: {
            tab: true, //true: 选中第一个tab
            options: {
                nowReferral: [],
                overReferral: [],
                All: []
            },
            DataList: [],
            ShowNoDataBox: false,
            PageModel: {
                Index: 0, //当前页数
                Size: 20, //每页显示的个数
                PageCount: 0, //总页数
                DataCount: 0 //总数据

            }
        },
        mounted: function() {
            showHtml();
            this.GetOrderList();
        },
        watch: {
            tab(curVal, oldVal) {
                //  this.DataList = this.options[this.tab ? 'nowReferral' : 'overReferral'];
                if (this.tab === true) {
                    this.DataList = this.options[this.tab ? 'nowReferral' : 'overReferral'];
                    this.ShowNoDataBox = this.DataList.length == 0;
                } else {
                    this.PageModel.Index = 0;
                    this.GetPageDate();
                }
            }
        },
        methods: {
            changeTab: function(p_index) {
                //切换初始化
                this.tab = p_index;
            },
            PagingFunction() {
                var _this = this;
                if (Rayelink.IsScrolBottom()) {
                    _this.GetPageDate();
                }
                //_this.options.overReferral.push(order);
            },
            GetPageDate: function() {
                var _this = this;
                if (_this.PageModel.Index < _this.PageModel.PageCount) {
                    _this.PageModel.Index = _this.PageModel.Index + 1;
                    _this.DataList = Enumerable.From(_this.options.All).Take(_this.PageModel.Size * _this.PageModel.Index).ToArray();
                }
                _this.ShowNoDataBox = _this.DataList.length == 0;
            },
            GetOrderList: function() {
                var _this = this;
                VueHttpGet(PageUrlModel.OrderList, { doctorId: PageModel.DoctorId, assistantId: PageModel.AssistantId }, function(result) {
                    if (result.IsSuccess) {
                        var msgList = result.ResultData.Result;
                        if (msgList) {
                            if (typeof(msgList) == "string") msgList = eval(msgList);
                            for (var index = 0; index < msgList.length; index++) {
                                var order = msgList[index];
                                //console.log(order);
                                _this.PushData(order);
                            }
                            //从小到大排序
                            _this.options.nowReferral.sort(function(a, b) {
                                return a.SortPosition - b.SortPosition;
                            });

                            _this.options.overReferral.sort(function(a, b) {
                                return a.SortPosition - b.SortPosition;
                            });

                            window.onscroll = function() {　　
                                if (_this.tab === false) {
                                    _this.PagingFunction();
                                }
                            };

                            /**************分页计算***************/
                            _this.PageModel.DataCount = _this.options.All.length;
                            _this.PageModel.PageCount = parseInt(_this.PageModel.DataCount / _this.PageModel.Size);
                            if (_this.PageModel.DataCount % _this.PageModel.Size > 0) _this.PageModel.PageCount += 1;
                            /**************分页计算***************/

                            if (_this.tab === true) {
                                _this.DataList = _this.options[_this.tab ? 'nowReferral' : 'overReferral'];
                            } else {
                                _this.GetPageDate();
                            }
                            _this.ShowNoDataBox = _this.DataList.length == 0;
                        } else {
                            _this.ShowNoDataBox = true;
                        }
                    } else {
                        _this.ShowNoDataBox = true;
                    }
                });
            },
            PushData: function(order) {
                var _this = this;
                order.IsRed = true;
                order.SortPosition = 1; //排序位置
                if (order.OrderDate) {
                    order.OrderDate = FDate(order.OrderDate.replace('T', ' '));
                    var date = order.OrderDate.formatDate('YYYY-MM-DD');
                    if (date == new Date().formatDate('YYYY-MM-DD')) //当天的
                    {
                        date = order.OrderDate.formatDate('hh:mm');
                    }
                    order.Date = date;
                }
                if (order.Status == 3 && (order.ConversationStatus == 7 || order.ConversationStatus < 3)) { //看诊中
                    if (null == order.MessageTime) {
                        order.IsRed = false;
                        order.SortPosition = 2;
                    }
                    if (PageModel.OrderSid == order.OrderSid) {
                        _this.tab = true;
                        setTimeout(function() {
                            backTop(PageModel.OrderSid);
                        }, 10);
                    }
                    _this.options.nowReferral.push(order);
                }
                if ((order.Status >= 8 && order.Status <= 9)) { //已结束
                    order.IsRed = false;
                    if (PageModel.OrderSid == order.OrderSid) {
                        _this.tab = false;
                        setTimeout(function() {
                            backTop(PageModel.OrderSid);
                        }, 10);
                    }
                    _this.options.overReferral.push(order);
                    _this.options.All.push(order);
                }
            },
            GoChatPage: function(event, _item) {
                location.href = PageUrlModel.Chat + "&orderSId=" + _item.OrderSid;
            }
        },
    })
})(Vue)