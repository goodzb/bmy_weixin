/**
 * Created by chesw on 2017/4/26.
 */
(function() {
    new Vue({
        el: '#referral-service',
        data: {
            tab: true, //true：显示图文，false: 显示远程
            rayeObject: {
                text: [
                    { value: '三甲医院', style: { width: '25%' }, index: 0 },
                    { value: '名医在线', style: { width: '25%' }, index: 0 },
                    { value: '极速审核', style: { width: '25%' }, index: 0 },
                    { value: '对症咨询', style: { width: '25%' }, index: 0 },
                    { value: '未回复自动退款', style: { width: '40%' }, index: 0 }
                ],
                long: [
                    { value: '权威专家', style: { width: '25%' }, index: 1 },
                    { value: '无需挂号', style: { width: '25%' }, index: 1 },
                    { value: '无需进沪', style: { width: '25%' }, index: 1 },
                    { value: '远程诊疗', style: { width: '25%' }, index: 1 },
                    { value: '省心省力省钱', style: { width: '40%' }, index: 1 }
                ]
            },
            orderNotice: {
                text: [
                    { value: '本次咨询为付费咨询，由医生决定结束时间。', seq: 1 },
                    { value: '为珍惜您的咨询机会，请全部围绕病情进行沟通，避免无关内容。', seq: 2 },
                    { value: '如果付费24小时内，医生没有进行答复，订单会全额退款至您的账户内。', seq: 3 }
                ],
                long: [
                    { value: '提交订单并支付成功，收到远程门诊预约卡号和密码的短信。', seq: 1 },
                    { value: '下单成功后，会有客服人员及时联系您，确认看诊事宜。', seq: 2 },
                    { value: '看诊当天携带病例，按照预约时间前往服务地点。', seq: 3 },
                    { value: '到达当地远程门诊服务点，向当地医生助理出示远程门诊卡号和密码的短信，进行远程看诊。', seq: 4 }
                ]
            },
            PayCost: PageModel.PayCost[0],
            PayCount: 0 //付费总数人
        },
        computed: {
            rayeList: function() {
                return this.rayeObject[this.tab ? 'text' : 'long']
            },
            noticeList: function() {
                return this.orderNotice[this.tab ? 'text' : 'long']
            }
        },
        mounted: function() {
            showHtml()
        },
        methods: {
            changeTab: function(p_flag) { //切换tab
                if (p_flag == this.tab) return;
                if (p_flag) {
                    this.PayCost = PageModel.PayCost[0];
                } else {
                    this.PayCost = PageModel.PayCost[1];
                }
                this.tab = p_flag;
            },
            pay: function() {
                var _url = this.tab ? PageUrlModel.ConsultPay : PageUrlModel.RemotelyPay;
                location.href = _url;
            },
            GoExpertApp: function() {
                location.href = PageUrlModel.ExpertApp;
            }
        }
    })


})()