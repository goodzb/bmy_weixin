/**
 * Created by chesw on 2017/4/27.
 */
(function() {
    new Vue({
        el: '#client-chat',
        data: {
            param: {
                Content: '取消后，医生将无法为您提供在线图文复诊咨询服务。您确定要取消吗？',
                LeftText: '取消订单',
                LeftFunc: null,
                RightText: "我在想想",
                RightFunc: null,
                Show: false
            },
            ChatConnection: {}, //当前连接对象
            MessageContent: '',
            MessageInfo: PageModel.MessageInfo, //当前需要发送的消息对象
            MessageList: [], //当前展示的消息列表

            Message_Left_TextList: [], //当前展示的消息列表(左-文本)
            Message_Left_ImageList: [], //当前展示的消息列表(左-图片)
            Message_Left_AudioList: [], //当前展示的消息列表(左-语音)
            Message_Right_TextList: [], //当前展示的消息列表(右-文本)
            Message_Right_ImageList: [], //当前展示的消息列表(右-图片)
            Message_Right_AudioList: [], //当前展示的消息列表(右-语音)

            PageSource: PageModel.PageSource,
            OrderInfo: PageModel.OrderInfo, //当前订单信息
            DoctorInfo: PageModel.DoctorInfo, //当前医生信息
            UserInfo: PageModel.UserInfo, //当前用户信息
            ReVisitUser: PageModel.ReVisitUser, //当前助理对象信息
            ReceiveMessageInfo: null, //接收到的新消息对象            
            ShowEndIco: false, //是否显示结束图标按钮    
            ShowRecordIco: false, //是否显示录音的按钮图标
            ShowRecording: false, //是否显示正在录音的按钮图标
            IsConversation: false, //是否可以会话
            Voice: { localId: '', serverId: '', Status: 0 },
            Images: { localId: [], serverId: [] },
            Media: { localId: '', serverId: '', Status: 0 },
            LastMessageDate: null, //最后一条消息的时间
            Dateflg: false,
            ChatState: 0,
        },
        components: {
            'dialog-confirm': DialogConfirm
        },
        mounted: function() {
            var _this = this;
            showHtml();
            _this.PageLoad();
            _this.HoldStart();
            var inputBox = document.getElementById("inputBox");
            autoTextarea(inputBox);

            document.onkeydown = function() {
                var oEvent = window.event;
                if (oEvent.keyCode == 13) {
                    _this.ClickSendBtn();
                }
            }
            _this.OnErrorFun();
            _this.focusScroll();
            _this.ScrollEnd();
        },
        watch: {
            MessageContent(curVal, oldVal) {
                var _this = this;
                if (curVal) {
                    _this.MessageContent = curVal.ReplaceEmoji().Trim();
                }
            },
            OrderInfo(curVal, oldVal) {
                var _this = this;
                if (curVal.Status === 8 || (curVal.ConversationStatus >= 3 && curVal.ConversationStatus <= 6)) {
                    _this.ShowEndIco = false;
                    _this.IsConversation = false;
                }
                _this.PageStatusManager();
                _this.IsConversationStatus();
            },
        },
        methods: {
            PageLoad: function() {
                if (this.OrderInfo && this.DoctorInfo && this.UserInfo) {
                    this.Status = 1;
                    this.MonitorConnection(); //监控连接
                    this.PageStatusManager();
                    this.ReadHistoryConversation(); //已读未读消息
                }
            },
            PageStatusManager: function() {
                var _this = this;
                if (PageModel.PageSource != 0 && _this.OrderInfo.ConversationStatus === 7) {
                    _this.ShowEndIco = true; //是否显示结束图标按钮
                }
            },
            IsConversationStatus: function() {
                var _this = this;
                if (_this.OrderInfo.Status === 3 && !(_this.OrderInfo.ConversationStatus >= 3 && _this.OrderInfo.ConversationStatus <= 6)) {
                    _this.IsConversation = true; //是否可以会话
                } else {
                    _this.IsConversation = false; //是否可以会话
                    console.log('不需要会话功能');
                }
            },
            MonitorConnection: function() { //监控连接
                var _this = this;
                var monitor = setInterval(function() {
                    if (typeof($) === "function") {
                        _this.GetHistoryConversation(); //加载历史消息
                        clearInterval(monitor);
                        if (_this.OrderInfo.Status === 3 && !(_this.OrderInfo.ConversationStatus >= 3 && _this.OrderInfo.ConversationStatus <= 6)) {
                            _this.Connection();
                        }
                    }
                }, 200);
            },
            Connection: function() {
                var _this = this;
                //引用自动生成的集线器代理
                if ($.connection) {
                    var chat = $.connection.fzChatHub;
                    if (chat) {
                        chat.connection.qs = { OpenId: LayoutModel.OpenId, orderSid: _this.OrderInfo.OrderSid }; //设置参数

                        chat.client.ReceiveMessage = _this.ReceiveMessage; //接受到服务器发过来的消息
                        chat.client.SendSucessCallBack = _this.SendSucessCallBack; //回执发送消息成功
                        chat.client.ReceiveOverOrder = _this.ReceiveOverOrder; //用户接受到结束的通知
                        chat.client.TriggerOnLineAction = _this.TriggerOnLineAction; //心跳检测到还在线的执行函数
                        chat.client.OnLineMonitoring = _this.OnLineMonitoring;
                        $.connection.hub.logging = true;
                        var tempFun = null,
                            tempFun1 = null;

                        $.connection.hub.start({}, function() {
                            // $.connection.hub.received(function(data) {  //收到消息
                            //console.log(data);
                            // });
                            $.connection.hub.error(function(error) {
                                //console.warn(error);
                            });
                            // $.connection.hub.reconnected(function() { //重新连接
                            //     console.log('Reconnected');
                            // });
                            // connected    1 连接中(正常连接的状态)
                            // connecting   0 正在连接
                            // disconnected 4 断开
                            // reconnecting 2 重新连接
                            $.connection.hub.stateChanged(function(change) {
                                //alert("____" + _this.ChatState);
                                if (change.newState === $.signalR.connectionState.disconnected) {
                                    _this.IsConversation = false;
                                    if (tempFun) clearInterval(tempFun);
                                    if (tempFun1) clearInterval(tempFun1);
                                    _this.AlertError('与服务器的连接发生异常，正在尝试重新连接!');
                                    tempFun = setInterval(function() {
                                        //alert("+++++" + _this.ChatState);
                                        if (change.newState === 4) {
                                            _this.AlertError('与服务器的连接发生异常，正在尝试重新连接!');
                                            _this.Connection();
                                        }
                                    }, 4000);

                                } else if (change.newState === 2) {
                                    _this.AlertError('与服务器的连接已断开,正在尝试重新连接!');
                                    if (_this.ChatState == 1) {
                                        _this.IsConversation = false;
                                        if (tempFun1) clearInterval(tempFun);
                                        tempFun1 = setInterval(function() {
                                            _this.AlertError('与服务器的连接已断开,正在尝试重新连接!');
                                        }, 4000)
                                    }
                                } else if (change.newState === 0) {
                                    _this.AlertError('正在连接中，请稍后...');
                                } else {
                                    if (tempFun) clearInterval(tempFun);
                                    if (tempFun1) clearInterval(tempFun1);
                                }
                                _this.ChatState = change.newState;
                            });

                        }).done(function(content) { // 启动连接,这里和1.0也有区别
                            console.log('连接成功:state=' + content.state + ',contentId=' + content.id);
                            _this.ChatState = content.state;
                            _this.IsConversationStatus(); //是否可以会话的监控
                            _this.ChatConnection.Chat = chat;
                            _this.ChatConnection.ConnectionId = content.id;
                            _this.OrderInfo.SenderConnectionId = content.id;
                            _this.AutoShowOrderDescription(); //连接成功后自动展示用户填写描述信息

                            //每隔5秒，发送心跳包信息,重新刷新订单状态
                            var thb = setInterval(function() {
                                if (_this.IsConversation) {
                                    chat.server.triggerHeartbeat(); //心跳包检测
                                } else {
                                    clearInterval(thb);
                                    console.log('关闭了订单，后续心跳检测停止')
                                }
                            }, 20000);

                        }).fail(function(error) {
                            _this.IsConversation = false; //是否可以会话
                            _this.AlertError('与服务器的连接发生异常，请尝试刷新页面后重试!');
                            //console.log('连接出错:' + error)
                            //alert("连接失败");
                        });
                    }
                } else {
                    _this.Connection();
                }
            },
            SendClientMessage: function(msg) { //给服务端发送一对一对话
                var _this = this;
                var chat = _this.ChatConnection.Chat;
                chat.server.sendClientMessage(_this.ChatConnection.ConnectionId, msg);
            },
            ReceiveMessage: function(connectionId, message) { //接受到服务器发过来的消息
                var _this = this;
                console.log("接受到消息:对方connectionId=" + connectionId);
                _this.ReceiveMessageInfo = message;
                if (message.ChatType === 1) { //图片会存在多个图片连续造成对象赋值乱了
                    _this.ReceiveMessageInfo.CreateTime = new Date();
                    _this.AppendMsg(_this.ReceiveMessageInfo);
                } else {
                    _this.RefreshOrder(function() { //刷新订单信息
                        _this.ReceiveMessageInfo.CreateTime = new Date();
                        _this.AppendMsg(_this.ReceiveMessageInfo);
                    });
                }
            },
            ShowErrorMessage: function(message) { //显示错误消息
                DialogTip({ url: "no", text: message.Content });
            },
            TempMessageInfo: function(info) {
                var _this = this;
                var item = { Source: info.Source, ChatType: info.ChatType, Content: info.Content, Date: info.CreateTime, TipContent: info.TipContent, MediaId: info.MediaId, CreateTime: info.CreateTime, FDate: FDate(info.CreateTime).formatDate('YYYY-MM-DD') };
                if (null == item.TipContent) item.TipContent = '';

                var newDate = FDate(item.CreateTime);
                var timeSpan = newDate.Sub(new Date());
                //每天第一条显示日期，后面的每隔五分钟显示时间         

                if (_this.MessageList.length === 0) { //一定是第一条
                    newDate = newDate.formatDate('MM-DD hh:mm');
                } else {
                    if (_this.MessageList[0].FDate != item.FDate && _this.Dateflg === false) { //跨天的第一条
                        _this.Dateflg = true;
                        newDate = newDate.formatDate('MM-DD hh:mm');
                    } else {
                        var date3 = newDate.getTime() - _this.LastMessageDate.getTime() //时间差的毫秒数
                        if (date3 < 1000 * 60 * 5) { // 五分钟
                            newDate = null;
                        } else {
                            newDate = newDate.formatDate('hh:mm');
                        }
                    }
                }
                _this.LastMessageDate = FDate(item.CreateTime);
                item.Date = newDate;
                switch (item.ChatType) { //聊天类别：0-文字、1-图片、2-语音、3-文件 
                    case 0:
                        _this.ShowMessage_Text(item);
                        break;
                    case 1:
                        _this.ShowMessage_Image(item);
                        break;
                    case 2:
                        _this.ShowMessage_Audio(item);
                        break;
                }
                //来源：0-用户发送、1-医生发送、2-系统发送，不在用户展示页面（用户身份）、
                //3-系统发送，不在用户展示页面（医生身份） 、
                //4-系统发送，在用户展示页面（医生身份）、
                //5-系统发送，在用户展示页面（用户身份）、
                //10-系统错误错误消息
            },
            ShowMessage_Text: function(info) { //文本
                var _this = this;
                info.Location = 0;
                //var index = PageModel.PageSource === 0 ? 0 : 1;
                //  var id = _this.Carrier_Text[index][info.Source];
                if (PageModel.PageSource === 0) { //用户
                    if (info.Source == 1 || info.Source == 4) { //左边
                        _this.Message_Left_TextList.push(info);
                        info.Location = 1;
                    } else if (info.Source == 0) { //右边
                        _this.Message_Right_TextList.push(info);
                        info.Location = 4;
                    }
                } else {
                    if (info.Source == 1 || info.Source == 4 || info.Source == 5) { //右边
                        _this.Message_Right_TextList.push(info);
                        info.Location = 4;
                    } else if (info.Source == 0) { //左边
                        _this.Message_Left_TextList.push(info);
                        info.Location = 1;
                    }
                }
                _this.MessageList.push(info);
                _this.ScrollEnd(); //滚动条到最底下
            },
            ShowMessage_Image: function(info) { //图片
                var _this = this;
                info.Location = 0;
                if (PageModel.PageSource === 0) { //用户
                    if (info.Source == 1 || info.Source == 4) { //左边
                        info.Location = 2;
                        _this.Message_Left_ImageList.push(info);
                    } else if (info.Source == 0) { //右边
                        _this.Message_Right_ImageList.push(info);
                        info.Location = 5;
                    }
                } else {
                    if (info.Source == 1 || info.Source == 4) { //右边
                        _this.Message_Left_ImageList.push(info);
                        info.Location = 5;
                    } else if (info.Source == 0) { //左边
                        _this.Message_Right_ImageList.push(info);
                        info.Location = 2;
                    }
                }
                _this.MessageList.push(info);
                _this.ScrollEnd(); //滚动条到最底下
            },
            ShowMessage_Audio: function(info) { //音频
                var _this = this;
                info.Location = 5;
                if (PageModel.PageSource === 0) { //用户
                    if (info.Source == 1) { //左边
                        _this.Message_Left_AudioList.push(info);
                        info.Location = 3;
                    } else if (info.Source == 0) { //右边
                        _this.Message_Right_AudioList.push(info);
                        info.Location = 6;
                    }
                } else {
                    if (info.Source == 1) { //右边
                        _this.Message_Left_AudioList.push(info);
                        info.Location = 6;
                    } else if (info.Source == 0) { //左边
                        _this.Message_Right_AudioList.push(info);
                        info.Location = 3;
                    }
                }
                _this.MessageList.push(info);
                _this.ScrollEnd(); //滚动条到最底下
            },
            HoldStart: function() { //按下说话按钮
                var _this = this;
                var holdStartY, holdMoveY;
                var holdToTalk = document.getElementById("holdToTalk"),
                    voicePromptBox = document.getElementById("voicePromptBox"), //语音提示盒子
                    voicePrompt = document.getElementById("voicePrompt"),
                    cancelVoice = document.getElementById("cancelVoice"),
                    voicePromptText = document.getElementById("voicePromptText");

                holdToTalk.addEventListener('touchstart', function(event) { //点击，启动滑动的距离计算
                    event.preventDefault();
                    holdStartY = event.targetTouches[0].clientY;
                }, false);

                holdToTalk.addEventListener('touchmove', function(event) { //手势上滑,取消录音，不发消息
                    event.preventDefault();
                    holdMoveY = event.targetTouches[0].clientY;
                    if (holdStartY - holdMoveY > 130) {
                        voicePrompt.style.display = "none";
                        cancelVoice.style.display = "block";
                        voicePromptText.innerHTML = "松开手指，取消发送";
                        voicePromptText.setAttribute('class', 'cancelActive');
                        if (_this.ShowRecording) _this.CancelRecord(); //取消录音
                    }
                }, false);

                holdToTalk.addEventListener('touchend', function(event) { //结束手势，停止录音，上传文件    
                    event.preventDefault();
                    voicePromptBox.style.display = "none";
                    voicePrompt.style.display = "block";
                    cancelVoice.style.display = "none";
                    voicePromptText.innerHTML = "手指上滑，取消发送";
                    voicePromptText.removeAttribute('class', 'cancelActive');
                    if (_this.ShowRecording) _this.StopRecord(); //停止录音，上传文件

                }, false);

                var hammertime = new Hammer(holdToTalk);

                hammertime.on('press', function(event) { //长按事件，开发录音
                    console.log("录音");
                    _this.StartRecord(function() {
                        voicePromptBox.style.display = "block";
                    });
                });
            },
            ScrollEnd: function() {
                this.StartCheckScroll();
            },
            StartRecord: function(successCallBack) { //开始录音
                var _this = this;
                if (null != wx && undefined != typeof(wx) && "undefined" != typeof(wx)) {
                    wx.startRecord({
                        cancel: function() {
                            _this.AlertError("用户拒绝授权录音");
                        },
                        success: function() {
                            _this.ShowRecording = true;
                            if (successCallBack) {
                                successCallBack();
                            }
                        },
                        complete: function(res) {
                            //alert(res);
                        }
                    });
                    // 监听录音自动停止
                    wx.onVoiceRecordEnd({
                        complete: function(res) {
                            _this.Voice.localId = res.localId;
                            _this.AlertError("录音时间已超过一分钟"); // 录音时间超过一分钟没有停止的时候会执行 complete 回调
                            _this.MessageInfo.ChatType = 2; //语音
                            document.getElementById("voicePromptBox").style.display = "none";
                            _this.UploadVoice();
                        }
                    });
                } else {
                    _this.AlertError("亲，请在微信端录音");
                }
            },
            CancelRecord: function(callBack) { //取消录音
                var _this = this;
                _this.ShowRecording = false;
                wx.stopRecord({
                    success: function(res) { if (callBack) callBack(); }
                });
            },
            StopRecord: function(callBack) { //停止录音
                var _this = this;
                _this.ShowRecording = false;
                wx.stopRecord({
                    success: function(res) {
                        _this.MessageInfo.ChatType = 2; //语音
                        _this.Voice.localId = res.localId;
                        _this.UploadVoice();
                        if (callBack) callBack();
                    },
                    fail: function(res) {
                        _this.AlertError("录音出现错误，请重试");
                        // alert(JSON.stringify(res));
                    }
                });
            },
            UploadVoice: function() { //上传语音
                var _this = this;
                if (_this.Voice.localId == '') {
                    return;
                }
                wx.uploadVoice({
                    isShowProgressTips: 1, // 默认为1，显示进度提示
                    localId: _this.Voice.localId,
                    success: function(res) {
                        //alert('上传语音成功，serverId 为' + res.serverId);
                        _this.Voice.serverId = res.serverId;
                        _this.Media.serverId = res.serverId;
                        _this.SucessMedia('amr');
                    }
                });
            },
            SucessMedia: function(suffix) {
                var _this = this;
                _this.RefreshOrder(function() { //需要重新更新一个订单的状态才能视情况确定是否要发送
                    _this.Parameter_Madia();
                    _this.MessageInfo.MediaId = _this.Media.serverId;

                    // alert('开始下载文件');
                    _this.DownloadMedia(_this.MessageInfo.MediaId, suffix, function(res) { // 将媒体文件下载至本地服务器
                        //alert(JSON.stringify(res));
                        _this.MessageInfo.Content = res.Text;
                        _this.SendMessageInfo(_this.MessageInfo);
                    }, function() {
                        if (suffix == "amr") {
                            _this.AlertError("录音时间过短，请尝试重试");
                        }
                    });
                });
            },
            ClickSendBtn: function() { //点击发送按钮
                var _this = this;
                if (Rayelink.isEmpty(_this.MessageContent)) { //空文本不发发送
                    return false
                }
                _this.MessageInfo.ChatType = 0; //文本
                _this.RefreshOrder(function() { //需要重新更新一个订单的状态才能视情况确定是否要发送
                    _this.MessageInfo.Content = _this.MessageContent;
                    _this.SendMessageInfo(_this.MessageInfo, function() {
                        _this.MessageContent = "";
                        var InputBox = document.getElementById("inputBox");
                        InputBox.style.height = '26px';
                    });
                });
                _this.ScrollEnd();
            },
            ChangeKeyBoardBox: function() { //切换录音与输入文本的按钮
                this.ShowRecordIco = !this.ShowRecordIco;
                if (!this.ShowRecordIco && _this.ShowRecording) {
                    this.StopRecord(); //进行停止录音操作
                }
            },
            SendMessageInfo: function(messageInfo, callBack) { //将一个消息发送给对方
                var _this = this;
                var newMessageInfo = messageInfo;
                if (null == newMessageInfo) {
                    newMessageInfo = _this.MessageInfo;
                }
                var chat = $.connection.fzChatHub;
                if (PageModel.PageSource === 0) { //用户进入
                    chat.server.receiverMessageByUser(newMessageInfo, _this.DoctorInfo.OpenId, _this.ReVisitUser.Id, _this.ReVisitUser.OpenId, _this.OrderInfo.UserName, _this.OrderInfo.ConversationStatus);
                } else if (PageModel.PageSource === 1 || PageModel.PageSource === 2) { //医生/助理进入
                    chat.server.receiverMessageByDoctor(newMessageInfo, _this.DoctorInfo.OpenId, _this.ReVisitUser.Id, _this.ReVisitUser.OpenId, _this.OrderInfo.UserName, _this.OrderInfo.ConversationStatus);
                }
                _this.MessageInfo.CreateTime = new Date();
                this.AppendMsg(_this.MessageInfo, function() {
                    _this.MessageContent = "";
                    var InputBox = document.getElementById("inputBox");
                    InputBox.style.height = '26px';
                });
            },
            AppendMsg: function(msg, callBack) {
                var _this = this;

                _this.TempMessageInfo(msg); //把这条消息展示出来
                if (callBack) { callBack() }
                _this.ScrollEnd(); //滚动条到最底下
            },
            ChooseImage: function() { //开始选择图片会话
                var _this = this;
                wx.chooseImage({
                    success: function(res) {
                        _this.Images.localId = res.localIds;
                        //alert('已选择 ' + res.localIds.length + ' 张图片');
                        _this.UploadImage();
                    }
                });
            },
            Parameter_Madia: function() {
                var _this = this;
                _this.MessageInfo.Content = "";
                _this.MessageInfo.TipContent = "";
            },
            UploadImage: function() {
                var _this = this;
                //_this.MessageInfo.ChatType = 1; //图片
                //_this.MessageInfo.Source = 0; //用户发送

                if (_this.Images.localId.length == 0) {
                    _this.AlertError('请先使用 chooseImage 接口选择图片');
                    return;
                }
                var i = 0,
                    length = _this.Images.localId.length;
                _this.Images.serverId = [];

                function upload() {
                    wx.uploadImage({
                        localId: _this.Images.localId[i],
                        success: function(res) {
                            i++;
                            // alert('已上传：' + i + '/' + length);
                            _this.Images.serverId.push(res.serverId);
                            _this.Media.serverId = res.serverId;

                            setTimeout(function() {
                                _this.DownLoadImages(res.serverId);
                            }, 350);

                            if (i < length) {
                                upload();
                            } else {
                                // _this.DownLoadImages(_this.Images.serverId);
                            }
                        },
                        fail: function(res) {
                            //alert(JSON.stringify(res));
                        }
                    });
                }
                upload();
            },
            DownLoadImages: function(serverId) {
                var _this = this;
                $.post(PageUrlModel.DownLoadImages, { openid: LayoutModel.OpenId, pics: serverId }, function(result) {
                    // alert(JSON.stringify(result))
                    if (result.IsSuccess) {
                        var newPic = result.ResultData.Result;
                        var newMessage = _this.MessageInfo;
                        newPic.forEach(function(element) {
                            //setTimeout(function() {
                            newMessage.ChatType = 1; //图片
                            newMessage.Content = element;
                            _this.SendMessageInfo(newMessage);
                            // }, 1000);
                        }, this);
                    } else {
                        _this.AlertError(result.Text);
                    }
                });
            },
            GetHistoryConversation: function() { //获取历史消息
                var _this = this;
                VueHttpGet(PageUrlModel.GetHistoryConversation, null, function(result) {
                    if (result.IsSuccess) {
                        var msgList = result.ResultData.Result;
                        if (msgList) {
                            $.each(msgList, function(index, item) {
                                //   _this.MessageList.push(item);
                                _this.TempMessageInfo(item);
                            });
                            this.StartCheckScroll(800);
                        }
                    }
                });
            },
            OverOrder: function() {
                var _this = this;
                var chat = $.connection.fzChatHub;
                _this.param.Show = true;
                _this.param.Content = "结束咨询后，将不能继续与患者用户交流。是否结束此次咨询订单？";
                _this.param.LeftText = "确定结束";
                _this.param.RightText = "取消结束";

                _this.param.LeftFunc = function() {
                    VueHttpPost(PageUrlModel.OverOrder, { openid: LayoutModel.OpenId, orderSid: PageModel.OrderSid }, function(result) {
                        if (result.IsSuccess) {
                            chat.server.overOrder(_this.MessageInfo, _this.DoctorInfo.OpenId, _this.ReVisitUser.Id, _this.ReVisitUser.OpenId); //给用户发送订单结束咨询的通知
                            _this.ShowEndIco = false;
                            _this.IsConversation = false; //不可以再会话了
                            _this.param.Show = false;
                        } else {
                            _this.AlertError(result.Text);
                        }
                    });
                }

            },
            ReceiveOverOrder: function(_connectionId, _message) { //用户接收到到结束的通知
                console.log("用户接受到结束的通知:");
                var _this = this;
                _message.CreateTime = new Date();
                _this.AppendMsg(_message);
                _this.IsConversation = false; //不可以再会话了
            },
            OnLineMonitoring: function(text, connectionId) {
                var _this = this;
                var chat = _this.ChatConnection.Chat;
                _this.ChatConnection.ConnectionId = connectionId;
                _this.MessageInfo.SenderConnectionId = connectionId;
                chat.server.onLineMonitoringResponse(text, connectionId);
                console.log("收到服务器轮询text=" + text + ',connectionId=' + connectionId);
                return text;
            },
            ShowImgBox: function(_data) { //显示对话图片
                var originalBg = document.getElementById("originalBg");
                var clientChat = document.getElementById("client-chat");
                $("#ShowPic").attr("src", _data.Content);
                originalBg.addEventListener('touchmove', function(e) { e.preventDefault(); }, false);
                //document.documentElement.style.overflow = "hidden";
                originalBg.style.display = "block";
            },
            hideImgBox: function() { //隐藏对话图片
                var originalBg = document.getElementById("originalBg");
                var clientChat = document.getElementById("client-chat");
                originalBg.removeEventListener('touchmove', function(e) { e.preventDefault(); }, false);
                //document.documentElement.style.overflow = "scroll";
                originalBg.style.display = "none";
            },
            SendSucessCallBack: function(message) {
                var _this = this;
            },
            DownloadMedia: function(media_id, suffix, callback, error) { // 将媒体文件下载至本地服务器
                var _this = this;
                VueHttpPost(PageUrlModel.DownloadMedia, { media_id: media_id, suffix: suffix, openid: LayoutModel.OpenId }, function(result) {
                    // alert(JSON.stringify(result));
                    if (result.IsSuccess) {
                        if (callback) callback(result);
                    } else {
                        if (error) error(result);
                    }
                });
            },
            RefreshOrder: function(callback) { //刷新订单信息
                var _this = this;
                VueHttpGet(PageUrlModel.GetOrderInfo, { openid: LayoutModel.OpenId, orderSid: PageModel.OrderSid }, function(result) {
                    if (result.IsSuccess) {
                        _this.OrderInfo = result.ResultData.Result;
                        console.log('订单数据重新请求了');
                        _this.RefreshOrderStatus(callback);
                    }
                });
            },
            RefreshOrderStatus: function(callback) { //刷新订单状态
                if (callback) { callback() }
            },
            AutoShowOrderDescription: function() { //连接成功后自动展示用户填写描述信息
                var _this = this;
                var chat = _this.ChatConnection.Chat;
                if (_this.OrderInfo.ConversationStatus === 0) { //会话状态：0-未开始、1-医生未回复、2-用户未回复、3-用户取消、4-医生关闭、5-系统超时关闭、6-其他原因关闭、7：正在会话中
                    chat.server.showOrderDescription(_this.MessageInfo);
                }
            },
            ReadHistoryConversation: function() { //已读未读消息
                var _this = this;
                if (PageModel.PageSource !== 0) {
                    VueHttpPost(PageUrlModel.ReadHistoryConversation, { doctorOpenId: _this.DoctorInfo.OpenId, reVisitOpenId: _this.ReVisitUser.OpenId }, function(result) {

                    });
                }
            },
            PlayVoice: function(src, control) {
                if (src) {
                    AudioModel.Play(src, control);
                }
            },
            TriggerOnLineAction: function(connectionId, message, _orderInfo) { // 心跳检测到还在线的执行函数
                var _this = this;
                if (typeof(message) == 'string') {
                    console.log(message);
                }
                _this.ChatConnection.ConnectionId = connectionId;
                _this.MessageInfo.SenderConnectionId = connectionId;
                if (_orderInfo) {
                    console.log('通过心跳更新了订单');
                    _this.OrderInfo = _orderInfo;
                }
            },
            AlertOk: function(text) {
                DialogNoTip(DialogOkTip);
            },
            AlertError: function(text) {
                DialogNoTip(text);
            },
            CheckScroll: function() {
                var pageHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight);
                var viewportHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || 0;
                var scrollHeight = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

                var result = pageHeight - viewportHeight - scrollHeight < 20;
                // console.log('pageHeight=' + pageHeight + ' viewportHeight=' + viewportHeight + ' scrollHeight=' + scrollHeight + ' result=' + result);
                return result
            },
            StartCheckScroll: function(time) {
                var _this = this;
                time = time || 200;
                setTimeout(function() {
                    if (!_this.CheckScroll()) {
                        window.scrollTo(0, document.body.clientHeight);
                    }
                }, time);
            },
            OnErrorFun: function() {
                var _this = this;
                window.onerror = function(errorMessage, scriptURI, lineNumber, columnNumber, errorObj) {
                    var messageStr = "错误信息：" + errorMessage;
                    messageStr = messageStr + "出错文件：" + scriptURI;
                    messageStr = messageStr + "出错行号：" + lineNumber;
                    messageStr = messageStr + "出错列号：" + columnNumber;
                    messageStr = messageStr + "错误详情：" + errorObj;
                    //alert('出错了》错误信息：' + messageStr);
                    if ('Uncaught Error: SignalR: Connection must be started before data can be sent. Call .start() before .send()' == errorMessage) {
                        console.log("连接出错，尝试自动重新连接服务器。");
                        _this.IsConversation = false; //不可以再会话了
                        _this.Connection();
                    }
                }
            },
            focusScroll: function() { //输入框获取焦点时 滚动到最底部
                var _this = this;
                var inputBox = document.getElementById("inputBox");
                inputBox.onfocus = function() {
                    setTimeout(function() {
                        window.scrollTo(0, document.body.clientHeight + 1000);
                        inputBox.scrollIntoViewIfNeeded();
                    }, 500);
                }
            }
        }
    })
})()