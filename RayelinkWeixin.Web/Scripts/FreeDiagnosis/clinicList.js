if (!!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
    $(".information_time span").css({
        "width": "122px"
    });
    $(".information_time input").css({
        "width": "415px"
    });
    $(".information_section").css({
        "margin-left": "36px"
    });
}
var userId = getParameter('userId');
function getParameter(sProp) {
    var re = new RegExp(sProp + "=([^\&]*)", "i");
    var a = re.exec(document.location.search);
    if (a == null)
        return null;
    return a[1];
};

$('#datePicker').Zebra_DatePicker({
	always_visible: $('#dateWrap'),
    format: 'Y-m-d',
    //direction: 1, //往前可以选择
    //direction: [1, 10],//未来10天内可选择
    direction: [1, 30], //用日期表示取可选区间
    //disabled_dates: ['* * * *'] // 全部禁用
    onChange:function(){
		//注册点击完成之后自动隐藏日期控件的方法
        $(".dp_daypicker tr td").click(function () {
            $("#dateWrap").css("height",0);
            $(".information_time_date").css("background", "url(../../images/FreeDiagnosis/arrdown.png) no-repeat right center");
			$("#dateWrap").addClass("no-height");
        });
    }
});
$("#datePicker").click(function () {
    $(".box").css({
        "width": "",
        "height": "",
        "overflow": ""
    })
    if ($("#hospiital_list").hasClass("no_height")) {
        $("#hospiital_list").css("height", "0px");
        $("#hospiital_add").css("background", "url(../../images/accompany/arrdowup.png) no-repeat right center");
        $("#hospiital_list").removeClass("no_height");
        $(".costForm").show();
        $(".prompt").show();
    }
    if($("#dateWrap").hasClass("no-height")){
        $("#dateWrap").css("height","631px");
        $(".information_time_date").css("background", "url(../../images/FreeDiagnosis/arrdowup.png) no-repeat right center");
        $("#dateWrap").removeClass("no-height");
    }else{
        $("#dateWrap").css("height",0);
        $(".information_time_date").css("background", "url(../../images/FreeDiagnosis/arrdown.png) no-repeat right center");
        $("#dateWrap").addClass("no-height");
    }
});


//获取医院名字和地址效果
var clientH = $(window).height();
var h = clientH - $(".hospiital_list").offset().top - 153;
console.log(clientH);
$("#hospital_add").click(function () {
    if (!$("#dateWrap").hasClass("no-height")) {
        $("#dateWrap").css("height", 0);
        $(".information_time_date").css("background", "url(../../images/FreeDiagnosis/arrdown.png) no-repeat right center");
        $("#dateWrap").addClass("no-height");
    }
    $('body').animate({ scrollTop: 0 }, 500);
    if ($("#hospiital_list").hasClass("no_height")) {
        $("#hospiital_list").css("height","0px");
        $("#hospital_add").css("background", "url(../../images/FreeDiagnosis/arrdown.png) no-repeat right center");
        $("#hospiital_list").removeClass("no_height");
        $('.coutent').css("overflow","");
        $(".costForm").show();
        $(".prompt").show();
        $(".box").css({
            "width": "",
            "height": "",
            "overflow": ""
        })
    }else{
        $("#hospiital_list").css("height",h);
        $("#hospital_add").css("background", "url(../../images/FreeDiagnosis/arrdowup.png) no-repeat right center");
        $("#hospiital_list").addClass("no_height");
        $('.coutent').css("overflow","hidden");
        $(".costForm").hide();
        $(".prompt").hide();
        $(".box").css({
            "width": "100%",
            "height": clientH,
            "overflow": "hidden"
        })
    }

})
$("#hospiital_list div").click(function(){
    var hospiital_name=$(this).children("b").text();
    var hospital_add=$(this).children("p").text();
    $("#hospital_add>b").text(hospiital_name);
    $("#hospital_add>p").text(hospital_add);
    $("#hospiital_list").css("height","0px");
    $("#hospital_add").css("background", "url(../../images/FreeDiagnosis/arrdown.png) no-repeat right center");
    $("#hospiital_list").removeClass("no_height");
    $('.coutent').css("overflow","");
    $(".costForm").show();
    $(".prompt").show();
    $(".box").css({
        "width": "",
        "height": "",
        "overflow": ""
    })
    var hospital = $(this).find("b").html();
    var address = $(this).find("p").html();
    if (hospital != null || hospital != "") {
        localStorage.removeItem("fd_hospitalName");
        localStorage.setItem("fd_hospitalName", hospital);
    }
    if (address != "" || address != null) {
        localStorage.removeItem("fd_hospitalAddress");
        localStorage.setItem("fd_hospitalAddress", address);
    }
})
if ($("#ID").val().length > 0) {
    $(".ID_clear").css("visibility", "visible");
} else {
    $(".ID_clear").css("visibility", "hidden");
}

//清空身份证号码
$(".ID_clear").click(function(){
    $("#ID").val("");
    $(".ID_clear").css("visibility","hidden");
});
$('#ID').bind('input propertychange', function() {
    if($(this).val().length>0){
        $(".ID_clear").css("visibility", "visible");
    }else{
        $(".ID_clear").css("visibility", "hidden");
    }
});
$('#ID').bind('input click', function () {
    if ($(this).val().length > 0) {
        $(".ID_clear").css("visibility", "visible");
    } else {
        $(".ID_clear").css("visibility", "hidden");
    }
});
//清空手机号
$(".phone_clear").click(function(){
    $("#phone").val("");
    $(".phone_clear").css("visibility", "hidden");
});
$('#phone').bind('input propertychange', function() {
    if($(this).val().length>0){
        $(".phone_clear").css("visibility", "visible");
    }else{
        $(".phone_clear").css("visibility", "hidden");
    }
});
//清空验证码
$(".phoneCode_clear").click(function () {
    $("#code").val("");
    $(".phoneCode_clear").css("visibility", "hidden");
});
$('#code').bind('input propertychange', function () {
    if ($(this).val().length > 0) {
        $(".phoneCode_clear").css("visibility", "visible");
    } else {
        $(".phoneCode_clear").css("visibility", "hidden");
    }
});
//更换就诊人
$('.change_user').on('click', function () {
    window.location.href = "/User/ChooseUser?type=1";
});
var WebAPP = {
    photoIDs: [],
    photoServerIDs: []

};
wx.config({
    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    appId: '@jsSDKModel.appId', // 必填，公众号的唯一标识
    timestamp: '@jsSDKModel.timestamp', // 必填，生成签名的时间戳
    nonceStr: '@jsSDKModel.nonceStr', // 必填，生成签名的随机串
    signature: '@jsSDKModel.signature',// 必填，签名，见附录1
    jsApiList: ['chooseImage', 'previewImage', 'uploadImage'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
wx.ready(function () {
    //选择照片
    $('.share_file').on('click', function () {
        wx.chooseImage({
            count: 3 - WebAPP.photoIDs.length, // 默认9
            sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
            success: function (res) {
                var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
                var len = localIds.length;
                for (var i = 0; i < len; i++) {
                    var item = localIds[i];
                    WebAPP.photoIDs.push(item);
                    var html = '<span class="share_file custom_file_1"><img style="width:60px;height:60px;" src="' + item + '" alt="" /><i class="share_x"></i></span>';
                    $('.share_area').find('.no_file').before($(html));
                }
                localStorage.removeItem("fd_picList");
                localStorage.setItem("fd_picList", WebAPP.photoIDs);
                var $shareX = $('.share_area').find('.share_x');
                if ($shareX.length == 3) {
                    $('.no_file').addClass('shadow');
                }
                $shareX.off('click').on('click', function () {

                    for (i = 0; i < WebAPP.photoIDs.length; i++) {

                        if (WebAPP.photoIDs[i] == $(this).parent().find("img").attr("src")) {

                            WebAPP.photoIDs = WebAPP.photoIDs.slice(0, i).concat(WebAPP.photoIDs.slice(i + 1, WebAPP.photoIDs.length));

                            //alert(WebAPP.photoServerIDs.length)
                            //WebAPP.photoServerIDs = WebAPP.photoServerIDs.slice(0, i).concat(WebAPP.photoServerIDs.slice(i + 1, WebAPP.photoServerIDs.length));
                            //alert(WebAPP.photoServerIDs.length)
                        }
                    }
                    localStorage.removeItem("fd_picList");
                    localStorage.setItem("fd_picList", WebAPP.photoIDs);
                    $(this).parent().remove();
                    $('.no_file').removeClass('shadow');
                });
            }
        });
    });
});
function initForm() {
    var selectDate = localStorage.getItem("fd_selectDateText");
    if (selectDate != "undefined" && selectDate != null && selectDate!="") {
        $("#datePicker").val(selectDate)
    }
    $('#datePicker').Zebra_DatePicker({
        always_visible: $('#dateWrap'),
        format: 'Y年m月d日',
        defaultDate: selectDate,
        //direction: 1, //往前可以选择
        //direction: [1, 10],//未来10天内可选择
        direction: [1, 90], //用日期表示取可选区间
        //disabled_dates: ['* * * *'] // 全部禁用
        onChange: function (view, elements) {
            //注册点击完成之后自动隐藏日期控件的方法
            $(".dp_daypicker tr td").click(function () {
                if ($(this).not(".dp_disabled, .dp_not_in_month, .dp_weekend_disabled").length) {
                    $("#dateWrap").css("height", 0);
                    $(".information_time_date").css("background", "url(../../images/accompany/arrdown.png) no-repeat right center");
                    $("#dateWrap").addClass("no-height");
                }
            });
        },
        onSelect: function (dateText, inst) {
            localStorage.removeItem("fd_selectDateText");
            localStorage.setItem("fd_selectDateText", dateText);
        }
    });
    $('.dp_clear').on('click', function () {
        $("#datePicker").val("选择日期")
        localStorage.removeItem("fd_selectDateText");
    });
    //var sectionName = localStorage.getItem("fd_section");
    //if (sectionName) {
    //    $(".information_section").val(sectionName);
    //}

    var hospital = localStorage.getItem("fd_hospitalName");
    if (hospital != null && hospital != "undefined" && hospital != "") {
        $("#hospital_add b").html(hospital);
    }
    var address = localStorage.getItem("fd_hospitalAddress");
    if (address != null && address != "undefined" && address != "") {
        $("#hospital_add p").html(address);
    }
    if (userId == null) {
        var name = localStorage.getItem("fd_name");
        if (name != "undefined"&&name!=null&&name!="") {
            $("#name").val(name);
        }
        var phone = localStorage.getItem("fd_phone");
        if (phone != "undefined" && phone != null && phone != "") {
            $("#phone").val(phone);
        }
        var id = localStorage.getItem("fd_id");
        if (id!= "undefined" &&id != null &&id!="") {
            $("#ID").val(id);
        }
    }

    var description = localStorage.getItem("fd_description");
    if (description!= "undefined" &&description != null &&description!="") {
        $(".description").val(description);
    }

    var picLists = localStorage.getItem("fd_picList");
    if (picLists!= "undefined" &&picLists!=null&&picLists!="") {
        var ImageList = localStorage.getItem("fd_picList").toString().split(',');
        for (i = 0; i < ImageList.length; i++) {
            var src = ImageList[i];
            showPicList(src);
        }
    }
}


var check_emoji = function (message) {
    var checkMessage = /\\\ud[0-9a-f]{3}/;
    var GB2312UnicodeConverter = {
        ToUnicode: function (str) {
            return escape(str).toLocaleLowerCase().replace(/%u/gi, '\\u');
        }
        , ToGB2312: function (str) {
            return unescape(str.replace(/\\u/gi, '%u'));
        }
    };
    var unicode = GB2312UnicodeConverter.ToUnicode(message);
    if (checkMessage.test(unicode)) {
        return true;
    }
    else {
        return false;
    }
}
window.saveForm = function () {
    //var sectionParam = $(".information_section").val();
    //if (sectionParam != "" || sectionParam != null) {
    //    localStorage.removeItem("fd_section");
    //    localStorage.setItem("fd_section", sectionParam);
    //}
    var name = $("#name").val();
    if (name != null || name != "") {
        localStorage.removeItem("fd_name");
        localStorage.setItem("fd_name", name);
    }
    var phone = $("#phone").val();
    if (phone != null || phone != "") {
        localStorage.removeItem("fd_phone");
        localStorage.setItem("fd_phone", phone);
    }
    var id = $("#id").val();
    if (id != null || id != "") {
        localStorage.removeItem("fd_id");
        localStorage.setItem("fd_id", id);
    }
    var description = $(".description").val();
    if (description != "" || description != null) {
        localStorage.removeItem("fd_description");
        localStorage.setItem("fd_description", description);
    }


}

var nowDate = new Date();
var year = nowDate.getFullYear() + "";
var month = nowDate.getMonth() + 1 + "";
if (month.length == 1) {
    month = "0" + month;
}
var day = nowDate.getDate() + "";
if (day.length == 1) {
    day = "0" + day;
}
var nowTime = year + month + day;
function syncUploadImage() {
    var resvDate = $("#datePicker").val();
    if (resvDate == "选择日期" || !resvDate) {
        showError("请选择就诊时间！");
        return false;
    }
    var hospitalName = $("#hospital_add b").html();
    if (!hospitalName) {
        showError("请选择就诊医院！");
        return false;
    }
    var name = $("#name").val();
    var checkname = /^[\u4E00-\u9FFF]{2,15}((\.){1,6})?$/;
    if (name == "") {
        showError("请输入姓名！");
        return false;
    }

    if (!checkname.test(name)) {
        showError("请输入正确格式的姓名！");
        return false;
    }
    if (check_emoji(name)) {
        showError("无法输入表情！");
        return false;
    }
    var phone = $('#phone').val();
    var checkphone = /^1[3|4|5|7|8][0-9]\d{4,8}$/;
    if (!checkphone.test(phone)) {
        showError("请输入正确格式的手机号！");
        return false;
    }
    if (check_emoji(phone)) {
        showError("无法输入表情！");
        return false;
    }
    var id = $('#ID').val();
    var checkid = /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
    if (!checkid.test(id)) {
        showError("请输入正确格式的身份证号！");
        return false;
    }
    if (check_emoji(id)) {
        showError("无法输入表情！");
        return false;
    }
    var description = $(".description").val();
    if (description.length > 200) {
        showError("症状：最多可输入200个字符！");
        return false;
    }
    if (check_emoji(description)) {
        showError("无法输入表情！");
        return false;
    }
    var localId = WebAPP.photoIDs.pop();
    console.log(WebAPP.photoIDs.length);
    wx.uploadImage({
        localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
        isShowProgressTips: 1, // 默认为1，显示进度提示
        success: function (res) {
            console.log(res);
            var serverId = res.serverId; // 返回图片的服务器端ID
            $.post('/Order/downloadPics', { serverId: serverId, openid: $('#hidStorageOpenID').val() }, function (data) {
                WebAPP.photoServerIDs.push("@imgurl" + "/UploadFile/" + nowTime + "/" + serverId + ".png");
                if (WebAPP.photoIDs.length > 0) {
                    syncUploadImage();
                } else {
                    submitDate();
                }
            });



        }
    });
}
function submitDate() {
    var pidStr = $("#pid").val();
    var resvDate = $("#datePicker").val();
    if (resvDate == "选择日期" || !resvDate) {
        showError("请选择就诊时间！");
        return false;
    }
    var hospitalName = $("#hospital_add b").html();
    if (!hospitalName) {
        showError("请选择就诊医院！");
        return false;
    }
    var name = $("#name").val();
    var checkname = /^[\u4E00-\u9FFF]{2,15}((\.){1,6})?$/;
    if (name == "") {
        showError("请输入姓名！");
        return false;
    }

    if (!checkname.test(name)) {
        showError("请输入正确格式的姓名！");
        return false;
    }
    if (check_emoji(name)) {
        showError("无法输入表情！");
        return false;
    }
    var phone = $('#phone').val();
    var checkphone = /^1[3|4|5|7|8][0-9]\d{4,8}$/;
    if (!checkphone.test(phone)) {
        showError("请输入正确格式的手机号！");
        return false;
    }
    if (check_emoji(phone)) {
        showError("无法输入表情！");
        return false;
    }
    var id = $('#ID').val();
    var checkid = /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
    if (!checkid.test(id)) {
        showError("请输入正确格式的身份证号！");
        return false;
    }
    if (check_emoji(id)) {
        showError("无法输入表情！");
        return false;
    }
    var description = $(".description").val();
    if (description.length > 200) {
        showError("症状：最多可输入200个字符！");
        return false;
    }
    if (check_emoji(description)) {
        showError("无法输入表情！");
        return false;
    }
    var picList = "";
    for (i = 0; i < WebAPP.photoIDs.length; i++) {
        picList = picList + WebAPP.photoIDs[i]
        if (i < WebAPP.photoIDs.length - 1) {
            picList = picList + ",";
        }
    }
    console.log("resvDate:"+ resvDate+", hospitalName:"+ hospitalName+",  userName:"+ name+", phone: "+phone+", id:"+ id+", description:"+ description+", pictures:"+ picList+", pid:"+ pidStr+",openId:"+openid);
    $.ajax({
        url: '/FreeDiagnosis/ResvServerAdd',
        type: 'POST', //GET
        async: false,    //或false,是否异步
        data: { resvDate: resvDate, hospitalName: hospitalName,  userName: name, phone: phone, id: id, description: description, pictures: picList, pid: pidStr,openId:openid },
        timeout: 15000,    //超时时间
        dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
        beforeSend: function (xhr) {

        },
        success: function (data) {
            if (data.success) {
                $(".alertBox_bg").show();
                clearLocation();

            } else {
                showError("提交意向单失败！");
            }
        },
        error: function (xhr, textStatus) {
            showError("网络错误，请重新提交！");
        },
        complete: function () {

        }
    })


}
window.clearLocation = function () {
    localStorage.removeItem("fd_selectDateText");
    localStorage.removeItem("fd_section");
    localStorage.removeItem("fd_hospitalName");
    localStorage.removeItem("fd_hospitalAddress");
    localStorage.removeItem("fd_name");
    localStorage.removeItem("fd_phone");
    localStorage.removeItem("fd_id");
    localStorage.removeItem("fd_description");
    localStorage.removeItem("fd_picList");
}
window.showPicList = function (aimgsrc) {
    WebAPP.photoIDs.push(aimgsrc);
    localStorage.removeItem("fd_picList");
    localStorage.setItem("fd_picList", WebAPP.photoIDs);
    var html = '<span class="share_file custom_file_1"><img style="width:60px;height:60px;" src="' + aimgsrc + '" alt="" /><i class="share_x"></i></span>';
    $('.share_area').find('.no_file').before($(html));
    //存放图片的父级元素
    var imgContainer = document.getElementsByClassName("addImg")[0];
    var imgUrl = aimgsrc;
    var img = document.createElement("img");
    img.setAttribute("src", imgUrl);
    var imgAdd = document.createElement("div");
    var closeBtn = document.createElement("span");
    closeBtn.innerText = "×";
    imgAdd.setAttribute("class", "z_addImg");
    closeBtn.setAttribute("class", "closeBtn");
    imgAdd.appendChild(img);
    imgAdd.appendChild(closeBtn);
    imgContainer.appendChild(imgAdd);

    var $shareX = $('#addImg').find('.closeBtn');
    if ($shareX.length == 3) {
        $('.no_file').addClass('shadow');
    }
    $shareX.off('click').on('click', function () {
        for (i = 0; i < WebAPP.photoIDs.length; i++) {
            if (WebAPP.photoIDs[i] == $(this).parent().find("img").attr("src")) {
                WebAPP.photoIDs = WebAPP.photoIDs.slice(0, i).concat(WebAPP.photoIDs.slice(i + 1, WebAPP.photoIDs.length));
            }
        }
        localStorage.removeItem("fd_picList");
        localStorage.setItem("fd_picList", WebAPP.photoIDs);
        $(this).parent(".z_addImg").remove();
        $('.no_file').removeClass('shadow');
    });
}
initForm();
if (userId) {
    saveForm();
}
$('.foot_submit').on('click', function () {
    //$('#picList').val("");
    setTimeout(function () {

        if (WebAPP.photoIDs.length > 0) {
            syncUploadImage();
        }
        else {
            submitDate();
        }
    }, 500);
});
//错误提示框关闭
$(".errorBox_sureBtn").click(function () {
    $(".error_alert").hide();
})
//成功提示框关闭
$(".success_sureBtn").click(function () {
    $(".alertBox_bg").hide();
    window.location.href = "/FreeDiagnosis/Index";
})

 window.showError=function(content) {
    $(".errorBox_tip").html(content);
    $(".error_alert").show();
}