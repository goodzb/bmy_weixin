﻿
var Rayelink = (window['Rayelink'] || {});
/**
    * 判断一个字符串是否是邮箱格式
    * @memberOf string
    * @param {String} emailStr
    * @return {Boolean}
    */
Rayelink.isEmail = function (emailStr) {
    if (emailStr.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) !== -1) {
        return true;
    }
    else {
        return false;
    }
};
/**
 * 判断一个字符串是否是以;间隔的多个邮箱
 */
Rayelink.isEmailS = function (emailStr) {
    if (Rayelink.isEmpty(emailStr))
        return false;
    var emails = emailStr.split(";");
    for (var i = 0; i < emails.length; i++) {
        if (!Rayelink.isEmail(emails[i]))
            return false;
    }
    return true;
}

/**
    * 生成随机数的方法
    * 
    * @method random
    * @memberOf Jx.prototype
    * 
    * @param {Number} min 生成随机数的最小值
    * @param {Number} max 生成随机数的最大值
    * @return {Number} 返回生成的随机数
    */
Rayelink.random = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
};

String.prototype.startWith = function (s) {
    return this.slice(0, s.length) == s;
};

String.prototype.endWith = function (s) {
    return this.slice(-s.length) == s;
};

String.prototype.Trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
};

String.prototype.LTrim = function () {
    return this.replace(/(^\s*)/g, "");
};

String.prototype.RTrim = function () {
    return this.replace(/(\s*$)/g, "");
};
/**
* 清除字符串开头和结尾的空格，并把字符串之间的多个空格转换为一个空格
* 
* @memberOf string
* 
* @return {String} 返回清除后的字符串
*/
String.prototype.clean = function () {
    return trim(this.replace(/\s+/g, ' '));
};
/*** 统计指定字符出现的次数 ***/
String.prototype.Occurs = function (ch) {
    //var re = eval("/[^"+ch+"]/g");
    //return this.replace(re, "").length;
    return this.split(ch).length - 1;
}

/*** 检查是否由数字组成 ***/
String.prototype.isDigit = function () {
    var s = this.Trim();
    return (s.replace(/\d/g, "").length == 0);
}

/*** 检查是否由数字字母和下划线组成 ***/
String.prototype.isAlpha = function () {
    return (this.replace(/\w/g, "").length == 0);
}
/*** 检查是否为数 ***/
String.prototype.isNumber = function () {
    var s = this.Trim();
    return (s.search(/^[+-]?[0-9.]*$/) >= 0);
}
/*** 返回字节数 ***/
String.prototype.byteLength = function () {
    return this.replace(/[^\x00-\xff]/g, "**").length;
}
/*** 检查是否包含汉字 ***/
String.prototype.isInChinese = function () {
    return (this.length != this.replace(/[^\x00-\xff]/g, "**").length);
}

/*****************校验是不是存的汉字*************************/
String.prototype.isChineseText = function () {
    return this.length * 2 === this.byteLength();
}

//String.prototype.isInChinese = function () {
//    var patten = /^[\u4E00-\u9FA5]+$/; //简体字正则表达式
//    var tureName = patten.test(turename);
//    if (tureName) {
//        return true;
//    } else {
//        return false;
//    }
//}


Array.prototype.Contains = function (needle) {
    ///<summary>数组判断是否包含某元素</summary>
    ///<param name="needle" type="String">需要包含的元素</param>
    ///<returns>true:包含</returns>   
    for (i in this) {
        if (this[i] == needle) return true;
    }
    return false;
}

/**
    * 将任意变量转换为字符串的方法
    * 
    * @method toString
    * @memberOf string
    * 
    * @param {Mixed} o 任意变量
    * @return {String} 返回转换后的字符串
    */
Rayelink.toString = function (o) {
    return (o + "");
};

Rayelink.isEmpty = function (s) {
    return (typeof (s) == "undefined") || s == undefined || s == null || Rayelink.toString(s).Trim() === "";
}

Rayelink.isNumber = function (s) {
    return /\d+/.test(s);
}


/**
**过滤输入的内容只有中文，英文和常用符号
**/

String.prototype.ReplaceEmoji = function () {
    var reg = /[^\u4e00-\u9fa5|\u0000-\u00ff|\u3002|\uFF1F|\uFF01|\uff0c|\u3001|\uff1b|\uff1a|\u3008-\u300f|\u2018|\u2019|\u201c|\u201d|\uff08|\uff09|\u2014|\u2026|\u2013|\uff0e]/g;
    return this.replace(reg, '');
}

//验证是否是手机号
Rayelink.isPhone = function (phoneStr) {
    if (phoneStr.search(/^1[34587]\d{9}$/) != -1) {
        return true;
    }
    else {
        return false;
    }
};

//验证是否是邮编
Rayelink.isZipCode = function (phoneStr) {
    if (phoneStr.search(/^\d{6}$/) != -1) {
        return true;
    }
    else {
        return false;
    }
};

/**
    * 处理页面上的所有脚本链接的加载
    * @src {String} s
    * @callback {Function} 
    */
Rayelink.LoadScript = function (src, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = src;
    script.onload = script.onreadystatechange = function () {
        if (callback) {
            callback();
        }
    }
    document.head.appendChild(script);
};


/**
* 让日期和时间按照指定的格式显示的方法
* @method date
* @memberOf format
* @param {String} format 格式字符串
* @return {String} 返回生成的日期时间字符串
* 
* @example
*     var d = new Date();
*     // 以 YYYY-MM-dd hh:mm:ss 格式输出 d 的时间字符串
*     d.format(d, "YYYY-MM-DD hh:mm:ss");
* 
*/
Date.prototype.formatDate = function (pattern) {
    /*
    * eg:pattern="YYYY-MM-DD hh:mm:ss";
    */
    var o = {
        "W+": this.getDay() == 0 ? 7 : this.getDay(),    //week
        "M+": this.getMonth() + 1,    //month
        "D+": this.getDate(),    //day
        "d+": this.getDate(),    //day
        "h+": this.getHours(),    //hour
        "m+": this.getMinutes(),    //minute
        "s+": this.getSeconds(),    //second
        "q+": Math.floor((this.getMonth() + 3) / 3),    //quarter
        "S": this.getMilliseconds(),    //millisecond
        "f+": this.getMilliseconds()
    }

    if (/(Y+)/.test(pattern) || /(y+)/.test(pattern)) {
        pattern = pattern.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(pattern)) {
            pattern = pattern.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return pattern;
};

/* 时间差
* @beginDate 开始日期,日期格式:yyyy-mm-dd
* @endDate 结束日期,日期格式:yyyy-mm-dd
*/
Date.prototype.Sub = function (beginDate) {
    var timeSpan = {};
    var subDate = new Date(Math.abs(this - beginDate));//相差的毫秒数
    timeSpan.Years = parseInt(subDate.getFullYear() - 1970);
    timeSpan.Months = parseInt(subDate.getMonth() + 1);
    timeSpan.Days = parseInt(subDate.getDate());
    return timeSpan;
}
Rayelink.LoadSDKScript = function (src, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = src;
    script.onload = script.onreadystatechange = function () {
        if (callback) {
            callback();
        }
    }
    document.head.appendChild(script);
};


//滚动条在Y轴上的滚动距离
Rayelink.GetScrollTop = function () {
    var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0;
    if (document.body) {
        bodyScrollTop = document.body.scrollTop;
    }
    if (document.documentElement) {
        documentScrollTop = document.documentElement.scrollTop;
    }
    scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
    return scrollTop;
}

//文档的总高度
Rayelink.GetScrollHeight = function () {
    var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0;
    if (document.body) {
        bodyScrollHeight = document.body.scrollHeight;
    }
    if (document.documentElement) {
        documentScrollHeight = document.documentElement.scrollHeight;
    }
    scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
    return scrollHeight;
}

//浏览器视口的高度
Rayelink.GetWindowHeight = function () {
    var windowHeight = 0;
    if (document.compatMode == "CSS1Compat") {
        windowHeight = document.documentElement.clientHeight;
    } else {
        windowHeight = document.body.clientHeight;
    }
    return windowHeight;
}

//滚动条是否到底部
Rayelink.IsScrolBottom = function () {
    if (Rayelink.GetScrollTop() + Rayelink.GetWindowHeight() == Rayelink.GetScrollHeight()) {
        //alert("已经到最底部了！!");
        return true;
    }
    return false;
}