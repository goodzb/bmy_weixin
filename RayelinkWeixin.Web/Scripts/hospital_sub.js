$(function () {

    $('.hospital_summary .zhankai').click(function () {
        //if ($(this).closest(".hospital_summary").find("p.summary").height() < 140) {
        //    $(this).closest(".hospital_summary").find("p.summary").css('min-height', '140px');
        //}
        $(this).closest(".hospital_summary")
            .find("p.summary").toggleClass("toggles").parent().find(".zhankai i").toggleClass("arrow_top arrow_top_style");
        return false;
    });

    $('.top_menu').each(function () {
        var topMenu = $(this),
            menuLink = topMenu.find('a');
        menuLink.click(function (e) {
            $(this).addClass('current_light')
                .parent('li').addClass('current_light')
                .siblings('li').removeClass('current_light').children('a').removeClass('current_light')
        })
    });

    //$(".order_btn").bind("touchstart touchend",function(e){
    //    e.preventDefault();
    //    $(this).toggleClass('order_active');
    //})

    window.addEventListener('scroll', scrollBottom, false);
    function scrollBottom() {
        var pageHeight = $("body").height(),
            scrollHeight = $("body").scrollTop(),
            phoneHeight = innerHeight,
            userComments = $(".user_comments").height(),
            users = pageHeight - userComments;
        if (phoneHeight + scrollHeight >= users) {
            $(".sub").removeClass("current_light")
                .siblings().addClass("current_light");
        } else {
            $(".sub").addClass("current_light")
                 .siblings().removeClass("current_light");
        }
    }
});