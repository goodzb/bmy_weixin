$(function () {
    $('.submit,.choose_btn,.online_btn,.send_btn,.submit .custom_style_edit,.storage_clear').bind('touchstart touchend', function () {
        $(this).toggleClass('active_style')
    })
    // 
    $('.delete_icon').bind("click touchend", function (e) {
        $(this).prev("input").val("");
        $(this).hide();
    }).bind("touchstart", function (e) {
        e.preventDefault();
        $(this).addClass("delete_hover");
    }).bind("touchend", function (e) {
        e.preventDefault();
        $(this).removeClass("delete_hover");
    })
    //
    base = {
        "touchAndend": function (elm, classNames) {
            elm.bind('touchstart touchend', function () {
                $(this).toggleClass(classNames)
            })
        },
        "hrefToggle": function (elm, elm2, classNames, classNames2) {
            elm.bind("click", function (e) {
                e.preventDefault();
                $(this).toggleClass(classNames)
                    .siblings().removeClass(classNames);
                elm2.eq($(this).index()).removeClass(classNames2).siblings().addClass(classNames2);
            })
        },
        "caozuotishi": function (action, title) { //action =1 表示成功，ation = 2表示失败 title传入需要提示的文字 页面必须加载base.css
            var caozuo_box = $('<div class="caozuo_box"></div>'),
                success_box = $('<figure class="success_box"><img src="../images/ok.png"><p class="caozuo_title">' + title + '</p></figure>'),
                no_success_box = $('<figure class="no_success_box"><img src="../images/no.png"><p class="caozuo_title">' + title + '</p></figure>');
            function addBox(objBox) {
                caozuo_box.append(objBox);
                $("body").append(caozuo_box);
            }
            if (action == 1) {//成功的提示
                if (title.length <= 4) { //提示文字长度小于等于4则2秒后消失
                    addBox(success_box)
                    setTimeout(function () { caozuo_box.empty(); }, 2000);
                }
                else if (title.length > 4) {//提示文字长度大于4则3秒后消失
                    addBox(success_box)
                    setTimeout(function () { caozuo_box.empty(); }, 3000);
                }
            }
            if (action == 2) {//失败的提示
                if (title.length <= 4) {//提示文字长度小于等于4则2秒后消失
                    addBox(no_success_box)
                    setTimeout(function () { caozuo_box.empty(); }, 2000);
                }
                else if (title.length > 4) {//提示文字长度大于4则3秒后消失
                    addBox(no_success_box)
                    setTimeout(function () { caozuo_box.empty(); }, 3000);
                }
            }
        }
    }

    //弹窗函数 title弹窗名字，sub弹窗具体文本，button按钮文字
    base.alert_mes = function (title, sub, button) {
        var title = title || "默认标题",
            sub = sub || "这是默认的文本内容",
            button = button || "弹窗按钮";
        alert_mes = $('<div class="alert_mes"><div class="title_box"><p class="title">' + title + '<i class="byebye"></i></p><p class="mes_summary">' + sub + '</p><a href="javascript:;"class="bangding btn1" style="padding:10px 0;display:block;margin:0 auto;">' + button + '</a></div></div>');
        $(".alert_mes").remove();
        $("body").addClass("b_no_scroll").append(alert_mes);
        base.touchAndend($(".btn1"), "active");
        $(".byebye").click(function (e) {
            $("body").removeClass("b_no_scroll"); $(this).parents(".alert_mes").remove();
        })
    }
    //弹窗函数 title弹窗名字，sub弹窗具体文本，button1按钮文字，button2按钮文字
    base.alert_mes2 = function (title, sub, button, button2) {
        var title = title || "默认标题",
            sub = sub || "这是默认的文本内容",
            button = button || "弹窗按钮",
            button2 = button2 || "按钮2";
        alert_mes = $('<div class="alert_mes"><div class="title_box"><p class="title">' + title + '<i class="byebye"></i></p><p class="mes_summary">' + sub + '</p><a href="javascript:;"class="bangding btn1">' + button + '</a><a href="javascript:;"class="bangding btn2">' + button2 + '</a></div></div>');
        $(".alert_mes").remove();
        $("html,body").addClass("b_no_scroll");
        $("body").append(alert_mes);
        base.touchAndend($(".btn1"), "active");
        base.touchAndend($(".btn2"), "active");
        $(".byebye").click(function (e) {
            $("html,body").removeClass("b_no_scroll"); $(this).parents(".title_box").toggleClass("hide").parents(".alert_mes").toggleClass("hide")
        })
    }


    //loading 界面
    loading_box = function (time) {
        var loading = $('<div class="loading"><figure class="load_img"><img src="../images/loading.gif" alt="" style="width:32px;height:32px;"></figure></div>');
        $("body").append(loading)
        if (time) {
            $(".loading").remove()
        }
    }

    //    $(".order_content_2").prepend($('<i class="closeBtn"></i>'));
    //    $(".order_content_2 p").before('<p class="title">预约规则</p>');
    //    $(".order_role").click(function(){
    //        $(".order_wrap_2").removeClass("hide");
    //    })
    //    $(".closeBtn").click(function(){
    //        $(".order_wrap_2").addClass("hide");
    //    })


    //判断页面是否滑动到底部
    is_scrolled_bottom = function () {
        var scrollTop = $(this).scrollTop();               //滚动条距离顶部的高度
        var scrollHeight = $(document).height();           //当前页面的总高度
        //var windowHeight = $(this).height();               //当前可视的页面高度
        var windowHeight = document.all ? document.getElementsByTagName("html")[0].offsetHeight: window.innerHeight;
        if (scrollTop + windowHeight >= scrollHeight) { //距离顶部+当前高度 >=文档总高度 即代表滑动到底部
            return true;
        }
        else {
            return false;
        }
    }

    $(function () {
        // 设置jQuery Ajax全局的参数  
        $.ajaxSetup({
            type: "POST",
            timeout: 15000,
            error: function (jqXHR, textStatus, errorThrown) {
                switch (jqXHR.status) {
                    case (500):
                        base.caozuotishi(2, "服务器系统内部错误，请检查网络后重试");
                        if ($("figure.no_success_box img") != null && $("figure.no_success_box img").length > 0) {
                            $("figure.no_success_box img").hide();
                        }
                        break;
                    case (401):
                        base.caozuotishi(2, "未登录");
                        if ($("figure.no_success_box img") != null && $("figure.no_success_box img").length > 0) {
                            $("figure.no_success_box img").hide();
                        }
                        break;
                    case (403):
                        base.caozuotishi(2, "无权限执行此操作");
                        if ($("figure.no_success_box img") != null && $("figure.no_success_box img").length > 0) {
                            $("figure.no_success_box img").hide();
                        }
                        break;
                    case (408):
                        base.caozuotishi(2, "请求超时，请检查网络后重试");
                        if ($("figure.no_success_box img") != null && $("figure.no_success_box img").length > 0) {
                            $("figure.no_success_box img").hide();
                        }
                        break;
                    default:
                        base.caozuotishi(2, "数据加载失败，请检查网络后重试");
                        if ($("figure.no_success_box img") != null && $("figure.no_success_box img").length > 0) {
                            $("figure.no_success_box img").hide();
                        }
                }
            },
            success: function (data) {
                //alert("临时测试，请测试人员先忽略：操作成功");
            }
        });
    });

    //判断页面是否滑动到底部
    base.is_emoji = function (message) {
        var checkMessage = /\\\ud[0-9a-f]{3}/;
        var GB2312UnicodeConverter = {
            ToUnicode: function (str) {
                return escape(str).toLocaleLowerCase().replace(/%u/gi, '\\u');
            }
            , ToGB2312: function (str) {
                return unescape(str.replace(/\\u/gi, '%u'));
            }
        };
        var unicode = GB2312UnicodeConverter.ToUnicode(message);
        if (checkMessage.test(unicode)) { 
            return true;
        }
        else {
            return false;
        }
    }

    })

