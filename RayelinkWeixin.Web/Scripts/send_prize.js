$(function () {
    $('.comments').each(function () {
        var This = $(this),
            flag = This.find('.flags'),
            prize_num = This.find('.flags span');

        flag.click(function (e) {
            e.preventDefault();
            flag.removeClass('active').find('span').addClass('shadow');
            $(this).addClass('active').find('span').toggleClass('shadow');
            $('#hidLevel').val($(this).data('level'));
        })
    })
});