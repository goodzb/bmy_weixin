﻿/* 弹窗
 * p_obj: {url: 'logo', text:'提示文字'}
 * */
window.HtmlAlertShowStatus = false;
function DialogTip(p_obj) {   
    if (window.HtmlAlertShowStatus) return;
    var _html = null;
    p_obj.url = "../../images/" + p_obj.url + ".png"; //图标名称或者相对路径
    p_obj.text = p_obj.text || "操作成功";
    var _doc = document.getElementsByClassName('html-alert');
    if (_doc.length <= 0) {
        _html = document.createElement('div');
        _html.className = 'html-alert';
        _html.innerHTML = '<figure class="tip_box">' +
            '<img src="' + p_obj.url + '">' +
            '<p class="title">' + p_obj.text + '</p>' +
            '</figure>';
        document.body.appendChild(_html);
    } else {
        _html = document.createElement('figure');
        _html.className = 'tip_box';
        _html.innerHTML = '<img src="' + p_obj.url + '">' +
            '<p class="title">' + p_obj.text + '</p>';
        _doc[0].appendChild(_html);
    }

    var _newDoc = document.getElementsByClassName('html-alert');
    if (p_obj.text.length <= 20) { //提示文字长度小于等于20则1秒后消失
        setTimeout(function () {
            _newDoc[0].parentNode.removeChild(_newDoc[0]);
            window.HtmlAlertShowStatus = false;
            if (p_obj.CallBack) { p_obj.CallBack(); }
        }, 1000);
    } else if (p_obj.text.length > 20) { //提示文字长度大于20则2秒后消失
        setTimeout(function () {
            _newDoc[0].parentNode.removeChild(_newDoc[0]);
            window.HtmlAlertShowStatus = false;
            if (p_obj.CallBack) { p_obj.CallBack(); }
        }, 2000);
    }
    window.HtmlAlertShowStatus = true;
}
var DialogOkTip = function (text) {
    DialogTip({ url: "ok", text: text });
};
var DialogNoTip = function (text) {
    DialogTip({ url: "no", text: text });
};

//带按钮弹窗
var DialogConfirm = Vue.extend({
    props: ['param'],
    methods: {
        close: function () {
            // this.$emit('cancel');           
            if (this.param.RightFunc) {
                this.param.RightFunc();
            }
            this.param.Show = false;
        },
        apply: function () {
            // this.$emit('apply');             
            if (this.param.LeftFunc) {
                this.param.LeftFunc();
            }
            this.param.Show = false;
        }
    },
    template: '<div class="dialog-confirm" v-if="param.Show">' +
    '<div class="black-cloth"></div>' +
    '<div class="dialog-confirm-inner">' +
    '<div class="dialog-confirm-content">' +
    '<p class="dialog-confirm-title">温馨提示' +
    '<i class="dialog-close" @click="close"></i></p>' +
    '<p class="dialog-confirm-msg">{{param.Content}}</p>' +
    '</div>' +
    '<div class="dialog-confirm-btn">' +
    '<span class="dialog-confirm-btn-left dialog-confirm-btn-btn" @click="apply">{{param.LeftText}}</span>' +
    '<span class="dialog-confirm-btn-right dialog-confirm-btn-btn" @click="close">{{param.RightText}}</span>' +
    '</div>' +
    '</div>' +
    '</div>'
});