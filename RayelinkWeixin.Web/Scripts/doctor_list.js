$(function(){

    $('.navigation').each(function(){
        var This = $(this),
            keshi = This.find('.choose_1'),
            zhenshi = This.find('.choose_2'),
            list_1 = $('.doctor_list_1'),
            list_1_height = list_1.height(),
            list_2 = $('.doctor_list_2'),
            shadow = $('.doctor_list_wrap'),
            navHeight = $(".navigation").height(),
            phoneHeight = innerHeight,
            doctor_list_height = phoneHeight - navHeight;
           
        
        shadow.height(phoneHeight);//.css({"top":navHeight+"px"})
        
       
        keshi.click(choose_list)
        zhenshi.click(choose_list)
        list_1.find("li").on("click", choose_liKeshi);
        list_2.find("li").on("click", choose_liZhichen);
        //菜单收起放下函数
         function choose_list(e){
            e.preventDefault();
            if($(this).hasClass("choose_1")){
                toggleKeshi();
                $(this).find('.arrow_bottom').toggleClass("arrows");
                if(list_1.hasClass('current_list')){shadow.removeClass('hide')}
            }
            if($(this).hasClass("choose_2")){
                toggleZhichen();
                $(this).find('.arrow_bottom').toggleClass("arrows");
                if(list_2.hasClass('current_list')){shadow.removeClass('hide')}
            }
            if(shadow.is(":visible")){
                $('.doctor_summary ul').addClass('scroll');
            }
            if(shadow.is(":hidden")){
                 $('.doctor_summary ul').removeClass('scroll');
                 $('.doctor_list_btn').find('.arrow_bottom').removeClass('arrows')
            }
        }
        $('.doctor_list_1 li,.doctor_list_2 li').click(function(){
            $('.doctor_list_btn').find('.arrow_bottom').removeClass('arrows')
        })
        function toggleKeshi(){ //选择科室
            list_2.addClass('hide').removeClass('current_list');
            $(".choose_2").find('.arrow_bottom').removeClass('arrows')
            list_1.toggleClass('hide current_list');
            shadow.toggleClass('hide');
            list_1.find("li").bind("click",choose_li)
        }
        function toggleZhichen(){ //选择职称
            list_1.addClass('hide').removeClass('current_list');
            $(".choose_1").find('.arrow_bottom').removeClass('arrows')
            list_2.toggleClass('hide current_list');
            shadow.toggleClass('hide');
            list_2.find("li").bind("click",choose_li)
        }
        function choose_li(e){ //选择科室或者职称列表的具体内容
            e.preventDefault()
            list_1.addClass('hide');
            list_2.addClass('hide');
            shadow.addClass('hide');
           $('.doctor_summary ul').removeClass('scroll')
        }

        function choose_liKeshi(e) { //选择科室的具体内容
            e.preventDefault();
            e.stopPropagation();
            list_1.addClass('hide');
            list_2.addClass('hide');
            shadow.addClass('hide');
            $('.doctor_summary ul').removeClass('scroll')
            $('#hidDeptID').val($(this).data('id'));
            var selectName = $.trim($(this).html());
            if ("全部" == selectName) {
                $('.choose_1').html("选择科室<i class='arrow_bottom arrow_style'></i>");
                $('#hidSelectedKeShi').val("选择科室<i class='arrow_bottom arrow_style'></i>");
            } else
            {
                $('.choose_1').html($(this).html() + "<i class='arrow_bottom arrow_style'></i>");
                $('#hidSelectedKeShi').val($(this).html() + "<i class='arrow_bottom arrow_style'></i>");
            }
            
            WebAPP.RefreshDoctorList();
        }
        function choose_liZhichen(e) { //选择科职称列表的具体内容
            e.preventDefault();
            e.stopPropagation();
            list_1.addClass('hide');
            list_2.addClass('hide');
            shadow.addClass('hide');
            $('.doctor_summary ul').removeClass('scroll')
            $('#hidTitleID').val($(this).data('id'));
            var selectName = $.trim($(this).html());
            if ("全部" == selectName)
            {
                $('.choose_2').html("选择职称<i class='arrow_bottom arrow_style'></i>");
                $('#hidSelectedZhiCheng').val("选择职称<i class='arrow_bottom arrow_style'></i>");
            } else
            {
                $('.choose_2').html($(this).html() + "<i class='arrow_bottom arrow_style'></i>");
                $('#hidSelectedZhiCheng').val($(this).html() + "<i class='arrow_bottom arrow_style'></i>");
            }
            
            WebAPP.RefreshDoctorList();
        }
    })


$(".delIcon").hide();
    $(".delIcon").click(function(){
        $(".search_doc_hos").val("");
        $(this).hide();
    })
    base.touchAndend($(".delIcon"),"active")
    $(".search_doc_hos").focusin(function(){
        $(".delIcon").show();
    })
})
