$(function(){
    $('.top_menu').each(function(){
        var topMenu = $(this),
            menuLink = topMenu.find('a');
        menuLink.click(function(e){
            $(this).addClass('current_light')
                .parent('li').addClass('current_light')
                .siblings('li').removeClass('current_light').children('a').removeClass('current_light')
        })
    });
    $('.choose_wrap').each(function(){
        var This = $(this),
            choose_loc = This.find('.choose_location'),
            choose_loc_list = This.find('.choose_location_list'),
            date_location = choose_loc_list.find("li");
        
        choose_loc.click(function () {
            choose_loc_list.toggleClass('hide');
            $('.arrow_bottom').toggleClass('arrows');
        })
        
       
        date_location.click(function(e){
            e.preventDefault();
            var location = $(this).html();
            choose_loc.find("span").html(location);
            choose_loc_list.addClass('hide');
            $('p.choose_location').find('span').data('id', $(this).data('location'));
            //window.sessionStorage.setItem('LocationID', $(this).data('location'));
            //window.sessionStorage.setItem('LocationName', location);
            WebAPP.InitHospitalData();
        })
        
    
    })
})