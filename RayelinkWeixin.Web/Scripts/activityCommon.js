﻿if (typeof console === "undefined" || typeof console.log === "undefined") {
    console = {};
    console.log = function () { };
}

/*
 * 测试所用pid，正式环境应该从服务器中直接获取
 */
var pid = GetQueryString("pid") || 5775311;
var nativeApis =false;//ios创建的对象 用于判断
//var isIOS = /iphone|ipad/i.test(navigator.userAgent);
var isIOS = /\((iPhone|iPad|iPod)/i.test(navigator.userAgent);
if (isIOS) {
    window.prompt = function () { };
   nativeApis = localStorage.getItem("nativeApis");
}
//var pid = 1231231;

/*
* comm.js
* 封装公用的方法,依赖jquery
*/
var app = {};
var pc = {};
/*
* msgAlert
* 模拟弹窗函数
* 参数说明：
* 	tit(弹窗标题): 类型：字符串。
* 	cont(弹窗内容): 类型：字符串。
* 	close(右上角关闭按钮): 类型：布尔。
*   btnText(按钮文字): 类型:字符串。
*   callback(执行之后回调函数): 类型：方法。
* 示例：app.msgAlert("我是标题","我是内容",true);
*/
app.msgAlert = function (tit, cont, close, btnText, callback) {
    var $maskdiv = $('<div class="maskdiv"></div>');
    var $alertpop = $(
            '<div class="popbox">' +
            '<a href="javascript:;" class="close"></a>' +
            '<div class="pop-tit">' + tit + '</div>' +
            '<div class="pop-cont">' + cont + '</div>' +
            '<div class="pop-btn">' +
                '<a href="javascript:;" class="submitbtn">确认</a>' +
            '</div>' +
        '</div>'
        );

    $maskdiv.appendTo("body");
    $alertpop.appendTo("body");

    if (btnText && typeof btnText === "string") {
        $(".popbox .submitbtn").text(btnText);
    }
    if (close == false) {
        $(".popbox .close").hide();
    } else {
        $(".popbox .close").show();
    }

    $(".popbox .close,.popbox .submitbtn").click(function () {
        if (typeof callback === "function") {
            callback();
        }
        $(".maskdiv,.popbox").hide();
    });
};

/*
* msgConfirm
* 模拟弹窗函数
* 参数说明：
* 	tit(弹窗标题): 类型：字符串。
* 	cont(弹窗内容): 类型：字符串。
* 	close(右上角关闭按钮): 类型：布尔。
* 	confirmFunc(点击确定的回调函数): 类型：方法。
* 示例：app.msgConfirm("我是标题","我是内容",true);
*/
app.msgConfirm = function (tit, cont, close, confirmFunc) {
    var $maskdiv = $('<div class="maskdiv"></div>');
    var $alertpop = $(
            '<div class="popbox">' +
            '<a href="javascript:;" class="close none"></a>' +
            '<div class="pop-tit">' + tit + '</div>' +
            '<div class="pop-cont">' + cont + '</div>' +
            '<div class="pop-btn">' +
                '<a href="javascript:;" class="cancelbtn">取消</a>' +
                '<a href="javascript:;" class="okbtn" style="width:241px">确认</a>' +
            '</div>' +
        '</div>'
        );

    $maskdiv.appendTo("body");
    $alertpop.appendTo("body");
    if (close == false) {
        $(".popbox .close").hide();
    } else {
        $(".popbox .close").show();
    }

    $(".popbox .close,.cancelbtn").click(function () {
        $(".maskdiv,.popbox").hide();
    });
    $(".okbtn").click(function () {
        if (typeof confirmFunc === "function") {
            confirmFunc();
        }
        $(".maskdiv,.popbox").hide();
    });
};

window.registerSelect = function (selectClick, itemClick) {
    //模拟下拉框
    $(".selectbox .select").unbind().click(function () {
        var list = $(this).siblings(".selectlist");
        if (list.height() > 0) {
            list.height(0);
            $(this).css("background-image", "url(../images/order/arrdown.png)");
        }
        else {
            list.height(list.children().length * list.children().outerHeight())
            $(this).css("background-image", "url(../images/order/arrup.png)");
        }

        if (typeof selectClick == "function") {
            selectClick();
        }
    });
    $(".selectlist span").unbind().click(function () {
        var val = $(this).html();
        $(this).parent().height(0).siblings().val(val);
        $(this).parents(".selectbox").find(".select").css("background-image", "url(../images/order/arrdown.png)");
        if (typeof itemClick == "function") {
            itemClick();
        }
    });
};

$(function () {
    $(".topbar .back, .header .back, .topbox .back").click(function (event) {
        event.stopPropagation();
        event.preventDefault();
        CommonRedirect.goBack();
        return false;
    });
    if (typeof checkBtnStatus == "function") {
        window.registerSelect(null, checkBtnStatus);
    } else {
        window.registerSelect();
    }

});

/*
 * webapi 调用封装
 */
(function ($) {
    var config = {
        urlBase: "/api/"
    };

    function setLocalStorage(key, data) {
        if (typeof data == "object" && data) {
            data = JSON.stringify(data);
        }
        localStorage.setItem(key, data);
        localStorage.setItem(key + "_cachetime", new Date() - 1);
    }
    function getLocalStorage(key) {
        var data = localStorage.getItem(key);
        var cacheTime = localStorage.getItem(key + "_cachetime");
        var obj = null;
        try {
            var duration = new Date() - cacheTime;
            if (data != null && duration < 7 * 24 * 3600 * 1000) {
                if (!data.indexOf("{") || !data.indexOf("[")) {
                    obj = JSON.parse(data);
                } else {
                    obj = data;
                }
            }
        } catch (e) {
            obj = data;
        }
        return obj;
    }
    /**
     * 
     * @param {请求的完全路径} url 
     * @param {Http Method} method 
     * @param {回调函数} callback 
     * @param {是否使用localStorage缓存} useCache 
     * @param {是否不使用遮罩} noMask 
     * @returns {undefined}  
     */
    var ajaxRequest = function (url, method, callback, useCache, useMask) {
        var key = url + "|" + method;
        if (useCache) {
            var obj = getLocalStorage(key);
            if (obj != null) {
                if (typeof callback === 'function') setTimeout(callback, 0, obj);
                return;
            }
        }

        $.ajax({
            type: method,
            url: url,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('token', getCookie('token'));
                if (useMask) loadDiv();//加载层
            },
            timeout: 60000,//15秒超时
            success: function (data) {
                if (useMask) completeLoading();//关闭加载层
                if (typeof data.Data != "undefined") data = data.Data;
                console.log(data);
                setLocalStorage(key, data);
                if (typeof callback === 'function') callback(data);
            },
            error: function (e, g, h) {
                if (useMask) completeLoading();//关闭加载层
                if (e.statusText == "Forbidden") {
                    app.msgAlert("操作失败", "<p>您尚未登录，请先去登录</p>", false);
                    $(".popbox .submitbtn").unbind().click(function () {
                        if (/wechat/i.test(location.href)) {
                            location.href = '/Html/ResvWechat/index.html';
                        } else {
                            location.href = '/login';
                        }
                    });
                }
                else {
                    app.msgAlert("操作失败", "<p>抱歉，操作失败，请检查你的网络连接</p>", true);
                    console.log(e, g);
                }
            }, omplete: function () {
                if (useMask) completeLoading();//关闭加载层
            }
        });
    };


    function getUrl(controller, options) {
        var params = '?';
        if (options) {
            for (i in options) {
                params += i + '=' + escape(options[i]) + '&';
            }
        }
        return config.urlBase + controller + params.substr(0, params.length - 1);
    }
    /*
     * 调用Web API
     * controller: web api控制器名称
     * options: 要传入的参数，js object 如{pid: 5337771, cardNo: 'V100930040'}
     * method: HTTP方法： POST/GET/PUT/DELETE，不区分大小写
     * callback: 成功之后的回调方法
     * useCache:是否启用离线存储
     */
    window.InvokeAction = function (controller, options, method, callback, useCache) {
        var url = getUrl(controller, options);
        ajaxRequest(url, method.toUpperCase(), callback, useCache, true);
    };

    /*
     * 调用Web API 但是不显示遮罩
     * controller: web api控制器名称
     * options: 要传入的参数，js object 如{pid: 5337771, cardNo: 'V100930040'}
     * method: HTTP方法： POST/GET/PUT/DELETE，不区分大小写
     * callback: 成功之后的回调方法
     * useCache:是否启用离线存储
     */
    window.InvokeActionWithNoMask = function (controller, options, method, callback, useCache) {
        var url = getUrl(controller, options);
        ajaxRequest(url, method.toUpperCase(), callback, useCache);
    };
})(window.jQuery);

//根据身份证号码获取性别：0男 1女
function getSexFromCertNo(certNo) {
    if (certNo.length == 18) {
        var start = 16;
    }
    else if (certNo.length == 15) {
        var start = 14;
    }
    else {
        return -1;
    }
    try {
        return (Number(certNo.substr(start, 1)) + 1) % 2;
    }
    catch (e) {
        return -1;
    }
}


String.prototype.format = function (args) {
    if (arguments.length > 0) {
        var result = this;
        if (arguments.length == 1 && typeof (args) == "object") {
            for (var key in args) {
                var reg = new RegExp("({" + key + "})", "g");
                result = result.replace(reg, args[key]);
            }
        }
        else {
            for (var i = 0; i < arguments.length; i++) {
                var reg = new RegExp("({[" + i + "]})", "g");

                // 王刘程修改于2015年8月25日15:28:28，当用户传undefined时直接将空字符串连接进入模版
                if (arguments[i] == undefined) {
                    result = result.replace(reg, "");
                }
                else {
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
        return result;
    }
    else {
        return this;
    }
}; /*
 * 获取Url参数
 */
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}
function setCookie(c_name, value) {
    document.cookie = c_name + "=" + escape(value);
}
$(function () {
    $('.menu .a1').click(function () {
        window.location.href = "../Reservation/index.html";
    });
    $('.menu .a2').click(function () {
        window.location.href = "../ReportQuery/ReportValidator.html";
    });
    $('.menu .a3').click(function () {
        window.location.href = "../ReportQuery/ReportQueryList.html?isSearch={0}".format(true);
    });
});

function diableScroll(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
}

function loadDiv() {
    if (!window.maskCount) {
        window.maskCount = 1;
    }
    else {
        window.maskCount++;
    }
    //如果已经有加载动画了就不要再次添加了
    if ($("#loadingDiv").length) {
        return;
    }
    //获取浏览器页面可见高度和宽度
    var _PageHeight = document.documentElement.clientHeight,
        _PageWidth = document.documentElement.clientWidth;

    //在页面未加载完毕之前显示的loading Html自定义内容http://localhost:50384/Html/images/common/loading.gif
    var _LoadingHtml = '<div id="loadingDiv"><div class="mask_loading"><p class="loading-content">正在加载中</p></div></div>'; //增加行内样式style="transform:translate(-50%,-50%);"
    //呈现loading效果
    $(document.body).prepend($(_LoadingHtml));
    var mask_loading = $(".mask_loading");

    //计算loading框距离顶部和左部的距离（loading框的宽度为215px，高度为61px）
    //var mask_top = (_PageHeight - mask_loading.height()) / 2;
    //var mask_left = (_PageWidth - mask_loading.width()) / 2;
    var mask_top = (mask_loading.outerHeight() / 2);
    var mask_left = (mask_loading.outerWidth() / 2);
    mask_loading.css({ "margin-left": -mask_left, "margin-top": -mask_top, "top": "50%", "left": "50%" });// top:mask_top, left:mask_left 
    if (document.body.addEventListener) {
        document.body.addEventListener("touchmove", diableScroll, false);
    }
}
function completeLoading() {
    window.maskCount--;
    if (window.maskCount > 0) {
        return;
    }
    var loadingMask = document.getElementById('loadingDiv');
    loadingMask && loadingMask.parentNode.removeChild(loadingMask);
    if (document.body.removeEventListener) {
        document.body.removeEventListener("touchmove", diableScroll, false);
    }

}

var CommonRedirect = {};
CommonRedirect.goBack = function () {
    history.go(-1);
};
var screen_w = window.screen.width;
if (screen_w < 640) {
    var phoneScale = parseInt(window.screen.width) / 640;
    document.write('<meta name="viewport" content="width=640, minimum-scale = ' + phoneScale + ', maximum-scale = ' + phoneScale + ', target-densitydpi=device-dpi">');
}
/*发验证码*/
function SendCRMVerificationcode(phone) {
    var url = '/APIAccount/SendCRMVerificationcode?mobilephone={0}'.format(phone);

    $.ajax({
        type: "Get",
        url: url,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (data) {
        },
        error: function (e, g, h) {

        }
    });
}
/*验证验证码*/
function Verificationphonecode(phone, code, callback, errorback) {
    var phonecode = { "Mobilephone": phone, "code": code };
    var url = '/APIAccount/Verificationphonecode';

    $.ajax({
        type: "Post",
        url: url,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(phonecode),
        success: function (data) {
            if (typeof callback === "function")
                return callback(data.Data.verificationstatus);;
        },
        error: function (e, g, h) {
            console.log(e.status + "," + e.readyState + "," + g);
            errorback();
        }

    });
}


/*发送邮件*/
function sendMail(toAccount, title, content, callback) {
    toAccount = 'zhangbin@rich-healthcare.com';
    var url = '/APIAccount/SendMail?toAccount={0}&title={1}&content={2}'.format(toAccount, title, content);

    $.ajax({
        type: "Post",
        url: url,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (typeof callback === "function")
                return callback(data);
        },
        error: function (e, g, h) {

        }
    });
}

/*发送邮件*/
function newSendMail(toAccount, title, content, callback) {
    toAccount = 'zhangbin@rich-healthcare.com';
    var url = '/APIAccount/newSendMail?toAccount={0}&title={1}&content={2}'.format(toAccount, title, JSON.stringify(content));

    $.ajax({
        type: "Post",
        url: url,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (typeof callback === "function")
                return callback(data);
        },
        error: function (e, g, h) {

        }
    });
}
/*无数据样式*/
app.showNodata = function () {
    return '<div class="none-info"><p>暂无任何数据</p></div>';
}; /*
 * 对Date的扩展，将 Date 转化为指定格式的String
 * 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
 * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
 * 例子： 
 * (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
 * (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
 * */
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}; /*
  *PC弹框方法
  *弹框标题
  *弹框内容
  *按钮文字
  *回写方法
*/
pc.msgAlert = function (tit, cont, btnText, callback) {
    $(".mesg-window").remove();
    var $alertpop = $('<div class="mesg-window" id="mesgShow">' +
            '<div class="mesg-header">' +
            '<span style="color: #fff">' + tit + '</span>' +
            '<a class="btn-close right">×</a>' + '</div>' +
            '<div class="mesg-content">' +
            '<div class="mesg-cont">' + cont + '</div>' +
            '<a href="javascript:;" class="altokbtn">确认</a>' + '</div>' +
        '</div>' + '</div>'
        );

    $alertpop.appendTo("body");
    Drag("mesgShow");
    if (btnText && typeof btnText === "string") {
        $(".mesg-content .submitbtn").text(btnText);
    }

    $(".altokbtn,.mesg-window .mesg-header .btn-close").click(function () {
        if (typeof callback === "function") {
            callback();
        }
        $(".mesg-window").remove();
    });
};
/*
  *PC弹框询问方法
  *弹框标题
  *弹框内容
  *回写方法
*/
pc.msgConfirm = function (tit, cont, confirmFunc) {
    $(".mesg-window").remove();
    var $alertpop = $('<div class="mesg-window" id="mesgShow">' +
            '<div class="mesg-header">' +
            '<span style="color: #fff">' + tit + '</span>' +
            '<a class="btn-close right">×</a>' + '</div>' +
            '<div class="mesg-content">' +
            '<div class="mesg-cont">' + cont + '</div>' +
             '<a href="javascript:;" class="cancelbtn">取消</a>' +
            '<a href="javascript:;" class="okbtn">确认</a>' + '</div>' +
        '</div>' + '</div>'
        );

    $alertpop.appendTo("body");
    Drag("mesgShow");
    $(".cancelbtn,.mesg-window .mesg-header .btn-close").click(function () {
        $(".mesg-window").remove();
    });
    $(".okbtn").click(function () {
        if (typeof confirmFunc === "function") {
            confirmFunc();
        }
        $(".mesg-window").remove();
    });
};
var token = GetQueryString("token");
if (token) {
    setCookie("token", token);
}


/*发送约名医提示短信*/
function sendFamousMessage(mobilephone, content) {
    var url = '/APIAccount/SendResvFamousMessage?mobilephone={0}&content={1}'.format(mobilephone, JSON.stringify(content));
    $.ajax({
        type: "Post",
        url: url,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (data) {
        },
        error: function (e, g, h) {
        }
    });
}
function GetResultSendResvMessage(mobilephone, content, callback, beforefunction) {
    var url = '/APIAccount/GetResultSendResvMessage?mobilephone={0}&content={1}'.format(mobilephone, JSON.stringify(content));
    $.ajax({
        type: "Post",
        url: url,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            if (typeof beforefunction === "function") {
                beforefunction();
            }
        },
        success: function (data) {
            if (typeof callback === "function") {
                callback(data);
            }
        },
        error: function (e, g, h) {
        }
    });

}

/*数据统计*/
function onEvent(tag, label, duration) {
    //isIOS = /\((iPhone|iPad|iPod)/i.test(navigator.userAgent);
    //if (isIOS && window.iOSUMTongJiEventId) {
    //    iOSUMTongJiEventId(tag, JSON.stringify({ tag: tag, label: label, duration: duration }));
    //}
    if (isIOS) {
       
        if (window.iOSUMTongJiEventId) {
            iOSUMTongJiEventId(tag, JSON.stringify({ tag: tag, label: label, duration: duration }));//只在老版本使用
           // iOSUMTongJiEventId("eventId=" + tag + "&label=" + label + "&duration=" + duration);////用于最新版本 不适用老版本
        } else {
            if (nativeApis) {
                location.href = "iosumtongji://event?eventId=" + tag + "&label=" + label + "&duration=" + duration;//只在新版本使用
            }
            
        }
        
    }
    prompt("event", JSON.stringify({ tag: tag, label: label, duration: duration }));
}
function onKVEvent(tag, map, duration) {
    //alert('enter kvevent: ' + tag)
    //isIOS = /\((iPhone|iPad|iPod)/i.test(navigator.userAgent);

    if (isIOS) {
        

        if (window.iOSUMTongJiEventId) {
            iOSUMTongJiEventId(tag, JSON.stringify(map));//只在老版本使用
            // iOSUMTongJiEventId("eventId=" + tag + getUrlDataForIos(map));////用于最新版本 不适用老版本
        } else {
            if (nativeApis) {
                location.href = "iosumtongji://event?eventId=" + tag + getUrlDataForIos(map);//只在新版本使用
            }
        }

        
    }
    map.id = tag;
    map.duration = duration;
    prompt("ekv", JSON.stringify(map));
}
function getUrlDataForIos(options) {
    var params = '';
    if (options) {
        for (i in options) {
            params +="&"+ i + '=' + options[i] + '&';
        }
    }
    return  params.substr(0, params.length - 1);
}

function getIosObject(NativeApis) {
    localStorage.setItem("nativeApis", NativeApis);
}


/**
 * input自动添加删除按钮
 */

$(function () {
    $('input.auto-delete').on('input', function () {
        !!$(this).val() ? $(this).next().show() : $(this).next().hide();
    }).on("blur", function () {
        var obj = $(this).next();
        setTimeout(function () { obj.hide() }, 200);
    }).on('focus', function () {
        !!$(this).val() ? $(this).next().show() : $(this).next().hide();
    }, null).each(function () {
        $(this).before("<div class='input-wrapper' style='display:inline-block;position:relative;line-height:0;'></div>");
        $(this).prev().append($(this)).append('<i class="inputDelete"></i>');
    });


    $('.inputDelete').on("click touchstart", function () {
        $(this).prev().val("").trigger("input").trigger("change").trigger("keydown").trigger("keyup").trigger("keypress").trigger("blur").focus();
    }).on("touchstart", function () {
        $(this).css({ "opacity": "1" });
    }).on("touchend", function () {
        $(this).css({ "opacity": "0.7" });//"background":"url(../../images/common/x.png) no-repeat",
    }).hide();

    $(window).resize(function () {
        if (innerHeight < 800) {
            $(".menu").css("position", "absolute");
        } else {
            $(".menu").css("position", "fixed");
        }
    });

    //返回按钮和获取验证码按钮的二胎效果

    $('#getVerify,.back,a.orderbtn').on('touchstart', function () {
        $(this).css({ "background-color": "rgba(0,0,0,0.1" });
    })
    .on('touchend', function () {
        $(this).css({ "background-color": "" });
    }); //上午下午
    $('#halfInput').click(function () {
        if ($('#half').height() == "0") {
            $(this).css({ "background": "url(../../images/doctor/arrdowup.png) right center no-repeat" });
        } else {
            $(this).css({ "background": "url(../../images/doctor/arrdown.png) right center no-repeat" });
        }
        $('#half a').click(function () {
            $('#halfInput').css({ "background": "url(../../images/doctor/arrdown.png) right center no-repeat" });
        });
    }); //男女性别
    $('.order-sex').click(function () {
        if ($('.selectlist').height() == "0") {
            $(this).css({ "background": "url(../../images/doctor/arrdowup.png) right center no-repeat", "background-position-x": "360px" });
        } else {
            $(this).css({ "background": "url(../../images/doctor/arrdown.png) right center no-repeat", "background-position-x": "360px" });
        }
    });
    $('.selectlist span').click(function () {
        $('.order-sex').css({ "background": "url(../../images/doctor/arrdown.png) right center no-repeat", "background-position-x": "360px" });
    });
    //点击后的背景效果
    $('.query-menu a').on('click', function () {
        $('.query-menu a').css({ "background-color": "rgba(255,255,255,0)" });
        $(this).css({ "background-color": "rgba(255,255,255,1)" })
    })
});

window.NewInvokeAction = function (controller, options, method, callback, useCache) {

    var useMask = true;
    $.ajax({
        url: "/api/" + controller,
        type: method,
        data: { "": options },
        beforeSend: function (xhr) {
            xhr.setRequestHeader('token', getCookie('token'));
            if (useMask) loadDiv();//加载层
        },
        timeout: 60000,//15秒超时
        success: function (data) {
            if (useMask) completeLoading();//关闭加载层
            if (typeof data.Data != "undefined") data = data.Data;
            console.log(data);
            if (typeof callback === 'function') callback(data);
        },
        error: function (e, g, h) {
            if (useMask) completeLoading();//关闭加载层
            if (e.statusText == "Forbidden") {
                app.msgAlert("操作失败", "<p>您尚未登录，请先去登录</p>", false);
                $(".popbox .submitbtn").unbind().click(function () {
                    if (/wechat/i.test(location.href)) {
                        location.href = '/Html/ResvWechat/index.html';
                    } else {
                        location.href = '/login';
                    }
                });
            }
            else {
                app.msgAlert("操作失败", "<p>抱歉，操作失败，请检查你的网络连接</p>", true);
                console.log(e, g);
            }
        }, omplete: function () {
            if (useMask) completeLoading();//关闭加载层
        }
    });

};
//navigator.for.ios
if (!!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
    window.RedirectListner = {};
    window.RedirectListner.goToIndex = function () {
        location.href = "rayelink://navigator.for.ios//"
    }
}

//验证手机号码是否合法
window.checkPhone=function(aPhone) {  
    var bValidate = RegExp(/^(0|86|17951)?(13[0-9]|15[0-9]|17[0-9]|18[0-9]|14[57])[0-9]{8}$/).test(aPhone);  
    if (bValidate) {  
        return true;  
    }  
    else{
        return false;  
    }            
}  