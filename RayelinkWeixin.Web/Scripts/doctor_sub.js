$(function () {
    $('.choose_wrap').each(function () {
        var This = $(this),
            choose_time = This.find('.choose_time'),
            time_td = choose_time.find('td');
        $('.yuyue,.jiahao').bind('touchstart touchend', function () {
            $(this).toggleClass('active');
        })
        time_td.bind('click', function () {
            if (!$(this).hasClass('opacity_time')) {
                time_td.removeClass('current_time');
                $(this).toggleClass('current_time');
            }
        })
    })

    $('.active_btn').each(function () {
        $('.yiguanzhu,.guanzhu,.songxinyi').bind('touchstart touchend click', function () {
            $(this).toggleClass('active_style');
            if ($(this).hasClass('songxinyi')) {
                window.location.href = '/Doctor/SendPrize?docID=' + WebAPP.common.docID;
            }
        })
    });

    $('.choose_wrap').each(function () {
        var This = $(this),
            RL_arrow = This.find('.arrow_right,.arrow_left');
        RL_arrow.bind('touchstart touchend', function () {
            $(this).toggleClass('active_style');
        })
    })
    $('.byebye').bind("click", close)

    function close(e) {
        e.preventDefault();
        $(".title_box").toggleClass("hide")
            .parent(".alert_mes").toggleClass("hide");

    }
    $('.summary_wrap').each(function () {
        var This = $(this),
            zhankai = This.find('.zhankai');
         if ($('.summary_section_wrap').eq(1).find('.summary_section').height() < 140) {
                $('.summary_section_wrap').eq(1).css('min-height', '140px');
         }
        zhankai.click(function (e) {
            e.preventDefault();
            switch (true) {
                case $(this).children('.arrow_bottom').hasClass('arrowTop'):
                    $(this).children('span').html("展开");
                    break;
                default:
                    $(this).children('span').html("收起");
                    break;
            }
            $(this).children('.arrow_bottom').toggleClass('arrow_top arrowTop').end().parent('.zhankai_wrap').siblings('.summary_section_wrap').toggleClass('toggles');
        })
    })


    //window.addEventListener("scroll", scroll_toggle, false)
    window.addEventListener("touchmove", scroll_toggle, false)
    $(window).scroll(function (event) {
        scroll_toggle();
    });

    //滚蛋的头部收缩
    function scroll_toggle() {
        var scrollHeight = $("body").scrollTop(),
            choose_wrap = $(".choose_wrap").innerHeight(),
            currentLi = null,
            tops = $(".summary_wrap").map(function () { return $(this).offset().top }).get(),
            nav_li = $(".summary_nav ul li");

        if (scrollHeight > 20) {
            $(".header_info").addClass("scroll_toggle");
        } else {
            $(".header_info").removeClass("scroll_toggle")
        }
        for (var i = tops.length - 1; i >= 0; i--) {
            if (scrollHeight > tops[i]) {
                current_li(i);
                $(".summary_nav").addClass("scroll_nav")
                break;
            }
        }
        if (scrollHeight < tops[0]) {
            $(".summary_nav").removeClass("scroll_nav")
        }


    }

    $(".summary_nav ul li").click(function () {
        var This = $(this),
            tops = $(".summary_wrap").map(function () { return $(this).offset().top }).get()
        currentLi = $(this).index();
        current_li(currentLi)
        if ($(".summary_nav").hasClass("scroll_nav"))
        {
            $("body").scrollTop(tops[currentLi + 1] - 110)
        } else
        {
            $("body").scrollTop(tops[currentLi + 1] - 160)
        }
       
    })

    //导航条高亮
    function current_li(indexs) {
        $(".summary_nav ul li").eq(indexs).addClass("current_nav")
            .siblings().removeClass("current_nav");
    }

    //$(".choose_btn").click(function (e) {
    //    e.preventDefault();
    //    alert_mes("温馨提示", "使用\"关注\"功能需绑定账号。绑定账号后，被关注的医生可以在“个人中心-我的关注”中进行查询。", "立即绑定")
    //})
})