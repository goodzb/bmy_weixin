﻿var WeChatShare = (WeChatShare || {});
(function (WeChatShare) {
    "use strict";

    WeChatShare.DefaultSetting = {
        WeChatJSSDK: "https://res.wx.qq.com/open/js/jweixin-1.0.0.js",

        WeChatShareOption: {
            ShareTimeline: {
                title: '', // 分享标题
                desc: '', // 分享描述
                link: '', // 分享链接
                imgUrl: '' // 分享图标
            },
            ShareQQ: {
                title: '', // 分享标题
                desc: '', // 分享描述
                link: '', // 分享链接
                imgUrl: '' // 分享图标
            },
            SendAppMessage: {
                title: '', // 分享标题
                desc: '', // 分享描述
                link: '', // 分享链接
                imgUrl: '' // 分享图标
            },
            ShareWeibo: {
                title: '', // 分享标题
                desc: '', // 分享描述
                link: '', // 分享链接
                imgUrl: '' // 分享图标
            },
            Default: {
                title: '', // 分享标题
                desc: '', // 分享描述
                link: '', // 分享链接
                imgUrl: '', // 分享图标
                type: '', // 分享类型,music、video或link，不填默认为link
                dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                },
                complete: function () {

                },
                fail: function () {

                },
                trigger: function () {

                }
            }

        },
        WeChatConfig: {
            debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: '', // 必填，公众号的唯一标识
            timestamp: '', // 必填，生成签名的时间戳
            nonceStr: '', // 必填，生成签名的随机串
            signature: '',// 必填，签名，见附录1
            jsApiList: ["onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo", "startRecord", "stopRecord", "onVoiceRecordEnd", "playVoice", "pauseVoice", "stopVoice", "onVoicePlayEnd", "uploadVoice", "downloadVoice", "chooseImage", "previewImage", "uploadImage", "downloadImage", "translateVoice", "getNetworkType", "openLocation", "getLocation", "hideOptionMenu", "showOptionMenu", "hideMenuItems", "showMenuItems", "hideAllNonBaseMenuItem", "showAllNonBaseMenuItem", "closeWindow", "scanQRCode", "chooseWXPay", "openProductSpecificView", "addCard", "chooseCard", "openCard"] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
        }
    };

    WeChatShare.WeChatShare = function (opt, cb) {
        var base = this;
        this.Option = opt;
        this.LoadSDKScript = function (src, callback) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = src;
            script.onload = script.onreadystatechange = function () {
                if (callback) {
                    callback();
                }
            }
            document.head.appendChild(script);
        };

        this.SetWeChatShareOption = function (option) {
            if (!option.title) {
                option.title = WeChatShare.DefaultSetting.WeChatShareOption.Default.title;
            }
            else {
                WeChatShare.DefaultSetting.WeChatShareOption.Default.title = option.title;
            }
            if (!option.desc) {
                option.desc = WeChatShare.DefaultSetting.WeChatShareOption.Default.desc;
            }
            else {
                WeChatShare.DefaultSetting.WeChatShareOption.Default.desc = option.desc;
            }
            if (!option.link) {
                option.link = WeChatShare.DefaultSetting.WeChatShareOption.Default.link;
            }
            else {
                WeChatShare.DefaultSetting.WeChatShareOption.Default.link = option.link;
            }
            if (!option.imgUrl) {
                option.imgUrl = WeChatShare.DefaultSetting.WeChatShareOption.Default.imgUrl;
            }
            else {
                WeChatShare.DefaultSetting.WeChatShareOption.Default.imgUrl = option.imgUrl;
            }
        };

        this.Ready = function (option, callback) {
            wx.ready(function () {
                base.hideAllNonBaseMenuItem();
                base.SetWeChatShareOption(option);

                //如果构造的参数当中定义了对应的处理函数,则设置到默认设置上
                if (option.success) {
                    WeChatShare.DefaultSetting.WeChatShareOption.Default.success = option.success;
                }
                if (option.fail) {
                    WeChatShare.DefaultSetting.WeChatShareOption.Default.fail = option.fail;
                }
                if (option.complete) {
                    WeChatShare.DefaultSetting.WeChatShareOption.Default.complete = option.complete;
                }
                if (option.cancel) {
                    WeChatShare.DefaultSetting.WeChatShareOption.Default.cancel = option.cancel;
                }
                //绑定事件
                base.weixinShareTimeline(option);
                base.weixinShareQQ(option);
                base.weixinShareWeibo(option);
                base.weixinSendAppMessage(option);

                //如果构造的参数当中定义了对应的回调函数,则执行
                if (option.callback) {
                    option.callback(base);
                }

                if (callback) {
                    callback(base);
                }
            });
        };

        this.Error = function () {
            wx.error(function (res) {
                //WeChatShare.Tracker("JSSDK_ERROR", "error", opt);
            });
        };

        //注册回调事件
        this.RegeisterShareCallBack = function (setting, option, shareType) {
            if (setting.success) {
                var _success = setting.success;
                setting.success = function (res) {
                    WeChatShare.Tracker("WeChatShareSuccess", shareType.name + "_success", option);
                    _success(res, shareType);
                }
            }
            if (setting.fail) {
                var _fail = setting.fail;
                setting.fail = function (res) {
                    WeChatShare.Tracker("WeChatShareFail", shareType.name + "_fail", option);
                    _fail(res, shareType);
                }
            }
            if (setting.complete) {
                var _complete = setting.complete;
                setting.complete = function (res) {
                    WeChatShare.Tracker("WeChatShareComplete", shareType.name + "_complete", option);
                    _complete(res, shareType);
                }
            }
            if (setting.cancel) {
                var _cancel = setting.cancel;
                setting.cancel = function (res) {
                    WeChatShare.Tracker("WeChatShareCancel", shareType.name + "_cancel", option);
                    _cancel(res, shareType);
                }
            }
        };

        //clone对象
        this.clone = function (data) {
            var cloneData = {};
            for (var key in data) {
                if (typeof (data[key]) == "object") {
                    cloneData[key] = base.clone(data[key]);
                }
                else if (typeof (data[key]) == "array") {
                    cloneData[key] = data[key].splice(0);
                }
                else if (typeof (data[key]) == "function") {
                    cloneData[key] = data[key];
                }
                else {
                    cloneData[key] = data[key];
                }
            }

            return cloneData;
        };

        this.weixinShareTimeline = function (option) {
            var setting = WeChatShare.DefaultSetting.WeChatShareOption.ShareTimeline;
            if (!setting.title || setting.title == "") {
                setting = base.clone(WeChatShare.DefaultSetting.WeChatShareOption.Default);
                console.log(setting);
            }
            base.RegeisterShareCallBack(setting, option, { name: "ShareTimeline", value: 1 });  //分享成功后回调函数
            wx.onMenuShareTimeline(setting);
        };

        this.weixinSendAppMessage = function (option) {
            var setting = WeChatShare.DefaultSetting.WeChatShareOption.SendAppMessage;
            if (!setting.title || setting.title == "") {
                setting = base.clone(WeChatShare.DefaultSetting.WeChatShareOption.Default);
                console.log(setting);
            }
            base.RegeisterShareCallBack(setting, option, { name: "SendAppMessage", value: 2 }); //分享成功后回调函数
            wx.onMenuShareAppMessage(setting);
        };

        this.weixinShareWeibo = function (option) {
            var setting = WeChatShare.DefaultSetting.WeChatShareOption.ShareWeibo;
            if (!setting.title || setting.title == "") {
                setting = base.clone(WeChatShare.DefaultSetting.WeChatShareOption.Default);
                console.log(setting);
            }
            base.RegeisterShareCallBack(setting, option, { name: "ShareWeibo", value: 3 }); //分享成功后回调函数
            wx.onMenuShareWeibo(setting);
        };

        this.weixinShareQQ = function (option) {
            var setting = WeChatShare.DefaultSetting.WeChatShareOption.ShareQQ;
            if (!setting.title || setting.title == "") {
                setting = base.clone(WeChatShare.DefaultSetting.WeChatShareOption.Default);
                console.log(setting);
            }
            base.RegeisterShareCallBack(setting, option, { name: "ShareQQ", value: 4 }); //分享成功后回调函数
            wx.onMenuShareQQ(setting);
        };

        /**
        * 显示网页右上角的按钮
        */
        this.showOptionMenu = function () {
            wx.showOptionMenu();
        }

        /**
         * 隐藏网页右上角的按钮
         */
        this.hideOptionMenu = function () {
            wx.hideOptionMenu();
        }

        /**
         * 返回如下几种类型：
         *
         * network_type:wifi     wifi网络
         * network_type:edge     非wifi,包含3G/2G
         * network_type:fail     网络断开连接
         * network_type:wwan     2g或者3g
         *
         * @param callback
         */
        this.getNetworkType = function (callback) {
            wx.getNetworkType({
                success: function (res) {
                    var networkType = res.networkType; // 返回网络类型2g，3g，4g，wifi
                    if (callback) callback(res);
                    //alert(JSON.stringify(res));
                }
            });
        }

        /**
         * 关闭当前微信公众平台页面
         */
        this.closeWindow = function () {
            wx.closeWindow();
        }

        //批量隐藏功能按钮接口
        this.hideMenuItems = function (btn) {
            wx.hideMenuItems({
                menuList: (btn.constructor == Array ? btn : btn.split(",")) // 要隐藏的菜单项，所有menu项见附录3
            });
        }

        //批量显示功能按钮接口
        this.showMenuItems = function (btn) {
            wx.showMenuItems({
                menuList: (btn.constructor == Array ? btn : btn.split(",")) // 要隐藏的菜单项，所有menu项见附录3
            });
        }

        //隐藏所有非基础按钮接口
        this.hideAllNonBaseMenuItem = function () {
            wx.hideAllNonBaseMenuItem();
        }

        //显示所有功能按钮接口
        this.showAllNonBaseMenuItem = function () {
            wx.showAllNonBaseMenuItem();
        }

        this.Init = function (initOption, callback) {
            WeChatShare.DefaultSetting.WeChatConfig.appId = initOption.appId;
            WeChatShare.DefaultSetting.WeChatConfig.timestamp = initOption.timestamp;
            WeChatShare.DefaultSetting.WeChatConfig.nonceStr = initOption.nonceStr;
            WeChatShare.DefaultSetting.WeChatConfig.signature = initOption.signature;
            //加载jssdk
            base.LoadSDKScript(WeChatShare.DefaultSetting.WeChatJSSDK, function () {
                WeChatShare.DefaultSetting.WeChatConfig.debug = initOption.debug || false;
                wx.config(WeChatShare.DefaultSetting.WeChatConfig);
                base.Ready(initOption, callback);
                base.Error();
            });
        };
        this.Init(opt, cb);
        return this;
    };
 
})(WeChatShare || (WeChatShare = {}));
