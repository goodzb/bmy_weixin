$(function(){
    $("#user_form").each(function(){
        var This = $(this),
            sex_m = This.find(".custom_style_sex .m"),
            sex_f = This.find(".custom_style_sex .f");
        //选择男性
        sex_m.click(function () {
            if (!$(this).hasClass("current_sex")) {
                $(this).toggleClass("current_sex");
                sex_f.removeClass("current_sex");
            }
        })
        //选择女性
        sex_f.click(function () {
            if (!$(this).hasClass("current_sex")) {
                $(this).toggleClass("current_sex");
                sex_m.removeClass("current_sex");
            }
        })
    })
    
    //上传图片和删除图片
    $('.share_file').each(function(){
        var This = $(this),
            share_x = This.find('.share_x'),
            add_file = This.find('.no_file');
       share_x.bind('click',function(){
            This.remove();
       })
       $('.no_file').bind('click',function(){
           
            $('.open_photo').toggleClass('shadow');
       })
   })
   $('.choose_photo_list').each(function(){
        var This = $(this),
            li = This.find('li'),
            close_btn = This.find('.byebye');
       
       li.bind('touchstart touchend',function(){
            $(this).toggleClass('active_style');
       })
       close_btn.click(function(){
            This.parent('.open_photo').addClass('shadow');
       })
   })
})