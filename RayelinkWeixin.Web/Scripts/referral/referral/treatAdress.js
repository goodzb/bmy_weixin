(function e(t, n, r) { function s(o, u) { if (!n[o]) { if (!t[o]) { var a = typeof require == "function" && require; if (!u && a) return a(o, !0); if (i) return i(o, !0); var f = new Error("Cannot find module '" + o + "'"); throw f.code = "MODULE_NOT_FOUND", f } var l = n[o] = { exports: {} }; t[o][0].call(l.exports, function (e) { var n = t[o][1][e]; return s(n ? n : e) }, l, l.exports, e, t, n, r) } return n[o].exports } var i = typeof require == "function" && require; for (var o = 0; o < r.length; o++) s(r[o]); return s })({
    1: [function (require, module, exports) {
        /**
         * 
         * @authors yangjie (914569804@qq.com)
         * @date    2016-09-30 17:38:04
         * @version $Id$
         */
        new Vue({
            el: "#treatAdress",
            data: {
                isActiveChoice: false,
                cityName: "选择城市",
                citys: [{ id: -1, name: '全部城市' }],
                hospitals: [],
                activeCityIndex: 0//选择城市的序号
            },
            methods: {
                init: function () {
                    this.loadCityList();
                    this.loadHospital(-1, 1);
                },
                loadHospital: function (cityId, pagenumber) {
                    Vue.http.post('/Service/GetOrganizationList', { cityId: cityId, pagenumber: pagenumber, pagesize: 100 })
                    .then(function (response) {
                        var jsonStr = response.json();
                        if (jsonStr != null && jsonStr != 'null') {
                            var data = JSON.parse(jsonStr);
                            var dataNew = [];
                            data.forEach(function (item) {
                                dataNew.push(item);
                                //if (!item.iscentral) {
                                //    dataNew.push(item);
                                //}
                            });
                            if (pagenumber == 1) {
                                //this.hospitals = dataNew.sort('hospitalName');
                                this.hospitals = dataNew;
                            } else {
                                //this.hospitals = (dataNew.concat(this.hospitals)).sort('hospitalName');
                                this.hospitals = (dataNew.concat(this.hospitals));
                            }
                        }
                        else {
                            if (pagenumber == 1) {
                                this.hospitals = [];
                            }
                        }

                    }.bind(this))
                    .catch(function () {
                        Toast.show({
                            msg: '服务器内部错误！'
                        });
                    });
                },
                loadCityList: function () {
                    Vue.http.post('/Service/GetCityList')
                        .then(function (response) {
                            if (response.body) {
                                var newData = this.citys.concat(JSON.parse(response.json()));
                                //this.citys = newData.sort('name');
                                this.citys = newData;
                                
                            } else {
                                this.citys = [];
                            }
                        }.bind(this))
                        .catch(function () {
                            Toast.show({
                                msg: '服务器内部错误！'
                            });
                        });
                },
                activeChoice: function () {
                    this.isActiveChoice = !this.isActiveChoice;
                },
                choiceList: function (city) {
                    this.cityName = city.name;
                    this.isActiveChoice = false;
                    this.loadHospital(city.id, 1);
                },
                closeChoice: function () {
                    this.isActiveChoice = false;
                },
                chooseOne: function (hospital) {
                    var ref = document.referrer.substr(document.referrer.lastIndexOf('/') + 1).toLowerCase();
                    var key = 'newReferral';
                    if (ref.indexOf( 'newreferral')!=0) {
                        key = 'newDocument';
                    }
                    var original = JSON.parse(localStorage.getItem(key));
                    if (original) {
                        original.HospitalName = hospital.hospitalName;
                        original.ClientName = hospital.hospitalName;
                        original.HospitalId = hospital.hospitalID;
                        original.ClientId = hospital.hospitalID;
                    } else {
                        original = { HospitalId: hospital.hospitalID, HospitalName: hospital.hospitalName, ClientName: hospital.hospitalName, ClientId: hospital.hospitalID };
                    }

                    localStorage.setItem(key, JSON.stringify(original));
                    //window.history.go(-1);
                    if (key == 'newDocument') {                        
                        location.replace("/Referral/" + ref);
                    }
                    else {
                        location.replace("/Referral/newReferral");
                    }
                }
            },
            ready: function () {
                this.init();
            }
        })

    }, {}]
}, {}, [1]);
