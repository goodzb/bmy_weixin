﻿(function e(t, n, r) { function s(o, u) { if (!n[o]) { if (!t[o]) { var a = typeof require == "function" && require; if (!u && a) return a(o, !0); if (i) return i(o, !0); var f = new Error("Cannot find module '" + o + "'"); throw f.code = "MODULE_NOT_FOUND", f } var l = n[o] = { exports: {} }; t[o][0].call(l.exports, function (e) { var n = t[o][1][e]; return s(n ? n : e) }, l, l.exports, e, t, n, r) } return n[o].exports } var i = typeof require == "function" && require; for (var o = 0; o < r.length; o++) s(r[o]); return s })({
    1: [function (require, module, exports) {
        /*
        *	confirm 组件
        */

        "use strict";

        module.exports = {
            template: "<div class='mui-confirm' v-show='isComponentShow' v-on:touchmove.prevent>\
        <div class='mui-confirm-masker' v-show='isShow' :transition='masker'></div>\
        <div class='mui-confirm-inner'>\
            <div class='mui-confirm-dialog' v-show='isShow' :transition='dialog'>\
                <div class='mui-confirm-dialog-content'>\
                    <p class='mui-confirm-dialog-title'>{{title}}<i class='byebye' v-touch:tap='onCallbackOne'></i></p>\
                    <p class='mui-confirm-dialog-msg'>{{msg}}</p>\
                </div>\
                <div class='mui-confirm-dialog-buttons'>\
					<a href='javascript:void(0)' v-touch:tap='onCallbackTwo' class='bangding mui-confirm-dialog-button'>{{buttons[1]}}</a>\
                    <a href='javascript:void(0)' v-touch:tap='onCallbackOne' class='bangdingno mui-confirm-dialog-button'>{{buttons[0]}}</a>\
                </div>\
            </div>\
        </div>\
    </div>",
            data: function () {
                return {
                    isComponentShow: !0,
                    isShow: !1,
                    buttonIndex: 0, //按钮索引1开始，从左到右1，2，...
                    masker: "mui-confirm-masker",
                    dialog: "mui-confirm-dialog"
                }
            }
        }
    }, {}], 2: [function (require, module, exports) {
        "use strict";

        var ConfirmTemp = require("./confirm"),
            ConfirmComponent = Vue.extend(ConfirmTemp),
            instance;

        // 初始化实例
        function show(op, cb) {
            var options = {
                title: "提示",
                buttons: ["取消", "确定"]
            };

            options.msg = op.msg ? op.msg : "";
            options.title = op.title ? op.title : "提示";
            options.buttons = op.buttons ? op.buttons : ["取消", "确定"];

            options.cb = cb ? cb : null;

            if (!instance) {
                var com = document.createElement("div");
                document.body.appendChild(com);

                instance = new ConfirmComponent({
                    el: com,
                    data: function () {
                        return {
                            title: options.title,
                            buttons: options.buttons,
                            msg: options.msg,
                            cb: options.cb
                        }
                    },
                    methods: {
                        onCallbackOne: function (e) {
                            this.isShow = !1;
                            this.buttonIndex = 1;
                            e.preventDefault();
                        },
                        onCallbackTwo: function (e) {
                            this.isShow = !1;
                            this.buttonIndex = 2;
                            e.preventDefault();
                        }
                    },
                    transitions: {
                        "mui-confirm-dialog": {
                            afterLeave: function () {
                                this.isComponentShow = !1;
                                typeof this.cb === "function" && this.cb({
                                    buttonIndex: this.buttonIndex
                                });
                            }
                        }
                    }
                });

                instance.isShow = !0;

            } else {
                instance.isComponentShow = !0;
                instance.isShow = !0;
                instance.title = options.title;
                instance.buttons = options.buttons;
                instance.msg = options.msg;
                instance.cb = options.cb;
            }

            return instance;
        }

        function hide() {
            instance.isShow = !1;
        }

        // 抛出show 和 hide方法
        module.exports = {
            show: show,
            hide: hide
        }

    }, { "./confirm": 1 }], 3: [function (require, module, exports) {
        /*
        *   loading组件，提供两种loading的效果
        *   1.默认，2.circle
        *   通过改变参数theme来设置主题，若是默认，则不需要设置任何主题
        *   可以设置参数noModal是否是模态来，非模态的状态下可以对loading下面的页面功能进行操作
        */

        "use strict";

        var LoadingTemp = require("./loading"),
            LoadingComponent = Vue.extend(LoadingTemp),
            instance;


        // 初始化组件
        function init(op) {
            var container = document.createElement("div");
            document.body.appendChild(container);

            var options = op || {};

            return new LoadingComponent({
                el: container,
                data: function () {
                    return {
                        isLoading: !0,
                        theme: op.theme || "",
                        noModal: op.noModal || !1
                    }
                }
            });
        }


        // 组件show
        function show(op) {
            var options = {};

            options = op ? op : {};

            if (!instance) {
                instance = init(options);
            } else {
                instance.isLoading = true;
                instance.theme = options.theme ? options.theme : "";
                instance.noModal = options.noModal ? options.noModal : !1;
            }

            return instance;
        }


        // 组件hide
        function hide() {
            if (instance) {
                instance.isLoading = false;
            }
        }


        // 抛出方法
        module.exports = {
            show: show,
            hide: hide
        }



    }, { "./loading": 4 }], 4: [function (require, module, exports) {
        /*
        *	loading组件
        *	theme主题，可选，默认是gray
        *	noModal模态，模态状态下不可以操作，非模态状态下可以操作，比如loading的过程中可以点击返回操作等。
        * 	noModal作用不大，先保留吧
        */

        "use strict";

        module.exports = {
            template: "<div class='mui-loading' :class='[theme ? theme: \"\", noModal ? \"mui-loading-nomodal\": \"\"]' v-show='isLoading'>\
		<div class='mui-loading-inner'>\
			<div class='mui-loading-spiner'>\
				<span class='mui-loading-spin'>\
				</span>\
			</div>\
		</div>\
	</div>"
        }
    }, {}], 5: [function (require, module, exports) {
        /*
        *	toast用于显示说明性的文字等。
        *	以浮层的形式弹出, 有几种形式
        *   1.纯文本，2.正确带icon，3.错误带icon
        * 	duration 显示时间
        */

        "use strict";

        var ToastTmep = require("./toast"),
            ToastCom = Vue.extend(ToastTmep),
            instance,
            clearTime;


        function show(op) {
            var _options = op ? op : {};

            if (!instance) {

                var com = document.createElement("div");
                document.body.appendChild(com);

                instance = new ToastCom({
                    el: com,
                    data: function () {
                        return {
                            type: _options.type || 1,
                            msg: _options.msg ? _options.msg : ""
                        }
                    },
                    transitions: {
                        "mui-toast-content": {
                            afterLeave: function () {
                                this.isComponentShow = !1;
                            }
                        }
                    }
                });

                instance.isShow = !0;
                instance.duration = _options.duration ? _options.duration : 1500;

            } else {
                instance.isComponentShow = !0;
                instance.isShow = !0;
                instance.type = _options.type ? _options.type : 1;
                instance.msg = _options.msg ? _options.msg : "";
                instance.duration = _options.duration ? _options.duration : 1500
            }

            clearTime && clearTimeout(clearTime);

            clearTime = setTimeout(function () {
                hide();
            }, instance.duration);

            return instance;
        }

        function hide() {
            instance.isShow = !1;
        }


        module.exports = {
            show: show,
            hdie: hide
        }

    }, { "./toast": 6 }], 6: [function (require, module, exports) {
        "use strict";

        module.exports = {
            template: "<div class='mui-toast' :class='[type==2? \"mui-toast-2\": \"\", type==3? \"mui-toast-3\": \"\"]' v-show='isComponentShow'>\
				<div class='mui-toast-inner'>\
					<div class='mui-toast-content' v-show='isShow' :transition='dialog'>\
						<i class='mui-iconfont mui-icon-check' v-if='type == 2'></i>\
						<i class='mui-iconfont mui-icon-warn' v-if='type == 3'></i>\
						<div class='mui-toast-body'>\
							{{msg}}\
						</div>\
					</div>\
				</div>\
			</div>",
            data: function () {
                return {
                    isShow: !1,
                    isComponentShow: !0,
                    duration: 1500,
                    dialog: "mui-toast-content"
                }
            }
        }
    }, {}], 7: [function (require, module, exports) {
        /**
         * 
         * @authors yangjie (914569804@qq.com)
         * @date    2016-10-08 11:23:45
         * @version $Id$
         */
        var Loading = require("../common/components/loading/");
        var Toast = require("../common/components/toast/");
        var Confirm = require("../common/components/confirm/");

        var Loadmore = require('mint-ui/lib/Loadmore');
        Vue.component('load-more', Loadmore);

        new Vue({
            el: "#choiceDoctor",
            data: {
                searchKey: '搜索',
                activeBarIndex: null,
                hospital: "选择医院",
                department: "选择科室",
                activeHospitalName: '', //选择医院默认
                activeHospitalIndex: 0,//选择医院的序号
                activeDepartIndex: 0,//选择大科室part 默认
                activeDeptId: 0, //选择一级科室
                activeDept2Name: '',//选择的二级科室
                activeItemId: 0,//选择二级科室序号
                hospitalLists: [],
                departmentParts: [],
                departmentItems: [],
                allLoaded: false,
                pagenumber: 0,//当前加载第几页
                doctors: [],
                localIdsCopy: [],
                imgPaths: [],
                nowTime: ''
            },
            methods: {
                init: function () {

                    Loading.show()

                    //Confirm.show({
                    //	title: "提示",
                    //	msg: "确定要删除该地址吗dsds？",
                    //	buttons: ["确定", "取消"]
                    //},function(ret){
                    //	if(ret.buttonIndex === 1){
                    //		alert("点击确定回调函数")
                    //	}else if(ret.buttonIndex === 2){
                    //		alert("点击取消回调函数")
                    //	}
                    //})

                    //如果是从搜索页面过来就要先去搜索词
                    var key = localStorage.getItem('searchKey');
                    if (key) {
                        this.searchKey = key;
                    }
                    this.loadHospital();
                    this.loadDepts();

                    //this.loadDoctors();
                    setTimeout(this.loadDoctors, 500);
                    var original = localStorage.getItem('newReferral');
                    if (original) {
                        this.localIdsCopy = JSON.parse(original).localIds || [];
                    }

                    var nowDate = new Date();
                    var year = nowDate.getFullYear() + "";
                    var month = nowDate.getMonth() + 1 + "";
                    if (month.length == 1) {
                        month = "0" + month;
                    }
                    var day = nowDate.getDate() + "";
                    if (day.length == 1) {
                        day = "0" + day;
                    }
                    this.nowTime = year + month + day;
                },
                loadHospital: function () {
                    //加载医院列表
                    Vue.http.post('/referral/FindAllHospital')
                    .then(function (response) {
                        if (response.body) {
                            this.hospitalLists = response.json();
                        }
                    }.bind(this))
                    .catch(function () {
                        Toast.show({
                            msg: '服务器内部错误！'
                        });
                    });
                },
                loadDepts: function () {
                    //加载科室
                    Vue.http.post('/referral/FindAllDepts')
                    .then(function (response) {
                        if (response.body) {
                            this.departmentParts = response.json();
                            this.departmentItems = this.departmentParts[0].Children;
                        }
                    }.bind(this))
                    .catch(function () {
                        Toast.show({
                            msg: '服务器内部错误！'
                        });
                    });
                },
                loadDoctors: function () {
                    //加载医生
                    Loading.show();
                    this.pagenumber += 1;

                    Vue.http.post('/service/GetDoctorList', { pagesize: 20, pagenumber: this.pagenumber, key: this.searchKey == '搜索' ? '' : this.searchKey, deptId: this.activeDeptId == 0 ? -1 : this.activeDeptId, hospitalName: this.activeHospitalName, deptName2: (this.activeDept2Name == '全部') ? '' : this.activeDept2Name })
                    .then(function (response) {
                        var jsonStr = response.json();
                        if (!jsonStr) {
                            Loading.hide();
                            if (this.pagenumber == 1) {
                                this.doctors = [];
                            } else {
                                Toast.show({
                                    msg: '加载完毕，已无更多内容'
                                });
                            }
                            this.pagenumber -= 1
                            //this.allLoaded = true;
                            return;
                        }
                        var data = JSON.parse(jsonStr);
                        if (data) {
                            data.forEach(function (item) {
                                item.isShow = false;
                            });
                            if (this.pagenumber == 1) {
                                this.doctors = data;
                            } else {
                                this.doctors = this.doctors.concat(data);
                            }
                            //if (data.length < 20) {
                            //    this.pagenumber -= 1
                            //    //this.allLoaded = true;
                            //}
                        } else if (this.pagenumber == 1) {
                            this.doctors = [];
                            this.pagenumber -= 1
                            //this.allLoaded = true;
                        }
                        Loading.hide();
                        //if (loaderId) {
                        //    this.$broadcast('onTopLoaded', loaderId);
                        //}
                    }.bind(this))
                    .catch(function (e) {
                        Loading.hide();
                        Toast.show({
                            msg: '服务器内部错误！' + JSON.stringify(e)
                        });
                    }.bind(this));
                },
                choiceTab: function (_index) { //tab下拉选项
                    if (this.activeBarIndex == _index) {
                        this.activeBarIndex = null;
                    } else {
                        this.activeBarIndex = _index;
                    }
                },
                closeChoice: function () { //关闭下拉选项
                    this.activeBarIndex = null;
                },
                choiceHospitalList: function ($index, hospital) { //选择下拉列表
                    this.activeHospitalName = hospital.Name;
                    this.hospital = hospital.Name;
                    this.activeHospitalId = hospital.Id;
                    this.activeBarIndex = null;
                    this.activeHospitalIndex = $index;
                    this.pagenumber = 0;
                    this.loadDoctors();
                },
                choiceDepartPart: function ($index, dept) { //选择科室
                    this.activeDeptId = dept.UnionId;
                    this.departmentItems = dept.Children;
                    this.department = dept.Name;
                    this.activeItemId = 0;
                    this.pagenumber = 0;
                    //this.loadDoctors();
                },
                choiceDetartItem: function ($index, item) { //选择科室项目
                    if (item.Name != '全部') {
                        this.department = item.Name;
                    }

                    this.activeDept2Name = item.Name;
                    this.activeItemId = item.Id;
                    this.activeBarIndex = null;
                    this.pagenumber = 0;
                    this.loadDoctors();
                },
                showInfo: function (item) { //info擅长折叠
                    item.isShow = !item.isShow;
                    //event.preventDefault();
                    window.event ? window.event.cancelBubble = true : event.stopPropagation();
                },
                loadBottom: function (id) {
                    //this.allLoaded = true;  //不可下拉
                    //alert('ok');
                    this.loadDoctors();
                    this.$broadcast('onBottomLoaded', id);
                },
                chooseDoc: function (doc) {//选中某医生
                    //alert('冒泡了'); return;
                    var that = this;
                    Confirm.show({
                        title: "温馨提示",
                        msg: "确定转诊给" + doc.name + "医生？",
                        buttons: ["取消", "确定"]
                    }, function (ret) {
                        if (ret.buttonIndex === 1) {
                            return;
                        } else if (ret.buttonIndex === 2) {
                            var ref = document.referrer.substr(document.referrer.lastIndexOf('/') + 1).toLowerCase();
                            if (ref.indexOf('newreferral')>=0) {
                                Loading.show();
                                that.uploadImgs(doc);
                            } else {
                                var original = JSON.parse(localStorage.getItem('newDocument'));
                                original.DocBrif = doc.name + '（' + ((doc.depName2 == 'undefined') ? '' : doc.depName2) + '）';
                                original.DocId = doc.id;
                                localStorage.setItem('newDocument', JSON.stringify(original));
                                //window.history.go(-1);
                                location.replace("/Referral/" + ref);
                            }
                        }
                    });
                },
                uploadImgs: function (doc) {//上传图片
                    if (doc) {
                        var original = JSON.parse(localStorage.getItem('newReferral'));
                        original.docId = doc.id;
                        original.DocBrif = doc.name + '（' + ((!doc.depName2 ) ? '' : doc.depName2) + '）';
                        localStorage.setItem('newReferral', JSON.stringify(original));
                    }

                    if (this.localIdsCopy.length == 0) {
                        this.submitNewReferral();
                        return;
                    }
                    var item = this.localIdsCopy[0];
                    var that = this;
                    wx.uploadImage({
                        localId: item, // 需要上传的图片的本地ID，由chooseImage接口获得
                        isShowProgressTips: 0, // 默认为1，显示进度提示
                        success: function (res) {
                            var serverId = res.serverId; // 返回图片的服务器端ID
                            try {
                                Vue.http.post('/Order/downloadPics', { serverId: serverId, openid: openid })
                                .then(function (response) {
                                    that.imgPaths.push("/UploadFile/" + that.nowTime + "/" + serverId + ".png");
                                    that.localIdsCopy.splice(0, 1);
                                    if (that.localIdsCopy.length > 0) {
                                        that.uploadImgs();
                                    } else {
                                        that.submitNewReferral();
                                    }
                                }.bind(that))
                                .catch(function () {
                                    Toast.show({
                                        msg: '服务器内部错误！'
                                    });
                                });

                            } catch (e) {
                                alert(e.message);
                            }

                        }
                    });

                },
                submitNewReferral: function () {
                    try {
                        var original = JSON.parse(localStorage.getItem('newReferral'));
                        Vue.http.post('/referral/AddReferral', { name: original.name, phone: original.phone, desc: original.desc, hospitalId: original.HospitalId, docId: original.docId, imgPaths: this.imgPaths.join(';'), docBrif: original.DocBrif, hospitalName: original.HospitalName })
                        .then(function (response) {
                            Loading.hide();
                            if (response.body == '0') {
                                Toast.show({
                                    msg: '新建失败！'
                                });
                            } else {
                                //Toast.show({
                                //    msg: '转诊需求提交成功!'
                                //});
                                //setTimeout("window.location.href = '/referral/myreferral'", 1000);

                                window.location.href = '/referral/myreferral';
                            }
                        }.bind(this))
                        .catch(function () {
                            Toast.show({
                                msg: '服务器内部错误！'
                            });
                        });
                    } catch (e) {
                        Toast.show({
                            msg: e.message
                        });
                    }

                }
            },
            ready: function () {

                this.init();
            }
        });
        //离开页面前清空
        window.onbeforeunload = function () {
            localStorage.removeItem('searchKey');
        }

    }, { "../common/components/confirm/": 2, "../common/components/loading/": 3, "../common/components/toast/": 5, "mint-ui/lib/Loadmore": 8 }], 8: [function (require, module, exports) {
        module.exports =
        /******/ (function (modules) { // webpackBootstrap
            /******/ 	// The module cache
            /******/ 	var installedModules = {};

            /******/ 	// The require function
            /******/ 	function __webpack_require__(moduleId) {

                /******/ 		// Check if module is in cache
                /******/ 		if (installedModules[moduleId])
                    /******/ 			return installedModules[moduleId].exports;

                /******/ 		// Create a new module (and put it into the cache)
                /******/ 		var module = installedModules[moduleId] = {
                    /******/ 			exports: {},
                    /******/ 			id: moduleId,
                    /******/ 			loaded: false
                    /******/
                };

                /******/ 		// Execute the module function
                /******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

                /******/ 		// Flag the module as loaded
                /******/ 		module.loaded = true;

                /******/ 		// Return the exports of the module
                /******/ 		return module.exports;
                /******/
            }


            /******/ 	// expose the modules object (__webpack_modules__)
            /******/ 	__webpack_require__.m = modules;

            /******/ 	// expose the module cache
            /******/ 	__webpack_require__.c = installedModules;

            /******/ 	// __webpack_public_path__
            /******/ 	__webpack_require__.p = "";

            /******/ 	// Load entry module and return exports
            /******/ 	return __webpack_require__(0);
            /******/
        })
        /************************************************************************/
        /******/({

            /***/ 0:
            /***/ function (module, exports, __webpack_require__) {

                module.exports = __webpack_require__(98);


                /***/
            },

            /***/ 98:
            /***/ function (module, exports, __webpack_require__) {

                'use strict';

                module.exports = __webpack_require__(99);

                /***/
            },

            /***/ 99:
            /***/ function (module, exports, __webpack_require__) {

                var __vue_script__, __vue_template__
                var __vue_styles__ = {}
                __webpack_require__(100)
                __vue_script__ = __webpack_require__(102)
                if (__vue_script__ &&
                    __vue_script__.__esModule &&
                    Object.keys(__vue_script__).length > 1) {
                    console.warn("[vue-loader] packages/loadmore/src/loadmore.vue: named exports in *.vue files are ignored.")
                }
                __vue_template__ = __webpack_require__(110)
                module.exports = __vue_script__ || {}
                if (module.exports.__esModule) module.exports = module.exports.default
                var __vue_options__ = typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports
                if (__vue_template__) {
                    __vue_options__.template = __vue_template__
                }
                if (!__vue_options__.computed) __vue_options__.computed = {}
                Object.keys(__vue_styles__).forEach(function (key) {
                    var module = __vue_styles__[key]
                    __vue_options__.computed[key] = function () { return module }
                })


                /***/
            },

            /***/ 100:
            /***/ function (module, exports) {

                // removed by extract-text-webpack-plugin

                /***/
            },

            /***/ 102:
            /***/ function (module, exports, __webpack_require__) {

                'use strict';

                exports.__esModule = true;

                var _fadingCircle = __webpack_require__(103);

                var _fadingCircle2 = _interopRequireDefault(_fadingCircle);

                function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

                exports.default = {
                    name: 'mt-loadmore',
                    components: {
                        'spinner': _fadingCircle2.default
                    },

                    props: {
                        maxDistance: {
                            type: Number,
                            default: 150
                        },
                        autoFill: {
                            type: Boolean,
                            default: true
                        },
                        topPullText: {
                            type: String,
                            default: '下拉刷新'
                        },
                        topDropText: {
                            type: String,
                            default: '释放更新'
                        },
                        topLoadingText: {
                            type: String,
                            default: '加载中...'
                        },
                        topDistance: {
                            type: Number,
                            default: 70
                        },
                        topMethod: {
                            type: Function
                        },
                        topStatus: {
                            type: String,
                            default: ''
                        },
                        bottomPullText: {
                            type: String,
                            default: '上拉刷新'
                        },
                        bottomDropText: {
                            type: String,
                            default: '释放更新'
                        },
                        bottomLoadingText: {
                            type: String,
                            default: '加载中...'
                        },
                        bottomDistance: {
                            type: Number,
                            default: 70
                        },
                        bottomMethod: {
                            type: Function
                        },
                        bottomStatus: {
                            type: String,
                            default: ''
                        },
                        bottomAllLoaded: {
                            type: Boolean,
                            default: false
                        }
                    },

                    data: function data() {
                        return {
                            uuid: null,
                            translate: 0,
                            scrollEventTarget: null,
                            containerFilled: false,
                            topText: '',
                            topDropped: false,
                            bottomText: '',
                            bottomDropped: false,
                            bottomReached: false,
                            direction: '',
                            startY: 0,
                            startScrollTop: 0,
                            currentY: 0
                        };
                    },


                    watch: {
                        topStatus: function topStatus(val) {
                            switch (val) {
                                case 'pull':
                                    this.topText = this.topPullText;
                                    break;
                                case 'drop':
                                    this.topText = this.topDropText;
                                    break;
                                case 'loading':
                                    this.topText = this.topLoadingText;
                                    break;
                            }
                        },
                        bottomStatus: function bottomStatus(val) {
                            switch (val) {
                                case 'pull':
                                    this.bottomText = this.bottomPullText;
                                    break;
                                case 'drop':
                                    this.bottomText = this.bottomDropText;
                                    break;
                                case 'loading':
                                    this.bottomText = this.bottomLoadingText;
                                    break;
                            }
                        }
                    },

                    events: {
                        onTopLoaded: function onTopLoaded(id) {
                            var _this = this;

                            if (id === this.uuid) {
                                this.translate = 0;
                                setTimeout(function () {
                                    _this.topStatus = 'pull';
                                }, 200);
                            }
                        },
                        onBottomLoaded: function onBottomLoaded(id) {
                            var _this2 = this;

                            this.bottomStatus = 'pull';
                            this.bottomDropped = false;
                            if (id === this.uuid) {
                                this.$nextTick(function () {
                                    if (_this2.scrollEventTarget === window) {
                                        document.body.scrollTop += 50;
                                    } else {
                                        _this2.scrollEventTarget.scrollTop += 50;
                                    }
                                    _this2.translate = 0;
                                });
                            }
                            if (!this.bottomAllLoaded && !this.containerFilled) {
                                this.fillContainer();
                            }
                        }
                    },

                    methods: {
                        getScrollEventTarget: function getScrollEventTarget(element) {
                            var currentNode = element;
                            while (currentNode && currentNode.tagName !== 'HTML' && currentNode.tagName !== 'BODY' && currentNode.nodeType === 1) {
                                var overflowY = document.defaultView.getComputedStyle(currentNode).overflowY;
                                if (overflowY === 'scroll' || overflowY === 'auto') {
                                    return currentNode;
                                }
                                currentNode = currentNode.parentNode;
                            }
                            return window;
                        },
                        getScrollTop: function getScrollTop(element) {
                            if (element === window) {
                                return Math.max(window.pageYOffset || 0, document.documentElement.scrollTop);
                            } else {
                                return element.scrollTop;
                            }
                        },
                        bindTouchEvents: function bindTouchEvents() {
                            this.$el.addEventListener('touchstart', this.handleTouchStart);
                            this.$el.addEventListener('touchmove', this.handleTouchMove);
                            this.$el.addEventListener('touchend', this.handleTouchEnd);
                        },
                        init: function init() {
                            this.topStatus = 'pull';
                            this.bottomStatus = 'pull';
                            this.topText = this.topPullText;
                            this.scrollEventTarget = this.getScrollEventTarget(this.$el);
                            if (typeof this.bottomMethod === 'function') {
                                this.fillContainer();
                                this.bindTouchEvents();
                            }
                            if (typeof this.topMethod === 'function') {
                                this.bindTouchEvents();
                            }
                        },
                        fillContainer: function fillContainer() {
                            var _this3 = this;

                            if (this.autoFill) {
                                this.$nextTick(function () {
                                    if (_this3.scrollEventTarget === window) {
                                        _this3.containerFilled = _this3.$el.getBoundingClientRect().bottom >= document.documentElement.getBoundingClientRect().bottom;
                                    } else {
                                        _this3.containerFilled = _this3.$el.getBoundingClientRect().bottom >= _this3.scrollEventTarget.getBoundingClientRect().bottom;
                                    }
                                    if (!_this3.containerFilled) {
                                        _this3.bottomStatus = 'loading';
                                        _this3.bottomMethod(_this3.uuid);
                                    }
                                });
                            }
                        },
                        checkBottomReached: function checkBottomReached() {
                            if (this.scrollEventTarget === window) {
                                return document.body.scrollTop + document.documentElement.clientHeight >= document.body.scrollHeight;
                            } else {
                                return this.$el.getBoundingClientRect().bottom <= this.scrollEventTarget.getBoundingClientRect().bottom + 1;
                            }
                        },
                        handleTouchStart: function handleTouchStart(event) {
                            this.startY = event.touches[0].clientY;
                            this.startScrollTop = this.getScrollTop(this.scrollEventTarget);
                            this.bottomReached = false;
                            if (this.topStatus !== 'loading') {
                                this.topStatus = 'pull';
                                this.topDropped = false;
                            }
                            if (this.bottomStatus !== 'loading') {
                                this.bottomStatus = 'pull';
                                this.bottomDropped = false;
                            }
                        },
                        handleTouchMove: function handleTouchMove(event) {
                            if (this.startY < this.$el.getBoundingClientRect().top && this.startY > this.$el.getBoundingClientRect().bottom) {
                                return;
                            }
                            this.currentY = event.touches[0].clientY;
                            var distance = this.currentY - this.startY;
                            this.direction = distance > 0 ? 'down' : 'up';
                            if (typeof this.topMethod === 'function' && this.direction === 'down' && this.getScrollTop(this.scrollEventTarget) === 0 && this.topStatus !== 'loading') {
                                event.preventDefault();
                                event.stopPropagation();
                                this.translate = distance <= this.maxDistance ? distance - this.startScrollTop : this.translate;
                                if (this.translate < 0) {
                                    this.translate = 0;
                                }
                                this.topStatus = this.translate >= this.topDistance ? 'drop' : 'pull';
                            }

                            if (this.direction === 'up') {
                                this.bottomReached = this.bottomReached || this.checkBottomReached();
                            }
                            if (typeof this.bottomMethod === 'function' && this.direction === 'up' && this.bottomReached && this.bottomStatus !== 'loading' && !this.bottomAllLoaded) {
                                event.preventDefault();
                                event.stopPropagation();
                                this.translate = Math.abs(distance) <= this.maxDistance ? this.getScrollTop(this.scrollEventTarget) - this.startScrollTop + distance : this.translate;
                                if (this.translate > 0) {
                                    this.translate = 0;
                                }
                                this.bottomStatus = -this.translate >= this.bottomDistance ? 'drop' : 'pull';
                            }
                        },
                        handleTouchEnd: function handleTouchEnd() {
                            if (this.direction === 'down' && this.getScrollTop(this.scrollEventTarget) === 0 && this.translate > 0) {
                                this.topDropped = true;
                                if (this.topStatus === 'drop') {
                                    this.translate = '50';
                                    this.topStatus = 'loading';
                                    this.topMethod(this.uuid);
                                } else {
                                    this.translate = '0';
                                    this.topStatus = 'pull';
                                }
                            }
                            if (this.direction === 'up' && this.bottomReached && this.translate < 0) {
                                this.bottomDropped = true;
                                this.bottomReached = false;
                                if (this.bottomStatus === 'drop') {
                                    this.translate = '-50';
                                    this.bottomStatus = 'loading';
                                    this.bottomMethod(this.uuid);
                                } else {
                                    this.translate = '0';
                                    this.bottomStatus = 'pull';
                                }
                            }
                            this.direction = '';
                        }
                    },

                    ready: function ready() {
                        this.uuid = Math.random().toString(36).substring(3, 8);
                        this.init();
                    }
                };

                /***/
            },

            /***/ 103:
            /***/ function (module, exports, __webpack_require__) {

                var __vue_script__, __vue_template__
                var __vue_styles__ = {}
                __webpack_require__(104)
                __vue_script__ = __webpack_require__(106)
                if (__vue_script__ &&
                    __vue_script__.__esModule &&
                    Object.keys(__vue_script__).length > 1) {
                    console.warn("[vue-loader] packages/spinner/src/spinner/fading-circle.vue: named exports in *.vue files are ignored.")
                }
                __vue_template__ = __webpack_require__(109)
                module.exports = __vue_script__ || {}
                if (module.exports.__esModule) module.exports = module.exports.default
                var __vue_options__ = typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports
                if (__vue_template__) {
                    __vue_options__.template = __vue_template__
                }
                if (!__vue_options__.computed) __vue_options__.computed = {}
                Object.keys(__vue_styles__).forEach(function (key) {
                    var module = __vue_styles__[key]
                    __vue_options__.computed[key] = function () { return module }
                })


                /***/
            },

            /***/ 104:
            /***/ function (module, exports) {

                // removed by extract-text-webpack-plugin

                /***/
            },

            /***/ 106:
            /***/ function (module, exports, __webpack_require__) {

                'use strict';

                exports.__esModule = true;

                var _common = __webpack_require__(107);

                var _common2 = _interopRequireDefault(_common);

                function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

                exports.default = {
                    name: 'fading-circle',

                    mixins: [_common2.default],

                    created: function created() {
                        this.styleNode = document.createElement('style');
                        var css = '.circle-color-' + this._uid + ' > div::before { background-color: ' + this.spinnerColor + '; }';

                        this.styleNode.type = 'text/css';
                        this.styleNode.rel = 'stylesheet';
                        this.styleNode.title = 'fading circle style';
                        document.getElementsByTagName('head')[0].appendChild(this.styleNode);
                        this.styleNode.appendChild(document.createTextNode(css));
                    },
                    destroyed: function destroyed() {
                        if (this.styleNode) {
                            this.styleNode.parentNode.removeChild(this.styleNode);
                        }
                    }
                };

                /***/
            },

            /***/ 107:
            /***/ function (module, exports, __webpack_require__) {

                var __vue_script__, __vue_template__
                var __vue_styles__ = {}
                __vue_script__ = __webpack_require__(108)
                if (__vue_script__ &&
                    __vue_script__.__esModule &&
                    Object.keys(__vue_script__).length > 1) {
                    console.warn("[vue-loader] packages/spinner/src/spinner/common.vue: named exports in *.vue files are ignored.")
                }
                module.exports = __vue_script__ || {}
                if (module.exports.__esModule) module.exports = module.exports.default
                var __vue_options__ = typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports
                if (__vue_template__) {
                    __vue_options__.template = __vue_template__
                }
                if (!__vue_options__.computed) __vue_options__.computed = {}
                Object.keys(__vue_styles__).forEach(function (key) {
                    var module = __vue_styles__[key]
                    __vue_options__.computed[key] = function () { return module }
                })


                /***/
            },

            /***/ 108:
            /***/ function (module, exports) {

                'use strict';

                exports.__esModule = true;
                exports.default = {
                    computed: {
                        spinnerColor: function spinnerColor() {
                            return this.color || this.$parent.color || '#ccc';
                        },
                        spinnerSize: function spinnerSize() {
                            return (this.size || this.$parent.size || 28) + 'px';
                        }
                    },

                    props: {
                        size: Number,
                        color: String
                    }
                };

                /***/
            },

            /***/ 109:
            /***/ function (module, exports) {

                module.exports = "\n  <div :class=\"['mint-spinner-fading-circle circle-color-' + _uid]\" :style=\"{\n      width: spinnerSize,\n      height: spinnerSize\n    }\">\n    <div v-for=\"n in 12\" :class=\"['is-circle' + (n + 1)]\" class=\"mint-spinner-fading-circle-circle\"></div>\n</template>";

                /***/
            },

            /***/ 110:
            /***/ function (module, exports) {

                module.exports = "\n<div class=\"mint-loadmore\">\n  <div class=\"mint-loadmore-content\" :class=\"{ 'is-dropped': topDropped || bottomDropped}\" :style=\"{ 'transform': 'translate3d(0, ' + translate + 'px, 0)' }\" v-el:loadmore-content>\n    <slot name=\"top\">\n      <div class=\"mint-loadmore-top\" v-if=\"topMethod\">\n        <spinner v-if=\"topStatus === 'loading'\" class=\"mint-loadmore-spinner\" :size=\"20\" type=\"fading-circle\"></spinner>\n        <span class=\"mint-loadmore-text\">{{ topText }}</span>\n      </div>\n    </slot>\n    <slot></slot>\n    <slot name=\"bottom\">\n      <div class=\"mint-loadmore-bottom\" v-if=\"bottomMethod\">\n        <spinner v-if=\"bottomStatus === 'loading'\" class=\"mint-loadmore-spinner\" :size=\"20\" type=\"fading-circle\"></spinner>\n        <span class=\"mint-loadmore-text\">{{ bottomText }}</span>\n      </div>\n    </slot>\n  </div>\n</div>\n";

                /***/
            }

            /******/
        });
    }, {}]
}, {}, [7]);
