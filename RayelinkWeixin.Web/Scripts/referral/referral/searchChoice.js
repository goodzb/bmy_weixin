(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

var SearchTemp = require("./search.js"),
	SearchCom = Vue.extend(SearchTemp);

// 注册全局
Vue.component("search", SearchCom);


},{"./search.js":2}],2:[function(require,module,exports){
"use strict";

module.exports = {
	template: "<div class='mui-search'>\
		<form class='mui-search-form' @submit.prevent='onSubmit'>\
       		<i class='mui-iconfont mui-icon-search'></i>\
        	<input type='search' autofocus='autofocus' class='mui-search-input' placeholder='{{placeholder}}' autocomplete='off' v-model='value' v-el:input>\
        	<a href='javascript:;' v-touch:tap='onClear' class='mui-input-clear' v-show='isShowClear && isHasContent'><i class='mui-iconfont mui-icon-close'></i></a>\
        </form>\
        <a href='javascript:;' class='mui-search-cancel-btn' v-if='onCancel' v-touch:tap='onCancel'><slot>取消</slot></a>\
    </div>", 
    props: {

    	placeholder: {
    		type: String,
    		default: "请输入搜索关键字"
    	},
    	value: {
    		type: String,
    		twoWay: true,
    		default: ""
    	},
    	isShowClear: {
    		type: Boolean,
    		default: true
    	},
    	cancelBtn: {
    		type: Boolean,
    		default: false
    	}
    },
    methods: {
    	onClear: function(e){
    		this.value = "";
    		this.onSetFocus();
    		e.preventDefault();
    	},
    	onSetFocus: function() {
	      	this.$els.input.focus();
	    },
	    onSubmit: function(e){
            
	        this.$emit("on-submit", this.value);
	    	this.$els.input.blur();
	    	e.preventDefault();
	    },
	    onCancel: function (e) {
	        this.$emit("on-cancel");
	    	this.$els.input.blur();
	    	this.value = "";
	    	e.preventDefault();
	    }
    },
    computed: {
    	isHasContent: function(){
    		return !!this.value;
    	},

    }
}
},{}],3:[function(require,module,exports){
/**
 * 
 * @authors yangjie (914569804@qq.com)
 * @date    2016-10-08 17:27:40
 * @version $Id$
 */

require("../common/components/search");

new Vue({
    el: "#searchChoice",
    data: {
        isShow: false,
        key: ''
    },
    methods: {
        init: function () {
            var sk = localStorage.getItem('searchKey');
            if (sk) {
                this.key = sk;
            }

            //document.getElementsByClassName('mui-search-input')[0].focus();

        },
        showInfo: function () {
            this.isShow = !this.isShow;
        },
        onSubmit: function (arg) {
            var str = arg[0];
            if (!str || !(this.trim(str))) return false;

            localStorage.setItem('searchKey', str);
            window.history.go(-1);
        },
        onCancel: function () {
            localStorage.removeItem('searchKey');
            window.history.go(-1);
        },
        trim: function (str) {
            return str.replace(/(^\s*)|(\s*$)/g, "");
        }
    },
    ready: function () {
        this.init();
    }
});
document.getElementsByClassName('mui-search-input')[0].focus();
},{"../common/components/search":1}]},{},[3]);
