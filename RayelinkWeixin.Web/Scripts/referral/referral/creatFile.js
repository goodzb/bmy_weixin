﻿(function e(t, n, r) { function s(o, u) { if (!n[o]) { if (!t[o]) { var a = typeof require == "function" && require; if (!u && a) return a(o, !0); if (i) return i(o, !0); var f = new Error("Cannot find module '" + o + "'"); throw f.code = "MODULE_NOT_FOUND", f } var l = n[o] = { exports: {} }; t[o][0].call(l.exports, function (e) { var n = t[o][1][e]; return s(n ? n : e) }, l, l.exports, e, t, n, r) } return n[o].exports } var i = typeof require == "function" && require; for (var o = 0; o < r.length; o++) s(r[o]); return s })({
    1: [function (require, module, exports) {
        /*
        *	alert组件
        *	和confirm，toast单独区分开来
        */

        "use strict";
        module.exports = {
            template: "<div class='mui-alert' v-show='isComponentShow' v-on:touchmove.prevent>\
        <div class='mui-alert-masker' v-show='isShow' :transition='masker'></div>\
        <div class='mui-alert-inner'>\
            <div class='mui-alert-dialog' v-show='isShow' :transition='dialog'>\
                <div class='mui-alert-dialog-content'>\
                    <h2 class='mui-alert-dialog-title'>{{title}}</h2>\
                    <p class='mui-alert-dialog-msg'>{{msg}}</p>\
                </div>\
                <div class='mui-alert-dialog-buttons'>\
                    <a href='javascript:void(0)' v-touch:tap='onHide' class='mui-alert-dialog-button'>{{button}}</a>\
                </div>\
            </div>\
        </div>\
    </div>",
            data: function () {
                return {
                    //判断是否显示alert，没有和isShow公用一个状态，原因执行leave动画的时候，如果直接通过isShow判断显示隐藏com-alert，动画会不执行。
                    isComponentShow: !0,
                    isShow: !1,
                    masker: "mui-alert-masker",
                    dialog: "mui-alert-dialog"
                }
            }
        }



    }, {}], 2: [function (require, module, exports) {
        /*
        *	alert.show()，里面的参数传递有两种方式，
        *	一种是直接字符串的形式 
        *	alert.show("这是提示");
        *
        *	另一种是对象的形式
        *	alert.show({
        *		msg: "这是提示",
        *		title: "标题",
        *		button: "按钮"
        *	}, function(){ 
        *		callback(); 
        *	});
        */

        "use strict";

        var AlertTemp = require("./alert"),
            AlertComponent = Vue.extend(AlertTemp),
            instance;

        // 初始化实例
        function show(op, cb) {
            var options = {
                title: "提示",
                button: "确定"
            };

            if (typeof op === "object") {
                options.msg = op.msg ? op.msg : "";
                options.title = op.title ? op.title : "提示";
                options.button = op.button ? op.button : "确定";
            } else {
                options.msg = typeof op === "string" || typeof op === "number" ? op : "";
            }

            //是否有回调
            options.cb = cb ? cb : null;


            // 钩子函数要放在实例化之前，否则无法执行
            // Vue.transition("xxx", {
            // 	afterLeave: function(){
            // 		....
            // 	}
            // });

            if (!instance) {
                var com = document.createElement("div");
                document.body.appendChild(com);

                instance = new AlertComponent({
                    el: com,
                    data: function () {
                        return {
                            title: options.title,
                            button: options.button,
                            msg: options.msg,
                            cb: options.cb
                        }
                    },
                    methods: {
                        onHide: function (e) {
                            this.isShow = !1;
                            e.preventDefault();
                        }
                    },
                    // 回调函数要通过数据代理，否则之后不会再进行更新
                    transitions: {
                        "mui-alert-masker": {
                            afterLeave: function () {
                                this.isComponentShow = !1;
                                typeof this.cb === "function" && this.cb();
                            }
                        }
                    }
                });

                instance.isShow = !0;

            } else {
                instance.isComponentShow = !0;
                instance.isShow = !0;
                instance.title = options.title;
                instance.button = options.button;
                instance.msg = options.msg;
                instance.cb = options.cb;
            }

            return instance;
        }

        function hide() {
            instance.isShow = !1;
        }

        // 抛出show 和 hide方法
        module.exports = {
            show: show,
            hide: hide
        }

    }, { "./alert": 1 }], 3: [function (require, module, exports) {
        /*
        *   loading组件，提供两种loading的效果
        *   1.默认，2.circle
        *   通过改变参数theme来设置主题，若是默认，则不需要设置任何主题
        *   可以设置参数noModal是否是模态来，非模态的状态下可以对loading下面的页面功能进行操作
        */

        "use strict";

        var LoadingTemp = require("./loading"),
            LoadingComponent = Vue.extend(LoadingTemp),
            instance;


        // 初始化组件
        function init(op) {
            var container = document.createElement("div");
            document.body.appendChild(container);

            var options = op || {};

            return new LoadingComponent({
                el: container,
                data: function () {
                    return {
                        isLoading: !0,
                        theme: op.theme || "",
                        noModal: op.noModal || !1
                    }
                }
            });
        }


        // 组件show
        function show(op) {
            var options = {};

            options = op ? op : {};

            if (!instance) {
                instance = init(options);
            } else {
                instance.isLoading = true;
                instance.theme = options.theme ? options.theme : "";
                instance.noModal = options.noModal ? options.noModal : !1;
            }

            return instance;
        }


        // 组件hide
        function hide() {
            if (instance) {
                instance.isLoading = false;
            }
        }


        // 抛出方法
        module.exports = {
            show: show,
            hide: hide
        }



    }, { "./loading": 4 }], 4: [function (require, module, exports) {
        /*
        *	loading组件
        *	theme主题，可选，默认是gray
        *	noModal模态，模态状态下不可以操作，非模态状态下可以操作，比如loading的过程中可以点击返回操作等。
        * 	noModal作用不大，先保留吧
        */

        "use strict";

        module.exports = {
            template: "<div class='mui-loading' :class='[theme ? theme: \"\", noModal ? \"mui-loading-nomodal\": \"\"]' v-show='isLoading'>\
		<div class='mui-loading-inner'>\
			<div class='mui-loading-spiner'>\
				<span class='mui-loading-spin'>\
				</span>\
			</div>\
		</div>\
	</div>"
        }
    }, {}], 5: [function (require, module, exports) {
        "use strict";

        var regular = {};

        //  电子邮箱
        regular.isEmail = function (str) {
            var reg = /^\w+([-+.]\w+)*\@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            return reg.test(str);
        };

        //  电话号码，包括手机号+固话
        regular.isPhone = function (str, all) {
            var reg;
            if (all) {
                reg = /(^0{0,1}(13[0-9]|14[0-9]|16[0-9]|15[0-9]|17[0-9]|18[0-9])[0-9]{8})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$/;
            } else {
                reg = /^0{0,1}(13[0-9]|14[0-9]|16[0-9]|15[0-9]|17[0-9]|18[0-9])[0-9]{8}$/;
            }
            return reg.test(str);
        };

        //  身份证号
        regular.isID = function (str) {
            var reg = /^((1[1-5])|(2[1-3])|(3[1-7])|(4[1-6])|(5[0-4])|(6[1-5])|71|(8[12])|91)\d{4}((19\d{2}(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(19\d{2}(0[13578]|1[02])31)|(19\d{2}02(0[1-9]|1\d|2[0-8]))|(19([13579][26]|[2468][048]|0[48])0229))\d{3}(\d|X|x)?$/;
            return reg.test(str);
        };

        // 正整数
        regular.isInteger = function (str) {
            var reg = /^[1-9]\d*$/;
            return reg.test(str);
        };

        module.exports = regular;
    }, {}], 6: [function (require, module, exports) {
        /*
        *	toast用于显示说明性的文字等。
        *	以浮层的形式弹出, 有几种形式
        *   1.纯文本，2.正确带icon，3.错误带icon
        * 	duration 显示时间
        */

        "use strict";

        var ToastTmep = require("./toast"),
            ToastCom = Vue.extend(ToastTmep),
            instance,
            clearTime;


        function show(op) {
            var _options = op ? op : {};

            if (!instance) {

                var com = document.createElement("div");
                document.body.appendChild(com);

                instance = new ToastCom({
                    el: com,
                    data: function () {
                        return {
                            type: _options.type || 1,
                            msg: _options.msg ? _options.msg : ""
                        }
                    },
                    transitions: {
                        "mui-toast-content": {
                            afterLeave: function () {
                                this.isComponentShow = !1;
                            }
                        }
                    }
                });

                instance.isShow = !0;
                instance.duration = _options.duration ? _options.duration : 1500;

            } else {
                instance.isComponentShow = !0;
                instance.isShow = !0;
                instance.type = _options.type ? _options.type : 1;
                instance.msg = _options.msg ? _options.msg : "";
                instance.duration = _options.duration ? _options.duration : 1500
            }

            clearTime && clearTimeout(clearTime);

            clearTime = setTimeout(function () {
                hide();
            }, instance.duration);

            return instance;
        }

        function hide() {
            instance.isShow = !1;
        }


        module.exports = {
            show: show,
            hdie: hide
        }

    }, { "./toast": 7 }], 7: [function (require, module, exports) {
        "use strict";

        module.exports = {
            template: "<div class='mui-toast' :class='[type==2? \"mui-toast-2\": \"\", type==3? \"mui-toast-3\": \"\"]' v-show='isComponentShow'>\
				<div class='mui-toast-inner'>\
					<div class='mui-toast-content' v-show='isShow' :transition='dialog'>\
						<i class='mui-iconfont mui-icon-check' v-if='type == 2'></i>\
						<i class='mui-iconfont mui-icon-warn' v-if='type == 3'></i>\
						<div class='mui-toast-body'>\
							{{msg}}\
						</div>\
					</div>\
				</div>\
			</div>",
            data: function () {
                return {
                    isShow: !1,
                    isComponentShow: !0,
                    duration: 1500,
                    dialog: "mui-toast-content"
                }
            }
        }
    }, {}], 8: [function (require, module, exports) {
        /**
         * 
         * @authors yangjie (914569804@qq.com)
         * @date    2016-10-09 09:54:52
         * @version $Id$
         */
        var Loading = require("../common/components/loading/");
        var Alert = require("../common/components/alert");
        var Toast = require("../common/components/toast/");
        var regular = require("../common/components/regular/");

        var vm = new Vue({
            el: '#NewDocument',
            data: window.model,
            methods: {
                init: function () {

                    var obj = localStorage.getItem('newDocument');
                    if (obj) {
                        this.$data = JSON.parse(obj);
                    }

                    if (this.Sex == 0) {
                        this.Sex = 1;
                    }
                    if (this.Age == 0) {
                        this.Age = '';
                    }
                    this.isShowAdd = true;
                    if (this.Imgs.length == 9) {
                        this.isShowAdd = false;
                    }
                    this.localIdsCopy = [];
                    this.imgPaths = [];//新的所有图片地址，在服务器上相对的地址
                    this.ImgStr = '';
                    this.changeDisable();

                    var nowDate = new Date();
                    var year = nowDate.getFullYear() + "";
                    var month = nowDate.getMonth() + 1 + "";
                    if (month.length == 1) {
                        month = "0" + month;
                    }
                    var day = nowDate.getDate() + "";
                    if (day.length == 1) {
                        day = "0" + day;
                    }
                    this.nowTime = year + month + day;
                },
                chooseHospital: function () {
                    //选择医院
                    localStorage.setItem('newDocument', JSON.stringify(this.$data));
                    window.location.href = "/Referral/choosehospital";
                },
                chooseDoctor: function () {
                    //选择医生
                    localStorage.setItem('newDocument', JSON.stringify(this.$data));
                    localStorage.removeItem('searchKey');
                    window.location.href = "/Referral/choosedoc";
                },
                submitDocu: function () {
                    this.ImgsStr = this.imgPaths.join(';');
                    Vue.http.post('/referral/SaveDocument', this.$data)
                    .then(function (response) {
                        Loading.hide();
                        var res = response.json();
                        if (res == 1) {
                            localStorage.removeItem('newDocument');
                            //Alert.show('转诊需求提交成功!');
                            window.location.href = '/referral/myreferral';
                        } else if (res == 0) {
                            Toast.show({
                                msg: '建档失败'
                            });
                        } else {
                            Toast.show({
                                msg: res
                            });
                        }
                    }.bind(this))
                    .catch(function () {
                        console.log('服务器内部错误！');
                    });
                },
                deleteVal: function (arg) {
                    this.$set(arg, '');
                },
                changeDisable: function () {
                    if (!regular.isInteger(this.Age)) {
                        this.canSubmit = false;
                        return;
                    }
                    if (!regular.isPhone(this.Phone)) {
                        this.canSubmit = false;
                        return;
                    }
                    if (!this.Name || !this.Phone || !regular.isPhone(this.Phone) || this.ClientId == -1 || !this.Age || this.Sex == 0 || !this.DocId) {
                        this.canSubmit = false;
                    } else {
                        this.canSubmit = true;
                    }
                },
                removeImg: function (index) {
                    var img = this.Imgs[index];
                    this.Imgs.splice(index, 1);
                    this.isShowAdd = true;
                },
                chooseImg: function () {//选择图片
                    if (this.Imgs.length >= 9) {
                        Toast.show({
                            msg: '最多只能上传9张！'
                        });
                    }
                    var that = this;
                    wx.chooseImage({
                        count: 9 - this.Imgs.length, // 默认9
                        sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
                        success: function (res) {
                            var Imgs = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
                            that.Imgs = that.Imgs.concat(Imgs);
                            if (that.Imgs.length >= 9) {
                                that.isShowAdd = false;
                            } else {
                                that.isShowAdd = true;
                            }
                        }
                    });
                },
                uploadImgs: function (index) {//上传图片
                    if (this.localIdsCopy.length == 0) {
                        this.submitDocu();
                        return;
                    }
                    var item = this.localIdsCopy[0];
                    //如果是已经下载到服务器的图片就不再上传
                    if (item.indexOf('Upload') >= 0) {
                        this.imgPaths.push(item);
                        this.localIdsCopy.splice(0, 1);
                        this.uploadImgs(); return;
                    }
                    var that = this;
                    wx.uploadImage({
                        localId: item, // 需要上传的图片的本地ID，由chooseImage接口获得
                        isShowProgressTips: 0, // 默认为1，显示进度提示
                        success: function (res) {
                            var serverId = res.serverId; // 返回图片的服务器端ID
                            try {
                                Vue.http.post('/Order/downloadPics', { serverId: serverId, openid: openid })
                                .then(function (response) {
                                    that.imgPaths.push("/UploadFile/" + that.nowTime + "/" + serverId + ".png");
                                    that.localIdsCopy.splice(0, 1);
                                    if (that.localIdsCopy.length > 0) {
                                        that.uploadImgs();
                                    } else {
                                        that.submitDocu();
                                    }
                                }.bind(that))
                                .catch(function () {
                                    Toast.show({
                                        msg: '服务器内部错误！'
                                    });
                                });

                            } catch (e) {
                                alert(e.message);
                            }

                        }
                    });

                },
                checkForm: function () {
                    if (!this.Name) {
                        Toast.show({
                            msg: '请输入患者姓名！'
                        });
                        return false;
                    }
                    if (!(/^[\u4e00-\u9fa5]{2,15}$/).test(this.Name)) {
                        //匹配2到四个汉字
                        Toast.show({
                            msg: '请输入正确的姓名！'
                        });
                        return false;
                    }
                    if (this.Sex==0) {
                        Toast.show({
                            msg: '请选择患者性别！'
                        });
                        return false;
                    }
                    if (!this.Age || this.Age == 0) {
                        Toast.show({
                            msg: '请输入患者年龄！'
                        });
                        return false;
                    }
                    if (!regular.isInteger(this.Age)) {
                        Toast.show({
                            msg: '请输入正确的患者年龄！'
                        });
                        return false;
                    }
                    if (!this.Phone) {
                        Toast.show({
                            msg: '请输入手机号！'
                        });
                        return false;
                    }
                    if (!regular.isPhone(this.Phone)) {
                        Toast.show({
                            msg: '请输入正确的手机号！'
                        });
                        return false;
                    }
                    if (this.HospitalId < 0) {
                        Toast.show({
                            msg: '请选择就诊地点！'
                        });
                        return false;
                    }
                    if (this.DocId <= 0) {
                        Toast.show({
                            msg: '请选择意向专家！'
                        });
                        return false;
                    }

                    return true;
                },
                submit: function () {
                    if (!this.canSubmit) return;
                    if (!this.checkForm()) return;
                    this.localIdsCopy = this.Imgs.slice(0);
                    Loading.show();
                    this.uploadImgs();
                },
                validNumber: function (e) {
                    console.log(e);
                }
            },
            ready: function () {
                this.init();
            }

        });

        vm.$watch('Name', function () {
            this.changeDisable();
        });
        vm.$watch('Phone', function () {
            this.changeDisable();
        });
        vm.$watch('ClientId', function () {
            this.changeDisable();
        });
        vm.$watch('Sex', function () {
            this.changeDisable();
        });
        vm.$watch('Age', function () {
            this.changeDisable();
        });
    }, { "../common/components/alert": 2, "../common/components/loading/": 3, "../common/components/regular/": 5, "../common/components/toast/": 6 }]
}, {}, [8]);
