﻿(function e(t, n, r) { function s(o, u) { if (!n[o]) { if (!t[o]) { var a = typeof require == "function" && require; if (!u && a) return a(o, !0); if (i) return i(o, !0); var f = new Error("Cannot find module '" + o + "'"); throw f.code = "MODULE_NOT_FOUND", f } var l = n[o] = { exports: {} }; t[o][0].call(l.exports, function (e) { var n = t[o][1][e]; return s(n ? n : e) }, l, l.exports, e, t, n, r) } return n[o].exports } var i = typeof require == "function" && require; for (var o = 0; o < r.length; o++) s(r[o]); return s })({
    1: [function (require, module, exports) {
        /*
        *   loading组件，提供两种loading的效果
        *   1.默认，2.circle
        *   通过改变参数theme来设置主题，若是默认，则不需要设置任何主题
        *   可以设置参数noModal是否是模态来，非模态的状态下可以对loading下面的页面功能进行操作
        */

        "use strict";

        var LoadingTemp = require("./loading"),
            LoadingComponent = Vue.extend(LoadingTemp),
            instance;


        // 初始化组件
        function init(op) {
            var container = document.createElement("div");
            document.body.appendChild(container);

            var options = op || {};

            return new LoadingComponent({
                el: container,
                data: function () {
                    return {
                        isLoading: !0,
                        theme: op.theme || "",
                        noModal: op.noModal || !1
                    }
                }
            });
        }


        // 组件show
        function show(op) {
            var options = {};

            options = op ? op : {};

            if (!instance) {
                instance = init(options);
            } else {
                instance.isLoading = true;
                instance.theme = options.theme ? options.theme : "";
                instance.noModal = options.noModal ? options.noModal : !1;
            }

            return instance;
        }


        // 组件hide
        function hide() {
            if (instance) {
                instance.isLoading = false;
            }
        }


        // 抛出方法
        module.exports = {
            show: show,
            hide: hide
        }



    }, { "./loading": 2 }], 2: [function (require, module, exports) {
        /*
        *	loading组件
        *	theme主题，可选，默认是gray
        *	noModal模态，模态状态下不可以操作，非模态状态下可以操作，比如loading的过程中可以点击返回操作等。
        * 	noModal作用不大，先保留吧
        */

        "use strict";

        module.exports = {
            template: "<div class='mui-loading' :class='[theme ? theme: \"\", noModal ? \"mui-loading-nomodal\": \"\"]' v-show='isLoading'>\
		<div class='mui-loading-inner'>\
			<div class='mui-loading-spiner'>\
				<span class='mui-loading-spin'>\
				</span>\
			</div>\
		</div>\
	</div>"
        }
    }, {}], 3: [function (require, module, exports) {
        /*
        *	mixin混合，用于多个item之间切换；可以返回item的长度，当前的索引index，监听切换的事件
        */

        var childMixin = {
            props: {
                selected: {
                    type: Boolean,
                    default: false
                }
            },
            ready: function () {
                this.$parent.updateIndex();
            },
            beforeDestory: function () {
                var $parent = this.$parent;
                this.$nextTick(function () {
                    $parent.updateIndex();
                });
            },
            methods: {
                onItemHandler: function () {
                    this.selected = true;
                    this.$parent.index = this.index;
                    this.$emit("on-item-handler", this.index);
                }
            },
            watch: {
                selected: function (val) {
                    if (val) {
                        this.$parent.index = this.index;
                    }
                }
            },
            data: function () {
                return {
                    index: 0
                }
            }
        }

        module.exports = childMixin;

    }, {}], 4: [function (require, module, exports) {
        /*
        *	mixin混合，用于多个item之间切换；可以返回item的长度，当前的索引index，监听切换的事件
        */

        "use strict";

        var parentMixin = {
            ready: function () {
                this.setDefaultItem(); // 设置默认选中

                this.updateIndex();
            },
            methods: {
                updateIndex: function () {
                    if (!this.$children) {
                        return;
                    }
                    var children = this.$children;
                    this.number = children.length;

                    for (var i = 0; i < children.length; i++) {
                        children[i].index = i;
                        if (children[i].selected) {
                            this.index = i;
                        }
                    }
                },
                setDefaultItem: function () {
                    if (!this.$children) {
                        return;
                    }
                    var children = this.$children;
                    children[0].selected = true;
                }
            },
            props: {
                index: {
                    type: Number,
                    twoWay: true,
                    default: 0
                }
            },
            data: function () {
                return {
                    number: this.$children.length
                }
            },
            watch: {
                index: function (val, oldVal) {
                    if (!this.$children.length) {
                        return;
                    }
                    this.$children[oldVal].selected = false;
                    this.$children[val].selected = true;
                }
            }
        }

        module.exports = parentMixin;


    }, {}], 5: [function (require, module, exports) {
        "use strict";

        var TabTemp = require("./tab.js"),
            TabItemTemp = require("./tab-item.js"),
            TabCom = Vue.extend(TabTemp),
            TabItemCom = Vue.extend(TabItemTemp);

        // 注册全局
        Vue.component("tab", TabCom);
        Vue.component("tab-item", TabItemCom);

    }, { "./tab-item.js": 6, "./tab.js": 7 }], 6: [function (require, module, exports) {
        "use strict";

        var childMixin = require("../mixins/child.js");

        module.exports = {
            template: "<div class='mui-tab-cell' :class='[selected? activeclass: \"\", {\"mui-tab-cell-active\": selected}]' v-touch:tap='onItemHandler'><slot></slot></div>",
            mixins: [childMixin],
            props: {
                activeclass: {
                    type: String,
                    default: ""
                }
            }
        }


    }, { "../mixins/child.js": 3 }], 7: [function (require, module, exports) {
        "use strict";

        var parentMixin = require("../mixins/parent.js");

        module.exports = {
            template: "<div class='mui-tab' :class='{\"mui-tab-no-animation\": !isAnimation, \"mui-tab-radius\": radius}'>\
		<slot></slot>\
		<div class='mui-tab-line' :style='tabLineStyle'></div>\
	</div>",
            mixins: [parentMixin],
            ready: function () {
                var _this = this;
                setTimeout(function () {
                    _this.hasReady = true;
                }, 0);
            },
            props: {
                isAnimation: {
                    type: Boolean,
                    default: true
                },
                lineWidth: {
                    type: Number,
                    default: 3
                },
                lineHeight: {
                    type: Number,
                    default: 2
                },
                activeColor: {
                    type: String,
                    default: "#66cc9a"
                },
                radius: {
                    type: Boolean,
                    default: false
                }
            },
            computed: {
                tabLeft: function () {
                    return "translateX(" + this.index * 100 + "%)";
                },
                tabLineStyle: function () {
                    return {
                        transform: this.tabLeft,
                        backgroundColor: this.activeColor,
                        height: this.lineHeight + "px",
                        width: 100 / this.lineWidth + "%",
                        transition: !this.hasReady ? "none" : null
                    }
                }
            },
            watch: {
                index: function (newIndex, oldIndex) {
                    this.$emit("on-item-change", newIndex, oldIndex);
                }
            },
            data: function () {
                return {
                    hasReady: false
                }
            }
        }


    }, { "../mixins/parent.js": 4 }], 8: [function (require, module, exports) {
        /*
        *	toast用于显示说明性的文字等。
        *	以浮层的形式弹出, 有几种形式
        *   1.纯文本，2.正确带icon，3.错误带icon
        * 	duration 显示时间
        */

        "use strict";

        var ToastTmep = require("./toast"),
            ToastCom = Vue.extend(ToastTmep),
            instance,
            clearTime;


        function show(op) {
            var _options = op ? op : {};

            if (!instance) {

                var com = document.createElement("div");
                document.body.appendChild(com);

                instance = new ToastCom({
                    el: com,
                    data: function () {
                        return {
                            type: _options.type || 1,
                            msg: _options.msg ? _options.msg : ""
                        }
                    },
                    transitions: {
                        "mui-toast-content": {
                            afterLeave: function () {
                                this.isComponentShow = !1;
                            }
                        }
                    }
                });

                instance.isShow = !0;
                instance.duration = _options.duration ? _options.duration : 1500;

            } else {
                instance.isComponentShow = !0;
                instance.isShow = !0;
                instance.type = _options.type ? _options.type : 1;
                instance.msg = _options.msg ? _options.msg : "";
                instance.duration = _options.duration ? _options.duration : 1500
            }

            clearTime && clearTimeout(clearTime);

            clearTime = setTimeout(function () {
                hide();
            }, instance.duration);

            return instance;
        }

        function hide() {
            instance.isShow = !1;
        }


        module.exports = {
            show: show,
            hdie: hide
        }

    }, { "./toast": 9 }], 9: [function (require, module, exports) {
        "use strict";

        module.exports = {
            template: "<div class='mui-toast' :class='[type==2? \"mui-toast-2\": \"\", type==3? \"mui-toast-3\": \"\"]' v-show='isComponentShow'>\
				<div class='mui-toast-inner'>\
					<div class='mui-toast-content' v-show='isShow' :transition='dialog'>\
						<i class='mui-iconfont mui-icon-check' v-if='type == 2'></i>\
						<i class='mui-iconfont mui-icon-warn' v-if='type == 3'></i>\
						<div class='mui-toast-body'>\
							{{msg}}\
						</div>\
					</div>\
				</div>\
			</div>",
            data: function () {
                return {
                    isShow: !1,
                    isComponentShow: !0,
                    duration: 1500,
                    dialog: "mui-toast-content"
                }
            }
        }
    }, {}], 10: [function (require, module, exports) {
        /**
         * 
         * @authors yangjie (914569804@qq.com)
         * @date    2016-09-30 10:53:09
         * @version $Id$
         */
        require("../common/components/tab");
        var Loading = require("../common/components/loading/");
        var Toast = require("../common/components/toast/");

        var Loadmore = require('mint-ui/lib/Loadmore');
        Vue.component('load-more', Loadmore);


        try {
            new Vue({
                el: "#myReferral",
                data: {
                    index: 0,//一级导航索引
                    activeMenuIndex: 0,
                    activeCancelIndex: 0,
                    referral: [[], [], [], [], [], [], ],//转诊记录分类集合，从左到右依次是 待建档/待预约/待就诊/已就诊/已取消/未到诊
                    pageindexs: [0, 0, 0, 0, 0, 0],//各个分类当前分别已加载到第几页
                    status: ['101', '102', '103', '110', '121', '122'],
                    allLoaded: false,
                    unread: [],
                    currentIndex: 0,//当前在referral的哪个子集中加载更多数据，二级导航总索引
                    subIndex: 0,//所选二级导航的索引
                    isNurse: isNurse
                },
                methods: {
                    init: function () {

                        //var referIndex = sessionStorage.getItem('referIndex')
                        //, subindex = sessionStorage.getItem('subIndex');
                        //if (referIndex != null) {
                        //    this.index = parseInt(referIndex);

                        //    subindex = parseInt(subindex);
                        //    if (referIndex == 0) {
                        //        this.activeMenuIndex = subindex;
                        //    } else {
                        //        this.activeCancelIndex = subindex;
                        //    }

                        //}

                        this.loadUnread();
                        this.loadReferral(0);

                    },
                    onItemChange: function () { //一级tab
                        this.pageMath = 1;
                        if (this.index == 0) {
                            // alert(1)
                            this.currentIndex = 0;
                            this.z_treatIng(0);
                        } else if (this.index == 1) {
                            // alert(2)
                            this.currentIndex = 3;
                            this.loadReferral(0);
                        } else if (this.index == 2) {
                            // alert(3)
                            this.currentIndex = 4;
                            this.z_cancel(0);
                        }
                        //this.loadReferral(0);

                    },
                    z_treatIng: function (_indx) {
                        this.currentIndex = _indx;
                        this.activeMenuIndex = _indx;
                        this.loadReferral(0);
                        this.subIndex = _indx;
                    },
                    z_cancel: function (_indx) {
                        this.currentIndex = _indx + 4;
                        this.subIndex = _indx;
                        this.activeCancelIndex = _indx;
                        this.loadReferral(0);
                    },
                    loadReferral: function (type) {
                        Loading.show();
                        //加载数据，type：0表示点击切换时的加载，1表示下拉加载；index：data中referral的索引，表示不同状态的数据
                        if (type == 0 && this.referral[this.currentIndex].length > 0) { Loading.hide(); return; }//点击切换而且当前分类下已经有数据就不加载了
                        Vue.http.post('/referral/GetReferralPage', { pagesize: 20, pageindex: this.pageindexs[this.currentIndex] + 1, status: this.status[this.currentIndex] })
                        .then(function (response) {
                            Loading.hide();
                            var data = response.json();
                            for (var i = 0; i < data.length; i++) {
                                this.referral[this.currentIndex].push(data[i]);
                            }
                            this.pageindexs[this.currentIndex] = this.pageindexs[this.currentIndex] + 1;
                            if (data.length == 0) {
                                if (this.pageindexs[this.currentIndex] > 1) {
                                    Toast.show({
                                        msg: '已经全部加载！'
                                    });
                                }
                                this.pageindexs[this.currentIndex] = this.pageindexs[this.currentIndex] - 1;
                            }



                        }.bind(this))
                        .catch(function () {
                            Loading.hide();
                            Toast.show({
                                msg: '服务器内部错误！'
                            });
                        });
                    },
                    loadBottom: function (id) {
                        var loadIndex = 0;//要加载到哪个分类集合中
                        switch (this.index) {
                            case 0:
                                loadIndex = this.activeMenuIndex;
                                break;
                            case 1:
                                loadIndex = 3;
                                break;
                            case 2:
                                loadIndex = this.activeCancelIndex + 4;
                                break;
                            default:
                                break;
                        }
                        //var pagenumber = this.pageIndexs[loadIndex] + 1;//该分类集合要加载第几页
                        this.loadReferral(1);
                        this.$broadcast('onBottomLoaded', id);

                    },
                    loadUnread: function () {
                        Vue.http.post('/referral/unread')
                        .then(function (response) {
                            var data = response.json();
                            this.unread = data;
                        }.bind(this))
                        .catch(function () {
                            Toast.show({
                                msg: '服务器内部错误！'
                            });
                        });
                    },
                    goNew: function () {
                        //清空之前新建数据
                        var refer = localStorage.getItem('newReferral');
                        if (refer) {
                            refer = JSON.parse(refer);
                            var hName = refer.HospitalName, hId = refer.HospitalId;
                            refer = { 'name': '', 'phone': '', 'desc': '', 'docId': 0, 'isShowAdd': true, 'imgPaths': [], 'localIds': [], 'localIdsCopy': [], 'canSubmit': false };
                            refer.HospitalName = hName;
                            refer.HospitalId = hId;
                            localStorage.setItem('newReferral', JSON.stringify(refer));
                        }
                        //保存当前所在子集
                        localStorage.setItem('referIndex', this.index);
                        localStorage.setItem('currentIndex', this.currentIndex);

                        window.location.href = 'newreferral';
                    },
                    goDetail: function (id) {
                        //保存当前所在子集
                        //sessionStorage.setItem('referIndex', this.index);
                        //sessionStorage.setItem('subIndex', this.index ? this.activeCancelIndex : this.activeMenuIndex);

                        window.location.href = '/referral/referDetail?id=' + id;

                    }
                },
                ready: function () {
                    this.init();
                }
            });

        } catch (e) {
            alert(e.message);
        }

    }, { "../common/components/loading/": 1, "../common/components/tab": 5, "../common/components/toast/": 8, "mint-ui/lib/Loadmore": 11 }], 11: [function (require, module, exports) {
        module.exports =
        /******/ (function (modules) { // webpackBootstrap
            /******/ 	// The module cache
            /******/ 	var installedModules = {};

            /******/ 	// The require function
            /******/ 	function __webpack_require__(moduleId) {

                /******/ 		// Check if module is in cache
                /******/ 		if (installedModules[moduleId])
                    /******/ 			return installedModules[moduleId].exports;

                /******/ 		// Create a new module (and put it into the cache)
                /******/ 		var module = installedModules[moduleId] = {
                    /******/ 			exports: {},
                    /******/ 			id: moduleId,
                    /******/ 			loaded: false
                    /******/
                };

                /******/ 		// Execute the module function
                /******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

                /******/ 		// Flag the module as loaded
                /******/ 		module.loaded = true;

                /******/ 		// Return the exports of the module
                /******/ 		return module.exports;
                /******/
            }


            /******/ 	// expose the modules object (__webpack_modules__)
            /******/ 	__webpack_require__.m = modules;

            /******/ 	// expose the module cache
            /******/ 	__webpack_require__.c = installedModules;

            /******/ 	// __webpack_public_path__
            /******/ 	__webpack_require__.p = "";

            /******/ 	// Load entry module and return exports
            /******/ 	return __webpack_require__(0);
            /******/
        })
        /************************************************************************/
        /******/({

            /***/ 0:
            /***/ function (module, exports, __webpack_require__) {

                module.exports = __webpack_require__(98);


                /***/
            },

            /***/ 98:
            /***/ function (module, exports, __webpack_require__) {

                'use strict';

                module.exports = __webpack_require__(99);

                /***/
            },

            /***/ 99:
            /***/ function (module, exports, __webpack_require__) {

                var __vue_script__, __vue_template__
                var __vue_styles__ = {}
                __webpack_require__(100)
                __vue_script__ = __webpack_require__(102)
                if (__vue_script__ &&
                    __vue_script__.__esModule &&
                    Object.keys(__vue_script__).length > 1) {
                    console.warn("[vue-loader] packages/loadmore/src/loadmore.vue: named exports in *.vue files are ignored.")
                }
                __vue_template__ = __webpack_require__(110)
                module.exports = __vue_script__ || {}
                if (module.exports.__esModule) module.exports = module.exports.default
                var __vue_options__ = typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports
                if (__vue_template__) {
                    __vue_options__.template = __vue_template__
                }
                if (!__vue_options__.computed) __vue_options__.computed = {}
                Object.keys(__vue_styles__).forEach(function (key) {
                    var module = __vue_styles__[key]
                    __vue_options__.computed[key] = function () { return module }
                })


                /***/
            },

            /***/ 100:
            /***/ function (module, exports) {

                // removed by extract-text-webpack-plugin

                /***/
            },

            /***/ 102:
            /***/ function (module, exports, __webpack_require__) {

                'use strict';

                exports.__esModule = true;

                var _fadingCircle = __webpack_require__(103);

                var _fadingCircle2 = _interopRequireDefault(_fadingCircle);

                function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

                exports.default = {
                    name: 'mt-loadmore',
                    components: {
                        'spinner': _fadingCircle2.default
                    },

                    props: {
                        maxDistance: {
                            type: Number,
                            default: 150
                        },
                        autoFill: {
                            type: Boolean,
                            default: true
                        },
                        topPullText: {
                            type: String,
                            default: '下拉刷新'
                        },
                        topDropText: {
                            type: String,
                            default: '释放更新'
                        },
                        topLoadingText: {
                            type: String,
                            default: '加载中...'
                        },
                        topDistance: {
                            type: Number,
                            default: 70
                        },
                        topMethod: {
                            type: Function
                        },
                        topStatus: {
                            type: String,
                            default: ''
                        },
                        bottomPullText: {
                            type: String,
                            default: '上拉刷新'
                        },
                        bottomDropText: {
                            type: String,
                            default: '释放更新'
                        },
                        bottomLoadingText: {
                            type: String,
                            default: '加载中...'
                        },
                        bottomDistance: {
                            type: Number,
                            default: 70
                        },
                        bottomMethod: {
                            type: Function
                        },
                        bottomStatus: {
                            type: String,
                            default: ''
                        },
                        bottomAllLoaded: {
                            type: Boolean,
                            default: false
                        }
                    },

                    data: function data() {
                        return {
                            uuid: null,
                            translate: 0,
                            scrollEventTarget: null,
                            containerFilled: false,
                            topText: '',
                            topDropped: false,
                            bottomText: '',
                            bottomDropped: false,
                            bottomReached: false,
                            direction: '',
                            startY: 0,
                            startScrollTop: 0,
                            currentY: 0
                        };
                    },


                    watch: {
                        topStatus: function topStatus(val) {
                            switch (val) {
                                case 'pull':
                                    this.topText = this.topPullText;
                                    break;
                                case 'drop':
                                    this.topText = this.topDropText;
                                    break;
                                case 'loading':
                                    this.topText = this.topLoadingText;
                                    break;
                            }
                        },
                        bottomStatus: function bottomStatus(val) {
                            switch (val) {
                                case 'pull':
                                    this.bottomText = this.bottomPullText;
                                    break;
                                case 'drop':
                                    this.bottomText = this.bottomDropText;
                                    break;
                                case 'loading':
                                    this.bottomText = this.bottomLoadingText;
                                    break;
                            }
                        }
                    },

                    events: {
                        onTopLoaded: function onTopLoaded(id) {
                            var _this = this;

                            if (id === this.uuid) {
                                this.translate = 0;
                                setTimeout(function () {
                                    _this.topStatus = 'pull';
                                }, 200);
                            }
                        },
                        onBottomLoaded: function onBottomLoaded(id) {
                            var _this2 = this;

                            this.bottomStatus = 'pull';
                            this.bottomDropped = false;
                            if (id === this.uuid) {
                                this.$nextTick(function () {
                                    if (_this2.scrollEventTarget === window) {
                                        document.body.scrollTop += 50;
                                    } else {
                                        _this2.scrollEventTarget.scrollTop += 50;
                                    }
                                    _this2.translate = 0;
                                });
                            }
                            if (!this.bottomAllLoaded && !this.containerFilled) {
                                this.fillContainer();
                            }
                        }
                    },

                    methods: {
                        getScrollEventTarget: function getScrollEventTarget(element) {
                            var currentNode = element;
                            while (currentNode && currentNode.tagName !== 'HTML' && currentNode.tagName !== 'BODY' && currentNode.nodeType === 1) {
                                var overflowY = document.defaultView.getComputedStyle(currentNode).overflowY;
                                if (overflowY === 'scroll' || overflowY === 'auto') {
                                    return currentNode;
                                }
                                currentNode = currentNode.parentNode;
                            }
                            return window;
                        },
                        getScrollTop: function getScrollTop(element) {
                            if (element === window) {
                                return Math.max(window.pageYOffset || 0, document.documentElement.scrollTop);
                            } else {
                                return element.scrollTop;
                            }
                        },
                        bindTouchEvents: function bindTouchEvents() {
                            this.$el.addEventListener('touchstart', this.handleTouchStart);
                            this.$el.addEventListener('touchmove', this.handleTouchMove);
                            this.$el.addEventListener('touchend', this.handleTouchEnd);
                        },
                        init: function init() {
                            this.topStatus = 'pull';
                            this.bottomStatus = 'pull';
                            this.topText = this.topPullText;
                            this.scrollEventTarget = this.getScrollEventTarget(this.$el);
                            if (typeof this.bottomMethod === 'function') {
                                this.fillContainer();
                                this.bindTouchEvents();
                            }
                            if (typeof this.topMethod === 'function') {
                                this.bindTouchEvents();
                            }
                        },
                        fillContainer: function fillContainer() {
                            var _this3 = this;

                            if (this.autoFill) {
                                this.$nextTick(function () {
                                    if (_this3.scrollEventTarget === window) {
                                        _this3.containerFilled = _this3.$el.getBoundingClientRect().bottom >= document.documentElement.getBoundingClientRect().bottom;
                                    } else {
                                        _this3.containerFilled = _this3.$el.getBoundingClientRect().bottom >= _this3.scrollEventTarget.getBoundingClientRect().bottom;
                                    }
                                    if (!_this3.containerFilled) {
                                        _this3.bottomStatus = 'loading';
                                        _this3.bottomMethod(_this3.uuid);
                                    }
                                });
                            }
                        },
                        checkBottomReached: function checkBottomReached() {
                            if (this.scrollEventTarget === window) {
                                return document.body.scrollTop + document.documentElement.clientHeight >= document.body.scrollHeight;
                            } else {
                                return this.$el.getBoundingClientRect().bottom <= this.scrollEventTarget.getBoundingClientRect().bottom + 1;
                            }
                        },
                        handleTouchStart: function handleTouchStart(event) {
                            this.startY = event.touches[0].clientY;
                            this.startScrollTop = this.getScrollTop(this.scrollEventTarget);
                            this.bottomReached = false;
                            if (this.topStatus !== 'loading') {
                                this.topStatus = 'pull';
                                this.topDropped = false;
                            }
                            if (this.bottomStatus !== 'loading') {
                                this.bottomStatus = 'pull';
                                this.bottomDropped = false;
                            }
                        },
                        handleTouchMove: function handleTouchMove(event) {
                            if (this.startY < this.$el.getBoundingClientRect().top && this.startY > this.$el.getBoundingClientRect().bottom) {
                                return;
                            }
                            this.currentY = event.touches[0].clientY;
                            var distance = this.currentY - this.startY;
                            this.direction = distance > 0 ? 'down' : 'up';
                            if (typeof this.topMethod === 'function' && this.direction === 'down' && this.getScrollTop(this.scrollEventTarget) === 0 && this.topStatus !== 'loading') {
                                event.preventDefault();
                                event.stopPropagation();
                                this.translate = distance <= this.maxDistance ? distance - this.startScrollTop : this.translate;
                                if (this.translate < 0) {
                                    this.translate = 0;
                                }
                                this.topStatus = this.translate >= this.topDistance ? 'drop' : 'pull';
                            }

                            if (this.direction === 'up') {
                                this.bottomReached = this.bottomReached || this.checkBottomReached();
                            }
                            if (typeof this.bottomMethod === 'function' && this.direction === 'up' && this.bottomReached && this.bottomStatus !== 'loading' && !this.bottomAllLoaded) {
                                event.preventDefault();
                                event.stopPropagation();
                                this.translate = Math.abs(distance) <= this.maxDistance ? this.getScrollTop(this.scrollEventTarget) - this.startScrollTop + distance : this.translate;
                                if (this.translate > 0) {
                                    this.translate = 0;
                                }
                                this.bottomStatus = -this.translate >= this.bottomDistance ? 'drop' : 'pull';
                            }
                        },
                        handleTouchEnd: function handleTouchEnd() {
                            if (this.direction === 'down' && this.getScrollTop(this.scrollEventTarget) === 0 && this.translate > 0) {
                                this.topDropped = true;
                                if (this.topStatus === 'drop') {
                                    this.translate = '50';
                                    this.topStatus = 'loading';
                                    this.topMethod(this.uuid);
                                } else {
                                    this.translate = '0';
                                    this.topStatus = 'pull';
                                }
                            }
                            if (this.direction === 'up' && this.bottomReached && this.translate < 0) {
                                this.bottomDropped = true;
                                this.bottomReached = false;
                                if (this.bottomStatus === 'drop') {
                                    this.translate = '-50';
                                    this.bottomStatus = 'loading';
                                    this.bottomMethod(this.uuid);
                                } else {
                                    this.translate = '0';
                                    this.bottomStatus = 'pull';
                                }
                            }
                            this.direction = '';
                        }
                    },

                    ready: function ready() {
                        this.uuid = Math.random().toString(36).substring(3, 8);
                        this.init();
                    }
                };

                /***/
            },

            /***/ 103:
            /***/ function (module, exports, __webpack_require__) {

                var __vue_script__, __vue_template__
                var __vue_styles__ = {}
                __webpack_require__(104)
                __vue_script__ = __webpack_require__(106)
                if (__vue_script__ &&
                    __vue_script__.__esModule &&
                    Object.keys(__vue_script__).length > 1) {
                    console.warn("[vue-loader] packages/spinner/src/spinner/fading-circle.vue: named exports in *.vue files are ignored.")
                }
                __vue_template__ = __webpack_require__(109)
                module.exports = __vue_script__ || {}
                if (module.exports.__esModule) module.exports = module.exports.default
                var __vue_options__ = typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports
                if (__vue_template__) {
                    __vue_options__.template = __vue_template__
                }
                if (!__vue_options__.computed) __vue_options__.computed = {}
                Object.keys(__vue_styles__).forEach(function (key) {
                    var module = __vue_styles__[key]
                    __vue_options__.computed[key] = function () { return module }
                })


                /***/
            },

            /***/ 104:
            /***/ function (module, exports) {

                // removed by extract-text-webpack-plugin

                /***/
            },

            /***/ 106:
            /***/ function (module, exports, __webpack_require__) {

                'use strict';

                exports.__esModule = true;

                var _common = __webpack_require__(107);

                var _common2 = _interopRequireDefault(_common);

                function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

                exports.default = {
                    name: 'fading-circle',

                    mixins: [_common2.default],

                    created: function created() {
                        this.styleNode = document.createElement('style');
                        var css = '.circle-color-' + this._uid + ' > div::before { background-color: ' + this.spinnerColor + '; }';

                        this.styleNode.type = 'text/css';
                        this.styleNode.rel = 'stylesheet';
                        this.styleNode.title = 'fading circle style';
                        document.getElementsByTagName('head')[0].appendChild(this.styleNode);
                        this.styleNode.appendChild(document.createTextNode(css));
                    },
                    destroyed: function destroyed() {
                        if (this.styleNode) {
                            this.styleNode.parentNode.removeChild(this.styleNode);
                        }
                    }
                };

                /***/
            },

            /***/ 107:
            /***/ function (module, exports, __webpack_require__) {

                var __vue_script__, __vue_template__
                var __vue_styles__ = {}
                __vue_script__ = __webpack_require__(108)
                if (__vue_script__ &&
                    __vue_script__.__esModule &&
                    Object.keys(__vue_script__).length > 1) {
                    console.warn("[vue-loader] packages/spinner/src/spinner/common.vue: named exports in *.vue files are ignored.")
                }
                module.exports = __vue_script__ || {}
                if (module.exports.__esModule) module.exports = module.exports.default
                var __vue_options__ = typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports
                if (__vue_template__) {
                    __vue_options__.template = __vue_template__
                }
                if (!__vue_options__.computed) __vue_options__.computed = {}
                Object.keys(__vue_styles__).forEach(function (key) {
                    var module = __vue_styles__[key]
                    __vue_options__.computed[key] = function () { return module }
                })


                /***/
            },

            /***/ 108:
            /***/ function (module, exports) {

                'use strict';

                exports.__esModule = true;
                exports.default = {
                    computed: {
                        spinnerColor: function spinnerColor() {
                            return this.color || this.$parent.color || '#ccc';
                        },
                        spinnerSize: function spinnerSize() {
                            return (this.size || this.$parent.size || 28) + 'px';
                        }
                    },

                    props: {
                        size: Number,
                        color: String
                    }
                };

                /***/
            },

            /***/ 109:
            /***/ function (module, exports) {

                module.exports = "\n  <div :class=\"['mint-spinner-fading-circle circle-color-' + _uid]\" :style=\"{\n      width: spinnerSize,\n      height: spinnerSize\n    }\">\n    <div v-for=\"n in 12\" :class=\"['is-circle' + (n + 1)]\" class=\"mint-spinner-fading-circle-circle\"></div>\n</template>";

                /***/
            },

            /***/ 110:
            /***/ function (module, exports) {

                module.exports = "\n<div class=\"mint-loadmore\">\n  <div class=\"mint-loadmore-content\" :class=\"{ 'is-dropped': topDropped || bottomDropped}\" :style=\"{ 'transform': 'translate3d(0, ' + translate + 'px, 0)' }\" v-el:loadmore-content>\n    <slot name=\"top\">\n      <div class=\"mint-loadmore-top\" v-if=\"topMethod\">\n        <spinner v-if=\"topStatus === 'loading'\" class=\"mint-loadmore-spinner\" :size=\"20\" type=\"fading-circle\"></spinner>\n        <span class=\"mint-loadmore-text\">{{ topText }}</span>\n      </div>\n    </slot>\n    <slot></slot>\n    <slot name=\"bottom\">\n      <div class=\"mint-loadmore-bottom\" v-if=\"bottomMethod\">\n        <spinner v-if=\"bottomStatus === 'loading'\" class=\"mint-loadmore-spinner\" :size=\"20\" type=\"fading-circle\"></spinner>\n        <span class=\"mint-loadmore-text\">{{ bottomText }}</span>\n      </div>\n    </slot>\n  </div>\n</div>\n";

                /***/
            }

            /******/
        });
    }, {}]
}, {}, [10]);
