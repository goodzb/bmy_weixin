(function e(t, n, r) { function s(o, u) { if (!n[o]) { if (!t[o]) { var a = typeof require == "function" && require; if (!u && a) return a(o, !0); if (i) return i(o, !0); var f = new Error("Cannot find module '" + o + "'"); throw f.code = "MODULE_NOT_FOUND", f } var l = n[o] = { exports: {} }; t[o][0].call(l.exports, function (e) { var n = t[o][1][e]; return s(n ? n : e) }, l, l.exports, e, t, n, r) } return n[o].exports } var i = typeof require == "function" && require; for (var o = 0; o < r.length; o++) s(r[o]); return s })({
    1: [function (require, module, exports) {
        /*
        *	alert组件
        *	和confirm，toast单独区分开来
        */

        "use strict";
        module.exports = {
            template: "<div class='mui-alert' v-show='isComponentShow' v-on:touchmove.prevent>\
        <div class='mui-alert-masker' v-show='isShow' :transition='masker'></div>\
        <div class='mui-alert-inner'>\
            <div class='mui-alert-dialog' v-show='isShow' :transition='dialog'>\
                <div class='mui-alert-dialog-content'>\
                    <h2 class='mui-alert-dialog-title'>{{title}}</h2>\
                    <p class='mui-alert-dialog-msg'>{{msg}}</p>\
                </div>\
                <div class='mui-alert-dialog-buttons'>\
                    <a href='javascript:void(0)' v-touch:tap='onHide' class='mui-alert-dialog-button'>{{button}}</a>\
                </div>\
            </div>\
        </div>\
    </div>",
            data: function () {
                return {
                    //判断是否显示alert，没有和isShow公用一个状态，原因执行leave动画的时候，如果直接通过isShow判断显示隐藏com-alert，动画会不执行。
                    isComponentShow: !0,
                    isShow: !1,
                    masker: "mui-alert-masker",
                    dialog: "mui-alert-dialog"
                }
            }
        }



    }, {}], 2: [function (require, module, exports) {
        /*
        *	alert.show()，里面的参数传递有两种方式，
        *	一种是直接字符串的形式 
        *	alert.show("这是提示");
        *
        *	另一种是对象的形式
        *	alert.show({
        *		msg: "这是提示",
        *		title: "标题",
        *		button: "按钮"
        *	}, function(){ 
        *		callback(); 
        *	});
        */

        "use strict";

        var AlertTemp = require("./alert"),
            AlertComponent = Vue.extend(AlertTemp),
            instance;

        // 初始化实例
        function show(op, cb) {
            var options = {
                title: "提示",
                button: "确定"
            };

            if (typeof op === "object") {
                options.msg = op.msg ? op.msg : "";
                options.title = op.title ? op.title : "提示";
                options.button = op.button ? op.button : "确定";
            } else {
                options.msg = typeof op === "string" || typeof op === "number" ? op : "";
            }

            //是否有回调
            options.cb = cb ? cb : null;


            // 钩子函数要放在实例化之前，否则无法执行
            // Vue.transition("xxx", {
            // 	afterLeave: function(){
            // 		....
            // 	}
            // });

            if (!instance) {
                var com = document.createElement("div");
                document.body.appendChild(com);

                instance = new AlertComponent({
                    el: com,
                    data: function () {
                        return {
                            title: options.title,
                            button: options.button,
                            msg: options.msg,
                            cb: options.cb
                        }
                    },
                    methods: {
                        onHide: function (e) {
                            this.isShow = !1;
                            e.preventDefault();
                        }
                    },
                    // 回调函数要通过数据代理，否则之后不会再进行更新
                    transitions: {
                        "mui-alert-masker": {
                            afterLeave: function () {
                                this.isComponentShow = !1;
                                typeof this.cb === "function" && this.cb();
                            }
                        }
                    }
                });

                instance.isShow = !0;

            } else {
                instance.isComponentShow = !0;
                instance.isShow = !0;
                instance.title = options.title;
                instance.button = options.button;
                instance.msg = options.msg;
                instance.cb = options.cb;
            }

            return instance;
        }

        function hide() {
            instance.isShow = !1;
        }

        // 抛出show 和 hide方法
        module.exports = {
            show: show,
            hide: hide
        }

    }, { "./alert": 1 }], 3: [function (require, module, exports) {
        /*
        *	confirm 组件
        */

        "use strict";

        module.exports = {
            template: "<div class='mui-confirm' v-show='isComponentShow' v-on:touchmove.prevent>\
        <div class='mui-confirm-masker' v-show='isShow' :transition='masker'></div>\
        <div class='mui-confirm-inner'>\
            <div class='mui-confirm-dialog' v-show='isShow' :transition='dialog'>\
                <div class='mui-confirm-dialog-content'>\
                    <p class='mui-confirm-dialog-title'>{{title}}<i class='byebye' v-touch:tap='onCallbackOne'></i></p>\
                    <p class='mui-confirm-dialog-msg'>{{msg}}</p>\
                </div>\
                <div class='mui-confirm-dialog-buttons'>\
                    <a href='javascript:void(0)' v-touch:tap='onCallbackTwo' class='bangding mui-confirm-dialog-button'>{{buttons[1]}}</a>\
                    <a href='javascript:void(0)' v-touch:tap='onCallbackOne' class='bangdingno mui-confirm-dialog-button'>{{buttons[0]}}</a>\
                </div>\
            </div>\
        </div>\
    </div>",
            data: function () {
                return {
                    isComponentShow: !0,
                    isShow: !1,
                    buttonIndex: 0, //按钮索引1开始，从左到右1，2，...
                    masker: "mui-confirm-masker",
                    dialog: "mui-confirm-dialog"
                }
            }
        }
    }, {}], 4: [function (require, module, exports) {
        "use strict";

        var ConfirmTemp = require("./confirm"),
            ConfirmComponent = Vue.extend(ConfirmTemp),
            instance;

        // 初始化实例
        function show(op, cb) {
            var options = {
                title: "提示",
                buttons: ["取消", "确定"]
            };

            options.msg = op.msg ? op.msg : "";
            options.title = op.title ? op.title : "提示";
            options.buttons = op.buttons ? op.buttons : ["取消", "确定"];

            options.cb = cb ? cb : null;

            if (!instance) {
                var com = document.createElement("div");
                document.body.appendChild(com);

                instance = new ConfirmComponent({
                    el: com,
                    data: function () {
                        return {
                            title: options.title,
                            buttons: options.buttons,
                            msg: options.msg,
                            cb: options.cb
                        }
                    },
                    methods: {
                        onCallbackOne: function (e) {
                            this.isShow = !1;
                            this.buttonIndex = 1;
                            e.preventDefault();
                        },
                        onCallbackTwo: function (e) {
                            this.isShow = !1;
                            this.buttonIndex = 2;
                            e.preventDefault();
                        }
                    },
                    transitions: {
                        "mui-confirm-dialog": {
                            afterLeave: function () {
                                this.isComponentShow = !1;
                                typeof this.cb === "function" && this.cb({
                                    buttonIndex: this.buttonIndex
                                });
                            }
                        }
                    }
                });

                instance.isShow = !0;

            } else {
                instance.isComponentShow = !0;
                instance.isShow = !0;
                instance.title = options.title;
                instance.buttons = options.buttons;
                instance.msg = options.msg;
                instance.cb = options.cb;
            }

            return instance;
        }

        function hide() {
            instance.isShow = !1;
        }

        // 抛出show 和 hide方法
        module.exports = {
            show: show,
            hide: hide
        }

    }, { "./confirm": 3 }], 5: [function (require, module, exports) {
        /*
        *   loading组件，提供两种loading的效果
        *   1.默认，2.circle
        *   通过改变参数theme来设置主题，若是默认，则不需要设置任何主题
        *   可以设置参数noModal是否是模态来，非模态的状态下可以对loading下面的页面功能进行操作
        */

        "use strict";

        var LoadingTemp = require("./loading"),
            LoadingComponent = Vue.extend(LoadingTemp),
            instance;


        // 初始化组件
        function init(op) {
            var container = document.createElement("div");
            document.body.appendChild(container);

            var options = op || {};

            return new LoadingComponent({
                el: container,
                data: function () {
                    return {
                        isLoading: !0,
                        theme: op.theme || "",
                        noModal: op.noModal || !1
                    }
                }
            });
        }


        // 组件show
        function show(op) {
            var options = {};

            options = op ? op : {};

            if (!instance) {
                instance = init(options);
            } else {
                instance.isLoading = true;
                instance.theme = options.theme ? options.theme : "";
                instance.noModal = options.noModal ? options.noModal : !1;
            }

            return instance;
        }


        // 组件hide
        function hide() {
            if (instance) {
                instance.isLoading = false;
            }
        }


        // 抛出方法
        module.exports = {
            show: show,
            hide: hide
        }



    }, { "./loading": 6 }], 6: [function (require, module, exports) {
        /*
        *	loading组件
        *	theme主题，可选，默认是gray
        *	noModal模态，模态状态下不可以操作，非模态状态下可以操作，比如loading的过程中可以点击返回操作等。
        * 	noModal作用不大，先保留吧
        */

        "use strict";

        module.exports = {
            template: "<div class='mui-loading' :class='[theme ? theme: \"\", noModal ? \"mui-loading-nomodal\": \"\"]' v-show='isLoading'>\
		<div class='mui-loading-inner'>\
			<div class='mui-loading-spiner'>\
				<span class='mui-loading-spin'>\
				</span>\
			</div>\
		</div>\
	</div>"
        }
    }, {}], 7: [function (require, module, exports) {
        /*
        *	toast用于显示说明性的文字等。
        *	以浮层的形式弹出, 有几种形式
        *   1.纯文本，2.正确带icon，3.错误带icon
        * 	duration 显示时间
        */

        "use strict";

        var ToastTmep = require("./toast"),
            ToastCom = Vue.extend(ToastTmep),
            instance,
            clearTime;


        function show(op) {
            var _options = op ? op : {};

            if (!instance) {

                var com = document.createElement("div");
                document.body.appendChild(com);

                instance = new ToastCom({
                    el: com,
                    data: function () {
                        return {
                            type: _options.type || 1,
                            msg: _options.msg ? _options.msg : ""
                        }
                    },
                    transitions: {
                        "mui-toast-content": {
                            afterLeave: function () {
                                this.isComponentShow = !1;
                            }
                        }
                    }
                });

                instance.isShow = !0;
                instance.duration = _options.duration ? _options.duration : 1500;

            } else {
                instance.isComponentShow = !0;
                instance.isShow = !0;
                instance.type = _options.type ? _options.type : 1;
                instance.msg = _options.msg ? _options.msg : "";
                instance.duration = _options.duration ? _options.duration : 1500
            }

            clearTime && clearTimeout(clearTime);

            clearTime = setTimeout(function () {
                hide();
            }, instance.duration);

            return instance;
        }

        function hide() {
            instance.isShow = !1;
        }


        module.exports = {
            show: show,
            hdie: hide
        }

    }, { "./toast": 8 }], 8: [function (require, module, exports) {
        "use strict";

        module.exports = {
            template: "<div class='mui-toast' :class='[type==2? \"mui-toast-2\": \"\", type==3? \"mui-toast-3\": \"\"]' v-show='isComponentShow'>\
				<div class='mui-toast-inner'>\
					<div class='mui-toast-content' v-show='isShow' :transition='dialog'>\
						<i class='mui-iconfont mui-icon-check' v-if='type == 2'></i>\
						<i class='mui-iconfont mui-icon-warn' v-if='type == 3'></i>\
						<div class='mui-toast-body'>\
							{{msg}}\
						</div>\
					</div>\
				</div>\
			</div>",
            data: function () {
                return {
                    isShow: !1,
                    isComponentShow: !0,
                    duration: 1500,
                    dialog: "mui-toast-content"
                }
            }
        }
    }, {}], 9: [function (require, module, exports) {
        /**
         * 
         * @authors yangjie (914569804@qq.com)
         * @date    2016-10-09 15:27:46
         * @version $Id$
         */
        var Loading = require("../common/components/loading/");
        var Alert = require("../common/components/alert");
        var Toast = require("../common/components/toast/");
        var Confirm = require("../common/components/confirm/");

        new Vue({
            el: "#referDetail",
            data: {
                isExpand: false,
                showPath:''
            },
            methods: {
                init: function () {

                    // Alert.show({
                    // 	msg:"确定跳转",
                    // button: "确定"
                    // },function(ret){
                    // 	//回调函数
                    // })

                },
                delRefer: function (id) {
                    Confirm.show({
                        title: '温馨提示',
                        msg: '取消操作不可逆，是否确定取消该转诊单？',
                        buttons: ['否', '是']
                    }, function (res) {
                        if (res.buttonIndex == 2) {
                            Vue.http.post('/referral/cancel', { id: id })
                            .then(function (response) {
                                if (response.json() == '1') {
                                    window.location.href = '/referral/myreferral';
                                };
                            }.bind(this))
                            .catch(function () {
                                Toast.show({
                                    msg: '服务器内部错误！'
                                });

                            });

                        }
                    });

                },
                goNewDocument: function (id) {
                    localStorage.removeItem('newDocument');
                    window.location.href = '/referral/newdocument?id=' + id;
                },
                expand: function (p) {
                    this.showPath = p;
                    this.isExpand = true;

                },
                closeExpand: function () {
                    this.isExpand = false;
                }
            },
            ready: function () {
                this.init();
            }
        })
    }, { "../common/components/alert": 2, "../common/components/confirm/": 4, "../common/components/loading/": 5, "../common/components/toast/": 7 }]
}, {}, [9]);
