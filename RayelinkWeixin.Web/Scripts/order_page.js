$(function(){
   $('.share_file').each(function(){
        var This = $(this),
            share_x = This.find('.share_x'),
            add_file = This.find('.no_file');
       share_x.bind('touchstart touchend',function(){
            This.remove();
       })
       add_file.bind('click',function(){
            $('.open_photo').toggleClass('shadow');
       })
   })
   $('.choose_photo_list').each(function(){
        var This = $(this),
            li = This.find('li'),
            close_btn = This.find('.byebye');
       
       li.bind('touchstart touchend',function(){
            $(this).toggleClass('active_style');
       })
       close_btn.click(function(){
            This.parent('.open_photo').addClass('shadow');
       })
   })
   
   $('.delete_icon').bind("click touchend", function (e) {
        $(this).prev("input").val("");
   })
})