﻿var InitLayoutShareInfo = function (jssdkOptions, openId, appid, shareOption, success, cancel, shareCallBack) {
    ///<summary>初始化页面分享信息(从页面自定义配置信息中获取)</summary>
    ///<param name="jssdkOptions" type="Object">页面JSJDK对象</param>
    ///<param name="openId" type="String">当前微信用户标识</param>
    ///<param name="appid" type="Int">当前微信AppId</param>
    ///<param name="shareOption" type="Object">当前分享信息(imgUrl,link,title,desc)</param>
    ///<param name="success" type="Function">用户确认分享后执行的回调函数</param>
    ///<param name="cancel" type="Function">用户取消分享后执行的回调函数</param>
    ///<param name="shareCallBack" type="Function">当WeiXinJSSD初始化成功后的回调函数</param>
    ///<returns>无返回值</returns>

    var shareInfo = { imgUrl: shareOption["imgUrl"] || "", link: shareOption["link"] || document.URL, title: shareOption["title"] || "", desc: shareOption["desc"] || "" };

    if (shareInfo.desc && shareInfo.desc.length > 100) {
        shareInfo.desc = shareInfo.desc.substring(0, 100);
    }
    shareInfo.link = shareOption["link"] + "&from=timeline&sourceOpenId=" + openId + "&_sourceuid_=" + openId;
    if (!/\(?is:\)appid=/.test(shareInfo.link)) {
        shareInfo.link += "&appid=" + appid;
    }
    // confirm(shareInfo.link );
    WeChatShare.DefaultSetting.WeChatShareOption.Default.title = shareInfo.title; // 分享标题
    WeChatShare.DefaultSetting.WeChatShareOption.Default.desc = shareInfo.desc; // 分享描述
    WeChatShare.DefaultSetting.WeChatShareOption.Default.link = shareInfo.link; // 分享链接
    WeChatShare.DefaultSetting.WeChatShareOption.Default.imgUrl = shareInfo.imgUrl; // 分享图标

    if (success) {
        WeChatShare.DefaultSetting.WeChatShareOption.Default.success = success; //用户确认分享后执行的回调函数
    }

    if (cancel) {
        WeChatShare.DefaultSetting.WeChatShareOption.Default.cancel = cancel; // 用户取消分享后执行的回调函数
    }

    //console.log(jssdkOptions);
    new WeChatShare.WeChatShare({
        appId: jssdkOptions.appId, //appkey
        timestamp: jssdkOptions.timestamp,
        nonceStr: jssdkOptions.nonceStr,
        signature: jssdkOptions.signature,       
        debug: shareOption["debug"] || false,
        callback: shareCallBack
    });
};

var InitLayoutHideShare = function (jssdkOptions, debug, callBack) {
    ///<summary>隐藏页面的分享菜单(此方法主要针对于页面不需要显示分享菜单时使用)</summary>
    ///<param name="jssdkOptions" type="Object">页面JSJDK对象</param>
    ///<param name="debug" type="Bool">是否开启调试模式</param>
    ///<param name="callBack" type="Function">配置完成回调函数</param>
    ///<returns>无返回值</returns>

    if (null != jssdkOptions) {
        new WeChatShare.WeChatShare({
            appId: jssdkOptions.appId, //appkey
            timestamp: jssdkOptions.timestamp,
            nonceStr: jssdkOptions.nonceStr,
            signature: jssdkOptions.signature,  
            debug: debug || false,
            callback: function (ret) {
                window.wx_ret = ret;
                ret.hideOptionMenu();
                if (callBack) {
                    callBack();
                }
            }
        });
    }
};