﻿/*****************************************************  Sweet  *************************************************/
// http://t4t5.github.io/sweetalert/
var SwalError = function (title, callback) {
    swal({
        title: title || "操作提示",
        //text: "You will not be able to recover this imaginary file!",
        type: "error",
        //showCancelButton: true,
        //confirmButtonColor: "#DD6B55",
        confirmButtonText: "确定",
        closeOnConfirm: false
    }, function () {
        if (callback) callback();
        swal.close();
    });
}

var SweetAlert = function (title, callback) {
    swal({
        title: title || "操作提示",
        timer: 1000,
        showConfirmButton: false
    }, function () {
        if (callback) callback();
        swal.close();
    });
}


var SweetConfirm = function (text, type, callback) {
    swal({
        title: "Are you sure?",
        text: text,
        type: type || "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sure",
        cancelButtonText: "Cancel",
        closeOnConfirm: false
    }, function () {
        if (callback) callback();
        swal.close();
    });
}
/***********************************************   toastr  **********************************************************************/
/*
https://github.com/CodeSeven/toastr
http://www.cnblogs.com/wuhuacong/p/4775282.html
http://codeseven.github.io/toastr/demo.html
*/

////显示一个警告,没有标题
//toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!')

////显示一个成功,标题
//toastr.success('Have fun storming the castle!', 'Miracle Max Says')

////显示错误标题
//toastr.error('I do not think that word means what you think it means.', 'Inconceivable!')

////清除当前的列表
//toastr.clear()

//参数设置，若用默认值可以省略以下面代
//toastr.options = {
//    "closeButton": false, //是否显示关闭按钮
//    "debug": false, //是否使用debug模式
//    "positionClass": "toast-top-full-width",//弹出窗的位置
//    "showDuration": "300",//显示的动画时间
//    "hideDuration": "1000",//消失的动画时间
//    "timeOut": "5000", //展现时间
//    "extendedTimeOut": "1000",//加长展示时间
//    "showEasing": "swing",//显示时的动画缓冲方式
//    "hideEasing": "linear",//消失时的动画缓冲方式
//    "showMethod": "fadeIn",//显示时的动画方式
//    "hideMethod": "fadeOut" //消失时的动画方式
//};
var ToastrSuccess = function (message, title, hiddenCallback, options) {
    if (!options) {
        options = {};
    }
    //参数设置，若用默认值可以省略以下面代
    toastr.options = {
        "closeButton": options["closeButton"] || false, //是否显示关闭按钮
        "debug": options["debug"] || false, //是否使用debug模式
        "positionClass": options["positionClass"] || "toast-top-center",//弹出窗的位置
        "showDuration": options["showDuration"] || "300",//显示的动画时间
        "hideDuration": options["hideDuration"] || "300",//消失的动画时间
        "timeOut": options["timeOut"] || "600", //展现时间
        "extendedTimeOut": options["extendedTimeOut"] || "100",//加长展示时间
        "showEasing": options["showEasing"] || "swing",//显示时的动画缓冲方式
        "hideEasing": options["hideEasing"] || "linear",//消失时的动画缓冲方式
        "showMethod": options["showMethod"] || "fadeIn",//显示时的动画方式
        "hideMethod": options["hideMethod"] || "fadeOut", //消失时的动画方式
        onShown: options["onShown"] || function () { console.log('hello'); },
        onHidden: options["onHidden"] || function () {
            if (hiddenCallback) hiddenCallback();
            console.log('goodbye');
        },
        onclick: options["onclick"] || function () { console.log('clicked'); },
        onCloseClick: options["onCloseClick"] || function () { console.log('close button clicked'); }
    };
    toastr.success(message, title || "");
}




/************************************************************  bootboxjs  ******************************************************************/
//http://bootboxjs.com/documentation.html