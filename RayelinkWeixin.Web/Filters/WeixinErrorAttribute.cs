﻿using RayelinkWeixin.Common.Utilites;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Filters
{
    /// <summary>
    /// 
    /// </summary>
    public class WeixinErrorAttribute : HandleErrorAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnException(ExceptionContext filterContext)
        {
            var ex = filterContext.Exception;
            LogHelper.Error(ex.Message, ex);
            filterContext.ExceptionHandled = true;
            base.OnException(filterContext);
        }
    }
}