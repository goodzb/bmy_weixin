﻿using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Models;
using Senparc.Weixin.MP.Containers;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Filters
{
    /// <summary>
    /// 取消授权： 微信权限扩展 除ajax方法以外都必须标记
    /// 这个的功能是在页面html后面加载openid
    /// 并且在渲染的页面的时候在页面注入一个隐藏域 保存openid
    /// <input id="hidStorageOpenID" type="hidden" value="b2VTX3V0MHdEcUFRNFI0SVg2VUhNYjlOZ3JlZw==">
    /// </summary>
    public class WeixinSpeficAuthorizeAttribute : ActionFilterAttribute
    {
        /// <summary>
        ///  
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = filterContext.HttpContext.User;
            var request = filterContext.HttpContext.Request;
            var url = request.Url.ToString();
            var port = WeixinConfig.port;
            if (!string.IsNullOrWhiteSpace(port))
            {
                url = url.Replace(":" + port, "");
            }
            if (user == null || user.Identity == null || !user.Identity.IsAuthenticated)
            {
                var obj = filterContext.HttpContext.Request.Params["openid"];
                //LogHelper.Info("obj=" + obj);
                if (!string.IsNullOrWhiteSpace(obj))
                {
                    if (filterContext.HttpContext.Request.Url.Host == "localhost")  //本机且含有openid时略过
                    {
                        return;
                    }

                    //如果用户头像和昵称为空，则获取一下
                    try
                    {
                        if (AuthorizeUser.headimgurl() == "" || AuthorizeUser.nickname() == "")
                        {
                            obj = Base64Helper.DecodeBase64(obj);
                            var _appId = WeixinConfig.appID;
                            var _appSecret = WeixinConfig.appSecret;
                            var accessToken = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret, false);
                            if (!string.IsNullOrEmpty(accessToken))
                            {
                                var userinfo = Senparc.Weixin.MP.AdvancedAPIs.UserApi.Info(accessToken, obj);
                                filterContext.HttpContext.Session[KeyConst.SessionUserHeadImageUrl] = userinfo.headimgurl;
                                filterContext.HttpContext.Session[KeyConst.SessionUserNickName] = userinfo.nickname;
                            }
                        }
                        else
                        {
                            obj = Base64Helper.DecodeBase64(obj);
                            //LogHelper.Info("obj_d=" + obj);
                            CRMWebService.ServiceForWebSoapClient client = new CRMWebService.ServiceForWebSoapClient();
                            var jsonStr = client.CRM_WEB_LoginByPartner(obj, WeixinConfig.crmPartner);
                            var model = JsonConvert.DeserializeObject<CRMLoginModel>(jsonStr);
                            if (model != null && model.result == "0")
                            {
                                AuthorizeUser.CreateLoginUserTicket(obj, obj);
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        LogHelper.Exception(ex);
                        // throw;
                    }
                }
                else
                {
                    var isFirst = request.QueryString["IsFirstAccess"];
                    if (!request.IsAjaxRequest())
                    {
                        if (string.IsNullOrWhiteSpace(isFirst))
                        {
                            if (url.IndexOf("?") > 0)
                            {
                                url += "&IsFirstAccess=false";
                            }
                            else
                            {
                                url += "?IsFirstAccess=false";
                            }
                            //LogHelper.Info("传的什么URL: " + url);
                            //LogHelper.Info("给ValidWeixinLogin传了reutrnUrl base64: " + Base64Helper.EncodeBase64(url));
                            filterContext.Result = new RedirectResult("~/OAuth/ValidWeixinLogin?id=" + Base64Helper.EncodeBase64(url) + "&type=0");
                            //filterContext.Result = new RedirectToRouteResult("Default", new RouteValueDictionary(new { controller = "OAuth", action = "ValidWeixinLogin", id = Base64Helper.EncodeBase64(url) })); 
                        }
                    }
                }
            }
            else
            {
                if (AuthorizeUser.headimgurl() == "" || AuthorizeUser.nickname() == "")
                {
                    var _appId = WeixinConfig.appID;
                    var _appSecret = WeixinConfig.appSecret;
                    var accessToken = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret, false);
                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        var userinfo = Senparc.Weixin.MP.AdvancedAPIs.UserApi.Info(accessToken, user.Identity.Name);
                        filterContext.HttpContext.Session[KeyConst.SessionUserHeadImageUrl] = userinfo.headimgurl;
                        filterContext.HttpContext.Session[KeyConst.SessionUserNickName] = userinfo.nickname;
                    }
                }
                else
                {
                    if (!request.IsAjaxRequest())
                    {
                        if (string.IsNullOrWhiteSpace(request.Params["openid"]))
                        {
                            //参数为空的情况下
                            var openid = Base64Helper.EncodeBase64(user.Identity.Name);
                            if (url.IndexOf("?") > 0)
                            {
                                url += "&openid=" + openid;
                            }
                            else
                            {
                                url += "?openid=" + openid;
                            }
                            filterContext.Result = new RedirectResult(url);
                        }
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// 重载次方法在每个get请求的页面 在页面请求上面每次保存一个openid的隐藏域
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            if (!request.IsAjaxRequest())
            {
                var obj = request.Params["openid"];
                filterContext.HttpContext.Response.Write(string.Format(@"<input id=""hidStorageOpenID"" type=""hidden"" value=""{0}"" />", obj));
            }
            base.OnResultExecuting(filterContext);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }
    }
}