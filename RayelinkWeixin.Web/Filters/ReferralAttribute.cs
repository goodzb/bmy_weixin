﻿using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Web.Controllers;
using RayelinkWeixin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Filters
{
    public class ReferralAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                var sessionUser = filterContext.HttpContext.Session["user"];
                var request = filterContext.HttpContext.Request;
                var url = request.Url.ToString();
                var openid = request["openid"];
                var port = WeixinConfig.port;
                string actionName = filterContext.ActionDescriptor.ActionName.ToLower();
                filterContext.HttpContext.Session["jsModel"] = JsSDKModel.GetJsSDKModel(url);

                if (!string.IsNullOrWhiteSpace(port))
                {
                    url = url.Replace(":" + port, "");
                }

                if (!string.IsNullOrWhiteSpace(openid) && actionName != "bind")
                {
                    if (openid.Length >= 32 && (openid.Length % 4) == 0)
                    {
                        openid = Base64Helper.DecodeBase64(openid);
                    }
                    //参数里面有openid,而且请求页面不是绑定，就是刚刚获取完openid跳回来的
                    if (sessionUser == null)
                    {
                        var res = HttpHelper.PostReq(WeixinConfig.userApi + "/APIAccount/GetUserInfo", "{'ThirdPartyID':'" + openid + "'}");
                        LogHelper.Debug("openid:"+openid+"，返回用户信息："+res);
                        ResModel resModel = JsonConvert.DeserializeObject<ResModel>(res);
                        if (resModel.Success && resModel.User != null)
                        {
                            RefUserModel user = new RefUserModel();
                            //获取到用户信息
                            user.Id = resModel.User.Id;
                            user.OpenId = resModel.User.ThirdPartyID;
                            user.Phone = resModel.User.Phone;
                            user.Type = resModel.User.UserType;
                            user.ClientId = resModel.User.UserInfo.HospitalId;
                            user.Name = resModel.User.UserInfo.DoctorName;
                            filterContext.HttpContext.Session["user"] = user;
                        }
                        else
                        {
                            //没获取到信息
                            filterContext.Result = new RedirectResult("/referral/bind?openid=" + openid);
                        }
                    }
                }
                else
                {
                    if (sessionUser == null && actionName!="bind")
                    {
                        //参数里没有openid，而且Session中也没有存储用户信息，跳转获取
                        var isFirst = request.QueryString["IsFirstAccess"];
                        if (!request.IsAjaxRequest())
                        {
                            if (string.IsNullOrWhiteSpace(isFirst))
                            {
                                if (url.IndexOf("?") > 0)
                                {
                                    url += "&IsFirstAccess=false";
                                }
                                else
                                {
                                    url += "?IsFirstAccess=false";
                                }

                                filterContext.Result = new RedirectResult("/OAuth/ValidWeixinLogin?type=1&id=" + Base64Helper.EncodeBase64(url));
                            }
                        }
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }

    }
}