﻿using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Models;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Filters
{

    /// <summary>
    /// 继承AuthorizeAttribute 使用asp.net的网站验证机制
    /// 使用cookie 和 session 同时保存openid 信息
    /// </summary>
    public class WebAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            var user = filterContext.HttpContext.User;
            if (user == null || user.Identity == null || !user.Identity.IsAuthenticated)
            {
                var obj = request.Params["openid"];
                //LogHelper.Info("yuan_OpenID:" + obj);
                if (!string.IsNullOrWhiteSpace(obj))
                {
                    if (obj.Length >= 32 && (obj.Length % 4) == 0)
                    {
                        obj = Base64Helper.DecodeBase64(obj);
                    }
                    //LogHelper.Info(user.Identity.Name + " OpenID:" + obj);
                    //还要判断是否登陆了
                    CRMWebService.ServiceForWebSoapClient client = new CRMWebService.ServiceForWebSoapClient();
                    var jsonStr = client.CRM_WEB_LoginByPartner(obj, WeixinConfig.crmPartner);
                    var model = JsonConvert.DeserializeObject<CRMLoginModel>(jsonStr);
                    if (model != null && model.result == "0")
                    {
                        AuthorizeUser.CreateLoginUserTicket(obj, obj);
                    }
                    else
                    {
                        base.OnAuthorization(filterContext);
                    }
                }
                else
                {
                    base.OnAuthorization(filterContext);
                }
            }
            else
            {
                var obj = request.Params["openid"];
                //LogHelper.Info("yuan_OpenID2:" + obj);
                if (!string.IsNullOrWhiteSpace(obj))
                {
                    if (obj.Length >= 32 && (obj.Length % 4) == 0)
                    {
                        obj = Base64Helper.DecodeBase64(obj);
                    }
                    //还要判断是否登陆了
                    CRMWebService.ServiceForWebSoapClient client = new CRMWebService.ServiceForWebSoapClient();
                    var jsonStr = client.CRM_WEB_LoginByPartner(obj, WeixinConfig.crmPartner);
                    var model = JsonConvert.DeserializeObject<CRMLoginModel>(jsonStr);
                    if (model != null && model.result == "0")
                    {
                        AuthorizeUser.CreateLoginUserTicket(obj, obj);
                    }
                    else
                    {
                        base.OnAuthorization(filterContext);
                    }
                }
                else
                {
                    base.OnAuthorization(filterContext);
                }
            }
        }


    }
}