﻿using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using System.Web;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Filters
{
    /// <summary>
    /// 微信授权(本地Attribute使用)
    /// </summary>
    public class WxOAuthAuthorizeAttribute : ActionFilterAttribute
    {
        string _type = "base";
        string _source = "";
        int _appid = 1;
        string _redirect_uri = "";
        string _state = "";

        /// <summary>
        /// 
        /// <param name="type">base/normal/super,授权类型。base：隐式授权，normal：显示授权，页面要弹窗进行提示授权操作,super:显示授权并返回用户信息，页面要弹窗进行提示授权操作</param>
        /// <param name="source">授权接口请求的来源</param>
        /// <param name="appid">appid</param> 
        /// <param name="code">授权码</param>
        /// <param name="_state">附加参数</param>
        public WxOAuthAuthorizeAttribute(string type = "base", string source = "", int appid = 1, string state = "")
        {
            _type = type;
            _source = source;
            _appid = appid;
            _state = state;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = filterContext.HttpContext.User;
            var url = HttpUtility.UrlEncode(TextHelper.RequestUrl());
            _redirect_uri = url;

            // if (user == null || user.Identity == null || !user.Identity.IsAuthenticated)
            // {
            var obj = filterContext.HttpContext.Request.Params["openid"];

            if (!string.IsNullOrWhiteSpace(obj))
            {
                return;
            }
            string qu = string.Format("?type={0}&source={1}&redirect_uri={2}", _type, _source, _redirect_uri);
            string authorizeurl = WeixinConfig.WxOAuthAuthorize + _appid + qu;
            filterContext.Result = new RedirectResult(authorizeurl);
            //string qu = string.Format("type={0}&source={1}&appid={2}&redirect_uri={3}&state={4}", _type, _source, _appid, _redirect_uri, _state);
            //filterContext.Result = new RedirectResult("~/OAuth/Authorize?" + qu);
            //}
        }
    }
}