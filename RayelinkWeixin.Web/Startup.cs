﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(RayelinkWeixin.Web.Startup))]

namespace RayelinkWeixin.Web
{
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888

            //这个路径前面的/Signalr/是目前你所有的SignalR的Hub在客户端连接时所访问的服务地址,后面的"/hubs"则是将要下载的前端JS资源,
            //当我们在Startup类中app.MapSignalR()注册SignalR的时候,这个地址将自动注册,当然也可以改成自定义的代码(对应的前端JS地址也需要更改为""GoJJSMD/hubs"")如下:
            //注册地址为"GoJJSMD"        
            app.MapSignalR("/ReVisit/signalr", new HubConfiguration());

            //这个虚拟目录的作用其实就是在程序第一次被访问的时候自动根据被访问的Hub动态生成的JS脚本所存放的地方.
            //app.MapSignalR();
        }
    }
}
