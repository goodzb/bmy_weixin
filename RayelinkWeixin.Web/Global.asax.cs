﻿using System;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.Routing;
using Senparc.Weixin.Cache;
using Senparc.Weixin.Threads;
using System.Web.Optimization;
using Senparc.Weixin.Cache.Redis;
using RayelinkWeixin.Common.Utilites;

namespace RayelinkWeixin.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        #region Application_Start
        /// <summary>
        /// 
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            /* 微信配置开始
            * 
            * 建议按照以下顺序进行注册，尤其须将缓存放在第一位！
            */
            RegisterWeixinCache();      //注册分布式缓存（按需，如果需要，必须放在第一个）
            ConfigWeixinTraceLog();
            RegisterWeixinThreads();


            //var _appId = WeixinConfig.appID;
            //var _appSecret = WeixinConfig.appSecret;
            //AccessTokenContainer.Register(_appId, _appSecret);
            //加入log日志
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\log4net.config");
            log4net.Config.XmlConfigurator.Configure(fileInfo);
        } 
        #endregion

        #region Senparc.Weixin 自定义缓存策略
        /// <summary>
        /// 激活微信缓存
        /// </summary>
        private void RegisterWeixinThreads()
        {
            ThreadUtility.Register();
        }

        /// <summary>
        /// Senparc.Weixin 自定义缓存策略
        /// </summary>
        private void RegisterWeixinCache()
        {
            //如果留空，默认为localhost（默认端口）

            #region  Redis配置
            var redisConfiguration = TextHelper.GetDataConfigItem("RedisExchangeHosts");
            RedisManager.ConfigurationOption = redisConfiguration;

            //如果不执行下面的注册过程，则默认使用本地缓存

            if (!string.IsNullOrEmpty(redisConfiguration))
            {
                CacheStrategyFactory.RegisterObjectCacheStrategy(() => RedisObjectCacheStrategy.Instance);//Redis
            }
            #endregion
        }  

        /// <summary>
        /// Weixin系统日志
        /// </summary>
        private void ConfigWeixinTraceLog()
        {
            //这里设为Debug状态时，/App_Data/WeixinTraceLog/目录下会生成日志文件记录所有的API请求日志，正式发布版本建议关闭
            Senparc.Weixin.Config.IsDebug = false;
            Senparc.Weixin.WeixinTrace.SendCustomLog("系统日志", "系统启动");

            //自定义日志记录回调
            Senparc.Weixin.WeixinTrace.OnLogFunc = () =>
            {
                //加入每次触发Log后需要执行的代码
            };

            //当发生基于WeixinException的异常时触发
            Senparc.Weixin.WeixinTrace.OnWeixinExceptionFunc = ex =>
            {
                //加入每次触发WeixinExceptionLog后需要执行的代码

                //发送模板消息给管理员
                //var eventService = new EventService();
                //eventService.ConfigOnWeixinExceptionFunc(ex);
            };
        }
        #endregion
    }
}