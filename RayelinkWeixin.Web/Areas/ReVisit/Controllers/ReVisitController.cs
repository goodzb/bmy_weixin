﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 复诊
    /// </summary>
    public partial class ReVisitController : BaseController
    {
        #region 变量
        private const string ViewPath = "~/Areas/ReVisit/Views/{0}.cshtml";  //页面位置      
        private static string DoctorAvatarUrlD = TextHelper.GetConfigItem("crsUrl");
        private readonly CRSService.CRS_ServiceSoapClient crs = new CRSService.CRS_ServiceSoapClient();

        /// <summary>
        /// 支付结果通用通知URL
        /// </summary>
        private const string PayCallBackUrl = "ReVisit/ReVisit/PayCallBack";
        /// <summary>
        /// 本次活动的主键标识
        /// </summary>
        private const string CampaignSid = "A145FB34-F038-493D-B92E-A2A9A86BC573";
        #endregion

        #region Action通用执行方法
        /// <summary>
        /// Action通用执行方法
        /// </summary>
        /// <param name="openId"></param>
        private void ReVisitLoyout(ref string openId,short appid=KeyConst.OrderAppId)
        {
            if (openId.Length != 28)
            {
                openId = Base64Helper.DecodeBase64(openId);
            }
            this.ViewBag.TestUser = ReVisitBusiness.Instance.TestUsers;     //配置的测试人员信息
            this.ViewBag.OpenId = openId;
            this.ViewBag.CampaignSid = CampaignSid;
            this.ViewBag.BaseUrl = BaseUrl;
            if (!BaseUrl.StartsWith("http://localhost:"))
            {
                ViewBag.JsSDKModel = JsSDKModel.GetJsSDK(appid);
            }
        }
        #endregion

        #region 清空所有的医生缓存信息
        /// <summary>
        /// 清空所有的医生缓存信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ClearAllDoctorCache()
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                var doc = ReVisitBusiness.Instance.DoctorsInfo;
                if (null != doc && doc.Any())
                {
                    foreach (var dd in doc)
                    {
                        ReVisitBusiness.Instance.ClearCacheDoctor(dd.UserId);
                    }
                    rm.IsSuccess = true;
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 获取所有的医生信息
        /// <summary>
        /// 获取所有的医生信息
        /// </summary>
        /// <param name="openId">当前粉丝标识</param>
        /// <param name="dataSourceType">数据使用方式(1:展示详细信息,2:下拉框使用)</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetDoctors(string openId, short dataSourceType = 1)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                var data = ReVisitBusiness.Instance.DoctorsInfo;
                if (null != data)
                {
                    rm.IsSuccess = true;
                    if (dataSourceType.Equals(1))
                    {
                        rm.ResultData["Result"] = data;
                    }
                    if (dataSourceType.Equals(2))
                    {
                        rm.ResultData["Result"] = from m in data select new { id = m.UserId, value = m.Name, parentId = 0 };
                    }
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 从接口将医生信息拉取过来(初始化医生信息)

        /// <summary>
        /// 从接口将医生信息拉取过来
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult InitDoctorsInfo()
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "" };
            try
            {
                JArray doctorConfig = ReVisitBusiness.Instance.ConsultDoctors;  //会诊医生信息配置
                if (null != doctorConfig && doctorConfig.Any())
                {
                    int[] doctorIds = doctorConfig.Select(t => t.Value<int>("id")).ToArray();
                    var doctors = crs.getDoctorListBy(string.Join(",", doctorIds));

                    List<Areas.ReVisit.Models.Doctor> ds = JsonConvert.DeserializeObject<List<Areas.ReVisit.Models.Doctor>>(doctors.Data.ToString());
                    var doctorsDb = BusinessContent.FZ_Doctor.Find(string.Format("CrsId In('{0}')", string.Join("','", doctorIds)), BasePageSize);  //找出数据库中现有的医生列表

                    var doctorUserDb = BusinessContent.FZ_User.FindAll(string.Format("UserType={0}", KeyConst.ReVisit_UserType_Doctor));//所有的医生user信息

                    FZ_DoctorInfo doctorTemp, dbModel = null;
                    FZ_UserInfo userInfo = null;
                    string openid = string.Empty;
                    foreach (var item in ds)
                    {
                        rm.Text += item.Id + ">>";
                        var jt = doctorConfig.FirstOrDefault(t => t.Value<int>("id") == item.Id);

                        #region 为空逻辑处理

                        if (null == jt)
                        {
                            rm.Text += "医生ID为：" + item.Id + "配置信息有误，后续操作中断";
                            break;
                        }
                        openid = jt.Value<string>("openid");
                        if (string.IsNullOrWhiteSpace(openid))
                        {
                            rm.Text += "医生ID为：" + item.Id + "未配置openid，后续操作中断";
                            break;
                        }
                        #endregion

                        doctorTemp = null;
                        dbModel = doctorsDb.FirstOrDefault(t => t.CrsId == item.Id);

                        #region 处理user表信息
                        userInfo = new FZ_UserInfo()
                        {
                            OpenId = openid,
                            NickName = item.Name,
                            AvatarUrl = item.Image
                        };

                        var oldDoctorUserInfo = doctorUserDb.FirstOrDefault(t => t.OpenId == openid);
                        if (null != oldDoctorUserInfo)
                        {
                            userInfo.UpdateTime = DateTime.Now;
                            userInfo.Id = oldDoctorUserInfo.Id;
                            BusinessContent.FZ_User.Update(new { userInfo.NickName, userInfo.AvatarUrl, userInfo.UpdateTime }, userInfo.Id);
                            ReVisitBusiness.Instance.ClearUserCacheByOpenId(userInfo.OpenId, userInfo.UserType);    // 清空一条用户信息缓存
                        }
                        else
                        {
                            userInfo.UserType = KeyConst.ReVisit_UserType_Doctor;
                            userInfo.Gender = 0;
                            userInfo.CreateTime = DateTime.Now;
                            userInfo.Status = 1;
                            userInfo.Id = BusinessContent.FZ_User.Insert(userInfo);
                        }
                        #endregion

                        if (null != dbModel)
                        {
                            doctorTemp = dbModel;
                            doctorTemp.OpenId = openid;
                            doctorTemp.AvatarUrl = DoctorAvatarUrlD + item.Image;
                            doctorTemp.Name = item.Name;
                            doctorTemp.Title = item.TitleName;
                            doctorTemp.Introduction = item.SkillIn;
                            doctorTemp.HospitalName = item.AffiliatedHospital;
                            doctorTemp.DepartmentName = item.DeptName;
                            doctorTemp.Status = 1;
                            BusinessContent.FZ_Doctor.Update(doctorTemp);
                            ReVisitBusiness.Instance.ClearCacheDoctor(doctorTemp.UserId);    //清除一个Doctor的缓存信息
                        }
                        else
                        {
                            doctorTemp = new FZ_DoctorInfo()
                            {
                                UserId = userInfo.Id,
                                CrsId = item.Id,
                                OpenId = openid,
                                AvatarUrl = DoctorAvatarUrlD + item.Image,
                                Name = item.Name,
                                Title = item.TitleName,
                                Introduction = item.SkillIn,
                                HospitalID = 0,
                                HospitalName = item.AffiliatedHospital,
                                DepartmentID = 0,
                                DepartmentName = item.DeptName,
                                Status = 1,
                                RecentReplyCount = 0,
                                TotalReplyCount = 0,
                                TotalConsultCount = 0,
                                UnhandledOrderCount = 0,
                                ConsultFee = 300,
                                RemoteFee = 600
                            };
                            doctorTemp.Id = BusinessContent.FZ_Doctor.Insert(doctorTemp);
                        }
                    }
                    rm.IsSuccess = true;
                    // rm.Text = "成功";
                }
                else
                {
                    rm.Text = "缺少医生ID配置项";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 初始化助理与医生的关系信息
        /// <summary>
        /// 初始化助理与医生的关系信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult InfoSoctorsMapAssist()
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "" };
            try
            {
                var doctorsMapAssist = ReVisitBusiness.Instance.DoctorsMapAssistConfig;
                if (null != doctorsMapAssist)
                {
                    JArray ja = JArray.Parse(doctorsMapAssist);

                    if (null != ja && ja.Any())
                    {
                        List<FZ_UserInfo> doctor_Assist = ReVisitBusiness.Instance.Doctors_Assistants.ToList();    //所有的助理和医生列表
                        var doctorsMapAssists = ReVisitBusiness.Instance.DoctorsMapAssists;    //助理与医生对应关系信息
                        var doctorInfo = ReVisitBusiness.Instance.DoctorsInfo;  // 所有的医生详细信息列表
                        foreach (JObject jobject in ja)
                        {
                            int doctorCrsId = jobject.Value<int>("DoctorId");
                            string assistantOpenId = jobject.Value<string>("AssistantOpenId");  //助理Openid
                            var doc = doctorInfo.FirstOrDefault(t => t.CrsId == doctorCrsId);
                            rm.Text += assistantOpenId + ">>";
                            if (null == doc)    //必须先录入医生
                            {
                                rm.Text += string.Format("医生:{0}未导入，后续操作中断", doctorCrsId);
                                break;
                            }

                            var assistant = doctor_Assist.FirstOrDefault(t => t.UserType == KeyConst.ReVisit_UserType_Assistant && t.OpenId == assistantOpenId);  //当前助理信息

                            if (null == assistant)   //没有助理 做增加
                            {
                                assistant = new FZ_UserInfo
                                {
                                    UserType = KeyConst.ReVisit_UserType_Assistant,
                                    Status = 1,
                                    CreateTime = DateTime.Now,
                                    OpenId = assistantOpenId,
                                    Gender = 0,
                                };
                                assistant.Id = BusinessContent.FZ_User.Insert(assistant);

                                if (assistant.Id > 0)
                                {
                                    doctor_Assist.Add(assistant);
                                    FZ_DoctorsMapAssistInfo map = new FZ_DoctorsMapAssistInfo
                                    {
                                        AssistantId = assistant.Id,
                                        DoctorId = doc.Id,
                                        Status = 1
                                    };
                                    map.MapId = BusinessContent.FZ_DoctorsMapAssist.Insert(map);
                                    rm.IsSuccess = map.MapId > 0;
                                    ReVisitBusiness.Instance.ClearDoctorMapAssistant(assistant.OpenId);  //清空一个助理的OpenId关联的医生列表缓存信息
                                }
                            }
                            else
                            {
                                if (doctorsMapAssists.Any(t => t.DoctorId == doc.Id && t.AssistantId == assistant.Id)) continue;  //两者已有对应关系，直接略过

                                FZ_DoctorsMapAssistInfo map = new FZ_DoctorsMapAssistInfo
                                {
                                    AssistantId = assistant.Id,
                                    DoctorId = doc.Id,
                                    Status = 1
                                };
                                map.MapId = BusinessContent.FZ_DoctorsMapAssist.Insert(map);
                                rm.IsSuccess = map.MapId > 0;

                                ReVisitBusiness.Instance.ClearDoctorMapAssistant(assistant.OpenId);  //清空一个助理的OpenId关联的医生列表缓存信息
                            }
                            if (!rm.IsSuccess)
                            {
                                rm.Text = string.Format("操作被终止：DoctorId:{0},AssistantOpenId:{1}", doctorCrsId, assistantOpenId);
                                break;
                            }
                        }
                    }
                    else
                    {
                        rm.Text = "配置信息有误，请检查";
                    }
                }
                else
                {
                    rm.Text = "没有配置关系数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion    

    }
}
