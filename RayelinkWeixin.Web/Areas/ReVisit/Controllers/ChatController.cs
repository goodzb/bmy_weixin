﻿using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 回话
    /// </summary>
    public partial class ReVisitController
    {
        #region 变量
        private const string sql_01 = "UPDATE FZ_Conversations SET Content=@0 WHERE MediaId=@1";

        private const string sql_02 = "UPDATE dbo.FZ_Conversations SET IsRead=1,ReadTime=@0 WHERE OrderSid=@1 AND IsRead=0 AND (ReceiverOpenId=@2 OR ReceiverOpenId=@3)";

        private const string DefaultUserAvatar = "images/revisit/photo.png";
        #endregion

        #region 会话页面
        /// <summary>
        /// 会话页面
        /// </summary>
        /// <param name="openId">当前微信openid</param>
        /// <param name="orderSId">当前订单GUID</param>
        /// <param name="pageSource">页面来源(0：用户进入,1:医生进入，2：助理进入)</param>
        /// <returns></returns>
        [WxOAuthAuthorize(type: "base", source: "Chat", appid: KeyConst.OrderAppId)]
        public ActionResult Chat(string openId, Guid orderSId, short pageSource = 0)
        {
            ReVisitLoyout(ref openId);
            var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(orderSId);   // 从缓存里面获取一个订单的信息
            ViewBag.OrderInfo = orderInfo;
            ViewBag.DoctorInfo = null;
            FZ_DoctorInfo doctorInfo = null;
            ViewBag.ReVisitUser = null;
            if (null != orderInfo)
            {
                doctorInfo = ReVisitBusiness.Instance.GetCacheDoctorByDoctorId(orderInfo.DoctorID);    //根据doctorId获取一个doctor信息
                ViewBag.DoctorInfo = null != doctorInfo ? new { doctorInfo.UserId, doctorInfo.Status, doctorInfo.AvatarUrl, doctorInfo.Name, doctorInfo.OpenId } : null;

                var reVisitUser = ReVisitBusiness.Instance.GetReVisitUserByDoctorId(orderInfo.DoctorID);  // 根据医生ID获取对应的助理信息
                if (null != reVisitUser)
                {
                    ViewBag.ReVisitUser = new { reVisitUser.Id, reVisitUser.OpenId, reVisitUser.NickName, reVisitUser.Status, reVisitUser.AvatarUrl };
                }         
                ViewBag.OrderShotDate = orderInfo.CreateTime.ToString("MM-dd HH:mm:ss");
            }
            FZ_UserInfo userInfo = ReVisitBusiness.Instance.GetUserCacheByOpenId(openId, pageSource);

            #region 初始化当前的消息
            var message = new FZ_ConversationInfo { SenderOpenId = openId, SenderUserId = userInfo.Id, Status = 1, Content = null != orderInfo ? orderInfo.Description : "", OrderSid = orderSId, ChatType = 0 };
            if (pageSource.Equals(0))  //1：用户进入
            {
                message.Source = KeyConst.FZ_Conversations_Source_0;
                message.ReceiverOpenId = doctorInfo.OpenId;
                message.ReceiverUserId = doctorInfo.UserId;
            }
            else if (pageSource.Equals(1))  //1:医生进入
            {
                message.Source = KeyConst.FZ_Conversations_Source_1;
                userInfo = ReVisitBusiness.Instance.GetUserCacheByOpenId(orderInfo.OpenId, KeyConst.ReVisit_UserType_User);   //当前订单用户用户信息
                message.ReceiverOpenId = userInfo.OpenId;
                message.ReceiverUserId = userInfo.Id;
            }
            else if (pageSource.Equals(2))  //1:助理进入
            {
                message.Source = KeyConst.FZ_Conversations_Source_1;
                userInfo = ReVisitBusiness.Instance.GetUserCacheByOpenId(orderInfo.OpenId, KeyConst.ReVisit_UserType_User);   //当前订单用户用户信息
                message.ReceiverOpenId = userInfo.OpenId;
                message.ReceiverUserId = userInfo.Id;
            }
            #endregion

            #region 计算页面显示的头像
            string leftAvatarUrl = doctorInfo.AvatarUrl;
            string rigthAvatarUrl = userInfo.AvatarUrl;
            if (pageSource != 0)
            {
                leftAvatarUrl = userInfo.AvatarUrl;
                rigthAvatarUrl = doctorInfo.AvatarUrl;
                if (string.IsNullOrWhiteSpace(leftAvatarUrl))
                {
                    leftAvatarUrl = BaseUrl + DefaultUserAvatar;
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(rigthAvatarUrl))
                {
                    rigthAvatarUrl = BaseUrl + DefaultUserAvatar;
                }
            }
            ViewBag.LeftAvatarUrl = leftAvatarUrl;
            ViewBag.RigthAvatarUrl = rigthAvatarUrl;
            #endregion

            ViewBag.PageSource = pageSource;
            ViewBag.UserInfo = null != userInfo ? new { userInfo.Status, userInfo.NickName, userInfo.AvatarUrl } : null;
            ViewBag.MessageInfo = new { message.Status, message.OrderSid, message.ChatType, message.Source, message.Content, message.TipContent, message.SenderUserId, message.SenderOpenId, message.SenderConnectionId, message.ReceiverUserId, message.ReceiverOpenId, message.ReceiverConnectionId, message.MediaId };
            return View(string.Format(ViewPath, RouteData.Values["action"]));
        }
        #endregion   

        #region 已读未读消息
        /// <summary>
        /// 已读未读消息
        /// </summary>
        /// <param name="orderSid">订单GUID</param>
        /// <param name="openid">当前微信标识</param>
        /// <param name="doctorOpenId">医生openid</param>
        /// <param name="reVisitOpenId">助理openid</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ReadHistoryConversation(Guid orderSid, string openid, string doctorOpenId, string reVisitOpenId)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                if (BusinessContent.FZ_Conversations.Execute(sql_02, DateTime.Now, orderSid, doctorOpenId, reVisitOpenId) > 0)
                {
                    rm.Text = "OK";
                    rm.IsSuccess = true;
                }
                else
                {
                    rm.Text = "fail";
                    rm.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 获取历史消息
        /// <summary>
        /// 获取历史消息
        /// </summary>
        /// <param name="orderSid">订单GUID</param>
        /// <param name="sourceType">数据类型 0：用户进入,1:医生进入，2：助理进入</param>
        /// <param name="userOpenId">userOpenId</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetHistoryConversation(string openid, Guid orderSid, short sourceType = 0)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                var data = ReVisitBusiness.Instance.GetHistoryConversation(orderSid, sourceType);// 获取历史消息
                if (null != data)
                {
                    rm.IsSuccess = true;
                    rm.ResultData["Result"] = data;
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 批量下载用户上传的图片
        /// <summary>
        /// 批量下载用户上传的图片
        /// </summary>
        /// <param name="pics"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DownLoadImages(string openid, string pics, string suffix = "jpg")
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                FileLogHelper.Instance.Log("来了,pics=" + pics, "DownLoadImages");
                string[] picsList = pics.Split(',');
                List<string> result = new List<string>();
                if (null != picsList && picsList.Any())
                {
                    foreach (var item in picsList)
                    {
                        FileLogHelper.Instance.Log("item=" + item, "DownLoadImages");
                        rm = base.DownloadMediaFunction(KeyConst.OrderAppId, item, suffix);
                        if (rm.IsSuccess)
                        {
                            result.Add(rm.Text);
                        }
                    }
                    if (result.Any())
                    {
                        rm.IsSuccess = true;
                        rm.ResultData["Result"] = result;
                    }
                    else
                    {
                        rm.IsSuccess = false;
                    }
                }
                else
                {
                    rm.Text = "没有需要传送的文件，请尝试重试。";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}