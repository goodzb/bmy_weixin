﻿using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using System;
using System.Data;
using System.Reflection;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 列表页
    /// </summary>
    public partial class ReVisitController
    {

        /// <summary>
        /// 我的问诊 该页面主要用于展示该用户账号下的所有在线复诊单列表（包括 3 个分类：有效问题、 无效问题、 全部）
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        [WxOAuthAuthorize(type: "base", source: "Interrogate", appid: KeyConst.OrderAppId)]
        public ActionResult Interrogate(string openid, Guid? orderSid)
        {
            ReVisitLoyout(ref openid);
            FZ_UserInfo user = ReVisitBusiness.Instance.GetUserCacheByOpenId(openid);  //找用户信息
            if (null != user && (user.UserType == KeyConst.ReVisit_UserType_Assistant || user.UserType == KeyConst.ReVisit_UserType_Doctor))
            {
                if (user.UserType == KeyConst.ReVisit_UserType_Assistant)
                {
                    return RedirectToAction("Referral", new { orderSid, openid, doctorId = 0, assistantId = user.Id });
                }
                else
                {
                    #region 主要解决多个医生绑定同一个微信用户
                    if (null != orderSid && orderSid != default(Guid))
                    {
                        try
                        {
                            openid = ReVisitBusiness.Instance.GetCacheDoctorByDoctorId(ReVisitBusiness.Instance.OrderInfoBySid((Guid)orderSid).DoctorID).OpenId;
                        }
                        catch (Exception ex)
                        {
                            ex.Data["method"] = "Interrogate";
                            new ExceptionHelper().LogException(ex);
                        }
                    }
                    #endregion
                    return RedirectToAction("Referral", new { orderSid, openid, doctorId = user.Id, assistantId = 0 });
                }
            }
            return View(string.Format(ViewPath, RouteData.Values["action"]));
        }

        /// <summary>
        /// 我的看诊 该页面主要用于展示该专家/专家助理账号下的所有图文复诊订单列表
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="doctorId"></param>
        /// <param name="assistantId"></param>
        /// <returns></returns>
        [WxOAuthAuthorize(type: "base", source: "Referral", appid: KeyConst.OrderAppId)]
        public ActionResult Referral(string openid, Guid? orderSid, int doctorId = 0, int assistantId = 0)
        {
            ReVisitLoyout(ref openid);

            ViewBag.DoctorId = doctorId;
            ViewBag.AssistantId = 0;
            short pageSource = 1;
            if (doctorId == 0)//代表当前是助理
            {
                pageSource = 2;
                var reVisitUser = ReVisitBusiness.Instance.GetUserCacheByOpenId(openid, KeyConst.ReVisit_UserType_Assistant);  //助理
                ViewBag.AssistantId = reVisitUser.Id;
            }
            ViewBag.PageSource = pageSource;  //1:医生进入，2：助理进入
            return View(string.Format(ViewPath, RouteData.Values["action"]));
        }

        /// <summary>
        /// 我的问诊列表
        /// </summary>
        /// <param name="openid">当前微信</param>
        /// <param name="doctorId">医生ID</param>
        /// <param name="assistantId">助理ID</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult OrderList(string openid, int doctorId = 0, int assistantId = 0)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                DataTable data = null;
                if (doctorId == 0 && assistantId == 0)
                {
                    data = ReVisitBusiness.Instance.OrderList_Interrogate(openid);// 我的问诊
                }
                else
                {
                    data = ReVisitBusiness.Instance.OrderList_Referral(openid, doctorId, assistantId);// 我的看诊
                }
                if (null != data)
                {
                    rm.IsSuccess = true;
                    rm.ResultData["Result"] = JsonHelper.ObjectToJsonStr(data);
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }

    }
}