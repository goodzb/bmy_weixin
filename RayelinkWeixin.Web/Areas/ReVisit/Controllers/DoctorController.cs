﻿using Newtonsoft.Json;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Web.Filters;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using System;
using System.Reflection;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 医生
    /// </summary>
    public partial class ReVisitController
    {

        #region 医生列表
        /// <summary>
        /// 医生列表
        /// </summary>
        /// <returns></returns>
        [WxOAuthAuthorize("super", appid: KeyConst.OrderAppId)]
        public ActionResult DoctorIndex(string openid)
        {
            ReVisitLoyout(ref openid);
            var user = ReVisitBusiness.Instance.GetUserCacheByOpenId(openid);   //当前用户信息
            if (null != user && (user.UserType == KeyConst.ReVisit_UserType_Assistant || user.UserType == KeyConst.ReVisit_UserType_Doctor))
            {
                return Content("医生/助理无权限进入此页面，请退出页面......");
            }
            ViewBag.Doctors = ReVisitBusiness.Instance.DoctorsInfo;
            InitUser(openid);   // 初始化一个用户信息
            return View(string.Format(ViewPath, RouteData.Values["action"]));
        } 
        #endregion

        #region 初始化一个用户信息
        /// <summary>
        /// 初始化一个用户信息
        /// </summary>
        /// <param name="openid"></param>
        private void InitUser(string openid)
        {
            try
            {
                var userInfo = ReVisitBusiness.Instance.GetUserCacheByOpenId(openid, KeyConst.ReVisit_UserType_User);   //当前用户信息
                string nickname = string.Empty;
                string avatarUrl = string.Empty;
                if (null != this.Request.QueryString["userinfo"])
                {
                    OAuthUserInfo oAuthUserinfo = JsonConvert.DeserializeObject<OAuthUserInfo>(Request.QueryString["userinfo"]);  //高级授权获取的用户详细信息
                    if (null != oAuthUserinfo)
                    {
                        nickname = oAuthUserinfo.nickname;
                        avatarUrl = oAuthUserinfo.headimgurl;
                    }
                }
                if (null == userInfo)
                {
                    if (!string.IsNullOrWhiteSpace(nickname) && !string.IsNullOrWhiteSpace(avatarUrl))
                    {
                        BusinessContent.FZ_User.Insert(new Model.FZ_UserInfo { OpenId = openid, UserType = KeyConst.ReVisit_UserType_User, Status = 1, NickName = nickname, AvatarUrl = avatarUrl });
                    }
                    else
                    {
                        BusinessContent.FZ_User.Insert(new Model.FZ_UserInfo { OpenId = openid, UserType = KeyConst.ReVisit_UserType_User, Status = 1 });
                    }                   
                }
                else if (!string.IsNullOrWhiteSpace(nickname) && !string.IsNullOrWhiteSpace(avatarUrl))
                {                   
                    BusinessContent.FZ_User.Update(new { NickName = nickname, AvatarUrl = avatarUrl }, userInfo.Id);//修改头像

                    ReVisitBusiness.Instance.ClearUserCacheByOpenId(openid, userInfo.UserType);  // 清空一条用户信息缓存
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
        }
        #endregion

        #region 刷新一个医生的缓存信息
        /// <summary>
        /// 刷新一个医生的缓存信息
        /// </summary>
        /// <param name="orderSid"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult FlushDortor(string doctorOpenid)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作完成" };
            try
            {
                rm.IsSuccess = true;
                ReVisitBusiness.Instance.ClearUserCacheByOpenId(doctorOpenid, KeyConst.ReVisit_UserType_Doctor);  //清空一条用户信息缓存      
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
