﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using RayelinkWeixin.Web.Filters;
using Senparc.Weixin.MP.TenPayLibV3;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace RayelinkWeixin.Web.Controllers
{
    /// <summary>
    /// 订单信息
    /// </summary>
    public partial class ReVisitController
    {
        #region 变量
        private const string PrivateCode = "668888";
        private const string OverOrder_sql = "UPDATE dbo.FZ_Orders SET Status=@0,ConversationStatus=@1,CloseTime=@2,CloseReason=@3 WHERE OrderSid=@4;";
        private const string RemotelySucessWxMsg = "您已成功付款，远程门诊预约卡号及密码已短信发送至您的手机，请注意查收 ！如有任何疑 问 ，请联系 021-62457275。";
        private const string CardSmsTemp = "尊敬的用户，您好！您在帮忙医商城购买的名医远程门诊服务已支付成功，预约卡号为 {0}，密码为 {1}。请您提前三天致电客服热线4001-108-102进行预约，就诊时请向当地医生助理出示本短信内容，即可抵消门诊费用。帮忙医衷心祝愿您身体健康！";
        private const string RemotelyMailMsg = "医卡订单通知： 订单号：{0} 下单日期：{1} 姓名：{2} 手机号：{3} 商品名称：{4} 卡号：{5} 密码：保密 卡类型编码：{6}";
        private static string RemotelyMail = "xiaohan1@rich-healthcare.com";    // 接收卡密记录的邮件地址
        private readonly string TwText = "专家暂时忙，请耐心等待。 专家回复后，会第一时间通知您, 请注意帮忙医服务号的消息通知。" + Environment.NewLine + "<a href='{0}ReVisit/ReVisit/Chat?openid={1}&pageSource=0&orderSId={2}'>点这里立即查看</a>";
        #endregion

        #region 静态构造
        /// <summary>
        /// 静态构造
        /// </summary>
        static ReVisitController()
        {
            RemotelyMail = ReVisitBusiness.Instance.RemotelyMail;
        }
        #endregion

        #region  填写患者信息(下订单)
        /// <summary>
        /// 填写患者信息(下订单)
        /// </summary>
        /// <param name="openId">粉丝标识</param>
        /// <param name="doctorId">选择的医生ID</param>
        /// <returns></returns>
        public ActionResult OrderIndex(string openId, int doctorId)
        {
            ReVisitLoyout(ref openId);
            var doctorInfo = ReVisitBusiness.Instance.GetCacheDoctorByDoctorId(doctorId);    //根据doctorId获取一个doctor信息
            ViewBag.DoctorInfo = doctorInfo;
            ViewBag.OrderInfo = new FZ_OrderInfo { Status = 1, OpenId = openId, ConversationStatus = 0, ProvinceName = "请选择地区", CityName = "", DistrictName = "", DoctorID = doctorId, DoctorName = doctorInfo.Name, TherapyDoctorName = "请选择您的主刀医生", TherapyDoctorId = -1, CloseReason = "", Address = "" };
            return View(string.Format(ViewPath, RouteData.Values["action"]));
        }
        #endregion

        #region 复诊服务页(选择订单类型进行支付)
        /// <summary>
        /// 复诊服务页(选择订单类型进行支付)
        /// </summary>
        /// <param name="openId">粉丝标识</param>
        /// <param name="doctorId">主刀医生</param>
        /// <param name="orderSId">订单主键GUID</param>
        /// <returns></returns>
        public ActionResult ReVisitService(string openId, int doctorId, Guid orderSId)
        {
            ReVisitLoyout(ref openId);
            int payCount = ReVisitBusiness.Instance.PayCount(doctorId);
            ViewBag.PayCount = payCount + 35;
            ViewBag.DoctorInfo = ReVisitBusiness.Instance.GetCacheDoctorByDoctorId(doctorId);    //根据doctorId获取一个doctor信息
            return View(string.Format(ViewPath, RouteData.Values["action"]));
        }
        #endregion

        #region 确认图文咨询支付页面
        /// <summary>
        /// 确认图文咨询支付页面
        /// </summary>
        /// <param name="openId">支付账号的openid(当前页面授权得来)</param>
        /// <param name="orderOpenId">提交订单的openid</param>
        /// <param name="doctorId"></param>
        /// <param name="orderSId"></param>
        /// <returns></returns>
        [WxOAuthAuthorize(type: "base", source: "ConsultPay", appid: KeyConst.PayAppId)]
        public ActionResult ConsultPay(string openId, string orderOpenId, int doctorId, Guid orderSId)
        {
            // List<string> contents = new List<string>();
            //contents.Add("------------------------------------------ConsultPay---------------------------------------");
            ReVisitLoyout(ref openId, KeyConst.PayAppId);
            var doctorInfo = ReVisitBusiness.Instance.GetCacheDoctorByDoctorId(doctorId);    //根据doctorId获取一个doctor信息       
            var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(orderSId, false);        //从缓存里面获取一个订单的信息
            ViewBag.DoctorInfo = doctorInfo;
            //contents.Add("订单信息：" + JsonConvert.SerializeObject(orderInfo));
            if (orderInfo.Status == KeyConst.ReVisit_OrderStatus_1 || orderInfo.Status == KeyConst.ReVisit_OrderStatus_5 || orderInfo.Status == KeyConst.ReVisit_OrderStatus_20)
            {
                orderInfo.WechatOrderName = doctorInfo.Name + "医生图文咨询";
                orderInfo.OrderAmount = doctorInfo.ConsultFee;
                ViewBag.OrderInfo = orderInfo;
                // contents.Add("二级判断");
                if ((orderInfo.OrderType == KeyConst.ReVisit_OrderType_0 && orderInfo.Status == KeyConst.ReVisit_OrderStatus_1) || (orderInfo.OrderType == KeyConst.ReVisit_OrderType_1 && orderInfo.Status == KeyConst.ReVisit_OrderStatus_20))
                {
                    string out_trade_no = RadomHelper.GetRandomString(30, false);
                    var obj = new { Out_trade_no = out_trade_no, orderInfo.OrderAmount, OrderType = KeyConst.ReVisit_OrderType_0, Status = KeyConst.ReVisit_OrderStatus_1 };
                    // contents.Add("新的对象：" + JsonConvert.SerializeObject(obj));
                    var res = BusinessContent.FZ_Order.Update("OrderSid", obj, orderSId);
                    // contents.Add("更新结果：" + res);
                    if (res > 0)
                    {
                        ReVisitBusiness.Instance.ClearOrderCache(orderSId); // 删除一个订单的缓存信息
                        Common.Share.ReturnMessage payJsJdk = ReVisitBusiness.Instance.GetWeChatPay(KeyConst.PayAppId, openId, out_trade_no, orderInfo.OrderSid, (orderInfo.OrderAmount * 100) + "", BaseUrl + PayCallBackUrl);
                        //contents.Add("payJsJdk：" + JsonConvert.SerializeObject(payJsJdk));
                        if (payJsJdk.IsSuccess)
                        {
                            ViewBag.OrderInfo = ReVisitBusiness.Instance.GetCacheDoctorByDoctorId(doctorId);
                            //FileLogHelper.Instance.Log(contents, "ConsultPay");
                            return View(string.Format(ViewPath, RouteData.Values["action"]), payJsJdk.ResultData["Result"]);
                        }
                    }
                }
                // contents.Add("没有结果：");
                // FileLogHelper.Instance.Log(contents, "ConsultPay");
                return View(string.Format(ViewPath, RouteData.Values["action"]));
            }
            else if (orderInfo.Status != KeyConst.ReVisit_OrderStatus_4 && orderInfo.Status != KeyConst.ReVisit_OrderStatus_5 && (orderInfo.Status == KeyConst.ReVisit_OrderStatus_3 || orderInfo.Status == KeyConst.ReVisit_OrderStatus_8 || orderInfo.Status == KeyConst.ReVisit_OrderStatus_9 || orderInfo.Status == KeyConst.ReVisit_OrderStatus_9))
            {
                // FileLogHelper.Instance.Log(contents, "ConsultPay");
                return RedirectToAction("Chat", new { openId, doctorId, orderSId });
            }
            else
            {
                //FileLogHelper.Instance.Log(contents, "ConsultPay");
                return RedirectToAction("DoctorIndex", new { openId, doctorId, orderSId });
            }
        }
        #endregion

        #region 确认远程门诊支付页面

        /// <summary>
        /// 确认远程门诊支付页面
        /// </summary>
        /// <param name="openId">支付账号的openid(当前授权得来)</param>
        /// <param name="orderOpenId">提交订单的openid</param>
        /// <param name="doctorId"></param>
        /// <param name="orderSId"></param>
        /// <returns></returns>
        [WxOAuthAuthorize(type: "base", source: "RemotelyPay", appid: KeyConst.PayAppId)]
        public ActionResult RemotelyPay(string openId, string orderOpenId, int doctorId, Guid orderSId)
        {
            ReVisitLoyout(ref openId, KeyConst.PayAppId);
            var doctorInfo = ReVisitBusiness.Instance.GetCacheDoctorByDoctorId(doctorId);    //根据doctorId获取一个doctor信息
            ViewBag.DoctorInfo = doctorInfo;
            var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(orderSId);
            orderInfo.OrderType = KeyConst.ReVisit_OrderType_1;
            orderInfo.WechatOrderName = doctorInfo.Name + "医生远程门诊";
            orderInfo.OrderAmount = doctorInfo.RemoteFee;
            ViewBag.OrderInfo = orderInfo;
            return View(string.Format(ViewPath, RouteData.Values["action"]));
        }
        #endregion

        #region 获取一个订单信息

        /// <summary>
        /// 获取一个订单信息
        /// </summary>
        /// <param name="orderSid"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetOrderInfo(Guid orderSid, string openid)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(orderSid);
                if (null != orderInfo)
                {
                    rm.IsSuccess = true;
                    rm.ResultData["Result"] = orderInfo;// new { orderInfo.OrderSid, orderInfo.Status, orderInfo.OpenId, orderInfo.ConversationStatus, orderInfo.OrderType };
                    rm.Text = "操作完成";
                }
                else
                {
                    rm.Text = "没有数据";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 提交订单
        /// <summary>
        /// 提交订单
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SubmitOrder(FZ_OrderInfo data)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成，请核实" };
            try
            {
                if (null != data)
                {
                    string validCode = data.WechatOrderId;  //前端将验证码先放在了这里面

                    if (!string.IsNullOrWhiteSpace(validCode) && (validCode.Equals(PrivateCode) || (ValidCode(data.UserMobile, validCode, CampaignSid, true)).IsSuccess))
                    {
                        #region 处理默认值
                        if (string.IsNullOrWhiteSpace(data.Description))
                        {
                            data.Description = "尚未确诊";
                        }
                        data.PayTime = new DateTime(2000, 1, 1);
                        data.CancelTime = new DateTime(2000, 1, 1);
                        data.CloseTime = new DateTime(2000, 1, 1);
                        data.EndTime = DateTime.Now.AddMinutes(ReVisitBusiness.Instance.OrderTimeOutMinutes);
                        #endregion
                        data.OrderNo = ReVisitBusiness.Instance.NewOrderNo;
                        data.OrderSid = Guid.NewGuid();
                        data.Id = BusinessContent.FZ_Order.Insert(data);
                        if (data.Id > 0)
                        {
                            rm.IsSuccess = true;
                            rm.Text = "操作完成";
                            rm.ReturnUrl = string.Format("{0}ReVisit/ReVisit/ReVisitService?orderSId={1}&doctorId={2}&openid={3}", BaseUrl, data.OrderSid, data.DoctorID, data.OpenId);
                        }
                    }
                    else
                    {
                        rm.Text = "请输入正确的验证码";
                    }
                }
                else
                {
                    rm.Text = "提交数据有误，请核实";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion 

        #region 提交远程门诊信息
        /// <summary>
        /// 提交远程门诊信息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SubmitRemotely(FZ_OrderInfo data)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                if (null != data)
                {
                    string payOpenid = data.Transaction_id;
                    data.Transaction_id = "";
                    string validCode = data.CloseReason;  //前端将验证码先放在了这里面
                    data.CloseReason = string.Empty;
                    var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(data.OrderSid);  // 从缓存里面获取一个订单的信息
                    if (orderInfo.Status == KeyConst.ReVisit_OrderStatus_3 || orderInfo.Status == KeyConst.ReVisit_OrderStatus_2)
                    {
                        rm.ReturnUrl = string.Format("{0}ReVisit/ReVisit/DoctorIndex?&openid={1}", BaseUrl, data.OpenId);
                        rm.Text = "此订单已支付，请勿重复支付";
                    }
                    else if (data.UserMobile != data.PatientMobile && string.IsNullOrEmpty(validCode))
                    {
                        rm.Text = "请输入正确的验证码";
                    }
                    else if (data.UserMobile != data.PatientMobile && string.IsNullOrWhiteSpace(validCode))
                    {
                        rm.Text = "请输入正确的验证码";
                    }
                    else if (!string.IsNullOrWhiteSpace(validCode) && !(validCode.Equals(PrivateCode) || (ValidCode(data.PatientMobile, validCode, CampaignSid)).IsSuccess))
                    {
                        rm.Text = "请输入正确的验证码";
                    }
                    else
                    {
                        var doctorInfo = ReVisitBusiness.Instance.GetCacheDoctorByDoctorId(data.DoctorID);    //根据doctorId获取一个doctor信息
                        string out_trade_no = RadomHelper.GetRandomString(30, false);
                        var obj = new { OrderAmount = doctorInfo.RemoteFee, Out_trade_no = out_trade_no, data.PatientCityId, data.PatientCityName, data.PatientMobile, data.PatientName, data.OrderSid, data.OrderType, data.WechatOrderName, Status = KeyConst.ReVisit_OrderStatus_20 };
                        if (BusinessContent.FZ_Order.Update("OrderSid", obj, data.OrderSid) > 0)
                        {
                            ReVisitBusiness.Instance.ClearOrderCache(data.OrderSid); // 删除一个订单的缓存信息
                            rm.IsSuccess = true;
                            rm.Text = "操作完成";
                            rm = ReVisitBusiness.Instance.GetWeChatPay(KeyConst.PayAppId, payOpenid, out_trade_no, data.OrderSid, (data.OrderAmount * 100) + "", BaseUrl + PayCallBackUrl);
                        }
                        else
                        {
                            rm.Text = "提交失败，请核实";
                        }
                    }
                }
                else
                {
                    rm.Text = "提交数据有误，请核实";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 支付处理

        /// <summary>
        /// 支付处理
        /// </summary>
        /// <param name="payOpenId">支付微信账号的openid</param>
        /// <param name="orderOpenId">订单微信账号的openid</param>
        /// <param name="orderNo">订单编号</param>
        /// <param name="orderSid">订单GUID</param>
        /// <param name="payResult">状态: 1：未支付，默认提交状态；2：H5支付成功；3:回调支付成功(这个是支付后微信回调post回来的支付数据，这才是真正意义上的支付成功):4：支付失败；5：支付过程中用户取消支付; 6：用户取消；7：超时job取消；8：医生结束咨询(此订单完成咨询服务)；20：未支付(远程门诊会存在)</param>
        /// <param name="orderType">订单类型：0-图文、1-远程</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PayResult(string payOpenId, string orderOpenId, string orderNo, Guid orderSid, short payResult, short orderType = 0)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "订单不存在" };
            try
            {
                string nextPage = string.Empty;
                LogHelper.Debug(string.Format("支付处理：{0}", Request.Url.ToString()));
                var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(orderSid);  // 从缓存里面获取一个订单的信息
                int res = 0;
                if (null != orderInfo)
                {
                    if (payResult.Equals(KeyConst.ReVisit_OrderStatus_4) || payResult.Equals(KeyConst.ReVisit_OrderStatus_5))
                    {
                        rm.Text = "支付失败";
                        res = BusinessContent.FZ_Order.Update("OrderSid", new { Status = payResult, PayTime = DateTime.Now, CancelTime = DateTime.Now }, orderSid);
                    }
                    else  //支付成功
                    {
                        if (orderInfo.Status != KeyConst.ReVisit_OrderStatus_3)  //经测试发现支付后回调很快，回调后会修改状态的,这里就不再修改了
                        {
                            res = BusinessContent.FZ_Order.Update("OrderSid", new { Status = payResult, PayTime = DateTime.Now, CancelTime = DateTime.Now }, orderSid);
                            if (res > 0)
                            {
                                rm.IsSuccess = true;
                            }
                        }
                        else
                        {
                            rm.IsSuccess = true;
                        }
                    }
                    if (rm.IsSuccess)
                    {
                        rm.Text = "支付完成";
                        if (orderType.Equals(KeyConst.ReVisit_OrderType_0))  //图文咨询
                        {
                            nextPage = string.Format("{0}ReVisit/ReVisit/Chat?orderSid={1}", BaseUrl, orderSid);

                            ReVisitBusiness.Instance.SendCustomText(KeyConst.OrderAppId, orderInfo.OpenId, string.Format(TwText, BaseUrl, orderInfo.OpenId, orderInfo.OrderSid));   //给用户发消息提醒
                        }
                        else
                        {
                            nextPage = string.Format("{0}ReVisit/ReVisit/DoctorIndex?openid={1}", BaseUrl, orderOpenId);
                            Task.Factory.StartNew(() =>
                           {
                               SucessRemotely(orderSid);   // 远程门诊成功处理
                           });
                        }
                        BusinessContent.FZ_PayRecords.Insert(new FZ_PayRecordInfo { SerialNumber = orderInfo.OrderNo, OrderSid = orderSid, Type = KeyConst.FZ_PayRecordType_0, Status = 1, PayResult = payResult, Remark = "H5支付", PayOpenId = payOpenId });
                    }
                }
                LogHelper.Debug(string.Format("支付处理走了，res:{0},nextPage：{1}", res, nextPage));
                rm.ReturnUrl = nextPage;
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(ex);
            }

            UserActionEventLog(new UserActionEventLogInfo
            {
                AppId = 1,
                OpenId = orderOpenId,
                ActionType = KeyConst.Uceat_Event,
                ActionName = "PayResult",
                ActionDesc = orderSid + ",res=" + payResult,
                Parameter = rm.Text
            });
            return MyJson(rm);
        }
        #endregion

        #region 远程门诊成功处理
        /// <summary>
        /// 远程门诊成功处理
        /// </summary>
        /// <param name="orderSId"></param>
        public void SucessRemotely(Guid orderSId)
        {
            LogHelper.Debug("-----------------------------SucessRemotely----------------------------------");
            string cardNO = string.Empty;
            string cardPWD = string.Empty;
            FZ_OrderInfo orderInfo = null;
            string res = string.Empty;
            string cardIdentityID = string.Empty;
            string cardPassword = string.Empty;
            int CardTypeID = 0;
            try
            {
                orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(orderSId);  // 从缓存里面获取一个订单的信息
                if (null != orderInfo && orderInfo.OrderType == KeyConst.ReVisit_OrderType_1)
                {
                    CardTypeID = ReVisitBusiness.Instance.CardTypeByPrice(orderInfo.OrderAmount);   // 根据价格找卡类别ID
                    cardIdentityID = ReVisitBusiness.Instance.ReVisitConfig<string>(KeyConst.OrderAppId, "CardIdentityID");
                    cardPassword = ReVisitBusiness.Instance.ReVisitConfig<string>(KeyConst.OrderAppId, "CardPassword");

                    res = CreateVirtualCard_BangMangYi(cardIdentityID, cardPassword, CardTypeID, orderInfo.OrderAmount, orderInfo.OrderNo);

                    if (!string.IsNullOrWhiteSpace(res))
                    {
                        JObject obj = JObject.Parse(res);

                        if (null != obj && obj.Value<string>("Success") == "true" && obj.Value<string>("Info").Contains("-"))
                        {
                            string Info = obj.Value<string>("Info");
                            LogHelper.Debug("Info：" + Info);
                            var infos = Info.Split('-');
                            cardNO = infos[0];
                            cardPWD = infos[1];
                            base.SendSmsMessage(orderInfo.OpenId, orderInfo.PatientMobile, string.Format(CardSmsTemp, cardNO, cardPWD));  //发卡密
                            ReVisitBusiness.Instance.SendCustomText(KeyConst.OrderAppId, orderInfo.OpenId, RemotelySucessWxMsg);   //给用户发消息提醒
                            base.SendMailMessage(orderInfo.OpenId, RemotelyMail, "十院复诊", string.Format(RemotelyMailMsg, orderInfo.OrderNo, DateTime.Now, orderInfo.PatientName, orderInfo.PatientMobile, "远程门诊" + orderInfo.OrderAmount + "元", cardNO, CardTypeID));  //发卡密
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrWhiteSpace(cardNO))
                {
                    cardNO = ex.Message;
                }
                new ExceptionHelper().LogException(ex);
            }

            BusinessContent.FZ_SendCardRecords.Insert(new FZ_SendCardRecordInfo
            {
                OrderNo = null != orderInfo ? orderInfo.OrderNo : "",
                InputParams = string.Format("cardIdentityID={0}, cardPassword={1}, CardTypeID={2},OrderAmount={3}, OrderNo={4}", cardIdentityID, cardPassword, CardTypeID, orderInfo.OrderAmount, orderInfo.OrderNo),
                Status = 1,
                CardNo = cardNO,
                CardPWD = cardPWD,
                OrderSid = orderSId,
                OutputParams = res,
                CreateTime = DateTime.Now
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identityID">固定值BangMangYi</param>
        /// <param name="password">固定值fed45Bc2eCe2A5DF</param>
        /// <param name="CardTypeID">卡类型ID</param>
        /// <param name="Price">实际销售价</param>
        /// <param name="orderID">订单号</param>
        /// <returns>{"Success":"true","Info":"V10002377031-470320"}</returns>
        private string CreateVirtualCard_BangMangYi(string identityID, string password, int CardTypeID, decimal Price, string orderID)
        {
            return new Web.ThridInterface.RichHealthThridInterfaceClient().CreateVirtualCard_BangMangYi(identityID, password, CardTypeID, Price, orderID);
        }
        #endregion

        #region 退款申请处理
        /// <summary>
        /// 退款申请处理
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult PayRefund(Guid orderSid)
        {
            short payResult = 0;
            string result_code = string.Empty;
            string out_refund_no = string.Empty;
            string refund_id = string.Empty;

            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(orderSid);  // 从缓存里面获取一个订单的信息
                if (null != orderInfo && (orderInfo.Status == KeyConst.ReVisit_OrderStatus_6 || orderInfo.Status == KeyConst.ReVisit_OrderStatus_7) && (orderInfo.ConversationStatus == KeyConst.ReVisit_ConversationStatus_3 || orderInfo.ConversationStatus == KeyConst.ReVisit_ConversationStatus_5))
                {
                    var pr = ReVisitBusiness.Instance.PayRefundByOrder(KeyConst.PayAppId, orderInfo);// 根据订单发起退款申请
                    if (pr.Item1)
                    {
                        result_code = pr.Item3;
                        out_refund_no = pr.Item4;
                        refund_id = pr.Item5;

                        payResult = 1;
                        rm.IsSuccess = true;
                        rm.Text = "Sucess";
                        var cres = BusinessContent.FZ_Order.Update("OrderSid", new { Status = KeyConst.ReVisit_OrderStatus_10 }, orderSid);   //修改状态为退款中
                        ReVisitBusiness.Instance.ClearOrderCache(orderSid);     //删除一个订单的缓存信息
                        SendSmsMessage(orderInfo.OpenId, orderInfo.UserMobile, string.Format("您有一笔订单退款{0}元，款项已退回至您的支付账户，请注意查收", orderInfo.OrderAmount));
                    }
                    else
                    {
                        rm.Text = "申请退款失败";
                    }
                }
                else
                {
                    rm.Text = "订单不存在或状态不正确，无法完成退款申请";
                }
            }
            catch (Exception ex)
            {
                payResult = -1;
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }

            string payOpenId = "";
            var payInfo = BusinessContent.FZ_PayRecords.FindOne("OrderSid=@0 AND LEN(PayOpenId)>0 ", orderSid);
            if (null != payInfo)
            {
                payOpenId = payInfo.PayOpenId;
            }
            BusinessContent.FZ_PayRecords.Insert(new FZ_PayRecordInfo { OrderSid = orderSid, Type = KeyConst.FZ_PayRecordType_2, Status = 1, PayResult = payResult, SerialNumber = result_code, WechatOrderId = out_refund_no, WechatOrderName = refund_id, PayOpenId = payOpenId, Remark = "申请退款" });
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 收到医生/助理关闭咨询
        /// <summary>
        /// 收到医生/助理关闭咨询(完成咨询)
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="orderSid"></param>
        /// <param name="closeReason"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult OverOrder(string openid, Guid orderSid, string closeReason = "手动关闭")
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                var res = BusinessContent.FZ_Order.Execute(OverOrder_sql, KeyConst.ReVisit_OrderStatus_8, KeyConst.ReVisit_ConversationStatus_4, DateTime.Now, closeReason, orderSid);
                UserActionEventLogBusiness.Instance.Push(new UserActionEventLogInfo { AppId = 1, OpenId = openid, ActionName = "OverOrder", ActionType = KeyConst.Uceat_Event, Parameter = res + " >> " + closeReason });
                ReVisitBusiness.Instance.ClearOrderCache(orderSid);  //删除订单缓存信息
                if (res > 0)
                {
                    rm.IsSuccess = true; rm.Text = "操作完成";
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm);
        }
        #endregion

        #region 用户取消咨询

        /// <summary>
        /// 用户取消咨询
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="orderSid"></param>
        /// <param name="closeReason"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CancelOrder(string openid, Guid orderSid, string closeReason = "用户手动关闭")
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "订单不存在或订单状态无法取消，请核实" };
            try
            {
                var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(orderSid);  // 从缓存里面获取一个订单的信息
                if (null != orderInfo && orderInfo.Status == KeyConst.ReVisit_OrderStatus_3 && orderInfo.ConversationStatus <= KeyConst.ReVisit_ConversationStatus_1)
                {
                    var res = BusinessContent.FZ_Order.Execute(OverOrder_sql, KeyConst.ReVisit_OrderStatus_6, KeyConst.ReVisit_ConversationStatus_3, DateTime.Now, closeReason, orderSid);
                    UserActionEventLogBusiness.Instance.Push(new UserActionEventLogInfo { AppId = 1, OpenId = openid, ActionName = "CancelOrder", ActionType = KeyConst.Uceat_Event, Parameter = res + " >> " + closeReason, ActionDesc = orderSid + "" });
                    ReVisitBusiness.Instance.ClearOrderCache(orderSid);  //删除订单缓存信息
                    if (res > 0)
                    {
                        rm.IsSuccess = true;
                        rm.Text = "操作完成";
                        RayelinkWeixin.Business.ReVisitBusiness.Instance.SendCustomText(KeyConst.OrderAppId, openid, ReVisitBusiness.Instance.InvalidTimeOutOrders_Msg);   //给用户发消息提醒 

                        PayRefund(orderSid);    //退款申请
                    }
                    else
                    {
                        rm.Text = "操作失败，请核实";
                    }
                }
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm);
        }
        #endregion

        #region 支付结果通用通知
        //<xml><mch_id><![CDATA[1298638601]]></mch_id><openid><![CDATA[oES39thH2btDCM5i5RJDvze2l0ro]]></openid><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><appid><![CDATA[wx00dc3619bfb4ba00]]></appid><is_subscribe><![CDATA[Y]]></is_subscribe><sign><![CDATA[7D387FC1F6D80BA244B19A58838B8E54]]></sign><trade_type><![CDATA[JSAPI]]></trade_type><out_trade_no><![CDATA[1683742590]]></out_trade_no><time_end><![CDATA[20170515140258]]></time_end><cash_fee>1</cash_fee><total_fee>1</total_fee><bank_type><![CDATA[CMB_CREDIT]]></bank_type><fee_type><![CDATA[CNY]]></fee_type><transaction_id><![CDATA[4007042001201705150932666504]]></transaction_id><attach><![CDATA[a7bb2410-ec25-444c-aeaa-fa9561ddf5ce]]></attach><nonce_str><![CDATA[303ED4C69846AB36C2904D3BA8573050]]></nonce_str></xml>
        /// <summary>
        /// 支付结果通用通知
        /// </summary>
        /// <returns></returns>
        public ActionResult PayCallBack()
        {
            string openid = string.Empty;
            Guid orderSid = default(Guid);
            LogHelper.Debug("----------------------------------------------微信支付回来了----------------------------------------------------------------");
            ResponseHandler resHandler = new ResponseHandler(null);
            string res = null;
            resHandler.SetKey(WeixinConfig.TenPayV3_Key);
            //验证请求是否从微信发过来（安全）
            if (resHandler.IsTenpaySign())
            {
                LogHelper.Debug("支付回调数据:" + resHandler.ParseXML());

                openid = resHandler.GetParameter("openid");
                string return_code = resHandler.GetParameter("return_code");
                string return_msg = resHandler.GetParameter("return_msg");
                orderSid = Guid.Parse(resHandler.GetParameter("attach"));
                string transaction_id = resHandler.GetParameter("transaction_id");  //微信支付订单号
                string out_trade_no = resHandler.GetParameter("out_trade_no");      //商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一
                var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(orderSid);

                if (null != orderInfo)
                {
                    short payResult = KeyConst.ReVisit_OrderStatus_3;   // 订单状态:3:回调支付成功(这个是支付后微信回调post回来的支付数据，这才是真正意义上的支付成功)
                    if (!"SUCCESS".Equals(return_code, StringComparison.CurrentCultureIgnoreCase))
                    {
                        payResult = KeyConst.ReVisit_OrderStatus_4;  // 订单状态: 4：支付失败
                    }
                    var cres = BusinessContent.FZ_Order.Update("OrderSid", new { Status = payResult, PayRemark = return_msg, PayTime = DateTime.Now, Transaction_id = transaction_id, Out_trade_no = out_trade_no, EndTime = DateTime.Now.AddMinutes(ReVisitBusiness.Instance.OrderTimeOutMinutes) }, orderSid);
                    ReVisitBusiness.Instance.ClearOrderCache(orderSid); // 删除一个订单的缓存信息

                    BusinessContent.FZ_PayRecords.Insert(new FZ_PayRecordInfo { OrderSid = orderSid, Type = KeyConst.FZ_PayRecordType_1, Status = 1, PayResult = payResult, SerialNumber = orderInfo.OrderNo, WechatOrderId = transaction_id, PayOpenId = openid, Remark="支付回调" });
                    res = "FAIL";
                    if (cres > 0)
                    {
                        res = "success";
                    }
                }
                else
                {
                    UserActionEventLog(new UserActionEventLogInfo
                    {
                        AppId = 1,
                        OpenId = openid,
                        ActionType = KeyConst.Uceat_Event,
                        ActionName = "PayCallBack",
                        ActionDesc = "支付回调未找到订单信息:" + orderSid
                    });
                }
            }
            else
            {
                res = "fail";
            }
            UserActionEventLog(new UserActionEventLogInfo
            {
                AppId = 1,
                OpenId = openid,
                ActionType = KeyConst.Uceat_Event,
                ActionName = "PayCallBack",
                ActionDesc = orderSid + ",res=" + res
            });
            return Content(res);
        }

        #endregion

        #region 刷新一个订单的缓存信息
        /// <summary>
        /// 刷新一个订单的缓存信息
        /// </summary>
        /// <param name="orderSid"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult FlushOrder(Guid orderSid)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作完成" };
            try
            {
                rm.IsSuccess = true;
                ReVisitBusiness.Instance.ClearOrderCache(orderSid);  //删除订单缓存信息
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return MyJson(rm, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
