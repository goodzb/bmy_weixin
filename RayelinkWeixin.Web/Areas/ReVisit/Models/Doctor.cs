﻿using Newtonsoft.Json;

namespace RayelinkWeixin.Web.Areas.ReVisit.Models
{
    /// <summary>
    /// 接口返回的医生对象信息
    /// </summary>
    public class Doctor
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("pointDiagnosis")]
        public int PointDiagnosis { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("stampUrl")]
        public string StampUrl { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("deptName")]
        public string DeptName { get; set; }

        [JsonProperty("depName2")]
        public string DepName2 { get; set; }

        [JsonProperty("titleName")]
        public string TitleName { get; set; }

        [JsonProperty("orgName")]
        public string OrgName { get; set; }

        [JsonProperty("affiliatedHospital")]
        public string AffiliatedHospital { get; set; }

        [JsonProperty("skillIn")]
        public string SkillIn { get; set; }

        [JsonProperty("comments")]
        public int Comments { get; set; }

        [JsonProperty("consultations")]
        public int Consultations { get; set; }

        [JsonProperty("prizes")]
        public int Prizes { get; set; }

        [JsonProperty("messages")]
        public int Messages { get; set; }

        [JsonProperty("rating")]
        public int[] Rating { get; set; }

        [JsonProperty("remark")]
        public string Remark { get; set; }
    }
}