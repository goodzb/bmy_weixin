﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Web.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;

namespace RayelinkWeixin.Web.Areas.ReVisit.Chat
{
    /// <summary>
    /// FzChatHub基类
    /// </summary>
    public abstract class FzChatHubBase : Hub, IChatHub
    {
        #region 变量
        /// <summary>
        /// 当前sessionId
        /// </summary>
        protected string _connectionId = string.Empty;
        protected string _openId = string.Empty;
        protected Guid _orderSid = default(Guid);
        protected static System.Collections.Concurrent.ConcurrentDictionary<string, string> LastSession = new System.Collections.Concurrent.ConcurrentDictionary<string, string>();
        protected static System.Collections.Concurrent.ConcurrentDictionary<string, UserChat> SessionUserChat = new System.Collections.Concurrent.ConcurrentDictionary<string, UserChat>();
        protected const string LastSessionIdCacheKey = "SessionId_{0}_{1}";
        protected const string connectionIdTimeCacheKey = "connectionIdTimeCacheKey_{0}";
        protected const int CheckOnLineCount = 5;
        private static bool IsStart = false;

        /// <summary>
        /// 心跳检测间隔(毫秒)
        /// </summary>
        public const int HeartbeatInterval = 5000;
        private static Timer Timer;
        #endregion

        #region 打开/断开连接    

        /// <summary>
        /// 轮询监控在线的在线状态
        /// </summary>
        private void TimerMonitoring()
        {
            try
            {
                if (LastSession.Keys.Count > 0 && !IsStart)
                {
                    if (null == Timer)
                    {
                        Timer = new Timer();
                    }
                    Timer.Interval = HeartbeatInterval;
                    Timer.Start();
                    Timer.Elapsed += (sender, args) =>
                    {
                        IsStart = true;
                        foreach (var item in LastSession.Keys)
                        {
                            OnLineMonitoring(item, LastSession[item]);
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
        }


        /// <summary>
        /// 打开连接事件
        /// </summary>
        /// <returns></returns>
        public override Task OnConnected()
        {
            System.Diagnostics.Trace.WriteLine("客户端连接成功！");
            LoadParamer();  //加载参数    
            AddConnectionId();// 存储sessionId
            string openid = _openId;

            UserActionEventLogBusiness.Instance.Push(new Model.UserActionEventLogInfo { AppId = 1, OpenId = openid, ActionName = "OnConnected", ActionType = "Connected", Parameter = _connectionId });
            ReVisitBusiness.Instance.AddSessionLog(openid, _connectionId);  // 记录一次session变化行为(数据库记录)
            TimerMonitoring();
            return base.OnConnected();
        }

        /// <summary>
        /// 重写断开的事件
        /// </summary>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            LoadParamer();  //加载参数
            LogHelper.Debug(string.Format("开始注销:_openId={0},stopCalled={1}", _openId, stopCalled));
            if (!string.IsNullOrWhiteSpace(_openId))
            {
                RemoveConnectionId(_openId, "OnDisconnected"); // 移除session存储信息
            }
            return base.OnDisconnected(stopCalled);
        }
        #endregion

        #region 处理sessionId信息      

        /// <summary>
        /// 当前openid对象的sessionId
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        protected string LastConnectionId(string openid)
        {
            LoadParamer();  //加载参数     
            var connectionId = string.Empty;
            string cacheKey = SessionIdCacheKey(openid);   //获取一个sessionID缓存KEY
            connectionId = CacheHelper.GetCache(cacheKey) as string;
            if (string.IsNullOrWhiteSpace(connectionId))
            {
                LastSession.TryGetValue(cacheKey, out connectionId);
            }
            return connectionId;
        }

        /// <summary>
        /// 当前openid对象的sessionId
        /// </summary>
        protected string CurrentConnectionId
        {
            get
            {
                return LastConnectionId(_openId);
            }
        }

        /// <summary>
        /// 当前sessionID缓存KEY
        /// </summary>
        protected string CurrentSessionIdCacheKey
        {
            get
            {
                return SessionIdCacheKey(_openId);
            }
        }

        /// <summary>
        /// 获取一个sessionID缓存KEY
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="orderSid"></param>
        /// <returns></returns>
        private string SessionIdCacheKey(string openid)
        {
            return TextHelper.Md5ToLower(string.Format(LastSessionIdCacheKey, openid, _orderSid));
        }

        /// <summary>
        /// 存储sessionId
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="connectionId"></param>
        protected void AddConnectionId()
        {
            string openid = _openId, connectionId = _connectionId;
            string cacheKey = SessionIdCacheKey(openid);  // 获取一个sessionID缓存KEY

            LastSession[cacheKey] = LastSession.GetOrAdd(cacheKey, (x) => { return connectionId; });

            CacheHelper.SetCache(cacheKey, connectionId, 60 * 2);   //将openid对应的connectionId缓存起来       
        }

        /// <summary>
        /// 移除session存储信息
        /// </summary>
        /// <param name="openid"></param>
        void RemoveConnectionId(string openid, string remarks = "OnDisconnected")
        {
            string cacheKey = SessionIdCacheKey(openid);
            LastSession.TryRemove(cacheKey, out openid);
            CacheHelper.RemoveCache(cacheKey);

            ReVisitBusiness.Instance.ClearSessionCache(openid);  // 清空一个用户的session缓存信息
            ReVisitBusiness.Instance.AddSessionLog(openid, _connectionId, KeyConst.Session_Type_2, remarks);    // 记录一次session变化行为(数据库记录)
        }
        #endregion

        #region 加载页面参数
        /// <summary>
        /// 加载页面参数
        /// </summary>
        public void LoadParamer()
        {
            _connectionId = Context.ConnectionId;
            if (null != Context.QueryString["openid"])
            {
                _openId = Context.QueryString["openid"];
            }
            if (null != Context.QueryString["orderSid"])
            {
                _orderSid = Guid.Parse(Context.QueryString["orderSid"]);
            }
        }
        #endregion

        /// <summary>
        /// 给排除这发送消息
        /// </summary>
        /// <param name="excludeConnectionIds">排除者</param>
        /// <param name="message"></param>
        public void SendAllExceptMessage(string[] excludeConnectionIds, dynamic message)
        {
            Clients.AllExcept(excludeConnectionIds).ReceiveMessage(Context.ConnectionId, message);
        }

        /// <summary>
        /// 给所有者发送消息
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="message"></param>
        public void SendAllMessage(dynamic message)
        {
            Clients.All.ReceiveMessage(Context.ConnectionId, message);
        }

        /// <summary>
        /// 发送消息（谁发的消息就回谁）
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="message"></param>
        public void SendCallerMessage(dynamic message)
        {
            LoadParamer();  //加载参数    
            Clients.Caller.ReceiveMessage(_connectionId, message);
            Model.FZ_ConversationInfo messageInfo = message as Model.FZ_ConversationInfo;

            if (null != messageInfo)
            {
                ReVisitBusiness.Instance.AddConversationMessage(messageInfo);// 数据库增加一条会话消息记录
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="message"></param>
        public void SendChat(string id, string name, string message)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 一对一对话(指定Client发送消息)
        /// </summary>
        /// <param name="connectionId">接收者</param>
        /// <param name="message"></param>
        public void SendClientMessage(string connectionId, dynamic message)
        {
            LoadParamer();  //加载参数    
            Model.FZ_ConversationInfo messageInfo = message as Model.FZ_ConversationInfo;

            if (null != messageInfo)
            {
                ReVisitBusiness.Instance.AddConversationMessage(messageInfo);
            }
            Clients.Client(connectionId).ReceiveMessage(Context.ConnectionId, message);
        }

        /// <summary>
        /// 指定给多个用户发送消息
        /// </summary>
        /// <param name="connectionIds">接收消息者的connectionId</param>
        /// <param name="message">消息内容</param>
        public void SendClientsMessage(IList<string> connectionIds, dynamic message)
        {
            LoadParamer();  //加载参数
            Clients.Clients(connectionIds).ReceiveMessage(Context.ConnectionId, message);
            Model.FZ_ConversationInfo messageInfo = message as Model.FZ_ConversationInfo;

            if (null != messageInfo)
            {
                ReVisitBusiness.Instance.AddConversationMessage(messageInfo);

                CallBackMediaId(messageInfo);// 回调多媒体发送成功的结果
            }
        }

        /// <summary>
        /// 回执发送消息成功（谁发的消息就回谁）
        /// </summary>
        /// <param name="message"></param>
        public void SendSucessCallBack(dynamic message)
        {
            Clients.Caller.SendSucessCallBack(message);
        }

        /// <summary>
        /// 回调多媒体发送成功的结果(主要的目的是告诉发送者对方收到了，你可以讲文件下载到服务器上了)
        /// </summary>
        /// <param name="message"></param>
        protected void CallBackMediaId(Model.FZ_ConversationInfo message)
        {
            if (message.ChatType == KeyConst.FZ_Conversations_Chat_Type_1)
            {
                SendSucessCallBack(new { message.ChatType, message.MediaId, message.Id, Suffix = "jpg" });  //回执发送消息成功
            }
            else if (message.ChatType == KeyConst.FZ_Conversations_Chat_Type_2)
            {
                SendSucessCallBack(new { message.ChatType, message.MediaId, message.Id, Suffix = "amr" });  //回执发送消息成功
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void SendOthersMessage(string message)
        {
            Clients.Others.ReceiveMessage(Context.ConnectionId, message);
        }

        /// <summary>
        /// 通知用户下线(当用户的心跳检测到用户已经不在线时下线时执行)
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="connectionId"></param>
        public void SendLogoff(string openid, string connectionId, string remarks = "")
        {
            UserChat userInfo = null;
            SessionUserChat.TryGetValue(openid, out userInfo);
            if (userInfo != null)
            {
                SessionUserChat.TryRemove(openid, out userInfo);
            }
            RemoveConnectionId(openid, remarks);     //移除session存储信息
        }

        /// <summary>
        /// 接收前端发送的心跳包并做计数器处理,以便于判断用户是否断开连接
        /// （有时候用户直接关闭浏览器或者在任务管理器中关闭浏览器，是无法检测用户离线与否的，所以这里引入了心跳包机制，一旦用户在20秒之后未发送任何心跳包到后端，则视为掉线）。
        /// </summary>
        [HubMethodName("triggerHeartbeat")]
        public void TriggerHeartbeat()
        {
            LoadParamer();  //加载参数   
            AddConnectionId(); // 存储sessionId

            TriggerOnLineAction(_connectionId, string.Format("{0}检测到{1}在线,ConnectionId:{2},请保持.", DateTime.Now, _openId, _connectionId));
        }

        /// <summary>
        /// 心跳检测到还在线的执行函数
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="message"></param>
        public void TriggerOnLineAction(string connectionId, dynamic message)
        {
            var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(_orderSid);   // 从缓存里面获取一个订单的信息  
            Clients.Client(connectionId).TriggerOnLineAction(connectionId, message, orderInfo);
        }

        /// <summary>
        /// 用户上线通知
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="connectionId"></param>
        /// <param name="remarks"></param>
        public void SendLogin(string openid, string connectionId, string remarks = "")
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="message"></param>
        public void ReceiveMessage(string connectionId, dynamic message)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 发送校验一个connectionId的请求
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="connectionId"></param>      
        protected void OnLineMonitoring(string cacheKey, string connectionId)
        {
            string str = TextHelper.Md5ToLower(cacheKey + connectionId + _orderSid);
            try
            {
                if (string.IsNullOrWhiteSpace(connectionId)) return;
                LogHelper.Debug(string.Format("发送校验一个connectionId的请求:cacheKey={0},connectionId={1}", cacheKey, connectionId));
                if (CheckOnLine(connectionId, cacheKey))
                {
                    string key = string.Format(connectionIdTimeCacheKey, connectionId);
                    int count = CacheHelper.GetCache<int>(key) + 1;

                    CacheHelper.SetCache(key, count, 1);
                    var res = Clients.Client(connectionId).OnLineMonitoring(str, connectionId);
                }
                //var task = res as Task<object>;
                //if (null != task && task.Status.Equals(TaskStatus.RanToCompletion))
                //{

                //}               
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 接受在线校验的回应
        /// </summary>
        [HubMethodName("onLineMonitoringResponse")]
        public void OnLineMonitoringResponse(string text, string connectionId)
        {
            LoadParamer();  //加载参数
            AddConnectionId(); // 存储sessionId
            string str = text;
            string key = string.Format(connectionIdTimeCacheKey, _connectionId);
            CacheHelper.RemoveCache(key);
            LogHelper.Debug(string.Format("接受在线校验的回应:text={0},connectionId={1},key={2}", text, _connectionId, key));
        }

        /// <summary>
        /// 校验一个connectionId是否是在线的
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="cacheKey">这个可以是openid与ordersid加密后的值</param>
        /// <param name="isLogout"></param>
        /// <returns>true在线 false离线</returns>
        protected bool CheckOnLine(string connectionId, string cacheKey = "", bool isLogout = true)
        {
            bool result = false;
            if (string.IsNullOrWhiteSpace(connectionId)) return result;

            string key = string.Format(connectionIdTimeCacheKey, connectionId);
            int count = CacheHelper.GetCache<int>(key);
            result = CheckOnLineCount > count;
            LogHelper.Debug(string.Format("校验一个connectionId是否是在线的:connectionId={0},count={1},key={2}", connectionId, count, key));
            if (!result && isLogout)  //超时
            {
                LogHelper.Debug(string.Format("开始注销:connectionId={0},count={1},cacheKey={2}", connectionId, count, cacheKey));
                if (!string.IsNullOrEmpty(cacheKey))
                {
                    string openid = string.Empty;
                    LastSession.TryRemove(cacheKey, out openid);
                    CacheHelper.RemoveCache(cacheKey);
                    var se = BusinessContent.SessionLog.FindOne(sqls: "ConnectionId=@0", arg: connectionId);
                    if (null != se)
                    {
                        openid = se.OpenId;
                        LogHelper.Debug(string.Format("找到session:connectionId={0},count={1},cacheKey={2},openid={3}", connectionId, count, cacheKey, openid));

                        ReVisitBusiness.Instance.ClearSessionCache(openid);  // 清空一个用户的session缓存信息
                        ReVisitBusiness.Instance.AddSessionLog(openid, connectionId, KeyConst.Session_Type_2, "轮询关闭");    // 记录一次session变化行为(数据库记录)
                    }
                }
                else
                {
                    LogHelper.Debug(string.Format("本身处理,connectionId={0}", connectionId));
                    OnDisconnected(true);
                }
            }
            return result;
        }
    }
}