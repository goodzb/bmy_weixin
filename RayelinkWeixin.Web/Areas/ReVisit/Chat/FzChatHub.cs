﻿using System;
using Microsoft.AspNet.SignalR.Hubs;
using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Const;
using System.Collections.Generic;
using System.Linq;

namespace RayelinkWeixin.Web.Areas.ReVisit.Chat
{
    /// <summary>
    /// 
    /// </summary>
    [HubName("fzChatHub")]
    public class FzChatHub : FzChatHubBase
    {
        #region 用户接受到医生/助理发送的消息
        /// <summary>
        /// 
        /// </summary>
        private void AllSession()
        {
            try
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                foreach (var item in LastSession.Keys)
                {
                    sb.AppendFormat("Key:{0} value:{1}", item, LastSession[item]);
                    sb.Append(Environment.NewLine);
                }
                LogHelper.Debug("所有用户信息:" + Environment.NewLine + sb.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
        }
        /// <summary>
        /// 用户接受到医生/助理发送的消息
        /// </summary>
        /// <param name="message">消息对象</param>
        /// <param name="doctorOpenId">医生openid</param>
        /// <param name="reVisitOpenId">助理openid</param>
        /// <param name="reVisitId">助理ID</param>
        /// <param name="userName">订单用户名称</param>
        /// <param name="orderConversationStatus">当前订单的会话状态</param>
        [HubMethodName("receiverMessageByDoctor")]
        public void ReceiverMessageByDoctor(Model.FZ_ConversationInfo message, string doctorOpenId, int reVisitId, string reVisitOpenId, string userName, short orderConversationStatus)
        {
            LogHelper.Debug("用户接受到医生/助理发送的消息:" + message.Content);
            AllSession();

            message.Source = KeyConst.FZ_Conversations_Source_1;        // 会话消息来源：1-医生发送
            message.CreateTime = DateTime.Now;
            message.Status = 1;
            LoadParamer();  //加载参数

            string userConnectionId = LastConnectionId(message.ReceiverOpenId);  //用户的session
            message.SenderConnectionId = _connectionId;
            ReVisitBusiness.Instance.ClearRemindTempCountCache(message.ReceiverOpenId, doctorOpenId, message.OrderSid);  //医生/助理消息后把用户累计说话的缓存次数要清理掉

            if (orderConversationStatus == KeyConst.ReVisit_ConversationStatus_1) // 会话状态：1-医生未回复
            {
                ReVisitBusiness.Instance.ChangeOrderConversationStatus(message.ReceiverOpenId, message.OrderSid, orderConversationStatus, KeyConst.ReVisit_ConversationStatus_2);  //修改为用户未回复
            }

            bool onLine = CheckOnLine(userConnectionId);
            LogHelper.Debug(string.Format("用户接受到医生/助理发送的消息:userConnectionId:{0},在线状态：{1}", userConnectionId, onLine));

            if (!string.IsNullOrWhiteSpace(userConnectionId) && onLine)
            {
                message.ReceiverConnectionId = userConnectionId;
                SendClientMessage(userConnectionId, message);   // 一对一对话(指定Client发送消息)             
            }
            else  //发送离线通知消息
            {
                ReVisitBusiness.Instance.AddConversationMessage(message);// 数据库增加一条会话消息记录

                string msg = message.Content;
                if (message.ChatType == KeyConst.FZ_Conversations_Chat_Type_0)
                {
                    if (msg.Length > 10)
                    {
                        msg = msg.Remove(10) + "...";
                    }
                    msg = "专家已针对您的问题作出回复" + Environment.NewLine + msg;
                }
                else
                {
                    msg = "专家已针对您的问题作出答复";
                }
                msg = msg + Environment.NewLine + string.Format("<a href='{0}ReVisit/ReVisit/Chat?openid={1}&orderSid={2}'>点这里立即查看</a>", TextHelper.BaseUrl(), message.ReceiverOpenId, message.OrderSid);

                ReVisitBusiness.Instance.SendCustomText(KeyConst.OrderAppId, message.ReceiverOpenId, msg);   //给用户发消息提醒
            }
        }
        #endregion

        #region 接收到用户发来的消息

        /// <summary>
        /// 接收到用户发来的消息
        /// </summary>
        /// <param name="message">消息对象</param>
        /// <param name="doctorOpenId">医生openid</param>
        /// <param name="reVisitOpenId">助理openid</param>
        /// <param name="reVisitId">助理ID</param>
        /// <param name="userName">订单用户名称</param>
        /// <param name="orderConversationStatus">当前订单的会话状态</param>
        [HubMethodName("receiverMessageByUser")]
        public void ReceiverMessageByUser(Model.FZ_ConversationInfo message, string doctorOpenId, int reVisitId, string reVisitOpenId, string userName, short orderConversationStatus)
        {
            LogHelper.Debug("接收到用户发来的消息");
            AllSession();
            message.Source = KeyConst.FZ_Conversations_Source_0;// 会话消息来源：0-用户发送
            message.CreateTime = DateTime.Now;
            message.Status = 1;
            LoadParamer();  //加载参数  
            List<string> connectionIds = new List<string>();
            string doctorConnectionId = LastConnectionId(doctorOpenId);     //获取医生的session
            string reVisitConnectionId = LastConnectionId(reVisitOpenId);   //获取助理的session

            #region 处理医生/助理的sessionid
            if (!string.IsNullOrWhiteSpace(doctorConnectionId))
            {
                connectionIds.Add(doctorConnectionId);
            }
            if (!string.IsNullOrWhiteSpace(reVisitConnectionId))
            {
                connectionIds.Add(reVisitConnectionId);
            }
            message.ReceiverConnectionId = doctorConnectionId;
            message.SenderConnectionId = _connectionId;

            LogHelper.Debug(string.Format("接收到用户发来的消息:reVisitConnectionId:{0},reVisitConnectionId:{1}", doctorConnectionId, reVisitConnectionId));

            if (string.IsNullOrWhiteSpace(message.ReceiverConnectionId) && !string.IsNullOrWhiteSpace(reVisitConnectionId))
            {
                message.ReceiverConnectionId = reVisitConnectionId;
                message.ReceiverOpenId = reVisitOpenId;
            }
            #endregion         

            if (orderConversationStatus == KeyConst.ReVisit_ConversationStatus_2) // 会话状态：1-用户未回复
            {
                ReVisitBusiness.Instance.ChangeOrderConversationStatus(message.ReceiverOpenId, message.OrderSid, orderConversationStatus, KeyConst.ReVisit_ConversationStatus_7);   //修改为正在会话中
            }

            if (connectionIds.Any())
            {
                SendClientsMessage(connectionIds, message);// 指定给多个用户发送消息
            }
            else
            {
                ReVisitBusiness.Instance.AddConversationMessage(message);// 数据库增加一条会话消息记录             

                string msg = string.Empty;
                if (message.ChatType == KeyConst.FZ_Conversations_Chat_Type_0)
                {
                    msg = message.Content;
                    if (msg.Length > 100)
                    {
                        msg = msg.Remove(100) + "...";
                    }
                }
                else if (message.ChatType == KeyConst.FZ_Conversations_Chat_Type_2)
                {
                    msg = "语音";
                }
                else if (message.ChatType == KeyConst.FZ_Conversations_Chat_Type_1)
                {
                    msg = "图片";
                }
                //发送离线消息模板通知
                ReVisitBusiness.Instance.SendRemindTemp(KeyConst.OrderAppId,new string[] { doctorOpenId }, userName, string.Format("{0}ReVisit/ReVisit/Interrogate?orderSid={1}&pageSource=1&openid={2}", TextHelper.BaseUrl(), message.OrderSid, doctorOpenId), msg);

                ReVisitBusiness.Instance.SendRemindTemp(KeyConst.OrderAppId,new string[] { reVisitOpenId }, userName, string.Format("{0}ReVisit/ReVisit/Referral?orderSid={1}&pageSource=2&openid={2}", TextHelper.BaseUrl(), message.OrderSid, reVisitOpenId), msg);
            }
            if (ReVisitBusiness.Instance.CheckRemindTempFull(message.SenderOpenId, doctorOpenId, message.OrderSid))     //校验用户连续发送消息而医生助理都没有回复是否超过10条
            {
                var newMessage = message;
                newMessage.ChatType = KeyConst.FZ_Conversations_Chat_Type_0;
                newMessage.Source = KeyConst.FZ_Conversations_Source_4;        // 会话消息来源：3-系统发送（医生身份）
                newMessage.Content = "您好，请耐心等待。 有问题 ，您可以现在这里提问，专家空余时会给您答复。";
                newMessage.TipContent = "提示：专家太忙，医助传达";
                SendCallerMessage(newMessage); // 发送消息（谁发的消息就回谁）
            }
            //CallBackMediaId(message);// 回调多媒体发送成功的结果
        }
        #endregion

        #region 连接成功后自动展示用户填写描述信息
        /// <summary>
        /// 连接成功后自动展示用户填写描述信息
        /// </summary>
        /// <param name="message">消息对象</param>
        [HubMethodName("showOrderDescription")]
        public void ShowOrderDescription(Model.FZ_ConversationInfo message)
        {
            string receiverConnectionId = string.Empty;
            try
            {
                LoadParamer();  //加载参数     
                string fromOpenId = message.SenderOpenId;   // 发送者OpenId
                message.SenderConnectionId = _connectionId;
                string doctorOpenId = message.ReceiverOpenId;  //医生的openid

                #region 在前端固定，不在这里处理了
                //receiverConnectionId = LastConnectionId(message.ReceiverOpenId);  //获取医生的session
                //string reVisitOpenId = string.Empty;  //助理openid
                //message.Source = KeyConst.FZ_Conversations_Source_0;
                //var orderInfo = ReVisitBusiness.Instance.OrderInfoBySid(message.OrderSid);
                //message.Content = orderInfo.UserName + " " + (orderInfo.Gender == 1 ? "男" : "女") + orderInfo.ProvinceName + orderInfo.CityName + orderInfo.DistrictName + orderInfo.Address + Environment.NewLine;
                //message.Content += "主刀医生:"+ orderInfo.DoctorName + Environment.NewLine;
                //message.Content += "复诊咨询医生:" + orderInfo.DoctorName + Environment.NewLine;
                //message.Content += "病例描述:" + orderInfo.Description ;
                //if (string.IsNullOrEmpty(receiverConnectionId))  //为空说明医生是不在线的，去找医生的助理
                //{
                //    var reVisitUser = ReVisitBusiness.Instance.GetReVisitUserByDoctorId(message.ReceiverUserId);// 根据医生ID获取对应的助理信息

                //    if (null != reVisitUser)
                //    {
                //        reVisitOpenId = reVisitUser.OpenId;
                //        receiverConnectionId = LastConnectionId(reVisitUser.OpenId);  //获取助理的session
                //        if (!string.IsNullOrEmpty(receiverConnectionId))
                //        {
                //            message.ReceiverUserId = reVisitUser.Id;
                //            message.ReceiverOpenId = reVisitUser.OpenId;
                //            message.ReceiverConnectionId = receiverConnectionId;
                //        }
                //    }
                //}
                //if (!string.IsNullOrEmpty(receiverConnectionId))   
                //{
                //    SendClientMessage(receiverConnectionId, message);// 一对一对话(指定Client发送消息)
                //}
                //else    //发送离线消息提醒
                //{
                //    ReVisitBusiness.Instance.SendOrderDescription(new string[] { doctorOpenId }, message.OrderSid, string.Format("ReVisit/ReVisit/Chat?orderSid={0}&pageSource=1", message.OrderSid));

                //    ReVisitBusiness.Instance.SendOrderDescription(new string[] { reVisitOpenId }, message.OrderSid, string.Format("ReVisit/ReVisit/Chat?orderSid={0}&pageSource=2", message.OrderSid));

                //    ReVisitBusiness.Instance.AddConversationMessage(message);// 数据库增加一条会话消息记录
                //} 
                #endregion

                ReVisitBusiness.Instance.ChangeOrderConversationStatus(_openId, message.OrderSid, KeyConst.ReVisit_ConversationStatus_0, KeyConst.ReVisit_ConversationStatus_1);  //修改订单的会话状态       
            }
            catch (Exception ex)
            {
                message.Source = KeyConst.FZ_Conversations_Source_10;
                message.Content = ex.Message;
                message.TipContent = "提示：系统错误";
                SendCallerMessage(message);     // 发送消息（谁发的消息就回谁）
                LogHelper.Exception(ex);
            }
        }
        #endregion

        #region 给用户发送订单结束咨询的通知
        /// <summary>
        /// 给用户发送订单结束咨询的通知
        /// </summary>
        /// <param name="message"></param>
        /// <param name="doctorOpenId"></param>
        /// <param name="reVisitId"></param>
        /// <param name="reVisitOpenId"></param>
        [HubMethodName("overOrder")]
        public void OverOrder(Model.FZ_ConversationInfo message, string doctorOpenId, int reVisitId, string reVisitOpenId)
        {
            LoadParamer();  //加载参数
            message.Source = KeyConst.FZ_Conversations_Source_4;        // 会话消息来源：4-系统发送，在用户展示页面（医生身份）
            message.CreateTime = DateTime.Now;
            message.Status = 1;
            message.OrderSid = _orderSid;
            message.ChatType = KeyConst.FZ_Conversations_Chat_Type_0;
            message.MediaId = "";
            message.SenderConnectionId = _connectionId;
            message.SenderOpenId = _openId;
            string userConnectionId = LastConnectionId(message.ReceiverOpenId);  //用户的session

            ReVisitBusiness.Instance.ChangeTotalReplyCount(_openId, doctorOpenId);  // 累计医生的咨询数量

            if (!string.IsNullOrWhiteSpace(userConnectionId))// 用户在线
            {
                message.ReceiverConnectionId = userConnectionId;
                message.Content = "您好，本次咨询已结束，如需再次咨询，请重新购买本服务";
                message.TipContent = "提示：医助传达";
                Clients.Client(userConnectionId).ReceiveOverOrder(Context.ConnectionId, message);
                ReVisitBusiness.Instance.AddConversationMessage(message);  //数据库记录这次消息
            }
            DortorOverMessage(message); // 给医生结束的消息记录
        }

        /// <summary>
        /// 给医生结束的消息记录
        /// </summary>
        /// <param name="oldMessage"></param>
        private void DortorOverMessage(Model.FZ_ConversationInfo oldMessage)
        {
            SendCallerMessage(new Model.FZ_ConversationInfo  //// 发送消息（谁发的消息就回谁）
            {
                OrderSid = oldMessage.OrderSid,
                Source = KeyConst.FZ_Conversations_Source_5,  //5-系统发送，在医生展示页面
                ChatType = KeyConst.FZ_Conversations_Chat_Type_0,
                Status = 1,
                SenderUserId = oldMessage.SenderUserId,
                SenderOpenId = oldMessage.SenderOpenId,
                SenderConnectionId = oldMessage.SenderConnectionId,

                ReceiverUserId = oldMessage.SenderUserId,
                ReceiverOpenId = oldMessage.SenderOpenId,
                ReceiverConnectionId = oldMessage.SenderConnectionId,

                CreateTime = DateTime.Now,
                Content = "您已结束本次咨询服务"
            });
        }
        #endregion

    }
}