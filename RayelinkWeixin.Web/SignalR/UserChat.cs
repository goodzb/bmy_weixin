﻿using System;
using System.Timers;

namespace RayelinkWeixin.Web.SignalR
{
    /// <summary>
    /// 
    /// </summary>
    public class UserChat : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        //private readonly Timer Timer;

        /// <summary>
        /// 设置心跳检测累计不在线的次数，超过后就进行下线处理
        /// </summary>
        public readonly int HeartbeatCount = 10;

        /// <summary>
        /// 心跳检测间隔(毫秒)
        /// </summary>
        public const int HeartbeatInterval = 20000;


        /// <summary>
        /// 执行心跳监测函数
        /// </summary>
        //public event Action<string, string, int> TriggerHeartbeatAction;

        /// <summary>
        /// 
        /// </summary>
        public string ConnectionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OpenId { get; set; }

        //内部计数器（每次递增1），如果服务端每5s能收到客户端的心跳包，那么count被重置为0；
        //如果服务端20s后仍未收到客户端心跳包，那么视为掉线
        public int Count { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public UserChat()
        {
            Count = 0;
            //Timer = Timer ?? new Timer();
            //Timer.Interval = HeartbeatInterval;
            //Timer.Start();
            //Timer.Elapsed += (sender, args) =>
            //{
            //    TriggerHeartbeatAction(OpenId, ConnectionId, HeartbeatCount);
            //};
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose() // NOT virtual
        {
            Dispose(true);
            GC.SuppressFinalize(this); // Prevent finalizer from running.
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Call Dispose() on other objects owned by this instance.
                // You can reference other finalizable objects here.
                // ...
            }
            // Release unmanaged resources owned by (just) this object.
            // ...
        }
    }
}