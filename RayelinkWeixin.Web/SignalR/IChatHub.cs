﻿
using System.Collections.Generic;

namespace RayelinkWeixin.Web.SignalR
{
    interface IChatHub
    {
        /// <summary>
        /// 服务器下发消息到各个客户端
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="connectionId"></param>
        /// <param name="message"></param>
        void SendChat(string openid, string connectionId, string message);

        /// <summary>
        /// 用户上线通知
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="connectionId"></param>
        /// <param name="remarks"></param>
        void SendLogin(string openid, string connectionId, string remarks = "");

        /// <summary>
        /// 用户下线通知
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="connectionId"></param>
        void SendLogoff(string openid, string connectionId, string remarks = "");

        /// <summary>
        /// 接收客户端发送的心跳包并处理
        /// </summary>     
        void TriggerHeartbeat();

        /// <summary>
        /// 发送消息（谁发的消息就回谁）
        /// </summary>
        /// <param name="message">消息内容或者一个FZ_ConversationInfo对象</param>
        void SendCallerMessage(dynamic message);

        /// <summary>
        ///  一对一对话(指定Client发送消息)
        /// </summary>
        /// <param name="to"></param>
        /// <param name="message">消息内容或者一个FZ_ConversationInfo对象</param>
        void SendClientMessage(string to, dynamic message);

        /// <summary>
        /// 指定给多个用户发送消息
        /// </summary>
        /// <param name="connectionIds">接收消息者的connectionId</param>
        /// <param name="message">消息内容或者一个FZ_ConversationInfo对象</param>
        void SendClientsMessage(IList<string> connectionIds, dynamic message);

        /// <summary>
        /// 加载参数信息
        /// </summary>
        void LoadParamer();

        /// <summary>
        /// 向所有在线用户发送消息
        /// </summary>
        /// <param name="message">消息内容或者一个FZ_ConversationInfo对象</param>
        void SendAllMessage(dynamic message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excludeConnectionIds"></param>
        /// <param name="message">消息内容或者一个FZ_ConversationInfo对象</param>
        void SendAllExceptMessage(string[] excludeConnectionIds, dynamic message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">消息内容或者一个FZ_ConversationInfo对象</param>
        void SendOthersMessage(string message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="message">消息内容或者一个FZ_ConversationInfo对象</param>
        void ReceiveMessage(string connectionId, dynamic message);

        /// <summary>
        /// 心跳检测到还在线的执行函数
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="message">消息内容或者一个FZ_ConversationInfo对象</param>
        void TriggerOnLineAction(string connectionId, dynamic message);
    }
}
