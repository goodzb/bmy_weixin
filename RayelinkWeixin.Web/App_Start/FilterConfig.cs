﻿
using RayelinkWeixin.Web.Filters;
using System.Web.Mvc;

namespace RayelinkWeixin.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filters"></param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //自定义错误过滤器
            filters.Add(new WeixinErrorAttribute());
            //权限过滤器
            //filters.Add(new WebAuthorizeAttribute());
        }
    }
}