﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web;
using System.Web.SessionState;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using Newtonsoft.Json;

namespace RayelinkWeixin.Auth
{
    /// <summary>
    /// 用户认证类
    /// </summary>
    public abstract class Authorize : IReadOnlySessionState, IRequiresSessionState
    {
        #region 变量
        /// <summary>
        /// 
        /// </summary>
        public const string USER_SESSION_KEY = "authorize_user";
        /// <summary>
        /// 
        /// </summary>
        public const string Employee_SESSION_KEY = "authorize_employee";
        /// <summary>
        /// 
        /// </summary>
        public const string CARDNO_SESSION_KEY = "authorize_user_cardno";

        /// <summary>
        /// COOKIE_LOGON
        /// </summary>
        public const string COOKIE_LOGON = "userlogoninfo";

        #endregion

        #region 登录
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="accountName"></param>
        public static void SignIn(string accountName)
        {
            FormsAuthentication.SetAuthCookie(accountName, false);
        }
        #endregion

        #region 注销
        /// <summary>
        /// 注销
        /// </summary>
        public static void SignOut()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Remove(USER_SESSION_KEY);
            CookieHelper.WriteEncryptCookie(COOKIE_LOGON, "email", "", -10);
            CookieHelper.WriteEncryptCookie(COOKIE_LOGON, "pwd", "", -10);
        }
        #endregion

        #region 当前登录的用户

        /// <summary>
        /// 当前登录的用户
        /// </summary>
        public static AccountInfo CurrentUser
        {
            get
            {
                AccountInfo user = null;

                //如果已经身份验证通过
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    if (HttpContext.Current.Session[USER_SESSION_KEY] == null)
                    {
                        user = HttpContext.Current.Session[USER_SESSION_KEY] as AccountInfo;

                        if (HttpContext.Current.User.Identity.IsAuthenticated)
                        {
                            if (!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
                            {
                                try
                                {
                                    user = BusinessContent.Account.FindOne(where: string.Format("AccountEmail='{0}'", HttpContext.Current.User.Identity.Name));

                                    HttpContext.Current.Session[USER_SESSION_KEY] = user;
                                }
                                catch (Exception e)
                                {
                                    //判断是否有这个人.就是说,这个账户有可能已经被禁用或者从别的地方转过来的
                                    FormsAuthentication.SignOut();
                                    HttpContext.Current.Session.Remove(USER_SESSION_KEY);
                                    new ExceptionHelper().LogException(e);

                                }
                            }
                            else
                            {
                                FormsAuthentication.SignOut();
                                HttpContext.Current.Session.Remove(USER_SESSION_KEY);
                            }
                        }
                    }
                    else
                    {
                        if (string.Equals(((AccountInfo)HttpContext.Current.Session[USER_SESSION_KEY]).Email,
                            HttpContext.Current.User.Identity.Name, StringComparison.InvariantCultureIgnoreCase))
                        {
                            user = HttpContext.Current.Session[USER_SESSION_KEY] as AccountInfo;
                        }
                        else if (HttpContext.Current.Session[USER_SESSION_KEY] != null)
                        {
                            user = HttpContext.Current.Session[USER_SESSION_KEY] as AccountInfo;
                        }
                        else
                        {
                            HttpContext.Current.Session.Remove(USER_SESSION_KEY);
                            //刚注销的用户换账户登陆
                            user = CurrentUser;
                        }
                    }
                }

                //单点登陆的主要实现代码
                if (user != null)
                {
                    string _cookieAccessToken = CookieHelper.ReadEncryptCookie(COOKIE_LOGON, "AccessToken");
                    user.AccessToken = _cookieAccessToken;
                    string host = HttpContext.Current.Request.Url.Host;
                    string accessToken = CacheHelper.GetCache<string>((string.Format("{0}_{1}", host, user.Email)));
                    if (user.AccessToken != accessToken)
                    {
                        SignOut();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/account/Logout");
                }
                return user;
            }
            set
            {
                //每次登陆都修改AccessToken,单点登陆的主要实现代码
                IList<string> logs = new List<string>();
                if (value != null)
                {
                    string host = HttpContext.Current.Request.Url.Host;
                    string _cookieAccessToken = CookieHelper.ReadEncryptCookie(COOKIE_LOGON, "AccessToken");
                    string _redisAccessToken = CacheHelper.GetCache<string>(string.Format("{0}_{1}", host, value.Email));
                    logs.Add("host:" + host);
                    logs.Add("_cookieAccessToken:" + _cookieAccessToken);
                    logs.Add("_redisAccessToken:" + _redisAccessToken);

                    string accessToken = Guid.NewGuid().ToString();
                    if (!_cookieAccessToken.Equals(_redisAccessToken))
                    {
                        CacheHelper.SetCache(string.Format("{0}_{1}", host, value.Email), accessToken);
                        CookieHelper.WriteEncryptCookie(COOKIE_LOGON, "AccessToken", accessToken);
                    }
                    else
                    {
                        accessToken = _cookieAccessToken;
                    }
                    logs.Add("AccountAccessToken:" + accessToken);
                    value.AccessToken = accessToken;
                }
                FileLogHelper.Instance.Log(logs, "CurrentUser_");
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.Session[USER_SESSION_KEY] = value;
                }

            }
        }
        #endregion

        #region 当前登陆用户选择的appID
        /// <summary>
        /// 当前登陆用户选择的appID
        /// </summary>
        public static int CurrentAppID
        {
            get
            {
                int appid = 1;
                if (CurrentUser != null)
                {
                    appid = CacheHelper.GetCache<int>(string.Format("{0}_appid", CurrentUser.Email));

                    if (appid > 0)
                    {
                        CacheHelper.SetCache(string.Format("{0}_appid", CurrentUser.Email), appid, 10 * 60);
                    }
                }
                else
                {
                    //重定向至登录页面
                    HttpContext.Current.Response.Redirect("~/account/logon");
                }
                return appid;
            }
            set
            {
                if (CurrentUser != null)
                {
                    CacheHelper.SetCache(string.Format("{0}_appid", CurrentUser.Email), value, 10 * 60);
                }
            }
        }
        #endregion

        #region 当前登陆用户选择的App对象
        /// <summary>
        /// 当前登陆用户选择的App对象
        /// </summary>
        public static AppInfo CurrentAppInfo
        {
            get
            {
                AppInfo appInfo = null;
                var user = CurrentUser;
                if (user != null)
                {
                    var cache = CacheHelper.GetCache(string.Format("{0}_AppInfo", user.Email));
                    if (null != cache)  //缓存中有数据
                    {
                        appInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<AppInfo>(cache.ToString());
                        if (appInfo == null)
                        {
                            appInfo = BusinessContent.App.GetFormDataInfoByIdentityKey(CurrentAppID);
                            CacheHelper.SetCache(string.Format("{0}_AppInfo", user.Email), JsonConvert.SerializeObject(appInfo), 10 * 60);
                        }
                    }
                    else   //从缓存中解析
                    {
                        if (appInfo == null)
                        {
                            appInfo = BusinessContent.App.GetFormDataInfoByIdentityKey(CurrentAppID);
                            CacheHelper.SetCache(string.Format("{0}_AppInfo", user.Email), JsonConvert.SerializeObject(appInfo), 10 * 60);
                        }
                    }
                }
                return appInfo;
            }
        }
        #endregion

        #region IsWeChatAdmin
        /// <summary>
        /// IsWeChatAdmin
        /// </summary>
        public static bool IsWeChatAdmin
        {
            get
            {
                bool result = false;

                AppConfigInfo aci = WeChatAdmin();

                if (aci != null && aci.Value.Split(",;".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Contains(CurrentUser.AccountId.ToString()))
                {
                    result = true;
                }

                return result;
            }
        }
        #endregion

        #region WeChatAdmin
        /// <summary>
        /// WeChatAdmin
        /// </summary>
        public static AppConfigInfo WeChatAdmin(bool force = false)
        {
            string key = string.Format("0_WeChatAdmin");

            AppConfigInfo result = null;
            string WeChatAdminString = CacheHelper.GetCache<string>(key);
            if (!string.IsNullOrWhiteSpace(WeChatAdminString))
            {
                result = JsonConvert.DeserializeObject<AppConfigInfo>(WeChatAdminString);
            }
            if (result == null || force)
            {
                result = BusinessContent.AppConfig.FindOne("[Key]=@0 and Name=@1 and AppID=@2", "WeChatAdmin", "系统维护管理员", 0);
                if (result != null)
                {
                    CacheHelper.SetCache(key, Newtonsoft.Json.JsonConvert.SerializeObject(result), 10 * 60);
                }
            }
            return result;
        }
        #endregion

    }
}
