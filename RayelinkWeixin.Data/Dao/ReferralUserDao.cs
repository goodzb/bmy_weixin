﻿using Dapper;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Dao
{
    public class ReferralUserDao : BaseDao<ReferralUserExtend>
    {
        private static ReferralUserDao _instance;
        public static ReferralUserDao Instance
        {
            get
            {
                if (_instance==null)
                {
                    _instance = new ReferralUserDao();
                }
                return _instance;
            }
        }

        public override int Add(ReferralUserExtend entity)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = "Insert into dbo.Referral_User(UnionId,Type,Name,Phone,OpenId,HospitalId)Values(@UnionId,@Type,@Name,@Phone,@OpenId,@HospitalId);select @@identity";
                return conn.Execute(sql, entity);
            }
        }

        public override int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralUserExtend> FindAll()
        {
            throw new NotImplementedException();
        }

        public override ReferralUserExtend FindById(int id)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = "select * from dbo.Referral_User where Id=@Id";
                return conn.QueryFirstOrDefault<ReferralUserExtend>(sql, new { Id = id });
            }
        }

        public ReferralUserExtend FindByOpenId(string openid)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "select * from dbo.Referral_User where OpenId=@openid";
                return conn.QueryFirstOrDefault<ReferralUserExtend>(sql, new { openid = openid });
            }
        }

        public override List<ReferralUserExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralUserExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public override int Update(ReferralUserExtend entity)
        {
            throw new NotImplementedException();
        }

        public override int Update(string strField, string strWhere)
        {
            throw new NotImplementedException();
        }
    }
}
