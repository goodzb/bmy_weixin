﻿using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using RayelinkWeixin.Common.Utilites;

namespace RayelinkWeixin.Data.Dao
{
    public class ReferralDao : BaseDao<ReferralExtend>
    {
        private static ReferralDao _instance;
        public static ReferralDao Instance
        {
            set { _instance = value; }
            get
            {
                if (_instance == null)
                {
                    _instance = new ReferralDao();
                }
                return _instance;
            }
        }

        #region Basic
        public override int Add(ReferralExtend entity)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "INSERT INTO [dbo].[Referral]([Name] ,[Phone],[Desc],[ClientId] ,[DocId] ,[Status] ,[AppointmentNo] ,[CreaterId] ,[CreateTime] ,[IsDel],[CreaterName],[CreaterPhone],[Sex],[Age],[AppointmentTime],[DocBrif],[ClientName],[CreaterOpenId],[NurseOpenId],[LastUpdateTime]) VALUES(@Name,@Phone,@Desc,@ClientId,@DocId,@Status,@AppointmentNo,@CreaterId,@CreateTime,@IsDel,@CreaterName,@CreaterPhone,@Sex,@Age,@AppointmentTime,@DocBrif,@ClientName,@CreaterOpenId,@NurseOpenId,getdate());SELECT @@identity;";

                return conn.ExecuteScalar<int>(query, entity);
            }
        }

        public override int Delete(int id)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "UPDATE [dbo].[Referral] SET IsDel=1 WHERE ID=@id";
                return conn.Execute(query, new { id = id });
            }
        }

        public override List<ReferralExtend> FindAll()
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "SELECT * FROM [dbo].[Referral] WHERE IsDel=0";
                return conn.Query<ReferralExtend>(query).ToList();
            }
        }

        public override ReferralExtend FindById(int id)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "SELECT * FROM [dbo].[Referral] WHERE Id=@id and IsDel=0";
                return conn.Query<ReferralExtend>(query, new { id = id }).SingleOrDefault();
            }
        }

        public override List<ReferralExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            string where = "IsDel=0", orderby = " Id desc";
            if (!string.IsNullOrEmpty(strWhere))
            {
                where += (" AND " + strWhere);
            }
            if (!string.IsNullOrEmpty(strOrderBy))
            {
                orderby = strOrderBy;
            }
            int end = pagesize * pageindex;
            int start = pagesize * (pageindex - 1) + 1;
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = @"SELECT * FROM(
                                 SELECT ROW_NUMBER() OVER(ORDER BY r.LastUpdateTime Desc) AS rowNum, r.*
                                ,(select count(0) from dbo.Referral_Read where RefId=r.Id) as IsRead
                                FROM dbo.Referral r
                                 WHERE " + strWhere + @"
                                 )AS temp
                                 WHERE temp.rowNum BETWEEN " + start + " AND " + end;

                return conn.Query<ReferralExtend>(query).ToList();
            }
        }

        public override List<ReferralExtend> FindList(string strWhere)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "SELECT * FROM [dbo].[Referral] WHERE @where";
                return conn.Query<ReferralExtend>(query, new { where = strWhere }).ToList();
            }
        }

        public override int Update(ReferralExtend entity)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "UPDATE [dbo].[Referral] SET [Name] = @Name,[Phone] = @Phone ,[Desc] = @Desc ,[ClientId] = @ClientId,[DocId] = @DocId ,[Status] = @Status ,[AppointmentNo] = @AppointmentNo ,[CreaterId] = @CreaterId ,[CreateTime] = @CreateTime ,[IsDel] = @IsDel,[CreaterName]=@CreaterName,[CreaterPhone]=@CreaterPhone,[Sex]=@Sex,[Age]=@Age,[AppointmentTime]=@AppointmentTime,[DocBrif]=@DocBrif,[ClientName]=@ClientName,[LastUpdateTime]=getdate() WHERE Id=@Id";
                return conn.Execute(sql, entity);
            }

        }

        public override int Update(string strField, string strWhere)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "UPDATE [dbo].[Referral] SET [LastUpdateTime]=getdate()," + strField + " WHERE " + strWhere;
                return conn.Execute(sql);
            }
        }
        #endregion

        #region Extention
        /// <summary>
        /// 查询未读数
        /// </summary>
        /// <param name="type">0是地方医生，2是护士</param>
        /// <param name="keyId">如果type是0，这里就是医生的Id；否则就是护士所属诊所Id</param>
        /// <returns></returns>
        public List<int> GetUnreadCount(int type, int keyId)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "SELECT [Status],COUNT(0) AS Total FROM dbo.Referral r LEFT JOIN dbo.Referral_Read rd ON rd.RefId=r.Id WHERE IsDel = 0 AND CreaterId = @keyId AND rd.RefId IS NULL GROUP BY[Status]";
                if (type == 2)
                {
                    query = "SELECT [Status],COUNT(0) AS Total FROM dbo.Referral WHERE IsDel = 0 AND ClientId = @keyId GROUP BY[Status]";
                }
                List<ReferralExtend> list = conn.Query<ReferralExtend>(query, new { keyId = keyId }).ToList();
                List<int> statusArr = new List<int>();
                if (type == 0)
                {
                    statusArr.Add(list.Where(r => r.Status.IndexOf("10") == 0).Sum(r => r.Total));
                    statusArr.Add(list.Count(r => r.Status == "101"));
                    statusArr.Add(list.Count(r => r.Status == "102"));
                    statusArr.Add(list.Count(r => r.Status == "103"));
                    statusArr.Add(list.Count(r => r.Status == "110"));
                    statusArr.Add(list.Where(r => r.Status.Contains("12")).Sum(r => r.Total));
                    statusArr.Add(list.Count(r => r.Status == "121"));
                    statusArr.Add(list.Count(r => r.Status == "122"));
                }
                else
                {
                    statusArr.Add(list.Where(r => r.Status == "101").Sum(r => r.Total));
                    statusArr.Add(list.Where(r => r.Status == "101").Sum(r => r.Total));
                    for (int i = 0; i < 6; i++)
                    {
                        statusArr.Add(0);
                    }
                }

                return statusArr;
            }
        }

        #endregion

        #region 对外接口
        /// <summary>
        /// 对外提供接口专用
        /// </summary>
        public List<ReferralExtend> FindByPageExport(string status, int organizationId, int pagesize, int pagenumber)
        {
            int start = pagesize * (pagenumber - 1) + 1;
            int end = pagesize * pagenumber;
            string strFilter = string.Empty;
            if (!string.IsNullOrEmpty(status))
            {
                strFilter += " and [Status]='" + status + "'";
            }
            if (organizationId > 0)
            {
                strFilter += " and [ClientId]=" + organizationId;
            }

            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY Id DESC) AS rowNumber, r.Id, Name, Phone, Sex, Age,[Desc], ClientId, DocId, [Status], CreateTime, CreaterId, CreaterName,(SELECT STUFF((SELECT ',' +  [Path] FROM dbo.Referral_Img subTitle WHERE subTitle.RefId=A.RefId FOR XML PATH('')),1, 1, '') AS A FROM dbo.Referral_Img A GROUP BY RefId HAVING A.RefId = r.Id)as ImgsStr from dbo.Referral r WHERE [IsDel]=0 " + strFilter + " ) AS temp WHERE temp.rowNumber BETWEEN @start AND @end";
                List<ReferralExtend> list = conn.Query<ReferralExtend>(sql, new { start = start, end = end }).ToList();
                list.ForEach(l =>
                {
                    if (l.Sex == 2)
                    {
                        l.Sex = 0;
                    }
                });
                return list;
            }
        }

        public int GetCount(string status, int organizationId)
        {
            string strFilter = string.Empty;
            if (!string.IsNullOrEmpty(status))
            {
                strFilter += " and [Status]='" + status + "'";
            }
            if (organizationId > 0)
            {
                strFilter += " and [ClientId]=" + organizationId;
            }
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "SELECT COUNT(0) From dbo.Referral Where IsDel=0 " + strFilter;
                return conn.ExecuteScalar<int>(sql);
            }
        }

        #endregion
    }
}
