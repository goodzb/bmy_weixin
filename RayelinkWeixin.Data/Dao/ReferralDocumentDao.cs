﻿using Dapper;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Dao
{
    public class ReferralDocumentDao : BaseDao<ReferralDocumentExtend>
    {
        private static ReferralDocumentDao _instance;
        public static ReferralDocumentDao Instance
        {
            get
            {
                if (_instance==null)
                {
                    _instance = new ReferralDocumentDao();
                }
                return _instance;
            }
        }

        #region Basic
        public override int Add(ReferralDocumentExtend entity)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = "INSERT INTO [dbo].[Referral_Document] ([Name] ,[Sex] ,[Age] ,[Phone] ,[HospitalId] ,[Desc] ,[DocId] ,[From] ,[TransforTime] ,[CreateTime] ,[CreaterId],[IsDel]) VALUES(@Name,@Sex,@Age,@Phone,@HospitalId,@Desc,@DocId,@From,@TransforTime,@CreateTime,@CreaterId,@IsDel)";
                return conn.Execute(sql, entity);
            }
        }

        public override int Delete(int id)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "UPDATE [dbo].[Referral_Document] SET [IsDel]=1 WHERE Id=@id";
                return conn.Execute(sql, new {id=id });
            }
        }

        public override List<ReferralDocumentExtend> FindAll()
        {
            throw new NotImplementedException();
        }

        public override ReferralDocumentExtend FindById(int id)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "SELECT * FROM [dbo].[Referral_Document] WHERE Id=@id";
                return conn.Query<ReferralDocumentExtend>(sql, new { id = id }).SingleOrDefault();
            }
        }

        public override List<ReferralDocumentExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralDocumentExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public override int Update(ReferralDocumentExtend entity)
        {
            throw new NotImplementedException();
        }

        public override int Update(string strField, string strWhere)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
