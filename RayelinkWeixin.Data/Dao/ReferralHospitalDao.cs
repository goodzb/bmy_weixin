﻿using Dapper;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Dao
{
    public class ReferralHospitalDao : BaseDao<ReferralHospitalExtend>
    {
        private static ReferralHospitalDao _instance;
        public static ReferralHospitalDao Instance
        {
            get
            {
                if (_instance==null)
                {
                    _instance = new ReferralHospitalDao();
                }
                return _instance;
            }
        }

        public List<ReferralHospitalExtend> FindAllCity()
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = "SELECT DISTINCT CityId,CityName FROM dbo.Referral_Hospital";
                return conn.Query<ReferralHospitalExtend>(sql).ToList();
            }
        }

        public List<ReferralHospitalExtend> FindByCityId(int id)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                if (id==0)
                {
                    string sql = "select * from dbo.Referral_Hospital";
                    return conn.Query<ReferralHospitalExtend>(sql).ToList();
                }
                string sql1 = "select * from dbo.Referral_Hospital Where CityId=@cityId";
                return conn.Query<ReferralHospitalExtend>(sql1, new { cityId = id }).ToList();
            }
        }

        public override int Add(ReferralHospitalExtend entity)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = "Insert into dbo.Referral_Hospital(Name,Address,CityId,CityName)Values(@Name,@Address,@CityId,@CityName)";
                return conn.Execute(sql, entity);
            }
        }

        public override int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralHospitalExtend> FindAll()
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "select * from dbo.Referral_Hospital ";
                return conn.Query<ReferralHospitalExtend>(sql).ToList();
            }
        }

        public override ReferralHospitalExtend FindById(int id)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "select * from dbo.Referral_Hospital Where Id=@id";
                return conn.QueryFirstOrDefault<ReferralHospitalExtend>(sql, new { id = id });
            }
        }

        public override List<ReferralHospitalExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralHospitalExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public override int Update(ReferralHospitalExtend entity)
        {
            throw new NotImplementedException();
        }

        public override int Update(string strField, string strWhere)
        {
            throw new NotImplementedException();
        }
    }
}
