﻿using Dapper;
using RayelinkWeixin.Data.PO.Base;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Dao
{
    public class ReferralReadDao : BaseDao<ReferralReadExtend>
    {
        private static ReferralReadDao _instance;
        public static ReferralReadDao Instance
        {
            set { _instance = value; }
            get
            {
                if (_instance == null)
                {
                    _instance = new ReferralReadDao();
                }
                return _instance;
            }
        }

        #region Basic
        public override int Add(ReferralReadExtend entity)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = @"IF NOT EXISTS(SELECT * FROM dbo.Referral_Read WHERE RefId=@RefId AND UserId=@UserId)
                                INSERT INTO [dbo].[Referral_Read]
                               ([RefId]
                               ,[UserId])
                         VALUES(@RefId,@UserId);select @@identity";
                return conn.Execute(sql, entity);
            }
        }

        public int ClearRead(int rid)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = "delete from dbo.Referral_Read where RefId=@rid";
                return conn.Execute(sql, new { rid = rid });
            }
        }

        public override int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralReadExtend> FindAll()
        {
            throw new NotImplementedException();
        }

        public override ReferralReadExtend FindById(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralReadExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralReadExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public override int Update(ReferralReadExtend entity)
        {
            throw new NotImplementedException();
        }

        public override int Update(string strField, string strWhere)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
