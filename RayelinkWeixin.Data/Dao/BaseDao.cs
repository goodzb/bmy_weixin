﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Dao
{
    public abstract class BaseDao<T> where T:class,new()
    {
        
        protected string connString = ConfigurationManager.ConnectionStrings["referral"].ConnectionString;

        public abstract int Add(T entity);
        public abstract int Update(T entity);
        public abstract int Delete(int id);
        public abstract List<T> FindAll();
        public abstract T FindById(int id);

        public abstract int Update(string strField, string strWhere);
        public abstract List<T> FindList(string strWhere);
        public abstract List<T> FindByPage(int pagesize, int pageindex,string strWhere,string strOrderBy);

        public List<T> QueryList(string strSql)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                return conn.Query<T>(strSql).ToList();
            }
        }

        public T QuerySingle(string strSql)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                return conn.QueryFirst<T>(strSql);
            }
        }
    }
}
