﻿using Dapper;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Dao
{
    public class ReferralImgDao : BaseDao<ReferralImgExtend>
    {
        private static ReferralImgDao _instance;
        public static ReferralImgDao Instance
        {
            set { _instance = value; }
            get
            {
                if (_instance == null)
                {
                    _instance = new ReferralImgDao();
                }
                return _instance;
            }
        }

        #region Basic
        public override int Add(ReferralImgExtend entity)
        {
            /*
             * INSERT INTO [dbo].[Referral_Img] ([RefId] ,[Path]) VALUES()
             * */
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "INSERT INTO [dbo].[Referral_Img] ([RefId] ,[Path]) VALUES(@RefId,@Path)";
                return conn.Execute(query, entity);
            }
        }

        public List<string> FindByRef(int refId)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string query = "select Path from Referral_Img where RefId=@refId";
                return conn.Query<string>(query, new {refId=refId }).ToList();
            }
        }

        public override int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralImgExtend> FindAll()
        {
            throw new NotImplementedException();
        }

        public override ReferralImgExtend FindById(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralImgExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralImgExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public override int Update(ReferralImgExtend entity)
        {
            throw new NotImplementedException();
        }

        public override int Update(string strField, string strWhere)
        {
            throw new NotImplementedException();
        }

        public int Delete(string path)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = "delete from dbo.Referral_Img where Path='"+path+"'";
                return conn.Execute(sql);
            }
        }
        
        public int DeleteByRefId(int refId)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "delete from dbo.Referral_Img where RefId="+refId;
                return conn.Execute(query);
            }
        }
        #endregion

    }
}
