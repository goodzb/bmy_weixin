﻿using Dapper;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Dao
{
    public class ReferralLogDao : BaseDao<ReferralLogExtend>
    {
        private static ReferralLogDao _instance;
        public static ReferralLogDao Instance
        {
            get
            {
                if (_instance==null)
                {
                    _instance = new ReferralLogDao();
                }
                return _instance;
            }
        }

        public List<ReferralLogExtend> GetByRefId(int refId)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "select * from dbo.Referral_Log where ReferralId=@refId";
                return conn.Query<ReferralLogExtend>(query, new { refId = refId }).ToList();
            }
        }

        #region Basic
        public override int Add(ReferralLogExtend entity)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = "INSERT INTO [dbo].[Referral_Log] ([ReferralId] ,[CreateTime] ,[Content] ,[Remark] ,[Next],[CreaterId]) VALUES(@ReferralId,@CreateTime,@Content,@Remark,@Next,@CreaterId)";
                return conn.Execute(sql, entity);
            }
        }

        public int Add(ReferralExtend referral,int userId,string time="")
        {
            ReferralLogExtend log = new ReferralLogExtend();

            string content = string.Empty, next = string.Empty;
            switch (referral.Status)
            {
                case "101":
                    content = "提交转诊单";
                    next = "待建档";
                    log.CreateTime = DateTime.Now;
                    break;
                case "102":
                    content = "建立患者档案";
                    next = "待预约";
                    log.CreateTime = DateTime.Now;
                    break;
                case "103":
                    string name = referral.DocBrif.Substring(0,referral.DocBrif.IndexOf("（"));
                    content = "预约远程视频门诊--"+name;
                    next = "待就诊";
                    log.Remark = referral.TimeDuring;
                    log.CreateTime = DateTime.Now;
                    break;
                case "110":
                    content = "就诊";
                    next = "已就诊";
                    log.CreateTime = DateTime.Now;
                    if (!string.IsNullOrEmpty(time))
                    {
                        log.CreateTime = DateTime.Parse(time);
                    }
                    break;
                case "121":
                    content = "取消预约";
                    next = "已取消";
                    log.CreateTime = DateTime.Now;
                    if (!string.IsNullOrEmpty(time))
                    {
                        log.CreateTime = DateTime.Parse(time);
                    }
                    break;
                case "122":
                    content = "未到诊";
                    next = "未到诊";
                    log.CreateTime = DateTime.Now;
                    break;
                default:
                    break;
            }
            log.Content = content;
            log.Next = next;
            log.CreaterId = userId;
            log.ReferralId = referral.Id;

            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "INSERT INTO [dbo].[Referral_Log] ([ReferralId] ,[CreateTime] ,[Content] ,[Remark] ,[Next],[CreaterId]) VALUES(@ReferralId,@CreateTime,@Content,@Remark,@Next,@CreaterId)";
                return conn.Execute(sql, log);
            }
        }

        public override int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralLogExtend> FindAll()
        {
            throw new NotImplementedException();
        }

        public override ReferralLogExtend FindById(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralLogExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralLogExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public override int Update(ReferralLogExtend entity)
        {
            throw new NotImplementedException();
        }

        public override int Update(string strField, string strWhere)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
