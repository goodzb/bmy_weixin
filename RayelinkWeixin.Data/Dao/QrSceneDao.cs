﻿using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using RayelinkWeixin.Common.Utilites;

namespace RayelinkWeixin.Data.Dao
{
    public class QrSceneDao : BaseDao<QrSceneExtend>
    {
        private static QrSceneDao _instance;
        public static QrSceneDao Instance
        {
            set { _instance = value; }
            get
            {
                if (_instance == null)
                {
                    _instance = new QrSceneDao();
                }
                return _instance;
            }
        }

        #region Basic
        public override int Add(QrSceneExtend entity)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string query = "INSERT INTO [dbo].[QrScene]([OpenID],[Event],[EventKey],[CreateTime]) VALUES(@OpenID,@Event,@EventKey,getdate());SELECT @@identity;";

                return conn.ExecuteScalar<int>(query, entity);
            }
        }
        public override int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override List<QrSceneExtend> FindAll()
        {
            throw new NotImplementedException();
        }

        public override QrSceneExtend FindById(int id)
        {
            throw new NotImplementedException();
        }

        public override List<QrSceneExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            throw new NotImplementedException();
        }

        public override List<QrSceneExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public override int Update(QrSceneExtend entity)
        {
            throw new NotImplementedException();
        }

        public override int Update(string strField, string strWhere)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
