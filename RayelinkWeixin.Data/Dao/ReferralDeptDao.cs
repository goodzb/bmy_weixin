﻿using Dapper;
using RayelinkWeixin.Data.PO.Base;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Dao
{

    /// <summary>
    /// 
    /// </summary>
    public class ReferralDeptDao : BaseDao<ReferralDeptExtend>
    {
        private static ReferralDeptDao _instance;
        public static ReferralDeptDao Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ReferralDeptDao();
                }
                return _instance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override int Add(ReferralDeptExtend entity)
        {
            throw new NotImplementedException();
        }

        public override int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralDeptExtend> FindAll()
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string sql = "select * from dbo.Referral_Dept";
                return conn.Query<ReferralDeptExtend>(sql).ToList();
            }
        }

        public override ReferralDeptExtend FindById(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralDeptExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralDeptExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public override int Update(ReferralDeptExtend entity)
        {
            throw new NotImplementedException();
        }

        public override int Update(string strField, string strWhere)
        {
            throw new NotImplementedException();
        }
    }
}
