﻿using Dapper;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Dao
{
    public class ReferralDocDao : BaseDao<ReferralDocExtend>
    {
        private static ReferralDocDao _instance;
        public static ReferralDocDao Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ReferralDocDao();
                }
                return _instance;
            }
        }

        /// <summary>
        /// 获取医生的姓名和所属科室，形如：专家名（科室）
        /// </summary>
        /// <param name="docId"></param>
        /// <returns></returns>
        public string GetDocBrif(int docId)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string query = "SELECT d.Name,p.Name AS DeptName FROM dbo.Referral_Doc d LEFT JOIN dbo.Referral_Dept p ON p.Id=d.DepId where d.Id=@id";
                ReferralDocExtend doc = conn.QueryFirstOrDefault<ReferralDocExtend>(query, new { id = docId });
                if (doc==null)
                {
                    return "";
                }
                return string.Format("{0}（{1}）",doc.Name,doc.DeptName);
            }
        }

        public override int Add(ReferralDocExtend entity)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = "Insert into dbo.Referral_Doc(UnionId,Name,DepId,Title,Desc,ThumbNail,HospitalId)Values(@UnionId,@Name,@DepId,@Title,@Desc,@ThumbNail)";
                return conn.Execute(sql);
            }
        }

        public override int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralDocExtend> FindAll()
        {
            throw new NotImplementedException();
        }

        public override ReferralDocExtend FindById(int id)
        {
            throw new NotImplementedException();
        }

        public override List<ReferralDocExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string sql = @"SELECT * FROM(
                                SELECT ROW_NUMBER() OVER(ORDER BY @orderby) AS rowNum,d.*,dp.Name as DeptName FROM dbo.Referral_Doc d left join dbo.Referral_Dept dp on dp.Id=d.DepId
                                WHERE d.Id>0 "+strWhere+@"
                                )AS temp
                                WHERE temp.rowNum BETWEEN @start AND @end";
                return conn.Query<ReferralDocExtend>(sql, new {orderby=strOrderBy,start=pagesize*(pageindex-1)+1,end=pagesize*pageindex }).ToList();
            }
        }

        public override List<ReferralDocExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public override int Update(ReferralDocExtend entity)
        {
            using (IDbConnection conn=new SqlConnection(connString))
            {
                string update = "Update dbo.Referral_Doc set UnionId=@UnionId,Name=@Name,DepId=@DepId,Title=@Title,Desc=@Desc,ThumbNail=@ThumbNail,HospitalId=@HospitalId Where Id=@Id";
                return conn.Execute(update, entity);
            }
        }

        public override int Update(string strField, string strWhere)
        {
            using (IDbConnection conn = new SqlConnection(connString))
            {
                string update = "Update dbo.Referral_Doc set @fields Where @strWhere";
                return conn.Execute(update, new { fields=strField,strWhere=strWhere});
            }
        }
    }
}
