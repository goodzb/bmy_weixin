﻿using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Data.Dao;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Repository
{
    public class ReferralRepository
    {
        private static ReferralRepository _instance;
        public static ReferralRepository Instance
        {
            set { _instance = value; }
            get
            {
                if (_instance == null)
                {
                    _instance = new ReferralRepository();
                }
                return _instance;
            }
        }

        #region 转诊单（Referral）
        
        public int AddReferral(ReferralExtend entity)
        {
            int res = ReferralDao.Instance.Add(entity);
            if (res > 0)
            {
                entity.Id = res;
                //插入日志
                ReferralLogDao.Instance.Add(entity, entity.CreaterId);
                //推送消息
                PushMessageRepository.Instance.ReferralSendWxMsg(entity);
            }
            return res;
        }
        
        public ReferralExtend FindReferralById(int id)
        {
            return ReferralDao.Instance.FindById(id);
        }

        public List<ReferralExtend> FindReferralByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            List<ReferralExtend> list = ReferralDao.Instance.FindByPage(pagesize, pageindex, strWhere, strOrderBy);
            foreach (ReferralExtend item in list)
            {
                if (DateTime.Now.Date==item.LastUpdateTime.Date)
                {
                    item.CreateTimeStr = item.LastUpdateTime.ToString("HH:mm");
                }
                else
                {
                    item.CreateTimeStr = item.LastUpdateTime.ToString("yyyy-MM-dd");
                }
            }
            return list;
        }
        
        public int UpdateReferral(ReferralExtend entity,int userId,string lastUpdateTime="")
        {
            int r = 0;
            try
            {
                //修改转诊单
                r = ReferralDao.Instance.Update(entity);
                //插入日志
                ReferralLogDao.Instance.Add(entity, userId,lastUpdateTime);
                //所有人都是未读
                ReferralReadDao.Instance.ClearRead(entity.Id);
                //推送消息
                if (userId>0)
                {
                    PushMessageRepository.Instance.ReferralSendWxMsg(entity);
                }
            }
            catch (Exception e)
            {
                LogHelper.Error(e);
            }

            return r;
        }

        public int Update(string fields,string where)
        {
            return ReferralDao.Instance.Update(fields, where);
        }
        
        public int UpdateNurseOpenId(int clientId,string openid)
        {
            return ReferralDao.Instance.Update("NurseOpenId='" + openid + "'", "ClientId="+clientId);
        }

        public List<int> GetReferralUnreadCount(int type,int keyId)
        {
            return ReferralDao.Instance.GetUnreadCount(type,keyId);
        }

        public ReferralExtend FindReferralDeailById(int id)
        {
            ReferralExtend refer = FindReferralById(id);
            refer.Imgs = ReferralImgDao.Instance.FindByRef(id);
            //refer.DocBrif = ReferralDocDao.Instance.GetDocBrif(refer.DocId);
            refer.Logs = ReferralLogDao.Instance.GetByRefId(id);
            return refer;
        }
        
        #endregion

        #region 已读（Read）
        public int AddRead(ReferralReadExtend entity)
        {
            return ReferralReadDao.Instance.Add(entity);
        }
        #endregion

        #region 图片（Img）
        public int AddImg(ReferralImgExtend entity)
        {
            return ReferralImgDao.Instance.Add(entity);
        }

        public int DelImg(string path)
        {
            return ReferralImgDao.Instance.Delete(path);
        }

        public int DelImg(int refid)
        {
            return ReferralImgDao.Instance.DeleteByRefId(refid);
        }
        #endregion
    }
}
