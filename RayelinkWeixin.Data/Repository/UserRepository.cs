﻿using RayelinkWeixin.Data.Dao;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Repository
{
    public class UserRepository 
    {
        private static UserRepository _instance;
        public static UserRepository Instance
        {
            get
            {
                if (_instance==null)
                {
                    _instance = new UserRepository();
                }
                return _instance;
            }
        }


        public int Add(ReferralUserExtend entity)
        {
            return ReferralUserDao.Instance.Add(entity);
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<ReferralUserExtend> FindAll()
        {
            throw new NotImplementedException();
        }

        public ReferralUserExtend FindById(int id)
        {
            return ReferralUserDao.Instance.FindById(id);
        }

        public ReferralUserExtend FindByOpenId(string openid)
        {
            return ReferralUserDao.Instance.FindByOpenId(openid);
        }

        public List<ReferralUserExtend> FindByPage(int pagesize, int pageindex, string strWhere, string strOrderBy)
        {
            throw new NotImplementedException();
        }

        public List<ReferralUserExtend> FindList(string strWhere)
        {
            throw new NotImplementedException();
        }

        public List<ReferralUserExtend> QueryList(string strSql)
        {
            throw new NotImplementedException();
        }

        public ReferralUserExtend QuerySingle(string strSql)
        {
            throw new NotImplementedException();
        }

        public int Update(ReferralUserExtend entity)
        {
            throw new NotImplementedException();
        }

        public int Update(string strField, string strWhere)
        {
            throw new NotImplementedException();
        }
    }
}
