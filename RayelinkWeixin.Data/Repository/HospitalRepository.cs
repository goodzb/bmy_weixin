﻿using RayelinkWeixin.Data.Dao;
using RayelinkWeixin.Data.PO.Extend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.Repository
{
    public class HospitalRepository
    {
        private static HospitalRepository _instance;
        public static HospitalRepository Instance
        {
            get
            {
                if (_instance==null)
                {
                    _instance = new HospitalRepository();
                }
                return _instance;
            }
        }

        #region 医院（Hospital）
        public ReferralHospitalExtend FindHospitalById(int id)
        {
            return ReferralHospitalDao.Instance.FindById(id);
        }

        public List<ReferralHospitalExtend> FindHospitalByCityId(int id)
        {
            return ReferralHospitalDao.Instance.FindByCityId(id);
        }

        public List<ReferralHospitalExtend> FindHospitalAllCity()
        {
            return ReferralHospitalDao.Instance.FindAllCity();
        }
        public List<ReferralHospitalExtend> FindAllHospital()
        {
            return ReferralHospitalDao.Instance.FindAll();
        }
        #endregion

        #region 科室（Department）
        public List<ReferralDeptExtend> FindDeptOrg()
        {
            List<ReferralDeptExtend> listAll = ReferralDeptDao.Instance.FindAll();
            List<ReferralDeptExtend> res = listAll.Where(r => r.PId == 0).OrderBy(i=>i.Name).ToList();
            List<ReferralDeptExtend> allChildren = listAll.Where(r => r.PId > 0).ToList().OrderBy(r=>r.PId).ThenBy(r=>r.Name).ToList();
            allChildren.Insert(0, new ReferralDeptExtend {Id=0,Name="全部"});

            foreach (var item in res)
            {
                item.Children = listAll.Where(r => r.PId == item.Id).ToList();
                item.Children.Insert(0, new ReferralDeptExtend { Name = "全部" });
            }
            res.Insert(0, new ReferralDeptExtend { Id = 0, Name = "全部科室", Children = allChildren });
            return res;
        }
        #endregion

        #region 医生（Doctor）
        public List<ReferralDocExtend> FindDocByPage(int pagesize,int pageindex,string key,int deptId,int hospitalId)
        {
            string where = string.Empty;
            if (!string.IsNullOrEmpty(key))
            {
                where += "and Name Like '%"+key+"%' ";
            }
            if (deptId>0)
            {
                where += "and DepId="+deptId;
            }
            if (hospitalId>0)
            {
                where += "and ";
            }

            return ReferralDocDao.Instance.FindByPage(pagesize, pageindex, where, "d.Id desc");
        }
        #endregion
    }
}
