﻿using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Data.Const;
using RayelinkWeixin.Data.PO.Extend;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using Senparc.Weixin.MP.CommonAPIs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RayelinkWeixin.Data.Repository
{
    public class PushMessageRepository
    {
        private static PushMessageRepository _instance;
        public static PushMessageRepository Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PushMessageRepository();
                }
                return _instance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="referral"></param>
        /// <returns></returns>
        public int ReferralSendWxMsg(ReferralExtend referral)
        {
            try
            {
                string domain = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
                string detailUrl = domain + "/Referral/ReferDetail?id=" + referral.Id;
                var temp = new WxTemplateData();
                // var accessToken = AccessTokenContainer.GetAccessToken(SiteConfig.appID);
                var accessToken = Senparc.Weixin.MP.Containers.AccessTokenContainer.TryGetAccessToken(SiteConfig.appID, SiteConfig.appSecret);
                SendTemplateMessageResult res = new SendTemplateMessageResult();
                switch (referral.Status)
                {
                    case "101":
                        if (string.IsNullOrEmpty(referral.NurseOpenId))
                        {
                            LogHelper.Info("没有对应的护士，不推送。");
                            return 0;
                        }
                        //刚创建转诊单待建档，要给护士推送消息
                        temp.first = new TemplateDataItem("您好，您有一条转诊单待处理");
                        temp.keyword1 = new TemplateDataItem(referral.Name);
                        temp.keyword2 = new TemplateDataItem(referral.Desc);
                        temp.remark = new TemplateDataItem("请及时查看！");
                        res = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(accessToken, referral.NurseOpenId, SiteConfig.NewReferral, detailUrl, temp);

                        break;
                    case "103":
                        //已预约待就诊，要给地方医生和护士都推送消息
                        temp.first = new TemplateDataItem("您好，您有一条转诊单状态更新");
                        temp.keyword1 = new TemplateDataItem(referral.Name);
                        temp.keyword2 = new TemplateDataItem(referral.Desc);
                        temp.keyword3 = new TemplateDataItem("待就诊");
                        temp.remark = new TemplateDataItem("请及时查看！");
                        //给医生
                        res = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(accessToken, referral.CreaterOpenId, SiteConfig.ReferralChange,  detailUrl, temp);
                        if (string.IsNullOrEmpty(referral.NurseOpenId))
                        {
                            LogHelper.Info("没有对应的护士，不推送。");
                            return 0;
                        }
                        //给护士
                        res = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(accessToken, referral.NurseOpenId, SiteConfig.ReferralChange,  detailUrl, temp);

                        break;
                    case "110":
                        //已预约待就诊，要给地方医生和护士都推送消息
                        temp.first = new TemplateDataItem("您好，您有一条转诊单状态更新");
                        temp.keyword1 = new TemplateDataItem(referral.Name);
                        temp.keyword2 = new TemplateDataItem(referral.Desc);
                        temp.keyword3 = new TemplateDataItem("已就诊");
                        temp.remark = new TemplateDataItem("请及时查看！");
                        //给医生
                        res = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(accessToken, referral.CreaterOpenId, SiteConfig.ReferralChange,  detailUrl, temp);

                        break;
                    default:
                        break;
                }
                LogHelper.Info("推送成功！");
                return 1;
            }
            catch (Exception e)
            {
                LogHelper.Info("推送失败！");
                LogHelper.Error(e);
                return 0;
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class WxTemplateData
    {
        public TemplateDataItem first { get; set; }
        public TemplateDataItem keyword1 { get; set; }
        public TemplateDataItem keyword2 { get; set; }
        public TemplateDataItem keyword3 { get; set; }
        public TemplateDataItem keyword4 { get; set; }
        public TemplateDataItem keyword5 { get; set; }
        public TemplateDataItem remark { get; set; }
    }
}
