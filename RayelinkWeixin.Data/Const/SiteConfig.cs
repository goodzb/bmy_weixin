﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace RayelinkWeixin.Data.Const
{
    public class SiteConfig
    {
        /// <summary>
        /// 微信appID
        /// </summary>
        public static string appID = WebConfigurationManager.AppSettings["appID"];

        /// <summary>
        /// 微信appsecret
        /// </summary>
        public static string appSecret = WebConfigurationManager.AppSettings["appSecret"];

        #region 微信消息模板Id
        /// <summary>
        /// 新建转诊
        /// </summary>
        public static string NewReferral = WebConfigurationManager.AppSettings["NewReferral"];

        /// <summary>
        /// 转诊单状态改变
        /// </summary>
        public static string ReferralChange = WebConfigurationManager.AppSettings["ReferralChange"];
        #endregion
    }
}
