﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.PO.Base
{
    public abstract class Referral_Log
    {
        public int Id { get; set; }
        public int ReferralId { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string Content { get; set; }
        public string Remark { get; set; }
        public string Next { get; set; }
        public int CreaterId { get; set; }
    }
}
