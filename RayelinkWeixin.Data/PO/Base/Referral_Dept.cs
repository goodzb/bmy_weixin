﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.PO.Base
{
    public abstract class Referral_Dept
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PId { get; set; }

        public int UnionId { get; set; }
    }
}
