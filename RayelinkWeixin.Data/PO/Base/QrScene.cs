﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.PO.Base
{
    public abstract class QrScene
    {
        public int Id { get; set; }
        public string OpenID { get; set; }
        public string Event { get; set; }
        public string EventKey { get; set; }
        public System.DateTime CreateTime { get; set; }
        
    }
}
