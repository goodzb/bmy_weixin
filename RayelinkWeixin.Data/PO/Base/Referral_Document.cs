﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.PO.Base
{
    public abstract class Referral_Document
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Sex { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }
        public int HospitalId { get; set; }
        public string Desc { get; set; }
        public int DocId { get; set; }
        public int From { get; set; }
        public System.DateTime TransforTime { get; set; }
        public System.DateTime CreateTime { get; set; }
        public int CreaterId { get; set; }
        public int IsDel { get; set; }
    }
}
