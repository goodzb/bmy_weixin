﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.PO.Base
{
    public abstract class Referral_User
    {
        public int Id { get; set; }
        public int UnionId { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string OpenId { get; set; }
        public int HospitalId { get; set; }
    }
}
