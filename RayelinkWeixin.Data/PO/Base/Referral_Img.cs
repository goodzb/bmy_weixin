﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.PO.Base
{
    public abstract class Referral_Img
    {
        public int Id { get; set; }
        public int RefId { get; set; }
        public string Path { get; set; }
    }
}
