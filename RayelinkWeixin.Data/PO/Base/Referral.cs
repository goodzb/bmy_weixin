﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.PO.Base
{
    public abstract class Referral
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Desc { get; set; }
        public int ClientId { get; set; }
        public int DocId { get; set; }
        public string Status { get; set; }
        /// <summary>
        /// 预约单号
        /// </summary>
        public string AppointmentNo { get; set; }
        public int CreaterId { get; set; }
        public System.DateTime CreateTime { get; set; }
        public bool IsDel { get; set; }
        /// <summary>
        /// 地方医生的名称
        /// </summary>
        public string CreaterName { get; set; }
        /// <summary>
        /// 地方医生的手机
        /// </summary>
        public string CreaterPhone { get; set; }
        /// <summary>
        /// 地方医生的OpenId
        /// </summary>
        public string CreaterOpenId { get; set; }
        public int Sex { get; set; }
        public int Age { get; set; }
        /// <summary>
        /// 预约时间
        /// </summary>
        public DateTime? AppointmentTime { get; set; }
        /// <summary>
        /// 意向专家信息
        /// </summary>
        public string DocBrif { get; set; }
        public string ClientName { get; set; }
        /// <summary>
        /// 对应诊所护士的OpenId
        /// </summary>
        public string NurseOpenId { get; set; }

        public DateTime LastUpdateTime { get; set; }
    }
}
