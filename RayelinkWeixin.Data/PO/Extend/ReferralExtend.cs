﻿using RayelinkWeixin.Data.PO.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Data.PO.Extend
{
    public class ReferralExtend:Referral
    {
        /// <summary>
        /// 某状态下的总条数，我的转诊显示
        /// </summary>
        public int Total { get; set; }

        private int _isread;
        /// <summary>
        /// 当前用户是否已读该单据，我的转诊显示
        /// </summary>
        public int IsRead {
            get { if (_isread > 0) { return 1; }return 0; }
            set { _isread = value; }
        }

        /// <summary>
        /// 转诊来源人信息，包括姓名和电话，逗号隔开，转诊详情显示
        /// </summary>
        public string FromBrif { get; set; }
        
        /// <summary>
        /// 转诊单的操作日志
        /// </summary>
        public List<ReferralLogExtend> Logs { get; set; }

        /// <summary>
        /// 病情资料图
        /// </summary>
        public List<string> Imgs { get; set; }
        public string CreateTimeStr { get; set; }

        public string ImgsStr { get; set; }

        private string _statusName;
        public string StatusName
        {
            set
            {
                _statusName = value;
            }
            get {
                switch (_statusName)
                {
                    case "101":
                        return "待建档";
                    case "102":
                        return "待预约";
                    case "103":
                        return "待就诊";
                    case "110":
                        return "待建档";
                    case "121":
                        return "待建档";
                    case "122":
                        return "待建档";
                    default:
                        return "";
                }
            }
        }

        /// <summary>
        /// 预约时间段，临时存放
        /// </summary>
        public string TimeDuring { get; set; }
    }
}
    
