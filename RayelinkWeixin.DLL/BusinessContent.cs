﻿namespace RayelinkWeixin.DLL
{
    /// <summary>
    /// 
    /// </summary>
    public partial class BusinessContent
    {
        /// <summary>
        /// 微信账号信息
        /// </summary>
        private static AppBLL m_App;

        /// <summary>
        /// 微信账号信息
        /// </summary>
        public static AppBLL App
        {
            get { return m_App ?? (m_App = new AppBLL()); }
        }

        private static UserActionEventLogBLL m_UserActionEventLog;

        /// <summary>
        /// 用户行为事件记录
        /// </summary>
        public static UserActionEventLogBLL UserActionEventLog
        {
            get { return m_UserActionEventLog ?? (m_UserActionEventLog = new UserActionEventLogBLL()); }
        }

        private static AppConfigBLL m_AppConfig;
        /// <summary>
        /// App配置表
        /// </summary>
        public static AppConfigBLL AppConfig
        {
            get { return m_AppConfig ?? (m_AppConfig = new AppConfigBLL()); }
        }

        private static FZ_OrdersBLL m_FZ_Order;

        /// <summary>
        /// 订单信息表
        /// </summary>
        public static FZ_OrdersBLL FZ_Order
        {
            get { return m_FZ_Order ?? (m_FZ_Order = new FZ_OrdersBLL()); }
        }

        private static FZ_UsersBLL m_FZ_User;

        /// <summary>
        /// 用户信息表
        /// </summary>
        public static FZ_UsersBLL FZ_User
        {
            get { return m_FZ_User ?? (m_FZ_User = new FZ_UsersBLL()); }
        }

        private static FZ_DoctorsBLL m_FZ_Doctor;

        /// <summary>
        /// 医生信息表
        /// </summary>
        public static FZ_DoctorsBLL FZ_Doctor
        {
            get { return m_FZ_Doctor ?? (m_FZ_Doctor = new FZ_DoctorsBLL()); }
        }

        private static FZ_ConversationsBLL m_FZ_Conversations;

        /// <summary>
        /// 会话消息
        /// </summary>
        public static FZ_ConversationsBLL FZ_Conversations
        {
            get { return m_FZ_Conversations ?? (m_FZ_Conversations = new FZ_ConversationsBLL()); }
        }

        private static FZ_DoctorsMapAssistBLL m_FZ_DoctorsMapAssist;

        /// <summary>
        /// 医生与助理的映射关系表
        /// </summary>
        public static FZ_DoctorsMapAssistBLL FZ_DoctorsMapAssist
        {
            get { return m_FZ_DoctorsMapAssist ?? (m_FZ_DoctorsMapAssist = new FZ_DoctorsMapAssistBLL()); }
        }

        private static FZ_PayRecordsBLL m_FZ_PayRecords;

        /// <summary>
        /// 订单微信支付记录信息表
        /// </summary>
        public static FZ_PayRecordsBLL FZ_PayRecords
        {
            get { return m_FZ_PayRecords ?? (m_FZ_PayRecords = new FZ_PayRecordsBLL()); }
        }

        private static FZ_SendCardRecordsBLL m_FZ_SendCardRecords;

        /// <summary>
        /// 发卡记录表
        /// </summary>
        public static FZ_SendCardRecordsBLL FZ_SendCardRecords
        {
            get { return m_FZ_SendCardRecords ?? (m_FZ_SendCardRecords = new FZ_SendCardRecordsBLL()); }
        }

        private static SessionLogBLL m_SessionLog;

        /// <summary>
        /// 在线登录记录
        /// </summary>
        public static SessionLogBLL SessionLog
        {
            get { return m_SessionLog ?? (m_SessionLog = new SessionLogBLL()); }
        }

        private static QRCodeBLL m_QRCode;

        /// <summary>
        /// 二维码
        /// </summary>
        public static QRCodeBLL QRCode
        {
            get { return m_QRCode ?? (m_QRCode = new QRCodeBLL()); }
        }

        /// 帐户信息
        /// </summary>
        private static AccountBLL m_Account;

        /// <summary>
        /// 帐户信息
        /// </summary>
        public static AccountBLL Account
        {
            get { return m_Account ?? (m_Account = new AccountBLL()); }
        }
    }
}
