﻿using RayelinkWeixin.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace RayelinkWeixin.DLL
{
    /// <summary>
    /// BLL基类，实现基本的方法
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseBLL<T> : IBLL where T : class, new()
    {
        #region 变量
        /// <summary>
        /// 表名
        /// </summary>
        protected string TableName = string.Empty;
        /// <summary>
        /// 主键ID
        /// </summary>
        protected string TableKey = string.Empty;

        /// <summary>
        /// 状态字段名称
        /// </summary>
        protected string StatusColumn = string.Empty;

        #endregion

        /// <summary>
        /// 条件查询一条数据
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="orderBy">排序字段及其排序方式</param>
        /// <param name="isContainDelete">是否查询status=0的数据，默认为否</param>
        /// <returns></returns>
        public T FindOne(string where, string orderBy = "", bool isContainDelete = false)
        {
            CheckWhere(ref where, isContainDelete, orderBy);
            var result = RayelinkWeixin.Model.DBConnection.Record<T>.FirstOrDefault(where);
            return result;
        }

        /// <summary>
        /// 查询一条信息，带有arg的查询
        /// </summary>
        /// <param name="sqls">查询条件</param>
        /// <param name="isContainDelete">是否查询status=0的数据，默认为否</param>
        /// <param name="orderBy">排序方式</param>
        /// <param name="arg">参数</param>
        /// <returns></returns>
        public T FindOne(string sqls, bool isContainDelete = false, string orderBy = "", params object[] arg)
        {
            CheckWhere(ref sqls, isContainDelete, orderBy);
            var result = DBConnection.Record<T>.FirstOrDefault(sqls, arg);
            return result;
        }

        /// <summary>
        /// 查询一条信息，带有arg的查询
        /// </summary>
        /// <param name="sqls">查询条件</param>
        /// <param name="arg">参数</param>
        /// <returns>实体对象</returns>
        public T FindOne(string sqls, params object[] arg)
        {
            return FindOne(sqls, false, string.Empty, arg);
        }

        /// <summary>
        /// 根据int主键ID查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetFormDataInfoByIdentityKey(int id)
        {
            var result = DBConnection.Record<T>.FirstOrDefault(string.Format(" WHERE {0}=@0", TableKey), id);
            return result;
        }

        /// <summary>
        /// 查询N条数据(默认为20条)
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="top">前N条</param>
        /// <param name="orderBy">排序字段及其排序方式</param>
        /// <param name="isContainDelete">是否查询status=0的数据，默认为否</param>
        /// <returns></returns>
        public IEnumerable<T> Find(string where, int top = 20, string orderBy = "", bool isContainDelete = false)
        {
            CheckWhere(ref where, isContainDelete, orderBy);
            CheckOrderBy(ref orderBy);
            return RayelinkWeixin.Model.DBConnection.Record<T>.SkipTake(0, top, where);
        }

        /// <summary>
        /// 查询所有符合条件的数据
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="isContainDelete">是否查询status=0的数据，默认为否</param>
        /// <returns></returns>
        public IEnumerable<T> FindAll(string where = "", string orderBy = "", bool isContainDelete = false)
        {
            CheckWhere(ref where, isContainDelete, orderBy);
            CheckOrderBy(ref orderBy);
            return RayelinkWeixin.Model.DBConnection.Record<T>.Fetch(where);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="pageIndex">当前页数</param>
        /// <param name="pageSize">每页显示数据条数</param>
        /// <param name="count">符合条件的总数</param>
        /// <param name="orderBy">排序字段及其排序方式</param>
        /// <param name="isContainDelete">是否查询status=0的数据，默认为否</param>
        /// <returns>实体对象集合</returns>
        public IEnumerable<T> GetPerPageFormDataList(string where, int pageIndex, int pageSize, out int count, string orderBy = "", bool isContainDelete = false)
        {
            CheckWhere(ref where, isContainDelete, orderBy);
            CheckOrderBy(ref orderBy);
            var modelList = RayelinkWeixin.Model.DBConnection.Record<T>.Page(pageIndex, pageSize, where);
            count = Convert.ToInt32(modelList.TotalItems);
            return modelList.Items;
        }

        /// <summary>
        /// 查询符合条件的数据量
        /// </summary>
        /// <param name="where"查询条件>查询条件</param>
        /// <param name="isContainDelete">是否查询status=0的数据，默认为否</param>
        /// <returns>符合条件的数据量</returns>
        public int FindCount(string where, bool isContainDelete = false)
        {
            CheckWhere(ref where, isContainDelete);
            return DBConnection.Record<T>.Fetch(where).Count;
        }

        /// <summary>
        /// 直接执行sql查询
        /// </summary>
        /// <param name="sql">完整的sql查询语句</param>
        /// <returns>影响的行数</returns>
        public IEnumerable<T> Query(string sql, params object[] args)
        {
            return RayelinkWeixin.Model.DBConnection.Record<T>.Query(sql, args);
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="sql">sql</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public bool Exists(string sql, params object[] args)
        {
            return DBConnection.Record<T>.Exists(sql, args);
        }

        /// <summary>
        /// 执行sql，一般是修改，删除等sql命令,不带查询的命令
        /// </summary>
        /// <param name="sql">一般是修改，删除等sql命令</param>
        /// <param name="args"></param>
        /// <returns>影响的行数</returns>
        public int Execute(string sql, params object[] args)
        {
            return DBConnection.Record<T>.Execute(sql, args);
        }

        /// <summary>
        /// 根据条件修改
        /// </summary>
        /// <param name="primaryKeyName">where字段名称（一个）</param>
        /// <param name="poco">修改的值，例如 new { Name = ma.Name, Tel = ma.Tel},请注意，这里一定要包含修改的字段的值</param>
        /// <param name="primaryKeyValue">where字段的值</param>
        /// <returns>影响的行数</returns>
        public int Update(string primaryKeyName, object poco, object primaryKeyValue)
        {
            return RayelinkWeixin.Model.DBConnection.GetInstance().Update(TableName, primaryKeyName, poco, primaryKeyValue);
        }

        /// <summary>
        /// 根据主键ID修改
        /// </summary>
        /// <param name="poco">修改的值，例如 new { Name = ma.Name, Tel = ma.Tel},请注意，这里一定要包含修改的字段的值</param>
        /// <param name="primaryKeyValue">主键ID的值</param>
        /// <returns>影响的行数</returns>
        public int Update(object poco, object primaryKeyValue)
        {
            return RayelinkWeixin.Model.DBConnection.GetInstance().Update(TableName, TableKey, poco, primaryKeyValue);
        }

        /// <summary>
        /// 修改当前对象
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>  
        public int Update(RayelinkWeixin.Model.DBConnection.Record<T> t)
        {
            CheckData(t);
            return Convert.ToInt32(t.Update());
        }

        /// <summary>
        /// 增加一个对象
        /// </summary>
        /// <param name="t">实体对象</param>
        /// <returns>新的主键ID（最后一次自增主键）</returns>
        public int Insert(RayelinkWeixin.Model.DBConnection.Record<T> t)
        {
            var newModel = CheckData(t);
            var ttt = newModel as RayelinkWeixin.Model.DBConnection.Record<T>;
            return Convert.ToInt32(ttt.Insert());
        }

        /// <summary>
        /// 删除一个对象(不建议使用此方法，一般时候逻辑删除靠谱)
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public int Delete(RayelinkWeixin.Model.DBConnection.Record<T> t)
        {
            return t.Delete();
        }

        /// <summary>
        /// 批量插入(传统的sql插入，当数据量大于等于500时自动使用使用SqlBulkCopy插入)
        /// </summary>
        /// <param name="pocos">要被插入指定列值POCO对象</param>        
        /// <param name="batchSize">每次执行的条数</param>   
        /// <returns>影响的行数</returns>
        public int BulkInsert(IList<T> pocos, bool isTransaction = true, int batchSize = 200)
        {
            if (pocos.Count >= 500)
            {
                if (BulkInsert(pocos))
                {
                    return pocos.Count;
                }
                else
                {
                    return 0;
                }
            }
            var result = DBConnection.Record<T>.BulkInsert(pocos, isTransaction, batchSize);
            return result;
        }

        /// <summary>
        /// 批量插入（使用SqlBulkCopy操作,当数据不足500时自动使用sql插入）
        /// </summary>
        /// <param name="pocos">指定要插入的列值的 poco 对象列表</param>
        /// <returns>影响行数是否大于一</returns>
        public bool BulkInsert(IList<T> pocos)
        {
            if (pocos.Count < 500)
            {
                return BulkInsert(pocos, true) > 0;
            }
            bool result = DBConnection.Record<T>.BulkInsert(pocos);
            return result;
        }

        /// <summary>
        /// 查询所有符合条件的数据，带有arg的查询
        /// </summary>
        /// <param name="sqls">查询条件，可以加排序方式</param>
        /// <param name="isContainDelete">是否查询status=0的数据，默认为否</param>
        /// <param name="arg">参数</param>
        /// <returns>实体对象集合</returns>
        public IEnumerable<T> Find(string sqls, bool isContainDelete = false, params object[] arg)
        {
            CheckWhere(ref sqls, isContainDelete);
            var result = DBConnection.Record<T>.Fetch(sqls, arg);
            return result;
        }

        /// <summary>
        /// 填充一个DataSet
        /// </summary>
        /// <param name="sql">查询条件</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public DataSet Fill(string sql, params object[] args)
        {
            DataSet ds = new DataSet();
            RayelinkWeixin.Model.DBConnection.Record<T>.Fill(ds, sql, args);
            return ds;
        }

        /// <summary>
        /// 填充一个DataTable
        /// </summary>
        /// <param name="sql">查询条件</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public DataTable FillTable(string sql, params object[] args)
        {
            DataTable dt = new DataTable();
            RayelinkWeixin.Model.DBConnection.Record<T>.Fill(dt, sql, args);
            return dt;
        }

        /// <summary>
        /// 根据指定的栏位计算出group的结果
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="groupColumnName">分组列（只能有一个）</param>
        /// <param name="where">查询条件</param>
        /// <param name="countColumnName"></param>
        /// <param name="isDistinct">是否Distinct</param>
        /// <param name="isContainDelete"></param>
        /// <returns></returns>
        public Dictionary<P, int> GetGroupValueCount<P>(string groupColumnName, string where, string countColumnName = "*", bool isDistinct = false, bool isContainDelete = false)
        {
            Dictionary<P, int> result = new Dictionary<P, int>();
            CheckWhere(ref where, isContainDelete);
            string sql = string.Empty;
            if (!isDistinct)
            {
                sql = string.Format("select {2} as GroupColumnName,count({0}) as GroupCount from {3} where {1} group by {2}", countColumnName, where, groupColumnName, TableName);
            }
            else
            {
                sql = string.Format("select {2} as GroupColumnName,count(distinct {0}) as GroupCount from {3} where {1}  group by {2}", countColumnName, where, groupColumnName, TableName);
            }

            using (DataSet ds = Fill(sql))
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        result.Add((P)Convert.ChangeType(dr["GroupColumnName"], typeof(P)), Convert.ToInt32(dr["GroupCount"]));
                    }

                }
            }
            return result;
        }

        /// <summary>
        /// 校验查询的排序规则，默认是根据主键ID倒序
        /// </summary>
        /// <param name="orderBy"></param>
        public void CheckOrderBy(ref string orderBy)
        {
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                orderBy = string.Format(" ORDER BY {0} DESC", TableKey);   //默认是主键ID倒序
            }
            else
            {
                if (!orderBy.TrimStart().StartsWith("order", StringComparison.CurrentCultureIgnoreCase))
                {
                    orderBy = string.Format(" ORDER BY {0}", orderBy);
                }
            }
        }

        /// <summary>
        /// 字段空值校验
        /// </summary>
        /// <param name="t"></param>
        public abstract T CheckData(RayelinkWeixin.Model.DBConnection.Record<T> t);

        #region 校验查询语句是否执行status的查询

        /// <summary>
        /// 校验查询语句是否执行status的查询
        /// </summary>
        /// <param name="where"></param>
        /// <param name="isContainDelete"></param>
        protected void CheckWhere(ref string where, bool isContainDelete)
        {
            if (!string.IsNullOrWhiteSpace(where) && !isContainDelete)
            {
                if (!string.IsNullOrWhiteSpace(StatusColumn) && !where.Contains(string.Format("{0}>0", StatusColumn)))
                {
                    where += string.Format(" AND {0}>0", StatusColumn);
                }
            }
            else if (!isContainDelete)
            {
                if (!string.IsNullOrWhiteSpace(StatusColumn) && !where.Contains(string.Format("{0}>0", StatusColumn)))
                {
                    where += string.Format(" WHERE {0}>0", StatusColumn);
                }
            }
            if (!string.IsNullOrWhiteSpace(where) && !where.TrimStart().StartsWith("where", StringComparison.CurrentCultureIgnoreCase))
            {
                where = " WHERE " + where.TrimStart();
            }
        }

        /// <summary>
        /// 连接where和order的sql
        /// </summary>
        /// <param name="where"></param>
        /// <param name="isContainDelete"></param>
        public void CheckWhere(ref string where, bool isContainDelete, string orderBy)
        {
            CheckWhere(ref where, isContainDelete);
            CheckOrderBy(ref orderBy);// 校验查询语句是否执行status的查询
            where = where + " " + orderBy;
        }
        #endregion



  

    }

    /// <summary>
    /// String扩展
    /// </summary>
    public static class StringExtension
    {
        #region 替换UnicodeCode字符
        /// <summary>
        /// 替换UnicodeCode字符
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ReplaceUnicodeCode(this string source)
        {
            source = source.Replace("\u0008", "");
            source = source.Replace("\u0009", "");
            source = source.Replace("\u000A", "");
            source = source.Replace("\u000B", "");
            source = source.Replace("\u000C", "");
            source = source.Replace("\u000D", "");
            source = source.Replace("\u0020", "");
            source = source.Replace("\u0022", "");
            source = source.Replace("\u0027", "");
            source = source.Replace("\u005C", "");
            source = source.Replace("\u00A0", "");
            source = source.Replace("\u2028", "");
            source = source.Replace("\u2029", "");
            source = source.Replace("\uFEFF", "");
            return source;
        }
        #endregion



    }
}
