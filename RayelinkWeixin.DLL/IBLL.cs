﻿using System.Collections.Generic;
using System.Data;

namespace RayelinkWeixin.DLL
{
    /// <summary>
    /// IBLL
    /// </summary>
    public interface IBLL
    {
        /// <summary>
        /// 查询符合条件的数据量
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="isContainDelete">是否查询status=0的数据，默认为否</param>
        /// <returns>符合条件的数据量</returns>
        int FindCount(string where, bool isContainDelete = false);

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="sql">sql</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        bool Exists(string sql, params object[] args);

        /// <summary>
        /// 执行sql，一般是修改，删除等sql命令,不带查询的命令
        /// </summary>
        /// <param name="sql">一般是修改，删除等sql命令</param>
        /// <param name="args"></param>
        /// <returns>影响的行数</returns>
        int Execute(string sql, params object[] args);

        /// <summary>
        /// 根据条件修改
        /// </summary>
        /// <param name="primaryKeyName">where字段名称（一个）</param>
        /// <param name="poco">修改的值，例如 new { Name = ma.Name, Tel = ma.Tel},请注意，这里一定要包含修改的字段的值</param>
        /// <param name="primaryKeyValue">where字段的值</param>
        /// <returns>影响的行数</returns>

        int Update(string primaryKeyName, object poco, object primaryKeyValue);


        /// <summary>
        /// 根据主键ID修改
        /// </summary>
        /// <param name="poco">修改的值，例如 new { Name = ma.Name, Tel = ma.Tel},请注意，这里一定要包含修改的字段的值</param>
        /// <param name="primaryKeyValue">主键ID的值</param>
        /// <returns>影响的行数</returns>
        int Update(object poco, object primaryKeyValue);

        /// <summary>
        /// 填充一个DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        DataSet Fill(string sql, params object[] args);

        /// <summary>
        /// 填充一个DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        DataTable FillTable(string sql, params object[] args);

        /// <summary>
        /// 根据指定的栏位计算出group的结果
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="groupColumnName">分组列（只能有一个）</param>
        /// <param name="where"></param>
        /// <param name="countColumnName"></param>
        /// <param name="isDistinct">是否Distinct</param>
        /// <param name="isContainDelete"></param>
        /// <returns></returns>
        Dictionary<P, int> GetGroupValueCount<P>(string groupColumnName, string where, string countColumnName = "*", bool isDistinct = false, bool isContainDelete = false);


        /// <summary>
        /// 校验查询的排序规则，默认是根据主键ID倒序
        /// </summary>
        /// <param name="orderBy"></param>
        void CheckOrderBy(ref string orderBy);

        /// <summary>
        /// 连接where和order的sql
        /// </summary>
        /// <param name="where"></param>
        /// <param name="isContainDelete"></param>
        void CheckWhere(ref string where, bool isContainDelete, string orderBy);
    }
}
