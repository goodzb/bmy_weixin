//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果重新生成代码，这些更改将会丢失。
//     生成时间 2017-06-01 17:57:24  by xiao99
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using RayelinkWeixin.Model;

namespace RayelinkWeixin.DLL
{
    /// <summary>
    /// 用户行为事件记录
    /// </summary>
    public partial class UserActionEventLogBLL : BaseBLL<UserActionEventLogInfo>
    {　
		#region 构造函数
	    /// <summary>
        /// 用户行为事件记录
        /// </summary>
        public UserActionEventLogBLL()
        {
            TableName = "UserActionEventLog";
            TableKey = "ActionId";
            StatusColumn = "ActionStatus";
        }
		#endregion

		#region 字段空值校验
		/// <summary>
        /// 字段空值校验
        /// </summary>
        /// <param name="t"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        public override UserActionEventLogInfo CheckData(RayelinkWeixin.Model.DBConnection.Record<UserActionEventLogInfo> t)
        {
		    var m = t as UserActionEventLogInfo;
			if (m.ActionSid.Equals(default(Guid)))
			{
				m.ActionSid = Guid.NewGuid();
			}

			if (string.IsNullOrWhiteSpace(m.OpenId))
			{
				m.OpenId = "";
			}
			else  if (m.OpenId.Length > 50)
			{
				m.OpenId= m.OpenId.Substring(0, 50).ReplaceUnicodeCode();      
			}

			if (string.IsNullOrWhiteSpace(m.ActionType))
			{
				m.ActionType = "";
			}
			else  if (m.ActionType.Length > 20)
			{
				m.ActionType= m.ActionType.Substring(0, 20).ReplaceUnicodeCode();      
			}

			if (string.IsNullOrWhiteSpace(m.ActionIp))
			{
				m.ActionIp = "";
			}
			else  if (m.ActionIp.Length > 80)
			{
				m.ActionIp= m.ActionIp.Substring(0, 80).ReplaceUnicodeCode();      
			}

			if (string.IsNullOrWhiteSpace(m.ReferUrl))
			{
				m.ReferUrl = "";
			}
			else  if (m.ReferUrl.Length > 500)
			{
				m.ReferUrl= m.ReferUrl.Substring(0, 500).ReplaceUnicodeCode();      
			}

			if (string.IsNullOrWhiteSpace(m.RequestUrl))
			{
				m.RequestUrl = "";
			}
			else  if (m.RequestUrl.Length > 500)
			{
				m.RequestUrl= m.RequestUrl.Substring(0, 500).ReplaceUnicodeCode();      
			}

			if (string.IsNullOrWhiteSpace(m.ActionName))
			{
				m.ActionName = "";
			}
			else  if (m.ActionName.Length > 60)
			{
				m.ActionName= m.ActionName.Substring(0, 60).ReplaceUnicodeCode();      
			}

			if (string.IsNullOrWhiteSpace(m.Parameter))
			{
				m.Parameter = "";
			}
			else  if (m.Parameter.Length > 200)
			{
				m.Parameter= m.Parameter.Substring(0, 200).ReplaceUnicodeCode();      
			}

			if (string.IsNullOrWhiteSpace(m.ActionDesc))
			{
				m.ActionDesc = "";
			}
			else  if (m.ActionDesc.Length > 200)
			{
				m.ActionDesc= m.ActionDesc.Substring(0, 200).ReplaceUnicodeCode();      
			}

			if (m.CreateDateTime.Year == 9999 || m.CreateDateTime.Year == 1971 || m.CreateDateTime.Year == 1)
			{
				m.CreateDateTime = DateTime.Now;
			}

			if (m.ActioneDateTime.Year == 9999 || m.ActioneDateTime.Year == 1971 || m.ActioneDateTime.Year == 1)
			{
				m.ActioneDateTime = DateTime.Now;
			}
		    return m;
		}
		#endregion
	}
}

