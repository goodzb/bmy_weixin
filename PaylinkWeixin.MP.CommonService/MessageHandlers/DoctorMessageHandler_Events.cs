﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：DoctorMessageHandler_Events
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-13 16:01:53
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using Senparc.Weixin.MP.Entities;
using System.Diagnostics;
using System.Web;

namespace PaylinkWeixin.MP.CommonService.MessageHandlers
{
    /// <summary>
    /// 
    /// </summary>
    public partial class DoctorMessageHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetWelcomeInfo()
        {
            //获取Senparc.Weixin.MP.dll版本信息
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(HttpContext.Current.Server.MapPath("~/bin/Senparc.Weixin.MP.dll"));
            var version = string.Format("{0}.{1}", fileVersionInfo.FileMajorPart, fileVersionInfo.FileMinorPart);
            //return string.Format(@"千山万水终相见 /:handclap
            return string.Format(@"千山万水终相见
以后看病不再难

这是国家卫计委支持推进的远程医疗服务，您在当地医疗机构通过视频看上海瑞金、中山、华山等各三甲医院大牌专家。
<a href ='https://h5.youzan.com/v2/feature/tf0kdsyq'>查看专家</a>",
                version);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hexEmoji"></param>
        /// <returns></returns>
        public static string emoji(int hexEmoji)
        {
            return char.ConvertFromUtf32(hexEmoji);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnTextOrEventRequest(RequestMessageText requestMessage)
        {
            return null;//返回null，则继续执行OnTextRequest或OnEventRequest
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_ClickRequest(RequestMessageEvent_Click requestMessage)
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_EnterRequest(RequestMessageEvent_Enter requestMessage)
        {      
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_LocationRequest(RequestMessageEvent_Location requestMessage)
        {            
            return null;
        }

        /// <summary>
        /// 扫码事件
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_ScanRequest(RequestMessageEvent_Scan requestMessage)
        {
            if (!string.IsNullOrEmpty(requestMessage.EventKey))
            {
                RayelinkWeixin.Data.PO.Extend.QrSceneExtend entity = new RayelinkWeixin.Data.PO.Extend.QrSceneExtend()
                {
                    Event = requestMessage.Event.ToString(),
                    EventKey = requestMessage.EventKey,
                    OpenID = requestMessage.FromUserName
                };
                int res = RayelinkWeixin.Data.Dao.QrSceneDao.Instance.Add(entity);
            }
            var baseResult = QrCodeMessageHandler(requestMessage);
            if (null != baseResult)
            {
                return baseResult;
            }
            var responseMessage = CreateResponseMessage<ResponseMessageText>();   
            responseMessage.Content = "欢迎进入帮忙医";
            //return responseMessage;
            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_ViewRequest(RequestMessageEvent_View requestMessage)
        {
            //说明：这条消息只作为接收，下面的responseMessage到达不了客户端，类似OnEvent_UnsubscribeRequest
            var responseMessage = CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "您点击了view按钮，将打开网页：" + requestMessage.EventKey;
            return responseMessage;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_MassSendJobFinishRequest(RequestMessageEvent_MassSendJobFinish requestMessage)
        {
            var responseMessage = CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "接收到了群发完成的信息。";
            return responseMessage;
        }

        /// <summary>
        /// 订阅（关注）事件
        /// </summary>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_SubscribeRequest(RequestMessageEvent_Subscribe requestMessage)
        {
            FileLogHelper.Instance.Log("--------------------------OnEvent_SubscribeRequest-----------------", "QrCode");
            var responseMessage = ResponseMessageBase.CreateFromRequestMessage<ResponseMessageText>(requestMessage);
            responseMessage.Content = GetWelcomeInfo();
            if (!string.IsNullOrEmpty(requestMessage.EventKey))
            {
                //responseMessage.Content += "\r\n============\r\n场景值：" + requestMessage.EventKey;
                RayelinkWeixin.Data.PO.Extend.QrSceneExtend entity = new RayelinkWeixin.Data.PO.Extend.QrSceneExtend()
                {
                    Event = requestMessage.Event.ToString(),
                    EventKey = requestMessage.EventKey.Split('_')[1],
                    OpenID = requestMessage.FromUserName
                };
                int res = RayelinkWeixin.Data.Dao.QrSceneDao.Instance.Add(entity);
            }
            if (requestMessage.EventKey.StartsWith("qrscene_"))
            {
                FileLogHelper.Instance.Log("--------------------------响应二维码其他的事件-----------------", "QrCode");
                var baseResult = QrCodeSubscribeHandler(requestMessage);    //响应二维码其他的事件           
            }
            FileLogHelper.Instance.Log("OnEvent_SubscribeRequest返回值:responseMessage=" + JsonHelper.ObjectToJSON(responseMessage), "QrCode");
            return responseMessage;
        }

        /// <summary>
        /// 退订
        /// 实际上用户无法收到非订阅账号的消息，所以这里可以随便写。
        /// unsubscribe事件的意义在于及时删除网站应用中已经记录的OpenID绑定，消除冗余数据。并且关注用户流失的情况。
        /// </summary>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_UnsubscribeRequest(RequestMessageEvent_Unsubscribe requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "有空再来";
            return responseMessage;
        }

        /// <summary>
        /// 事件之扫码推事件(scancode_push)
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_ScancodePushRequest(RequestMessageEvent_Scancode_Push requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之扫码推事件";
            return responseMessage;
        }

        /// <summary>
        /// 事件之扫码推事件且弹出“消息接收中”提示框(scancode_waitmsg)
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_ScancodeWaitmsgRequest(RequestMessageEvent_Scancode_Waitmsg requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之扫码推事件且弹出“消息接收中”提示框";
            return responseMessage;
        }

        /// <summary>
        /// 事件之弹出拍照或者相册发图（pic_photo_or_album）
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_PicPhotoOrAlbumRequest(RequestMessageEvent_Pic_Photo_Or_Album requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之弹出拍照或者相册发图";
            return responseMessage;
        }

        /// <summary>
        /// 事件之弹出系统拍照发图(pic_sysphoto)
        /// 实际测试时发现微信并没有推送RequestMessageEvent_Pic_Sysphoto消息，只能接收到用户在微信中发送的图片消息。
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_PicSysphotoRequest(RequestMessageEvent_Pic_Sysphoto requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之弹出系统拍照发图";
            return responseMessage;
        }

        /// <summary>
        /// 事件之弹出微信相册发图器(pic_weixin)
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_PicWeixinRequest(RequestMessageEvent_Pic_Weixin requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之弹出微信相册发图器";
            return responseMessage;
        }

        /// <summary>
        /// 事件之弹出地理位置选择器（location_select）
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_LocationSelectRequest(RequestMessageEvent_Location_Select requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之弹出地理位置选择器";
            return responseMessage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_User_Get_CardRequest(RequestMessageEvent_User_Get_Card requestMessage)
        {
            LogHelper.Info("领取卡券接口触发：用户OpenID:" + requestMessage.FromUserName + "用户自定义UserCode：" + requestMessage.UserCardCode + ",CardId=" + requestMessage.CardId);
            ServiceForWeChat.ServiceForWeChatSoapClient client = new ServiceForWeChat.ServiceForWeChatSoapClient();
            var pid = 0;
            var openid = requestMessage.FromUserName;
            ServiceForWeb.ServiceForWebSoapClient crmClient = new ServiceForWeb.ServiceForWebSoapClient();
            var webAccount = crmClient.CRM_WEB_LoginWebUserByOpenID(openid, WeixinConfig.crmPartner);
            if (webAccount != null)
            {
                pid = webAccount.PID;
            }
            client.CRM_WeChat_ApplyCoupon(pid, openid, requestMessage.UserCardCode, requestMessage.CardId);
            return base.OnEvent_User_Get_CardRequest(requestMessage);
        }
    }
}
