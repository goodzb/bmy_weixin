﻿using Senparc.Weixin.Context;
using Senparc.Weixin.MP.Entities;

namespace PaylinkWeixin.MP.CommonService.MessageHandlers
{
    /// <summary>
    /// 
    /// </summary>
    public class DoctorMessageContext : MessageContext<IRequestMessageBase, IResponseMessageBase>
    {

        /// <summary>
        /// 
        /// </summary>
        public DoctorMessageContext()
        {
            base.MessageContextRemoved += DoctorMessageContext_MessageContextRemoved;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DoctorMessageContext_MessageContextRemoved(object sender, WeixinContextRemovedEventArgs<IRequestMessageBase, IResponseMessageBase> e)
        {
            var messageContext = e.MessageContext as DoctorMessageContext;
            if (messageContext == null)
            {
                return;//如果是正常的调用，messageContext不会为null
            }
        }
    }
}
