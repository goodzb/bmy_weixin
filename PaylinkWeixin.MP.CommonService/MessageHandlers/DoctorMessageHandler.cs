﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：DoctorMessageHandler
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-13 15:52:27
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using RayelinkWeixin.Business;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Model;
using Senparc.Weixin.MP.Entities;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.MP.MessageHandlers;
using System.IO;

namespace PaylinkWeixin.MP.CommonService.MessageHandlers
{
    /// <summary>
    /// 
    /// </summary>
    public partial class DoctorMessageHandler : MessageHandler<DoctorMessageContext>
    {
        private string appKey = WeixinConfig.appID;
        private string appSecret = WeixinConfig.appSecret;
        private readonly int AppId = 1;
        private readonly AppInfo AppInfo = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputStream"></param>
        /// <param name="postModel"></param>
        /// <param name="maxRecordCount"></param>
        public DoctorMessageHandler(Stream inputStream, PostModel postModel, int maxRecordCount = 0) : base(inputStream, postModel, maxRecordCount)
        {
            //这里设置仅用于测试，实际开发可以在外部更全局的地方设置，
            //比如MessageHandler<MessageContext>.GlobalWeixinContext.ExpireMinutes = 3。
            WeixinContext.ExpireMinutes = 3;

            if (!string.IsNullOrEmpty(postModel.AppId))
            {
                appKey = postModel.AppId;//通过第三方开放平台发送过来的请求

                var appInfo = new ShareFunction().GetAppInfo(appKey);
                if (null != appInfo)
                {
                    AppInfo = appInfo;
                    AppId = appInfo.AppId;
                    appSecret = appInfo.AppSecret;
                    appKey = appInfo.AppKey;
                }
            }

            //在指定条件下，不使用消息去重
            base.OmitRepeatedMessageFunc = requestMessage =>
            {
                var textRequestMessage = requestMessage as RequestMessageText;
                if (textRequestMessage != null && textRequestMessage.Content == "容错")
                {
                    return false;
                }
                return true;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override Senparc.Weixin.MP.Entities.IResponseMessageBase DefaultResponseMessage(Senparc.Weixin.MP.Entities.IRequestMessageBase requestMessage)
        {
            //var responseMessage = this.CreateResponseMessage<ResponseMessageText>();
            //responseMessage.Content = "这条消息来自DefaultResponseMessage。";
            //return responseMessage;
            return null;
        }
    }
}
