﻿using RayelinkWeixin.Business;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.DLL;
using RayelinkWeixin.WxApi;
using Senparc.Weixin.MP.Entities;
using System;
using System.Threading.Tasks;

namespace PaylinkWeixin.MP.CommonService.MessageHandlers
{
    /// <summary>
    /// 二维码响应处理
    /// </summary>
    public partial class DoctorMessageHandler
    {
        #region 扫描事件处理
        /// <summary>
        /// 扫描事件处理
        /// </summary>
        /// <param name="requestMessage">请求对象</param>
        /// <param name="sendMessage">是否是关注事件的响应，如果是需要将这次的相应使用客服发出给用户</param>
        /// <returns></returns>
        public ResponseMessageBase QrCodeMessageHandler(RequestMessageEvent_Scan requestMessage, bool subscribeEvent = false)
        {
            ResponseMessageBase result = null;
            int qrSceneId = 0;
            if (!string.IsNullOrEmpty(requestMessage.EventKey))
            {
                qrSceneId = Convert.ToInt32(requestMessage.EventKey);
                result = ScanMessage(requestMessage.FromUserName, qrSceneId, false);
            }
            return result;
        }
        #endregion

        #region 处理二维码关注响应事件

        /// <summary>
        /// 处理二维码关注响应事件
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public ResponseMessageBase QrCodeSubscribeHandler(RequestMessageEvent_Subscribe requestMessage)
        {
            FileLogHelper.Instance.Log("--------------------------QrCodeSubscribeHandler-----------------", "QrCode");
            ResponseMessageBase result = null;
            int qrSceneId = 0;
            if (!string.IsNullOrEmpty(requestMessage.EventKey))
            {
                qrSceneId = Convert.ToInt32(requestMessage.EventKey.Replace("qrscene_", ""));
                FileLogHelper.Instance.Log("QrCodeSubscribeHandler:qrSceneId=" + qrSceneId, "QrCode");
                result = ScanMessage(requestMessage.FromUserName, qrSceneId, true);
            }
            return result;
        }
        #endregion

        #region 处理二维码扫描回复消息

        /// <summary>
        /// 处理二维码扫描回复消息
        /// </summary>
        /// <param name="openid">微信粉丝标识</param>
        /// <param name="qrSceneId">二维码场景ID</param>
        /// <param name="subscribeEvent">是否是关注事件</param>
        /// <returns></returns>
        private ResponseMessageBase ScanMessage(string openid, int qrSceneId, bool subscribeEvent = false)
        {
            FileLogHelper.Instance.Log("--------------------------ScanMessage-----------------", "QrCode");
            FileLogHelper.Instance.Log("ScanMessage:qrSceneId=" + qrSceneId, "QrCode");
            ResponseMessageText result = null;
            FileLogHelper.Instance.Log("ScanMessage:qrSceneId=" + qrSceneId + " AppId:" + AppId, "QrCode");
            var qrCodeInfo = BusinessContent.QRCode.FindOne("QRSceneId=@0 AND AppId=@1", qrSceneId, AppId);
            if (null != qrCodeInfo)
            {
                FileLogHelper.Instance.Log("ScanMessage:qrCodeInfo=" + JsonHelper.ObjectToJSON(qrCodeInfo), "QrCode");
                Task.Factory.StartNew(() =>
                {
                    FileLogHelper.Instance.Log("根据配置项发送二维码其他的图文相应事件", "QrCode");
                    SendQrCode_Article(AppId, qrSceneId, openid);// 根据配置项发送二维码其他的图文相应事件
                });
                switch (qrCodeInfo.QRcodeType)      // 1:文本,2:图片,3:组件,4:图片,7:mp素材
                {
                    case 1:
                        result = CreateResponseMessage<ResponseMessageText>();
                        result.Content = qrCodeInfo.QRcodeValue;
                        FileLogHelper.Instance.Log("subscribeEvent:" + subscribeEvent, "QrCode");
                        if (subscribeEvent)
                        {
                            FileLogHelper.Instance.Log("ScanMessage>>>>>>发送文本信息:openid:" + openid, "QrCode");
                            new Custom(WeixinConfig.appID, WeixinConfig.appSecret).SendText(openid, qrCodeInfo.QRcodeValue);
                        }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                FileLogHelper.Instance.Log("没有找到二维码", "QrCode");
            }
            FileLogHelper.Instance.Log("ScanMessage:result=" + JsonHelper.ObjectToJSON(result), "QrCode");
            return result;
        }
        #endregion

        #region 根据配置项发送二维码其他的图文相应事件
        /// <summary>
        /// 根据配置项发送二维码其他的图文相应事件
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="sceneId"></param>
        /// <param name="openid"></param>
        private void SendQrCode_Article(int appid, int sceneId, string openid)
        {
            FileLogHelper.Instance.Log("--------------------------SendQrCode_Article-----------------", "QrCode");
            var articleList = new ShareFunction(appid).QrCode_ArticleConfig(sceneId, appid);
            if (null != articleList)
            {
                FileLogHelper.Instance.Log("SendQrCode_Article:articleList=" + JsonHelper.ObjectToJSON(articleList), "QrCode");
                new Custom(appKey, appSecret).SendNews(openid, articleList);
                FileLogHelper.Instance.Log("发送图文消息", "QrCode");
            }
            else
            {
                FileLogHelper.Instance.Log("没有找到根据配置项发送二维码其他的图文相应事件", "QrCode");
            }
        }
        #endregion
    }
}

