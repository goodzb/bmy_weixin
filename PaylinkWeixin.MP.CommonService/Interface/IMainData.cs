﻿using System.Collections.Generic;

namespace PaylinkWeixin.MP.CommonService.Interface
{
    interface IMainData
    {
        /// <summary>
        /// 获取所有科室
        /// </summary>
        /// <returns>object {id:科室id name:科室name}</returns>
        List<object> GetDepartmentList();

        /// <summary>
        /// 获取职称
        /// </summary>
        /// <returns>object {id:职称id name:职称name}</returns>
        List<object> GetTitleList();

        /// <summary>
        /// 获取医生列表
        /// </summary>
        /// <param name="deptID">科室ID</param>
        /// <param name="titleID">职务ID</param>
        /// <param name="keyword">医生名字或疾病名字（模糊匹配）</param>
        /// <param name="pageIndex">当前页数(默认加载20笔数据)</param>
        /// <returns>医生列表{
        ///          image:医生头像，name:医生名字 deptName:医生科室 
        ///          titleName:职称名称 orgName:机构名称 skillIn:擅长
        ///          rating:评论等级 几星
        ///          comments:评论数量
        ///          }
        /// </returns>
        List<object> GetDoctorList(int? deptID, int? titleID, string keyword, int pageIndex = 0);

        /// <summary>
        /// 根据医生ID获取单个医生
        /// </summary>
        /// <param name="docID">医生ID</param>
        /// <returns>医生{
        ///          image:医生头像，name:医生名字 deptName:医生科室 
        ///          titleName:职称名称 orgName:机构名称 skillIn:擅长
        ///          docInfo:医生详情
        ///          rating:评论等级 几星
        ///          comments:评论数量
        ///          }
        /// </returns>
        object GetDoctorSingle(int docID);

        /// <summary>
        /// 获取医院列表
        /// </summary>
        /// <param name="cityID">城市ID</param>
        /// <param name="orgName">诊所名称</param>
        /// <param name="pageIndex">当前页数(默认加载20笔数据)</param>
        /// <returns>医院
        ///       {
        ///         image:医院图片,comments:评论数量
        ///         rating:评论等级 几星,hospitalName:医院名称
        ///         address:医院地址 coordinate:坐标
        ///       }
        /// </returns>
        List<object> GetHospitalList(int? cityID, string orgName, int pageIndex = 0);

        /// <summary>
        /// 根据医院ID获取医院详情
        /// </summary>
        /// <param name="hospitalID">医院ID</param>
        /// <returns>
        /// 医院
        ///       {
        ///         image:医院图片,comments:评论数量 hospitalTel:医院电话
        ///         rating:评论等级 几星,hospitalName:医院名称
        ///         address:医院地址 coordinate:坐标,hospitalInfo:医院详情
        ///         carousel:["url1","url2"]轮播图片
        ///       }
        ///       </returns>
        object GetHospitalSingle(int hospitalID);
    }
}
