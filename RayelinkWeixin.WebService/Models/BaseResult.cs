﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayelinkWeixin.WebService.Models
{
    public class BaseResult
    {
        public BaseResult()
        {
            Code = "200";
            ErrorMsg = "";
        }

        public string Code { get;private set; }
        public string ErrorMsg { get; private set; }
        public object Content { get; private set; }
        
        public void SetContent(object obj)
        {
            Content = obj;
        }
        public void SetEror(string msg,string code)
        {
            Code = code;
            ErrorMsg = msg;
        }
    }
}