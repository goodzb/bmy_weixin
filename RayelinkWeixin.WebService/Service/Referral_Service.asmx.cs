﻿using RayelinkWeixin.Data.Dao;
using RayelinkWeixin.Data.PO.Extend;
using RayelinkWeixin.Data.Repository;
using RayelinkWeixin.WebService.Mapper;
using RayelinkWeixin.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;

namespace RayelinkWeixin.WebService.Service
{
    /// <summary>
    /// Summary description for Referral_Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Referral_Service : System.Web.Services.WebService
    {

        /// <summary>
        /// 分页获取转诊列表
        /// </summary>
        [XmlInclude(typeof(List<ReferralModel>))]
        [WebMethod]
        public BaseResult GetReferralsByPage(string status,int organizationId,int pagesize,int pagenumber)
        {
            BaseResult res = new BaseResult();
            List<ReferralExtend> refs= ReferralDao.Instance.FindByPageExport(status, organizationId, pagesize, pagenumber);
            List<ReferralModel>  list = ReferralMapper.ReferralDo2Dto(refs);
            res.SetContent(list);
            return res;
        }

        /// <summary>
        /// 修改转诊单状态
        /// </summary>
        [WebMethod]
        public BaseResult UpdateReferral(int refId,string status,string appointmentNo,string appointDate)
        {
            BaseResult res = new BaseResult();
            StringBuilder updateField = new StringBuilder();
            if (!string.IsNullOrEmpty(status))
            {
                updateField.Append(" [Status]='"+status+"',");
            }
            if (!string.IsNullOrEmpty(appointmentNo))
            {
                updateField.Append(" [AppointmentNo]='"+appointmentNo+"',");
            }
            if (!string.IsNullOrEmpty(appointDate))
            {
                updateField.Append(" [AppointmentTime]='"+appointDate+"'");
            }
            if (updateField.Length>0)
            {
                updateField= updateField.Remove(updateField.Length,1);
             }

            int r= ReferralDao.Instance.Update(updateField.ToString(),"Id="+refId);
            res.SetContent(r);
            return res;
        }
    }
}
