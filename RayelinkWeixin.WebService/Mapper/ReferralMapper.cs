﻿using RayelinkWeixin.Data.PO.Extend;
using RayelinkWeixin.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RayelinkWeixin.WebService.Mapper
{
    public class ReferralMapper
    {
        public static ReferralModel ReferralDo2Dto(ReferralExtend refExtend)
        {
            ReferralModel refModel = new ReferralModel
            {
                Age = refExtend.Age,
                CreaterId = refExtend.CreaterId,
                CreaterName = refExtend.CreaterName,
                CreateTime = refExtend.CreateTime,
                Desc = refExtend.Desc,
                DocId = refExtend.DocId,
                Id = refExtend.Id,
                Imgs = refExtend.ImgsStr,
                Name = refExtend.Name,
                OrganizationId = refExtend.ClientId,
                Phone = refExtend.Phone,
                Sex = refExtend.Sex,
                Status = refExtend.Status
            };
            return refModel;
        }

        public static List<ReferralModel> ReferralDo2Dto(List<ReferralExtend> extends)
        {
            List<ReferralModel> models = new List<ReferralModel>();
            foreach (var item in extends)
            {
                models.Add(ReferralDo2Dto(item));
            }
            return models;
        }
    }
}