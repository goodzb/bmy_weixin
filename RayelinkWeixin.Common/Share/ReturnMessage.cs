﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace RayelinkWeixin.Common.Share
{
    /// <summary>
    /// 方法的返回对象
    /// </summary>
    [Serializable]
    public class ReturnMessage
    {
        private IDictionary<string, object> m_Data = new Dictionary<string, object>();

        /// <summary>
        /// 构造函数
        /// </summary>
        public ReturnMessage()
        {
            this.IsSuccess = false;
            this.Text = "";
            this.Message = "";
            this.ReturnUrl = "";
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="isSuccess">默认是true还是false</param>
        public ReturnMessage(bool isSuccess)
        {
            this.IsSuccess = isSuccess;
            this.Text = "";
            this.Message = "";
            this.ReturnUrl = "";
        }

        private bool _isSuccess;
        /// <summary>
        /// 操作是否成功
        /// </summary>
        public bool IsSuccess
        {
            get { return _isSuccess; }
            set
            {
                if (value && string.IsNullOrWhiteSpace(Text))
                {
                    Text = "操作成功";
                }
                _isSuccess = value;
            }
        }

        /// <summary>
        /// 返回信息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 返回单项数据信息
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// 返回跳转地址
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// 错误代码，默认为0
        /// </summary>
        public int ErrCode { get; set; }

        /// <summary>
        /// 返多项值,以字典形式返回
        /// </summary>
        [XmlIgnore]
        public IDictionary<string, object> ResultData
        {
            get { return m_Data; }
            set { m_Data = value; }
        }

        /// <summary>
        /// 异常信息
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public Exception Exception { get; set; }

        /// <summary>
        /// ToJsonString
        /// </summary>
        /// <returns></returns>
        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                DateFormatString = "yyyy-MM-dd HH:mm:ss"
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static implicit operator bool(ReturnMessage message)
        {
            return message != null ? message.IsSuccess : false;
        }

    }
}
