﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// Bitmap辅助类
    /// </summary>
    public class ImageHelper
    {
        #region 加载一张图片

        /// <summary>
        /// 加载一张图片（先生成无logo文件，在生成有log文件）
        /// </summary>
        /// <param name="directoryName">目录名称</param>
        /// <param name="appid">微信标识</param>
        /// <param name="imgsrc">原图URL</param>
        /// <param name="logosrc">logoURL</param>   
        /// <returns></returns>
        public static string LoadUrl(string directoryName, int appid, string imgsrc, string logosrc = "")
        {
            string baseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
            string res = string.Empty;
            if (!string.IsNullOrWhiteSpace(imgsrc))
            {
                string[] imgpaths = imgsrc.Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                string name = string.Empty;
                string size = string.Empty;
                string piclocalpath = string.Empty;
                if (imgsrc.ToLower().Contains("wx.qlogo.cn"))
                {
                    name = imgpaths[3];
                    size = imgpaths[4];
                    int zi = 0;
                    if (!int.TryParse(size, out zi))
                    {
                        size = appid.ToString();
                        name = TextHelper.MD5(imgsrc);
                    }
                }
                else
                {
                    name = Guid.NewGuid().ToString().ToLower();
                    size = "200";
                }

                if (!string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(size))
                {
                    piclocalpath = Path.Combine(baseDirectory, directoryName, string.Format("{0}/{1}/{2}.jpg", appid, size, name));
                    string path = Path.Combine(baseDirectory, directoryName, string.Format("{0}/{1}", appid, size));

                    if (!File.Exists(piclocalpath))
                    {
                        WebClient wc = new WebClient();

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        wc.DownloadFile(imgsrc, piclocalpath);
                    }

                    #region 处理logo合成
                    if (!string.IsNullOrWhiteSpace(logosrc))
                    {
                        string corelogopicpath = Path.Combine(baseDirectory, directoryName, "core.jpg");
                        string logopath = Path.Combine(baseDirectory, directoryName, string.Format("{0}/logo", appid));  //logo所在文件夹
                        string logoName = Path.GetFileName(logosrc);
                        string logoExtension = Path.GetExtension(logosrc);
                        logoName = logoName.Replace(logoExtension, "");
                        string logopiclocalpath = Path.Combine(baseDirectory, directoryName, string.Format("{0}/logo/{1}.jpg", appid, logoName));  //logo文件
                        string qrlogopiclocalpath = Path.Combine(baseDirectory, directoryName, string.Format("{0}/logo/{1}_logo.jpg", appid, name));  //合称后文件

                        if (!File.Exists(logopiclocalpath))
                        {
                            WebClient wc = new WebClient();

                            if (!Directory.Exists(logopath))
                            {
                                Directory.CreateDirectory(logopath);
                            }
                            wc.DownloadFile(logosrc, logopiclocalpath);
                        }
                        Bitmap bg = new Bitmap(piclocalpath);
                        Bitmap logo = new Bitmap(logopiclocalpath);
                        Bitmap corelogo = new Bitmap(corelogopicpath);
                        logo = Resize(logo, 75, 75);

                        int start_x = (bg.Width - logo.Width) / 2;
                        int start_y = (bg.Height - logo.Height) / 2;
                        Point start = new Point(start_x, start_y);

                        Bitmap result = Ps(bg, logo, start);
                        result = Ps(bg, logo, start);

                        start_x = (bg.Width - corelogo.Width) / 2;
                        start_y = (bg.Height - corelogo.Height) / 2;
                        start = new Point(start_x, start_y);
                        result = Ps(result, corelogo, start);

                        if (File.Exists(qrlogopiclocalpath))
                        {
                            File.Delete(qrlogopiclocalpath);
                        }
                        result.Save(qrlogopiclocalpath, ImageFormat.Jpeg);
                        bg.Dispose();
                        logo.Dispose();
                        corelogo.Dispose();
                    } 
                    #endregion
                }
                string requestPath = TextHelper.BaseUrl();

                if (string.IsNullOrWhiteSpace(logosrc))  //无_logo
                {
                    res = string.Format("{0}{1}", requestPath, directoryName + string.Format("/{0}/{1}/{2}.jpg", appid, size, name));
                }
                else
                {
                    res = string.Format("{0}{1}", requestPath, directoryName + string.Format("/{0}/{1}/{2}_logo.jpg", appid, "logo", name));
                }
            }
            return res;
        }
        #endregion

        #region 图像尺寸调节

        /// <summary>
        /// 图像尺寸调节
        /// </summary>
        /// <param name="b">原始图像</param>
        /// <param name="dstWidth">目标宽度</param>
        /// <param name="dstHeight">目标高度</param>
        /// <returns></returns>
        public static Bitmap Resize(Bitmap b, int dstWidth, int dstHeight)
        {
            Bitmap dstImage = new Bitmap(dstWidth, dstHeight);
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(dstImage);

            // 设置插值模式
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            // 设置平滑模式
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            g.DrawImage(b,
              new System.Drawing.Rectangle(0, 0, dstImage.Width, dstImage.Height),
              new System.Drawing.Rectangle(0, 0, b.Width, b.Height),
              System.Drawing.GraphicsUnit.Pixel);
            g.Save();
            g.Dispose();

            return dstImage;
        }

        #endregion

        #region Ps
        /// <summary>
        /// Ps
        /// </summary>
        /// <param name="bg"></param>
        /// <param name="avatar"></param>
        /// <param name="p">ps的起始位置</param>
        /// <returns></returns>
        public static Bitmap Ps(Bitmap bg, Image avatar, Point p)
        {
            //如果原图片是索引像素格式之列的，则需要转换
            if (IsPixelFormatIndexed(bg.PixelFormat))
            {
                Bitmap bmp = new Bitmap(bg.Width, bg.Height, PixelFormat.Format32bppArgb);
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.DrawImage(bg, 0, 0, bg.Width, bg.Height);
                    g.DrawImage(avatar, p.X, p.Y, avatar.Width, avatar.Height);
                    g.Dispose();
                }
                //下面的水印操作，就直接对 bmp 进行了
                return bmp;
            }
            else //否则直接操作
            {
                //直接对img进行水印操作
                using (Graphics g = Graphics.FromImage(bg))
                {
                    g.DrawImage(bg, 0, 0, bg.Width, bg.Height);
                    g.DrawImage(avatar, p.X, p.Y, avatar.Width, avatar.Height);
                    g.Dispose();
                }
            }
            return bg;
        }

        #endregion

        #region 会产生graphics异常的PixelFormat
        /// <summary>
        /// 会产生graphics异常的PixelFormat
        /// </summary>
        private static PixelFormat[] indexedPixelFormats = { PixelFormat.Undefined, PixelFormat.DontCare,
            PixelFormat.Format16bppArgb1555, PixelFormat.Format1bppIndexed, PixelFormat.Format4bppIndexed,
            PixelFormat.Format8bppIndexed
        };
        #endregion

        #region  判断图片的PixelFormat 是否在 引发异常的 PixelFormat 之中

        /// <summary>
        /// 判断图片的PixelFormat 是否在 引发异常的 PixelFormat 之中
        /// </summary>
        /// <param name="imgPixelFormat">原图片的PixelFormat</param>
        /// <returns></returns>
        private static bool IsPixelFormatIndexed(PixelFormat imgPixelFormat)
        {
            foreach (PixelFormat pf in indexedPixelFormats)
            {
                if (pf.Equals(imgPixelFormat)) return true;
            }
            return false;
        }
        #endregion
    }
}
