﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：DateHelper
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-25 15:13:19
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using System;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// 
    /// </summary>
    public static class DateHelper
    {
        /// <summary>
        /// 一周开始时间
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime WeekStartDay(this DateTime dt)
        {
            int weeknow = Convert.ToInt32(dt.DayOfWeek);
            int daydiff = (-1) * weeknow + 1;
            return dt.AddDays(daydiff);
        }

        /// <summary>
        /// 一周结束时间
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime WeekEndDay(this DateTime dt)
        {
            int weeknow = Convert.ToInt32(dt.DayOfWeek);
            int dayadd = 7 - weeknow;
            return dt.AddDays(dayadd);
        }

        /// <summary>
        /// 转成yyyyMMdd格式
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string yyyyMMdd(this DateTime dt)
        {
            return dt.ToString("yyyyMMdd");
        }
    }
}
