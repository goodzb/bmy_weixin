﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：LogHelper
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-14 12:01:59
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using log4net;
using System;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// 
    /// </summary>
    public class LogHelper
    {
        #region 属性

        private static ILog _fiLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region ERROR
        /// <summary>
        /// 往文件写入错误级别的日志
        /// </summary>
        /// <param name="message">日志信息</param>
        public static void Error(object message)
        {
            _fiLog.Error(message);
        }

        /// <summary>
        /// 往文件写入错误级别的日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常信息</param>
        public static void Error(object message, Exception exception)
        {
            _fiLog.Error(message, exception);
        }

        /// <summary>
        /// Exception
        /// </summary>
        /// <param name="exception"></param>
        public static void Exception(Exception exception)
        {
            _fiLog.Error(exception.Message, exception);
        }
        #endregion

        #region Warning
        /// <summary>
        /// 往文件写入警告级别的日志
        /// </summary>
        /// <param name="message">日志信息</param>
        public static void Waring(object message)
        {
            _fiLog.Warn(message);
        }

        /// <summary>
        /// 往文件写入警告级别的日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常信息</param>
        public static void Waring(object message, Exception exception)
        {
            _fiLog.Warn(message, exception);
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static void ConsoleWriteLine(string message)
        {
            Console.WriteLine(message);
            _fiLog.Debug(message);
        }

        #region Debug
        /// <summary>
        /// 往文件写入调试级别的日志
        /// </summary>
        /// <param name="message">日志信息</param>
        public static void Debug(object message)
        {            
            _fiLog.Debug(message);
        }

        /// <summary>
        /// 往文件写入调试级别的日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常信息</param>
        public static void Debug(object message, Exception exception)
        {
            _fiLog.Debug(message, exception);
        }
        #endregion

        #region INFO
        /// <summary>
        /// 往文件写入信息级别的日志
        /// </summary>
        /// <param name="message">日志信息</param>
        public static void Info(object message)
        {
            _fiLog.Info(message);
        }

        /// <summary>
        /// 往文件写入信息级别的日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常信息</param>
        public static void Info(object message, Exception exception)
        {
            _fiLog.Info(message, exception);
        }

        #endregion
    }
}
