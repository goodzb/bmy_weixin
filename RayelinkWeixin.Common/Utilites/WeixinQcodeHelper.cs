﻿using Newtonsoft.Json;
namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// 
    /// </summary>
    public class WeixinQcodeHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="scene_str">场景值ID（字符串形式的ID），字符串类型，长度限制为1到64，仅永久二维码支持此字段  </param>
        /// <returns></returns>
        public static string CreateTicket(string accessToken,string scene_id)
        {

            string url = string.Format("https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={0}", accessToken);
            string WeiXin_QrCodeTicket_Create_JsonString = "{\"action_name\": \"QR_LIMIT_SCENE\", \"action_info\": {\"scene\": {\"scene_id\":{0}}}}";
            string postData = WeiXin_QrCodeTicket_Create_JsonString.Replace("{0}", scene_id);

            string result =HttpHelper.PostReq(url, postData);
            Ticket ticket = JsonConvert.DeserializeObject<Ticket>(result); 

            return ticket.ticket;
        }
        
    }
    public class Ticket
    {

        private string _ticket;
        /// <summary>
        /// 
        /// </summary>
        private string _expire_seconds;

        /// <summary>
        /// 凭借此ticket可以在有效时间内换取二维码。  
        /// </summary>
        public string ticket
        {
            get { return _ticket; }
            set { _ticket = value; }
        }

        /// <summary>  
        /// 凭证有效时间，单位：秒  
        /// </summary>  
        public string expire_seconds
        {
            get { return _expire_seconds; }
            set { _expire_seconds = value; }
        }
    }
    public class ErrorMessage
    {
        //{"errcode":40001,"errmsg":"invalid credential"} AppId AppSecret   配置错误，或AccessToken 过期

        public string ErrCode { get; set; }

        public string ErrMsg { get; set; }

        public bool TokenExpired
        {
            get { return ErrCode == "40001"; }
        }
    }
}
