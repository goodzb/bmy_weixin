﻿using System;
using System.Web;
using System.Web.Security;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// 
    /// </summary>
    public class CookieHelper
    {
        /// <summary>
        ///  新增Cookies
        /// </summary>
        /// <param name="Response">Response</param>
        /// <param name="Cookieskey">Cookieskey</param>
        /// <param name="Value">Value</param>
        /// <param name="minutes">失效的分钟</param>
        public static void AddCookies(HttpResponseBase Response, string Cookieskey, string Value, int minutes)
        {
            Response.Cookies[Cookieskey].Value = Value;
            Response.Cookies[Cookieskey].Expires = DateTime.Now.AddMinutes(minutes);
        }

        public static void AddCookies(string Cookieskey, string Value, int minutes)
        {
            HttpContext.Current.Response.Cookies[Cookieskey].Value = Value;
            HttpContext.Current.Response.Cookies[Cookieskey].Expires = DateTime.Now.AddMinutes(minutes);
        }

        /// <summary>
        ///  根据Key获取值
        /// </summary>
        /// <param name="Request"></param>
        /// <param name="Cookieskey"></param>
        /// <returns></returns>
        public static string GetValueByCookieskey(HttpRequestBase Request, string Cookieskey)
        {
            string cookiesValue = string.Empty;
            if (Request.Cookies[Cookieskey] != null)
            {
                cookiesValue = Request.Cookies[Cookieskey].Value;
            }
            return cookiesValue;
        }

        /// <summary>
        ///  根据Key获取值
        /// </summary>
        /// <param name="Request"></param>
        /// <param name="Cookieskey"></param>
        /// <returns></returns>
        public static string GetValueByCookieskey(string Cookieskey)
        {
            string cookiesValue = string.Empty;
            if (HttpContext.Current.Request.Cookies[Cookieskey] != null)
            {
                cookiesValue = HttpContext.Current.Request.Cookies[Cookieskey].Value;
            }
            return cookiesValue;
        }

        /// <summary>
        ///  删除Cookies
        /// </summary>
        /// <param name="Request">Request</param>
        /// <param name="Response">Response</param>
        /// <param name="Cookieskey">Cookieskey</param>
        public static void RemoveCookiesByCookieskey(HttpRequestBase Request, HttpResponseBase Response, string Cookieskey)
        {
            HttpCookie cookies = Request.Cookies[Cookieskey];
            if (cookies != null)
            {
                cookies.Expires = DateTime.Today.AddDays(-1);
                Response.Cookies.Add(cookies);
            }
        }

        /// <summary>
        ///  删除Cookies
        /// </summary>
        /// <param name="Request">Request</param>
        /// <param name="Response">Response</param>
        /// <param name="Cookieskey">Cookieskey</param>
        public static void RemoveCookiesByCookieskey(string Cookieskey)
        {
            HttpCookie cookies = HttpContext.Current.Request.Cookies[Cookieskey];
            if (cookies != null)
            {
                cookies.Expires = DateTime.Today.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(cookies);
            }
        }




        /// <summary>
        /// 写入加密cookie
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="minute"></param>
        public static void WriteEncryptCookie(string name, string value, int minute = 43200)
        {
            if (HttpContext.Current != null)
            {
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1011, name, DateTime.Now, DateTime.Now.AddMinutes(minute), true, value, FormsAuthentication.FormsCookiePath);
                string encryptTicket = FormsAuthentication.Encrypt(ticket);
                HttpCookie cookie = new HttpCookie(name, encryptTicket);

                if (minute > 0)
                {
                    cookie.Expires = DateTime.Now.AddMinutes(minute);
                }
                HttpContext.Current.Response.AppendCookie(cookie);
            }
        }
        /// <summary>
        /// 写入加密cookie
        /// </summary>
        /// <param name="name"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="minute">分钟</param>
        public static void WriteEncryptCookie(string name, string key, string value, int minute = 43200)
        {
            if (HttpContext.Current != null)
            {
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1011, name, DateTime.Now, DateTime.Now.AddMinutes(minute), true, value, FormsAuthentication.FormsCookiePath);
                string encryptTicket = FormsAuthentication.Encrypt(ticket);
                HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
                if (cookie == null)
                {
                    cookie = new HttpCookie(name);
                }
                if (minute > 0)
                {
                    cookie.Expires = DateTime.Now.AddMinutes(minute);
                }
                cookie[key] = encryptTicket;
                HttpContext.Current.Response.AppendCookie(cookie);
            }
        }

        /// <summary>
        /// 读取加密cookie
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string ReadEncryptCookie(string name)
        {
            if (HttpContext.Current != null)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
                if (cookie != null)
                {
                    try
                    {
                        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                        return ticket.UserData;
                    }
                    catch (Exception)
                    {
                        return string.Empty;
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 读取加密cookie
        /// </summary>
        /// <param name="name"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ReadEncryptCookie(string name, string key)
        {
            if (HttpContext.Current != null)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
                if (cookie != null && cookie[key] != null)
                {
                    try
                    {
                        string value = cookie[key];
                        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(value);
                        return ticket == null ? string.Empty : ticket.UserData;
                    }
                    catch
                    {
                    }
                }
            }
            return string.Empty;
        }


    }
}
