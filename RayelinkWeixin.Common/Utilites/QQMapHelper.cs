﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：QQLbsHelper
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-27 9:34:51
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


using Newtonsoft.Json;
using RayelinkWeixin.Const.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// QQ 地图的WebService
    /// </summary>
    public class QQMapHelper
    {
        /// <summary>
        /// 逆地址转换API
        /// </summary>
        public static string reverseAddrTransApi = "http://apis.map.qq.com/ws/geocoder/v1/?coord_type=1&location={0},{1}&key={2}&get_poi=0";

        public static string distanceApi = "http://apis.map.qq.com/ws/distance/v1/?mode=driving&from={0}&to={1}&key={2}";

        public static string GetAddress(string latitude, string longitude)
        {
            var url = string.Format(reverseAddrTransApi, latitude, longitude, WeixinConfig.qqMapKey);
            var str = HttpGet(url);
            return str;
        }

        private const double EARTH_RADIUS = 6378.137; //地球半径
        private static double rad(double d)
        {
            return d * Math.PI / 180.0;
        }


        public static double GetDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radLat1 = rad(lat1);
            double radLat2 = rad(lat2);
            double a = radLat1 - radLat2;
            double b = rad(lng1) - rad(lng2);
            double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2) +
             Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(b / 2), 2)));
            s = s * EARTH_RADIUS;
            s = Math.Round(s * 10000) / 10000;
            return s;
        }

        /// <summary>
        /// 获取距离
        /// </summary>
        /// <param name="from">39.071510,117.190091</param>
        /// <param name="toList">39.071510,117.190091的List</param>
        /// <returns></returns>
        public static QQDistinct GetDistince(string from, List<string> toList)
        {
            var to = string.Empty;
            foreach (var item in toList)
            {
                to += item + ";";
            }
            var url = string.Format(distanceApi, from, to.TrimEnd(';'), WeixinConfig.qqMapKey);
            var str = HttpGet(url);
            var distinct = JsonConvert.DeserializeObject<QQDistinct>(str);
            return distinct;
        }

        private static string HttpGet(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ServicePoint.Expect100Continue = false;
            request.KeepAlive = true;
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
            string retString = "";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream myResponseStream = response.GetResponseStream())
                {
                    using (StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8")))
                    {
                        retString = myStreamReader.ReadToEnd();
                        //myStreamReader.Close();
                        //myResponseStream.Close();
                    }
                }
            }

            return retString;
        }


        public class QQDistinct
        {
            public string status { get; set; }

            public Result result { get; set; }
        }


        public class Result
        {
            public List<Elements> elements { get; set; }
        }

        public class Elements
        {
            public decimal distance { get; set; }

            public To to { get; set; }
        }

        public class To
        {
            public string lat { get; set; }
            public string lng { get; set; }
        }

    }
}
