﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using CM = System.Configuration.ConfigurationManager;

namespace RayelinkWeixin.Common.Utilites
{
    public static class TextHelper
    {
        private const bool IsDNAT = false;

        #region 获取当前URL
        /// <summary>
        /// 获取当前URL
        /// </summary>
        /// <param name="isPort">是否需要端口号</param>
        /// <returns></returns>
        public static string RequestUrl(bool isPort = true)
        {
            string baseUrl = string.Empty;
            if (System.Web.HttpContext.Current != null)
            {
                HttpRequest request = System.Web.HttpContext.Current.Request;
                baseUrl = request.Url.AbsoluteUri;
                if (!isPort)
                {
                    baseUrl = baseUrl.Replace(request.Url.Port + "", "");
                }
            }
            return baseUrl;
        }
        #endregion

        #region 根据当前的请求获取网站的根路径(包含虚拟目录)
        /// <summary>
        /// 根据当前的请求获取网站的根路径(包含虚拟目录)
        /// </summary>
        /// <param name="isDNAT">默认为true,增加此参数主要是在本机调用此方法的时候可以无视端口映射的问题</param>
        /// <returns></returns>
        public static string BaseUrl(bool isDNAT = true)
        {
            string baseUrl = string.Empty;
            if (HttpContext.Current != null)
            {
                HttpRequest request = System.Web.HttpContext.Current.Request;
                if (string.IsNullOrWhiteSpace(request.ApplicationPath) || request.ApplicationPath.Equals("/"))
                {
                    if (request.Url.Port == 80 || (IsDNAT && isDNAT))
                    {
                        baseUrl = string.Format("http://{0}/", request.Url.Host);
                    }
                    else
                    {
                        baseUrl = string.Format("http://{0}:{1}/", request.Url.Host, request.Url.Port);
                    }
                }
                else
                {
                    if (request.Url.Port == 80 || (IsDNAT && isDNAT))
                    {
                        baseUrl = string.Format("http://{0}/{1}/", request.Url.Host, request.ApplicationPath.TrimStart('/'));
                    }
                    else
                    {
                        baseUrl = string.Format("http://{0}:{1}/{2}/", request.Url.Host, request.Url.Port, request.ApplicationPath.TrimStart('/'));
                    }
                }
                if (request.Url.Port == 443 && !string.IsNullOrWhiteSpace(baseUrl))
                {
                    baseUrl = baseUrl.Replace("http://", "https://");
                }

            }
            return baseUrl;
        }
        #endregion

        #region 获取当前请求的域名
        /// <summary>
        /// 获取当前请求的域名
        /// </summary>
        /// <returns></returns>
        public static string BaseUrlDomain()
        {
            string baseUrl = string.Empty;
            if (System.Web.HttpContext.Current != null)
            {
                HttpRequest request = System.Web.HttpContext.Current.Request;
                if (request.Url.Port == 80 || IsDNAT)
                {
                    baseUrl = string.Format("http://{0}/", request.Url.Host);
                }
                else
                {
                    baseUrl = string.Format("http://{0}:{1}/", request.Url.Host, request.Url.Port);
                }
            }
            return baseUrl;
        }
        #endregion

        #region 相对路径转换为完整路径
        /// <summary>
        /// 相对路径转换为完整路径
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string Content(string url)
        {
            return ContentComm(url);
        }

        /// <summary>
        /// 重载Content,当传入两个参数的时候匹配的Url.Action("action","controller")
        /// </summary>
        /// <param name="strAction"></param>
        /// <param name="strCon"></param>
        /// <returns></returns>
        public static string Content(string strCon, string strAction)
        {
            string url = string.Format("{0}/{1}", strAction, strCon);

            return ContentComm(url);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static string ContentComm(string url)
        {
            string result = string.Empty;
            string baseUrl = BaseUrl();
            if (url.StartsWith("http://"))
            {
                if (IsDNAT)
                {
                    result = url.Replace(string.Format(":{0}", new System.Uri(url).Port), "");
                }
                else
                {
                    result = url;
                }
            }
            else
            {
                if (url.StartsWith("~/") || url.StartsWith("/"))
                {
                    url = url.TrimStart("~/".ToCharArray());
                }
                result = string.Format("{0}/{1}", baseUrl.TrimEnd('/'), url);
            }

            return result;
        }
        #endregion

        #region 获取webconfig当中appSetting中的配置项
        /// <summary>
        /// 获取webconfig当中appSetting中的配置项
        /// </summary>
        /// <param name="key">配置项的键</param>
        /// <returns>配置项的值</returns>
        public static string GetConfigItem(string key)
        {
            string result = string.Empty;
            try
            {
                result = CM.AppSettings[key];
            }
            catch { }
            return result;
        }
        #endregion

        #region 获取webconfig当中appSetting中的配置项
        /// <summary>
        /// 获取webconfig当中appSetting中的配置项
        /// </summary>
        /// <param name="key">配置项的键</param>
        /// <returns>配置项的值</returns>
        public static T GetConfigItem<T>(string key)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(CM.AppSettings[key]))
                {
                    return (T)Convert.ChangeType(CM.AppSettings[key], typeof(T));
                }
                else
                {
                    return default(T);
                }
            }
            catch
            {
                return (T)Convert.ChangeType(DBNull.Value, typeof(T));
            }
        }
        #endregion

        #region 获取数据库配置项
        /// <summary>
        /// 获取数据库配置项
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetDataConfigItem(string key)
        {
            string result = string.Empty;
            try
            {
                result = CM.ConnectionStrings[key].ConnectionString;
            }
            catch { }
            return result;
        }
        #endregion

        #region 对指定的字符串,进行md5加密散列值计算
        /// <summary>
        /// 对指定的字符串,进行md5加密散列值计算
        /// </summary>
        /// <param name="source">需要md5加密的字符串值</param>
        /// <returns>md5值</returns>
        public static string MD5(string source)
        {
            byte[] result = Encoding.Default.GetBytes(source);
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            return ByteArrayToHexString(md5.ComputeHash(result));
        }

        /// <summary>
        ///  对指定的字符串,进行md5加密散列值计算，返回值转为全小写
        /// </summary>
        /// <param name="source"></param>
        /// <returns>MD5加密后转为全小写数据值</returns>
        public static string Md5ToLower(string source)
        {
            byte[] result = Encoding.Default.GetBytes(source);
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            return ByteArrayToHexString(md5.ComputeHash(result)).ToLower();
        }
        #endregion

        #region 把对应的字节值转换为对应的字符串
        /// <summary>
        /// 把对应的字节值转换为对应的字符串
        /// </summary>
        /// <param name="values">字节值</param>
        /// <returns>字符串</returns>
        private static string ByteArrayToHexString(byte[] values)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte value in values)
            {
                sb.AppendFormat("{0:X2}", value);
            }
            return sb.ToString();
        }
        #endregion

        #region 处理响应内容中看不到的字符
        /// <summary>
        /// 处理响应内容中看不到的字符
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static string ReplaceHexadecimalSymbols(string txt)
        {
            string r = "[\x00-\x08\x0B\x0C\x0E-\x1F]";
            return Regex.Replace(txt, r, "", RegexOptions.Compiled);
        }
        #endregion
    }
}
