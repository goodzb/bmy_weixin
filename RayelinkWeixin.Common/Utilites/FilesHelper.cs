﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// 文件管理辅助类
    /// </summary>
    public class FilesHelper
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, SetLastError = true)]
        private static extern IntPtr FindFirstFile(string pFileName, ref WIN32_FIND_DATA pFindFileData);
        [System.Runtime.InteropServices.DllImport("kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, SetLastError = true)]
        private static extern bool FindNextFile(IntPtr hndFindFile, ref WIN32_FIND_DATA lpFindFileData);
        [System.Runtime.InteropServices.DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool FindClose(IntPtr hndFindFile);

        //具体方法函数  

        Stack<string> m_scopes = new Stack<string>();
        private static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);
        //WIN32_FIND_DATA FindFileData;
        private System.IntPtr hFind = INVALID_HANDLE_VALUE;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="searchPattern"></param>
        /// <param name="searchOption"></param>
        /// <returns></returns>
        public static IEnumerable<string> EnumerateFiles(string path, string searchPattern = "*.*", SearchOption searchOption = SearchOption.AllDirectories)
        {
            IntPtr hFind = INVALID_HANDLE_VALUE;
            WIN32_FIND_DATA FindFileData = default(WIN32_FIND_DATA);

            hFind = FindFirstFile(Path.Combine(path, searchPattern), ref FindFileData);
            if (hFind != INVALID_HANDLE_VALUE)
            {
                do
                {
                    if (FindFileData.cFileName.Equals(@".") || FindFileData.cFileName.Equals(@".."))
                        continue;
                    if (searchOption == SearchOption.AllDirectories && ((FindFileData.dwFileAttributes & (int)FileAttributes.Directory) == (int)FileAttributes.Directory))
                    {

                        foreach (var file in EnumerateFiles(Path.Combine(path, FindFileData.cFileName)))
                            yield return file;
                    }

                    else
                    {
                        yield return Path.Combine(path, FindFileData.cFileName);
                    }
                }
                while (FindNextFile(hFind, ref FindFileData));
            }
            FindClose(hFind);
        }

        /// <summary>
        /// 读取文件
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        public static void SafeRead(Stream stream, byte[] data)
        {
            int offset = 0;
            int remaining = data.Length;
            // 只要有剩余的字节就不停的读
            while (remaining > 0)
            {
                int read = stream.Read(data, offset, remaining);
                if (read <= 0)
                    throw new EndOfStreamException("文件读取到" + read.ToString() + "失败！");

                // 减少剩余的字节数
                remaining -= read;

                // 增加偏移量
                offset += read;
            }
        }


        /// <summary>
        /// 读取流直到流里面的数据完全读取出来为止
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static byte[] ReadFully(Stream stream)
        {
            // 初始化一个32k的缓存
            byte[] buffer = new byte[32768];

            using (MemoryStream ms = new MemoryStream())
            {
                //返回结果后会自动回收调用该对象的Dispose方法释放内存
                // 不停的读取
                while (true)
                {
                    int read = stream.Read(buffer, 0, buffer.Length);
                    // 直到读取完最后的3M数据就可以返回结果了
                    if (read <= 0)
                        return ms.ToArray();
                    ms.Write(buffer, 0, read);
                }
            }

        }

        /// <summary>
        /// 用指定缓存长度的方式读取流
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="BufferLen"></param>
        /// <returns></returns>
        public static string Read2Buffer(string filename, int BufferLen = 2048)
        {
            // 如果指定的无效长度的缓冲区，则指定一个默认的长度作为缓存大小
            if (BufferLen < 1)
            {
                BufferLen = 0x8000;
            }

            // 初始化一个缓存区
            byte[] buffer = new byte[BufferLen];
            int read = 0;
            int block;
            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                // 每次从流中读取缓存大小的数据，知道读取完所有的流为止
                while ((block = stream.Read(buffer, read, buffer.Length - read)) > 0)
                {
                    // 重新设定读取位置
                    read += block;
                    // 检查是否到达了缓存的边界，检查是否还有可以读取的信息
                    if (read == buffer.Length)
                    {
                        // 尝试读取一个字节
                        int nextByte = stream.ReadByte();
                        // 读取失败则说明读取完成可以返回结果
                        if (nextByte == -1)
                        {
                            break;
                        }
                        // 调整数组大小准备继续读取
                        byte[] newBuf = new byte[buffer.Length * 2];
                        Array.Copy(buffer, newBuf, buffer.Length);
                        newBuf[read] = (byte)nextByte;
                        buffer = newBuf;// buffer是一个引用（指针），这里意在重新设定buffer指针指向一个更大的内存
                        read++;
                    }
                }
            }
            // 如果缓存太大则使用ret来收缩前面while读取的buffer，然后直接返回
            byte[] ret = new byte[read];
            Array.Copy(buffer, ret, read);  
            string str = System.Text.Encoding.UTF8.GetString(ret);
            return str;
        }

        /// <summary>
        /// 将一个包含ASCII编码字符的Byte数组转化为一个完整的String
        /// </summary>
        /// <param name="characters"></param>
        /// <returns></returns>
        public static string FromASCIIByteArray(byte[] characters)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            string constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        /// <summary>
        /// 将一个包含Unicode编码字符的Byte数组转化为一个完整的String
        /// </summary>
        /// <param name="characters"></param>
        /// <returns></returns>
        public static string FromUnicodeByteArray(byte[] characters)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            string constructedString = encoding.GetString(characters);
            return (constructedString);
        }
    }

    #region 声明WIN32API函数以及结构 **************************************  
    [Serializable, System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Auto), System.Runtime.InteropServices.BestFitMapping(false)]
    public struct WIN32_FIND_DATA
    {
        public int dwFileAttributes;
        public int ftCreationTime_dwLowDateTime;
        public int ftCreationTime_dwHighDateTime;
        public int ftLastAccessTime_dwLowDateTime;
        public int ftLastAccessTime_dwHighDateTime;
        public int ftLastWriteTime_dwLowDateTime;
        public int ftLastWriteTime_dwHighDateTime;
        public int nFileSizeHigh;
        public int nFileSizeLow;
        public int dwReserved0;
        public int dwReserved1;
        [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 260)]
        public string cFileName;
        [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 14)]
        public string cAlternateFileName;
    }
    #endregion
}
