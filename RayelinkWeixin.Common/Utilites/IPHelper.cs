﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// IPHelper
    /// </summary>
    public static class IPHelper
    {
        #region  获取本机的ip地址
        /// <summary>
        /// 获取本机的ip地址
        /// </summary>
        /// <returns></returns>
        public static IList<string> LocalIP()
        {
            var temp = Dns.GetHostEntry(Dns.GetHostName()).AddressList.Where(p => p.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
            return temp.Select(p => p.ToString()).ToList<string>();
        }
        #endregion

        #region  获取客户端IP
        /// <summary>
        /// 获取客户端IP
        /// </summary>
        public static string ClientIP(System.Web.HttpContext httpContext = null)
        {
            if (httpContext == null)
            {
                httpContext = System.Web.HttpContext.Current;
            }
            string result = string.Empty;
            if (null != httpContext)
            {
                //Add 反向代理中的真实IP地址 by Wuyj 2015-12-24 START
                if (httpContext.Request.Headers["X-Real-IP"] != null) //反向代理中的真实IP地址
                {
                    return httpContext.Request.Headers["X-Real-IP"].ToString();
                }
                //Add 反向代理中的真实IP地址 by Wuyj 2015-12-24 END

                result = httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (string.IsNullOrWhiteSpace(result))
                {
                    result = httpContext.Request.ServerVariables["REMOTE_ADDR"];
                }

                if (string.IsNullOrWhiteSpace(result))
                {
                    result = httpContext.Request.UserHostAddress;
                }
                if (!new Regex(@"((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))").IsMatch(result))
                {
                    result = "127.0.0.1";
                }
            }
            return result;
        }
        #endregion
    }
}
