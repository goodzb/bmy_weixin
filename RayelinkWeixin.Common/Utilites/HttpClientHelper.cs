﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    ///作者：xiaohan
    ///日期：2017/05/16 14:28:47
    ///说明：HttpClient分装
    ///版本号：  V1.0.0.0
    /// </summary>
    public class HttpClientHelper
    {
        #region HttpClientResult
        //定义HttpClient返回结果的对象
        private class HttpClientResult
        {
            /// <summary>
            /// 请求接口地址
            /// </summary>
            public string URL { get; set; }

            /// <summary>
            /// 请求开始时间
            /// </summary>
            public DateTime StartDate { get; set; }

            /// <summary>
            /// 请求开始时间
            /// </summary>
            public DateTime EndDate { get; set; }

            /// <summary>
            /// 请求参数
            /// </summary>
            public string postData { get; set; }

            /// <summary>
            /// 接口请求状态
            /// </summary>
            public int ResponseStatus { get; set; }

            /// <summary>
            /// 请求耗时
            /// </summary>
            public string TimeElapsed { get; set; }

            /// <summary>
            /// 请求参数
            /// </summary>
            public string RequestContent { get; set; }

            /// <summary>
            /// 响应结果
            /// </summary>
            public string ResponseContent { get; set; }

            /// <summary>
            /// CallStark
            /// </summary>
            public string CallStark { get; set; }

        }
        #endregion

        #region 业务方法封装
        /// <summary>
        /// 业务封装HTTP POST接口调用
        /// </summary>
        /// <param name="url">PostUrl</param>     
        /// <param name="postData">json字符串</param>
        /// <param name="action">回调函数</param>
        /// <param name="header">回调函数</param>
        /// <returns></returns>       
        public static string PostHttp(string url, string postData, Action<string, string, string, string, DateTime, DateTime> action, Dictionary<string, string> header = null)
        {
            string result = string.Empty;
            string callStack = string.Empty;
            HttpClientResult model = new HttpClientResult
            {
                StartDate = DateTime.Now,
                URL = url,
                postData = postData,
                CallStark = callStack,
                ResponseStatus = 500,
            };
            Stopwatch watch = new Stopwatch();    //创建计时对象
            watch.Start();
            try
            {
                var htmlResult = WebClientPost(url, postData, action: action, header: header);
                model.ResponseContent = htmlResult;
                watch.Stop();
                model.TimeElapsed = watch.Elapsed.ToString();
                model.ResponseStatus = 200;
                result = JsonHelper.ObjectToJsonStr(model);
            }
            catch (System.Exception ex)
            {
                model.ResponseStatus = -1000;
                model.ResponseContent = ex.Message;
                result = JsonHelper.ObjectToJsonStr(model);
                ex.Data["Result"] = result;
                LogHelper.Exception(ex);
            }

            return result;
        }

        #endregion

        #region HttpGet请求

        /// <summary>
        /// HttpGet请求
        /// </summary>
        /// <param name="url">请求URL</param>
        /// <param name="timeout">请求超时时间，默认是10秒</param>
        /// <param name="encoding">编码</param>
        /// <param name="action">回调函数(一般是做Log记录使用的,url,request，apiName，response，startDate，endDate)</param>
        /// <param name="header">需要额外加的头部请求信息</param>
        /// <param name="certificateValidation">是否校驗本地证书</param>
        /// <returns>返回请求的数据值</returns>
        public static string WebClientGet(string url, int timeout = 10000, string encoding = "utf-8", Action<string, string, string, string, DateTime, DateTime> action = null, Dictionary<string, string> header = null, bool certificateValidation = false)
        {
            string responseContent = string.Empty;
            DateTime satrtDate = DateTime.Now;
            string request = string.Empty;
            Uri uri = null;
            string apiName = string.Empty;
            try
            {
                uri = new Uri(url);
                apiName = uri.AbsolutePath;
                request = uri.Query;

                #region 解决问题：请求被中止: 未能创建 SSL/TLS 安全通道。
                if (!certificateValidation) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                #endregion

                using (WebClient client = new NewWebClient(timeout))
                {
                    if (null != header)
                    {
                        foreach (var h in header)
                        {
                            client.Headers.Add(h.Key, h.Value);
                        }
                    }
                    client.Encoding = System.Text.Encoding.GetEncoding(encoding);

                    try
                    {
                        responseContent = client.DownloadString(uri);
                         if(null!=  action) action.Invoke(url, request, apiName, responseContent, satrtDate, DateTime.Now);
                    }
                    catch (WebException ex)
                    {
                        if (null != ex && null != ex.Response)
                        {
                            StreamReader reader = new StreamReader(ex.Response.GetResponseStream());
                            responseContent = reader.ReadToEnd();
                             if(null!=  action) action.Invoke(url, request, apiName, responseContent, satrtDate, DateTime.Now);
                        }
                        throw;
                    }
                }
                return responseContent;
            }
            catch (System.Exception ex)
            {
                responseContent = ex.Message;
             if(null!=  action) action.Invoke(url, request, apiName, responseContent, satrtDate, DateTime.Now);
                throw;
            }
        }
        #endregion

        #region HttpPost请求

        /// <summary>
        /// HttpPost请求
        /// </summary>
        /// <param name="url">请求URL</param>
        /// <param name="postData">请求参数</param>
        /// <param name="timeout">请求超时时间，默认是10秒</param>
        /// <param name="encoding">编码</param>
        /// <param name="action">回调函数(一般是做Log记录使用的,url,request，apiName，response，startDate，endDate)</param>
        /// <param name="header">需要额外加的头部请求信息</param>
        /// <param name="certificateValidation">是否校驗本地证书</param>
        /// <returns>返回请求的数据值</returns>
        public static string WebClientPost(string url, string postData, int timeout = 10000, string encoding = "utf-8", Action<string, string, string, string, DateTime, DateTime> action = null, Dictionary<string, string> header = null, bool certificateValidation = false)
        {
            string responseContent = string.Empty;
            DateTime satrtDate = DateTime.Now;
            Uri uri = null;
            string apiName = string.Empty;
            byte[] responseData = null;
            try
            {
                uri = new Uri(url);
                apiName = uri.AbsolutePath;

                #region 解决问题：请求被中止: 未能创建 SSL/TLS 安全通道。
                if (!certificateValidation) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                #endregion

                using (WebClient client = new NewWebClient(timeout))
                {
                    client.Encoding = System.Text.Encoding.GetEncoding(encoding);

                    if (null != header && header.Any())
                    {
                        foreach (var h in header)
                        {
                            client.Headers.Add(h.Key, h.Value);
                        }
                    }
                    byte[] bytes = Encoding.UTF8.GetBytes(postData);
                    try
                    {
                        responseData = client.UploadData(url, "POST", bytes);
                        responseContent = client.Encoding.GetString(responseData);
                        //解决接口返回的时候中文乱码问题
                        //Encoding.Default.GetString(responseData);                  
                         if(null!=  action) action.Invoke(url, postData, apiName, responseContent, satrtDate, DateTime.Now);
                    }
                    catch (WebException ex)
                    {
                        if (null != ex && null != ex.Response)
                        {
                            StreamReader reader = new StreamReader(ex.Response.GetResponseStream());
                            responseContent = reader.ReadToEnd();
                             if(null!=  action) action.Invoke(url, postData, apiName, responseContent, satrtDate, DateTime.Now);
                        }
                        throw;
                    }
                }
                return responseContent;
            }
            catch (System.Exception ex)
            {
                responseContent = ex.Message;
                 if(null!=  action) action.Invoke(url, postData, apiName, responseContent, satrtDate, DateTime.Now);
                throw;
            }
        }
        #endregion

        #region DownloadSerializedJSONDataAsync
        /// <summary>
        /// 还没有测试此方法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<T> DownloadSerializedJSONDataAsync<T>(string url) where T : new()
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //API called is YouTube, add whatever HTTP headers needed

                var jsonData = string.Empty;
                try
                {
                    jsonData = await httpClient.GetStringAsync(url);
                }
                catch (Exception)
                {
                    return default(T);
                }
                return !string.IsNullOrEmpty(jsonData) ? JsonConvert.DeserializeObject<T>(jsonData) : default(T);
            }
        }
        #endregion

        #region DownloadByteDataAsync
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="timeout"></param>
        /// <param name="encoding"></param>
        /// <param name="action"></param>
        /// <param name="header"></param>
        /// <param name="certificateValidation"></param>
        /// <returns></returns>
        public async Task<byte[]> DownloadByteDataAsync(string url, int timeout = 30000, string encoding = "utf-8", Action<string, string, string, string, DateTime, DateTime> action = null, Dictionary<string, string> header = null, bool certificateValidation = false)
        {
            string responseContent = string.Empty;
            DateTime satrtDate = DateTime.Now;
            string request = string.Empty;
            Uri uri = null;
            string apiName = string.Empty;
            using (var httpClient = new HttpClient())
            {
                RequestHeaders(httpClient, timeout, header);

                #region 解决问题：请求被中止: 未能创建 SSL/TLS 安全通道。
                if (!certificateValidation) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                #endregion

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var jsonData = string.Empty;
                try
                {
                    uri = new Uri(url);
                    apiName = uri.AbsolutePath;
                    request = uri.Query;
                    RequestHeaders(httpClient, timeout, header);
                    var response = await httpClient.GetByteArrayAsync(url);
                   if(null!= action) action.Invoke(url, request, apiName, response.ToString(), satrtDate, DateTime.Now);
                    return response;
                }
                catch (Exception ex)
                {
                    responseContent = ex.Message;
                      if(null!= action) action.Invoke(url, request, apiName, responseContent, satrtDate, DateTime.Now);
                    throw;
                }
            }
        }
        #endregion

        #region 设置RequestHeaders信息
        /// <summary>
        /// 设置RequestHeaders信息
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="timeout"></param>
        /// <param name="header"></param>
        private void RequestHeaders(HttpClient httpClient, int timeout = 30000, Dictionary<string, string> header = null)
        {
            // 设置请求头信息              
            httpClient.DefaultRequestHeaders.Add("KeepAlive", "false");   // HTTP KeepAlive设为false，防止HTTP连接保持  
            httpClient.DefaultRequestHeaders.Add("UserAgent", "Mozilla/5.2 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0");
            httpClient.Timeout = new TimeSpan(timeout * 10 * 1000);

            if (header != null)
            {
                header.All((item) =>
                {
                    if (httpClient.DefaultRequestHeaders.Contains(item.Key))
                    {
                        httpClient.DefaultRequestHeaders.Remove(item.Key);
                    }
                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation(item.Key, item.Value);
                    return true;
                });
            }
        }
        #endregion

        #region 获取或设置用于验证服务器证书的回调
        /// <summary>
        /// 获取或设置用于验证服务器证书的回调
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }
        #endregion
    }


    /// <summary>
    /// 为WebClient增加超时时间
    /// <para>从WebClient派生一个新的类，重载GetWebRequest方法</para>
    /// </summary>
    public class NewWebClient : WebClient
    {
        private int _timeout;

        /// <summary>
        /// 超时时间(毫秒)
        /// </summary>
        public int Timeout
        {
            get
            {
                return _timeout;
            }
            set
            {
                _timeout = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeout"></param>
        public NewWebClient(int timeout = 30000)
        {
            this._timeout = timeout;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        protected override WebRequest GetWebRequest(Uri address)
        {
            var result = base.GetWebRequest(address);
            result.Timeout = this._timeout;
            return result;
        }
    }
}
