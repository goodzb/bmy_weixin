﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：Base64Helper
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-26 9:56:27
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// 
    /// </summary>
    public class Base64Helper
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string EncodeBase64(string str)
        {
            System.Text.Encoding pEncoding = System.Text.Encoding.GetEncoding("utf-8");
            string results = "";
            try
            {
                results = System.Convert.ToBase64String(pEncoding.GetBytes(str)).Replace("+", "|").Replace("/", "-A-").Replace("%", "-B-");
            }
            catch
            {
                results = str;
            }
            return results;
            //return System.Convert.ToBase64String(pEncoding.GetBytes(str)).Replace("+", "|").Replace("/", "-A-").Replace("%", "-B-");
            //return System.Convert.ToBase64String(pEncoding.GetBytes(str)).Replace("+", "|").Replace("/", "-A-").Replace("%", "-B-").Replace("/", "_").Replace("+", "-");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string DecodeBase64(string str)
        {
            System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("utf-8");
            string results = "";
            try
            {
                results = encoding.GetString(System.Convert.FromBase64String(str.Replace("|", "+").Replace("-A-", "/").Replace("-B-", "%")));
            }
            catch {
                results = str;
            }
            return results;
            //return encoding.GetString(System.Convert.FromBase64String(str.Replace("|", "+").Replace("-A-", "/").Replace("-B-", "%")));
            //return encoding.GetString(System.Convert.FromBase64String(str.Replace("|", "+").Replace("-A-", "/").Replace("-B-", "%").Replace("_", "/").Replace("-", "+")));
        }

    }
}
