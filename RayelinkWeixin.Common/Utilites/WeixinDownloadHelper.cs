﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：WeixinUploadHelper
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-02-03 14:59:11
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


using RayelinkWeixin.Const.Config;
using System;
using System.IO;
using System.Net;
using System.Web;
namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// 
    /// </summary>
    public class WeixinDownloadHelper
    {
        public static string GetSavePDFPath()
        {
            var path = Path.Combine(WeixinConfig.PdfPath, "UploadFile");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        public static bool IsUseCasePDFExists(int caseID, int orgID, int type,int reportId, string name = "")
        {
            var fileName = "";
            if (type == 4)
            {
                fileName = caseID.ToString() + orgID.ToString() + type.ToString() + name + ".png";
            }else
            {
                fileName = caseID.ToString() + orgID.ToString() + type.ToString() + reportId.ToString() + name + ".pdf";
            }
            var path = Path.Combine(GetSavePDFPath(), fileName);
            return File.Exists(path);
        }

        /// <summary>
        /// 微信异步加载
        /// </summary>
        /// <param name="token"></param>
        /// <param name="mediaID"></param>
        public static void WeixinAsyncDownload(string accessToken, string mediaID)
        {
            WeixinDownload(accessToken, mediaID);
        }

        /// <summary>
        /// 这个是用来发送消息的静态方法
        /// </summary>
        /// <param name="message"></param>
        private static void WeixinDownload(string token, string mediaID)
        {
            var path = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "UploadFile");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = Path.Combine(path, DateTime.Now.ToString("yyyyMMdd"));
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string url = string.Format("http://file.api.weixin.qq.com/cgi-bin/media/get?access_token={0}&media_id={1}", token, mediaID);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ServicePoint.Expect100Continue = false;
            request.KeepAlive = true;
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
            var fileName = mediaID;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    if (stream != null)
                    {
                        string filePath = Path.Combine(path, fileName + ".png");
                        using (var fileStream = File.Create(filePath))
                        {
                            byte[] buffer = new byte[1024];
                            int bytesRead = 0;
                            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) != 0)
                            {
                                fileStream.Write(buffer, 0, bytesRead);
                            }
                            fileStream.Flush();
                        }
                        stream.Close();
                    }
                }
            }
        }
    }
}
