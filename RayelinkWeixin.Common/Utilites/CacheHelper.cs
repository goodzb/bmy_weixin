﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：CacheHelper
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-22 9:30:53
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using System;
using System.Collections;
using System.Web;
using System.Web.Caching;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// 
    /// </summary>
    public class CacheHelper
    {
        /// <summary>
        ///     获取数据缓存
        /// </summary>
        /// <param name="cacheKey">键</param>
        public static object GetCache(string cacheKey)
        {
            Cache objCache = HttpRuntime.Cache;
            return objCache.Get(cacheKey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public static T GetCache<T>(string cacheKey)
        {
            T result = default(T);
            Cache objCache = HttpRuntime.Cache;
            var cache= objCache.Get(cacheKey);

            if (null!= cache)
            {
                result = (T)Convert.ChangeType(cache, typeof(T));
            }
            return result;
        }

        /// <summary>
        ///  设置数据缓存
        /// </summary>
        /// <param name="cacheKey">Key值</param>
        /// <param name="objObject">对象</param>
        /// <param name="fileName">文件名</param>
        /// <param name="timeOutHours">过期时间(小时)</param>
        public static void SetCache(string cacheKey, object objObject, string fileName = null, int timeOutHours = 1)
        {
            Cache objCache = HttpRuntime.Cache;
            CacheDependency cacheDependency = fileName == string.Empty ? null : new CacheDependency(fileName);
            //objCache.Insert(cacheKey, objObject);
            objCache.Insert(cacheKey, objObject, cacheDependency, Cache.NoAbsoluteExpiration, TimeSpan.FromHours(timeOutHours));

        }

        /// <summary>
        ///  设置数据缓存
        /// </summary>
        /// <param name="cacheKey">Key值</param>
        /// <param name="objObject">对象</param>     
        /// <param name="timeOutHours">过期时间(分钟)</param>
        public static void SetCache(string cacheKey, object objObject, int timeOutMinute)
        {
            Cache objCache = HttpRuntime.Cache;
            objCache.Insert(cacheKey, objObject, null, Cache.NoAbsoluteExpiration,
                TimeSpan.FromMinutes(timeOutMinute));
        }

        /// <summary>
        /// 设置数据缓存
        /// </summary>
        /// <param name="cacheKey">Key值></param>
        /// <param name="objObject">对象</param>
        /// <param name="Timeout">过期时间</param>
        public static void SetCache(string cacheKey, object objObject, TimeSpan Timeout)
        {
            Cache objCache = HttpRuntime.Cache;
            objCache.Insert(cacheKey, objObject, null, DateTime.MaxValue, Timeout, CacheItemPriority.NotRemovable, null);
        }

        /// <summary>
        ///     设置数据缓存
        /// </summary>
        /// <param name="cacheKey">Key值</param>
        /// <param name="objObject">对象</param>
        /// <param name="absoluteExpiration">绝对过期</param>
        /// <param name="slidingExpiration">滑动过期</param>
        public static void SetCache(string cacheKey, object objObject, DateTime absoluteExpiration,
            TimeSpan slidingExpiration)
        {
            Cache objCache = HttpRuntime.Cache;
            objCache.Insert(cacheKey, objObject, null, absoluteExpiration, slidingExpiration);
        }

        /// <summary>
        ///   移除指定数据缓存
        /// </summary>
        /// <param name="cacheKey">Key值</param>
        public static void RemoveCache(string cacheKey)
        {
            Cache cache = HttpRuntime.Cache;
            cache.Remove(cacheKey);
        }

        /// <summary>
        ///     移除全部缓存
        /// </summary>
        public static void RemoveAllCache()
        {
            Cache cache = HttpRuntime.Cache;
            IDictionaryEnumerator cacheEnum = cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                cache.Remove(cacheEnum.Key.ToString());
            }
        }
    }
}
