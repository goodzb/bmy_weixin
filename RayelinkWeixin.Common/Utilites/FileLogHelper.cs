﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Reflection;
using log4net.Appender;
using log4net.Layout;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// 文件日志类(属性可配置)
    /// </summary>
    public class FileLogHelper
    {
        /** 配置文件中可配置项
        <configuration>
          <appSettings>
            <!--是否启用日志记录功能，默认为true-->
            <add key="LogIsEnabled" value="true"/>
            <!--是否同时记录堆栈信息(文件,方法,行号,列号)，默认为true-->
            <add key="IsRecordStack" value="true"/>
            <!--单个日志文件的最大限制(单位：MB)，默认为10(范围[1-10])-->
            <add key="LogFileMaxLimit" value="10"/>
          </appSettings>
        </configuration>
         */

        private static FileLogHelper _instance;
        private static readonly object lockObject = new object();
        //private static Dictionary<string,IList<string>> _data;

        private bool IsEnabledLog = true; // 是否启用日志功能
        private bool IsRecordStackInfo = true; // 是否记录堆栈信息
        private long fileMaxLenth; // 日志文件的最大长度(字节)
        //private FileStream fs;
        //private string filePath;

        /// <summary>
        /// 单例模式
        /// </summary>
        public static FileLogHelper Instance
        {
            get
            {
                lock (lockObject)
                {
                    if (_instance == null)
                    {
                        _instance = new FileLogHelper();
                    }
                    return _instance;
                }
            }
        }


        /// <summary>
        /// 加载配置文件信息
        /// </summary>
        private FileLogHelper()
        {
            NameValueCollection appSettings = System.Configuration.ConfigurationManager.AppSettings;
            string isEnabled = "true";
            string maxLimit = "true";
            try
            {
                isEnabled = appSettings["LogIsEnabled"];
                maxLimit = appSettings["LogFileMaxLimit"];
            }
            catch (Exception)
            {
                //throw;
            }

            bool enabled;
            if (bool.TryParse(isEnabled, out enabled))
            {
                IsEnabledLog = enabled;
            }
            else
            {
                IsEnabledLog = true; // 默认启用日志功能
            }

            IsRecordStackInfo = false; // 默认记录堆栈信息

            int fileMaxLimit, max;
            if (int.TryParse(maxLimit, out max))
            {
                fileMaxLimit = Math.Max(1, max); // 限制最小5M
                fileMaxLimit = Math.Min(10, max); // 限制最大20M
            }
            else
            {
                fileMaxLimit = 10; // 默认20M
            }
            fileMaxLenth = fileMaxLimit * 1024 * 1024;
            Log4NetHelper.MaximumFileSizePerDay = fileMaxLimit.ToString() + "MB";
        }

        /// <summary>
        /// 记录文件日志
        /// </summary>
        /// <param name="content">内容</param>
        /// <param name="logfilePrdfix">log文件名称的前缀</param>
        /// <param name="isLogTime">是否记录日志的中间时间</param>
        /// <param name="listcount">批量写入的检查点</param>
        public void Log(string content, string logfilePrdfix = "", bool isLogTime = true, int listcount = 10)
        {
            if (!IsEnabledLog || string.IsNullOrWhiteSpace(content))
            {
                return;
            }
            try
            {
                StringBuilder sb = new StringBuilder();

                if (isLogTime)
                {
                    sb.Append("-----------------------------------------------------------------------------------\r\n");
                    sb.AppendFormat("【时间】{0}\r\n", DateTime.Now.ToString("HH:mm:ss"));
                    if (IsRecordStackInfo)
                    {
                        sb.Append("【堆栈信息】\r\n");
                        #region 获取堆栈信息
                        StackTrace trace = new StackTrace(true);
                        StackFrame[] frames = trace.GetFrames();
                        int maxLenth = frames.Select(c => c.GetMethod().Name.Length).Max();
                        int tabNum = (maxLenth + 7) / 8;
                        sb.Append("\t方法");
                        //并行计算
                        for (int i = 0; i < tabNum; i++)
                        {
                            sb.Append("\t");
                        }
                        sb.Append("行号\t列号\t文件\r\n");
                        foreach (StackFrame frame in frames)
                        {
                            sb.AppendFormat("\t{0}", frame.GetMethod().Name);
                            int num = (int)Math.Ceiling((decimal)(tabNum * 8 - frame.GetMethod().Name.Length) / 8);
                            //并行计算
                            for (int i = 0; i < num; i++)
                            {
                                sb.Append("\t");
                            }
                            sb.AppendFormat("{0}\t{1}\t{2}\r\n", frame.GetFileLineNumber(), frame.GetFileColumnNumber(), frame.GetFileName());
                        }
                        #endregion
                    }
                    sb.Append("【内容】\r\n\t" + content + "\r\n\r\n");
                }
                else
                {
                    sb.Append(content);
                }

                Log4NetHelper.GetLog(logfilePrdfix).Info(sb.ToString());
            }
            catch (Exception e)
            {
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { e.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(e);
            }
        }

        /// <summary>
        /// 运行日志
        /// </summary>
        /// <param name="contents"></param>
        /// <param name="logfilePrdfix"></param>
        public void RunTimeLog(IList<string> contents, string logfilePrdfix = "")
        {
            if (contents == null || contents.Count == 0)
            {
                return;
            }
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("-----------------------------------------------------------------------------------\r\n");
                sb.AppendFormat("【时间】{0}\r\n", DateTime.Now.ToString("HH:mm:ss"));
                sb.Append("【内容】\r\n");
                for (int i = 0; i < contents.Count; i++)
                {
                    sb.Append(contents[i] + "\r\n");
                }
                Log4NetHelper.GetLog(logfilePrdfix).Info(sb.ToString());
            }
            catch (Exception e)
            {
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { e.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(e);
            }
        }

        /// <summary>
        /// 记录文件日志
        /// </summary>
        /// <param name="contents">日志内容</param>
        /// <param name="logfilePrdfix">日志文件前缀</param>
        public void Log(IList<string> contents, string logfilePrdfix = "")
        {
            if (!IsEnabledLog || contents == null || contents.Count == 0)
            {
                return;
            }
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("-----------------------------------------------------------------------------------\r\n");
                sb.AppendFormat("【时间】{0}\r\n", DateTime.Now.ToString("HH:mm:ss"));
                if (IsRecordStackInfo)
                {
                    sb.Append("【堆栈信息】\r\n");
                    #region 获取堆栈信息
                    StackTrace trace = new StackTrace(true);
                    StackFrame[] frames = trace.GetFrames();
                    int maxLenth = frames.Select(c => c.GetMethod().Name.Length).Max();
                    int tabNum = (maxLenth + 7) / 8;
                    sb.Append("\t方法");
                    //并行计算
                    for (int i = 0; i < tabNum; i++)
                    {
                        sb.Append("\t");
                    }
                    sb.Append("行号\t列号\t文件\r\n");
                    foreach (StackFrame frame in frames)
                    {
                        sb.AppendFormat("\t{0}", frame.GetMethod().Name);
                        int num = (int)Math.Ceiling((decimal)(tabNum * 8 - frame.GetMethod().Name.Length) / 8);
                        //并行计算
                        for (int i = 0; i < num; i++)
                        {
                            sb.Append("\t");
                        }
                        sb.AppendFormat("{0}\t{1}\t{2}\r\n", frame.GetFileLineNumber(), frame.GetFileColumnNumber(), frame.GetFileName());
                    }
                    #endregion
                }
                sb.Append("【内容】\r\n");


                for (int i = 0; i < contents.Count; i++)
                {
                    sb.Append(contents[i] + "\r\n");
                }
                Log4NetHelper.GetLog(logfilePrdfix).Info(sb.ToString());
            }
            catch (Exception e)
            {
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { e.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                new ExceptionHelper().LogException(e);
            }
        }
    }


    /// <summary>
    /// Log4Net帮助类
    /// 创建本地日志文件
    /// </summary>
    class Log4NetHelper
    {
        /// <summary>
        /// LOG4Net锁
        /// </summary>
        public static object LockObj = new object();

        /// <summary>
        /// 每天日志的最大数
        /// </summary>
        public static string MaximumFileSizePerDay = "20MB";

        /// <summary>
        /// 日期模版
        /// </summary>
        public const string DataPatternTemplate = "\"{0}Log_\"yyyyMMdd\".log\"";

        /// <summary>
        /// 线程安全的日志对象列表
        /// </summary>
        private static System.Collections.Concurrent.ConcurrentDictionary<string, log4net.ILog> s_Loggers = new System.Collections.Concurrent.ConcurrentDictionary<string, log4net.ILog>();

        /// <summary>
        /// 创建文件日志的写入器
        /// </summary>
        /// <param name="filePath">日志路径</param>
        /// <param name="filePattern">日志模版</param>
        /// <returns>返回结果</returns>
        private static IAppender CreateFileAppender(string filePath, string filePattern)
        {
            var appender = new RollingFileAppender()
            {
                AppendToFile = true,
                Encoding = Encoding.UTF8,
                ImmediateFlush = false,
                LockingModel = new log4net.Appender.FileAppender.MinimalLock(),
                File = filePath,
                MaxSizeRollBackups = -1,
                MaximumFileSize = MaximumFileSizePerDay,
                RollingStyle = RollingFileAppender.RollingMode.Composite,
                DatePattern = filePattern,
                StaticLogFileName = false,
                Layout = new PatternLayout("%message%newline")
                {
                    Footer = Environment.NewLine
                }
            };
            appender.ActivateOptions();
            return appender;
        }

        /// <summary>
        /// 获取日志记录对象
        /// </summary>
        /// <param name="name">日志名称</param>
        /// <returns>返回结果</returns>
        public static log4net.ILog GetLog(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) name = "DEFAULT";
            return s_Loggers.GetOrAdd(name, (x) =>
            {
                var appender = CreateFileAppender(string.Format("{0}\\logs\\", System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd("\\".ToCharArray())),
                    string.Format(DataPatternTemplate, name != "DEFAULT" ? name : ""));
                log4net.Repository.ILoggerRepository repository = null;
                lock (LockObj)
                {
                    repository = log4net.LogManager.GetAllRepositories().FirstOrDefault(r => r.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
                    if (repository == null)
                    {
                        repository = log4net.LogManager.CreateRepository(name);
                        log4net.Config.BasicConfigurator.Configure(repository, appender);
                    }
                }
                return log4net.LogManager.GetLogger(repository.Name, name);
            });
        }
    }

}
