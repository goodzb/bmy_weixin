﻿using System;
using System.Diagnostics;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// Dos命令执行类
    /// </summary>
    public class CommandHelper
    {
        /// <summary>
        /// 执行DOS命令，返回DOS命令的输出，如果发生异常，返回空字符串
        /// </summary>
        /// <param name="dosCommand"></param>
        /// <returns></returns>
        public static string Execute(string dosCommand)
        {
            return Execute(dosCommand, 0);
        }

        /// <summary>
        /// 执行DOS命令，返回DOS命令的输出，如果发生异常，返回空字符串
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dosCommand"></param>
        /// <returns></returns>
        public static string Execute(string fileName, string dosCommand)
        {
            return Execute(fileName, dosCommand, 0);
        }

        /// <summary>
        /// 执行cmd
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="param"></param>
        /// <param name="milliseconds"></param>
        /// <returns></returns>
        public static string Execute(string fileName, string param, int milliseconds)
        {
            string output = "";     //输出字符串
            if (!string.IsNullOrWhiteSpace(fileName) && !string.IsNullOrWhiteSpace(param))
            {
                Process process = new Process();     //创建进程对象
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = fileName;      //设定需要执行的命令
                startInfo.Arguments = "/C " + param;   //设定参数，其中的“/C”表示执行完命令后马上退出
                startInfo.UseShellExecute = false;     //不使用系统外壳程序启动
                startInfo.RedirectStandardInput = false;   //不重定向输入
                startInfo.RedirectStandardOutput = true;   //重定向输出
                startInfo.CreateNoWindow = true;     //不创建窗口
                process.StartInfo = startInfo;
                try
                {
                    if (process.Start())       //开始进程
                    {
                        if (milliseconds == 0)
                        {
                            process.WaitForExit();     //这里无限等待进程结束
                        }
                        else
                        {
                            process.WaitForExit(milliseconds);  //这里等待进程结束，等待时间为指定的毫秒
                            //读取进程的输出
                            process.Close();
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    if (process != null)
                    {
                        process.Close();
                    }
                }
            }
            return output;
        }


        /// <summary>
        /// 执行cmd
        /// </summary>
        /// <param name="dosCommand"></param>
        /// <param name="milliseconds">等待关联进程退出的时间（以毫秒为单位）。最大值为 32 位整数的最大可能值，这对于操作系统而言表示无限大。</param>
        /// <returns></returns>
        public static string Execute(string dosCommand, int milliseconds)
        {
            string output = "";     //输出字符串
            if (dosCommand != null && dosCommand != "")
            {
                Process process = new Process();     //创建进程对象
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "cmd.exe";      //设定需要执行的命令
                startInfo.UseShellExecute = false;     //不使用系统外壳程序启动
                startInfo.RedirectStandardInput = true;   //不重定向输入
                startInfo.RedirectStandardOutput = true;   //重定向输出
                startInfo.RedirectStandardError = true;
                startInfo.CreateNoWindow = true;     //不创建窗口
                process.StartInfo = startInfo;
                try
                {
                    if (process.Start())       //开始进程
                    {
                        LogHelper.Debug(dosCommand + "开始执行....");
                        process.StandardInput.WriteLine(dosCommand);

                        if (milliseconds == 0)
                        {
                            process.WaitForExit();     //这里无限等待进程结束
                        }
                        else
                        {
                            process.WaitForExit(milliseconds);
                            if (process != null && !process.HasExited)
                            {
                                LogHelper.Debug(dosCommand + "已退出....");
                                process.Close();
                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    if (process != null && !process.HasExited)
                    {
                        process.Kill();
                        process.Close();
                    }
                    LogHelper.Exception(e);
                }
                finally
                {

                }
            }
            return output;
        }

        /// <summary>
        /// 按照应用程序执行
        /// </summary>
        /// <param name="exeName"></param>
        /// <param name="dosCommand"></param>
        /// <param name="milliseconds"></param>
        /// <returns></returns>
        public static string ExecuteFile(string exeName, string dosCommand, int milliseconds)
        {
            string output = "";     //输出字符串
            if (dosCommand != null && dosCommand != "")
            {
                Process process = new Process();     //创建进程对象
                ProcessStartInfo startInfo = new ProcessStartInfo(exeName, dosCommand);
                startInfo.UseShellExecute = false;     //不使用系统外壳程序启动
                startInfo.RedirectStandardInput = false;   //不重定向输入
                startInfo.RedirectStandardOutput = true;   //重定向输出
                startInfo.CreateNoWindow = false;     //不创建窗口
                process.StartInfo = startInfo;

                try
                {
                    if (process.Start())       //开始进程
                    {
                        LogHelper.Debug(dosCommand + "开始执行....");
                        if (milliseconds == 0)
                        {
                            process.WaitForExit();     //这里无限等待进程结束
                        }
                        else
                        {
                            process.WaitForExit(milliseconds);
                            LogHelper.Debug(dosCommand + "已退出....");
                            try
                            {
                                if (process != null && !process.HasExited)
                                {
                                    process.Kill();
                                    process.Close();
                                }
                            }
                            catch
                            {

                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    LogHelper.Exception(e);
                }
                finally
                {
                }
            }
            return output;
        }

    }
}
