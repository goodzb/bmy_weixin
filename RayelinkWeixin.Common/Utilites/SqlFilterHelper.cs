﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RayelinkWeixin.Common.Utilites
{
    /// <summary>
    /// sql防注入
    /// </summary>
    public class SqlFilterHelper
    {
        /// <summary>
        /// 获取安全sql语句
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="isSingleQuotation"></param>
        /// <returns></returns>
        public static string SafeSqlString(string sql, bool isSingleQuotation = true)
        {
            string result = string.Empty;
            //处理非法字符
            result = StripSQLInjection(sql);
            //过滤关键字
            result = FilterSqlKeyWords(sql);
            //转义字符
            result = UnEscapeSQL(result);
            //检查单引号
            if (isSingleQuotation)
            {
                result = CheckSingleQuotationSQL(result);
            }
            return result;
        }

        /// <summary>
        /// 过滤关键字
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        private static string FilterSqlKeyWords(string sql)
        {
            string result = string.Empty;

            result = Regex.Replace(sql, "((SET)|(INSERT)|(UPDATE)|(DELETE)|(REPLACE)|(CREATE)|(DROP)|(TRUNCATE)|(LOAD)|(DATA)|(COPY)|(ALTER)|(GRANT)|(REVOKE)|(LOCK)|(UNLOCK))", string.Empty, RegexOptions.IgnoreCase);

            return result;
        }

        /// <summary>  
        /// 删除SQL注入特殊字符  
        /// </summary>  
        private static string StripSQLInjection(string sql)
        {
            if (!string.IsNullOrEmpty(sql))
            {
                //过滤 ' --  
                string pattern1 = @"(\%27)|(\')|(\-\-)";

                //防止执行 ' or  
                string pattern2 = @"((\%27)|(\'))\s*((\%6F)|o|(\%4F))((\%72)|r|(\%52))";

                //防止执行sql server 内部存储过程或扩展存储过程  
                string pattern3 = @"\s+exec(\s|\+)+(s|x)p\w+";

                sql = Regex.Replace(sql, pattern1, string.Empty, RegexOptions.IgnoreCase);
                sql = Regex.Replace(sql, pattern2, string.Empty, RegexOptions.IgnoreCase);
                sql = Regex.Replace(sql, pattern3, string.Empty, RegexOptions.IgnoreCase);

                sql = sql.Replace("?", "");

                sql = sql.Replace("%", "");
            }
            return sql;
        }

        /// <summary>  
        /// 改正sql语句中的转义字符  
        /// </summary>  
        private static string UnEscapeSQL(string str)
        {
            return (str == null) ? "" : str.Replace("\'", "'");
        }

        /// <summary>  
        /// 替换sql语句中的有问题符号 
        /// </summary>  
        private static string CheckSingleQuotationSQL(string str)
        {
            return (str == null) ? "" : str.Replace("'", "''");
        }


        /// <summary>
        /// 获取Url、Form参数值，自动处理sql防注入的问题
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <param name="isSafeSql">是否处理自动处理sql防注入的问题(true/false)</param>
        /// <returns></returns>
        public static string RequestString(string name, string defaultValue = "", bool isSafeSql = true)
        {
            string result = defaultValue;

            if (System.Web.HttpContext.Current != null)
            {
                if (System.Web.HttpContext.Current.Request != null)
                {
                    result = System.Web.HttpContext.Current.Request[name];

                    if (string.IsNullOrWhiteSpace(result))
                    {
                        if (System.Web.HttpContext.Current.Request.RequestContext.RouteData.Values.ContainsKey(name))
                        {
                            result = Convert.ToString(System.Web.HttpContext.Current.Request.RequestContext.RouteData.Values[name]);
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(result) && isSafeSql)
                    {
                        result = SafeSqlString(result);
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(result))
            {
                result = defaultValue;
            }

            result = result.Trim();

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T RequestString<T>(string name, string defaultValue = "")
        {
            string requestString = RequestString(name, defaultValue);
            T result = default(T);
            try
            {
                if (typeof(T).Name == "Guid")
                {
                    result = (T)(new GuidConverter().ConvertFromString(requestString));
                }
                else
                {
                    result = (T)Convert.ChangeType(requestString, typeof(T));
                }
            }
            catch (Exception)
            {
                result = default(T);
            }
            return result;
        }
    }
}
