﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using Newtonsoft.Json;
using System.Data.Common;
using System.Reflection.Emit;

namespace RayelinkWeixin.Common.Utilites
{
    /*
     *作者：肖寒
     *日期：2017-05-31
     *说明：数据转换辅助类
     */
    /// <summary>
    /// 数据转换辅助类
    /// </summary>
    public class ObjectConvertHelper
    {
        #region List转DataTable
        /// <summary>
        /// List转DataTable
        /// </summary>
        /// <typeparam name="T">原实体对象Class</typeparam>
        /// <param name="list">转换的数据源</param>
        /// <returns></returns>
        public static DataTable ListConvertToDataTable<T>(IEnumerable<T> list)
        {
            DataTable table = CreateTable<T>();
            try
            {
                Type entityType = typeof(T);
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);
                if (list != null)
                {
                    foreach (T item in list)
                    {
                        DataRow row = table.NewRow();
                        foreach (PropertyDescriptor prop in properties)
                        {
                            row[prop.Name] = prop.GetValue(item);
                        }
                        table.Rows.Add(row);
                    }
                }
            }
            catch
            {
                throw;
            }
            return table;
        }
        #endregion

        #region 根据一个实体创建一个相应的DataTable
        /// <summary>
        /// 根据一个实体创建一个相应的DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static DataTable CreateTable<T>()
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            return table;
        }
        #endregion

        #region  DataTable转List<Model>通用类
        /// <summary>
        /// DataTable转ListModel通用类
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static IList<T> ConvertToListModel<T>(DataTable dt) where T : new()
        {
            //定义集合
            IList<T> ts = new List<T>();
            T t = new T();
            string tempName = String.Empty;
            //获取此模型的公共属性
            PropertyInfo[] propertys = t.GetType().GetProperties();
            foreach (DataRow row in dt.Rows)
            {
                t = new T();
                foreach (PropertyInfo pi in propertys)
                {
                    tempName = pi.Name;
                    //检查DataTable是否包含此列
                    if (dt.Columns.Contains(tempName))
                    {
                        //判断此属性是否有set
                        if (!pi.CanWrite)
                            continue;
                        object value = row[tempName];
                        if (value != DBNull.Value)
                        {
                            pi.SetValue(t, Convert.ChangeType(value, pi.PropertyType), null);
                        }
                    }
                }
                ts.Add(t);
            }
            return ts;
        }
        #endregion

        #region List转DataTable
        /// <summary>
        /// List转DataTable
        /// </summary>
        /// <typeparam name="T">原实体对象Class</typeparam>
        /// <param name="list">转换的数据源</param>
        /// <param name="type">过滤方式(0:选择列, 1:删除列)</param>
        /// <param name="propertyName">要过滤的列名</param>
        /// <returns></returns>
        public static DataTable ListConvertToDataTable<T>(IList<T> list, int type, params string[] propertyName)
        {
            DataTable table = CreateTable<T>(type, propertyName);
            Type entityType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            List<string> propertyNameList = new List<string>();
            if (propertyName != null)
            {
                propertyNameList.AddRange(propertyName);
            }

            if (list != null)
            {
                foreach (T item in list)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                    {
                        if (propertyNameList.Contains(prop.Name) && type == 1)
                            continue;
                        else if (!propertyNameList.Contains(prop.Name) && type == 0)
                            continue;

                        row[prop.Name] = prop.GetValue(item);
                    }
                    table.Rows.Add(row);
                }
            }
            return table;
        }
        #endregion

        #region 根据一个实体创建一个相应的DataTable
        /// <summary>
        /// 根据一个实体创建一个相应的DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type">过滤方式(0:选择列, 1:删除列)</param>
        /// <param name="propertyName">要过滤的列名</param>
        /// <returns></returns>
        public static DataTable CreateTable<T>(int type, params string[] propertyName)
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            List<string> propertyNameList = new List<string>();
            if (propertyName != null)
            {
                propertyNameList.AddRange(propertyName);
            }

            foreach (PropertyDescriptor prop in properties)
            {
                if (propertyNameList.Contains(prop.Name) && type == 1)
                    continue;
                else if (!propertyNameList.Contains(prop.Name) && type == 0)
                    continue;

                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            return table;
        }
        #endregion


        /// <summary>
        /// ObjectToDataTable
        /// </summary>
        /// <param name="objectModel"></param>
        public static DataTable ObjectToDataTable(dynamic objectModel)
        {
            var res = JsonConvert.DeserializeObject<DataTable>(JsonConvert.SerializeObject(objectModel));
            return res;
        }
        /// <summary>
        /// 将一个对象的 同名同类型的 属性 赋值给另一个对象的 同名同类型 属性
        /// 将T1的属性值 赋值给T2,返回T2
        /// 在赋值之前需要先判断T2中有没有T1的同名属性,如果没有则忽略赋值
        /// 相比ObjectAttribute2ObjectAttribute方法,ObjectAttribute2ObjectAttributeBeforeCheck效率低一些
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static T2 ObjectAttribute2ObjectAttributeBeforeCheck<T1, T2>(T1 t1, T2 t2) where T1 : class where T2 : class
        {
            Type t1Type = t1.GetType();
            Type t2Type = t2.GetType();
            PropertyInfo[] t1_pis = t1Type.GetProperties();
            PropertyInfo[] t2_pis = t2Type.GetProperties();

            foreach (PropertyInfo pi in t1_pis)
            {
                //先判断T2中有没有T1中的属性,没有则忽略
                bool _bool = false;
                for (int i = 0; i < t2_pis.Length; i++)
                {
                    if (t2_pis[i].Name == pi.Name)
                    {
                        _bool = true;
                        break;
                    }
                }

                if (_bool)
                {
                    object piValue = pi.GetValue(t1, null);
                    t2Type.GetProperty(pi.Name).SetValue(t2, piValue);
                }
            }
            return t2;
        }

        /// <summary>
        /// 将一个对象的 同名同类型的 属性 赋值给另一个对象的 同名同类型 属性
        /// 将T1的属性值 赋值给T2,返回T2
        /// 相比ObjectAttribute2ObjectAttributeBeforeCheck效率低一些,ObjectAttribute2ObjectAttribute效率高一些
        /// </summary>
        /// <typeparam name="T1">原类型对象</typeparam>
        /// <typeparam name="T2">目标类型对象</typeparam>
        /// <param name="t1">原类型对象实例</param>
        /// <param name="t2">目标类型对象实例</param>
        /// <returns></returns>
        public static T2 ObjectAttribute2ObjectAttribute<T1, T2>(T1 t1, T2 t2) where T1 : class where T2 : class
        {
            Type t1Type = t1.GetType();
            Type t2Type = t2.GetType();
            PropertyInfo[] t1_pis = t1Type.GetProperties();
            foreach (PropertyInfo pi in t1_pis)
            {
                object piValue = pi.GetValue(t1, null);
                t2Type.GetProperty(pi.Name).SetValue(t2, piValue);
            }
            return t2;
        }

        /// <summary>
        /// 深拷贝
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T DeepClone<T>(T obj)
        {
            //如果是字符串或值类型则直接返回
            if (obj is string || obj.GetType().IsValueType) return obj;

            object retval = Activator.CreateInstance(obj.GetType());
            FieldInfo[] fields = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            foreach (FieldInfo field in fields)
            {
                try { field.SetValue(retval, DeepClone(field.GetValue(obj))); }
                catch { }
            }
            return (T)retval;
        }

        private delegate T mapEntity<T>(DbDataReader dr);
        private static Dictionary<Type, Delegate> cachedMappers = new Dictionary<Type, Delegate>();

        /// <summary>
        /// 高性能DataReader to List类 使用 EMIT(参考：http://www.codeisbug.com/Doc/3/1113)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static IEnumerable<T> MapToEntities<T>(DbDataReader dr)
        {
            // If a mapping function from dr -> T does not exist, create and cache one
            if (!cachedMappers.ContainsKey(typeof(T)))
            {
                // Our method will take in a single parameter, a DbDataReader
                Type[] methodArgs = { typeof(DbDataReader) };

                // The MapDR method will map a DbDataReader row to an instance of type T
                DynamicMethod dm = new DynamicMethod("MapDR", typeof(T), methodArgs, Assembly.GetExecutingAssembly().GetType().Module);
                ILGenerator il = dm.GetILGenerator();

                // We'll have a single local variable, the instance of T we're mapping
                il.DeclareLocal(typeof(T));

                // Create a new instance of T and save it as variable 0
                il.Emit(OpCodes.Newobj, typeof(T).GetConstructor(Type.EmptyTypes));
                il.Emit(OpCodes.Stloc_0);

                foreach (PropertyInfo pi in typeof(T).GetProperties())
                {
                    // Load the T instance, SqlDataReader parameter and the field name onto the stack
                    il.Emit(OpCodes.Ldloc_0);
                    il.Emit(OpCodes.Ldarg_0);
                    il.Emit(OpCodes.Ldstr, pi.Name);

                    // Push the column value onto the stack
                    il.Emit(OpCodes.Callvirt, typeof(DbDataReader).GetMethod("get_Item", new Type[] { typeof(string) }));

                    // Depending on the type of the property, convert the datareader column value to the type
                    switch (pi.PropertyType.Name)
                    {
                        case "Int16":
                            il.Emit(OpCodes.Call, typeof(Convert).GetMethod("ToInt16", new Type[] { typeof(object) }));
                            break;
                        case "Int32":
                            il.Emit(OpCodes.Call, typeof(Convert).GetMethod("ToInt32", new Type[] { typeof(object) }));
                            break;
                        case "Int64":
                            il.Emit(OpCodes.Call, typeof(Convert).GetMethod("ToInt64", new Type[] { typeof(object) }));
                            break;
                        case "Boolean":
                            il.Emit(OpCodes.Call, typeof(Convert).GetMethod("ToBoolean", new Type[] { typeof(object) }));
                            break;
                        case "String":
                            il.Emit(OpCodes.Callvirt, typeof(string).GetMethod("ToString", new Type[] { }));
                            break;
                        case "DateTime":
                            il.Emit(OpCodes.Call, typeof(Convert).GetMethod("ToDateTime", new Type[] { typeof(object) }));
                            break;
                        case "Decimal":
                            il.Emit(OpCodes.Call, typeof(Convert).GetMethod("ToDecimal", new Type[] { typeof(object) }));
                            break;
                        default:
                            // Don't set the field value as it's an unsupported type
                            continue;
                    }

                    // Set the T instances property value
                    il.Emit(OpCodes.Callvirt, typeof(T).GetMethod("set_" + pi.Name, new Type[] { pi.PropertyType }));
                }

                // Load the T instance onto the stack
                il.Emit(OpCodes.Ldloc_0);

                // Return
                il.Emit(OpCodes.Ret);

                // Cache the method so we won't have to create it again for the type T
                cachedMappers.Add(typeof(T), dm.CreateDelegate(typeof(mapEntity<T>)));
            }

            // Get a delegate reference to the dynamic method
            mapEntity<T> invokeMapEntity = (mapEntity<T>)cachedMappers[typeof(T)];

            // For each row, map the row to an instance of T and yield return it
            while (dr.Read())
                yield return invokeMapEntity(dr);
        }
        /// <summary>
        /// 
        /// </summary>
        public static void ClearCachedMapperMethods()
        {
            cachedMappers.Clear();
        }

    }
}
