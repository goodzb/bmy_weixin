﻿using System.IO;
using System.Net;
using System.Text;

namespace RayelinkWeixin.Common.Utilites
{
    public class HttpHelper
    {
        public static string GetUrltoHtml(string Url, string type)

        {

            try

            {

                System.Net.WebRequest wReq = System.Net.WebRequest.Create(Url);

                // Get the response instance.

                System.Net.WebResponse wResp = wReq.GetResponse();

                System.IO.Stream respStream = wResp.GetResponseStream();

                // Dim reader As StreamReader = New StreamReader(respStream)

                using (System.IO.StreamReader reader = new System.IO.StreamReader(respStream, Encoding.GetEncoding(type)))
                {
                    return reader.ReadToEnd();
                }

            }

            catch (System.Exception)
            {
                //errorMsg = ex.Message;
            }

            return "";

        }

        ///<summary>

        ///采用https协议访问网络

        ///</summary>

        ///<param name="URL">url地址</param>

        ///<param name="strPostdata">发送的数据</param>

        ///<returns></returns>

        public static string OpenReadWithHttps(string URL, string method,string contentType, string strEncoding, string strPostdata="")

        {

            Encoding encoding = Encoding.UTF8;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);

            request.Method = method;

            request.Accept = "text/html, application/xhtml+xml, */*";

            request.ContentType = contentType;

            byte[] buffer = encoding.GetBytes(strPostdata);

            request.ContentLength = buffer.Length;

            request.GetRequestStream().Write(buffer, 0, buffer.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            using (StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.GetEncoding(strEncoding)))
            {

                return reader.ReadToEnd();
            }

        }

        public static string PostReq(string url,string data="",string contentType="application/json")
        {
            return OpenReadWithHttps(url, "POST", contentType, "utf-8", data);
        }

        public static string GetReq(string url)
        {
            return OpenReadWithHttps(url, "GET", "text/html", "utf-8");
        }

    }

}
