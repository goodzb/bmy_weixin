﻿using RayelinkWeixin.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Job
{
    public class ReVisitJob : BaseJob, IJob
    {
        Dictionary<string, Action> ActionDictionary = null;
        Action action = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public ReVisitJob(string[] args)
        {
            Dictionary<string, string> dict = this.GeneralArgs(args);
            string actionName = GetArgsValue<string>("actionName");

            if (string.IsNullOrEmpty(actionName))
            {
                throw new Exception("actionName不能为空,请核实......");
            }
            ActionDictionary = new Dictionary<string, Action> {
                { "TimeOutOrders", TimeOutOrders },
                { "InvalidTimeOutOrders", InvalidTimeOutOrders },
                { "PayRefundquery", PayRefundquery } };

            ActionDictionary.TryGetValue(actionName, out action);
            if (null == action)
            {
                throw new Exception("未找到所需要执行的方法,请核实......");
            }
            Start();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Start()
        {
            action();
        }

        /// <summary>
        /// 超时未关闭订单 -job ReVisitJob -actionName TimeOutOrders
        /// </summary>
        public void TimeOutOrders()
        {
            ReVisitBusiness.Instance.TimeOutOrders();
        }

        /// <summary>
        /// 无效订单的处理 -job ReVisitJob -actionName InvalidTimeOutOrders
        /// </summary>
        public void InvalidTimeOutOrders()
        {
            ReVisitBusiness.Instance.InvalidTimeOutOrders();
        }

        /// <summary> 
        /// 退款状态处理 -job ReVisitJob -actionName PayRefundquery
        /// </summary> 
        public void PayRefundquery()
        {
            ReVisitBusiness.Instance.PayRefundquery();          // 退款状态处理
        }
    }
}
