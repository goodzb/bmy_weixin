﻿using RayelinkWeixin.Common.Utilites;
using Senparc.Weixin.Cache;
using Senparc.Weixin.Cache.Redis;
using System;
using System.Collections.Generic;
using System.Reflection;
using CM = System.Configuration.ConfigurationManager;

namespace RayelinkWeixin.Job
{
    public class Program
    {
        /// <summary>
        /// 配置项允许的job名称
        /// </summary>
        static string className = CM.AppSettings["ActionInitWithClass"];

        static void Main(string[] args)
        {           
            try
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\log4net.config");   
                log4net.Config.XmlConfigurator.Configure(fileInfo);
                LogHelper.ConsoleWriteLine("启动Job....................");

                string jobType = string.Empty;
                //RayelinkWeixin.Job -job ReVisitJob -actionName TimeOutOrders
                if (args != null && args.Length > 0)
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        switch (args[i])
                        {
                            case "-job":
                                jobType = args[i + 1];
                                break;
                        }
                    }
                }
                string job = string.Empty;

                Jobs.TryGetValue(jobType, out job);

                if (string.IsNullOrWhiteSpace(job))
                {
                    Jobs.TryGetValue(className, out job);
                }

                if (!string.IsNullOrWhiteSpace(job))
                {
                    RegisterWeixinCache();  // Senparc.Weixin 自定义缓存策略
                    RegisterWeixinThreads();// 激活微信缓存
                    Assembly.GetExecutingAssembly().GetType(job).GetConstructor(new Type[1] { typeof(string[]) }).Invoke(new object[1] { args });
                }
            }
            catch (Exception e)
            {
                LogHelper.Exception(e);
                LogHelper.ConsoleWriteLine("出错了：" + e.Message);
                // throw;
            }
            LogHelper.ConsoleWriteLine("Job退出....................");
        }

        /// <summary>
        /// 字典参数, 
        /// 参数1和配置项ActionInitWithClass 对应
        /// 参数2是job类的完全限定名(包括命名空间)
        /// </summary>  

        private static readonly Dictionary<string, string> Jobs = new Dictionary<string, string>
            {
                {"ReVisitJob","RayelinkWeixin.Job.ReVisitJob"}
            };


        #region Senparc.Weixin 自定义缓存策略

        /// <summary>
        /// Senparc.Weixin 自定义缓存策略
        /// </summary>
        private static void RegisterWeixinCache()
        {
            //如果留空，默认为localhost（默认端口）
            Console.WriteLine("正在初始化Senparc.Weixin 自定义缓存策略，请稍候……");
            #region  Redis配置
            var redisConfiguration = TextHelper.GetDataConfigItem("RedisExchangeHosts"); //CM.AppSettings["RedisExchangeHosts"];
            RedisManager.ConfigurationOption = redisConfiguration;

            //如果不执行下面的注册过程，则默认使用本地缓存

            if (!string.IsNullOrEmpty(redisConfiguration))
            {
                CacheStrategyFactory.RegisterObjectCacheStrategy(() => RedisObjectCacheStrategy.Instance);//Redis
            }
            #endregion
            Console.WriteLine("初始化Senparc.Weixin 自定义缓存策略，完成……");
        }

        /// <summary>
        /// 激活微信缓存
        /// </summary>
        private static void RegisterWeixinThreads()
        {
            Senparc.Weixin.Threads.ThreadUtility.Register();
        }
        #endregion
    }
}
