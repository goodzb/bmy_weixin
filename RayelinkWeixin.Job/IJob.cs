﻿using RayelinkWeixin.Common.Share;
using RayelinkWeixin.Common.Utilites;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;


namespace RayelinkWeixin.Job
{
    /// <summary>
    /// IJob
    /// </summary>
    public interface IJob
    {
        /// <summary>
        /// Start
        /// </summary>
        void Start();
        /// <summary>
        /// Stop
        /// </summary>
        void Stop();
    }

    public enum JobState
    {
        Error=0,
        Success = 1
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class BaseJob : IJob
    {

        private DateTime beginDate = DateTime.Now; 

        public virtual void Start()
        {

        }

        public virtual void Stop()
        {

        }

        /// <summary>
        /// 记录Job运行log
        /// </summary>
        /// <param name="jobState">job状态</param>
        /// <param name="errorMessage">异常信息</param>
        public virtual void Stop(JobState jobState, string errorMessage = "")
        {
            SaveLog(jobState, errorMessage);
        }

        /// <summary>
        /// ExecuteNonQuery
        /// </summary>
        /// <param name="sqlResult"></param>
        /// <returns></returns>
        public ReturnMessage ExecuteNonQuery(string sqlText, System.Data.SqlClient.SqlConnection sqlConn)
        {

            ReturnMessage rm = new ReturnMessage();

            try
            {
                if (sqlConn.State == ConnectionState.Closed)
                {
                    sqlConn.Open();
                }
                System.Data.SqlClient.SqlCommand sqlComm = new System.Data.SqlClient.SqlCommand(sqlText, sqlConn);
                sqlComm.CommandTimeout = 7200;
                sqlComm.ExecuteNonQuery();
                sqlConn.Close();
                rm.IsSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LogHelper.Exception(ex);         
                sqlConn.Close();
                rm.IsSuccess = false;
                rm.Message = ex.Message;
            }
            return rm;
        }

        /// <summary>
        /// ExecuteScalar
        /// </summary>
        /// <param name="sqlResult"></param>
        /// <returns></returns>
        public ReturnMessage ExecuteScalar(string sqlText, System.Data.SqlClient.SqlConnection sqlConn)
        {

            ReturnMessage rm = new ReturnMessage();

            try
            {
                if (sqlConn.State == ConnectionState.Closed)
                {
                    sqlConn.Open();
                }
                System.Data.SqlClient.SqlCommand sqlComm = new System.Data.SqlClient.SqlCommand(sqlText, sqlConn);
                sqlComm.CommandTimeout = 7200;
                rm.ResultData["Data"] = sqlComm.ExecuteScalar();
                sqlConn.Close();
                rm.IsSuccess = true;
            }
            catch (Exception ex)
            {                
                Console.WriteLine(ex.Message);
                LogHelper.Exception(ex);
            }
            return rm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobState"></param>
        /// <param name="errorMessage"></param>
        private void SaveLog(JobState jobState, string errorMessage = "")
        {
       
        }

        public Dictionary<string, string> Args = new Dictionary<string, string>();

        /// <summary>
        /// 获取参数
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public Dictionary<string, string> GeneralArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                for (int i = 0; i < args.Length; i=i+2)
                {
                    if (args.Length >= i + 1)
                    {
                        Args.Add(args[i].Replace("-",""), args[i + 1]);
                    }
                }
            }
            return Args;
        }

        /// <summary>
        /// 获取参数值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetArgsValue<T>(string key,string defaultValue = null)
        {
            T result = default(T);

            if (Args.ContainsKey(key))
            {
                result = (T)Convert.ChangeType(Args[key], typeof(T));
            }
            else if(!string.IsNullOrWhiteSpace(defaultValue))
            {
                result = (T)Convert.ChangeType(defaultValue, typeof(T));
            }                                                                               
            return result;
        }


        /// <summary>
        /// 如果有线程池,检测线程池中的线程是否执行完成
        /// </summary>
        public void GlobalCheckThreadStatus()
        {

            //所有活动的线程池数量
            int availableCount = 0;
            //最大的线程池数量
            int maxWorkerThreads = 0;
            //已经完成的线程
            int tempCompletedCount = 0;

            //如果还存在活动线程则继续等待
            while (true)
            {
                //获取线程池中活动的线程和已经结束的线程
                ThreadPool.GetAvailableThreads(out availableCount, out tempCompletedCount);
                ThreadPool.GetMaxThreads(out maxWorkerThreads, out tempCompletedCount);

                //如果所有的线程都已经执行结束
                if (maxWorkerThreads - availableCount == 0)
                {
                    break;
                }

                //主线程休息半分钟
                Thread.Sleep(30000);
            }
        }

        #region SaveCSV
        /// <summary>
        /// 将DataTable中数据写入到CSV文件中
        /// </summary>
        /// <param name="dt">提供保存数据的DataTable</param>
        /// <param name="fileName">CSV的文件路径</param>
        /// <param name="separator">分隔符</param>
        public void SaveCSV<T>(List<T> lst, string fileName,string separator="@$@")
        {
            var tmp = lst[0];
            Type t = tmp.GetType();

            DataTable dt = new DataTable();
            foreach (var p in t.GetProperties())
            {
                dt.Columns.Add(p.Name);
            }
            foreach (var entity in lst)
            {
                DataRow row = dt.NewRow();
                foreach (var p in t.GetProperties())
                {
                    row[p.Name] = p.GetValue(entity, null);
                }
                dt.Rows.Add(row);

            }

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            FileStream fs = new FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default);
            string data = "";
            try
            {
                //写出列名称
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    data += dt.Columns[i].ColumnName.ToString();
                    if (i < dt.Columns.Count - 1)
                    {
                        data += separator;
                    }
                }
                sw.WriteLine(data);

                //写出各行数据
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    data = "";
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        data += dt.Rows[i][j].ToString();
                        if (j < dt.Columns.Count - 1)
                        {
                            data += separator;
                        }
                    }
                    sw.WriteLine(data);
                }
            }
            catch
            {

            }

            sw.Close();
            fs.Close();

        }
        #endregion



    }

    public static class StringExtension
    {
        /// <summary>
        /// 给string类扩展一个Contains的方法
        /// </summary>
        /// <param name="source"></param>
        /// <param name="keyWords"></param>
        /// <returns></returns>
        public static bool Contains(this string source, IList<string> keyWords)
        {
            bool result = false;

            foreach (string keyword in keyWords)
            {
                if (source.Contains(keyword))
                {
                    result = true;
                }
            }

            return result;
        }
    }
}
