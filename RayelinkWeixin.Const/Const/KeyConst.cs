﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：KeyConst
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-20 16:50:18
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Web.Configuration;
using CM = System.Configuration.ConfigurationManager;

namespace RayelinkWeixin.Const.Const
{
    /// <summary>
    /// 
    /// </summary>
    public class KeyConst
    {
        #region Cookie Key
        /// <summary>
        /// 微信拦截登陆
        /// </summary>
        public static readonly string SessionReturnUrl = "SessionReturnUrl";


        /// <summary>
        /// 绑定用户放回的url
        /// </summary>
        public static readonly string SessionBindUserBackUrl = "SessionBindUserBackUrl";

        /// <summary>
        /// 微信用户的头像
        /// </summary>
        public static readonly string SessionUserHeadImageUrl = "SessionUserHeadImageUrl";
        /// <summary>
        /// 微信用户的昵称
        /// </summary>
        public static readonly string SessionUserNickName = "SessionUserNickName";

        #endregion

        #region SessionKey

        /// <summary>
        /// 微信拦截登陆
        /// </summary>
        public static readonly string SessionAuthorizeUser = "SessionAuthorizeUser";


        public static readonly string SessionOrderUrl = "SessionOrderUrl";


        public static readonly string SessionPayState = "SessionPayState";


        ///// <summary>
        ///// SessionOpenID
        ///// </summary>
        //public static readonly string SessionOpenID = "SessionOpenID";


        #endregion

        #region Cache Key

        /// <summary>
        /// Cache 所有的科室
        /// </summary>
        public static readonly string CacheAllDept = "CacheAllDept";

        /// <summary>
        /// Cache 所有的职称
        /// </summary>
        public static readonly string CacheAllTitle = "CacheAllTitle";

        /// <summary>
        /// Cache JsAPITicket
        /// </summary>
        public static readonly string CacheJsAPITicket = "CacheJsAPITicket";

        // <summary>
        /// Cache 诊所详情
        /// </summary>
        public static readonly string CacheOrganization = "CacheOrganization";

        // <summary>
        /// Cache 所有城市
        /// </summary>
        public static readonly string CacheAllCity = "CacheAllCity";

        // <summary>
        /// Cache 医生详情
        /// </summary>
        public static readonly string CacheDoctor = "CacheDoctor";

        // <summary>
        /// Cache 医生列表
        /// </summary>
        public static readonly string CacheDoctorList = "CacheDoctorList";

        // <summary>
        /// Cache 新闻列表
        /// </summary>
        public static readonly string CacheNewsList = "CacheNewsList";

        /// <summary>
        /// 我的病例列表
        /// </summary>
        public static readonly string CacheCaseList = "CacheCaseList";

        /// <summary>
        /// 咨询记录
        /// </summary>
        public static readonly string CacheAllConsultation = "CacheAllConsultation";

        /// <summary>
        /// 评价记录
        /// </summary>
        public static readonly string CacheAllCommons = "CacheAllCommons";

        /// <summary>
        /// 机构评价记录
        /// </summary>
        public static readonly string CacheAllOrgCommons = "CacheAllOrgCommons";

        /// <summary>
        /// 送心意
        /// </summary>
        public static readonly string CacheAllPrizes = "CacheAllPrizes";

        // <summary>
        /// Cache 医生排班时间表
        /// </summary>
        public static readonly string CacheDoctorScheduleTime = "CacheDoctorScheduleTime";


        // <summary>
        /// Cache 医生排班最大可以选择的日期
        /// </summary>
        public static readonly string CacheLastSelectableDate = "CacheLastSelectableDate";

        // <summary>
        /// Cache 医生排班最小可以选择的日期
        /// </summary>
        public static readonly string CacheFirstSelectableDate = "CacheFirstSelectableDate";
        #endregion

        #region 行为事件操作类型(UserActionEventLog)
        /// <summary>
        /// View
        /// </summary>
        public static string Uceat_View = "view";

        /// <summary>
        /// click
        /// </summary>
        public static string Uceat_Click = "click";

        /// <summary>
        /// event
        /// </summary>
        public static string Uceat_Event = "event";

        /// <summary>
        /// share
        /// </summary>
        public static string Uceat_Share = "share";
        /// <summary>
        /// api
        /// </summary>
        public static string Uceat_API = "api";

        /// <summary>
        /// wechatservice,微信客服
        /// </summary>
        public static string Uceat_WechatService = "wechatservice";

        /// <summary>
        /// 授权
        /// </summary>
        public static string Uceat_OAuth = "OAuth";

        #endregion

        #region 复诊常量

        /// <summary>
        /// 支付微信标识
        /// </summary>
        public const short PayAppId = 1;

        /// <summary>
        /// 订单微信app标识
        /// </summary>
        public const short OrderAppId = 1;// GetConfigItem<short>("OrderAppId");

        /// <summary>
        /// 用户类型——用户
        /// </summary>
        public static short ReVisit_UserType_User = 0;

        /// <summary>
        /// 用户类型——医生
        /// </summary>
        public static short ReVisit_UserType_Doctor = 1;

        /// <summary>
        /// 用户类型——助理
        /// </summary>
        public static short ReVisit_UserType_Assistant = 2;


        //状态: 1：未支付，默认提交状态；2：H5支付成功；3:回调支付成功(这个是支付后微信回调post回来的支付数据，这才是真正意义上的支付成功):4：支付失败；5：支付过程中用户取消支付; 6：用户取消；
        //      7：超时job取消；8：医生结束咨询(此订单完成咨询服务)；20：未支付(远程门诊会存在)


        /// <summary>
        /// 默认查询条数
        /// </summary>
        public static int BasePageSize = 99999999;

        /// <summary>
        /// 订单状态: 1：未支付，默认提交状态
        /// </summary>
        public static short ReVisit_OrderStatus_1 = 1;

        /// <summary>
        /// 订单状态:2：H5支付成功
        /// </summary>
        public static short ReVisit_OrderStatus_2 = 2;

        /// <summary>
        /// 订单状态:3:回调支付成功(这个是支付后微信回调post回来的支付数据，这才是真正意义上的支付成功)
        /// </summary>
        public static short ReVisit_OrderStatus_3 = 3;

        /// <summary>
        /// 订单状态: 4：支付失败
        /// </summary>
        public static short ReVisit_OrderStatus_4 = 4;

        /// <summary>
        /// 订单状态: 5：支付过程中用户取消支付
        /// </summary>
        public static short ReVisit_OrderStatus_5 = 5;

        /// <summary>
        /// 订单状态: 6：用户取消
        /// </summary>
        public static short ReVisit_OrderStatus_6 = 6;

        /// <summary>
        ///  7：超时job取消(超过24小时医生没有回复，job取消了)
        /// </summary>
        public static short ReVisit_OrderStatus_7 = 7;

        /// <summary>
        /// 8：医生结束咨询(此订单完成咨询服务)
        /// </summary>
        public static short ReVisit_OrderStatus_8 = 8;

        /// <summary>
        /// 9:JOB关闭(超过24小时医生没有关闭，job关闭了)
        /// </summary>
        public static short ReVisit_OrderStatus_9 = 9;

        /// <summary>
        /// 10:退款中(已经提交了退款申请)
        /// </summary>
        public static short ReVisit_OrderStatus_10 = 10;

        /// <summary>
        /// 11:已退款(去接口扫描出已经退款成功的)
        /// </summary>
        public static short ReVisit_OrderStatus_11 = 11;

        /// <summary>
        /// 20：已经确认单还未支付(远程门诊会存在)
        /// </summary>
        public static short ReVisit_OrderStatus_20 = 20;

        /// <summary>
        /// 会话状态：0-未开始
        /// </summary>
        public const short ReVisit_ConversationStatus_0 = 0;

        /// <summary>
        /// 会话状态：1-医生未回复
        /// </summary>
        public const short ReVisit_ConversationStatus_1 = 1;

        /// <summary>
        /// 会话状态：2-用户未回复
        /// </summary>
        public const short ReVisit_ConversationStatus_2 = 2;

        /// <summary>
        /// 会话状态：3-用户取消
        /// </summary>
        public const short ReVisit_ConversationStatus_3 = 3;

        /// <summary>
        /// 会话状态：4-医生关闭
        /// </summary>
        public const short ReVisit_ConversationStatus_4 = 4;

        /// <summary>
        /// 会话状态：5-系统超时关闭(超过24小时医生没有回复，job取消了)
        /// </summary>
        public const short ReVisit_ConversationStatus_5 = 5;

        /// <summary>
        /// 会话状态：6-其他原因关闭
        /// </summary>
        public const short ReVisit_ConversationStatus_6 = 6;

        /// <summary>
        /// 会话状态：7：正在会话中
        /// </summary>
        public const short ReVisit_ConversationStatus_7 = 7;

        /// <summary>
        /// 订单类型：0-图文咨询
        /// </summary>
        public static short ReVisit_OrderType_0 = 0;

        /// <summary>
        /// 订单类型：1-远程门诊
        /// </summary>
        public static short ReVisit_OrderType_1 = 1;

        /// <summary>
        ///  Session_Type 1：上线
        /// </summary>
        public const short Session_Type_1 = 1;

        /// <summary>
        /// Session_Type 2：下线
        /// </summary>
        public const short Session_Type_2 = 2;

        /// <summary>
        /// 会话消息来源：0-用户发送
        /// </summary>
        public const short FZ_Conversations_Source_0 = 0;

        /// <summary>
        /// 会话消息来源：1-医生发送
        /// </summary>
        public const short FZ_Conversations_Source_1 = 1;

        /// <summary>
        /// 会话消息来源：2-系统发送，不在用户展示页面（用户身份）
        /// </summary>
        public const short FZ_Conversations_Source_2 = 2;

        /// <summary>
        /// 会话消息来源：3-系统发送，不在用户展示页面（医生身份）
        /// </summary>
        public const short FZ_Conversations_Source_3 = 3;

        /// <summary>
        ///  会话消息来源：4-系统发送，在用户展示页面（医生身份）
        /// </summary>
        public const short FZ_Conversations_Source_4 = 4;

        /// <summary>
        /// 会话消息来源：5-系统发送，在医生展示页面
        /// </summary>
        public const short FZ_Conversations_Source_5 = 5;

        /// <summary>
        /// 会话消息来源：10-系统错误错误消息
        /// </summary>
        public const short FZ_Conversations_Source_10 = 10;

        /// <summary>
        /// 聊天类别：0-文字
        /// </summary>
        public const short FZ_Conversations_Chat_Type_0 = 0;

        /// <summary>
        /// 聊天类别：1-图片 
        /// </summary>
        public const short FZ_Conversations_Chat_Type_1 = 1;

        /// <summary>
        /// 聊天类别：2-语音 
        /// </summary>
        public const short FZ_Conversations_Chat_Type_2 = 2;

        /// <summary>
        /// 聊天类别：3-文件 
        /// </summary>
        public const short FZ_Conversations_Chat_Type_3 = 3;

        /// <summary>
        /// 支付类型：0-付款  H5
        /// </summary>

        public const short FZ_PayRecordType_0 = 0;

        /// <summary>
        /// 订单微信支付记录信息 支付类型：1-付款(回调付款成功)
        /// </summary>
        public const short FZ_PayRecordType_1 = 1;

        /// <summary>
        /// 订单微信支付记录信息 支付类型：2：申请退款
        /// </summary>
        public const short FZ_PayRecordType_2 = 2;

        /// <summary>
        /// 订单微信支付记录信息 支付类型：3：退款成功
        /// </summary>
        public const short FZ_PayRecordType_3 = 3;
        #endregion


        /// <summary>
        /// 是否是测试支付，为1的话支付金额为1分钱
        /// </summary>
        public static string PayTest = WebConfigurationManager.AppSettings["PayTest"];


        /// <summary>
        /// “CARD_STATUS_NOT_VERIFY”,待审核；
        //“CARD_STATUS_VERIFY_FAIL”,审核失败；
        //“CARD_STATUS_VERIFY_OK”，通过审核；
        //“CARD_STATUS_DELETE”，卡券被商户删除；
        //“CARD_STATUS_DISPATCH”，在公众平台投放过的卡券；
        //https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1451025272
        /// </summary>
        public static List<string> WxCardStatus = new List<string> { "CARD_STATUS_NOT_VERIFY", "CARD_STATUS_VERIFY_FAIL", "CARD_STATUS_VERIFY_OK", "CARD_STATUS_DELETE", "CARD_STATUS_DISPATCH" };

        #region 获取webconfig当中appSetting中的配置项
        /// <summary>
        /// 获取webconfig当中appSetting中的配置项
        /// </summary>
        /// <param name="key">配置项的键</param>
        /// <returns>配置项的值</returns>
        public static T GetConfigItem<T>(string key)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(CM.AppSettings[key]))
                {
                    return (T)Convert.ChangeType(CM.AppSettings[key], typeof(T));
                }
                else
                {
                    return default(T);
                }
            }
            catch
            {
                return (T)Convert.ChangeType(DBNull.Value, typeof(T));
            }
        }
        #endregion
    }
}
