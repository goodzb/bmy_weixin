﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：WeixinConfig
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-14 13:29:37
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using System.Web.Configuration;

namespace RayelinkWeixin.Const.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class WeixinConfig
    {
        /// <summary>
        /// 微信appID
        /// </summary>
        public static string appID = WebConfigurationManager.AppSettings["appID"];

        /// <summary>
        /// 微信appsecret
        /// </summary>
        public static string appSecret = WebConfigurationManager.AppSettings["appSecret"];

        /// <summary>
        /// 域名
        /// </summary>
        public static string webDomain = WebConfigurationManager.AppSettings["webDomain"];


        /// <summary>
        /// crmPartner
        /// </summary>
        public static string crmPartner = WebConfigurationManager.AppSettings["crmPartner"];


        /// <summary>
        /// 端口号
        /// </summary>
        public static string port = WebConfigurationManager.AppSettings["port"];

        /// <summary>
        /// 腾讯地图Key
        /// </summary>
        public static string qqMapKey = WebConfigurationManager.AppSettings["qqMapKey"];

        /// <summary>
        /// CRS服务器端口地址
        /// </summary>
        public static string crsUrl = WebConfigurationManager.AppSettings["crsUrl"];

        /// <summary>
        /// 获取用户信息的接口
        /// </summary>
        public static string userApi = WebConfigurationManager.AppSettings["userApi"];

        public static string PdfPath = WebConfigurationManager.AppSettings["PdfPath"];


        #region 支付

        public static string TenPayV3_MchId = WebConfigurationManager.AppSettings["TenPayV3_MchId"];

        public static string TenPayV3_Key = WebConfigurationManager.AppSettings["TenPayV3_Key"];

        public static string TenPayV3_AppId = WebConfigurationManager.AppSettings["TenPayV3_AppId"];

        public static string TenPayV3_AppSecret = WebConfigurationManager.AppSettings["TenPayV3_AppSecret"];

        public static string TenPayV3_TenpayNotify = WebConfigurationManager.AppSettings["TenPayV3_TenpayNotify"];

        /// <summary>
        /// 支付证书物理路径
        /// </summary>
        public static string TenPayV3_CertPath = WebConfigurationManager.AppSettings["TenPayV3_CertPath"];

        /// <summary>
        /// 支付证书密码
        /// </summary>
        public static string TenPayV3_CertPasWord = WebConfigurationManager.AppSettings["TenPayV3_CertPasWord"];
        #endregion

        /// <summary>
        /// 微信消息加密Key
        /// </summary>
        public static string EncodingAESKey = WebConfigurationManager.AppSettings["EncodingAESKey"];

        #region 模板消息


        /// <summary>
        /// 预约成功
        /// </summary>
        public static string BookingSuccess = WebConfigurationManager.AppSettings["BookingSuccess"];

        /// <summary>
        /// 预约失败
        /// </summary>
        public static string BookingFail = WebConfigurationManager.AppSettings["BookingFail"];

        /// <summary>
        /// 取消预约
        /// </summary>
        public static string BookingCancel = WebConfigurationManager.AppSettings["BookingCancel"];


        /// <summary>
        /// 加号/点诊申请成功
        /// </summary>
        public static string PlusClinicalSuccess = WebConfigurationManager.AppSettings["PlusClinicalSuccess"];

        #endregion

        /// <summary>
        /// 微信授权地址
        /// </summary>
        public static string WxOAuthAuthorize = WebConfigurationManager.AppSettings["WxOAuthAuthorize"];

    }
}
