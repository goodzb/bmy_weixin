﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：OrderModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-28 9:26:58
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


using System;
namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderModel
    {
        public int docID { get; set; }
        public string docName { get; set; }

        public string docImage { get; set; }

        public string deptName { get; set; }

        public string titleName { get; set; }

        public string isRemote { get; set; }

        public string date { get; set; }
        public string time { get; set; }
        public string onOffLine { get; set; }
        public string type { get; set; }
        public int hospitalID { get; set; }

        public int remoteHospId { get; set; }

        public int pid { get; set; }

        public string age { get; set; }

        public int clinicID { get; set; }

        public int clinicinfoID { get; set; }

        public string hospitalName { get; set; }

        public string address { get; set; }

        public double fee { get; set; }

        public int famliyId { get; set; }

        public int userId { get; set; }

        public int nowUserId { get; set; }

        public bool IsDefault { get; set; }

        public string formatDate { get; set; }

        public string formatTime { get; set; }

        /// <summary>
        /// 症状病情
        /// </summary>
        public string message { get; set; }

        public string target { get; set; }


        /// <summary>
        /// 就诊人姓名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 就诊人电话
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 就诊人身份证
        /// </summary>
        public string id { get; set; }

        public DateTime? Birthday { get; set; }


        public string sex { get; set; }

        /// <summary>
        /// 就诊人图片信息 用逗号隔开
        /// </summary>
        public string medicalImage { get; set; }

        public bool isLastSelectableDate { get; set; }
        public bool isFirstSelectableDate { get; set; }
    }
}
