﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：PrizeModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-25 13:29:19
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 心意
    /// </summary>
    public class PrizeModel
    {
        public int id { get; set; }

        public int pid { get; set; }

        public int prizeImgID { get; set; }

        public string userName { get; set; }

        public string info { get; set; }

        public string date { get; set; }

        public int Level { get; set; }

        public string PayTime { get; set; }

        public string Content { get; set; }

    }
}
