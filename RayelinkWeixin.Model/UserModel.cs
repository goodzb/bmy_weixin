﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：UserModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-19 9:56:35
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


using System;
namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class UserModel
    {
        public string UserName { get; set; }

        public string Telphone { get; set; }

        public string ValidCode { get; set; }
        public string image { get; set; }
        public int id { get; set; }
        public string time { get; set; }
        public string messages_title { get; set; }//标题
        public string d_message { get; set; }
        public string u_message { get; set; }
        public int UserId { get; set; }

        public string url { get; set; }

        public int pid { get; set; }

        public int type { get; set; }

        public string FamilyName { get; set; }

        public string FamilyMobile { get; set; }

        public string Gender { get; set; }

        public bool IsDefault { get; set; }

        public int isSetDefault { get; set; }

        public string IDCardNo { get; set; }

        public DateTime? Birthday { get; set; }

        public int orderNum { get; set; }

        public int commNum { get; set; }

        public int caseNum { get; set; }

        public int MyCashNum { get; set; }

    }
}
