﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：CommentModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-25 13:26:16
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 评论
    /// </summary>
    public class UserCommentModel
    {
        public int id { get; set; }

        public string title { get; set; }

        public string info { get; set; }

        public string userName { get; set; }

        public string date { get; set; }

        public int[] rating { get; set; }

        public DoctorCommentModel docComment { get; set; }

        public int subjectId { get; set; }
        public string subjectName { get; set; }
        public string content { get; set; }
        public int stars { get; set; }
        public string replyContent { get; set; }
        public string reply_time { get; set; }
        public string add_time { get; set; }
        public int type { get; set; }
        public int visitId { get; set; }
        public bool hasReservation { get; set; }
        public string visitResult { get; set; }
    }
}
