﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Model
{
   public class UserInfoModel
    {
        public int PID { get; set; }
        public string 姓名 { get; set; }
        public string 出生日期 { get; set; }
        public string 联系手机 { get; set; }
        public int 性别 { get; set; }
        public string 联系地址 { get; set; }
        public string 联系电话 { get; set; }
        public string 联系邮箱 { get; set; }
        public string 单位名称 { get; set; }
    }
}
