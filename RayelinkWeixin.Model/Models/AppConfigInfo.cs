
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果重新生成代码，这些更改将会丢失。
//     生成时间 2017-06-01 17:57:24  by xiao99
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using System.Data;
//using static RayelinkWeixin.Model.DBConnection;
namespace RayelinkWeixin.Model
{   
     /// <summary>
     /// App配置表
     /// </summary> 

	[TableName("dbo.AppConfig")]



	[PrimaryKey("ConfigId")]




	[ExplicitColumns]

    public partial class AppConfigInfo : DBConnection.Record<AppConfigInfo>  
    {



	
		/// <summary>
        ///  自增列
        /// </summary> 
		[Column] 
		public int ConfigId { get; set; }





	
		/// <summary>
        ///  AppID
        /// </summary> 
		[Column] 
		public int AppId { get; set; }





	
		/// <summary>
        ///  配置项类型
        /// </summary> 
		[Column] 
		public byte Type { get; set; }





	
		/// <summary>
        ///  配置项名称
        /// </summary> 
		[Column] 
		public string Name { get; set; }





	
		/// <summary>
        ///  配置项键
        /// </summary> 
		[Column] 
		public string Key { get; set; }





	
		/// <summary>
        ///  配置项值
        /// </summary> 
		[Column] 
		public string Value { get; set; }





	
		/// <summary>
        ///  配置项说明
        /// </summary> 
		[Column] 
		public string Memo { get; set; }





	
		/// <summary>
        ///  状态(1:启用,0:禁用)
        /// </summary> 
		[Column] 
		public byte Status { get; set; }





	
		/// <summary>
        ///  创建者
        /// </summary> 
		[Column] 
		public int Creator { get; set; }





	
		/// <summary>
        ///  创建时间
        /// </summary> 
		[Column] 
		public DateTime CreateTime { get; set; }





	
		/// <summary>
        ///  更新者
        /// </summary> 
		[Column] 
		public int Updator { get; set; }





	
		/// <summary>
        ///  最后一次更新时间
        /// </summary> 
		[Column] 
		public DateTime LastUpdateTime { get; set; }



	}
}

