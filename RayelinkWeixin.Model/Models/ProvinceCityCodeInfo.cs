
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由T4模板自动生成
//     对此文件的更改可能会导致不正确的行为，并且如果重新生成代码，这些更改将会丢失。
//     生成时间 2017-06-01 17:57:24  by xiao99
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using System.Data;
//using static RayelinkWeixin.Model.DBConnection;
namespace RayelinkWeixin.Model
{   
     /// <summary>
     /// 省份城市代码表
     /// </summary> 

	[TableName("dbo.ProvinceCityCode")]



	[PrimaryKey("ProvinceCityCodeID")]




	[ExplicitColumns]

    public partial class ProvinceCityCodeInfo : DBConnection.Record<ProvinceCityCodeInfo>  
    {



	
		/// <summary>
        ///  ID，自增列
        /// </summary> 
		[Column] 
		public int ProvinceCityCodeID { get; set; }





	
		/// <summary>
        ///  名称
        /// </summary> 
		[Column] 
		public string Name { get; set; }





	
		/// <summary>
        ///  1,country 2,province,3,city 4,district
        /// </summary> 
		[Column] 
		public byte Level { get; set; }





	
		/// <summary>
        ///  父ID
        /// </summary> 
		[Column] 
		public int ParentID { get; set; }





	
		/// <summary>
        ///  
        /// </summary> 
		[Column] 
		public string PostCode { get; set; }





	
		/// <summary>
        ///  组别:20151229:截止到20151229国家提供的数据
        /// </summary> 
		[Column] 
		public int GroupID { get; set; }





	
		/// <summary>
        ///  标准区县代码
        /// </summary> 
		[Column] 
		public int StandardCode { get; set; }





	
		/// <summary>
        ///  
        /// </summary> 
		[Column] 
		public short Status { get; set; }



	}
}

