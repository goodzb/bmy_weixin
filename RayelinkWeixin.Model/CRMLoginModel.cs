﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：CRMLoginModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-20 17:14:06
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class CRMLoginModel
    {
        public string result { get; set; }

        public string pid { get; set; }

        public string name { get; set; }
        public string userName { get; set; }

        public string source { get; set; }
    }
}
