﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：DoctorCommentModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-25 13:53:24
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class DoctorCommentModel
    {
        public int id { get; set; }

        public string image { get; set; }

        public string docName { get; set; }

        public string info { get; set; }

        public string date { get; set; }
    }
}
