﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：HospitalModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-27 15:17:54
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


using System;
using System.Collections.Generic;
namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class HospitalModel
    {
        public int hospitalID { get; set; }

        public int remoteHospId { get; set; }

        public string hospitalName { get; set; }
        public string hospitalAddress { get; set; }

        public int hospitalComment { get; set; }

        public string type { get; set; }
        public string hospitalImage { get; set; }

        public string url { get; set; }

        public int[] rating { get; set; }

        public string distance { get; set; }

        public int dist { get; set; }

        public string onOffLine { get; set; }

        public string docID { get; set; }

        public string selectDate { get; set; }

        public string selectTime { get; set; }

        public string hospitalTel { get; set; }

        public string hospitalIntroduce { get; set; }

        public List<UserCommentModel> listUserComment { get; set; }

        public string  coordinate { get; set; }

        public string target { get; set; }


        public double fare { get; set; }

        public List<String> CarPicList { get; set; }

        public int comments { get; set; }

        public int clinicID { get; set; }

        public int remote { get; set; }

        public int clinicinfoID { get; set; }

        public string range { get; set; }
    }
}
