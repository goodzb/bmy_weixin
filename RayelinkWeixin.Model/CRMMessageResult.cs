﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：CRMMessageResult
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-19 12:04:16
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class CRMMessageResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CRMMessageResult"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool success { get; set; }
        public string serialNumber { get; set; }
        public string extendNum { get; set; }
        public string info { get; set; }

    }
}
