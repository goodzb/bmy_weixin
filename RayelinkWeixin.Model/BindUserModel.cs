﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：BindUserModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-03-14 14:15:39
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class BindUserModel
    {
        public string UserName { get; set; }

        public string Telphone { get; set; }

        public string ValidCode { get; set; }
    }
}
