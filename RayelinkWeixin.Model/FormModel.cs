﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：FormModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-19 9:46:35
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class FormModel<T> where T : class
    {
        public T TModel { get; set; }

        private Message _message;
        public Message message
        {
            get
            {
                if (_message == null)
                {
                    _message = new Message();
                    _message.success = true;
                }
                return _message;
            }
            set
            {
                _message = value;
            }
        }
    }
}
