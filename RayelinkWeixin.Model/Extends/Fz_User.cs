﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RayelinkWeixin.Model
{
    public partial class FZ_User
    {
        public bool IsActive;

        public Action logOutAction;

        private int heartBeatsCount;

        public FZ_User()
        {
            IsActive = true;
            Timer timer = new Timer(p => { if (++heartBeatsCount > 30 && logOutAction != null) { IsActive = false; logOutAction(); } }, null, 0, 1000);
        }

        public void SetActive()
        {
            IsActive = true;
            heartBeatsCount = 0;
        }
    }
}
