﻿using RayelinkWeixin.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace PetaPoco
{
    /// <summary>
    /// PetaPoco扩展类
    /// </summary>
    public partial class Database
    {
        private SqlBulkCopy bulkCopy;

        /// <summary>
        /// 批量向 sql 插入多行(这个是传统的sql插入)
        /// </summary>
        /// <param name="tableName">要插入到的表的名称 </param>
        /// <param name="primaryKeyName">表的主键列的名称 </param>
        /// <param name="pocos">指定要插入的列值的 poco 对象</param>
        /// <param name="batchSize">每次插入条数</param>  
        /// <param name="isTransaction">是否使用事务</param>  
        /// <param name="autoIncrement">如果主键由 db 自动分配, 则为true</param>
        /// <returns>影响行数</returns>
        public int BulkInsert(string tableName, string primaryKeyName, IEnumerable<object> pocos, int batchSize, bool isTransaction = true, bool autoIncrement = true)
        {
            int result = 0;
            try
            {
                OpenSharedConnection();
                try
                {
                    using (var cmd = CreateCommand(_sharedConnection, ""))
                    {
                        var pd = PocoData.ForObject(pocos.First(), primaryKeyName, _defaultMapper);
                        // Create list of columnnames only once
                        var names = new List<string>();
                        foreach (var i in pd.Columns)
                        {
                            // Don't insert result columns
                            if (i.Value.ResultColumn)
                                continue;

                            // Don't insert the primary key (except under oracle where we need bring in the next sequence value)
                            if (autoIncrement && primaryKeyName != null && string.Compare(i.Key, primaryKeyName, true) == 0)
                            {
                                // Setup auto increment expression
                                string autoIncExpression = _provider.GetAutoIncrementExpression(pd.TableInfo);
                                if (autoIncExpression != null)
                                {
                                    names.Add(i.Key);
                                }
                                continue;
                            }
                            names.Add(_provider.EscapeSqlIdentifier(i.Key));
                        }
                        var namesArray = names.ToArray();

                        var values = new List<string>();
                        int count = 0;
                        do
                        {
                            cmd.CommandText = "";
                            cmd.Parameters.Clear();
                            var index = 0;
                            foreach (var poco in pocos.Skip(count).Take(batchSize))
                            {
                                values.Clear();
                                foreach (var i in pd.Columns)
                                {
                                    // Don't insert result columns
                                    if (i.Value.ResultColumn) continue;

                                    // Don't insert the primary key (except under oracle where we need bring in the next sequence value)
                                    if (autoIncrement && primaryKeyName != null && string.Compare(i.Key, primaryKeyName, true) == 0)
                                    {
                                        // Setup auto increment expression
                                        string autoIncExpression = _provider.GetAutoIncrementExpression(pd.TableInfo);
                                        if (autoIncExpression != null)
                                        {
                                            values.Add(autoIncExpression);
                                        }
                                        continue;
                                    }

                                    values.Add(string.Format("{0}{1}", _paramPrefix, index++));
                                    AddParam(cmd, i.Value.GetValue(poco), i.Value.PropertyInfo);
                                }

                                string outputClause = String.Empty;
                                if (autoIncrement)
                                {
                                    outputClause = _provider.GetInsertOutputClause(primaryKeyName);
                                }

                                cmd.CommandText += string.Format("INSERT INTO {0} ({1}){2} VALUES ({3})", _provider.EscapeTableName(tableName),
                                                                 string.Join(",", namesArray), outputClause, string.Join(",", values.ToArray()));
                            }
                            // Are we done?
                            if (cmd.CommandText == "") break;
                            if (isTransaction)
                            {
                                cmd.CommandText = " begin transaction " + cmd.CommandText + " commit transaction;";
                            }
                            count += batchSize;
                            DoPreExecute(cmd);
                            result += cmd.ExecuteNonQuery();
                            OnExecutedCommand(cmd);
                        }
                        while (true);

                    }
                }
                finally
                {
                    CloseSharedConnection();
                }
            }
            catch (Exception x)
            {
                result = -1;
                if (OnException(x))
                    throw;
            }

            return result;
        }

        /// <summary>
        /// 执行 sql 大容量插入
        /// </summary>
        /// <param name="pocos">指定要插入的列值的 poco 对象列表</param>        
        /// <param name="isTransaction">是否使用事务</param>  
        /// <param name="batchSize">每次插入条数</param>    
        /// <returns>影响行数</returns>
        public int BulkInsert(IEnumerable<object> pocos, bool isTransaction, int batchSize)
        {
            if (!pocos.Any()) return 0;
            var pd = PocoData.ForType(pocos.First().GetType(), _defaultMapper);

            #region 当前参数的个数大于2100会报“传入的表格格式数据流 (TDS)远程过程调用(RPC)协议流不正确。此 RPC 请求中提供了过多的参数。最多应为 2100”错误，故此，对batchSize做限制处理
            int columnsCount = pd.QueryColumns.Count();  //列的总数
            int maxBatchSize = 2100 / columnsCount;
            if (2100 % columnsCount > 0) maxBatchSize -= 1;
            batchSize = batchSize > maxBatchSize ? maxBatchSize : batchSize;
            #endregion

            return BulkInsert(pd.TableInfo.TableName, pd.TableInfo.PrimaryKey, pocos, batchSize, isTransaction, pd.TableInfo.AutoIncrement);
        }

        /// <summary>
        /// 初始化SqlBulkCopy
        /// </summary>
        /// <param name="sourceTable">生成的数据表(无需有数据，有结构即可)</param>
        /// <param name="tableName">生成的表明</param>
        private void InitSqlBulkCopy(DataTable sourceTable, string tableName)
        {
            bulkCopy = new SqlBulkCopy(_connectionString)
            {
                DestinationTableName = tableName,
                BulkCopyTimeout = 300 // 超时之前操作完成允许的秒数
            };
            for (int i = 0; i < sourceTable.Columns.Count; i++)
            {
                /*------设置映射关系------*/
                bulkCopy.ColumnMappings.Add(sourceTable.Columns[i].ColumnName, sourceTable.Columns[i].ColumnName);
            }
        }

        /// <summary>
        /// 生成SqlBulkCopy数据源
        /// </summary>
        /// <param name="list">对象列表</param>
        /// <returns>DataTable</returns>
        private DataTable GenerateSqlBulkCopyDataSource<T>(IEnumerable<T> list)
        {
            DataTable table = RayelinkWeixin.Common.Utilites.ObjectConvertHelper.ListConvertToDataTable<T>(list);
            return table;
        }

        /// <summary>
        /// 批量插入（使用SqlBulkCopy操作）
        /// </summary>
        /// <param name="list">操作对象List</param>
        /// <returns>是否成功(true：成功，false：失败)</returns>
        public bool SqlBulkCopyInsert<T>(IEnumerable<T> list)
        {
            if (list == null || !list.Any())
                return false;
            try
            {
                var pd = PocoData.ForType(list.First().GetType(), _defaultMapper);
                string tableName = pd.TableInfo.TableName.Replace("dbo.","");
                DataTable sourceTable = GenerateSqlBulkCopyDataSource(list);  //将list对象转为DataTable              
                InitSqlBulkCopy(sourceTable.Clone(), tableName);  //初始化SqlBulkCopy
                bulkCopy.BatchSize = list.Count();
                bulkCopy.WriteToServer(sourceTable);
                return true;
            }
            catch (Exception x)
            {
                if (OnException(x))
                    throw;
            }
            return false;
        }
    }
}
