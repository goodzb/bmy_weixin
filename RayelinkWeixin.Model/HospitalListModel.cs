﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：HospitalListModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-26 16:34:59
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/
using System.Collections.Generic;

namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class HospitalListModel
    {
        /// <summary>
        /// 城市选择
        /// </summary>
        public List<SelectItemModel> listCity { get; set; }

        public int docID { get; set; }
        public string date { get; set; }
        public string time { get; set; }

        public string type { get; set; }

        public string isRemote { get; set; }

        public int hospitalID { get; set; }

        public int remoteHospId { get; set; }

        public int clinicinfoID { get; set; }

        public int clinicID { get; set; }

        public string target { get; set; }
    }
}
