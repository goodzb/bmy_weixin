﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Model
{
  public   class CheckUserModel
    {
        public int orderNum { get; set; }
        public int commNum { get; set; }
        public int caseNum { get; set; }
        public int MyCashNum { get; set; }
        public List<UserInfoModel> Userinfo { get; set; }
    }
}
