﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Model
{
    public class ClinicCategoryModel
    {
        public double consultationFee { get; set; }
        public double registrationFee { get; set; }
        

        public double remoteconsultationFee { get; set; }
        public double remoteregistrationFee { get; set; }
        
    }
}
