﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：Message
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-19 9:47:09
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class Message
    {
        public bool success { get; set; }

        public string message { get; set; }

        public int id { get; set; }

        public int docID { get; set; }

        public string date { get; set; }

        public string userName { get; set; }

        public string info { get; set; }

        public int Type { get; set; }

        public string CreateTime { get; set; }

        public string Content { get; set; }
    }
}
