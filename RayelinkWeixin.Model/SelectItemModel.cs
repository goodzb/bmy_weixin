﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：SelectItemModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-21 11:28:59
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


using System;
namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class SelectItemModel
    {
        public string id { get; set; }

        public string name { get; set; }
    }
}
