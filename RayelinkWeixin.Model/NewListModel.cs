﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Model
{
    public class NewListModel
    {
        public DataTable Table { get; set; }

        public int RecordCount  {get; set; }

        public int id  {get; set; }
        public string title { get; set; }
        public DateTime add_time { get; set; }

        public DateTime update_time { get; set; }
        public string content { get; set; }
        public string format_time { get; set; }

    }
}
