﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Model
{
  public  class ResvIntentionModel
    {
        public int id { get; set; }
        public string ResvNo { get; set; }
        public int Pid { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Sex { get; set; }
        public string CertNo { get; set; }

        public DateTime BirthDay { get; set; }
        public int DoctorId { get; set; }

        public string DoctorName { get; set; }
        public int HospId { get; set; }
        public string HospName { get; set; }
        public int DepId { get; set; }
        public string DepName { get; set; }
        public int DepType { get; set; }
        public string DepTypeName { get; set; }
        public DateTime ResvDate { get; set; }
        public int DurationId { get; set; }
        public string Duration { get; set; }
        public string IsRemote { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public int OperatorPid { get; set; }
        public string OperatorName { get; set; }
        public string OperatorMobile { get; set; }
        public int Valid { get; set; }
        public string ServiceRecord { get; set; }
        public DateTime ConfirmTime { get; set; }
        public int ConfirmUserId { get; set; }
        public string UserIP { get; set; }
        public string Platform { get; set; }
        public string Remark { get; set; }
        public DateTime CreateTime { get; set; }
        public int RemoteHospId { get; set; }
        public string RemoteHospName { get; set; }
        public string Description { get; set; }
        public string Pictures { get; set; }
        public string Price { get; set; }
        public string VisitAddress { get; set; }
    }
}
