﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：AmModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-03-02 13:17:13
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


using System.Collections.Generic;
namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class TimeModel
    {
        public string className { get; set; }

        public string text { get; set; }
        public string date { get; set; }
        public List<string> time { get; set; }


    }
}
