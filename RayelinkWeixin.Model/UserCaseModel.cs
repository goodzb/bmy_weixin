﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Model
{
    public class UserCaseModel
    {
        public int PID { get; set; }
        public string 就诊ID { get; set; }
        public string 就诊日期 { get; set; }

        public string 就诊时间 { get; set; }
        public string 接诊医生姓名 { get; set; }
        public string 接诊医生科室 { get; set; }

        public string 接诊医生工号 { get; set; }
        public string 诊断结论 { get; set; }

        public string 诊断建议 { get; set; }

        public int 病历ID { get; set; }

        public int 诊所ID { get; set; }

        public string 医生头像 { get; set; }

        public string 图章 { get; set; }
        public string 就诊人 { get; set; }

        public string 诊所名 { get; set; }
        public string 医生职称 { get; set; }
        public string 类型 { get; set; }

        public string 主诉 { get; set; }

        public string 既往病史 { get; set; }

        public double 费用 { get; set; }

        public string 评论数量 { get; set; }

        public int 是否查看 { get; set; }

        public string html { get; set; }

        public int messageId { get; set; }

        public List<ReportModel> listReportModel { get; set; }

        public List<PacsPicturesModel> listPacsPicturesModel { get; set; }
        
    }
}
