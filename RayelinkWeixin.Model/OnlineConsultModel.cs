﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：ConsultModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-25 13:28:38
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 在线咨询
    /// </summary>
    public class OnlineConsultModel
    {
        public int id { get; set; }

        public int pid { get; set; }

        public int DoctorId { get; set; }

        public string UserName { get; set; }

        public string CreateTime { get; set; }

        public string Content { get; set; }

        public string UserSex { get; set; }

        public int type { get; set; }

        public int ConsultationId { get; set; }

    }
}
