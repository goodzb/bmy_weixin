﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：WeixinPayModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-03-08 14:11:22
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/


namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class WeixinPayModel
    {
        public string appID { get; set; }
        public string timeStamp { get; set; }
        public string nonceStr { get; set; }

        public string package { get; set; }
        public string paySign { get; set; }

        public string docID { get; set; }
        public int type { get; set; }
    }
}
