﻿/*----------------------------------------------------------------
// Copyright (C) 2016苏州盈天地资讯科技公司
// 版权所有。
// 文   件   名：DoctorModel
// 文件功能描述：
//
// 
// 创 建 人：haiqing
// 创建日期：2016-01-22 13:16:27
//
// 修 改 人：
// 修改描述：
//
// 修改标识：
//----------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Data;

namespace RayelinkWeixin.Model
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class DoctorModel
    {
        public int id { get; set; }

        public string doctorNumber { get; set; }

        public string image { get; set; }

        public string name { get; set; }

        public int deptID { get; set; }

        public string deptName { get; set; }

        public int titleID { get; set; }

        public string titleName { get; set; }

        public int orgID { get; set; }

        public int hospitalID { get; set; }

        public int clinicinfoID { get; set; }

        public int clinicID { get; set; }

        public string orgName { get; set; }

        public string skillIn { get; set; }

        public string docInfo { get; set; }

        public int[] rating { get; set; }

        public int comments { get; set; }

        public int consultations { get; set; }

        public int prizes { get; set; }

        public int messages { get; set; }

        public string time { get; set; }//登陆者时间
        /// <summary>
        /// 用户评价数量
        /// </summary>
        public int consultNum { get; set; }

        /// <summary>
        /// 心意数量
        /// </summary>
        public int prizeNum { get; set; }
        public string messages_title { get; set; }//标题
        public string d_message { get; set; }
        public string u_message { get; set; }

        /// <summary>
        /// 是否关注
        /// </summary>
        public bool isFocus { get; set; }

        /// <summary>
        /// 当前用户是否已经登录
        /// </summary>
        public bool isLogin { get; set; }

        /// <summary>
        /// 当前用户线上线下
        /// </summary>
        public string isRemote { get; set; }

        /// <summary>
        /// 当前登陆人姓名
        /// </summary>
        public string userName { get; set; }

        public string stampUrl { get; set; }

        /// <summary>
        /// 当前登录人的在线咨询
        /// </summary>
        public OnlineConsultModel currentLatestConsult { get; set; }

        /// <summary>
        /// 在线咨询列表
        /// </summary>
        public List<OnlineConsultModel> listConsult { get; set; }


        /// <summary>
        /// 用户评论
        /// </summary>
        public List<UserCommentModel> listUserComments { get; set; }


        /// <summary>
        /// 用户心意
        /// </summary>

        public List<PrizeModel> listPrizeModel { get; set; }


        /// <summary>
        /// 当前日期的一周开始时间
        /// </summary>
        public DateTime WeekStartDate { get; set; }

        /// <summary>
        /// 当前登录人
        /// </summary>
        public UserModel UserModel { get; set; }

        public OrderModel orderModel { get; set; }

        public OnlineConsultModel OnlineConsultModel { get; set; }

        /// <summary>
        /// 支持点诊 1是 0否
        /// </summary>
        public int pointDiagnosis { get; set; }

        public DataTable newsList { get; set; }

        public string affiliatedHospital { get; set; }
        public string depName2 { get; set; }
        public string Remark { get; set; }

    }
}
