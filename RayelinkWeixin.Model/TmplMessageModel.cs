﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Model
{
    public class TmplMessageModel
    {
        public string username { get; set; }
        public string docName { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string id { get; set; }
        public string address { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string target { get; set; }
    }
}
