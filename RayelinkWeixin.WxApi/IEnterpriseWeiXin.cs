﻿
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP.Containers;
using System;

namespace RayelinkWeixin.WxApi
{
    /// <summary>
    /// 
    /// </summary>
    public class IEnterpriseWeiXin
    {
        /// <summary>
        /// 公众账号ID:微信支付分配的公众账号ID（企业号corpid即为此appId）
        /// </summary>
        public string _appId = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string _appSecret = string.Empty;

        /// <summary>
        /// 获取AccessToken
        /// </summary>
        public EnterpriseWeiXinAccessTokenInfo AccessToken
        {
            get
            {
                var _AccessTokenStr = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret, false);
                return new EnterpriseWeiXinAccessTokenInfo { AccessToken = _AccessTokenStr, CreateTime = DateTime.Now };
            }
        }

        /// <summary>
        /// 强制获取最新的AccessToken
        /// </summary>
        public EnterpriseWeiXinAccessTokenInfo NewAccessToken
        {
            get
            {
                var _AccessTokenStr = AccessTokenContainer.TryGetAccessToken(_appId, _appSecret, true);
                return new EnterpriseWeiXinAccessTokenInfo { AccessToken = _AccessTokenStr, CreateTime = DateTime.Now };
            }
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="appid"></param>
        public IEnterpriseWeiXin(string appid, string appSecret)
        {
            _appId = appid;
            _appSecret = appSecret;
        }
    }

    /// <summary>
    /// 微信返回,企业号token
    /// </summary>
    public class EnterpriseWeiXinAccessTokenInfo
    {
        /// <summary>
        /// token
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
