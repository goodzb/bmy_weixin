﻿namespace RayelinkWeixin.WxApi.Model
{
    /// <summary>
    /// 微信支付对象
    /// </summary>
    public class WeixinPayModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string appID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string timeStamp { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string nonceStr { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string package { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string paySign { get; set; }
 
    }
}
