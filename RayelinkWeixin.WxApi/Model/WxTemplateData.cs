﻿using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;

namespace RayelinkWeixin.WxApi.Model
{
    /// <summary>
    /// 发送模板消息对象
    /// </summary>
    public class WxTemplateData
    {
        public TemplateDataItem first { get; set; }
        public TemplateDataItem keyword1 { get; set; }
        public TemplateDataItem keyword2 { get; set; }
        public TemplateDataItem keyword3 { get; set; }
        public TemplateDataItem keyword4 { get; set; }
        public TemplateDataItem keyword5 { get; set; }
        public TemplateDataItem remark { get; set; }
    }
}
