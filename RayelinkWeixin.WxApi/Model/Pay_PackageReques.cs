﻿namespace RayelinkWeixin.WxApi.Model
{
    /// <summary>
    /// 统一下单参数对象
    /// 应用场景：除被扫支付场景以外，商户系统先调用该接口在微信支付服务后台生成预支付交易单，返回正确的预支付交易回话标识后再按扫码、JSAPI、APP等不同场景生成交易串调起支付。
    /// https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_1
    /// </summary>
    public class Pay_PackageReques
    {
        /// <summary>
        /// 公众账号ID:微信支付分配的公众账号ID（企业号corpid即为此appId）
        /// </summary>
        public string appid { set; get; }

        /// <summary>
        /// 商户号:微信支付分配的商户号
        /// </summary>
        public string mch_id { set; get; }

        /// <summary>
        /// 设备号:自定义参数，可以为终端设备号(门店号或收银设备ID)，PC网页或公众号内支付可以传"WEB"
        /// </summary>
        public string device_info { set; get; }

        /// <summary>
        /// 随机字符串：随机字符串，长度要求在32位以内
        /// </summary>
        public string nonce_str { set; get; }

        /// <summary>
        /// 签名：通过签名算法计算得出的签名值
        /// </summary>
        public string sign { set; get; }

        /// <summary>
        /// 签名类型 签名类型，默认为MD5，支持HMAC-SHA256和MD5。
        /// </summary> 
        public string sign_type { set; get; }

        /// <summary>
        /// 商品描述 商品简单描述，该字段请按照规范传递
        /// </summary>
        public string body { set; get; }

        /// <summary>
        /// 商品详情	 单品优惠字段(暂未上线)
        /// </summary>
        public string detail { set; get; }

        /// <summary>
        /// 附加数据 附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用。
        /// </summary>
        public string attach { set; get; }

        /// <summary>
        /// 商户订单号	 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
        /// </summary>
        public string out_trade_no { set; get; }

        /// <summary>
        /// 标价币种		否	String(16)	CNY	符合ISO 4217标准的三位字母代码，默认人民币：CNY，详细列表请参见货币类型
        /// </summary>
        public string fee_type { set; get; }

        /// <summary>
        /// 标价金额		是	Int	88	订单总金额，单位为分
        /// </summary>
        public string total_fee { set; get; }

        /// <summary>
        /// 终端IP		是	String(16)	123.12.12.123	APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP
        /// </summary>
        public string spbill_create_ip { set; get; }

        /// <summary>
        /// 交易起始时间		否	String(14)	20091225091010	订单生成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则        
        /// /// </summary>
        public string time_start { set; get; }

        /// <summary>
        /// 交易结束时间		否	String(14)	20091227091010 	        
        /// 订单失效时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010。其他详见时间规则        
        /// 注意：最短失效时间间隔必须大于5分钟
        /// </summary>
        public string time_expire { set; get; }

        /// <summary>
        /// 商品标记		否	String(32)	WXG	商品标记，使用代金券或立减优惠功能时需要的参数，说明详见代金券或立减优惠
        /// </summary>
        public string goods_tag { set; get; }

        /// <summary>
        /// 通知地址	notify_url	是	String(256)	http://www.weixin.qq.com/wxpay/pay.php	异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。
        /// </summary>
        public string notify_url { set; get; }

        /// <summary>
        /// 交易类型		是	String(16)	JSAPI	取值如下：JSAPI，NATIVE，APP等，说明详见参数规定
        /// </summary>
        public string trade_type { set; get; }


        /// <summary>
        /// 商品ID		否	String(32)	12235413214070356458058	trade_type=NATIVE时（即扫码支付），此参数必传。此参数为二维码中包含的商品ID，商户自行定义。
        /// </summary>
        public string product_id { set; get; }


        /// <summary>
        /// 指定支付方式		否	String(32)	no_credit	上传此参数no_credit--可限制用户不能使用信用卡支付
        /// </summary>
        public string limit_pay { set; get; }

        /// <summary>
        /// 用户标识		否	String(128)	oUpF8uMuAJO_M2pxb1Q9zNjWeS6o	trade_type=JSAPI时（即公众号支付），此参数必传，此参数为微信用户在商户对应appid下的唯一标识。openid如何获取，可参考【获取openid】。企业号请使用【企业号OAuth2.0接口】获取企业号内成员userid，再调用【企业号userid转openid接口】进行转换
        /// </summary>
        public string openid { set; get; }
    }
}
