﻿using Senparc.Weixin.Entities;
using Senparc.Weixin.MP.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RayelinkWeixin.WxApi
{
    /// <summary>
    /// 客户接口
    /// 当用户和公众号产生特定动作的交互时（具体动作列表请见下方说明），微信将会把消息数据推送给开发者，开发者可以在一段时间内（目前修改为48小时）调用客服接口，
    /// 通过POST一个JSON数据包来发送消息给普通用户。此接口主要用于客服等有人工消息处理环节的功能，方便开发者为用户提供更加优质的服务。
    /// 目前允许的动作列表如下（公众平台会根据运营情况更新该列表，不同动作触发后，允许的客服接口下发消息条数不同，下发条数达到上限后，会遇到错误返回码，具体请见返回码说明页）：
    /// 1、用户发送信息
    /// 2、点击自定义菜单（仅有点击推事件、扫码推事件、扫码推事件且弹出“消息接收中”提示框这3种菜单类型是会触发客服接口的）
    /// 3、关注公众号
    /// 4、扫描二维码
    /// 5、支付成功
    /// 6、用户维权
    /// </summary>
    public class Custom : IEnterpriseWeiXin
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="appSecret"></param>
        public Custom(string appid, string appSecret) : base(appid, appSecret) { }

        ///// <summary>
        /////  发送文本信息[异步方法]
        ///// </summary>
        ///// <param name="openId">接受者</param>
        ///// <param name="content">发送文本消息内容</param>
        ///// <returns></returns>
        //public async Task<WxJsonResult> SendTextAsync(string openId, string content)
        //{
        //    return await Senparc.Weixin.MP.AdvancedAPIs.CustomApi.SendTextAsync(base.AccessToken.AccessToken, openId, content);
        //}

        /// <summary>
        ///  发送文本信息
        /// </summary>
        /// <param name="openId">接受者</param>
        /// <param name="content">发送文本消息内容</param>
        /// <returns></returns>
        public WxJsonResult SendText(string openId, string content)
        {
            try
            {
                return Senparc.Weixin.MP.AdvancedAPIs.CustomApi.SendText(AccessToken.AccessToken, openId, content);
            }
            catch (System.Exception ex)
            {
                if (ex.Message.Contains("access_token is invalid or not latest hint"))
                {
                    return Senparc.Weixin.MP.AdvancedAPIs.CustomApi.SendText(NewAccessToken.AccessToken, openId, content);
                }
                throw;
            }
        }

        /// <summary>
        /// 发送图片消息
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public WxJsonResult SendImage(string openId, string mediaId)
        {
            return Senparc.Weixin.MP.AdvancedAPIs.CustomApi.SendImage(AccessToken.AccessToken, openId, mediaId);
        }

        ///// <summary>
        ///// 发送图片消息[异步方法]
        ///// </summary>
        ///// <param name="openId"></param>
        ///// <param name="mediaId"></param>
        ///// <returns></returns>
        //public async Task<WxJsonResult> SendImageAsync(string openId, string mediaId)
        //{
        //    return await Senparc.Weixin.MP.AdvancedAPIs.CustomApi.SendImageAsync(AccessToken.AccessToken, openId, mediaId);
        //}

        /// <summary>
        /// 发送图文消息
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="articles">图文消息</param>
        /// <returns></returns>
        public WxJsonResult SendNews(string openId, List<Article> articles)
        {
            return Senparc.Weixin.MP.AdvancedAPIs.CustomApi.SendNews(AccessToken.AccessToken, openId, articles);
        }


    }
}
