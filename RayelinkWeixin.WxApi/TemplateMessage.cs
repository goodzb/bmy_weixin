﻿using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using System;
using System.Threading.Tasks;

namespace RayelinkWeixin.WxApi
{
    /// <summary>
    /// 模板消息接口
    /// 模板消息仅用于公众号向用户发送重要的服务通知，只能用于符合其要求的服务场景中，如信用卡刷卡通知，商品购买成功通知等。不支持广告等营销类消息以及其它所有可能对用户造成骚扰的消息。
    /// 关于使用规则，请注意：
    /// 1、所有服务号都可以在功能->添加功能插件处看到申请模板消息功能的入口，但只有认证后的服务号才可以申请模板消息的使用权限并获得该权限；
    /// 2、需要选择公众账号服务所处的2个行业，每月可更改1次所选行业；
    /// 3、在所选择行业的模板库中选用已有的模板进行调用；
    /// 4、每个账号可以同时使用25个模板。
    /// 5、当前每个账号的模板消息的日调用上限为10万次，单个模板没有特殊限制。【2014年11月18日将接口调用频率从默认的日1万次提升为日10万次，可在MP登录后的开发者中心查看】。当账号粉丝数超过10W/100W/1000W时，模板消息的日调用上限会相应提升，以公众号MP后台开发者中心页面中标明的数字为准。
    /// 关于接口文档，请注意：
    /// 1、模板消息调用时主要需要模板ID和模板中各参数的赋值内容；
    /// 2、模板中参数内容必须以".DATA"结尾，否则视为保留字；
    /// 3、模板保留符号"{{ }}"。
    /// </summary>
    public class TemplateMessage : IEnterpriseWeiXin
    {
        public TemplateMessage(string appid, string appSecret) : base(appid, appSecret) { }

        /// <summary>
        /// 发送模板消息
        /// </summary>
        /// <param name="openId">消息接受者</param>
        /// <param name="templateId">模板消息ID</param>
        /// <param name="data">消息内容</param>
        /// <param name="topColor">topColor</param>
        /// <param name="url">跳转URL</param>
        /// <param name="timeOut">代理请求超时时间(毫秒)</param>
        public SendTemplateMessageResult SendTemplateMessage(string openId, string templateId, object data, string topColor= "#FF0000", string url = "", int timeOut = 10000)
        {
            try
            {
                return Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(base.AccessToken.AccessToken, openId, templateId, url, data,null, timeOut);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("access_token is invalid or not latest hint"))
                {
                    return Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(base.AccessToken.AccessToken, openId, templateId, url, data, null,timeOut);
                }
                throw;
            }           
        }

        ///// <summary>
        ///// 发送模板消息
        ///// </summary>
        ///// <param name="openId">消息接受者</param>
        ///// <param name="templateId">模板消息ID</param>
        ///// <param name="data">消息内容</param>
        /////  /// <param name="url">跳转URL</param>
        ///// <param name="timeOut">代理请求超时时间(毫秒)</param>
        ///// <returns></returns>
        //public async Task<SendTemplateMessageResult> SendTemplateMessageAsync(string openId, string templateId, Object data, string url = "", int timeOut = 10000)
        //{
        //    return await Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessageAsync(base.AccessToken.AccessToken, openId, templateId, url, data, null, timeOut);
        //}
    }
}
