﻿using System.Collections.Generic;
using Senparc.Weixin.MP.TenPayLibV3;
using RayelinkWeixin.WxApi.Model;
using Senparc.Weixin.MP.AdvancedAPIs;
using System.Xml.Linq;
using Senparc.Weixin.MP;
using RayelinkWeixin.Common.Utilites;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Net.Security;
using System.Text;
using System.IO;
using System;

namespace RayelinkWeixin.WxApi
{
    /// <summary>
    /// 微信支付
    /// </summary>
    public class Pay : IEnterpriseWeiXin
    {
        #region 变量
        /// <summary>
        /// 
        /// </summary>
        private string mch_id = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        private string tenPayV3_Key = string.Empty;
        /// <summary>
        /// 退款接口地址
        /// </summary>
        private const string refundRefundUrl = "https://api.mch.weixin.qq.com/secapi/pay/refund";

        /// <summary>
        /// 查询退款 接口地址
        /// </summary>
        private const string refundqueryUrl = "https://api.mch.weixin.qq.com/pay/refundquery";
        #endregion

        #region 构造
        /// <summary>
        /// 公众账号ID:微信支付分配的公众账号ID（企业号corpid即为此appId）
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="appSecret"></param>
        /// <param name="_mch_id">微信支付分配的商户号</param>
        /// <param name="_tenPayV3_Key">商户支付密钥</param>
        public Pay(string appid, string appSecret, string _mch_id, string _tenPayV3_Key) : base(appid, appSecret)
        {
            mch_id = _mch_id;
            tenPayV3_Key = _tenPayV3_Key;
        }

        #endregion

        #region 网页端调起支付API
        /// <summary>
        /// 网页端调起支付API
        /// </summary>
        /// <param name="code">网页授权获取到的Code，主要用于授权去获取用户的openid:code作为换取access_token的票据，每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。</param>
        /// <param name="parameter">参数列表，具体参数key请参照RayelinkWeixin.WxApi.Model.Pay_PackageReques的定义</param>
        /// <returns></returns>
        public WeixinPayModel WeChatPay(string code, Dictionary<string, string> parameter)
        {
            var openIdResult = OAuthApi.GetAccessToken(_appId, _appSecret, code);
            return GetPayJSSDK(mch_id, tenPayV3_Key, parameter, openIdResult.openid);
        }

        /// <summary>
        /// 获取微信支付对象
        /// </summary>
        /// <param name="mch_id">微信支付分配的商户号</param>
        /// <param name="tenPayV3_Key">商户支付密钥</param>
        /// <param name="parameter">参数列表，具体参数key请参照RayelinkWeixin.WxApi.Model.Pay_PackageReques的定义</param>
        /// <param name="openid">openid</param>
        /// <returns></returns>
        private WeixinPayModel GetPayJSSDK(string mch_id, string tenPayV3_Key, Dictionary<string, string> parameter, string openid)
        {
            //创建支付应答对象
            RequestHandler packageReqHandler = new RequestHandler(null);
            //初始化
            packageReqHandler.Init();

            string timeStamp = TenPayV3Util.GetTimestamp();
            string nonceStr = TenPayV3Util.GetNoncestr();

            //设置package订单参数
            packageReqHandler.SetParameter("appid", _appId);		  //公众账号ID
            packageReqHandler.SetParameter("mch_id", mch_id);		  //商户号
            packageReqHandler.SetParameter("nonce_str", nonceStr);    //随机字符串
            packageReqHandler.SetParameter("trade_type", TenPayV3Type.JSAPI.ToString());//交易类型

            foreach (var key in parameter.Keys)
            {
                packageReqHandler.SetParameter(key, parameter[key]);        //设置参数
            }

            #region MyRegion
            //packageReqHandler.SetParameter("body", "送心意");                  //商品信息
            //packageReqHandler.SetParameter("out_trade_no", orderNo);		    //商家订单号
            //packageReqHandler.SetParameter("total_fee", fee.ToString());			         //商品金额,以分为单位(money * 100).ToString()
            //packageReqHandler.SetParameter("spbill_create_ip", Request.UserHostAddress);     //用户的公网ip，不是商户服务器IP
            //packageReqHandler.SetParameter("notify_url", WeixinConfig.TenPayV3_TenpayNotify); //接收财付通通知的URL
            //packageReqHandler.SetParameter("trade_type", TenPayV3Type.JSAPI.ToString());	                    //交易类型 
            #endregion

            packageReqHandler.SetParameter("openid", openid);	                    //用户的openId
            string sign = packageReqHandler.CreateMd5Sign("key", tenPayV3_Key);
            packageReqHandler.SetParameter("sign", sign);                       //签名

            string data = packageReqHandler.ParseXML();
            var result = TenPayV3.Unifiedorder(data);

            var res = XDocument.Parse(result);
            LogHelper.Debug("GetPayJSSDK:" + res.ToString());
            var return_code = res.Element("xml").Element("return_code").Value;
            LogHelper.Debug("GetPayJSSDK:" + return_code);
            if ("SUCCESS" != return_code)
            {
                throw new System.Exception(res.Element("xml").Element("return_msg").Value);
            }

            string prepayId = res.Element("xml").Element("prepay_id").Value;

            //设置支付参数
            RequestHandler paySignReqHandler = new RequestHandler(null);
            paySignReqHandler.SetParameter("appId", _appId);
            paySignReqHandler.SetParameter("timeStamp", timeStamp);
            paySignReqHandler.SetParameter("nonceStr", nonceStr);
            paySignReqHandler.SetParameter("package", string.Format("prepay_id={0}", prepayId));
            paySignReqHandler.SetParameter("signType", "MD5");
            var paySign = paySignReqHandler.CreateMd5Sign("key", tenPayV3_Key);
            return new WeixinPayModel() { appID = _appId, timeStamp = timeStamp, nonceStr = nonceStr, package = string.Format("prepay_id={0}", prepayId), paySign = paySign };
        }

        /// <summary>
        /// 根据openid获取微信支付对象
        /// </summary>
        /// <param name="openid">openid</param>
        /// <param name="parameter">参数列表，具体参数key请参照RayelinkWeixin.WxApi.Model.Pay_PackageReques的定义</param>
        /// <returns></returns>
        public WeixinPayModel WeChatPayByOpenId(string openid, Dictionary<string, string> parameter)
        {
            return GetPayJSSDK(mch_id, tenPayV3_Key, parameter, openid);
        }
        #endregion

        #region 退款申请

        /// <summary>
        /// 退款申请
        /// </summary>
        /// <param name="mch_id">微信支付分配的商户号</param>
        /// <param name="tenPayV3_Key">商户支付密钥</param>
        /// <param name="pay_CertPath">微信支付密钥本地物理路径</param>
        /// <param name="certPassword">微信支付密钥密码</param>
        /// <param name="parameter">退款申请参数</param>
        /// <returns>1：false：失败 true：成功 2：退款申请返回的原XML 3：result_code 4：out_refund_no 5：refund_id</returns>
        public Tuple<bool, string, string, string, string> Refund(string pay_CertPath, string certPassword, Dictionary<string, string> parameter)
        {
            LogHelper.Debug("-----------------------------------------退款申请---------------------------------------------------");
            string nonceStr = TenPayV3Util.GetNoncestr();
            RequestHandler packageReqHandler = new RequestHandler(null);

            //设置package订单参数
            packageReqHandler.SetParameter("appid", _appId);		  //公众账号ID
            packageReqHandler.SetParameter("mch_id", mch_id);         //商户号
            packageReqHandler.SetParameter("nonce_str", nonceStr);              //随机字符串
            foreach (var key in parameter.Keys)
            {
                packageReqHandler.SetParameter(key, parameter[key]);        //设置参数
            }
            LogHelper.Debug("Refund:签名前：cert：" + pay_CertPath + " certPassword:" + certPassword + " Parameters:" + JsonHelper.ObjectToJSON(packageReqHandler.ParseXML()));
            string sign = packageReqHandler.CreateMd5Sign("key", tenPayV3_Key);
            packageReqHandler.SetParameter("sign", sign);     //签名

            LogHelper.Debug("Refund:签名后：sign" + sign);

            //退款需要post的数据
            string data = packageReqHandler.ParseXML();
            LogHelper.Debug("Refund:请求参数：data：" + Environment.NewLine + data);

            //本地或者服务器的证书位置（证书在微信支付申请成功发来的通知邮件中）
            string cert = pay_CertPath;
            //私钥（在安装证书时设置）
            string password = certPassword;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            //调用证书
            X509Certificate2 cer = new X509Certificate2(cert, password, X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.MachineKeySet);

            #region 发起post请求
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(refundRefundUrl);
            webrequest.ClientCertificates.Add(cer);
            webrequest.Method = "post";

            byte[] postdatabyte = Encoding.UTF8.GetBytes(data);
            webrequest.ContentLength = postdatabyte.Length;
            Stream stream;
            stream = webrequest.GetRequestStream();
            stream.Write(postdatabyte, 0, postdatabyte.Length);
            stream.Close();

            HttpWebResponse httpWebResponse = (HttpWebResponse)webrequest.GetResponse();
            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
            string responseContent = streamReader.ReadToEnd();
            #endregion

            LogHelper.Debug("Refund:返回值：responseContent：" + responseContent);

            var res = XDocument.Parse(responseContent);
            string result_code = res.Element("xml").Element("result_code").Value;       //调用退款申请接口的状态码 SUCCESS/FAIL
            string out_refund_no = "";
            string refund_id = "";
            if ("SUCCESS".Equals(result_code))
            {
                //return_code为SUCCESS的时候有以下返回
                result_code = res.Element("xml").Element("result_code").Value;//提交结果，SUCCESS退款申请接收成功，FAIL 提交业务失败
                out_refund_no = res.Element("xml").Element("out_refund_no").Value;//商户退款单号
                refund_id = res.Element("xml").Element("refund_id").Value;//微信退款单号

                //可根据实际业务，做业务数据处理
                LogHelper.Info("微信退款申请---result_code:" + result_code);
                return Tuple.Create(true, responseContent, result_code, out_refund_no, refund_id);

            }
            return Tuple.Create(false, responseContent, string.Empty, string.Empty, string.Empty);
        }
        #endregion

        #region 查询退款
        /// <summary>
        /// 查询退款
        /// 提交退款申请后，通过调用该接口查询退款状态。退款有一定延时，用零钱支付的退款20分钟内到账，银行卡支付的退款3个工作日后重新查询退款状态。
        /// 微信订单号	transaction_id	四选一	String(32)	1217752501201407033233368018	微信订单号
        /// 商户订单号 out_trade_no    String(32)  1217752501201407033233368018	商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
        /// 商户退款单号 out_refund_no   String(32)  1217752501201407033233368018	商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。
        /// 微信退款单号 refund_id
        /// </summary>
        /// <param name="mch_id">微信支付分配的商户号</param>
        /// <param name="tenPayV3_Key">商户支付密钥</param>
        /// <param name="parameter">四选一(transaction_id/out_trade_no/out_refund_no/refund_id)</param>
        /// <returns>1：false：失败 true：成功 2：退款申请返回的原XML 3：refund_id 4：refund_channel 5：transaction_id</returns>
        public Tuple<bool, string, string, string, string> PayRefundquery(Dictionary<string, string> parameter)
        {
            string return_code = string.Empty;
            string refund_id = string.Empty;
            string refund_channel = string.Empty;
            string transaction_id = string.Empty;
            var res = RefundQuery(parameter);
            if (null != res)
            {
                return_code = res.GetValue("return_code").ToString();

                if ("SUCCESS".Equals(return_code))
                {
                    refund_id = res.GetValue("refund_id_0").ToString();  //微信退款单号
                    refund_channel = res.GetValue("refund_channel_0").ToString();//ORIGINAL  —原路退款 BALANCE—退回到余额   OTHER_BALANCE—原账户异常退到其他余额账户 OTHER_BANKCARD—原银行卡异常退到其他银行卡
                    transaction_id = res.GetValue("transaction_id").ToString();//微信订单号
                    return Tuple.Create(true, res.ToXml(), refund_id, refund_channel, transaction_id);
                }
            }
            return Tuple.Create(false, res.ToXml(), string.Empty, string.Empty, string.Empty);
        }
        #endregion

        #region 查询退款

        /**
         ** 
         ** 查询退款
         ** 提交退款申请后，通过该接口查询退款状态。退款有一定延时，
         ** 用零钱支付的退款20分钟内到账，银行卡支付的退款3个工作日后重新查询退款状态。
         ** out_refund_no、out_trade_no、transaction_id、refund_id四个参数必填一个
         ** @param WxPayData inputObj 提交给查询退款API的参数
         ** @param int timeOut 接口超时时间
         ** @throws WxPayException
         ** @return 成功时返回，其他抛异常
         **/
        public WxPayData RefundQuery(Dictionary<string, string> parameter)
        {
            string nonceStr = TenPayV3Util.GetNoncestr();
            WxPayData inputObj = new WxPayData();

            foreach (var key in parameter.Keys)
            {
                inputObj.SetValue(key, parameter[key]);//微信退款单号，优先级最高
            }

            //检测必填参数
            if (!inputObj.IsSet("out_refund_no") && !inputObj.IsSet("out_trade_no") && !inputObj.IsSet("transaction_id") && !inputObj.IsSet("refund_id"))
            {
                throw new Exception("退款查询接口中，out_refund_no、out_trade_no、transaction_id、refund_id四个参数必填一个！");
            }

            inputObj.SetValue("appid", _appId);//公众账号ID
            inputObj.SetValue("mch_id", mch_id);//商户号
            inputObj.SetValue("nonce_str", nonceStr);//随机字符串
            inputObj.SetValue("sign", inputObj.MakeSign(tenPayV3_Key));//签名

            string xml = inputObj.ToXml();

            var start = DateTime.Now;//请求开始时间

            LogHelper.Debug("RefundQuery request : " + xml);
            string response = HttpClientHelper.WebClientPost(refundqueryUrl, xml);

            LogHelper.Debug("RefundQuery response : " + response);

            var end = DateTime.Now;
            int timeCost = (int)((end - start).TotalMilliseconds);//获得接口耗时

            //将xml格式的结果转换为对象以返回
            WxPayData result = new WxPayData();
            result.FromXml(tenPayV3_Key, response);
            return result;
        }
        #endregion

        #region 在安装证书时设置
        /// <summary>
        /// 在安装证书时设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            if (errors == SslPolicyErrors.None)
                return true;
            return false;
        }
        #endregion
    }
}
