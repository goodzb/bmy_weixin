﻿using System;
using System.IO;
namespace RayelinkWeixin.WxApi
{
    /// <summary>
    /// 二维码处理
    /// </summary>
    public class QrCode : IEnterpriseWeiXin
    {
        public QrCode(string appid, string appSecret) : base(appid, appSecret) { }

        #region 获取二维码mp链接
        /// <summary>
        ///  获取二维码mp链接
        /// </summary>
        /// <param name="scene_id">场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）</param>
        /// <param name="scene_str">场景值ID（字符串形式的ID），字符串类型，长度限制为1到64，仅永久二维码支持此字段  </param>
        /// <returns></returns>
        public string ShowQrcodeUrl(string scene_id, string scene_str = "")
        {
            var accessToken = base.AccessToken.AccessToken;
            Common.Utilites.LogHelper.Info("accessToken=" + accessToken);
            string Ticket = Common.Utilites.WeixinQcodeHelper.CreateTicket(accessToken, scene_id);

            if (Ticket == null)
                throw new System.ArgumentNullException("Ticket不能为空！");

            //ticket需 urlEncode
            string strUrl = string.Format("https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}", System.Web.HttpUtility.UrlEncode(Ticket));
            return strUrl;
        } 
        #endregion

        #region 根据SceneId 获取 二维码图片流
        /// <summary>
        /// 根据SceneId 获取 二维码图片流
        /// </summary>
        /// <param name="scene_id"></param>
        /// <returns></returns>
        public System.IO.Stream GetQrCodeImageStream(string scene_id)
        {
            string strUrl = ShowQrcodeUrl(scene_id);
            Common.Utilites.LogHelper.Info("url=" + strUrl);
            try
            {
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                System.Byte[] bytes = client.GetByteArrayAsync(strUrl).Result;
                return new System.IO.MemoryStream(bytes);
            }
            catch
            {
                throw new Exception("获取二维码图片失败！");
            }
        } 
        #endregion

        #region 根据Ticket获取二维码图片保存在本地

        /// <summary>
        /// 根据Ticket获取二维码图片保存在本地
        /// </summary>
        /// <param name="scene_id">二维码场景id</param>
        /// <param name="imgPath">图片存储路径</param>
        public void SaveQrCodeImage(string scene_id, string imgPath)
        {
            string strUrl = ShowQrcodeUrl(scene_id);

            System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(strUrl);

            req.Method = "GET";

            using (System.Net.WebResponse wr = req.GetResponse())
            {
                System.Net.HttpWebResponse myResponse = (System.Net.HttpWebResponse)req.GetResponse();
                string strpath = myResponse.ResponseUri.ToString();

                System.Net.WebClient mywebclient = new System.Net.WebClient();

                try
                {
                    string path = imgPath.Substring(0, imgPath.LastIndexOf("\\"));

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    mywebclient.DownloadFile(strpath, imgPath);
                }
                catch (System.Exception)
                {
                    throw;
                }
            }
        } 
        #endregion

    }
}
