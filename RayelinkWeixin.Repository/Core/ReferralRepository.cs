﻿using RayelinkWeixin.Repository.Base;
using RayelinkWeixin.Repository.Entity;
using RayelinkWeixin.Repository.Entity.Extend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Repository.Core
{
    public class ReferralRepository:BaseRepository<Referral>
    {
        public List<int> GetUnreadCount(int userId)
        {
            List<ReferralExtend> list= dbContext.Database.SqlQuery<ReferralExtend>("SELECT [Status],COUNT(0) AS Total FROM dbo.Referral WHERE IsDel = 0 AND CreaterId = 14 GROUP BY[Status]").ToList();
            List<int> statusArr = new List<int>();
            statusArr.Add(list.Find(r=>r.Status.Contains("10"))==null?0: list.Find(r => r.Status.Contains("10")).Total);
            statusArr.Add(list.Find(r => r.Status == "101") == null ? 0 : list.Find(r => r.Status == "101").Total);
            statusArr.Add(list.Find(r => r.Status == "102") == null ? 0 : list.Find(r => r.Status == "102").Total);
            statusArr.Add(list.Find(r => r.Status == "103") == null ? 0 : list.Find(r => r.Status == "103").Total);
            statusArr.Add(list.Find(r => r.Status.Contains("11")) == null ? 0 : list.Find(r => r.Status.Contains("11")).Total);
            statusArr.Add(list.Find(r => r.Status.Contains("12")) == null ? 0 : list.Find(r => r.Status.Contains("12")).Total);
            statusArr.Add(list.Find(r => r.Status == "121") == null ? 0 : list.Find(r => r.Status == "121").Total);
            statusArr.Add(list.Find(r => r.Status == "122") == null ? 0 : list.Find(r => r.Status == "122").Total);
            
            return statusArr;
        }
    }
}
