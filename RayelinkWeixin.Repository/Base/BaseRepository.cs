﻿using RayelinkWeixin.Repository.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Repository.Base
{
    public class BaseRepository<TEntity> where TEntity : class, new()
    {
        protected ReferralEntities dbContext { get; set; }

        protected IDbSet<TEntity> dbSet { get; set; }

        protected virtual IQueryable<TEntity> Entities
        {
            get { return dbContext.Set<TEntity>(); }
        }

        public BaseRepository()
        {
            this.dbContext = DbContextFactory.GetContext();
            this.dbSet = dbContext.Set<TEntity>();
        }

        #region 1.0 新增实体，返回受影响的行数 +  int Add(T model)
        /// <summary>
        /// 1.0 新增实体，返回受影响的行数
        /// </summary>
        /// <param name="model"></param>
        /// <returns>返回受影响的行数</returns>
        public int Add(TEntity model)
        {
            try
            {
                this.ConvertBaseProperties(model, true);
                this.dbSet.Add(model);
                //保存成功后，会将自增的id设置给model的主键属性，并返回受影响的行数。
                return this.dbContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var message = string.Empty;
                foreach (var item in e.EntityValidationErrors)
                {
                    foreach (var validError in item.ValidationErrors)
                    {
                        message += validError.ErrorMessage + ",";
                    }
                }
                throw new Exception(message);
            }

        }
        #endregion

        #region 1.1 新增实体，返回对应的实体对象 + T AddReturnModel(T model)
        /// <summary>
        /// 1.1 新增实体，返回对应的实体对象
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public TEntity AddReturnModel(TEntity model)
        {
            try
            {
                this.ConvertBaseProperties(model, true);
                this.dbSet.Add(model);
                this.dbContext.SaveChanges();
                return model;
            }
            catch (DbEntityValidationException e)
            {
                var message = string.Empty;
                foreach (var item in e.EntityValidationErrors)
                {
                    foreach (var validError in item.ValidationErrors)
                    {
                        message += validError.ErrorMessage + ",";
                    }
                }
                throw new Exception(message);
            }
        }
        #endregion

        #region 2.0 根据id删除 +  int Del(T model)
        /// <summary>
        /// 2.0 根据id删除
        /// </summary>
        /// <param name="model">必须包含要删除id的对象</param>
        /// <returns></returns>
        public int Del(TEntity model)
        {
            this.dbSet.Attach(model);
            this.dbSet.Remove(model);
            return this.dbContext.SaveChanges();
        }
        #endregion

        #region 2.1 根据条件删除 + int DelBy(Expression<Func<T, bool>> delWhere)
        /// <summary>
        /// 2.1 根据条件删除
        /// </summary>
        /// <param name="delWhere"></param>
        /// <returns>返回受影响的行数</returns>
        public int DelBy(Expression<Func<TEntity, bool>> delWhere)
        {
            //2.1.1 查询要删除的数据
            List<TEntity> listDeleting = this.dbSet.Where(delWhere).ToList();
            //2.1.2 将要删除的数据 用删除方法添加到 EF 容器中
            listDeleting.ForEach(u =>
            {
                this.dbSet.Attach(u);  //先附加到EF 容器
                this.dbSet.Remove(u); //标识为删除状态
            });
            //2.1.3 一次性生成sql语句 到数据库执行删除
            return this.dbContext.SaveChanges();
        }
        #endregion

        #region 3.0 修改实体 +  int Modify(T model)
        /// <summary>
        /// 修改实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Modify(TEntity model)
        {
            this.ConvertBaseProperties(model, false);
            var entry = this.dbContext.Entry<TEntity>(model);
            entry.State = EntityState.Modified;
            return this.dbContext.SaveChanges();
        }
        #endregion

        #region 3.1 修改实体，可修改指定属性 + int Modify(T model, params string[] propertyNames)
        /// <summary>
        /// 3.1 修改实体，可修改指定属性
        /// </summary>
        /// <param name="model"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public int Modify(TEntity model, params string[] propertyNames)
        {
            this.ConvertBaseProperties(model, false);
            //3.1.1 将对象添加到EF中
            var entry = this.dbContext.Entry<TEntity>(model);
            //3.1.2 先设置对象的包装状态为 Unchanged
            entry.State = EntityState.Unchanged;
            //3.1.3 循环被修改的属性名数组
            foreach (string propertyName in propertyNames)
            {
                //将每个被修改的属性的状态设置为已修改状态；这样在后面生成的修改语句时，就只为标识为已修改的属性更新
                entry.Property(propertyName).IsModified = true;
            }
            Type t = typeof(TEntity);
            List<PropertyInfo> propertyInfos = t.GetProperties(BindingFlags.Instance | BindingFlags.Public).ToList();
            propertyInfos.ForEach(p =>
            {
                if (p.Name == "UpdateUser")
                {
                    entry.Property("UpdateUser").IsModified = true;
                }
                else if (p.Name == "UpdateDate")
                {
                    entry.Property("UpdateDate").IsModified = true;
                }
            });
            return this.dbContext.SaveChanges();
        }
        #endregion

        #region 3.2 批量修改 + int ModifyBy(T model, Expression<Func<T, bool>> whereLambda, params string[] modifiedPropertyNames)
        /// <summary>
        /// 3.2 批量修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="whereLambda"></param>
        /// <param name="modifiedPropertyNames"></param>
        /// <returns></returns>
        public int ModifyBy(TEntity model, Expression<Func<TEntity, bool>> whereLambda, params string[] modifiedPropertyNames)
        {
            this.ConvertBaseProperties(model, false);
            //3.2.1 查询要修改的数据
            List<TEntity> listModifing = this.dbSet.Where(whereLambda).ToList();
            //3.2.2 获取实体类类型对象
            Type t = typeof(TEntity);
            //3.2.3 获取实体类所有的公共属性
            List<PropertyInfo> propertyInfos = t.GetProperties(BindingFlags.Instance | BindingFlags.Public).ToList();
            //3.2.4 创建实体属性字典集合
            Dictionary<string, PropertyInfo> dicPropertys = new Dictionary<string, PropertyInfo>();
            //3.2.5 将实体属性中要修改的属性名 添加到字典集合中  键：属性名  值：属性对象
            List<string> list = new List<string>();
            list.AddRange(modifiedPropertyNames);
            list.Add("UpdateUser");
            list.Add("UpdateDate");
            modifiedPropertyNames = list.ToArray();
            propertyInfos.ForEach(p =>
            {
                if (modifiedPropertyNames.Contains(p.Name))
                {
                    dicPropertys.Add(p.Name, p);
                }
            });
            //3.2.6 循环要修改的属性名
            foreach (string propertyName in modifiedPropertyNames)
            {
                //判断要修改的属性名是否在实体类的属性集合中存在
                if (dicPropertys.ContainsKey(propertyName))
                {
                    //如果存在，则取出要修改的属性对象
                    PropertyInfo proInfo = dicPropertys[propertyName];
                    //取出要修改的值
                    object newValue = proInfo.GetValue(model, null);
                    //批量设置要修改对象的属性
                    foreach (TEntity item in listModifing)
                    {
                        //为要修改的对象的要修改的属性设置新的值
                        proInfo.SetValue(item, newValue, null);
                    }
                }
            }
            //一次性生成sql语句 到数据库执行
            return this.dbContext.SaveChanges();
        }
        #endregion

        #region 4.0 根据条件查询单个model + T GetModel(Expression<Func<T, bool>> whereLambda)
        /// <summary>
        /// 4.0 根据条件查询单个model
        /// </summary>
        /// <param name="whereLambda"></param>
        /// <returns></returns>
        public TEntity GetFirstModel(Expression<Func<TEntity, bool>> whereLambda)
        {
            return this.dbSet.Where(whereLambda).AsNoTracking().FirstOrDefault();
        }
        #endregion

        #region 4.1 根据条件查询单个model并排序  +  T GetModel<TKey>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey>> orderLambda, bool isAsc = true)
        /// <summary>
        /// 4.1 根据条件查询单个model并排序
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereLambda"></param>
        /// <param name="orderLambda"></param>
        /// <param name="isAsc"></param>
        /// <returns></returns>
        public TEntity GetFirstModel<TKey>(Expression<Func<TEntity, bool>> whereLambda, Expression<Func<TEntity, TKey>> orderLambda, bool isAsc = true)
        {
            if (isAsc)
            {
                return this.dbSet.Where(whereLambda).OrderBy(orderLambda).AsNoTracking().FirstOrDefault();
            }
            else
            {
                return this.dbSet.Where(whereLambda).OrderByDescending(orderLambda).AsNoTracking().FirstOrDefault();
            }
        }
        #endregion

        #region  5.0 根据条件查询 + List<T> GetListBy(Expression<Func<T, bool>> whereLambda)
        /// <summary>
        /// 5.0 根据条件查询
        /// </summary>
        /// <param name="whereLambda"></param>
        /// <returns></returns>
        public List<TEntity> GetListBy(Expression<Func<TEntity, bool>> whereLambda)
        {
            return this.dbSet.Where(whereLambda).AsNoTracking().ToList();
        }
        #endregion

        #region 5.1 根据条件查询，并排序 +  List<T> GetListBy<TKey>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey>> orderLambda, bool isAsc = true)
        /// <summary>
        /// 5.1 根据条件查询，并排序
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereLambda"></param>
        /// <param name="orderLambda"></param>
        /// <param name="isAsc"></param>
        /// <returns></returns>
        public List<TEntity> GetListBy<TKey>(Expression<Func<TEntity, bool>> whereLambda, Expression<Func<TEntity, TKey>> orderLambda, bool isAsc = true)
        {
            if (isAsc)
            {
                return this.dbSet.Where(whereLambda).OrderBy(orderLambda).AsNoTracking().ToList();
            }
            else
            {
                return this.dbSet.Where(whereLambda).OrderByDescending(orderLambda).AsNoTracking().ToList();
            }
        }
        #endregion

        #region 5.2 根据条件查询Top多少个，并排序 + List<T> GetListBy<TKey>(int top, Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey>> orderLambda, bool isAsc = true)
        /// <summary>
        /// 5.2 根据条件查询Top多少个，并排序
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="top"></param>
        /// <param name="whereLambda"></param>
        /// <param name="orderLambda"></param>
        /// <param name="isAsc"></param>
        /// <returns></returns>
        public List<TEntity> GetListBy<TKey>(int top, Expression<Func<TEntity, bool>> whereLambda, Expression<Func<TEntity, TKey>> orderLambda, bool isAsc = true)
        {
            if (isAsc)
            {
                return this.dbSet.Where(whereLambda).OrderBy(orderLambda).Take(top).AsNoTracking().ToList();
            }
            else
            {
                return this.dbSet.Where(whereLambda).OrderByDescending(orderLambda).Take(top).AsNoTracking().ToList();
            }
        }
        #endregion

        #region  5.3 根据条件排序查询  双排序 + List<T> GetListBy<TKey1, TKey2>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey1>> orderLambda1, Expression<Func<T, TKey2>> orderLambda2, bool isAsc1 = true, bool isAsc2 = true)
        /// <summary>
        /// 5.3 根据条件排序查询  双排序
        /// </summary>
        /// <typeparam name="TKey1"></typeparam>
        /// <typeparam name="TKey2"></typeparam>
        /// <param name="whereLambda"></param>
        /// <param name="orderLambda1"></param>
        /// <param name="orderLambda2"></param>
        /// <param name="isAsc1"></param>
        /// <param name="isAsc2"></param>
        /// <returns></returns>
        public List<TEntity> GetListBy<TKey1, TKey2>(Expression<Func<TEntity, bool>> whereLambda, Expression<Func<TEntity, TKey1>> orderLambda1, Expression<Func<TEntity, TKey2>> orderLambda2, bool isAsc1 = true, bool isAsc2 = true)
        {
            if (isAsc1)
            {
                if (isAsc2)
                {
                    return this.dbSet.Where(whereLambda).OrderBy(orderLambda1).ThenBy(orderLambda2).AsNoTracking().ToList();
                }
                else
                {
                    return this.dbSet.Where(whereLambda).OrderBy(orderLambda1).ThenByDescending(orderLambda2).AsNoTracking().ToList();
                }
            }
            else
            {
                if (isAsc2)
                {
                    return this.dbSet.Where(whereLambda).OrderByDescending(orderLambda1).ThenBy(orderLambda2).AsNoTracking().ToList();
                }
                else
                {
                    return this.dbSet.Where(whereLambda).OrderByDescending(orderLambda1).ThenByDescending(orderLambda2).AsNoTracking().ToList();
                }
            }
        }
        #endregion

        #region 5.3 根据条件排序查询Top个数  双排序 + List<T> GetListBy<TKey1, TKey2>(int top, Expression<Func<T, bool>> whereLambda, System.Linq.Expressions.Expression<Func<T, TKey1>> orderLambda1, Expression<Func<T, TKey2>> orderLambda2, bool isAsc1 = true, bool isAsc2 = true)
        /// <summary>
        ///  5.3 根据条件排序查询Top个数  双排序
        /// </summary>
        /// <typeparam name="TKey1"></typeparam>
        /// <typeparam name="TKey2"></typeparam>
        /// <param name="top"></param>
        /// <param name="whereLambda"></param>
        /// <param name="orderLambda1"></param>
        /// <param name="orderLambda2"></param>
        /// <param name="isAsc1"></param>
        /// <param name="isAsc2"></param>
        /// <returns></returns>
        public List<TEntity> GetListBy<TKey1, TKey2>(int top, Expression<Func<TEntity, bool>> whereLambda, Expression<Func<TEntity, TKey1>> orderLambda1, Expression<Func<TEntity, TKey2>> orderLambda2, bool isAsc1 = true, bool isAsc2 = true)
        {
            if (isAsc1)
            {
                if (isAsc2)
                {
                    return this.dbSet.Where(whereLambda).OrderBy(orderLambda1).ThenBy(orderLambda2).Take(top).AsNoTracking().ToList();
                }
                else
                {
                    return this.dbSet.Where(whereLambda).OrderBy(orderLambda1).ThenByDescending(orderLambda2).Take(top).AsNoTracking().ToList();
                }
            }
            else
            {
                if (isAsc2)
                {
                    return this.dbSet.Where(whereLambda).OrderByDescending(orderLambda1).ThenBy(orderLambda2).Take(top).AsNoTracking().ToList();
                }
                else
                {
                    return this.dbSet.Where(whereLambda).OrderByDescending(orderLambda1).ThenByDescending(orderLambda2).Take(top).AsNoTracking().ToList();
                }
            }
        }
        #endregion

        #region 6.0 分页查询 + List<T> GetPagedList<TKey>
        /// <summary>
        /// 分页查询 + List<T> GetPagedList
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <param name="whereLambda">条件 lambda表达式</param>
        /// <param name="orderBy">排序 lambda表达式</param>
        /// <returns></returns>
        public List<TEntity> GetPagedList<TKey>(int pageIndex, int pageSize, Expression<Func<TEntity, bool>> whereLambda, Expression<Func<TEntity, TKey>> orderByLambda, bool isAsc = true)
        {
            // 分页 一定注意： Skip 之前一定要 OrderBy
            if (isAsc)
            {
                return this.dbSet.Where(whereLambda).OrderBy(orderByLambda).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToList();
            }
            else
            {
                return this.dbSet.Where(whereLambda).OrderByDescending(orderByLambda).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToList();
            }
        }
        #endregion

        #region 6.1分页查询 带输出 +List<T> GetPagedList<TKey>
        /// <summary>
        /// 分页查询 带输出
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="rowCount"></param>
        /// <param name="whereLambda"></param>
        /// <param name="orderBy"></param>
        /// <param name="isAsc"></param>
        /// <returns></returns>
        public List<TEntity> GetPagedList<TKey>(int pageIndex, int pageSize, ref int rowCount, Expression<Func<TEntity, bool>> whereLambda, Expression<Func<TEntity, TKey>> orderByLambda, bool isAsc = true)
        {
            rowCount = this.dbSet.Where(whereLambda).Count();
            if (isAsc)
            {
                return this.dbSet.OrderBy(orderByLambda).Where(whereLambda).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToList();
            }
            else
            {
                return this.dbSet.OrderByDescending(orderByLambda).Where(whereLambda).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToList();
            }
        }
        #endregion

        #region 6.2 分页查询 带输出 并支持双字段排序
        /// <summary>
        /// 分页查询 带输出 并支持双字段排序
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="rowCount"></param>
        /// <param name="whereLambda"></param>
        /// <param name="orderByLambda1"></param>
        /// <param name="orderByLambda2"></param>
        /// <param name="isAsc1"></param>
        /// <param name="isAsc2"></param>
        /// <returns></returns>
        public List<TEntity> GetPagedList<TKey1, TKey2>(int pageIndex, int pageSize, ref int rowCount, Expression<Func<TEntity, bool>> whereLambda, Expression<Func<TEntity, TKey1>> orderByLambda1, Expression<Func<TEntity, TKey2>> orderByLambda2, bool isAsc1 = true, bool isAsc2 = true)
        {
            rowCount = this.dbSet.Where(whereLambda).Count();
            if (isAsc1)
            {
                if (isAsc2)
                {
                    return this.dbSet.OrderBy(orderByLambda1).ThenBy(orderByLambda2).Where(whereLambda).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToList();
                }
                else
                {
                    return this.dbSet.OrderBy(orderByLambda1).ThenByDescending(orderByLambda2).Where(whereLambda).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToList();
                }
            }
            else
            {
                if (isAsc2)
                {
                    return this.dbSet.OrderByDescending(orderByLambda1).ThenBy(orderByLambda2).Where(whereLambda).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToList();
                }
                else
                {
                    return this.dbSet.OrderByDescending(orderByLambda1).ThenByDescending(orderByLambda2).Where(whereLambda).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsNoTracking().ToList();
                }
            }
        }
        #endregion

        #region 6.3 执行sql

        /// <summary>
        /// 执行sql
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int ExecuteCommand(string sqlCommand, params object[] parameters)
        {
            return this.dbContext.Database.ExecuteSqlCommand(sqlCommand, parameters);
        }

        #endregion

        #region 6.3 查询所有的实体

        /// <summary>
        /// 6.3  查询所有的实体
        /// </summary>
        /// <returns></returns>
        public List<TEntity> GetAllList()
        {

            return this.dbSet.AsNoTracking().ToList();
        }

        #endregion

        #region 私有方法

        /// <summary>
        /// 换换基础属性
        /// </summary>
        /// <param name="model"></param>
        /// <param name="isAdd"></param>
        private void ConvertBaseProperties(TEntity model, bool isAdd)
        {
            //if (model is BaseEntity)
            //{
            //    var userCode = "System";
            //    if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[SessionKeyConst.CurrentUser] != null)
            //    {
            //        var currentUser = HttpContext.Current.Session[SessionKeyConst.CurrentUser] as CurrentUser;

            //        if (currentUser.SysUser != null)
            //        {
            //            userCode = currentUser.SysUser.UserAccount;
            //        }
            //    }
            //    if (isAdd)
            //    {
            //        (model as BaseEntity).CreateUser = userCode;
            //        (model as BaseEntity).CreateDate = DateTime.Now;
            //    }
            //    (model as BaseEntity).UpdateUser = userCode;
            //    (model as BaseEntity).UpdateDate = DateTime.Now;
            //}
        }

        #endregion

    }
}
