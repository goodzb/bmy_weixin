﻿using RayelinkWeixin.Repository.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Repository.Base
{
    public class DbContextFactory
    {
        private static string efKey = "ef_key";

        public static ReferralEntities GetContext()
        {
            ReferralEntities db = CallContext.GetData(efKey) as ReferralEntities;
            if (db == null)
            {
                db = new ReferralEntities();
                //System.Diagnostics.Debug.WriteLine("EF上下文获取：" + db.GetHashCode() + ",时间：" + DateTime.Now.ToString("hh-mm-ss ffff"));
                CallContext.SetData(efKey, db);
            }
            return db;
        }

        public static void Dispose()
        {
            var context = GetContext();
            //System.Diagnostics.Debug.WriteLine("EF上下文释放：" + context.GetHashCode() + ",时间：" + DateTime.Now.ToString("hh-mm-ss ffff"));
            context.Dispose();
        }
    }
}
