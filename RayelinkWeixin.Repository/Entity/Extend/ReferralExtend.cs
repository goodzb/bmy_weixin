﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RayelinkWeixin.Repository.Entity.Extend
{
    public class ReferralExtend:Referral
    {
        /// <summary>
        /// 某状态下的总条数，我的转诊显示
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// 当前用户是否已读该单据，我的转诊显示
        /// </summary>
        public int IsRead { get; set; }

        /// <summary>
        /// 意向专家信息，包括姓名和科室，转诊详情显示
        /// </summary>
        public string DocBrif { get; set; }

        /// <summary>
        /// 转诊来源人信息，包括姓名和电话，逗号隔开，转诊详情显示
        /// </summary>
        public string FromBrif { get; set; }



    }
}
