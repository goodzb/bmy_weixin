﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using Senparc.Weixin.MP.Entities;
using System.Collections.Generic;
using System.Linq;

namespace RayelinkWeixin.Business
{
    /// <summary>
    /// 公共方法
    /// </summary>
    public class ShareFunction
    {

        private readonly int AppId;
        private const string appCacheKey = "appCacheKey_{0}";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appid"></param>
        public ShareFunction(int appid)
        {
            AppId = appid;
        }

        public ShareFunction()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public AppInfo App
        {

            get
            {
                return GetAppInfo(AppId);
            }
        }


        /// <summary>
        /// 获取一个app
        /// </summary>
        /// <param name="appid"></param>
        /// <returns></returns>
        private AppInfo GetAppInfo(int appid)
        {
            AppInfo app = null;
            string key = string.Format(appCacheKey, appid);
            var cache = CacheHelper.GetCache(key);
            if (null != cache)
            {
                app = cache as AppInfo;
            }
            else
            {
                app = BusinessContent.App.GetFormDataInfoByIdentityKey(appid);
            }
            return app;
        }

        /// <summary>
        /// 根据appKey查找一个AppInfo
        /// </summary>
        /// <param name="appKey"></param>
        /// <returns></returns>
        public AppInfo GetAppInfo(string appKey)
        {
            AppInfo app = null;
            string key = string.Format(appCacheKey, appKey);
            var cache = CacheHelper.GetCache(key);
            if (null != cache)
            {
                app = cache as AppInfo;
            }
            else
            {
                app = BusinessContent.App.FindOne("AppKey=@0", arg: appKey);
            }
            return app;
        }


        /// <summary>
        /// 获取二维码响应图文对照关系
        /// </summary>
        /// <param name="appid"></param>
        /// <returns></returns>
        public string QrCode_ArticleConfig(int appid)
        {
            return new LangAdapter(appid).TryGet("QrCode_Article");
        }

        /// <summary>
        /// 根据二维码场景ID获取二维码响应图文对照关系
        /// </summary>
        /// <param name="sceneId">二维码场景I</param>
        /// <param name="appid"></param>
        /// <returns></returns>
        public List<Article> QrCode_ArticleConfig(int sceneId, int appid = 1)
        {
            string json = QrCode_ArticleConfig(appid);

            if (!string.IsNullOrWhiteSpace(json))
            {
                var jArray = JArray.Parse(json);
                if (null != jArray)
                {
                    var articleList = jArray.FirstOrDefault(t => t.Value<int>("QrCodeSceneId") == sceneId);
                    if (null != articleList && articleList.Any())
                    {
                        return JsonConvert.DeserializeObject<List<Article>>(articleList.Value<JArray>("ArticleList").ToString());
                    }
                }
            }
            return null;
        }

    }
}
