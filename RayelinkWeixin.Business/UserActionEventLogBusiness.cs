﻿using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RayelinkWeixin.Business
{ /// <summary>
  ///作者：xiao99
  ///日期：2017/04/28 11:43:13
  ///说明：
  ///版本号：  V1.0.0.0
  /// </summary>
    public class UserActionEventLogBusiness
    {
        #region 变量
        private static UserActionEventLogBusiness _mInstance;
        private static readonly object SyncObject = new object();
        #endregion

        #region 单例
        /// <summary>
        /// 单例
        /// </summary>
        public static UserActionEventLogBusiness Instance
        {
            get
            {
                if (_mInstance != null) return _mInstance;
                lock (SyncObject)
                {
                    if (_mInstance == null)
                    {
                        _mInstance = new UserActionEventLogBusiness();
                    }
                }
                return _mInstance;
            }
        }
        #endregion

        #region 初始化实体对象
        /// <summary>
        /// 初始化实体对象
        /// </summary>
        /// <param name="model">(需要填写：AppId，OpenId，ActionType，ActionName，parameter，ActionDesc)</param>
        /// <returns>初始化后的对象</returns>
        private UserActionEventLogInfo InitUserActionEventLog(UserActionEventLogInfo model)
        {
            model.ActionStatus = 1;
            model.CreateDateTime = DateTime.Now;
            model.ActionYear = (short)DateTime.Now.Year;
            model.ActionMonth = (short)DateTime.Now.Month;
            model.ActionDay = (short)DateTime.Now.Day;
            model.ActionHour = (short)DateTime.Now.Hour;
            model.ActionIp = IPHelper.ClientIP();
            model.ActionDate = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));

            var httpContext = HttpContext.Current;
            if (null != httpContext)
            {
                if (httpContext.Request.UrlReferrer != null) model.ReferUrl = httpContext.Request.UrlReferrer.AbsoluteUri;
                model.RequestUrl = httpContext.Request.Url.OriginalString;
            }
            model.ActionSid = Guid.NewGuid();
            model.ModID = (short)(model.AppId % 50);
            model = BusinessContent.UserActionEventLog.CheckData(model);
            return model;
        }
        #endregion

        #region 插入
        /// <summary>
        /// 往数据库插入(先一条一条的插入，后续增加Redis后修改成先插入Redis再批量插入数据库中)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Push(UserActionEventLogInfo model)
        {
            bool result = true;
            try
            {
                model = InitUserActionEventLog(model);  //初始化实体对象

                BusinessContent.UserActionEventLog.Insert(model);
            }
            catch (Exception ex)
            {
                result = false;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return result;
        }
        #endregion

    }
}
