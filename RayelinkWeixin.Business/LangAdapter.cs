﻿using Newtonsoft.Json.Linq;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace RayelinkWeixin.Business
{
    /// <summary>
    /// 配置项
    /// </summary>
    public partial class LangAdapter
    {
        /// <summary>
        /// 
        /// </summary>
        public int AppID { get; set; }

        /// <summary>
        /// 存储配置信息
        /// </summary>
        private static System.Collections.Concurrent.ConcurrentDictionary<int, LangSetting> m_LangDict = new System.Collections.Concurrent.ConcurrentDictionary<int, LangSetting>();


        /// <summary>
        /// 清空配置信息
        /// </summary>
        public static void ClearLangDict(int appid)
        {
            if (m_LangDict.ContainsKey(appid))
            {
                LangSetting setting = null;
                m_LangDict.TryRemove(appid, out setting);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private LangSetting LangDict
        {
            get
            {
                return m_LangDict.GetOrAdd(AppID, (x) =>
                {
                    var list = BusinessContent.AppConfig.Find(string.Format("AppId={0}", this.AppID), 99999);
                    var lang = new LangSetting();
                    if (list != null && list.Any())
                    {
                        foreach (var item in list)
                        {
                            lang.ManuallySetMember(item.Key, item.Value.Replace("{enter}", Environment.NewLine));
                        }
                    }
                    return lang;
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public LangAdapter(int appID)
        {
            AppID = appID;
        }

        /// <summary>
        /// 
        /// </summary>
        public dynamic LangBag
        {
            get
            {
                return this.LangDict;
            }
        }

        /// <summary>
        /// 尝试获取字符串类型的值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string TryGet(string key)
        {
            object result = string.Empty;
            this.LangDict.TryGetMember(key, out result);
            return Convert.ToString(result);
        }

        /// <summary>
        /// 获取JObject类型的值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public JObject TryGetJObject(string key)
        {
            object result = string.Empty;
            try
            {
                this.LangDict.TryGetMember(key, out result);

                if (null != result) return JObject.Parse(result.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
            return null;
        }

        /// <summary>
        /// 获取JArray类型的值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public JArray TryGetJArray(string key)
        {
            object result = string.Empty;
            try
            {
                this.LangDict.TryGetMember(key, out result);

                if (null != result) return JArray.Parse(result.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
            return null;
        }

        /// <summary>
        /// 根据key尝试获取配置信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T TryGet<T>(string key) where T : Type
        {
            T result = default(T);
            try
            {
                object tmp;
                this.LangDict.TryGetMember(key, out tmp);
                result = (T)Convert.ChangeType(tmp, typeof(T));
            }
            catch (Exception ex)
            {
                result = default(T);
                LogHelper.Exception(ex);
            }
            return result;
        }

        /// <summary>
        /// 尝试获取配置信息（如果当前的appid无相关配置，则获取公共配置）
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string TryGetOrDefault(string key)
        {
            object result = string.Empty;
            try
            {
                if (!this.LangDict.TryGetMember(key, out result))
                {
                    LangAdapter lang = new LangAdapter(0);
                    result = lang.TryGet(key);
                }
            }
            catch
            {
                LangAdapter lang = new LangAdapter(0);
                result = lang.TryGet(key);
            }
            return Convert.ToString(result);
        }

        #region 获取配置中的某一项信息
        /// <summary>
        /// 获取配置中的某一项信息(只针对于取JToken中的每一个配置值)
        /// </summary>
        /// <typeparam name="T">值的类型</typeparam>
        /// <param name="name">appConfig的名称</param>
        /// <param name="key">取值的字段</param>
        /// <returns></returns>
        public T GetLangAdapterValue<T>(string name, string key)
        {
            var value = default(T);
            string config = TryGet(name);
            if (!string.IsNullOrWhiteSpace(config))
            {
                JToken scoreData = JToken.Parse(config);
                value = scoreData.Value<T>(key);
            }
            return value;
        }

        /// <summary>
        /// 直接从数据库中获取某一个配置项的信息
        /// </summary>
        /// <param name="key">ConfigKey</param>
        /// <param name="appid"></param>
        /// <returns></returns>
        public string GetConfigKeyValueDb(string key, short appid)
        {
            string result = string.Empty;
            try
            {
                var appConfig = BusinessContent.AppConfig.FindOne(string.Format("Key='{0}' AND AppId={1} ", key, appid), "");
                if (null != appConfig && !string.IsNullOrWhiteSpace(appConfig.Value))
                {
                    result = appConfig.Value;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                throw;
            }
            return result;
        }

        /// <summary>
        /// 从数据库中直接获取Json配置信息中的每一个节点的数据值
        /// </summary>
        /// <typeparam name="T">节点数据值类型</typeparam>
        /// <param name="key">ConfigKey</param>
        /// <param name="jsonKey">取值的Json节点名称</param>
        /// <param name="appid"></param>
        /// <returns></returns>
        public T GetLangAdapterValueDb<T>(string key, string jsonKey, short appid)
        {
            var value = default(T);
            try
            {
                var configValue = GetConfigKeyValueDb(key, appid);
                if (!string.IsNullOrWhiteSpace(configValue))
                {
                    JToken scoreData = JToken.Parse(configValue);
                    value = scoreData.Value<T>(jsonKey);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                throw;
            }
            return value;
        }

        /// <summary>
        /// 设置新值,存在更新,不存在则创建
        /// </summary>
        /// <param name="name"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SettLangAdapterValueDb(string name, string key, string value)
        {
            var appConfig = BusinessContent.AppConfig.FindOne(string.Format("Name='{0}' and Key='{1}' AND AppId={2} ", name, key, AppID), "");
            if (null != appConfig)
            {
                appConfig.Value = value;
                BusinessContent.AppConfig.Execute(string.Format("Update AppConfig set Value='{3}' where Name='{0}' and Key='{1}' AND AppId={2}", name, key, AppID, value), null);
            }
            else
            {
                appConfig = new AppConfigInfo()
                {
                    AppId = AppID,
                    CreateTime = DateTime.Now,
                    Key = key,
                    Name = name,
                    Type = 2,
                    Value = value,
                    Status = 1,
                    Updator = 1,
                    LastUpdateTime = DateTime.Now,
                    Creator = 1,
                    Memo = ""
                };
                BusinessContent.AppConfig.Insert(appConfig);
            }
            ClearLangDict(AppID);
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class LangSetting : DynamicObject
    {
        /// <summary>
        /// 
        /// </summary>
        Dictionary<string, object> dictionary = new Dictionary<string, object>();

        /// <summary>
        /// 
        /// </summary>
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            string name = binder.Name.ToLower();
            if (dictionary.ContainsKey(binder.Name.ToLower()))
            {
                return dictionary.TryGetValue(name, out result);
            }
            else
            {
                result = "";
                return false;
            }
        }

        /// <summary>
        /// 获取属性值
        /// </summary>
        public bool TryGetMember(string name, out object result)
        {
            name = name.ToLower();
            if (dictionary.ContainsKey(name))
            {
                return dictionary.TryGetValue(name, out result);
            }
            else
            {
                result = "";
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            try
            {
                if (dictionary.ContainsKey(binder.Name.ToLower()))
                {
                    dictionary[binder.Name.ToLower()] = value;
                }
                else
                {
                    dictionary.Add(binder.Name.ToLower(), value);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void ManuallySetMember(string key, object value)
        {
            key = key.ToLower();
            if (dictionary.ContainsKey(key))
                dictionary[key] = value;
            else
                dictionary.Add(key, value);
        }

        /// <summary>
        /// 提供索引方式查询参数
        /// </summary>
        /// <param name="binder">绑定信息</param>
        /// <param name="indexes">索引数据</param>
        /// <param name="result">返回结果</param>
        /// <returns>执行是否成功</returns>
        public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
        {
            if (indexes.Length != 1) throw new NotSupportedException();
            result = null;
            string propertyName = indexes[0].ToString();
            if (dictionary.ContainsKey(propertyName.ToLower()))
            {
                result = dictionary[propertyName.ToLower()];
            }
            return true;
        }

    }
}
