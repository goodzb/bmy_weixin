﻿using RayelinkWeixin.Common.Share;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using System;
using System.Linq;

namespace RayelinkWeixin.Business
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ReVisitBusiness
    {
        #region 变量
        private const string TimeOutOrders_sql = "UPDATE  FZ_Orders set Status=@0,CloseTime=@1,CloseReason='超时自动完成',ConversationStatus=@2 where Status = @3 and ConversationStatus =@4 and EndTime <=@5 ";
        private const string InvalidTimeOutOrders_sql = "UPDATE  FZ_Orders set Status=@0,ConversationStatus=@1,CloseTime=@2,CloseReason='超时自动取消',CancelTime=@3 where Status = @4 and ConversationStatus = @5 and EndTime <=@3";

        private const string TimeOutOrders_Msg = "您好，本次咨询已结束（已满24小时），如需再次咨询，请重新购买本服务。";
        public string InvalidTimeOutOrders_Msg = "您的图文复诊咨询订单已取消。 如无异常，5 个工作日内会全额退款到您的支付账户，请稍作等待。";
        private string FlushOrderWebSite = "ReVisit/ReVisit/FlushOrder?orderSid={0}";
        private string FlushDortorWebSite = "ReVisit/ReVisit/FlushDortor?doctorOpenid={0}";
        private string PayRefundWebSite = "ReVisit/ReVisit/PayRefund?orderSid={0}";
        private string SendCustomTextWebSite = "Base/SendCustomText?openid={0}&message={1}";
        #endregion

        #region 超时未关闭订单

        /// <summary>
        /// 处理超时未关闭订单
        /// </summary>
        public void TimeOutOrders()
        {
            try
            {
                LogHelper.ConsoleWriteLine("-------------------------开始超时未关闭订单的处理--------------------------------");
                var orderList = BusinessContent.FZ_Order.FindAll(string.Format("OrderType =0 AND Status = {0} and (ConversationStatus={1} OR ConversationStatus = 2) and EndTime <='{2}'", KeyConst.ReVisit_OrderStatus_3, KeyConst.ReVisit_ConversationStatus_7, DateTime.Now));
                if (null != orderList && orderList.Any())
                {
                    LogHelper.ConsoleWriteLine("订单总数：" + orderList.Count());
                    foreach (var order in orderList)
                    {
                        LogHelper.ConsoleWriteLine("正在处理：" + order.OrderSid);
                        string url = ReVisitConfig<string>(KeyConst.OrderAppId, "WebSite") + string.Format(FlushOrderWebSite, order.OrderSid);  //刷新订单缓存的URL

                        var doctorInfo = GetCacheDoctorByDoctorId(order.DoctorID);    //根据doctorId获取一个doctor信息

                        var obj = new { Status = KeyConst.ReVisit_OrderStatus_9, CloseTime = DateTime.Now, CloseReason = "超时自动完成", ConversationStatus = KeyConst.ReVisit_ConversationStatus_5 };
                        var res = BusinessContent.FZ_Order.Update("OrderSid", obj, order.OrderSid);

                        if (res > 0)
                        {
                            Instance.ChangeTotalReplyCount(order.OpenId, doctorInfo.OpenId);  // 累计医生的咨询数量

                            //数据库记录这次消息
                            AddConversationMessage(new Model.FZ_ConversationInfo
                            {
                                OrderSid = order.OrderSid,
                                ChatType = KeyConst.FZ_Conversations_Chat_Type_0,
                                ReceiverUserId = doctorInfo.UserId,
                                SenderOpenId = doctorInfo.OpenId,
                                IsRead = 0,
                                ReceiverOpenId = order.OpenId,
                                Source = KeyConst.FZ_Conversations_Source_4,
                                Content = TimeOutOrders_Msg,
                                TipContent = "提示：医助传达",
                                Status = 1
                            });

                            AddConversationMessage(new Model.FZ_ConversationInfo
                            {
                                OrderSid = order.OrderSid,
                                ChatType = KeyConst.FZ_Conversations_Chat_Type_0,
                                ReceiverUserId = doctorInfo.UserId,
                                SenderOpenId = doctorInfo.OpenId,
                                IsRead = 0,
                                ReceiverOpenId = order.OpenId,
                                Source = KeyConst.FZ_Conversations_Source_5,
                                Content = "本次咨询已满24小时，系统自动结束",
                                Status = 1
                            });

                            var html = HttpClientHelper.WebClientGet(url);  //刷新订单缓存
                            LogHelper.ConsoleWriteLine("刷新 " + order.OrderSid + " 结果为：" + html);
                            url = ReVisitConfig<string>(KeyConst.OrderAppId, "WebSite") + string.Format(FlushDortorWebSite, doctorInfo.OpenId);  //刷新一个医生的缓存信息
                            LogHelper.ConsoleWriteLine("刷新一个医生的缓存信息 " + doctorInfo.OpenId + " 结果为：" + html);
                        }
                        else
                        {
                            LogHelper.ConsoleWriteLine("处理：" + order.OrderSid + "失败");
                        }
                    }
                }
                else
                {
                    LogHelper.ConsoleWriteLine("没有查找到超时订单");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                LogHelper.ConsoleWriteLine("超时订单处理出错：" + ex.Message);
            }
            LogHelper.ConsoleWriteLine("-------------------------结束超时未关闭订单的处理--------------------------------");
        }
        #endregion

        #region 无效订单的处理

        /// <summary>
        /// 无效订单的处理
        /// </summary>
        public void InvalidTimeOutOrders()
        {
            try
            {
                //var ht = HttpClientHelper.WebClientGet(ReVisitConfig<string>("WebSite") + string.Format(SendCustomTextWebSite, "oES39thH2btDCM5i5RJDvze2l0ro", InvalidTimeOutOrders_Msg));

                LogHelper.ConsoleWriteLine("-------------------------开始无效订单的处理--------------------------------");
                var orderList = BusinessContent.FZ_Order.FindAll(string.Format("OrderType =0 AND Status = {0} and ConversationStatus={1} and EndTime <='{2}' AND (LEN(Transaction_id) > 0 OR LEN(Out_trade_no) > 0)", KeyConst.ReVisit_OrderStatus_3, KeyConst.ReVisit_ConversationStatus_1, DateTime.Now));
                if (null != orderList && orderList.Any())
                {
                    LogHelper.ConsoleWriteLine("订单总数：" + orderList.Count());
                    foreach (var order in orderList)
                    {
                        var oldOrder = new { order.Status, order.ConversationStatus, order.CloseReason, order.CloseTime, order.CancelTime };

                        LogHelper.ConsoleWriteLine("正在处理：" + order.OrderSid);
                        string url = ReVisitConfig<string>(KeyConst.OrderAppId, "WebSite") + string.Format(PayRefundWebSite, order.OrderSid);  //退款申请处理URL
                        string flushOrderUrl = ReVisitConfig<string>(KeyConst.OrderAppId, "WebSite") + string.Format(FlushOrderWebSite, order.OrderSid);  //刷新订单缓存的URL

                        var doctorInfo = GetCacheDoctorByDoctorId(order.DoctorID);    //根据doctorId获取一个doctor信息

                        var obj = new { Status = KeyConst.ReVisit_OrderStatus_7, ConversationStatus = KeyConst.ReVisit_ConversationStatus_5, CloseTime = DateTime.Now, CloseReason = "超时自动取消", CancelTime = DateTime.Now };
                        var res = BusinessContent.FZ_Order.Update("OrderSid", obj, order.OrderSid);

                        if (res > 0)
                        {
                            var html = HttpClientHelper.WebClientGet(flushOrderUrl);  //退款申请处理
                            LogHelper.ConsoleWriteLine("刷新 " + order.OrderSid + " 结果为：" + html);

                            html = HttpClientHelper.WebClientGet(url);  //退款申请处理
                            LogHelper.ConsoleWriteLine("退款申请处理 " + order.OrderSid + " 结果为：" + html);
                            if (!string.IsNullOrWhiteSpace(html))
                            {
                                var rm = JsonHelper.JsonStrToObject<ReturnMessage>(html);
                                if (null != rm && rm.IsSuccess)
                                {
                                    html = HttpClientHelper.WebClientGet(ReVisitConfig<string>(KeyConst.OrderAppId, "WebSite") + string.Format(SendCustomTextWebSite, order.OpenId, InvalidTimeOutOrders_Msg));
                                    LogHelper.ConsoleWriteLine("退款提醒消息 " + order.OpenId + " 结果为：" + html);
                                    continue;
                                }
                                else
                                {
                                    RestoreOrder(order.OrderSid, oldOrder);
                                }
                            }
                        }
                        else
                        {
                            LogHelper.ConsoleWriteLine("处理无效订单：" + order.OrderSid + "失败");
                        }
                    }
                }
                else
                {
                    LogHelper.ConsoleWriteLine("没有查找到无效订单");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                LogHelper.ConsoleWriteLine("无效订单的处理出错：" + ex.Message);
            }
            LogHelper.ConsoleWriteLine("-------------------------结束无效订单的处理--------------------------------");
        }

        #endregion

        #region 操作失败后需要将得到的状态在修改回来
        /// <summary>
        /// 操作失败后需要将得到的状态在修改回来
        /// </summary>
        /// <param name="orderSid"></param>
        /// <param name="oldOrder"></param>
        private void RestoreOrder(Guid orderSid, object oldOrder)
        {
            BusinessContent.FZ_Order.Update("OrderSid", oldOrder, orderSid);
            string url = ReVisitConfig<string>(KeyConst.OrderAppId, "WebSite") + string.Format(FlushOrderWebSite, orderSid);  //刷新订单缓存的URL
            var html = HttpClientHelper.WebClientGet(url);  //刷新订单缓存
        }
        #endregion

        #region 退款状态处理

        /// <summary>
        /// 退款状态处理
        /// </summary>
        public void PayRefundquery()
        {
            short payResult = 0;
            string refund_id = string.Empty;
            string refund_channel = string.Empty;
            string transaction_id = string.Empty;
            string out_refund_no = string.Empty;
            try
            {
                LogHelper.ConsoleWriteLine("-------------------------开始退款状态处理--------------------------------");
                var orderList = BusinessContent.FZ_Order.FindAll(string.Format("OrderType =0 AND Status = {0}", KeyConst.ReVisit_OrderStatus_10));
                if (null != orderList && orderList.Any())
                {
                    LogHelper.ConsoleWriteLine("订单总数：" + orderList.Count());
                    foreach (var order in orderList)
                    {
                        string url = ReVisitConfig<string>(KeyConst.OrderAppId, "WebSite") + string.Format(FlushOrderWebSite, order.OrderSid);  //刷新订单缓存的URL
                        LogHelper.ConsoleWriteLine("正在处理：" + order.OrderSid);

                        string payOpenId = "";
                        var prs = BusinessContent.FZ_PayRecords.FindOne("OrderSid=@0 AND Type=@1", order.OrderSid, KeyConst.FZ_PayRecordType_2);
                        if (null != prs)
                        {
                            payOpenId = prs.PayOpenId;
                            out_refund_no = prs.WechatOrderId;
                            var pr = PayRefundquery(KeyConst.PayAppId, out_refund_no);     // 查询退款
                            if (pr.Item1)
                            {
                                refund_id = pr.Item3;
                                refund_channel = pr.Item4;
                                transaction_id = pr.Item5;
                                payResult = 1;

                                var cres = BusinessContent.FZ_Order.Update("OrderSid", new { Status = KeyConst.ReVisit_OrderStatus_11 }, order.OrderSid);   //修改状态为已退款
                                ClearOrderCache(order.OrderSid);     //删除一个订单的缓存信息
                            }

                            var html = HttpClientHelper.WebClientGet(url);  //刷新订单缓存
                            LogHelper.ConsoleWriteLine("刷新 " + order.OrderSid + " 结果为：" + html);
                        }
                        else
                        {
                            LogHelper.ConsoleWriteLine("订单：" + order.OrderSid + "没有找到退款信息");
                        }
                        BusinessContent.FZ_PayRecords.Insert(new FZ_PayRecordInfo { OrderSid = order.OrderSid, Type = KeyConst.FZ_PayRecordType_3, Status = 1, PayResult = payResult, SerialNumber = refund_id, WechatOrderId = out_refund_no, WechatOrderName = refund_id, PayOpenId = payOpenId, Remark = "退款成功" });
                    }
                }
                else
                {
                    LogHelper.ConsoleWriteLine("没有查找到退款状态订单");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                LogHelper.ConsoleWriteLine("退款状态处理出错：" + ex.Message);
            }
            LogHelper.ConsoleWriteLine("-------------------------结束退款状态处理--------------------------------");
        }
        #endregion
    }
}
