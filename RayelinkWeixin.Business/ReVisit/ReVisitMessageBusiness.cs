﻿using Newtonsoft.Json;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.WxApi;
using RayelinkWeixin.WxApi.Model;
using Senparc.Weixin;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using System;

namespace RayelinkWeixin.Business
{
    /// <summary>
    /// 处理发送模板消息/客户消息
    /// </summary>
    public partial class ReVisitBusiness
    {

        #region 变量
        private const string RemindTempCacheKey = "RemindTemp_{0}_{1}_{2}";
        private const int RemindCount = 10;
        #endregion

        /// <summary>
        /// 校验用户连续发送消息而医生助理都没有回复是否超过10条（即10条连续聊天内容均为用户发送）
        /// </summary>
        /// <param name="openid">用户openid</param>
        /// <param name="doctorOpenId">医生openid</param>
        /// <returns>true：超过10条，false：没有超过，并且增加一条记录</returns>
        public bool CheckRemindTempFull(string openid, string doctorOpenId, Guid orderSid)
        {
            string key = string.Format(RemindTempCacheKey, openid, doctorOpenId, orderSid);
            int count = RemindTempCount(openid, doctorOpenId, orderSid);
            if (count > 0 && count % 10 == 0)
            {
                CacheHelper.SetCache(key, count + 1, 60);
                return true;
            }
            else
            {
                CacheHelper.SetCache(key, count + 1, 60);
                return false;
            }
        }

        /// <summary>
        /// 用户连续发送消息总数
        /// </summary>
        /// <param name="openid">用户openid</param>
        /// <param name="doctorOpenId">医生openid</param>
        /// <returns></returns>
        public int RemindTempCount(string openid, string doctorOpenId, Guid orderSid)
        {
            string key = string.Format(RemindTempCacheKey, openid, doctorOpenId, orderSid);
            var result = CacheHelper.GetCache(key);
            int count = 0;
            if (null != result)
            {
                count = Convert.ToInt32(result);
            }
            return count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="openid"></param>
        public void ClearRemindTempCountCache(string openid, string doctorOpenId, Guid orderSid)
        {
            string key = string.Format(RemindTempCacheKey, openid, doctorOpenId, orderSid);
            CacheHelper.RemoveCache(key);
        }

        #region 发送离线消息模板通知
        /// <summary>
        /// 发送离线消息模板通知           
        /// </summary>
        /// <param name="openids">医生和助理的openid</param>
        /// <param name="userName">患者名称</param>
        /// <param name="url">跳转的URL</param>
        /// <returns></returns>
        public void SendRemindTemp(short appid, string[] openids, string userName, string url = "", string remak = "")
        {
            try
            {
                foreach (var openid in openids)
                {
                    if (string.IsNullOrWhiteSpace(openid)) continue;
                    SendRemindTempMessage(appid, openid, "您有一条新的咨询消息", userName, remak, url);
                }
            }
            catch (Exception)
            {
                //  throw;
            }
        }

        /// <summary>
        /// 发送第一条病历消息(根据订单)
        /// </summary>
        /// <param name="openids">医生和助理的openid</param>
        /// <param name="orderSid">订单GUID</param>
        /// <param name="url">跳转的URL</param>
        public void SendOrderDescription(short appid, string[] openids, Guid orderSid, string url = "")
        {
            var orderInfo = Instance.OrderInfoBySid(orderSid);
            SendRemindTemp(appid, openids, orderInfo.UserName, url);
        }

        #endregion

        #region 发送消息提醒的模板消息
        /// <summary>
        /// 发送消息提醒的模板消息
        /// </summary>
        /// <param name="appid">微信标识</param>
        /// <param name="openid">接受者</param>
        /// <param name="content">问内容</param>
        /// <param name="keyWord1">提醒内容：患者姓名<提醒内容/param>
        /// <param name="remak">备注</param>
        /// <param name="url">跳转的url</param>
        /// <returns></returns>
        public bool SendRemindTempMessage(short appid, string openid, string content, string keyWord1, string remak = "", string url = "")
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(url) && !url.StartsWith("http"))
                {
                    url = BaseUrl + url;
                }
                url = url.Replace("openid={openid}", "openid=" + openid);
                var appInfo = new ShareFunction(appid).App;
                if (null != appInfo)
                {
                    WxTemplateData temp = new WxTemplateData()
                    {
                        first = new TemplateDataItem(content),
                        keyword1 = new TemplateDataItem(keyWord1),       //提醒内容：患者姓名
                        keyword2 = new TemplateDataItem(DateTime.Now.ToString()),//提醒时间：消息发送时间
                        remark = new TemplateDataItem(remak)     //咨询消息的全部文本（若为语音，显示“语音”；若为图片，不显示）
                    };
                    var res = new WxApi.TemplateMessage(appInfo.AppKey, appInfo.AppSecret).SendTemplateMessage(openid, TempMessageId, temp, url: url);
                    return res.errcode == ReturnCode.请求成功;
                }
                else
                {
                    LogHelper.Debug("SendRemindTempMessage 没有找到相应的app信息");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
            return false;
        }
        #endregion

        #region 发送客服消息
        /// <summary>
        /// 发送客服消息
        /// </summary>
        /// <param name="appid">微信标识</param>
        /// <param name="openid">消息接收者</param>
        /// <param name="text">消息文本</param>
        public bool SendCustomText(short appid, string openid, string text)
        {
            bool result = false;
            LogHelper.Debug("SendCustomText:" + openid + ">>>>" + text);
            try
            {
                var appInfo = new ShareFunction(appid).App;
                if (null != appInfo)
                {
                    var wx = new Custom(appInfo.AppKey, appInfo.AppSecret).SendText(openid, text);
                    result = wx.errmsg == "ok";

                    LogHelper.Debug("SendCustomText 返回值：" + JsonConvert.SerializeObject(wx));
                }
                else
                {
                    LogHelper.Debug("SendCustomText 没有找到相应的app信息");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                throw;
            }
            return result;
        }
        #endregion

    }
}
