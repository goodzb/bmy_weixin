﻿using Newtonsoft.Json.Linq;
using RayelinkWeixin.Common.Utilites;
using RayelinkWeixin.Const.Config;
using RayelinkWeixin.Const.Const;
using RayelinkWeixin.DLL;
using RayelinkWeixin.Model;
using RayelinkWeixin.WxApi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace RayelinkWeixin.Business
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ReVisitBusiness
    {
        #region 私有变量
        private static ReVisitBusiness m_Instance;
        private static object syncObject = new object();
        private const string ReVisitConfigKey = "FZ_AppConfig";
        private const string FriestOrderNoStr = "WXFZ{0}00001";
        private const string OrderNoStr = "WXFZ";
        private const int OrderNoNum = 100000;
        private const int CacheTimeOut = 12 * 60;//缓存超时时间 12 小时
        private const string LastSessionCacheKey = "LastSessionCacheKey_{0}";
        private const string OrderCacheKey = "Order_{0}";
        private const string UserByDoctorIdCacheKey = "UserByDoctorId_{0}";
        private const string ChangeOrderStatusSql = "UPDATE dbo.FZ_Orders SET Status={0} WHERE OrderSid='{1}' AND Status={2}";
        private const string ChangeConversationStatusSql = "UPDATE dbo.FZ_Orders SET ConversationStatus={0} WHERE OrderSid='{1}'";
        private const string ChangeEndDate_sql = "UPDATE dbo.FZ_Orders SET EndTime=@0 WHERE OrderSid=@1;";
        public const string TotalReplyCount_sql = "UPDATE dbo.FZ_Doctors SET TotalReplyCount=TotalReplyCount+1,RecentReplyCount=RecentReplyCount+1 WHERE OpenId=@0;";
        private const string DoctorsMapAssistKey = "FZ_DoctorsMapAssist";
        private const string TestUsersConfigKey = "FZ_TestUsers";
        private const string ConsultDoctorsConfigKey = "FZ_ConsultDoctors";
        private const string OrderTimeOutMinutesKey = "OrderTimeOutMinutes";
        private const string RemotelyMailKey = "RemotelyMail";
        private const string UserCacheKey = "UserCacheKey_{0}_{1}";
        private const string DoctorByDoctorOpenIdCacheKey = "DoctorByDoctorOpenId_{0}";
        private const string DoctorsMapAssistCacheKey = "DoctorsMapAssist_{0}_{1}";

        private readonly string BaseUrl = TextHelper.BaseUrl();
        /// <summary>
        /// 成功支付图文复诊订单
        /// </summary>
        private const string PaySucessText_1 = "专家暂时忙，请耐心等待。 专家回复后，会第一时间通知您,请注意帮忙医服务号的消息通知。<a href='{0}'>点这里立即查看</a>";

        /// <summary>
        /// 成功支付远程门诊订单
        /// </summary>
        private const string PaySucessText_2 = "您已成功付款，远程门诊预约卡号及密码已短信发送至您的手机，请注意查收！如有任何疑问，请联系021-62457275，谢谢配合！";


        private const string OrderList_Referral_SQL = @"SELECT o.OrderSid,o.UserName,CASE o.Gender when 1 THEN '男' ELSE '女' END as 'Gender',o.Birthday,o.TherapyDoctorName ,o.DoctorName,csm.MessageTime,csm.ReceiverOpenId,o.Age,o.Status, o.ConversationStatus,o.CreateTime AS 'OrderDate'
FROM FZ_Orders  o
INNER JOIN FZ_Users  u on o.OpenId=u.OpenId and u.UserType=0
LEFT JOIN(
SELECT cs.OrderSid, MAX(CreateTime) as 'MessageTime', MAX(cs.ReceiverOpenId) as'ReceiverOpenId',MAX(cs.Id) AS 'MId' from FZ_Conversations  cs
WHERE cs.Source= 0
GROUP BY cs.OrderSid) as csm on o.OrderSid=csm.OrderSid AND csm.MId=(SELECT TOP(1) Id FROM FZ_Conversations me WHERE me.OrderSid=o.OrderSid AND me.Source <=1 ORDER BY Id DESC )
where o.Status IN(3,8,9)  AND o.OrderType=0 {0}
ORDER BY o.CreateTime DESC";

        private const string OrderList_Interrogate_SQL = @"SELECT o.OrderSid,o.UserName,CASE o.Gender when 1 THEN '男' ELSE '女' END as 'Gender',d.Name as 'DoctorName',d.Title as 'DoctorTitle',csm.MessageTime,o.CreateTime as 'OrderTime',o.Age,o.Status, o.ConversationStatus
FROM FZ_Orders o 
INNER JOIN dbo.FZ_Users us ON us.Id=o.DoctorID
INNER JOIN FZ_Doctors d on  us.Id=d.UserId
LEFT JOIN(SELECT cs.OrderSid,MIN(CreateTime) as 'MessageTime' from FZ_Conversations cs
WHERE cs.Source=0 GROUP BY cs.OrderSid )as csm on o.OrderSid=csm.OrderSid
where  (( o.Status > 1 AND o.Status < 4) OR o.Status >5) AND o.OrderType=0 AND o.OpenId=@0 ORDER BY OrderTime DESC;";

        #endregion

        #region 单例
        /// <summary>
        /// 单例
        /// </summary>
        public static ReVisitBusiness Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    lock (syncObject)
                    {
                        if (m_Instance == null)
                        {
                            m_Instance = new ReVisitBusiness();
                        }
                    }
                }
                return m_Instance;
            }
        }
        #endregion

        #region 发送成功支付图文复诊订单消息
        /// <summary>
        /// 发送成功支付图文复诊订单消息
        /// </summary>
        /// <param name="openId"></param>
        public void SendPaySucessText(string openId)
        {
            new Custom(WeixinConfig.appID, WeixinConfig.appSecret).SendText(openId, PaySucessText_1);
        }
        #endregion

        #region 发送成功支付远程门诊订单消息
        /// <summary>
        /// 发送成功支付远程门诊订单消息
        /// </summary>
        /// <param name="openId"></param>
        public void SendPayRemoteSucessText(string openId)
        {
            new Custom(WeixinConfig.appID, WeixinConfig.appSecret).SendText(openId, PaySucessText_2);
        }
        #endregion

        #region 会诊配置信息中其中一项的值
        /// <summary>
        /// 会诊配置信息中其中一项的值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T ReVisitConfig<T>(short appid, string key)
        {
            return new LangAdapter(appid).GetLangAdapterValue<T>(ReVisitConfigKey, key);
        }
        #endregion

        #region 设置订单超时时间间隔(分钟)
        /// <summary>
        /// 设置订单超时时间间隔(分钟)
        /// </summary>
        /// <returns></returns>
        public int OrderTimeOutMinutes
        {
            get { return ReVisitConfig<int>(KeyConst.OrderAppId, OrderTimeOutMinutesKey); }
        }
        #endregion

        #region 接收卡密记录的邮件地址
        /// <summary>
        /// 接收卡密记录的邮件地址
        /// </summary>
        /// <returns></returns>
        public string RemotelyMail
        {
            get
            {
                return ReVisitConfig<string>(KeyConst.OrderAppId, RemotelyMailKey);
            }
        }
        #endregion

        #region 配置的测试人员信息
        /// <summary>
        /// 配置的测试人员信息
        /// </summary>
        public JObject TestUsers
        {
            get
            {
                JObject result = new LangAdapter(KeyConst.OrderAppId).TryGetJObject(TestUsersConfigKey);
                return result;
            }
        }
        #endregion

        #region 会诊医生信息配置
        /// <summary>
        /// 会诊医生信息配置
        /// </summary>
        public JArray ConsultDoctors
        {
            get
            {
                JArray result = new LangAdapter(KeyConst.OrderAppId).TryGetJArray(ConsultDoctorsConfigKey);
                return result;
            }
        }
        #endregion

        #region 医生与助理关系映射配置信息
        /// <summary>
        /// 医生与助理关系映射配置信息
        /// </summary>
        /// <returns></returns>
        public string DoctorsMapAssistConfig
        {
            get
            {
                var lang = new LangAdapter(KeyConst.OrderAppId);
                string str = lang.LangBag.FZ_DoctorsMapAssist;
                return str;
            }
        }
        #endregion

        #region 根据医生ID获取对应的助理信息
        /// <summary>
        /// 根据医生ID获取对应的助理信息
        /// </summary>
        /// <param name="doctorId"></param>
        /// <returns></returns>
        public FZ_UserInfo GetReVisitUserByDoctorId(int doctorId)
        {
            string cacheKey = string.Format(UserByDoctorIdCacheKey, doctorId);
            var result = CacheHelper.GetCache(cacheKey) as FZ_UserInfo;
            if (null == result)
            {
                result = BusinessContent.FZ_User.FindOne(string.Format("UserType={0} AND Id =(SELECT TOP(1) AssistantId FROM [FZ_DoctorsMapAssist] WHERE Status >0 AND DoctorId={1})", KeyConst.ReVisit_UserType_Assistant, doctorId), "");
                if (null != result)
                {
                    CacheHelper.SetCache(cacheKey, result, CacheTimeOut);
                }
            }
            return result;
        }
        #endregion

        #region 根据OpenId获取一条用户信息
        /// <summary>
        /// 根据OpenId获取一条用户信息(先从缓存中取，再从数据库中取)
        /// </summary>
        /// <param name="openid">openid</param>
        /// <returns></returns>
        public FZ_UserInfo GetUserCacheByOpenId(string openid)
        {
            FZ_UserInfo info = GetUserCacheByOpenId(openid, KeyConst.ReVisit_UserType_User);
            if (null == info)
            {
                info = GetUserCacheByOpenId(openid, KeyConst.ReVisit_UserType_Assistant);
            }
            if (null == info)
            {
                info = GetUserCacheByOpenId(openid, KeyConst.ReVisit_UserType_Doctor);
            }
            return info;
        }

        /// <summary>
        /// 根据OpenId获取一条用户信息(先从缓存中取，再从数据库中取)
        /// </summary>
        /// <param name="openid">openid</param>
        /// <param name="userType">类型</param>
        /// <returns></returns>
        public FZ_UserInfo GetUserCacheByOpenId(string openid, int userType)
        {
            FZ_UserInfo info = null;
            string cacheKey = string.Format(UserCacheKey, openid, userType);

            info = CacheHelper.GetCache(cacheKey) as FZ_UserInfo;
            if (null == info)
            {
                info = GetUserByOpenId(openid, userType);
                if (null != info)
                {
                    CacheHelper.SetCache(cacheKey, info, CacheTimeOut);
                }
            }
            return info;
        }

        /// <summary>
        /// 根据OpenId获取一条用户信息(直接从数据库中取)
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="userType">用户类型: 0-用户,1-医生，2-助理</param>
        /// <returns></returns>
        private FZ_UserInfo GetUserByOpenId(string openid, int userType)
        {
            return BusinessContent.FZ_User.FindOne(string.Format("OpenId='{0}' AND userType={1}", openid, userType), "");
        }

        /// <summary>
        /// 根据openid获取一个UID
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="userType">用户类型: 0-用户,1-医生，2-助理</param>
        /// <returns></returns>
        public int UserId(string openid, int userType)
        {
            var userInfo = GetUserCacheByOpenId(openid, userType);
            if (null != userInfo)
            {
                return userInfo.Id;
            }
            return 0;
        }

        /// <summary>
        /// 清空一条用户信息缓存
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="userType">用户类型: 0-用户,1-医生，2-助理</param>
        public void ClearUserCacheByOpenId(string openid, int userType)
        {
            string cacheKey = string.Format(UserCacheKey, openid, userType);
            CacheHelper.RemoveCache(cacheKey);
        }
        #endregion

        #region 根据doctorId获取一个doctor信息

        /// <summary>
        /// 根据doctorId获取一个doctor信息(先从缓存中取，再从数据库中取)
        /// </summary>
        /// <param name="doctorId">User表中的ID</param>
        /// <returns></returns>
        public FZ_DoctorInfo GetCacheDoctorByDoctorId(int doctorId)
        {
            FZ_DoctorInfo doctor = null;
            string cacheKey = string.Format(DoctorByDoctorOpenIdCacheKey, doctorId);
            doctor = CacheHelper.GetCache(cacheKey) as FZ_DoctorInfo;
            if (null == doctor)
            {
                doctor = GetDoctorByDoctorId(doctorId);
                if (null != doctor)
                {
                    CacheHelper.SetCache(cacheKey, doctor, CacheTimeOut);
                }
            }
            return doctor;
        }

        /// <summary>
        /// 根据OpenId获取一条医生信息(直接从数据库中取)
        /// </summary>
        /// <param name="doctorId"></param>
        /// <returns></returns>
        private FZ_DoctorInfo GetDoctorByDoctorId(int doctorId)
        {
            return BusinessContent.FZ_Doctor.FindOne(string.Format("UserId={0}", doctorId), "");
        }

        /// <summary>
        /// 清除一个Doctor的缓存信息
        /// </summary>
        /// <param name="doctorId"></param>
        public void ClearCacheDoctor(int doctorId)
        {
            string cacheKey = string.Format(DoctorByDoctorOpenIdCacheKey, doctorId);
            CacheHelper.RemoveCache(cacheKey);
        }
        #endregion

        #region 根据一个助理的OpenId获取关联的所有医生列表
        /// <summary>
        /// 根据一个助理的OpenId获取关联的所有医生列表
        /// </summary>
        /// <param name="assistantOpenId">助理openid</param>
        /// <returns></returns>
        public IEnumerable<FZ_UserInfo> GetDoctorByAssistantOpenId(string assistantOpenId)
        {
            int assistantId = UserId(assistantOpenId, KeyConst.ReVisit_UserType_Assistant);
            string cacheKey = string.Format(DoctorsMapAssistCacheKey, assistantOpenId, assistantId);
            var result = CacheHelper.GetCache(cacheKey) as IEnumerable<FZ_UserInfo>;
            if (null == result)
            {
                if (assistantId > 0)
                {
                    result = BusinessContent.FZ_User.FindAll(string.Format("UserType={0} AND Id IN(SELECT DoctorId FROM [FZ_DoctorsMapAssist] WHERE Status >0 AND AssistantId={1})", KeyConst.ReVisit_UserType_Doctor, assistantId));
                    if (null != result)
                    {
                        CacheHelper.SetCache(cacheKey, result, CacheTimeOut);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 清空一个助理的OpenId关联的医生列表缓存信息
        /// </summary>
        /// <param name="assistantOpenId"></param>
        public void ClearDoctorMapAssistant(string assistantOpenId)
        {
            int assistantId = UserId(assistantOpenId, KeyConst.ReVisit_UserType_Doctor);
            string cacheKey = string.Format(DoctorsMapAssistCacheKey, assistantOpenId, assistantId);
            ClearCacheKey(cacheKey);
        }

        /// <summary>
        /// 执行RemoveCache
        /// </summary>
        /// <param name="key"></param>
        private void ClearCacheKey(string key)
        {
            CacheHelper.RemoveCache(key);
        }
        #endregion

        #region 修改一个订单的状态
        /// <summary>
        /// 修改一个订单的状态
        /// </summary>
        /// <param name="openid">操作者</param>
        /// <param name="orderSid">订单GUID</param>
        /// <param name="oldStatus">现有状态</param>
        /// <param name="newStatus">更改为新的状态</param>
        /// <returns>true：修改成功</returns>
        public bool ChangeOrderStatus(string openid, Guid orderSid, short oldStatus, short newStatus)
        {
            var res = BusinessContent.FZ_Order.Execute(string.Format(ChangeOrderStatusSql, newStatus, orderSid, oldStatus));
            UserActionEventLogBusiness.Instance.Push(new UserActionEventLogInfo { AppId = 1, OpenId = openid, ActionName = "ChangeOrderStatus", ActionType = KeyConst.Uceat_Event, Parameter = res + "" });
            ClearOrderCache(orderSid);  //删除订单缓存信息
            return res > 0;
        }
        #endregion

        #region 修改一个订单的会话状态标示
        /// <summary>
        /// 修改一个订单的会话状态标示
        /// </summary>
        /// <param name="openid">操作者</param>
        /// <param name="orderSid">订单GUID</param>
        /// <param name="oldStatus">现有会话状态</param>
        /// <param name="newStatus">更改为新的会话状态</param>
        /// <returns>true：修改成功</returns>
        public bool ChangeOrderConversationStatus(string openid, Guid orderSid, short oldStatus, short newStatus)
        {
            var res = BusinessContent.FZ_Order.Execute(string.Format(ChangeConversationStatusSql, newStatus, orderSid, oldStatus));
            UserActionEventLogBusiness.Instance.Push(new UserActionEventLogInfo { AppId = 1, OpenId = openid, ActionName = "ChangeOrderConversationStatus", ActionType = KeyConst.Uceat_Event, Parameter = res + "" });
            ClearOrderCache(orderSid);  //删除订单缓存信息
            return res > 0;
        }
        #endregion

        #region 修改订单的结束日期
        /// <summary>
        /// 修改订单的结束日期
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="orderSid"></param>
        /// <returns></returns>
        public bool ChangeOrderEndDate(string openid, Guid orderSid)
        {
            var res = BusinessContent.FZ_Order.Execute(ChangeEndDate_sql, DateTime.Now.AddHours(24), orderSid);
            UserActionEventLogBusiness.Instance.Push(new UserActionEventLogInfo { AppId = 1, OpenId = openid, ActionName = "ChangeOrderEndDate", ActionType = KeyConst.Uceat_Event, Parameter = res + "" });
            ClearOrderCache(orderSid);  //删除订单缓存信息
            return res > 0;
        }
        #endregion

        #region 累计医生的咨询数量
        /// <summary>
        /// 累计医生的咨询数量
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="doctorOpenid"></param>
        /// <returns></returns>
        public bool ChangeTotalReplyCount(string openid, string doctorOpenid)
        {
            var res = BusinessContent.FZ_Order.Execute(TotalReplyCount_sql, doctorOpenid);
            UserActionEventLogBusiness.Instance.Push(new UserActionEventLogInfo { AppId = 1, OpenId = openid, ActionName = "ChangeTotalReplyCount", ActionType = KeyConst.Uceat_Event, Parameter = res + "" });
            ClearUserCacheByOpenId(doctorOpenid, KeyConst.ReVisit_UserType_Doctor);  //清空一条用户信息缓存
            return res > 0;
        }
        #endregion

        #region 生成一个新的订单编号(每天从头开始)
        /// <summary>
        /// 生成一个新的订单编号(每天从头开始)
        /// </summary>
        /// <param name="appid"></param>
        public string NewOrderNo
        {
            get
            {
                string dt = DateTime.Now.yyyyMMdd();
                string result = string.Format(FriestOrderNoStr, dt);
                var order = BusinessContent.FZ_Order.FindOne(string.Format("LEN(OrderNo)>0 AND DATEDIFF(DAY,CreateTime,'{0}')=0 ", DateTime.Now), "ORDER BY OrderNo DESC", true);
                if (null != order && !string.IsNullOrWhiteSpace(order.OrderNo))
                {
                    var num = OrderNoNum + Convert.ToInt32(order.OrderNo.Replace(OrderNoStr + dt, "")) + 1;
                    result = OrderNoStr + dt + num;
                    result = result.Replace(OrderNoStr + dt + "1", OrderNoStr + dt);
                }
                return result;
            }
        }
        #endregion

        #region 当前医生付费咨询总人数
        /// <summary>
        /// 当前医生付费咨询总人数
        /// </summary>
        /// <param name="doctorId"></param>
        /// <returns></returns>
        public int PayCount(int doctorId)
        {
            return BusinessContent.FZ_Order.FindCount(string.Format("DoctorID={0} AND OrderType={1} AND Status IN(8,9) ", doctorId, KeyConst.ReVisit_OrderType_0));
        }
        #endregion

        #region 动态变量
        /// <summary>
        /// 所有的医生列表
        /// </summary>
        public IEnumerable<FZ_UserInfo> Doctors
        {
            get
            {
                return BusinessContent.FZ_User.Find(string.Format("UserType={0}", KeyConst.ReVisit_UserType_Doctor), KeyConst.BasePageSize);
            }
        }

        /// <summary>
        /// 所有的助理列表
        /// </summary>
        public IEnumerable<FZ_UserInfo> Assistants
        {
            get
            {
                return BusinessContent.FZ_User.Find(string.Format("UserType={0}", KeyConst.ReVisit_UserType_Assistant), KeyConst.BasePageSize);
            }
        }

        /// <summary>
        /// 所有的用户列表
        /// </summary>
        public IEnumerable<FZ_UserInfo> Users
        {
            get
            {
                return BusinessContent.FZ_User.Find(string.Format("UserType={0}", KeyConst.ReVisit_UserType_User), KeyConst.BasePageSize);
            }
        }
        /// <summary>
        /// 所有的助理和医生列表
        /// </summary>
        public IEnumerable<FZ_UserInfo> Doctors_Assistants
        {
            get
            {
                return BusinessContent.FZ_User.Find(string.Format("UserType={0} OR UserType={1}", KeyConst.ReVisit_UserType_Doctor, KeyConst.ReVisit_UserType_Assistant), KeyConst.BasePageSize);
            }
        }

        /// <summary>
        /// 助理与医生对应关系信息
        /// </summary>
        public IEnumerable<FZ_DoctorsMapAssistInfo> DoctorsMapAssists
        {
            get
            {
                return BusinessContent.FZ_DoctorsMapAssist.Find("", KeyConst.BasePageSize);
            }
        }

        /// <summary>
        /// 所有的医生详细信息列表
        /// </summary>
        public IEnumerable<FZ_DoctorInfo> DoctorsInfo
        {
            get
            {
                return BusinessContent.FZ_Doctor.Find("", KeyConst.BasePageSize, "Sort");
            }
        }

        /// <summary>
        /// 模板消息ID
        /// </summary>
        public string TempMessageId
        {
            get
            {
                var lang = new LangAdapter(KeyConst.OrderAppId);
                return lang.LangBag.FZ_TempMessageId;
            }
        }
        #endregion

        #region 根据价格找卡类别ID
        /// <summary>
        /// 根据价格找卡类别ID
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public int CardTypeByPrice(decimal price)
        {
            int cardTypeId = -1;
            try
            {
                var lang = new LangAdapter(KeyConst.OrderAppId);
                var con = lang.TryGetJArray("FZ_Price_CardType");
                if (null != con && con.Any())
                {
                    cardTypeId = con.FirstOrDefault(t => t.Value<decimal>("Price") == price).Value<int>("CardTypeID");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
            return cardTypeId;
        }
        #endregion

        #region 获取历史消息
        /// <summary>
        /// 获取历史消息
        /// </summary>
        /// <param name="orderSid">订单GUID</param>
        /// <param name="receiverOpenId">接收者</param>
        /// <param name="sourceType">数据类型 0：用户进入,1:医生进入，2：助理进入</param>
        /// <param name="senderOpenId">发送者</param>
        /// <returns></returns>
        public IEnumerable<FZ_ConversationInfo> GetHistoryConversation(Guid orderSid, short sourceType = 0, string receiverOpenId = "", string senderOpenId = "")
        {
            string sql = string.Format("OrderSid='{0}' AND (Source = 0 OR Source =1 OR Source =5)", orderSid);
            if (sourceType.Equals(0))
            {
                sql = string.Format("OrderSid='{0}' AND (Source = 0 OR Source =1 OR Source =4)", orderSid);
            }

            if (!string.IsNullOrEmpty(senderOpenId))
            {
                sql += string.Format(" AND SenderOpenId='{0}'", senderOpenId);
            }
            if (!string.IsNullOrEmpty(receiverOpenId))
            {
                sql += string.Format(" AND ReceiverOpenId='{0}'", receiverOpenId);
            }
            return BusinessContent.FZ_Conversations.FindAll(sql, "CreateTime"); ;
        }
        #endregion

        #region  记录一次session变化行为
        /// <summary>
        ///  记录一次session变化行为(数据库记录)
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="connectionId">当前sessionId</param>
        /// <param name="type">类型(1：上线，2：下线)</param>
        /// <param name="remarks">备注</param>
        public SessionLogInfo AddSessionLog(string openId, string connectionId, short type = KeyConst.Session_Type_1, string remarks = "")
        {
            SessionLogInfo info = new SessionLogInfo
            {
                SessionSid = Guid.NewGuid(),
                Status = 1,
                OpenId = openId,
                ConnectionId = connectionId,
                Type = type,
                CreateTime = DateTime.Now,
                OnlineTime = 0,
                Remarks = remarks
            };
            if (type == KeyConst.Session_Type_2)
            {
                var onLine = LastSession(openId);
                if (null != onLine)
                {
                    info.OnlineTime = Convert.ToInt32((info.CreateTime - onLine.CreateTime).TotalSeconds);
                }
            }
            info.SessionId = BusinessContent.SessionLog.Insert(info);
            return info;
        }
        #endregion

        #region 获取一个openid最后一个在线的session记录
        /// <summary>
        /// 获取一个openid最后一个在线的session记录
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        public SessionLogInfo LastSession(string openid)
        {
            string cacheKey = string.Format(LastSessionCacheKey, openid);
            var result = CacheHelper.GetCache(cacheKey) as SessionLogInfo;
            if (null == result)
            {
                result = LastSessionByOpenId(openid);
                if (null != result)
                {
                    CacheHelper.SetCache(cacheKey, result, CacheTimeOut);
                }
            }
            return result;
        }

        /// <summary>
        /// 获取一个openid最后一个在线的session记录
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        private SessionLogInfo LastSessionByOpenId(string openid)
        {
            return BusinessContent.SessionLog.FindOne(string.Format("Type={0} AND OpenId='{1}'AND SessionId >(SELECT  TOP(1) SessionId FROM dbo.SessionLog WHERE Type={2} AND OpenId='{1}' ORDER BY SessionId DESC)", KeyConst.Session_Type_1, openid, KeyConst.Session_Type_2), "");
        }

        /// <summary>
        /// 清空一个用户的session缓存信息
        /// </summary>
        /// <param name="openid"></param>
        public void ClearSessionCache(string openid)
        {
            string cacheKey = string.Format(LastSessionCacheKey, openid);
            CacheHelper.RemoveCache(cacheKey);
        }
        #endregion

        #region 数据库增加一条会话消息记录
        /// <summary>
        /// 数据库增加一条会话消息记录
        /// </summary>
        /// <param name="info"></param>
        public void AddConversationMessage(FZ_ConversationInfo info)
        {
            info.CreateTime = DateTime.Now;
            info.Status = 1;
            BusinessContent.FZ_Conversations.Insert(info);
        }
        #endregion    

        #region 获取一个订单的详细信息
        /// <summary>
        /// 获取一个订单的详细信息(直接从数据中取值)
        /// </summary>
        /// <param name="orderSid">订单GUID</param>
        /// <returns></returns>
        public FZ_OrderInfo GetOrderBySid(Guid orderSid)
        {
            return BusinessContent.FZ_Order.FindOne(string.Format("OrderSid='{0}'", orderSid), "");
        }

        /// <summary>
        /// 从缓存里面获取一个订单的信息
        /// </summary>
        /// <param name="orderSid"></param>
        /// <returns></returns>
        public FZ_OrderInfo OrderInfoBySid(Guid orderSid, bool cache = true)
        {
            FZ_OrderInfo result = null;
            if (cache)
            {
                string cacheKey = string.Format(OrderCacheKey, orderSid);
                result = CacheHelper.GetCache(cacheKey) as FZ_OrderInfo;
                if (null == result)
                {
                    result = GetOrderBySid(orderSid);
                    if (null != result)
                    {
                        CacheHelper.SetCache(cacheKey, result, CacheTimeOut);
                    }
                }
            }
            else
            {
                result = GetOrderBySid(orderSid);
            }
            return result;
        }

        /// <summary>
        /// 删除一个订单的缓存信息
        /// </summary>
        /// <param name="orderSid"></param>
        public void ClearOrderCache(Guid orderSid)
        {
            string cacheKey = string.Format(OrderCacheKey, orderSid);
            CacheHelper.RemoveCache(cacheKey);
        }
        #endregion

        #region 我的看诊
        /// <summary>
        /// 我的看诊
        /// </summary>
        /// <param name="openid">当前微信</param>
        /// <param name="dcotorOpenId">医生ID</param>
        /// <param name="doctorId">助理ID</param>
        /// <returns></returns>
        public DataTable OrderList_Referral(string openid, int doctorId = 0, int assistantId = 0)
        {
            string wh = string.Format(" AND DoctorID= {0}", doctorId);
            if (doctorId == 0 && assistantId > 0)  //助理
            {
                wh = string.Format(" AND DoctorID IN ({0})", string.Join(",", GetDoctorByAssistantOpenId(openid).Select(t => t.Id)));
            }
            string sql = string.Format(OrderList_Referral_SQL, wh);

            var dt = BusinessContent.FZ_Order.FillTable(sql);
            return dt;
        }
        #endregion

        #region 我的问诊
        /// <summary>
        /// 我的问诊
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        public DataTable OrderList_Interrogate(string openid)
        {
            var dt = BusinessContent.FZ_Order.FillTable(OrderList_Interrogate_SQL, openid);
            return dt;
        }
        #endregion

        #region 网页端调起支付API
        /// <summary>
        /// 网页端调起支付API
        /// </summary>
        /// <param name="openid">openid</param>
        /// <param name="orderNo">订单编号</param>
        /// <param name="orderSid">订单GUID</param>
        /// <param name="orderAmount">订单金额(以分为单位)</param>
        /// <param name="userHostAddress">用户的公网ip，不是商户服务器IP</param>
        /// <param name="notify_url">接收财付通通知的URL</param>
        /// <returns></returns>
        private WxApi.Model.WeixinPayModel WeChatPay(short appid, string openid, string orderNo, Guid orderSid, string orderAmount, string userHostAddress, string notify_url)
        {
            var appInfo = new ShareFunction(appid).App;
            if (null != appInfo)
            {
                return new Pay(appInfo.AppKey, appInfo.AppSecret, WeixinConfig.TenPayV3_MchId, WeixinConfig.TenPayV3_Key).WeChatPayByOpenId(openid, new Dictionary<string, string>() {
                { "body","微信复诊"},
                { "notify_url",notify_url},
                { "out_trade_no",orderNo},          //商家订单号
                { "total_fee",orderAmount},         //商品金额,以分为单位(money * 100).ToString()
                { "spbill_create_ip",userHostAddress}, //用户的公网ip，不是商户服务器IP
                { "openid",openid},              //用户的openId
                { "attach",orderSid.ToString()},
            });
            }
            else
            {
                throw new Exception("找不到对应的app信息");
            }
        }

        /// <summary>
        /// 网页端调起支付API
        /// </summary>
        /// <param name="openid">openid</param>
        /// <param name="orderNo">订单编号</param>
        /// <param name="orderSid">订单GUID</param>
        /// <param name="orderAmount">订单金额(以分为单位)</param>
        /// <param name="notify_url">接收财付通通知的URL</param>
        /// <returns></returns>
        public Common.Share.ReturnMessage GetWeChatPay(short appid, string openid, string orderNo, Guid orderSid, string orderAmount, string notify_url)
        {
            Common.Share.ReturnMessage rm = new Common.Share.ReturnMessage { Text = "操作未完成" };
            try
            {
                LogHelper.Debug("网页端调起支付API来了：notify_url：" + notify_url);
                if (KeyConst.PayTest == "1")
                {
                    orderAmount = "1";
                }
                rm.Text = "操作完成";
                rm.IsSuccess = true;
                rm.ResultData["Result"] = WeChatPay(appid, openid, orderNo, orderSid, orderAmount, IPHelper.ClientIP(), notify_url);
                LogHelper.Debug("网页端调起支付API走了");
            }
            catch (Exception ex)
            {
                rm.IsSuccess = false;
                rm.Text = "操作出错";
                rm.Message = ex.Message;
                var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
                if (declaringType != null) { ex.Data["Method"] = declaringType.Name + "=>" + MethodBase.GetCurrentMethod().Name; }
                LogHelper.Exception(ex);
            }
            return rm;
        }
        #endregion

        #region 根据订单发起退款申请

        /// <summary>
        /// 根据订单发起退款申请
        /// </summary>
        /// <param name="orderInfo">订单信息</param>
        /// <returns>1：false：失败 true：成功 2：退款申请返回的原XML 3：result_code 4：out_refund_no 5：refund_id</returns>
        public Tuple<bool, string, string, string, string> PayRefundByOrder(short appid, FZ_OrderInfo orderInfo)
        {
            try
            {
                var appInfo = new ShareFunction(appid).App;
                if (null == appInfo)
                {
                    throw new Exception("找不到对应的app信息");
                }
                decimal orderAmount = orderInfo.OrderAmount;
                string out_refund_no = orderInfo.Out_trade_no;
                if (KeyConst.PayTest == "1")
                {
                    out_refund_no = RadomHelper.GetRandomString(30, false);
                    orderAmount = 1;
                }
                return new Pay(appInfo.AppKey, appInfo.AppSecret, WeixinConfig.TenPayV3_MchId, WeixinConfig.TenPayV3_Key).Refund(WeixinConfig.TenPayV3_CertPath, WeixinConfig.TenPayV3_CertPasWord, new Dictionary<string, string>() {
                { "out_trade_no",orderInfo.Out_trade_no},  //商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
               { "out_refund_no",out_refund_no},//商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。
                { "total_fee",orderAmount+""},//订单总金额，单位为分，只能为整数，详见支付金额
                { "refund_fee",orderAmount+""},//退款总金额，订单总金额，单位为分，只能为整数，详见支付金额
                { "op_user_id",WeixinConfig.TenPayV3_MchId}, //操作员Id，默认就是商户号             
            });
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                throw;
            }
        }
        #endregion

        #region 查询退款
        /// <summary>
        /// 查询退款
        /// </summary>
        /// <param name="out_refund_no">商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。</param>
        /// <returns></returns>
        public Tuple<bool, string, string, string, string> PayRefundquery(short appid, string out_refund_no)
        {
            try
            {
                var appInfo = new ShareFunction(appid).App;
                if (null == appInfo)
                {
                    throw new Exception("找不到对应的app信息");
                }
                return new Pay(appInfo.AppKey, appInfo.AppSecret, WeixinConfig.TenPayV3_MchId, WeixinConfig.TenPayV3_Key).PayRefundquery(new Dictionary<string, string>() { { "out_refund_no", out_refund_no } });
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                throw;
            }
        }
        #endregion
    }
}
