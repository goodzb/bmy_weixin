﻿using System;
using System.Collections.Generic;
using System.Web;

namespace RayelinkWeixin.WebAPI.Models
{
    public class ReferralModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Desc { get; set; }
        public int OrganizationId { get; set; }
        public int DocId { get; set; }
        public string Status { get; set; }
        public int CreaterId { get; set; }
        public string CreateTime { get; set; }
        public string CreaterName { get; set; }
        public int Sex { get; set; }
        public int Age { get; set; }
        public string Imgs { get; set; }

    }

    public class PageReferral
    {
        public int TotalCount { get; set; }
        public int TotalPage { get; set; }
        public int CurrentPage { get; set; }

        public List<ReferralModel> Result { get; set; }

    }
}
