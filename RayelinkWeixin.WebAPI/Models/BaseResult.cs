﻿using System;
using System.Web;

namespace RayelinkWeixin.WebAPI.Models
{
    public class BaseResult 
    {
        public BaseResult()
        {
            Code = "200";
            ErrorMsg = "";
        }

        public string Code { get; set; }
        public string ErrorMsg { get; set; }
        public object Content { get; set; }

        public void SetContent(object obj)
        {
            Content = obj;
        }
        public void SetEror(string msg, string code)
        {
            Code = code;
            ErrorMsg = msg;
        }
    }
}
