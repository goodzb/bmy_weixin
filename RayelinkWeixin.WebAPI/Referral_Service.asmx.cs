﻿using RayelinkWeixin.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using RayelinkWeixin.Data.Dao;
using RayelinkWeixin.Data.PO.Extend;
using System.Text;
using RayelinkWeixin.WebAPI.Mapper;
using System.Xml.Serialization;
using RayelinkWeixin.Data.Repository;
using RayelinkWeixin.Common.Utilites;
using System.Configuration;

namespace RayelinkWeixin.WebAPI
{
    /// <summary>
    /// Referral_Service 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。
    // [System.Web.Script.Services.ScriptService]
    public class Referral_Service : System.Web.Services.WebService
    {
        [XmlInclude(typeof(PageReferral))]
        [WebMethod]
        public BaseResult GetReferralsByPage(string status, int organizationId, int pagesize, int pagenumber)
        {
            BaseResult res = new BaseResult();
            List<ReferralExtend> refs = ReferralDao.Instance.FindByPageExport(status, organizationId, pagesize, pagenumber);
            List<ReferralModel> list = ReferralMapper.ReferralDo2Dto(refs);
            int count = ReferralDao.Instance.GetCount(status, organizationId);
            int pagecount = (count + pagesize - 1) / pagesize;
            PageReferral pageRef = new PageReferral
            {
                CurrentPage = pagenumber,
                TotalCount = count,
                TotalPage = pagecount,
                Result = list
            };
            res.SetContent(pageRef);
            return res;
        }

        /// <summary>
        /// 修改转诊信息
        /// </summary>
        /// <param name="refId">转诊Id</param>
        /// <param name="status">要修改的状态</param>
        /// <param name="appointmentNo">预约单号</param>
        /// <param name="appointDate">预约日期</param>
        /// <param name="timeDuring">预约的时间段</param>
        /// <returns></returns>
        [WebMethod]
        public BaseResult UpdateReferral(int refId, string status, string appointmentNo, string appointDate,string timeDuring="")
        {
            BaseResult res = new BaseResult();
            try
            {
                ReferralExtend referral = ReferralRepository.Instance.FindReferralById(refId);
                if (referral == null)
                    return res;

                if (!string.IsNullOrEmpty(status))
                    referral.Status = status;
                if (!string.IsNullOrEmpty(appointmentNo))
                    referral.AppointmentNo = appointmentNo;
                if (!string.IsNullOrEmpty(appointDate))
                {
                    referral.AppointmentTime = Convert.ToDateTime(appointDate);
                    referral.TimeDuring = ((DateTime)referral.AppointmentTime).ToString("yyyy-MM-dd") + " " + timeDuring;
                }

                 ReferralRepository.Instance.UpdateReferral(referral, 0);
                string domain = ConfigurationManager.AppSettings["webDomain"];
                string jsonStr = JsonHelper.ObjectToJSON(referral);

                string r = HttpHelper.PostReq(domain + "/service/PushWxMsg", jsonStr);
                LogHelper.Debug("请求推送的结果："+r);

                res.SetContent(r);
            }
            catch (Exception e)
            {
                LogHelper.Error(e);
            }

            return res;
        }
    }
}
